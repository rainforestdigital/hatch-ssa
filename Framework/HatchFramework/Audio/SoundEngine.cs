using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace HatchFramework
{
	public delegate void OnAudioWord(string eventID);
	public delegate void OnAudioComplete();
	
	public class SoundEngine : MonoBehaviour
	{
		OnAudioWord OnAudioWordEventInvoker;
		public event OnAudioWord OnAudioWordEvent{ add{OnAudioWordEventInvoker += value;} remove{OnAudioWordEventInvoker -= value;} }	
		OnAudioComplete OnAudioCompleteEventInvoker;
		public event OnAudioComplete OnAudioCompleteEvent { add{OnAudioCompleteEventInvoker += value;} remove{OnAudioCompleteEventInvoker -= value;} }	

		public static AudioSource themeAudio;
		protected static SoundEngine instance;
		
		protected static float themeVolume = 0.45f;
		protected static float themeDuckedVolume = 0.15f;
		
		private CancelToken cancelToken = new CancelToken();
		private PauseToken pauseToken = new PauseToken();
		
		public static SoundEngine Instance{
			get{
				if(instance == null){
					GameObject soundManager = new GameObject("Sound Manager");
					
					instance = soundManager.AddComponent<SoundEngine>();
					
					AudioSource src = soundManager.AddComponent<AudioSource>();
					src.bypassEffects = true;
					src.priority = 0;
					src.dopplerLevel = 0;
					
					GameObject themeAudioGO = new GameObject("Theme audio");
					themeAudioGO.transform.parent = soundManager.transform;
					
					themeAudio  = themeAudioGO.AddComponent<AudioSource>().audio;
					themeAudio.priority = 255;
					themeAudio.dopplerLevel = 0;
					themeAudio.bypassEffects = true;
					themeAudio.volume = themeVolume;
					themeAudio.ignoreListenerPause = true;
					GameObject.DontDestroyOnLoad(soundManager);
					
				}
				return instance;
			}
		}
		
		public void ClearListeners(){
		
			OnAudioWordEventInvoker = null;
			OnAudioCompleteEventInvoker = null;
		}
		
		public void StopAll()
		{
			bool themeIsPlaying = themeAudio.isPlaying;
			// Setting AudioListener.pause keeps the sounds from playing.  We were experiencing sounds continue to play after this was called on a cancellation
			AudioListener.pause = true;

			if (themeIsPlaying) {
				themeAudio.Play();
			}
			audio.Stop();
			//Clear clip so that it doesn't accidentally get re-played later
			audio.clip = null;
			SendCancelToken();
		}
		
		public void SendCancelToken()
		{
			cancelToken.Cancel();
			//cancelToken = new CancelToken();
		}
		
		private float pauseTime;
		public void SendPauseToken()
		{
			pauseTime = Time.time;
			
			if( audio != null )
			{
				pauseToken.time = audio.time;			
				audio.Pause();
			}
			pauseToken.Pause();
		}
		
		public void SendUnPauseToken()
		{
			if( audio != null )
			{
				audio.PlayDelayed( pauseToken.time );
			}
			
			runtime += Time.time - pauseTime;
			pauseTime = 0;
			pauseToken.UnPause();
		}
	
		/// <summary>
		/// Sound bundle. - Stores list of clips to play in order and events to dispatch REALTIVE to the clip index
		/// 
		/// SOundBundle.AddClip(word1);
		/// SoundBundle.AddCLip(word2_door, "DOOR", 1.5f);
		/// SoundBundle.AddCLip(word3);
		/// 
		/// </summary>
		public class SoundBundle{
			
			public class ClipInfo{
			
				public AudioClip clip;
				public float eventDispatchDelay = -1;
				public string eventToDispatch;
				public bool theme = false;
				
				public ClipInfo(){}
				
				public ClipInfo(AudioClip clip)
				{
					this.clip = clip;	
				}
				
				public ClipInfo(AudioClip clip, float delay, string eventToDispatch)
				{				
					this.clip = clip;
					this.eventDispatchDelay = delay;
					this.eventToDispatch = eventToDispatch;
				}
				
				public ClipInfo(AudioClip clip, float delay, string eventToDispatch, bool theme)
				{				
					this.clip = clip;
					this.eventDispatchDelay = delay;
					this.eventToDispatch = eventToDispatch;
					this.theme = theme;
				}
			}
			
			public List<ClipInfo> clips = new List<ClipInfo>();

			
			public void AddClip(AudioClip clipToPlayNext)
			{
				ClipInfo newClip = new ClipInfo(clipToPlayNext);
				clips.Add(newClip);
			}
			
			public void AddClip(AudioClip clipToPlayNext, string eventName, float delayFromClipStart)
			{
				AddClip(clipToPlayNext,eventName, delayFromClipStart, false);
			}
			
			public void AddClip(AudioClip clipToPlayNext, string eventName, float delayFromClipStart, bool loop)
			{
				ClipInfo newClip = new ClipInfo(clipToPlayNext, delayFromClipStart, eventName);
				clips.Add(newClip);
			}
			
		}
		
		public void PlayTheme(AudioClip clip)
		{
			if( clip == null )
			{
				themeAudio.Stop();
				return;
			}
				
			AudioListener.pause = false;
			themeAudio.loop = true;
			themeAudio.clip = clip;
			themeAudio.Play();
		}
		
		public void PlayBundle(SoundBundle bundle)
		{
			// Since multiple sound bundles can no longer correctly play at the same time,
			// cancel earlier one.
			SendCancelToken();
			cancelToken = new CancelToken();
			AudioListener.pause = false;
			StartCoroutine(PlaySoundBundle(bundle, cancelToken, pauseToken));
		}
		
		public void PlayEffect(AudioClip clip)
		{
			AudioListener.pause = false;
			audio.PlayOneShot(clip);
		}
		
	
		
		void Update(){
		
			float tVol =  audio.isPlaying ? themeDuckedVolume : themeVolume;
			themeAudio.volume -= (themeAudio.volume-tVol) * 0.15f;
		}
		
		private float runtime;
		private IEnumerator PlaySoundBundle(SoundBundle bundle, CancelToken token, PauseToken pauseToken)
		{		
			float inc = .01f;
			
			foreach(var clip in bundle.clips)
			{
				if(token.Cancelled){ yield break; }
				while(pauseToken.Paused) yield return new WaitForSeconds(inc);
				if(token.Cancelled){  yield break; }
				
				Debug.Log("Playing clip: " + clip.clip + " " + clip.eventToDispatch);
				audio.clip = clip.clip;
				audio.Play();
				
				if(clip.eventToDispatch != null) 
				{
					runtime = Time.time;

					while(!token.Cancelled)
					{
						if(pauseToken.Paused )
						{
							yield return new WaitForSeconds(inc);
							continue;
						}
						
						float totalTime = Time.time - runtime;
						
						if( totalTime < clip.eventDispatchDelay ) yield return new WaitForSeconds(inc);
						else if( totalTime >= clip.eventDispatchDelay )
						{
							if(OnAudioWordEventInvoker != null) 
							{
								OnAudioWordEventInvoker(clip.eventToDispatch);
							}
							break;
						}
					}
					
					if( token.Cancelled ){ yield break; }
					
					yield return new WaitForSeconds(clip.clip.length - clip.eventDispatchDelay);
				}
				else
				{
					yield return new WaitForSeconds(clip.clip.length);
				}				

				if(OnAudioCompleteEventInvoker != null) {
					OnAudioCompleteEventInvoker();
				}
			}
			
		}
		
		private class CancelToken
		{
			private bool cancelled;
			public bool Cancelled { get { return cancelled; } }
			public void Cancel() 
			{
				cancelled = true;
			}
		}
		
		private class PauseToken
		{
			public float time;
			private bool paused = false;
			public bool Paused { get { return paused; } }
			public void Pause() 
			{
				paused = true;
			}
			public void UnPause()
			{
				paused = false;
			}
		}
		
	}
}

