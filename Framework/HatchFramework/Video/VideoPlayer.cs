using System;
using System.Collections;
using UnityEngine;

namespace HatchFramework
{
	
	public delegate void OnVideoComplete();
	
	public class VideoPlayer : MonoBehaviour
	{
		OnVideoComplete OnVideoCompleteInvoker;
		public event OnVideoComplete OnVideoCompleteEvent{ add{OnVideoCompleteInvoker += value;} remove{OnVideoCompleteInvoker -= value;} }
		
		public string IntroPath{ get{ return ""; } }
		
		private static VideoPlayer instance;
		public static VideoPlayer Instance
		{
			get
			{
				if(instance == null){
					GameObject videoplayer = new GameObject("Video Player");
					GameObject.DontDestroyOnLoad(videoplayer);
					instance = videoplayer.AddComponent<VideoPlayer>();
					
				}
				return instance;
			}
		}
		
		private bool _isPlaying = false;
		public bool isPlaying{ get{ return _isPlaying; } }
		
		private bool _isLoaded = false;
		public bool isLoaded{ get{ return _isLoaded; } }
		
		public void PlayVideo( string path )
		{
			#if UNITY_ANDROID || UNITY_IPHONE
			StartCoroutine( PlayMobileVideo() );
			#else
			SoundEngine.Instance.OnAudioWordEvent += onAudioWord;
			//StartCoroutine( loadVideo(path) );
			movieTexture = (MovieTexture) Resources.Load( "splashScreen", typeof( MovieTexture ) );
			
			_isLoaded = true;
			#endif
		}
		
		private IEnumerator PlayMobileVideo()
		{
			Debug.Log("PLAY MOBILE VIDEO" );
			yield return Handheld.PlayFullScreenMovie ("splashScreen.mp4", Color.white, FullScreenMovieControlMode.CancelOnInput);
			Debug.Log("VIDEO DONE");
			if(OnVideoCompleteInvoker != null) OnVideoCompleteInvoker();
		}
				
#if !UNITY_ANDROID && !UNITY_IPHONE
		private WWW videoDownload;
		private IEnumerator loadVideo( string path )
		{
			Debug.Log("playing video");
			videoDownload = new WWW( path );
			yield return videoDownload;
			//movieTexture = videoDownload.movie;  <-- uncomment when video is uploaded
			
			movieTexture = (MovieTexture) Resources.Load( "splashScreen", typeof( MovieTexture ) );
			
			_isLoaded = true;
			
		}
		
		private MovieTexture movieTexture;
		void OnGUI()
		{
			//if(!_isLoaded) Debug.Log("video download progress: " + videoDownload.progress);
			if(_isLoaded && movieTexture != null)
			{
				GUI.DrawTexture( new Rect( 0, 0, Screen.width, Screen.height), movieTexture, ScaleMode.ScaleToFit );
				if(Screen.showCursor) Screen.showCursor = false;
				if(!Screen.fullScreen) Screen.fullScreen = true;
				if(!movieTexture.isPlaying)
				{
					movieTexture.Play();
					SoundEngine.SoundBundle videoSound = new SoundEngine.SoundBundle();
					videoSound.AddClip( movieTexture.audioClip, "introAudio", 0 );
					SoundEngine.Instance.PlayBundle( videoSound );
					waitForVideoFinish();
					_isPlaying = true;
				}
				
				if(movieTexture.isPlaying && Input.GetMouseButtonUp(0))
				{
					onComplete();
				}
				
			}			
		}
		
		private void waitForVideoFinish()
		{
			StartCoroutine( wait(movieTexture.duration) );
			//onComplete();
		}
		
		private IEnumerator wait(float duration)
		{
			yield return new WaitForSeconds( duration );
			onComplete();
		}
		
#endif
		
		private void onComplete()
		{
#if !UNITY_ANDROID && !UNITY_IPHONE
			movieTexture.Stop();
			Screen.showCursor = true;
			Screen.fullScreen = false;
#endif
			
			_isPlaying = false;
			_isLoaded = false;
			
			//movieTexture = null;
			if(OnVideoCompleteInvoker != null) OnVideoCompleteInvoker();
		}
		
		private void onAudioWord( string word )
		{
			
		}
		
		
	}
	
}