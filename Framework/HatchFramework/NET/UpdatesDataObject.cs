using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UpdatesDataObject
{
	public System.Version applicationVersion = new System.Version();
	public bool forcedApplicationUpdate = false;
	
	public Dictionary<string, string> bundleList; // bundleName, version
	
	public UpdatesDataObject()
	{
		
	}
}

