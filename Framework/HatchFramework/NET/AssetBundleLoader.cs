using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using pumpkin.swf;
using System.IO;
/// <summary>
/// Asset bundle loader.  Responsible for managing the loading and extraction of bundle assets
/// </summary>
namespace HatchFramework
{
	public delegate void OnAssetBundleLoadProgress(float percentage);
	public delegate void OnAssetBundleLoadComplete(AssetBundle bundle, string bundleName, string assetPath);
	
	public class AssetBundleLoader : MonoBehaviour
	{
		OnAssetBundleLoadProgress OnAssetBundleLoadProgressInvoker;
		public event OnAssetBundleLoadProgress OnAssetBundleLoadProgressEvent{ add{OnAssetBundleLoadProgressInvoker += value;} remove{OnAssetBundleLoadProgressInvoker -= value;} }
		
		OnAssetBundleLoadComplete OnAssetBundleLoadCompleteInvoker;
		public event OnAssetBundleLoadComplete OnAssetBundleLoadCompleteEvent{ add{OnAssetBundleLoadCompleteInvoker += value;} remove{OnAssetBundleLoadCompleteInvoker -= value;} }
		
		public string id = "GlobalAssets";
		public AssetBundle bundle;
		public int version;
		public string versionString;
		
		public float progress = 0;
		
		public bool isDownloadingBundle = false;
		public bool isDownloaded = false;
		public bool failed = false;
		
		private WWW www;
		private string wwwPath = "";
		private string localPath;
		
		public void Awake()
		{
			wwwPath = GlobalConfig.GetProperty<string>("ASSET_BUNDLE_URL");
			localPath = AssetLoader.Instance.loadAssetBundlesLocally == true ? "file://" + PlatformUtils.GetBundlePathForProject() : "";
			
		}
		
		private int GetVersionFromString(string version)
		{
			versionString = version.Replace('.', '_');
			return int.Parse(string.Join("", version.Split(new char[]{'_','.'})));//new comment
		}
		
		public void UnloadAssetBundle()
		{
			// DO NOT PUT THIS NEXT LINE AFTER THE bundle.Unload(true) CALL - CAUSES AN ERROR WITH UNISWF IN THAT IT CAN'T FIND THE BUNDLE LATER FOR SOME REASON - JOHNG
			UnloadAssetBundleFromUniswf();
			
			if( bundle != null )
			{				
				bundle.Unload(true);				
			}
	
			bundle = null;
		}
		
		public void UnloadAssetBundleFromUniswf()
		{
			if( bundle != null )
			{				
				BuiltinResourceLoader loader = pumpkin.display.MovieClip.rootResourceLoader as BuiltinResourceLoader;
				DebugConsole.LogWarning("UNLOAD BUNDLE FROM UNISWF: " + bundle.ToString() + ", does it contain bundle? " + loader.bundles.Contains(bundle));

				loader.removeAssetBundle(bundle);
				loader.bundles.RemoveAll(x => x == bundle);		
				//DebugConsole.LogWarning("UNLOAD BUNDLE FROM UNISWF: " + bundle.ToString() + ", does it contain bundle AFTERWARDS? " + loader.bundles.Contains(bundle));
				
				UniswfTextureMonitor.clearTextureCache();

			}
		}
		
		/// <summary>
		/// Gets the asset bundle from cache.  This only works if it's been cached
		/// </summary>
		/// <returns>
		/// The asset bundle from cache.
		/// </returns>
		/// <param name='fullBundleName'>
		/// Full bundle name.
		/// </param>
		public AssetBundle GetAssetBundleFromCache(string fullBundleName)
		{
			if( bundle != null ) return bundle;
			
			DebugConsole.LogError("BUNDLE "+fullBundleName+ " MUST BE CACHED PRIOR TO LOAD");
			return null;
		}
		
		//private bool isPreloadingBundle = false;
		public IEnumerator PreloadBundle(string fullBundleName, System.Action<AssetBundle> result)
		{			
			//if( isPreloadingBundle ) yield return null;
			//isPreloadingBundle = true;
			
			if( bundle != null )
			{
				if(result != null) result(bundle);				
			}
			else
			{			
				string pathToSWF = (AssetLoader.Instance.loadAssetBundlesLocally ? localPath : wwwPath).Replace( '\\', '/' );
				if( pathToSWF.Substring( pathToSWF.Length - 2 ).IndexOf('/') < 0 )
					pathToSWF += "/";
				pathToSWF += fullBundleName + ".unity3d";
				string internalPath = Application.persistentDataPath + "/" + fullBundleName;
				if( !fullBundleName.Contains( versionString ) )
					internalPath += "_" + versionString;
				internalPath += ".unity3d";
				
				if( File.Exists( internalPath ) )
				{
					DebugConsole.Log( "Found bundle internally at " + internalPath + " - Loading internal asset" );
					pathToSWF = "file://" + internalPath;
				}
				else
				{
					if( pathToSWF.ToLower().IndexOf("android/data") > -1 )
						pathToSWF = internalPath.Substring(0, internalPath.IndexOf("ndroid/data")) + pathToSWF.Substring( pathToSWF.ToLower().IndexOf("ndroid/data") );
					
					if( pathToSWF.IndexOf("file://") < 0 && pathToSWF.IndexOf("http://") < 0 && pathToSWF.IndexOf("https://") < 0 )
						pathToSWF = "file://" + pathToSWF;
				}
				DebugConsole.Log("CACHING BUNDLE: " + fullBundleName + ", pathToSWF: " + pathToSWF + ", version: " + version);
				
				www = WWW.LoadFromCacheOrDownload(pathToSWF, version);
			
				yield return www;
				if( !string.IsNullOrEmpty(www.error) ) DebugConsole.LogError("WWW ERROR ON CACHE LOAD: (" + pathToSWF + ")" + www.error);
				
				bundle = www.assetBundle;		
				if(result != null) result(bundle);
				www.Dispose();
				www = null;
			}			
			
			//isPreloadingBundle = false;
		}

		
		public void LoadAssetBundle(string fullBundleName, string bundleName, string version, string assetPath)
		{			
			StartCoroutine( DoLoadAssetBundle(fullBundleName, bundleName, GetVersionFromString(version), assetPath) );
		}
		
		private IEnumerator DoLoadAssetBundle(string fullBundleName, string bundleName, int version, string assetPath)
		{			
			id = bundleName;
			this.version = version;
			isDownloadingBundle = true;
			isDownloaded = false;
			
			string pathToSWF = (AssetLoader.Instance.loadAssetBundlesLocally ? localPath : wwwPath).Replace( '\\', '/' );
			if( pathToSWF.Substring( pathToSWF.Length - 2 ).IndexOf('/') < 0 )
				pathToSWF += "/";
			pathToSWF += fullBundleName + ".unity3d";
			string internalPath = Application.persistentDataPath + "/" + fullBundleName;
			if( !fullBundleName.Contains( versionString ) )
				internalPath += "_" + versionString;
			internalPath += ".unity3d";
			
			if( File.Exists( internalPath ) )
			{
				DebugConsole.Log( "Found bundle internally at " + internalPath + " - Loading internal asset" );
				pathToSWF = "file://" + internalPath;
			}
			else
			{
				if( pathToSWF.ToLower().IndexOf("android/data") > -1 )
					pathToSWF = internalPath.Substring(0, internalPath.IndexOf("ndroid/data") + 1) + pathToSWF.Substring( pathToSWF.ToLower().IndexOf("ndroid/data") );
				
				if( pathToSWF.IndexOf("file://") < 0 && pathToSWF.IndexOf("http://") < 0 && pathToSWF.IndexOf("https://") < 0 )
					pathToSWF = "file://" + pathToSWF;
			}
			
			if(Caching.IsVersionCached(pathToSWF, version))
			{
				Caching.MarkAsUsed(pathToSWF, version);
				isDownloaded = true;
				isDownloadingBundle = false;
				progress = 1;
				if( OnAssetBundleLoadCompleteInvoker != null ) OnAssetBundleLoadCompleteInvoker(bundle, bundleName, assetPath);
				yield break;
			}
			
			www = WWW.LoadFromCacheOrDownload(pathToSWF, version);
			
			StartCoroutine( DoProgressCheck() );
			
			float dlProgress = -1.0f;
			float timeHung = -1f;
				
			yield return new WaitForSeconds(0.1f);
			
			while( !www.isDone )
			{
				if( dlProgress == www.progress || ( www.progress == 0.0f && dlProgress == www.uploadProgress ))
				{
					if( timeHung < 0 )
					{
						timeHung = Time.realtimeSinceStartup;
						DebugConsole.LogWarning( "Possible Download Hang at: " + timeHung + " seconds since startup" );
					}
					
					if( Time.realtimeSinceStartup - timeHung >= 300 )
					{
						timeHung = -1f;
						DebugConsole.LogWarning( "Asset Download Hanging - Re-attempting after: " + (Time.realtimeSinceStartup - timeHung) + " seconds" );
						
						www.Dispose();
						www = WWW.LoadFromCacheOrDownload(pathToSWF, version);
					}
				}
				else if( timeHung > 0 )
				{
					DebugConsole.LogWarning( "Download hang resolved after: " + (Time.realtimeSinceStartup - timeHung) + " seconds" );
					timeHung = -1f;
				}
				
				dlProgress = www.progress;
				if( dlProgress == 0.0f )
					dlProgress = www.uploadProgress;
				
				yield return new WaitForSeconds(0.2f);
			}
			
			if( !string.IsNullOrEmpty(www.error) )
			{
				DebugConsole.LogError("WWW ERROR WHILE LOADING "+ pathToSWF + " : " + www.error);
				isDownloadingBundle = false;
				failed = true;
			}
			else
			{
				isDownloadingBundle = false;
				isDownloaded = true;
				
				DebugConsole.Log("ASSETS LOADED FROM " + pathToSWF + " : " + id.ToString());
				if( OnAssetBundleLoadCompleteInvoker != null ) OnAssetBundleLoadCompleteInvoker(bundle, bundleName, assetPath);
			}
			
			www.Dispose();
			www = null;
		}
		
		private IEnumerator DoProgressCheck()
		{
			while( isDownloadingBundle && (www != null && !www.isDone))
			{	
				if( OnAssetBundleLoadProgressInvoker != null ) OnAssetBundleLoadProgressInvoker(www.progress);
				progress = www.progress;
				
				if( www.progress == 1 ) break;
				yield return new WaitForSeconds(.025f);
			}	
			
			progress = 1;
			//isDownloaded = true;
			
			// we pass this so that it gets the final "1" as in full load 
			if( OnAssetBundleLoadProgressInvoker != null ) OnAssetBundleLoadProgressInvoker(1f);
		}
	}
}
