using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using pumpkin;
using pumpkin.display;
using pumpkin.text;
using pumpkin.ui;
using pumpkin.swf;
using pumpkin.events;

namespace HatchFramework
{
	public delegate void OnRequestedMovieClipLoaded(MovieClip clip, string assetPath);
	public delegate void OnTotalProgressUpdate(float percentLoaded);
	public delegate void OnInitialAssetBundlesLoaded(bool success);
	
	public class AssetLoader : MonoBehaviour
	{
		public static string LoadLanguageFlag = "";
		
		private static AssetLoader instance;
		public static AssetLoader Instance
		{
			get
			{
				return instance;
			}
	
			set
			{
				instance = value;
			}
		}
		
		OnTotalProgressUpdate OnTotalProgressUpdateInvoker;
		public event OnTotalProgressUpdate OnTotalProgressUpdateEvent{ add{OnTotalProgressUpdateInvoker += value;} remove{OnTotalProgressUpdateInvoker -= value;} }
		
		OnInitialAssetBundlesLoaded OnInitialAssetBundlesLoadedInvoker;
		public event OnInitialAssetBundlesLoaded OnInitialAssetBundlesLoadedEvent{ add{OnInitialAssetBundlesLoadedInvoker += value;} remove{OnInitialAssetBundlesLoadedInvoker -= value;} }
		
		public bool loadAssetBundlesLocally = false;

		//public Platforms editorPlatform = Platforms.WIN;
		
		private Dictionary<string, AssetBundleLoader> loadedBundels;
		private Dictionary<string, string> bundleVersions;
	
		public void Awake()
		{
			instance = this;
			loadedBundels = new Dictionary<string, AssetBundleLoader>();

			loadAssetBundlesLocally = !GlobalConfig.GetProperty<bool>("loadAssetsFromRMS");
			
			if(GlobalConfig.GetProperty<bool>("clearAssetCache"))
				Caching.CleanCache();
			
			DebugConsole.Instance.RegisterBehaviour(this);
		}
		
		/// <summary>
		/// Gets the asset bundle.  Since the assetsbundles are unloaded after the inital upgrademanager fires, we have to get it out of cache / load it and then pass it back
		/// </summary>
		/// <returns>
		/// The asset bundle.
		/// </returns>
		/// <param name='bundleName'>
		/// Bundle name.
		/// </param>
		public AssetBundle GetAssetBundle(string bundleName)
		{
			if( !string.IsNullOrEmpty( LoadLanguageFlag ) && SkillImplementationUtil.IsSkillSpanish(bundleName) && !bundleName.Contains( LoadLanguageFlag ) )
				bundleName = LoadLanguageFlag + bundleName;
			
			if( loadedBundels.ContainsKey(bundleName) )
			{
				if( loadedBundels[bundleName].isDownloaded ) 
				{
					AssetBundleLoader abl = loadedBundels[bundleName];
					return abl.GetAssetBundleFromCache(GetBundlePath(abl.id));
				}
				else DebugConsole.LogError("BUNDLE NOT DOWNLOADED");
			}
			else DebugConsole.LogError("BUNDLE NOT CACHED: " + bundleName);
			
			return null;
		}
		
		public IEnumerator PreloadAssetBundle(string bundleName, System.Action<AssetBundle> result)
		{
			if( !string.IsNullOrEmpty( LoadLanguageFlag ) && SkillImplementationUtil.IsSkillSpanish(bundleName) && !bundleName.Contains( LoadLanguageFlag ) )
				bundleName = LoadLanguageFlag + bundleName;
			
			DebugConsole.Log ("Preloading bundle: "+bundleName);
			if( loadedBundels.ContainsKey(bundleName) )
			{
				
				if( loadedBundels[bundleName].isDownloaded ) 
				{
					AssetBundleLoader abl = loadedBundels[bundleName];
					
					yield return StartCoroutine(abl.PreloadBundle(GetBundlePath(abl.id), result));
					
				}else{
					DebugConsole.LogError("Asset bundle not downloaded: "+bundleName);	
				}
			}
			else DebugConsole.Log("BUNDLE NOT CACHED: " + bundleName);
			
			
			yield return null;
			
		}
		
		
		private void UnloadAssetBundles()
		{
			foreach(KeyValuePair<string, AssetBundleLoader> pair in loadedBundels)
			{
				pair.Value.UnloadAssetBundle();
			}
		}
		
		/// <summary>
		/// Loads the asset bundle list.  Called from UpgradeManager
		/// </summary>
		/// <param name='bundleList'>
		/// Bundle list.
		/// </param>
		public void LoadAssetBundleList(Dictionary<string, string> bundleList)
		{
			bundleVersions = bundleList;
			
			//revise this if multiple calls are expected to be additive
			loadedBundels.Clear();
			
			foreach(KeyValuePair<string, string> pair in bundleVersions)
			{
				CreateAssetBundle( pair.Key );
				DebugConsole.Log("ASSET BUNDLE TO LOAD: " + pair.Key);
			}
			
			StartCoroutine( MonitorLoaderDownloads() );
		}
		
		private IEnumerator MonitorLoaderDownloads()
		{
			
			int bundleCount = loadedBundels.Count;
			int loadedCount = 0;
			float totalProgress = 0;
			float splice = 1 / (float)bundleCount;
			bool failed = false;
			
			DebugConsole.Log("ASSET LOADER: bundleCount: " + bundleCount + ", splicePercentage: " + splice);
			
			AssetBundleLoader currentAbl = GetNextNotLoaded();
			
			
			
			if( currentAbl == null )
			{
				DebugConsole.LogError("THERE ARE NO ASSETS TO BE LOADED");
				yield break;
			}
			else
			{	
				DebugConsole.Log("NEXT ABL, LOADEDCOUNT: " + loadedCount + ", is the next one null? " + (currentAbl == null) + (currentAbl != null ? ", assetBundle: " + currentAbl.id : ""));
				
				while( loadedCount < bundleCount && currentAbl != null )
				{
					string fullBundleName = GetBundlePath(currentAbl.id);
					currentAbl.LoadAssetBundle(fullBundleName, currentAbl.id, bundleVersions[currentAbl.id.ToString()], "");
					
					while( currentAbl.isDownloadingBundle )
					{
						totalProgress = ((float)loadedCount * splice) + (currentAbl.progress * splice);
						//DebugConsole.Log("PROGRESS: asset: " + currentAbl.progress + ", total; " + totalProgress);
						
						if( OnTotalProgressUpdateInvoker != null ) OnTotalProgressUpdateInvoker(totalProgress);
						yield return new WaitForSeconds(.025f);
					}
					currentAbl.isDownloaded = true;
					failed |= currentAbl.failed;
					//currentAbl.UnloadAssetBundle();
					currentAbl = GetNextNotLoaded();
					loadedCount ++;
					
					totalProgress = ((float)loadedCount * splice);
					if( OnTotalProgressUpdateInvoker != null ) OnTotalProgressUpdateInvoker(totalProgress);
					
					DebugConsole.Log("NEXT ABL, LOADEDCOUNT: " + loadedCount + ", is the next one null? " + (currentAbl == null) + (currentAbl != null ? ", assetBundle: " + currentAbl.id : ""));
					
					if( currentAbl == null ) break;
				}
				
				
				
				// we're fully loaded at this point
				if( OnInitialAssetBundlesLoadedInvoker != null ) {
					OnInitialAssetBundlesLoadedInvoker(!failed);
				}
			}
		}
		
		private AssetBundleLoader GetNextNotLoaded()
		{
			foreach(KeyValuePair<string, AssetBundleLoader> pair in loadedBundels)
			{
				if( !pair.Value.isDownloaded ) return pair.Value;
			}
			
			return null;
		}
		
		public void CreateAssetBundle(string bundleName)
		{
			// create AssetBundleLoader and adds to the loadedBundles list for monitoring
			AssetBundleLoader abl = gameObject.AddComponent<AssetBundleLoader>();
			abl.id = bundleName;
			
			loadedBundels.Add(bundleName, abl);
		}
		
		/*public AssetBundleLoader LoadAssetBundle(string bundleName)
		{
			return LoadAssetBundle(bundleName, ""); 
		}
		
		public AssetBundleLoader LoadAssetBundle(string bundleName, string assetPath)
		{
			// create AssetBundleLoader and load
			string fullBundleName = GetBundlePath(bundleName);
			AssetBundleLoader abl = gameObject.AddComponent<AssetBundleLoader>();
			abl.LoadAssetBundle(fullBundleName, bundleName, bundleVersions[bundleName], assetPath);
			
			loadedBundels.Add(bundleName, abl);
			
			return abl;
		}*/
		
		private string GetBundlePath(string bundleName)
		{
			if(GlobalConfig.GetProperty<bool>("useLocalAssetConfig")){
				string plat = PlatformUtils.GetPlatform().ToString();
				if( Application.isEditor )
				{
	#if UNITY_IPHONE
					plat = Platforms.IOS.ToString();
	#elif UNITY_ANDROID
					plat = Platforms.ANDROID.ToString();
	#else
					plat = Platforms.WIN.ToString();
	#endif
				}
				return plat + "_" + bundleName + "_" + bundleVersions[bundleName];
			}else{
				return bundleName;	
			}
		}
		
		public AudioClip GetAudioClip(string bundleName, string assetPath)
		{
			if( !string.IsNullOrEmpty( LoadLanguageFlag ) && SkillImplementationUtil.IsSkillSpanish(bundleName) && !bundleName.Contains( LoadLanguageFlag ) )
				bundleName = LoadLanguageFlag + bundleName;
			
			AudioClip clip = null;
			
			var bundle = GetAssetBundle(bundleName);
			if(bundle != null) {
				clip = (AudioClip)bundle.Load(assetPath, typeof(AudioClip));
			}
			else
				DebugConsole.LogError("Failed to get: " + assetPath + "; from: " + bundleName + "; bundle is null" );
			
			return clip;
		}
		
		public Texture2D GetTexture(string bundleName, string assetPath)
		{
			return null;
		}
		
		[DebugConsoleMethod("unload_bundle", "unload asset bundle with name")]
		public void UnloadBundle(string bundleName)
		{
			if( !string.IsNullOrEmpty( LoadLanguageFlag ) && SkillImplementationUtil.IsSkillSpanish(bundleName) && !bundleName.Contains( LoadLanguageFlag ) )
				bundleName = LoadLanguageFlag + bundleName;
			
			foreach(KeyValuePair<string, AssetBundleLoader> pair in loadedBundels)
			{
				if( pair.Value != null && pair.Key == bundleName ) 
				{					
					pair.Value.UnloadAssetBundle();
					break;
				}
			}
		}
		
		public void UnloadBundleFromUniswf(string bundleName)
		{
			if( !string.IsNullOrEmpty( LoadLanguageFlag ) && SkillImplementationUtil.IsSkillSpanish(bundleName) && !bundleName.Contains( LoadLanguageFlag ) )
				bundleName = LoadLanguageFlag + bundleName;
			
			foreach(KeyValuePair<string, AssetBundleLoader> pair in loadedBundels)
			{
				if( pair.Key == bundleName ) 
				{					
					pair.Value.UnloadAssetBundleFromUniswf();
					break;
				}
			}
		}
		
		[DebugConsoleMethod("list_bundle", "list all asset bundles loaded")]
		public void Debug_ListBundles()
		{
			string ret = "";
			
			foreach(KeyValuePair<string, AssetBundleLoader> pair in loadedBundels)
			{
				ret += pair.Key+"\n";
			}
			DebugConsole.Log(DebugConsole.LogLevel.DEBUG, "", ret);
		}
		
		public Dictionary<string, string> GetBundleConfig() //get bundle names and version #s
		{
			return null;
		}
	}
}