using UnityEngine;
using System.Collections;

using pumpkin;
using pumpkin.display;
using pumpkin.events;


namespace HatchFramework
{

	public class ScrollView : DisplayObjectContainer
	{
		
		protected DisplayObjectContainer content_mc;
		
		protected DisplayObjectContainer container_mc;
		
		protected Rect _mask;
		
		private bool touchDown;
		private float speed;
		private float lastMouseY;
				
		/// <summary>
		/// Initializes a new instance of the <see cref="ScrollView"/> class.
		/// </summary>
		/// <param name='content'>
		/// Content to be scrollable. The mask is created based on this MovieClip's size
		/// </param>
		public ScrollView( DisplayObjectContainer content ) : base( )
		{
			content_mc = content;
			_mask = new Rect(content.x, content.y, content.width, content.height);
			init();
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ScrollView"/> class.
		/// </summary>
		/// <param name='content'>
		/// Content to be scrollable.
		/// </param>
		/// <param name='mask'>
		/// Mask rect.
		/// </param>
		public ScrollView( DisplayObjectContainer content, Rect mask ) : base(  )
		{
			content_mc = content;
			this._mask = mask;
			init();
		}
			
		private void init()
		{
			
			container_mc = new DisplayObjectContainer();
			
			addChild( container_mc );
			container_mc.addChild( content_mc );
								
			container_mc.addEventListener( MouseEvent.MOUSE_DOWN, onListDown );
			
			container_mc.clipRect = _mask;
			container_mc.addEventListener(CEvent.ADDED_TO_STAGE, OnAddedToStage);
			container_mc.addEventListener(CEvent.REMOVED_FROM_STAGE, OnRemovedFromStage);
			
		}
		
		public bool UseMomentum
		{
			get; set;
		}
		
		private void update( CEvent e )
		{
			if(touchDown) {
				var dy = mouseY - mouseDistance;
				float containerPos = Mathf.Clamp(dy, _mask.height-content_mc.height, 0);
				if(content_mc.y != containerPos) {
					content_mc.y = containerPos;
					this.dispatchEvent(new CEvent(CEvent.CHANGED));
				}
				speed = (mouseY - lastMouseY) / Time.deltaTime;
				lastMouseY = mouseY;
			} else {
				speed *= Mathf.Max(0, 1 - 3*Time.deltaTime);
				float offset = speed * Time.deltaTime;
				float containerPos = Mathf.Clamp(content_mc.y+offset, _mask.height-content_mc.height, 0);
				if(content_mc.y != containerPos) {
					content_mc.y = containerPos;
					this.dispatchEvent(new CEvent(CEvent.CHANGED));
				}
				if(Mathf.Abs(speed) < 10f) {
					removeEventListener( CEvent.ENTER_FRAME, update );
				}
			}
			
		}
		
		private float mouseDistance;
		private void onListDown( CEvent e )
		{
			mouseDistance = mouseY - content_mc.y;
			lastMouseY = mouseY;
			touchDown = true;
			removeEventListener( CEvent.ENTER_FRAME, update );
			addEventListener( CEvent.ENTER_FRAME, update );
		}
		
		private void onListUp( CEvent e )
		{
			touchDown = false;
			if(!UseMomentum) {
				removeEventListener( CEvent.ENTER_FRAME, update );
			}
		}
		
		public void SetScrollValue( float val )
		{
			float containerPos =  (_mask.height-content_mc.height) * val;
			if(content_mc.y != containerPos) {
				content_mc.y = containerPos;
				this.dispatchEvent(new CEvent(CEvent.CHANGED));
			}
		}
		
		public float ScrollValue
		{
			get
			{
				return content_mc.y / (_mask.height-content_mc.height);
			}
		}

		void OnAddedToStage ( CEvent e )
		{
			stage.addEventListener( MouseEvent.MOUSE_UP, onListUp );
		}

		void OnRemovedFromStage ( CEvent e )
		{
			stage.removeEventListener( MouseEvent.MOUSE_UP, onListUp );
		}
		
	}
	
}

