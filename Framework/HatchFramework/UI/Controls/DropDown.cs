using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.text;

namespace HatchFramework
{
	
	public class DropDown<T> : MovieClip
	{
		
		private string swfName;
		private string listItemName;
		private int maxViewableItems;
		private IList<T> items;
		private T selectedItem;
		private string defaultText;
		private float itemHeight;
		
		private List<DropDownListItem<T>> item_mcs;
		
		private DisplayObjectContainer contentContainer;
		private MovieClip base_mc;
		private TextField label_txt;
		
		public T SelectedItem
		{
			get{ 
				return selectedItem; 
			}
			
			set
			{
				Open = false;
				selectedItem = value;
				if(value != null) {
					label_txt.text = value.ToString();
				}
				else
				{
					label_txt.text = DefaultText;
				}
					
				dispatchEvent( new CEvent(CEvent.CHANGED) );
			}
			
		}
		
		public string DefaultText
		{
			get { return defaultText; }
			set
			{
				defaultText = value;
				if(selectedItem == null)
				{
					label_txt.text = defaultText;
				}
			}
		}
		
		private ScrollView scrollView;
		
		private bool _isOpen = false;
		public bool Open
		{
			get{ return _isOpen; }
			
			set
			{
				if(_isOpen != value) {
					_isOpen = value;
					UpdateListItems();
				}
			}
		}
		
		public DropDown(string swf, string symbolname, string listItemName, float itemHeight) : base( swf, symbolname )
		{
			swfName = swf;
			this.listItemName = listItemName;
			this.itemHeight = itemHeight;
			init();
		}
	
		public void SetInfo(int maxViewableItems, IList<T> items)
		{
			this.items = new List<T>(items);
			this.maxViewableItems = maxViewableItems;
			SetupList();
		}
		
		public void SetInfo(int maxViewableItems, params T[] items)
		{
			SetInfo(maxViewableItems, new List<T>(items));
		}
		
		private void init()
		{
			item_mcs = new List<DropDownListItem<T>>();
			contentContainer = new DisplayObjectContainer();
			base_mc = getChildByName<MovieClip>( "base" );
			label_txt = getChildByName<TextField>( "label_txt" );
			label_txt.mouseEnabled = false;

			// Because UniSWF can remove transparent portions, and the draw rects are used
			// for hit detection, add transparent draw rect here.
			if(base_mc.numChildren > 0) {
				var child = base_mc.getChildAt(0);
				base_mc.graphics.drawSolidRectangle(Color.clear, 0, 0, base_mc.width * 1.05f, child.y * 1.5f);
			}
		}
		
		private void SetupList()
		{
			
			base_mc.removeEventListener( MouseEvent.MOUSE_DOWN, OnClick );
			
			item_mcs.Clear();
			contentContainer = new DisplayObjectContainer();
			
			if(items.Count == 0)
			{
				return;
			}
			
			float lastY = 0;
			foreach(var item in items)
			{
				var newItem = new DropDownListItem<T>( swfName, listItemName, this );
				newItem.Item = item;
				newItem.y = lastY;
				lastY += itemHeight;
				item_mcs.Add( newItem );
				contentContainer.addChild(newItem);
			}
			
			
			var lastItem = item_mcs[item_mcs.Count-1];
			float listHeight = (items.Count <= maxViewableItems) ? items.Count : (maxViewableItems+0.5f);
			Rect listMask = new Rect( 0, 0, lastItem.width, itemHeight * listHeight );
			
			contentContainer.y = 0;
			contentContainer.height = lastY;
			
			if(scrollView != null)
			{
				removeChild(scrollView);
				scrollView = null;
			}
			scrollView = new ScrollView( contentContainer, listMask );
			scrollView.visible = Open;
			scrollView.y = this.height;
			scrollView.x = 13f;
			if(Open) addChild( scrollView );
			
			base_mc.addEventListener( MouseEvent.MOUSE_DOWN, OnClick );
		}
		
		private float mouseDownTime;
		private void OnMouseDown( CEvent e )
		{
			mouseDownTime = Time.time;
		}
		
		private void OnMouseUp( CEvent e )
		{
			mouseDownTime = Time.time - mouseDownTime;
			Open = !Open;
		}
		
		private void OnClick( CEvent e )
		{
			Open = !Open;
		}
		
		private void UpdateListItems()
		{
			if(scrollView == null) return;
			scrollView.visible = _isOpen;
			if(_isOpen) {
				addChild(scrollView);
			} else {
				removeChild(scrollView);
			}
		}
		
	}
	
} 
