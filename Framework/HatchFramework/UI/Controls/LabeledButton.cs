using UnityEngine;
using System.Collections;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.text;

namespace HatchFramework
{
	public class LabeledButton : BasicButton
	{
		
		private string labelName = "lbl_txt";
		
		private TextField lbl_txt;
		public string Label
		{
		
			get{ return lbl_txt.text; }
			
			set
			{
				lbl_txt.text = value;
			}
			
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="LabeledButton"/> class.
		/// </summary>
		/// <param name='linkage'>
		/// Full swf and Symbolname string.
		/// </param>
		public LabeledButton( string linkage ) : base( linkage )
		{
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="LabeledButton"/> class.
		/// </summary>
		/// <param name='swf'>
		/// Swf.
		/// </param>
		/// <param name='symbolname'>
		/// Symbolname.
		/// </param>
		public LabeledButton( string swf, string symbolname ) : base( swf, symbolname )
		{
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="LabeledButton"/> class.
		/// </summary>
		/// <param name='swf'>
		/// Swf.
		/// </param>
		/// <param name='symbolname'>
		/// Symbolname.
		/// </param>
		/// <param name='_labelName'>
		/// _labelname override.
		/// </param>
		public LabeledButton( string swf, string symbolname, string _labelName ) : base( swf, symbolname )
		{
			labelName = _labelName;
		}
		
		public LabeledButton(MovieClip root, string _labelName) : base(root)
		{
			labelName = _labelName;
		}
		
		protected override void init()
		{
			
			base.init();
			
			lbl_txt = getChildByName<TextField>( labelName );
			
		}
		
	}
}
