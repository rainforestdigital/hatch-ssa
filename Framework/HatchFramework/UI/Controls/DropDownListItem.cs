using UnityEngine;
using System.Collections;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.text;

namespace HatchFramework
{
	public class DropDownListItem<T> : BasicButton
	{
		
		private DropDown<T> _parent;
		
		private TextField label_txt;
		private T item;
		
		public T Item
		{
			get{ return item; }
			
			set
			{
				label_txt.text = value.ToString();
				item = value;
			}
			
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="DropDownListItem"/> class.
		/// </summary>
		/// <param name='linkage'>
		/// Linkage.
		/// </param>
		/// <param name='_parent'>
		/// parent <see cref="DropDown"/> object that this list item is attached to.
		/// </param>
		public DropDownListItem( string linkage, DropDown<T> _parent  ) : base( linkage )
		{
			root = this;
			this._parent = _parent;
			init();
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="DropDownListItem"/> class.
		/// </summary>
		/// <param name='swf'>
		/// Swf.
		/// </param>
		/// <param name='symbolName'>
		/// Symbol name.
		/// </param>
		/// <param name='_parent'>
		///parent <see cref="DropDown"/> object that this list item is attached to.
		/// </param>
		public DropDownListItem( string swf, string symbolName, DropDown<T> _parent  ) : base( swf, symbolName )
		{
			root = this;
			this._parent = _parent;
			init();
		}
		
		protected override void init()
		{
			
			base.init();
			
			label_txt = getChildByName<TextField>( "label_txt" );
			
		}
		
		private float mouseYPosition;
		
		protected override void onMouseDown( CEvent e )
		{
			mouseYPosition = stage.mouseY;
		}
		
		protected override void onMouseUp( CEvent e )
		{		
			float mathDistance = Mathf.Abs(stage.mouseY - mouseYPosition);
			if(	mathDistance <= this.height/2f)
				_parent.SelectedItem = Item;
		}
		
		
	}
}
