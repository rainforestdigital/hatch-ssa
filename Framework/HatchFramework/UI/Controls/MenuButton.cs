using UnityEngine;
using System.Collections;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.text;

namespace HatchFramework
{
	public class MenuButton : BasicButton
	{
		
		
		private TextField lbl_txt;
		public string Label
		{
			get{ return lbl_txt.text; }
			
			set
			{
				lbl_txt.text = value;
			}
			
		}
		
		private TextField sub_txt;
		public string SubText
		{
			get{ return sub_txt.text; }
			
			set
			{
				sub_txt.text = value;
			}
			
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="MenuButton"/> class.
		/// </summary>
		/// <param name='linkage'>
		/// Full swf and Symbolname string.
		/// </param>
		public MenuButton( string linkage ) : base( linkage )
		{
			root = this;
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="MenuButton"/> class.
		/// </summary>
		/// <param name='swf'>
		/// Swf.
		/// </param>
		/// <param name='symbolname'>
		/// Symbolname.
		/// </param>
		public MenuButton( string swf, string symbolname ) : base( swf, symbolname )
		{
			root = this;
		}
		
		protected override void init()
		{
			base.init();
			
			lbl_txt = getChildByName<TextField>( "lbl_txt" );
			if(getChildByName<TextField>( "sub_txt" ) != null) sub_txt = getChildByName<TextField>( "sub_txt" );
			
			lbl_txt.text = "";
			if(sub_txt != null) sub_txt.text = "";
			
		}
		
	}
}

