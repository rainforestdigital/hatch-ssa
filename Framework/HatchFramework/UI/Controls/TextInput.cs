using UnityEngine;
using System.Collections;
using System;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.text;

namespace HatchFramework
{
	public class TextInput
	{ 
		public Action<bool> OnHasFocus;
		
		private TextField textField;

		public string Text
		{ 
			get{ return textField.text; }
			set
			{
				textField.text = value;
			}
		}
		
		private bool hasFocus = false;
		public bool HasFocus
		{
			get
			{
				return hasFocus;
			}
			
			set
			{
				hasFocus = value;
				if( OnHasFocus != null ) OnHasFocus(value); 
			}
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="HatchFramework.TextInput"/> class.
		/// Used to activate mobile platform's native keyboard when activating input fields.
		/// </summary>
		/// <param name='tf'>
		/// The UniSWF <see cref="TextField"/> input object to use for the keyboard input.
		/// </param>
		/// <param name='type'>
		/// TouchScreenKeyboardType.
		/// </param>
		/// <param name='keyboard'>
		/// TouchScreenKeyboard object.
		/// </param>
#if UNITY_IPHONE || UNITY_ANDROID
		
		private TouchScreenKeyboardType keyboardType;
		private TouchScreenKeyboard keyboard;
#endif
		
		private void onFocusIn( CEvent e )
		{
			
			if(HasFocus) return;
			
			HasFocus = true;
			
			if(!Application.isEditor){
#if UNITY_IPHONE || UNITY_ANDROID
			keyboard = TouchScreenKeyboard.Open( textField.text, keyboardType );
			keyboard.text = textField.text;
			textField.addEventListener( CEvent.ENTER_FRAME, update );
#endif
			}
		}
		
		
		public TextInput( TextField tf ) : base()
		{
			textField = tf;

			if(!Application.isEditor){
#if UNITY_IPHONE || UNITY_ANDROID
			keyboardType = TouchScreenKeyboardType.Default;
#endif
			}
			init();
	
		}
		
		
		
		private void init()
		{
			textField.addEventListener( FocusEvent.FOCUS_IN, onFocusIn );
			textField.addEventListener( FocusEvent.FOCUS_OUT, onFocusOut );
		}


		/// <summary>
		/// Event Handler for when textfield gains focus. 
		/// If the textfield doesn't already have focus, it checks the current application platform,
		/// and sets the keyboard ONLY if it is on Android or iOS.
		/// </summary>
		
		/// <summary>
		/// Event Handler for when textfield loses focus.
		/// </summary>
		private void onFocusOut( CEvent e )
		{
			HasFocus = false;
			
			Debug.Log("Focus Out");
#if UNITY_IPHONE || UNITY_ANDROID
			textField.removeEventListener( CEvent.ENTER_FRAME, update );
			keyboard = null;
#endif			
			
		}
		
		/// <summary>
		/// Updates the textfield to the keyboard's text.
		/// </summary>
		private void update( CEvent e )
		{
#if UNITY_IPHONE || UNITY_ANDROID
			if( keyboard != null) {
				if( !keyboard.active ) {
			
					// Set final value
					textField.text = keyboard.text;
			
					// Clear keyboard
					keyboard = null;
			
					// Clear focus
					textField.stage.focus = null;
			
				} else {
			        if( keyboard.text != textField.text ) {
			            textField.text = keyboard.text;
			        }
				}
			}
#endif
        }
    }
}
