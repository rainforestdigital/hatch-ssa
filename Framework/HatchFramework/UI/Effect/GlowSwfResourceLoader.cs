using UnityEngine;
using System.Collections;
using pumpkin.swf;
using pumpkin.display;

public class CustomShaderSwfResourceLoader : ISwfResourceLoader {
	
	
	static Material m_Material = null;
	protected Material material {
		get {
			if (m_Material == null) {
				Shader blurShader = (Shader)Resources.Load ("Shaders/BlurEffectConeTaps", typeof(Shader));
				m_Material = new Material(blurShader);
				m_Material.hideFlags = HideFlags.DontSave;
			}
			return m_Material;
		} 
	}
	
	
	Shader baseBitmapShader;
	/// Blur iterations - larger number means more blur.
	public int iterations = 9;
	
	/// Blur spread for each iteration. Lower values
	/// give better looking blur, but require more iterations to
	/// get large blurs. Value is usually between 0.5 and 1.0.
	public float blurSpread = 0.1f;
	
	public SwfAssetContext loadSWF( string swfUri ) {
		
		// Use root loader
		SwfAssetContext context = MovieClipPlayer.rootResourceLoader.loadSWF( swfUri );
		
		// Override materials
		context.resourceLoader = this;
		return context;
		
	}
			
	public Material getMaterial( MovieClipPlayer player, SwfAssetContext assetContext, DisplayObjectInfo dispInfo, BitmapAssetInfo bmpInfo ) {
		string uri = assetContext.swfPrefix + bmpInfo.textureUri;
		
		// Get bitmap from texture manager					
		Texture tex = (Texture)Resources.Load( uri );
		if( tex == null ) {
			Debug.Log("Failed to load texture: " + uri);
			return null;
		}
		
		if( baseBitmapShader == null ) {
			baseBitmapShader = (Shader)Resources.Load("Shaders/ColorTint", typeof(Shader));
		}
		
		Material mat = new Material( baseBitmapShader );
		mat.color = new Color(1,0,0,1f);
		mat.name = uri;
		tex.name = uri;
		mat.mainTexture = tex;		
		return mat;
	}
	
	public virtual Material getCharMaterial( BitmapTextField txt, SwfAssetContext assetContext, BitmapFontAssetInfo fontInfo, BitmapFontCharInfo charInfo, BitmapAssetInfo bmpInfo ) {
		string uri = assetContext.swfPrefix + bmpInfo.textureUri;
		
		// Get bitmap from texture manager					
		Texture tex = (Texture)Resources.Load( uri );
		if( tex == null ) {
			Debug.Log("Failed to load texture: " + uri);
			return null;
		}
		
		if( baseBitmapShader == null ) {
			baseBitmapShader = (Shader)Resources.Load("Shaders/uniSWF-Double-Alpha-Diffuse-Inverted", typeof(Shader));
		}
		
		float scale = 1.25f;
		float w = (float)tex.width;
		float h = (float)tex.height;
		RenderTexture rt = new RenderTexture((int)(w*scale), (int)(h*scale), 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Default);
		CreateBlur(tex, rt);
		
		Material mat = new Material( baseBitmapShader );
		mat.color = new Color(1,0,0,1f);
		mat.name = uri;
		tex.name = uri;
		mat.mainTexture = rt;		
		return mat;		
	}
	
	// Performs one blur iteration.
	public void FourTapCone (RenderTexture source, RenderTexture dest, int iteration)
	{
		float off = 0.5f + iteration*blurSpread;
		UnityEngine.Graphics.BlitMultiTap (source, dest, material,
			new Vector2(-off, -off),
			new Vector2(-off,  off),
			new Vector2( off,  off),
			new Vector2( off, -off)
		);
	}
	
	// Downsamples the texture to a quarter resolution.
	private void DownSample4x (Texture source, RenderTexture dest)
	{
		float off = 1.0f;
		UnityEngine.Graphics.BlitMultiTap (source, dest, material,
			new Vector2(-off, -off),
			new Vector2(-off,  off),
			new Vector2( off,  off),
			new Vector2( off, -off)
		);
	}
	
	void CreateBlur (Texture source, RenderTexture destination) {		
		RenderTexture buffer = RenderTexture.GetTemporary(source.width/4, source.height/4, 0);
		RenderTexture buffer2 = RenderTexture.GetTemporary(source.width/4, source.height/4, 0);
		
		// Copy source to the 4x4 smaller texture.
		DownSample4x (source, buffer);
		
		// Blur the small texture
		bool oddEven = true;
		for(int i = 0; i < iterations; i++)
		{
			if( oddEven )
				FourTapCone (buffer, buffer2, i);
			else
				FourTapCone (buffer2, buffer, i);
			oddEven = !oddEven;
		}
		if( oddEven )
			UnityEngine.Graphics.Blit(buffer, destination);
		else
			UnityEngine.Graphics.Blit(buffer2, destination);
		
		RenderTexture.ReleaseTemporary(buffer);
		RenderTexture.ReleaseTemporary(buffer2);
	}
	
	public bool unloadSWF( string swfUri ) {	
		return false;
	}	
}

public class CustomSwfResourceLoaderTest : MonoBehaviour {

	
	void Start () {
	
		Stage stage = GetComponent<MovieClipOverlayCameraBehaviour>().stage;		
		
		
		MovieClip mc3 = new MovieClip(new BuiltinResourceLoader(), "uniSWF/common/swf/common_uniswf_overlay.swf:TEXT" );
		mc3.x = 400;
		mc3.y = 200;
		stage.addChild( mc3);
		
		CustomShaderSwfResourceLoader customLoader = new CustomShaderSwfResourceLoader();		
		MovieClip mc = new MovieClip( customLoader, "uniSWF/common/swf/common_uniswf_overlay.swf:TEXT" );
		mc.x = 200;
		mc.y = 200;
		stage.addChild( mc );
		
	}
	
	
	void Update () {
	
	}
}
