using System;
using UnityEngine;
using pumpkin;
using pumpkin.display;

namespace HatchFramework
{
	public class GlowMovieClipFilter : MovieClipFilter
	{
		Material m_Material = null;

		protected Material material {
			get {
				if (m_Material == null) {
					m_Material = new Material ((Shader)Resources.Load ("Shaders/BlurEffectConeTaps", typeof(Shader)));
					m_Material.hideFlags = HideFlags.DontSave;
				}
				return m_Material;
			} 
		}
		
		Material c_Material = null;

		protected Material cmaterial {
			get {
				if (c_Material == null) {
					Shader centr = (Shader)Resources.Load ("Shaders/CenterTexture", typeof(Shader));
					c_Material = new Material (centr);//centerShader);
					c_Material.hideFlags = HideFlags.DontSave;
				}
				return c_Material;
			} 
		}
		
		Material t_Material = null;

		protected Material tmaterial {
			get {
				if (t_Material == null) {
					Shader centr = (Shader)Resources.Load ("Shaders/ColorTint", typeof(Shader));
					t_Material = new Material (centr);//centerShader);
					t_Material.hideFlags = HideFlags.DontSave;
					t_Material.color = this.color;
				}
				return t_Material;
			} 
		}
		
		//Number of steps to spread
		public int iterations = 1;
		
		//Amount to blur (lower == better quality, but needs more iterations to spread out)
		public float blurSpread = 0.1f;
		public Color color;
		public float drawScale = 1;
		
		public GlowMovieClipFilter (int border, int iterations, float spread, Color color):base(border)
		{
			this.iterations = iterations;
			this.blurSpread = spread;
			this.color = color;
		}
		
		public GlowMovieClipFilter (int border, int iterations, float spread, Color color, float drawScale):base(border)
		{
			this.iterations = iterations;
			this.blurSpread = spread;
			this.color = color;
			this.drawScale = drawScale;
		}
		
		public override void ApplyEffect ()
		{
			float w = drawOp.drawWidth;
			float h = drawOp.drawHeight;
			float scale = (border + w) / w;
			bool redraw = output == null;
			if (redraw) {				
				//int s = Mathf.Max(, );
				output = new RenderTexture ((int)(w * scale), (int)(h * scale), 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Default);
				output.name = "glow";
			}
			
			RenderTexture trans = RenderTexture.GetTemporary(output.width, output.height, 0);
			
			//set the rect to use for the atlas
			cmaterial.SetVector ("drawRect", new Vector4 (drawOp.srcRect.x, 1 - (drawOp.srcRect.y + drawOp.srcRect.height), drawOp.srcRect.width, drawOp.srcRect.height));
			cmaterial.SetFloat ("inputScale", 1f / scale);
			Vector4 offset = new Vector4 ();
			offset.x = (scale - 1) * 0.5f;
			offset.y = (scale - 1) * 0.5f;
			cmaterial.SetVector ("offsetPosition", offset);
			
			
			UnityEngine.Graphics.Blit (input, trans, cmaterial);
			
			CreateBlur (trans, trans);
			
			UnityEngine.Graphics.Blit (trans, output, tmaterial);
			
			float ty = (output.height - drawOp.drawHeight) * -0.5f;
			float drawOffset = 0.5f * (1-drawScale);
			if (redraw) {
				outputMaterial = new Material( Shader.Find("Sprites/Default") );
				outputMaterial.mainTexture = output;
				
				GetSprite ().graphics.clear ();
				GetSprite ().graphics.drawRectUV (outputMaterial, new Rect (0, 0, 1, 1), new Rect (
					(-border * 0.5f + drawOp.x)*drawScale + output.width*drawOffset,
					ty*drawScale + output.height*drawOffset,
					output.width*drawScale,
					output.height*drawScale));
			}
			dirty = false;
			RenderTexture.ReleaseTemporary(trans);
			RenderTexture.active = null;
		}
		
		// Performs one blur iteration.
		public void FourTapCone (RenderTexture source, RenderTexture dest, int iteration)
		{
			float off = 0.5f + iteration * blurSpread;
			//Debug.Log ("offset: "+off);
			UnityEngine.Graphics.BlitMultiTap (source, dest, material,
												new Vector2 (-off, -off),
												new Vector2 (-off, off),
												new Vector2 (off, off),
												new Vector2 (off, -off)
											);
		}
	
		// Downsamples the texture to a quarter resolution.
		private void DownSample4x (Texture source, RenderTexture dest)
		{
			float off = 1.0f;
			UnityEngine.Graphics.BlitMultiTap (source, dest, material,
												new Vector2 (-off, -off),
												new Vector2 (-off, off),
												new Vector2 (off, off),
												new Vector2 (off, -off)
											);
		}
	
		//Run the blur loop onto the destination
		void CreateBlur (Texture source, RenderTexture destination)
		{		
			RenderTexture buffer = RenderTexture.GetTemporary (source.width / 4, source.height / 4, 0);
			RenderTexture buffer2 = RenderTexture.GetTemporary (source.width / 4, source.height / 4, 0);
		
			// Copy source to the 4x4 smaller texture.
			DownSample4x (source, buffer);
		//Debug.Log ("BLURRING "+iterations+" "+blurSpread);
			// Blur the small texture
			bool oddEven = true;
			for (int i = 0; i < iterations; i++) {
				if (oddEven)
					FourTapCone (buffer, buffer2, i);
				else
					FourTapCone (buffer2, buffer, i);
				oddEven = !oddEven;
			}
			if (oddEven)
				UnityEngine.Graphics.Blit (buffer, destination);
			else
				UnityEngine.Graphics.Blit (buffer2, destination);
		
			RenderTexture.ReleaseTemporary (buffer);
			RenderTexture.ReleaseTemporary (buffer2);
		}
		
		public override void Destroy ()
		{
			base.Destroy ();
			
			if( m_Material != null )
				UnityEngine.Object.Destroy(m_Material);
			if( c_Material != null )
				UnityEngine.Object.Destroy(c_Material);
			if( t_Material != null )
				UnityEngine.Object.Destroy(t_Material);
		}
		
	}
	
	
}

