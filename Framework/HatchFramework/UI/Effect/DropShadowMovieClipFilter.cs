using System;
using UnityEngine;

namespace HatchFramework
{
	public class DropShadowMovieClipFilter : GlowMovieClipFilter
	{
		protected int offset;
		public DropShadowMovieClipFilter(int border, int iterations, float spread, Color color, int offset):base(border, iterations, spread, color){
			this.offset = offset;
		}
		public override void SetTexture (pumpkin.display.Sprite sprite)
		{
			base.SetTexture (sprite);
			GetSprite().x += offset;
			GetSprite().y += offset;
		}
	}
}

