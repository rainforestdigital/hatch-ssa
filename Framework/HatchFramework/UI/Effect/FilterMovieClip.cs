using System;
using pumpkin.display;
using pumpkin;
using UnityEngine;
using System.Collections.Generic;
using pumpkin.geom;

namespace HatchFramework
{
	public class FilterMovieClip : DisplayObjectContainer
	{
		protected bool destroyed;
		protected MovieClip target;
		public MovieClip Target
		{
			get{ return target; }
		}
		public List<MovieClipFilter> filters; 
		
		public FilterMovieClip (MovieClip targetMovieClip):base()
		{
			this.target = targetMovieClip;
			filters = new List<MovieClipFilter>();
			addChild(target);
		}
		
		~FilterMovieClip() {
			if(!destroyed) {
				Debug.LogWarning("Potential Leak - FilterMovieClip was not destroyed");
			}
		}
		
		public void ApplyAllFilters(){
			
			foreach(MovieClipFilter filter in filters){
				filter.ApplyEffect();
				
			}
		}
		
		public virtual void Destroy() {
			destroyed = true;
			while(filters.Count > 0)
			{
				RemoveFilter( filters[0] );
			}
		}
		
		public virtual void AddFilter(MovieClipFilter filter){
			AddFilter( filter, findSpriteWithDrawOp() );
		}
		public virtual void AddFilter(MovieClipFilter filter, Sprite sprite){
		
			filters.Add(filter);
			
			filter.SetTexture(sprite);
			addChildAt(filter.GetSprite(), 0);
			filter.ApplyEffect();
			
			Matrix matrix = sprite.getFullMatrix();
			matrix.multNoCopy(this.getFullMatrix().invert());
			filter.GetSprite().setMatrixOverride(matrix);
			destroyed = false;
		}

		public Rectangle getUnfilteredBounds(DisplayObject coordinateSpace) 
		{
			return target.getBounds (coordinateSpace);
		}

		private Sprite findSpriteWithDrawOp() {
			if(target.graphics.drawOPs.Count > 0) {
				return target;
			}
			
			return findSpriteWithDrawOp(target);
		}
		
		private Sprite findSpriteWithDrawOp(DisplayObjectContainer clip) {
			for(int i = 0; i < clip.numChildren; ++i) {
				var child = clip.getChildAt(i);
				
				var sprite = child as Sprite;
				if(sprite !=  null && sprite.graphics.drawOPs.Count > 0) {
					return sprite;
				}
				
				var container = child as DisplayObjectContainer;
				if(container != null) {
					var result = findSpriteWithDrawOp(container);
					if(result != null) {
						return result;
					}
				}
			}
			return null;
		}
		
		public virtual void RemoveFilter(MovieClipFilter filter){
			if(contains(filter.GetSprite()))
				removeChild(filter.GetSprite());
			filter.Visible = false;
			filter.Destroy();
			filters.Remove(filter);
		}
		
	}
	
	public abstract class MovieClipFilter{
	
		protected bool dirty = false;
		protected RenderTexture output;
		protected Material outputMaterial;
		protected Texture input;
		protected GraphicsDrawOP drawOp;
		protected Sprite inputSprite;
		protected Sprite sprite;
		protected int border = 0;
		
		public MovieClipFilter( int border){
	
			this.border = border;
			this.dirty =true;
		}
		
		public virtual void SetTexture(Sprite sprite){
			this.inputSprite = sprite;
			this.drawOp = sprite.graphics.getDrawOPAt(0);
			GetSprite().setMatrix(sprite.getMatrix());
			this.input = drawOp.material.mainTexture;	
		
		}
		
		public virtual void ApplyEffect(){
		
			sprite = GetSprite();
			sprite.graphics.clear();
	
		}
		
		public virtual Sprite GetSprite(){
			
			if(sprite == null){
				sprite = new Sprite();	
			}
			
			return sprite;
			
		}
		
		public virtual void SetBorder(int border){
			this.border = border;
		}
		
		public bool Visible{
		
			get{
				return GetSprite().visible;
			}
			set{
				GetSprite ().visible = value;
			}
		}
		
		public virtual void Destroy() {
			GetSprite().graphics.clear();
			
			if( output != null )
				UnityEngine.Object.Destroy(output);
			if( outputMaterial != null )
				UnityEngine.Object.Destroy(outputMaterial);
		}
		
		
	}
}

