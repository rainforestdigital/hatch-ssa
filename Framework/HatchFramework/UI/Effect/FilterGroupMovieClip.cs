using System;
using pumpkin.display;
using pumpkin;
using UnityEngine;
using System.Collections.Generic;
using pumpkin.geom;

namespace HatchFramework
{	
	public class FilterGroupMovieClip : FilterMovieClip
	{
		public delegate MovieClipFilter filterConstructer();
		public List<int> FilterGroups;
		
		public FilterGroupMovieClip( MovieClip targetMovieClip ) : base(targetMovieClip)
		{
			FilterGroups = new List<int>();
		}
		
		public override void AddFilter (MovieClipFilter filter)
		{
			base.AddFilter (filter);
			FilterGroups.Add(1);
			DebugConsole.LogWarning( "You just passed a MovieClipFilter to FilterGroupMovieClip : Did you mean to use a filterConstructor?" );
		}
		
		public void AddFilterSimple (MovieClipFilter filter)
		{
			base.AddFilter (filter);
			FilterGroups.Add(1);
		}
		
		public void AddFilter (filterConstructer filter)
		{
			int start = base.filters.Count;
			
			addFilterRecursive( base.target, filter );
			
			for( int i = 0; i< base.filters.Count; i++ )
			{
				addChildAt( base.filters[i].GetSprite(), i );
			}
			
			FilterGroups.Add( base.filters.Count - start );
		}
		
		private void addFilterRecursive ( Sprite targ, filterConstructer filter )
		{
			for( int i = 0; i < targ.numChildren; i++ )
			{
				Sprite child = targ.getChildAt<Sprite>( i );
				if( child != null )
					addFilterRecursive( child, filter );
			}
			
			if( targ.graphics.drawOPs.Count > 0 )
			{
				AddFilter( filter(), targ );
			}
		}
		
		public override void RemoveFilter (MovieClipFilter filter)
		{
			int i = -1, min = 0, max = 0, index = base.filters.IndexOf( filter );
			
			foreach( int count in FilterGroups )
			{
				i++;
				min = max;
				max += count;
				
				if( max >= index )
					break;
			}
			
			FilterGroups.RemoveAt( i );
			
			for( i = max - 1; i >= min; i-- )
			{
				if( i < base.filters.Count )
					base.RemoveFilter( base.filters[i] );
			}
		}
		
		public override void Destroy ()
		{
			base.Destroy ();
			FilterGroups.Clear();
		}
	}
}
