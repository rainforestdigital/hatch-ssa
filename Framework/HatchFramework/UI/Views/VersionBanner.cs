using System;
using UnityEngine;

namespace HatchFramework
{
	public class VersionBanner : MonoBehaviour
	{
		public string versionOutput = "";
		protected GUIStyle style;
		
		protected float timeout = 0;
		protected float lastClick = -1000;
		protected Rect labelRect = new Rect(Screen.width-110, Screen.height-70, 100, 60);
		public void OnGUI(){
		
			if(DebugConsole.Instance.enabled){
				if(style == null){
					style = new GUIStyle(GUI.skin.label);
					style.alignment = TextAnchor.LowerRight;
				}

				if (GUI.Button (labelRect, versionOutput, style)) {

					if (Time.time - lastClick < 0.5f) {
						//double tap!!!!
						DebugConsole.Log ("GOT A DOUBLE CLICK");
						PlatformUtils.CaptureScreenshot ();
						lastClick = 0;
					} else {

						lastClick = Time.time;
					}

				}

				if(Input.GetMouseButton(0) && labelRect.Contains(Event.current.mousePosition)){
					timeout += Time.deltaTime * 2;

				}else{
					timeout = 0;	
				}
				
				if(timeout > 3){
					DebugConsole.Instance.ToggleVisible();
					timeout = 0;
				}
			}
		}

		public void Update(){

			if (!DebugConsole.Instance.enabled)
				return;


		}
	}
}

