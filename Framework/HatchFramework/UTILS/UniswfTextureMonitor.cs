using System;
using UnityEngine;
using pumpkin;
using pumpkin.swf;
using System.Collections.Generic;
using System.Collections;
using System.Linq;


namespace HatchFramework
{
	public class UniswfTextureMonitor : MonoBehaviour
	{
		
		public List<Texture> textures;
		
		public static UniswfTextureMonitor instance { get; private set;}
		void Awake(){
			instance = this;
		}
		
		
		public static void clearTextureCache(){
			
			instance.ReleaseMemory();
			
		}
		 
		
		public void ReleaseMemory(){
			StartCoroutine(ReleaseMemoryAsync());
		}
		
		protected IEnumerator ReleaseMemoryAsync(){
			
			System.GC.Collect();
			
			yield return new WaitForSeconds( 0.015f );
			
			TextureManager.instance.textures.Clear();
			
			yield return new WaitForSeconds( 0.01f );
			
			TextureManager.instance.materials.Clear();
			
			yield return new WaitForSeconds( 0.01f );

			//textures = TextureManager.instance.textures.Values.ToListUtil();
			yield return Resources.UnloadUnusedAssets();
			
			yield return new WaitForSeconds( 0.02f );
			
			System.GC.Collect();
			
			yield return new WaitForSeconds( 0.015f );
		}
	} 
}
