using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Reflection;
using HatchFramework;

[AttributeUsage(AttributeTargets.Method)]
public class DebugConsoleMethodAttribute : System.Attribute{
	public string Command{get; set;}
	public string HelpText{get;set;}

	public DebugConsoleMethodAttribute(string command, string helpText){
		this.Command = command;
		this.HelpText = helpText;
	}
}


public class DebugConsole : MonoBehaviour
{
	public enum LogLevel{DEBUG, WARNING, ERROR, SYSTEM, NONE}
	
	public static Queue<DebugOutput> outputs = new Queue<DebugOutput>();
	static Vector2 scrollPos = Vector2.zero;
	
	
	bool isShowing = false;
	string lastCommand = null;
	string command = "";
	float logTop = 0;
	int displayLogLevel = 0;
	
	public static bool forwardToUnity = true;
	
	protected List<DebugCommand> debugCommands;
	
	protected bool isMobile = false;
	GUIStyle popupBox;
	
	public static DebugConsole Instance{get; protected set;}
	
	
	void Awake(){
		if(Instance != null){
			DestroyImmediate(this.gameObject);
			return;
		}else{
			Instance = this;
			DontDestroyOnLoad(this.gameObject);
		}
		
		logTop  = -Screen.height/2;
		
		debugCommands = new List<DebugCommand>();
		
#if UNITY_IPHONE || UNITY_ANDROID
		isMobile = true;
#endif
		if(Application.platform != RuntimePlatform.Android && Application.platform != RuntimePlatform.IPhonePlayer){
			isMobile = false;	
		}
		
		Application.RegisterLogCallback(HandleUnityLog);
		
	}
	
	
	void Start(){
		
		CollectMethods();	
	}
	
	protected int lastLevelLoaded = -1;
	void OnLevelWasLoaded (int i) {
   	 
		if(Instance == this){
		
   	 		DebugConsole.LogSystem("Rebuilding command set for level {0}", i);
			Reset ();
		}
   	 
	}
	
	public void ProcessCommand(){
		//find the command that relates!
		DebugCommand dc = debugCommands.FirstOrDefault(x=>command.IndexOf(x.command) == 0);
		LogSystem(">"+command);
		lastCommand = command;
		if(dc == null){
			LogError("****INVALID COMMAND****");	
		}else{
		
			Debug.Log (dc.allMethods.Count);
			foreach(KeyValuePair<object, MethodInfo> method in dc.allMethods){
				
				DebugConsole.DebugCommandProps props = new DebugConsole.DebugCommandProps(dc, command);
				string []stringParams = props.Params;
				ParameterInfo[] parms = method.Value.GetParameters();
				object[] passValues = new object[parms.Length];
				
				if(parms.Length != stringParams.Length){
					
					DebugConsole.Log("", "Invalid number of parameters!  Found "+stringParams.Length+" but expects "+parms.Length);
				}
				
				for(int i=0;i<parms.Length;i++){
					TypeCode typeCode = Type.GetTypeCode(parms[i].ParameterType);
					switch(typeCode){
						case TypeCode.Boolean:	
							bool b = false;
							if(!bool.TryParse(stringParams[i], out b)){
								DebugConsole.LogError("Unable to parse boolean type for : "+stringParams[i]);
							}
							passValues[i] = b;
							break;
						case TypeCode.Int32:	
							int k = 0;
							if(!int.TryParse(stringParams[i], out k)){
								DebugConsole.LogError("Unable to parse int type for : "+stringParams[i]);
							}
							passValues[i] = k;
							break;
						case TypeCode.String:
							passValues[i] = stringParams[i];
							break;
						case TypeCode.Single:
							float f = 0;
							if(!float.TryParse(stringParams[i], out f)){
								DebugConsole.LogError("Unable to parse float type for: "+stringParams[i]);
							}
							passValues[i] = f;
							break;
						default:
							DebugConsole.LogError("Doesn't support this type - talk to andy: "+parms[i].ParameterType);
							break;
					}
					
				}
				
				Debug.Log ("Should be invoking");
				method.Value.Invoke(method.Key, passValues);	
			}
			
		}
		
		command = "";
	}
	
	#region Collection and Registration
	
	void Reset(){

		debugCommands.Clear();
		CollectMethods();
	}
	
	void CollectMethods(){
	
		
		MonoBehaviour[] bees = GameObject.FindObjectsOfType(typeof(MonoBehaviour)) as MonoBehaviour[];
	
			foreach(MonoBehaviour b in bees){
			
				RegisterBehaviour(b);
				
			}
		
	}
	
	public void RegisterBehaviour(System.Object b){
		MethodInfo[] methods = b.GetType().GetMethods();
		foreach(MethodInfo method in methods){
			
			DebugConsoleMethodAttribute attr = System.Attribute.GetCustomAttribute(method,
					typeof(DebugConsoleMethodAttribute)) as DebugConsoleMethodAttribute;
			if(attr == null)
				continue;
	
			
			DebugCommand dbg = debugCommands.FirstOrDefault(x=>x.command == attr.Command);	
			debugCommands.Remove(dbg);
			
			DebugCommand cmd = new DebugCommand();
			cmd.help = attr.HelpText;
			cmd.command = attr.Command;
			debugCommands.Add(cmd);
						
			cmd.allMethods.Add(new KeyValuePair<object, MethodInfo>(b, method));
			
		}
	}
	
	#endregion
	
	
	public void ToggleVisible(){
		
		if(!enabled && !isShowing)
			return;
		
		isShowing = !isShowing;	
		iTween.ValueTo(this.gameObject, iTween.Hash("from", logTop, "to", isShowing ? 0 : -Screen.height/2, "onupdate", "LogTopUpdate", "time", 0.25f));
		
		if(isShowing)
			GUI.FocusControl("commandText");
		Event.current.Use();
		command = "";
		
	}
	
	#region Display
	
#if UNITY_IPHONE || UNITY_ANDROID
		DebugTextEntry currentEntry = new DebugTextEntry();
#endif 
	
	void MoveCursorToEnd ()
	{
		TextEditor editor = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);
		if(editor != null) {
			editor.pos = command.Length;
			editor.selectPos = command.Length;
		}
	}
	
	void OnGUI(){
		
		if(logTop <= -Screen.height/2){
			return;	
		}
		
		
		GUI.depth  = -100000;
		
#if UNITY_IPHONE || UNITY_ANDROID
		if(isMobile){
			bool cancelled;
			if(currentEntry.Closed(out cancelled)){
				
				command = currentEntry.text;	
				if(!cancelled){
					ProcessCommand();
					command = currentEntry.text = "";
				}
				
			}else{
				command = currentEntry.text;	
			}
		}
#endif 
		
		if (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.Return && GUI.GetNameOfFocusedControl() == "commandText"){
			ProcessCommand();	
			Event.current.Use();
		}
		
		if(Event.current.type == EventType.KeyUp && Event.current.keyCode == KeyCode.BackQuote){
			ToggleVisible();
			return;
		}
		
		if (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.UpArrow && GUI.GetNameOfFocusedControl() == "commandText"){
			if(lastCommand != null) {
				command = lastCommand;
				MoveCursorToEnd ();
				Event.current.Use();
			}
		}
		
		if (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.Tab && GUI.GetNameOfFocusedControl() == "commandText"){
			var completionCandidates = debugCommands.Select(x => x.command)
				.Where(x => x.Length >= command.Length && x.StartsWith(command)).ToList();
			var prefix = CommonPrefix(completionCandidates);
			if(prefix.Length > command.Length) {
				command = prefix;
				if(completionCandidates.Count == 1) {
					command += " ";
				};
				MoveCursorToEnd ();
			}
		}
		
		
		
		GUI.Box(new Rect(0, logTop, Screen.width, Screen.height/2), "");
		GUI.Box(new Rect(0, logTop, Screen.width, Screen.height/2), "");
		GUILayout.BeginArea(new Rect(0, logTop, Screen.width, Screen.height/2));
		scrollPos = GUILayout.BeginScrollView(scrollPos, GUILayout.Height(Screen.height/2f-30));
			foreach(DebugOutput d in outputs.Where(x=>(int)x.logLevel >= displayLogLevel)){
				GUILayout.BeginHorizontal();
					GUI.color = d.OutputColor;
					if(d.header != "")
						GUILayout.Label(d.header, GUILayout.Width(140));
					GUILayout.Label(d.output);
					GUI.color = Color.white;
				GUILayout.EndHorizontal();
			}
		GUILayout.EndScrollView();
		GUI.SetNextControlName("commandText");

#if  UNITY_IPHONE || UNITY_ANDROID
		
		if(isMobile){
			if(GUILayout.Button(command)){
				currentEntry.Open();	
			}
			
			//GUI.Label(GUILayoutUtility.GetLastRect(), command);
		}else{
			command = GUILayout.TextField(command);
		}
			
		
#else
		command = GUILayout.TextField(command);
#endif
		
		GUILayout.EndArea();
		
		if(command != "" && !command.Contains("`") && isShowing){
			string matches = "";
		
			debugCommands.Where(x=>command.StartsWith(x.command.Substring(0, Mathf.Min(x.command.Length, command.Length)))).ToList().ForEach(y=>matches+=y.command+y.ParamHelp+" "+y.help+"\n");
			if(matches == ""){
				matches = "No commands found";	
			}
			Rect popupRect = new Rect(0, Screen.height/2, Screen.width, Screen.height/2);
			if(isMobile)
				popupRect = new Rect(0, 10, Screen.width, Screen.height/2-10);
			GUILayout.BeginArea(popupRect);
			if(popupBox == null){
				popupBox = new GUIStyle(GUI.skin.box);
				popupBox.alignment = TextAnchor.UpperLeft;
			}
			
			GUILayout.Label(matches, popupBox);
			GUILayout.EndArea();
			
		}
		
		/*if(GUI.Button(new Rect(10, Screen.height-40, 100, 30), this.isShowing ? "Hide" : "Show")){
				ToggleVisible();
		}*/
		
	}
				
	private int PrefixLength(string a, string b)
	{
		int max = Math.Min(a.Length, b.Length);
		for(int i = 0; i < max; ++i) {
			if(a[i] != b[i]) {
				return i;
			}
		}
		
		return max;
	}
	
	private string CommonPrefix(IEnumerable<string> strings)
	{
		string lastElem = null;
		int minPrefix = Int32.MaxValue;
		foreach(string s in strings) {
			if(lastElem != null) {
				var prefix = PrefixLength(s, lastElem);
				minPrefix = Math.Min(minPrefix, prefix);
			} else {
				minPrefix = s.Length;
			}
			lastElem = s;
		}
		
		if(lastElem != null) {
			return lastElem.Substring(0, minPrefix);
		} else {
			return "";
		}
	}
	
	public void LogTopUpdate(float amt){
		logTop = amt;	
	}
	
	#endregion
	
	#region Logging Functions
	public static void LogWarning(string output, params object[] objs){
		if( objs != null && objs.Length > 0 )
			Log(LogLevel.WARNING, "", String.Format(output, objs));
		else
			Log(LogLevel.WARNING, "", output);
	}
	public static void LogError(string output, params object[] objs){
		if( objs != null && objs.Length > 0 )
			Log(LogLevel.ERROR, "", String.Format(output, objs));
		else
			Log(LogLevel.ERROR, "", output);
	}
	
	public static void LogSystem(string output, params object[] objs){
		if( objs != null && objs.Length > 0 )
			Log(LogLevel.SYSTEM, "", String.Format(output, objs));
		else
			Log(LogLevel.SYSTEM, "", output);
	}
	
	public static void Log(string output, params object[] objs){
		if( objs != null && objs.Length > 0 )
			Log(LogLevel.DEBUG, System.DateTime.Now.ToString(), String.Format(output, objs));
		else
			Log(LogLevel.SYSTEM, "", output);
	}
	
	public static void Log(string header, string output, params object[] objs){
	
		
		Log(LogLevel.DEBUG, System.DateTime.Now.ToString(), output);
		
	}
	
	
	
	void HandleUnityLog(string message, string stackTrace, LogType type)
	{
		if(type == LogType.Exception)
		{
			outputs.Enqueue(new DebugConsole.DebugOutput(LogLevel.ERROR, System.DateTime.Now.ToString(), message+"\n"+stackTrace));
			scrollPos.y = 1000000000;
		}
	}
	
	public static void Log(LogLevel type, string header, string output){
	
		outputs.Enqueue(new DebugConsole.DebugOutput(type, header, output));
		
		if(outputs.Count > GlobalConfig.GetProperty<int>("MAX_LOG_SIZE", 100))
			outputs.Dequeue();
		
		scrollPos.y = 1000000000;
		
		if(forwardToUnity){
			switch(type){
				case LogLevel.DEBUG:
					Debug.Log(output);break;
				case LogLevel.ERROR:
					Debug.LogError(output);break;
				case LogLevel.WARNING:
					Debug.LogWarning(output);break;
				default:
					Debug.Log (output);break;
			}
		}
	}

	#endregion
	
	#region Built-in Debug Helper functions
	[DebugConsoleMethod("help", "- get a list of all current commands")]
	public void Debug_Help(){
	
		string output = "Command List:\n";
		foreach(DebugConsole.DebugCommand command in debugCommands){
			output += command.command+command.ParamHelp+" "+command.help+"\n";	
		}
		DebugConsole.LogSystem(output);
	}
	
		
	[DebugConsoleMethod("debug_level", "set the level filter for the log: 0=Debug, 1=Warning, 2=Error, 3=System, 4=Off")]
	public void SetDisplayLevel(int lvl){
		displayLogLevel = lvl;
	}

	
	#endregion
	
	#region Support Classes

	public class DebugCommand{
	
		public string command = "";
		public string help = "";
		public DebugConsole.LogLevel messageType;
		
		public string ParamHelp{
		
			get{
				if(allMethods.Count > 0){
					string output = "";
					foreach(ParameterInfo info in allMethods[0].Value.GetParameters()){
						output += " ["+info.Name+"]";
					}
					return output;
				}
				return "";
				
			}
		}
		
		
				
		public List<KeyValuePair<object, MethodInfo>> allMethods;
		
		
		public DebugCommand(){
			 allMethods = new List<KeyValuePair<object, MethodInfo>>();
		}
		
	}
	
	[System.Serializable]
	public class DebugCommandProps{
	
		public DebugCommand command;
		public string commandString;
		public string[] Params{
			get{
				
				if(command.command.Length+1 > commandString.Length)
					return new string[0];
				
				return commandString.Substring(command.command.Length+1).Split(new char[]{' '}, StringSplitOptions.RemoveEmptyEntries);
			}
		}
		public DebugCommandProps(DebugCommand cmd, string commandString){
			this.command = cmd;
			this.commandString = commandString;
		}
	}
	
	public class DebugOutput{
		public string header = "";
		public string output = "";
		public LogLevel logLevel;
		
		public DebugOutput(LogLevel type, string head, string output){
			this.header = head;
			this.output = output;
			this.logLevel = type;
		}
		
		public Color OutputColor{
		
			get{
				switch((int)logLevel){
				case 0: return Color.white;
				case 1: return Color.yellow;
				case 2: return Color.red;
				case 3: return Color.white; 
				default: return Color.white;
				}
				
			}
		}

		public override string ToString ()
		{
			return logLevel.ToString()+" "+header+" "+output;
		}
	}
	
	#endregion
	
#if UNITY_IPHONE || UNITY_ANDROID
	public class DebugTextEntry{
	
		protected string startText = "";
		public string text = "";
		protected TouchScreenKeyboard currentKeyboard;
		public void Open(){
			currentKeyboard = TouchScreenKeyboard.Open(text, TouchScreenKeyboardType.Default, false, false, false, false);	
			startText = text;
		}
		
		public string GetText(){
			text = currentKeyboard.text;	
			return text;
		}
		
		public bool Closed(out bool cancelled){
			
			cancelled = false;
			if(currentKeyboard == null)
				return false;
			
			if(currentKeyboard.wasCanceled){ 
				cancelled = true;
				text = startText;
				currentKeyboard = null;
				return true;	
				
			}
			
			if(currentKeyboard.done){
			
				text = currentKeyboard.text;
				currentKeyboard = null;
				return true;
			}
			
			text = currentKeyboard.text;
			return false;
		}
		
	}
#endif
	
}

