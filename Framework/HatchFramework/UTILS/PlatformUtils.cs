using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Runtime.InteropServices;

namespace HatchFramework
{
	public enum Platforms:int
	{
		IOS,
		IOSRETINA,
		ANDROID,
		WIN
	}
	
	public enum ScreenRatio{
	
		FOURxTHREE,
		SIXTEENxNINE
	}

	public class PlatformUtils : MonoBehaviour
	{

		#if UNITY_IPHONE
		[DllImport ("__Internal")]
		private static extern string GetUnityDeviceUniqueIdentifier();

		#endif

		private static float _backgroundScale = 1f;
		public static float BackgroundScale {
			get {
				return _backgroundScale;
			}
			set {
				_backgroundScale = value;
			}
		}

		public static Platforms GetPlatform()
		{
			switch(Application.platform)
			{
				case RuntimePlatform.Android:
					return Platforms.ANDROID;
				
				case RuntimePlatform.WindowsPlayer:
					return Platforms.WIN;			
				
				case RuntimePlatform.IPhonePlayer:
					
					if (Application.platform == RuntimePlatform.OSXEditor)
						return Platforms.IOSRETINA;
	
					if( Screen.width > 1024 )
						return Platforms.IOSRETINA;
					
					return Platforms.IOS;			
			}
			
			return Platforms.WIN;
		}
		
		public static ScreenRatio GetClosestRatio(){
			
			float ratio = (float)Screen.height/Screen.width;
			if(Mathf.Abs(0.75f-ratio) < Mathf.Abs(0.5625f-ratio)){
				return ScreenRatio.FOURxTHREE;
			}else{
				return ScreenRatio.SIXTEENxNINE;	
			}
			
		}
		
		public static string GetBundlePathForProject()
		{
			// /Users/NeoRiley/Documents/Infrared5/clients/Hatch/UnityConversion/Project/Apps/Loader/Assets
			int appsLoc = Application.dataPath.IndexOf("Apps");
			string path = Application.dataPath.Substring(0, appsLoc) + "AssetBundles/";
			Debug.Log("path : " + path);
			return path;
		}
		
		private static string internalDataPath;
		public static string InternalDataPath
		{
			get
			{
				if(internalDataPath == null) {
					internalDataPath = BuildInternalDataPath();
				}
				return internalDataPath;
			}
		}
			
		private static string BuildInternalDataPath()
		{
#if UNITY_ANDROID
			if(!Application.isEditor) {
				AndroidJavaClass classPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

				AndroidJavaObject objActivity = classPlayer.GetStatic<AndroidJavaObject>("currentActivity");
				string path = objActivity.Call<AndroidJavaObject>("getFilesDir")
					.Call<string>("getAbsolutePath");
				return path;
			}
#endif
			if(Application.platform == RuntimePlatform.WindowsPlayer) {
				return "C:\\ProgramData\\HatchSSG\\Assets";
			}
			
			return Application.persistentDataPath;
		}
		

		
		public static void CaptureScreenshot(){

#if UNITY_IPHONE || UNITY_ANDROID
			DebugConsole.Log("CaptureScreenshot not implemented for iOS/Android");
		return;
#else


			string folderPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop);
			folderPath = Path.Combine (folderPath, "SSG-Debug");

			if (!Directory.Exists (folderPath))
				Directory.CreateDirectory (folderPath);

			string timestamp = DateTime.Now.ToString ("yy-MMM-dd ddd hh.mm.ss");

			string path = Path.Combine(folderPath, GlobalConfig.GetProperty("ScreenshotFileName")+" "+timestamp+".png");
			string tpath = Path.Combine(folderPath, GlobalConfig.GetProperty("ScreenshotFileName")+" "+timestamp+"_log.txt");

			DebugConsole.Log ("**** SAVING SCREENSHOT TO " + path);
			Application.CaptureScreenshot (path);


			List<DebugConsole.DebugOutput> outputs = DebugConsole.outputs.ToListUtil();
			string[] lines = new string[outputs.Count];
			for (int i=0; i<outputs.Count; i++) {
				lines [i] = outputs [i].ToString ();
			}
			System.IO.File.WriteAllLines (tpath, lines);
			 
	#endif
		}
		
		public static string GetSerial() {
#if UNITY_ANDROID
			var build = new AndroidJavaClass("android.os.Build");
			string serial = build.GetStatic<string>("SERIAL");
			if(!string.IsNullOrEmpty(serial) && serial != "unknown") {
				return serial;
			}
			Debug.LogWarning("No serial found for device, using uniqueIdentifier");
			return SystemInfo.deviceUniqueIdentifier;

#elif UNITY_IPHONE

			return GetUnityDeviceUniqueIdentifier();

#else
			return SystemInfo.deviceUniqueIdentifier;

#endif

		}

		
		public static int GetDeviceModel(){
		
#if UNITY_IPHONE
	
			if( Screen.width > 1024 )
				return 12;
			else
				return 11;

			
#elif UNITY_ANDROID
		string model = SystemInfo.deviceModel;
		
		// being deliberately loose in case of variations in strings
		if(model.Contains("TF101")) {
			return 1;
		}
		else if(model.Contains("TF300T")) {
			return 9;
		} else {
			DebugConsole.LogWarning("Unknown device model: {0}", model);
			return 9;
		}
#else 
		DebugConsole.LogWarning("NOT RETURNING PROPER WINDOWS DEVICE - FIX");
		// It is unlikely that windows will be discernable
		// assuming to be AI0 2.0
		return 4;
#endif
			

		}
		
		public static void Quit() {
#if !UNITY_IPHONE && !UNITY_ANDROID
			// workaround for windows crash on exit
			if(!Application.isEditor) {
				System.Diagnostics.Process.GetCurrentProcess().Kill();
				return;
			}
#endif
			Application.Quit();
		}
	}
}