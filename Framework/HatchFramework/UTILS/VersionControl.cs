using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Versioning;

namespace HatchFramework
{
	public class VersionControl : MonoBehaviour 
	{
		protected static Version currentVersion;
		public static Version CurrentVersion{ 
			get {
				if(currentVersion == null){
					currentVersion = new Version(GlobalConfig.GetProperty("version"));	
				}
				return currentVersion;
			}
		}
		
		public static string CurrentVersionString
		{ 
			get { return GlobalConfig.GetProperty("version"); }
		}
		
		public static int CurrentVersionInt
		{
			get
			{
				return int.Parse (VersionControl.CurrentVersion.Major+""+VersionControl.CurrentVersion.Minor+""+VersionControl.CurrentVersion.Build+""+(int)VersionControl.CurrentVersion.MinorRevision);	
			}
		}
	}
}