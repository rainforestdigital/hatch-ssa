using System;

namespace HatchFramework
{
	public class SkillImplementationUtil
	{
		public static string ENG_LANG_TAG = "";
		public static string SPN_LANG_TAG = "SPN-";
		
		public SkillImplementationUtil ()
		{
		}
		
		public static bool IsSkillImplemented(string skill)
		{
			switch(skill)
			{
			case "LetterRecognition":
			case "NumeralRecognition":
			case "InitialSounds":
			case "CommonShapes":
			case "CountingFoundations":
			case "Sorting":
			case "SpatialSkills":
			case "SentenceSegmenting":
			case "LanguageVocabulary":
			case "Addition":
			case "AdditionUpToTen":
			case "Subtraction":
			case "SubtractionFromTen":
			case "OnsetRime":
			case "DeletingOnsetAndRime":
			case "ObjectsInASet":
			case "SequenceCounting":
			case "SegmentingCompoundWords":
			case "BlendingCompoundWords":
			case "Patterning":
			case "Measurement":
			case "BlendingSoundsInWords":
				return true;
			default:
				return false;
			}
		}
		
		public static bool isSkillValidforProfile( string skill, int profile )
		{
			if( getLangTagforSkill( skill, profile ).Contains("BAD") )
				return false;
			
			return true;
		}
		
		public static string getLangTagforSkill( string skill, int profile )
		{
			if( !IsSkillImplemented( skill ) )
				return "BAD_SKILL";
			
			switch( profile )
			{
			//1 - all English
			case 1:
				return ENG_LANG_TAG;
				
			//2 - all Spanish
			case 2:
				if( IsSkillSpanish( skill ) )
					return SPN_LANG_TAG;
				else
					return "BAD_SKILL_FOR_SPANISH";
				
			//3 - Spanish Math; English Literacy
			case 3:
				if( SpanishMath( skill ) )
					return SPN_LANG_TAG;
				else if( EnglishLiteracy( skill ) )
					return ENG_LANG_TAG;
				else
					return "BAD_SKILL_FOR_PROFILE";
				
			//4 - English Math; Spanish Literacy
			case 4:
				if( SpanishLiteracy( skill ) )
					return SPN_LANG_TAG;
				else if( EnglishMath( skill ) )
					return ENG_LANG_TAG;
				else
					return "BAD_SKILL_FOR_PROFILE";
				
			default:
				return "BAD_PROFILE";
			}
		}
		
		public static bool IsSkillSpanish( string skill )
		{
			return SpanishMath(skill) || SpanishLiteracy(skill);
		}
		
		public static bool EnglishMath( string skill )
		{
			if( !IsSkillImplemented( skill ) )
				return false;
			
			switch(skill)
			{
			case "AdditionUpToTen":
			case "SubtractionFromTen":
				return true;
			default:
				return SpanishMath( skill );
			}
		}
		
		public static bool SpanishMath( string skill )
		{
			if( !IsSkillImplemented( skill ) )
				return false;
			
			switch(skill)
			{
			case "Addition":
			case "CountingFoundations":
			case "CommonShapes":
			case "NumeralRecognition":
			case "ObjectsInASet":
			case "Sorting":
			case "SequenceCounting":
			case "Subtraction":
			case "Patterning":
				return true;
			default:
				return false;
			}
		}
		
		public static bool EnglishLiteracy( string skill )
		{
			if( !IsSkillImplemented( skill ) )
				return false;
			
			switch(skill)
			{
			case "InitialSounds":
			case "SentenceSegmenting":
			case "OnsetRime":
			case "DeletingOnsetAndRime":
			case "SegmentingCompoundWords":
			case "BlendingCompoundWords":
			case "BlendingSoundsInWords":
				return true;
			default:
				return SpanishLiteracy( skill );
			}
		}
		
		public static bool SpanishLiteracy( string skill )
		{
			if( !IsSkillImplemented( skill ) )
				return false;
			
			switch(skill)
			{
			case "SpatialSkills":
			case "Measurement":
			case "LetterRecognition":
			case "LanguageVocabulary":
				return true;

			default:
				return false;
			}
		}
	}
}
