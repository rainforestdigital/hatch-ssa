using System;
using pumpkin.events;

namespace HatchFramework
{
	public interface IEventDispatcher
	{
		void AddEventListener(string type, pumpkin.events.EventDispatcher.EventCallback callback);
		void RemoveEventListener(string type, pumpkin.events.EventDispatcher.EventCallback callback);

	}
}

