using System;
using System.Collections.Generic;

namespace HatchFramework
{
	public static class CollectionUtils
	{
		public static void Shuffle<T>(this IList<T> list)
		{
			// standard Fisher-Yates shuffle
			var rand = new Random();
			for(int i = list.Count-1; i > 0; --i)
			{
				int j = rand.Next(0, i+1);
				var temp = list[i];
				list[i] = list[j];
				list[j] = temp;
			}
		}
		
		public static List<T> RandomSample<T>(this IEnumerable<T> list, int sampleSize)
		{
			// Use a reservoir sample to select K elements
			List<T> result = new List<T>(sampleSize);
				
			var rand = new Random();
			int index = 0;
			foreach(var elem in list)
			{
				if(index < sampleSize)
				{
					result.Add(elem);
				}
				else
				{
					int j = rand.Next(0, index+1);
					if(j < sampleSize)
					{
						result[j] = elem;
					}
				}
				index++;
			}
			
			// This algorithm generates a random sample, but the order
			// is not guaranteed to be random, especially when N < k,
			// so explicitly shuffle the results
			result.Shuffle();
			
			return result;
		}
		
		public static List<T> RandomSampleWith<T>(this ICollection<T> list, int sampleSize, T ensure)
		{
			var result = list.RandomSample(sampleSize);
			if(!result.Contains(ensure)) 
			{
				var rand = new Random();
				result[rand.Next(result.Count)] = ensure;
			}
			
			return result;
		}
		
		public delegate int WeightFunction<T>(T param);
		
		public static T SelectWeighted<T>(this ICollection<T> list, WeightFunction<T> weight)
		{
			List<T> items = list.ToListUtil();
			items.Shuffle();
			
			int totalWeight = 0;
			foreach(var elem in items)
			{
				totalWeight += weight(elem);
			}
			
			var rand = new Random();
			int result = rand.Next(totalWeight);
			foreach(var elem in items)
			{
				result -= weight(elem);
				if(result < 0)
				{
					return elem;
				}
			}
			
			return default(T);
		}
		
		public static IEnumerable<T> Filter<T>(this IEnumerable<T> list, Predicate<T> predicate)
		{
			foreach(var elem in list)
			{
				if(predicate(elem))
				{
					yield return elem;
				}
			}
		}
		
		
		public static List<T> ToListUtil<T>(this IEnumerable<T> enumerable)
		{
			return new List<T>(enumerable);
		}
		
	}
}

