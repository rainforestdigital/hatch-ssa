using System;
using uUnit;
using HatchFramework;
using SSGCore;
using UnityEngine;

namespace SSGUnitTests
{
	[TestFixture]
	public class UBaseSkill
	{

		protected Tutorial tutorial;

		/// <summary>
		/// Sets Up TestFixture
		/// Called Once Before any tests are run
		/// </summary>
		[TestFixtureSetUp]
		public virtual void TestSetUp()
		{
			//GameObject tutorial_go = new GameObject ();
			//tutorial = new Tutorial();
		}

		[Test]
		public virtual void  VirtualTest()
		{
			Assert.Pass();
		}


		/// <summary>
		/// Tests the tear down.
		/// </summary>
		[TestFixtureTearDown]
		public virtual void TestTearDown()
		{
			
		}

	}
}

