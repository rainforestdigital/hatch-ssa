using UnityEngine;
using UnityEditor;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;
using Unity;
using System.IO;

using System.Net;
using System.Threading;

using Renci.SshNet;
using Renci.SshNet.Sftp;

namespace SSGEditorTools
{
	public class BuildSupport
	{

		private static string rmsXML;

		public static string GetRootPathForProject()
		{
			// /Users/NeoRiley/Documents/Infrared5/clients/Hatch/UnityConversion/Project/Apps/Loader/Assets
			int appsLoc = Application.dataPath.IndexOf("Apps");
			string path = Application.dataPath.Substring (0, appsLoc+5);
			return path;
		}

		public static string GetBundlePathForBundles()
		{
			// /Users/NeoRiley/Documents/Infrared5/clients/Hatch/UnityConversion/Project/Apps/Loader/Assets
			int appsLoc = Application.dataPath.IndexOf("Apps");
			string path = Application.dataPath.Substring(0, appsLoc) + "AssetBundles/";

			return path;
		}
		
		public static void SetBuildSettings(string version, string config)
		{	
			// update for iOS and Android
			PlayerSettings.bundleVersion = version;
			
			// updated the BuildConfig.txt
			string[] str = File.ReadAllLines(GetRootPathForProject ()+ "Loader/Assets/Resources/Config/BuildConfig.txt");
			for(int i=0; i< str.Length; i++)
			{
				if( str[i].Length > 2 && str[i].Substring(0,1) == "#" && i < str.Length-2) continue;
				if( str[i].Contains("version =") && !str[i].Contains("dbversion") ) str[i] = "version = " + version;
				if( str[i].Contains("@LoadConfig ") && !str[i].Contains("#@LoadConfig ") ) str[i] = "@LoadConfig Config/Builds/" + config;
				Debug.Log("line: " + str[i]);
			}
			
			File.WriteAllLines(GetRootPathForProject ()+ "Loader/Assets/Resources/Config/BuildConfig.txt", str);
			AssetDatabase.Refresh ();
		}
	
		[MenuItem("Shell Squad Games/Build All Bundles")]
		public static void BuildBundles()
		{

			Debug.Log (GetRootPathForProject ());

			//parse XML file from 
			rmsXML = File.ReadAllText(GetRootPathForProject ()+ "Loader/Assets/Resources/Config/BuildBundles.xml");

			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(rmsXML);			
			XmlNodeList bundleList = xmlDoc.GetElementsByTagName("bundle");
			
			string currentXML = File.ReadAllText(GetRootPathForProject ()+ "Loader/Assets/Resources/Config/RMSResponse.xml");
			
			XmlDocument rmsDoc = new XmlDocument();
			rmsDoc.LoadXml(currentXML);
			XmlNodeList rmsList = rmsDoc.GetElementsByTagName("bundle");

			Platforms[] platforms = new Platforms[] { Platforms.ANDROID, Platforms.IOS, Platforms.IOSRETINA, Platforms.WIN }; 

			string[] s = Application.dataPath.Split('/');
			string projectName = s[s.Length - 2];
			string version = "0_0_0_1";
			
			//logic for skipping individual bundles based on RMSResponse.xml vs BuildBundles.xml - if identical, build all
			bool buildAll = true;
			bool buildThis = false;
			
			foreach( XmlNode bNode in bundleList )
			{
				string bName = bNode.Attributes["name"].Value;
				bool bNameFound = false;
				if( bName == projectName )
					version = bNode.Attributes ["version"].Value;
				
				for( int j = 0; j < rmsList.Count; j++ )
				{
					XmlNode rNode = rmsList[j];
					if( bName == rNode.Attributes["name"].Value)
					{
						bNameFound = true;
						if( bNode.Attributes["version"].Value != rNode.Attributes["version"].Value )
						{
							if( bName == projectName )
								buildThis = true;
							buildAll = false;
						}
						break;
					}
				}
				if( !bNameFound )
				{
					if( bName == projectName )
						buildThis = true;
					buildAll = false;
				}
			}
			
			if( !buildAll && !buildThis )
			{
				Debug.Log( "RMSResponse version == BuildBundles version, differences in other versions detected, skipping " + projectName );
				return;
			}
			else
				Debug.Log( projectName + " - Build All? " + buildAll + ", Build This? " + buildThis );
			
			string bundlePath = CreateAssetBundleWindow.GetBundlePathForBundles ();
			List<string> bundlePaths = new List<string> ();

			int i = 0;

			for(i = 0; i < platforms.Length; i++)
			{

				if (!CreateBundleUtil.BundlesExist (version, projectName, platforms[i], bundlePath)) 
				{
					//Debug.LogWarning ("BUNDLE DOES NOT EXIST at " + version + " " + projectName);
					string path = CreateBundleUtil.CreateBundle (projectName, bundlePath, platforms [i], version, version);
					if (string.IsNullOrEmpty (path)) 
					{
						Debug.LogError ("BUNDLE PATH IS NULL");
					} 
					else 
					{
						bundlePaths.Add (path);
					}
				}

			}

			//Upload To FTP
			for (i = 0; i < bundlePaths.Count; i++) 
			{
				Upload (bundlePaths[i]);
			}
		}

		[MenuItem("Shell Squad Games/Test Upload")]
		public static void TestUpload(){
			UploadSftp ("/Users/anthonycapobianchi/Documents/Infrared5/Repos/Hatch/hatch-unity-port/AssetBundles/ANDROID_LanguageVocabulary_0_0_1_6.unity3d");
		}

		private static void Upload(string fileName)
		{

			Debug.Log ("Upload || fileName: " + fileName);

			int ftp_port = 21;
			string hostname = "52.2.199.130";
			string ftpUserID = "hatch";
			string ftpPassword = "dvduTMnbC6ZQM3";
			string ftp_folder = ""; //Not needed?

			FileInfo fileInfo = new FileInfo (fileName);
			FtpWebRequest reqFTP;

			string fullPath = "ftp://" + hostname + ":" + ftp_port + "/" + ftp_folder + fileInfo.Name;
			Debug.Log ("Upload || fullPath: " + fullPath);

			reqFTP = (FtpWebRequest)FtpWebRequest.Create (new Uri (fullPath));

			reqFTP.Credentials = new NetworkCredential (ftpUserID, ftpPassword);
			reqFTP.KeepAlive = false;
			reqFTP.Method = WebRequestMethods.Ftp.UploadFile;
			reqFTP.UseBinary = true;
			reqFTP.ContentLength = fileInfo.Length;

			int buffLength = 2048 * 2;
			byte[] buff = new byte[buffLength];
			int contentLength;
			FileStream fs = fileInfo.OpenRead ();

			Debug.Log ( "Upload || fs is null: " + (fs == null));

			try 
			{
				Stream strm = reqFTP.GetRequestStream ();
				contentLength = fs.Read (buff, 0, buffLength);

				while (contentLength != 0) 
				{
					strm.Write (buff, 0, contentLength);
					contentLength = fs.Read(buff, 0, buffLength);
				}
				strm.Close();
				fs.Close();
				Debug.LogWarning("Upload Successful");
			} 
			catch (Exception ex) {
				Debug.LogError ("Upload Error -ABORTING- : " + ex.Message);
			}

		}					                          

	
		static private bool UploadSftp(string filename)
		{
			int ftp_port = 22;
			string hostname = "184.106.133.84";
			string ftpUserID = "ftp";
			string ftpPassword = "hatch-unity";
			string ftp_folder = "";

			if (hostname == "" || ftp_port <= 0)
			{
				EditorUtility.DisplayDialog("Error", "Invalid FTP details, please verify the FTP settings.", "OK");
				return false;
			}


			try
			{
				using (var sftp = new SftpClient(hostname, ftp_port, ftpUserID, ftpPassword))
				{
					sftp.Connect();

					FileInfo info = new FileInfo(filename);
					EditorUtility.DisplayProgressBar("Publish", "Uploading " + info.Name, 0);
					using (var file = info.OpenRead())
					{
						var remoteFilename = ftp_folder + info.Name;
						if(sftp.Exists(remoteFilename))
						{
							Debug.LogError("FIle already exists at " + remoteFilename);
							return true;
						}

						float uploadSize = info.Length;
						var result = (SftpUploadAsyncResult)sftp.BeginUploadFile(file, remoteFilename);
						while(!result.IsCompleted)
						{
							EditorUtility.DisplayProgressBar("Publish", "Uploading " + info.Name, result.UploadedBytes / uploadSize);
							Thread.Sleep(100);
						}
						sftp.EndUploadFile(result);
						EditorUtility.ClearProgressBar(); 
					}
				}
			}
			catch (Exception ex)
			{
				EditorUtility.ClearProgressBar(); 
				EditorUtility.DisplayDialog("Upload error: ", ex.Message, "OK");
				Debug.LogError("Upload Error: " + ex.Message);
				return false;
			}
			return true;
		}
	}
	

}

