using System;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace SSGEditorTools
{
	public class BuildSettingsWindow : EditorWindow
	{
		private static BuildSettingsWindow window;
		
		[MenuItem ("Shell Squad Games/Set Build Settings")]
		static void Init () 
		{
			// Get existing open window or if none, make a new one:
			window = (BuildSettingsWindow)EditorWindow.GetWindow (typeof (BuildSettingsWindow));
			window.InitializeWindow();
			
	    }
		
		private string[] configs = new string[3]{"DEBUG", "QA", "RC"};
		private int index = 0;
		private int newIndex = 0;
		private string version;
		private bool initializing = false;
		void InitializeWindow()
		{
			string[] str = File.ReadAllLines(BuildSupport.GetRootPathForProject ()+ "Loader/Assets/Resources/Config/BuildConfig.txt");
			for(int i=0; i< str.Length; i++)
			{
				if( str[i].Length > 2 && str[i].Substring(0,1) == "#" && i < str.Length-2) continue;
				if( str[i].Contains("@LoadConfig ") && !str[i].Contains("#@LoadConfig ") )
				{
					int index = str[i].ToString().LastIndexOf('/');
					string currentConfig = str[i].Substring(index+1);
					
					for( int j=0; j< configs.Length; j++ )
					{
						if( configs[j] == currentConfig ) 
						{
							newIndex = j;
							initializing = true;
						}
					}
				}
			}
			version = PlayerSettings.bundleVersion;
		}
		
		public void OnGUI() 
		{
			if( initializing )
			{
				initializing = false;
				index = newIndex;
			}
			
			EditorGUILayout.LabelField("Build Settings");
			index = EditorGUILayout.Popup(index, configs);
			
			version = EditorGUILayout.TextField("Version:", version);
			
			if( GUILayout.Button("Submit") )
			{
				BuildSupport.SetBuildSettings(version, configs[index]);
			}
		}
	}
}

