using System;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace SSGEditorTools
{
	public class CreateAssetBundleWindow : EditorWindow
	{ 
		private static CreateAssetBundleWindow window;
		
		[MenuItem ("Shell Squad Games/Asset Bundle Creator")]
		static void Init () 
		{
			// Get existing open window or if none, make a new one:
			window = (CreateAssetBundleWindow)EditorWindow.GetWindow (typeof (CreateAssetBundleWindow));
			window.InitializeWindow();
			
	    }

		public string projectName;
		public string bundlePath;
		public Platforms platform = Platforms.IOS;
		public string version;
		public string currentVersion;
		void InitializeWindow()
		{
			previousPlatform = platform;
			SetProjectName();
			bundlePath = GetBundlePathForBundles();
			SetFileVersion();
			
			if( !Directory.Exists(Application.dataPath + "/Resources") )
			{
				Debug.LogError("CANNOT LOAD WINDOW - THE PROJECT DOES NOT CONTAIN THE FILE STRUCTURE FOR CREATING ASSET BUNDLES - Resources is missing");
				window.Close();
				return;
			}
			
			if( !Directory.Exists(Application.dataPath + "/Resources/SSGAssets/UNIVERSAL") ) 
			{
				Debug.LogError("CANNOT LOAD WINDOW - THE PROJECT DOES NOT CONTAIN THE FILE STRUCTURE FOR CREATING ASSET BUNDLES - Universal is missing");
				window.Close();
				return;
			}
			
			if( !Directory.Exists(Application.dataPath + "/Resources/SSGAssets/" + platform.ToString()) ) 
			{
				Debug.LogWarning("THE PROJECT DOES NOT CONTAIN THE FILE STRUCTURE FOR CREATING ASSET BUNDLES - " + platform.ToString() + " is missing");
				//window.Close();
				//return;
			}
			
			Debug.Log("BUNDLE PATH: " + bundlePath);
		}
		
		public static string GetBundlePathForBundles()
		{
			// /Users/NeoRiley/Documents/Infrared5/clients/Hatch/UnityConversion/Project/Apps/Loader/Assets
			int appsLoc = Application.dataPath.IndexOf("Apps");
			string path = Application.dataPath.Substring(0, appsLoc) + "AssetBundles/";
			Debug.Log("path : " + path);
			return path;
		}
		
		private void SetProjectName()
		{
		    string[] s = Application.dataPath.Split('/');
			projectName = s[s.Length - 2];
		    Debug.Log("project = " + projectName);
		}
		
		private Platforms previousPlatform;
		public void OnGUI() 
		{
			EditorGUILayout.LabelField("Project:", projectName);
			platform = (Platforms)EditorGUILayout.EnumPopup("Platform:", platform);
			
			if( platform != previousPlatform )
			{
				SetFileVersion();
				previousPlatform = platform;
			}
			
			EditorGUILayout.LabelField("Current Version:", currentVersion);
			version = EditorGUILayout.TextField("Version:", version);
			
			if( GUILayout.Button("Build Bundle") )
			{
				CreateBundleUtil.CreateBundle (projectName, bundlePath, platform, version, currentVersion);
				//BuildBundle();
			}
		}
		
		private void BuildBundle()
		{
			System.Version ver = null;
			
			// have user use . notation, save with underscores
			try
			{
				ver = new System.Version(version.Replace("_", ".")); // 0.0.0.0
			}
			catch( System.Exception err )
			{
				Debug.LogError("VERSION ERROR: You must enter a valid version number (x.x.x.x): " + err.Message);
				return;
			}
			
			string path = "Assets/Resources/SSGAssets/";

			Dictionary<string, string> filelist = GetFileListFromFolder(Application.dataPath + "/Resources/SSGAssets/" + platform.ToString(), path + platform.ToString() + "/");
			
			if( filelist == null ) return;		
			
			Dictionary<string, string> univlist = GetFileListFromFolder(Application.dataPath + "/Resources/SSGAssets/UNIVERSAL", path + "UNIVERSAL/");
			Dictionary<string, string> platformSWFFolderList = GetSWFFoldersInPlatform();
			Dictionary<string, string> universalSWFFolderList = GetSWFFoldersInUniversal();
			
			
			
			// strip duplicate files
			foreach(KeyValuePair<string, string> pair in filelist)
			{
				if( univlist.ContainsKey( pair.Key ) )
				{
					univlist.Remove(pair.Key);
				}
			}
			
			// strip duplicate folders
			if( universalSWFFolderList != null && platformSWFFolderList != null )
			{
				foreach(KeyValuePair<string, string> pair in platformSWFFolderList )
				{
					if( universalSWFFolderList.ContainsKey(pair.Key) )
					{
						universalSWFFolderList.Remove(pair.Key);
					}
				}
			}
			
			Dictionary<string, string> finalList = new Dictionary<string, string>();
			Dictionary<string, string> finalSWFFolderList = new Dictionary<string, string>();
			
			// combine folders
			if( platformSWFFolderList != null )
			{
				foreach( KeyValuePair<string, string> pair in platformSWFFolderList )
				{
					finalSWFFolderList.Add(path + pair.Key, path + pair.Value);
				}
			}
			
			if( universalSWFFolderList != null )
			{
				foreach( KeyValuePair<string, string> pair in universalSWFFolderList )
				{
					finalSWFFolderList.Add(path + pair.Key, path + pair.Value);
				}
			}
			
			Debug.LogWarning("FINAL SWF FOLDER LIST COUNT: " + finalSWFFolderList.Count);
			
			// combine files
			foreach(KeyValuePair<string, string> pair in filelist)
			{
				finalList.Add(pair.Key, pair.Value);
			}
			
			foreach(KeyValuePair<string, string> pair in univlist)
			{
				finalList.Add(pair.Key, pair.Value);
			}
			
			// add SWF folders to the files list
			foreach( KeyValuePair<string, string> pair in finalSWFFolderList )
			{
				Dictionary<string, string> files = GetSWFFileList(pair.Value);
				Debug.LogWarning("SWF FILE LIST COUNT FOR " + pair.Value + ": " + files.Count);
				foreach( KeyValuePair<string, string> filePair in files )
				{
					Debug.LogWarning("FILE BEING ADDED: " + filePair.Key + ", PATH: " + filePair.Value);
					finalList.Add(filePair.Key, filePair.Value);
				}
			}
			
			UnityEngine.Object[] objs = new UnityEngine.Object[finalList.Count];
			string[] strs = new string[finalList.Count];;
			int counter = 0;
			string filesCreated = "";
			foreach(KeyValuePair<string, string> pair in finalList)
			{
				objs[counter] = AssetDatabase.LoadMainAssetAtPath(pair.Value);
				if( objs[counter] == null ) Debug.LogError("ASSET DATABASE FAILED TO LOAD: " + pair.Value);
				strs[counter] = pair.Key;
				filesCreated += pair.Key + ", path: " + pair.Value + "\n";
				counter++;
			}
			
			
			string v = ver.Major.ToString() + "_"+ ver.Minor.ToString() + "_" + ver.Build.ToString() + "_" + ver.Revision.ToString();
			string fileName = platform.ToString() + "_" + projectName + "_" + v + ".unity3d";
			Debug.Log("FILE NAME: " + fileName + ", FILES: " + filesCreated);
			BuildPipeline.BuildAssetBundleExplicitAssetNames(objs, strs, bundlePath + fileName, BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.CompleteAssets, SSGEditorTools.PlatformUtils.GetBuildPlatformForBundle(platform));
		}
    	
		private char pad = '0';
		private void SetFileVersion()
		{			
			DirectoryInfo dir = new DirectoryInfo(bundlePath);
			FileInfo[] info = dir.GetFiles("*.*");
			
			if( info.Length == 0 ) 
			{
				currentVersion = "NOT FOUND";
				version = "0.0.0.1";
				return;
			}
			
			System.Version highVersion = null;
			
			foreach (FileInfo f in info) 
			{
				if( f.Name.Contains(platform.ToString()) && f.Name.Contains(projectName) ) 
				{
					//IOS_GlobalAssets_0_1_0_0.unity3d
					//0 = platform, 1 = bundleName, 2+ = version
					string file = f.Name.Replace(".unity3d", "");
					string[] ary = file.Split('_');
					
					System.Version tempVersion = new System.Version(ary[2] + "." + ary[3] + "." + ary[4] + "." + ary[5]);
					
					if( highVersion != null && highVersion.CompareTo(tempVersion) == -1 )
					{
						highVersion = tempVersion;
					}
					else if( highVersion == null ) highVersion = tempVersion;
				}
			}
			
			if( highVersion == null ) 
			{
				currentVersion = "NOT FOUND";
				version = "0.0.0.1";
				return;
			}
						
			currentVersion = highVersion.Major.ToString() + "."+ highVersion.Minor.ToString() + "." + highVersion.Build.ToString() + "." + highVersion.Revision.ToString();
			
			float v = float.Parse("." + currentVersion.Replace(".", ""));
			v += .0001f;
			version = v.ToString().Split('.')[1];
			
			version = version.PadRight(4, pad);
			
			string[] a = version.Select(c => c.ToString()).ToArray();
			version = String.Join(".", a);
		}
		
		private Dictionary<string, string> GetFileListFromFolder(string fullPath, string assetPath)
		{
			Dictionary<string, string> list = new Dictionary<string, string>();
			DirectoryInfo dir = new DirectoryInfo(fullPath);
			
			if( dir == null ) return null;
			
			FileInfo[] info = dir.GetFiles("*.*");
			
			foreach (FileInfo f in info) 
			{
				if( f.Name.Contains("meta") ) continue;
				if( f.Name.Contains(".DS_Store") ) continue;
				if( f.Name.Contains(".swf") ) continue;
				
				string name = f.Name;
				bool isAudioFile = name.Contains(".mp3") || name.Contains(".wav") ? true : false;
				if( isAudioFile ) 
				{
					//Debug.Log("IS AUDIO: " + fullPath + ", assetPath: " + assetPath);
					int index = assetPath.IndexOf("Audio/");
					string temp = assetPath.Substring(index + 6);
					name = temp + name;
				}
				if( name.Contains(".png") ) name = name.Replace(".png", "");
				if( name.Contains(".jpg") ) name = name.Replace(".jpg", "");
				if( name.Contains(".wav") ) name = name.Replace(".wav", "");
				if( name.Contains(".mp3") ) name = name.Replace(".mp3", "");
				if( name.Contains(".mov") ) name = name.Replace(".mov", "");
				if( name.Contains(".MOV") ) name = name.Replace(".MOV", "");
				//Debug.Log("GET FILE LIST FROM FOLDER: " + name);
				list.Add(name, assetPath + f.Name );
			}
			
			DirectoryInfo[] dirs = dir.GetDirectories();
			
			if( dirs.Length > 0 )
			{
				foreach(DirectoryInfo d in dirs )
				{
					if( d.Name.Contains(".swf") ) continue;
					Dictionary<string, string> tempList = GetFileListFromFolder(fullPath + "/" + d.Name, assetPath + d.Name + "/");
					
					if( tempList.Count > 0 )
					{
						foreach(KeyValuePair<string, string> pair in tempList)
						{
							list.Add(pair.Key, pair.Value);
						}
					}
				}
			}
			
			return list;
		}
		
		private Dictionary<string, string> GetSWFFileList(string swfPath)
		{
			Debug.LogWarning("GET SWF FILE LIST: " + swfPath);
			Dictionary<string, string> list = new Dictionary<string, string>();
			DirectoryInfo dir = new DirectoryInfo(swfPath);
			
			if( dir == null ) return null;
			
			FileInfo[] info = dir.GetFiles("*.*");
			
			foreach (FileInfo f in info) 
			{
				if( f.Name.Contains("meta") ) continue;
				if( f.Name.Contains(".DS_Store") ) continue;
				string name = f.Name;
				if( name.Contains(".bytes") ) name = name.Replace(".bytes", "");
				if( name.Contains(".png") ) name = name.Replace(".png", "");
				list.Add(name, swfPath + "/" + f.Name);
			}
			
			return list;
		}
		
		private Dictionary<string, string> GetSWFFoldersInUniversal()
		{
			if( !Directory.Exists(Application.dataPath + "/Resources/SSGAssets/UNIVERSAL/Resources/") ) 
			{
				Debug.LogWarning("UNIVERSAL DOES NOT CONTAIN A RESOURCES DIRECTORY FOR LOADING SWF CONTENT");
				return null;
			}
			
			Debug.LogWarning("FOUND RESOURCES DIRECTORY IN UNIVERSAL WHILE SEARCHING FOR SWF FOLDERS");
			
			Dictionary<string, string> list = new Dictionary<string, string>();
			DirectoryInfo dir = new DirectoryInfo(Application.dataPath + "/Resources/SSGAssets/UNIVERSAL/Resources/");
			
			DirectoryInfo[] dirs = dir.GetDirectories("*.swf");
			
			foreach(DirectoryInfo d in dirs )
			{
				Debug.LogWarning("SWF DIR: "  +d.Name);
				list.Add(d.Name, "UNIVERSAL/Resources/" + d.Name);
			}
			
			return list;
		}
		
		private Dictionary<string, string> GetSWFFoldersInPlatform()
		{
			if( !Directory.Exists(Application.dataPath + "/Resources/SSGAssets/" + platform.ToString() + "/Resources/") ) 
			{
				Debug.LogWarning(platform.ToString() + " DOES NOT CONTAIN A RESOURCES DIRECTORY FOR LOADING SWF CONTENT");
				return null;
			}
			
			Dictionary<string, string> list = new Dictionary<string, string>();
			DirectoryInfo dir = new DirectoryInfo(Application.dataPath + "/Resources/SSGAssets/" + platform.ToString() + "/Resources/");
			
			DirectoryInfo[] dirs = dir.GetDirectories("*.swf");
			
			foreach(DirectoryInfo d in dirs )
			{
				list.Add(d.Name, platform.ToString() + "/Resources/" + d.Name);
			}
			
			return list;
		}
	}
}
