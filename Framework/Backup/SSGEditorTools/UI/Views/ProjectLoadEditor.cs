using System;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Collections.Generic;


namespace SSGEditorTools
{
	public class ProjectLoadEditor : EditorWindow
	{
		
		public DirectoryInfo[] skillProjects;
		protected DirectoryInfo rootDirectory;
		
		
		public ProjectLoadEditor ()
		{
			
		}
		
		[MenuItem ("Shell Squad Games/Load Project")]
		static void Init () 
		{
			// Get existing open window or if none, make a new one:
			ProjectLoadEditor window = (ProjectLoadEditor)EditorWindow.GetWindow (typeof (ProjectLoadEditor));
			window.InitializeWindow();
	    }
		
		void InitializeWindow(){
			
			DirectoryInfo currentDirectory = new DirectoryInfo(Application.dataPath);
			bool atRoot = false;
			while(!atRoot){
			
				int count = 0;
				DirectoryInfo[] children = currentDirectory.GetDirectories();
				foreach(DirectoryInfo c in children){
					if(c.Name == "Apps" || c.Name == "Framework")
						count++;
					
					
				}
			
				if(count == 2){
					atRoot = true;
					break;
				}else{
					currentDirectory = currentDirectory.Parent;
					if(currentDirectory == null)
						break;
				}
			}
			
			if(atRoot){
			
				rootDirectory = currentDirectory;
				skillProjects = new DirectoryInfo(rootDirectory.FullName+"/Apps/Skills").GetDirectories();

			}else{
				rootDirectory = null;	
			}
		}
		
		void OnGUI(){
		
			if(rootDirectory != null){
				
				if(GUILayout.Button("Main Loader")){
					EditorApplication.OpenProject(rootDirectory.FullName+"/Apps/Loader");	
				}
				
				if(GUILayout.Button("Global Assets")){
					EditorApplication.OpenProject(rootDirectory.FullName+"/Apps/GlobalAssets");	
				}
				
				EditorGUILayout.LabelField("Skill Projects");
				foreach(DirectoryInfo di in skillProjects){
				
					if(GUILayout.Button(di.Name)){
						EditorApplication.OpenProject(di.FullName);	
					}
				}
				
				
			}else{
				GUILayout.Label("No root directory found");	
			}
		}
    
	}
}
