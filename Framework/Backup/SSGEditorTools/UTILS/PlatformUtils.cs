using System;
using UnityEditor;

namespace SSGEditorTools
{
	/// <summary>
	/// Platforms.  THIS HAS TO MATCH WHATEVER HATCHFRAMEWORK.PLATFORMUTILS HAS SPECIFIED
	/// </summary>
	public enum Platforms:int
	{
		IOS,
		IOSRETINA,
		ANDROID,
		WIN
	}
	
	public class PlatformUtils
	{
		public static BuildTarget GetBuildPlatformForBundle(Platforms platform)
		{
			switch(platform)
			{
				case Platforms.ANDROID:
					return BuildTarget.Android;
				
				case Platforms.WIN:
					return BuildTarget.StandaloneWindows;			
				
				case Platforms.IOS:
				case Platforms.IOSRETINA:
					return BuildTarget.iPhone;			
			}
			
			return BuildTarget.StandaloneWindows;
		}
	}
}

