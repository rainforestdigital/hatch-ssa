using System;
using uUnit;
using HatchFramework;
using SSGCore;
using UnityEngine;
using System.Threading;
namespace SSGUnitTests
{
	[TestFixture]
	public class UDownloadDeviceConfig
	{
		
		
		protected DownloadConfig deviceConfig;
		
		[SetUp]
		public void Setup(){
			GameObject go = new GameObject();
			deviceConfig = go.AddComponent<DownloadConfig>() as DownloadConfig;
			deviceConfig.OnDownloadComplete += HandleDownloadComplete;
		}
		
		public void HandleDownloadComplete(){
			Assert.Equals(deviceConfig.error, "");
			Assert.DoesNotEqual(deviceConfig.config, "");
		}
		
		[TearDown]
		public void TearDown(){
			//destroy the objects
			GameObject.Destroy(deviceConfig.gameObject);
		}
		
		[Test]
		public void DownloadConfigurationFileFromHatchRMS ()
		{
			
			deviceConfig.GetConfig();
			while(deviceConfig.error == "" && deviceConfig.config == ""){
				Thread.Sleep(2);
			}
		}
		
	}
}

