using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using pumpkin;
using pumpkin.display;
using pumpkin.text;
using pumpkin.ui;
using pumpkin.swf;
using pumpkin.events;

namespace HatchFramework
{
	public delegate void OnRequestedMovieClipLoaded(MovieClip clip, string assetPath);
	public delegate void OnTotalProgressUpdate(float percentLoaded);
	public delegate void OnInitialAssetBundlesLoaded();
	
	public class AssetLoader : MonoBehaviour
	{
		private static AssetLoader instance;
		public static AssetLoader Instance
		{
			get
			{
				return instance;
			}
	
			set
			{
				instance = value;
			}
		}
		
		public event OnTotalProgressUpdate OnTotalProgressUpdateEvent;
		public event OnInitialAssetBundlesLoaded OnInitialAssetBundlesLoadedEvent;
		
		public bool loadAssetBundlesLocally = false;
		public bool clearCacheFirst = false;
		public Platforms editorPlatform = Platforms.WIN;
		
		private Dictionary<string, AssetBundleLoader> loadedBundels;
		private Dictionary<string, string> bundleVersions;
	
		public void Awake()
		{
			instance = this;
			loadedBundels = new Dictionary<string, AssetBundleLoader>();

			loadAssetBundlesLocally = !GlobalConfig.GetProperty<bool>("loadAssetsFromRMS");

			clearCacheFirst = GlobalConfig.GetProperty<bool>("clearAssetCache");
		}
		
		/// <summary>
		/// Gets the asset bundle.  Since the assetsbundles are unloaded after the inital upgrademanager fires, we have to get it out of cache / load it and then pass it back
		/// </summary>
		/// <returns>
		/// The asset bundle.
		/// </returns>
		/// <param name='bundleName'>
		/// Bundle name.
		/// </param>
		public AssetBundle GetAssetBundle(string bundleName)
		{
			if( loadedBundels.ContainsKey(bundleName) )
			{
				if( loadedBundels[bundleName].isDownloaded ) 
				{
					AssetBundleLoader abl = loadedBundels[bundleName];
					return abl.GetAssetBundleFromCache(GetBundlePath(abl.id));
				}
			}
			else Debug.LogError("BUNDLE NOT CACHED: " + bundleName);
			
			return null;
		}
		
		private void UnloadAssetBundles()
		{
			foreach(KeyValuePair<string, AssetBundleLoader> pair in loadedBundels)
			{
				pair.Value.UnloadAssetBundle();
			}
		}
		
		/// <summary>
		/// Loads the asset bundle list.  Called from UpgradeManager
		/// </summary>
		/// <param name='bundleList'>
		/// Bundle list.
		/// </param>
		public void LoadAssetBundleList(Dictionary<string, string> bundleList)
		{
			bundleVersions = bundleList;
			
			foreach(KeyValuePair<string, string> pair in bundleVersions)
			{
				CreateAssetBundle( pair.Key );
			}
			
			StartCoroutine( MonitorLoaderDownloads() );
		}
		
		private IEnumerator MonitorLoaderDownloads()
		{
			if( clearCacheFirst ) Caching.CleanCache();
			
			int bundleCount = loadedBundels.Count;
			int loadedCount = 0;
			float totalProgress = 0;
			float splice = 1 / (float)bundleCount;
			
			Debug.Log("ASSET LOADER: bundleCount: " + bundleCount + ", splicePercentage: " + splice);
			
			AssetBundleLoader currentAbl = GetNextNotLoaded();
			
			if( currentAbl == null )
			{
				Debug.LogError("THERE ARE NO ASSETS TO BE LOADED");
				yield break;
			}
			else
			{			
				while( loadedCount < bundleCount && currentAbl != null )
				{
					string fullBundleName = GetBundlePath(currentAbl.id);
					currentAbl.LoadAssetBundle(fullBundleName, currentAbl.id, bundleVersions[currentAbl.id.ToString()]);
					Debug.Log("++++ LOADING ABL: " + currentAbl.id.ToString());
					while( currentAbl.progress < 1 )
					{
						totalProgress = ((float)loadedCount * splice) + (currentAbl.progress * splice);
						Debug.Log("...PROGRESS OF " + currentAbl.id.ToString() + ": " + currentAbl.progress + ", totalProgress: " + totalProgress);
						if( OnTotalProgressUpdateEvent != null ) OnTotalProgressUpdateEvent(totalProgress);
						yield return new WaitForSeconds(.025f);
					}			

					currentAbl = GetNextNotLoaded();
					loadedCount ++;
					
					totalProgress = ((float)loadedCount * splice);
					if( OnTotalProgressUpdateEvent != null ) OnTotalProgressUpdateEvent(totalProgress);
					
					Debug.Log("NEXT ABL, LOADEDCOUNT: " + loadedCount);
					if( currentAbl == null ) break;
				}
				
				yield return new WaitForEndOfFrame();
				
				// unload the bundles now
				UnloadAssetBundles();
				
				// we're fully loaded at this point
				if( OnInitialAssetBundlesLoadedEvent != null ) OnInitialAssetBundlesLoadedEvent();
			}
		}
		
		private AssetBundleLoader GetNextNotLoaded()
		{
			foreach(KeyValuePair<string, AssetBundleLoader> pair in loadedBundels)
			{
				if( !pair.Value.isDownloaded ) return pair.Value;
			}
			
			return null;
		}
		
		public void CreateAssetBundle(string bundleName)
		{
			// create AssetBundleLoader and adds to the loadedBundles list for monitoring
			AssetBundleLoader abl = gameObject.AddComponent<AssetBundleLoader>();
			abl.id = bundleName;
			
			loadedBundels.Add(bundleName, abl);
		}
		
		public AssetBundleLoader LoadAssetBundle(string bundleName)
		{
			return LoadAssetBundle(bundleName, ""); 
		}
		
		public AssetBundleLoader LoadAssetBundle(string bundleName, string assetPath)
		{
			// create AssetBundleLoader and load
			string fullBundleName = GetBundlePath(bundleName);
			AssetBundleLoader abl = gameObject.AddComponent<AssetBundleLoader>();
			abl.LoadAssetBundle(fullBundleName, bundleName, bundleVersions[bundleName], assetPath);
			
			loadedBundels.Add(bundleName, abl);
			
			return abl;
		}
		
		private string GetBundlePath(string bundleName)
		{
			if( Application.isEditor ) return editorPlatform.ToString() + "_" + bundleName + "_" + bundleVersions[bundleName];
			return PlatformUtils.GetPlatform().ToString() + "_" + bundleName + "_" + bundleVersions[bundleName];
		}
		
		public AudioClip GetAudioClip(string bundleName, string assetPath)
		{
			AudioClip clip = null;
			
			var bundle = GetAssetBundle(bundleName);
			if(bundle != null) {
				clip = (AudioClip)bundle.Load(assetPath, typeof(AudioClip));
			}
			
			return clip;
		}
		
		public Texture2D GetTexture(string bundleName, string assetPath)
		{
			return null;
		}
		
		public void UnloadBundle(string bundleName)
		{
			foreach(KeyValuePair<string, AssetBundleLoader> pair in loadedBundels)
			{
				if( pair.Key == bundleName ) 
				{
					pair.Value.UnloadAssetBundle();
					break;
				}
			}
		}
		
		public Dictionary<string, string> GetBundleConfig() //get bundle names and version #s
		{
			return null;
		}
	}
}