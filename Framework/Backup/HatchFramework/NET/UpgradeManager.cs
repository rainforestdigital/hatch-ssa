using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

/// <summary>
/// Upgrade manager.  Responsible for managing version control of bundles and that application
/// </summary>
namespace HatchFramework
{
	public delegate void OnComplete(bool success);
	public delegate void OnConfigDownloadStart();
	public delegate void OnConfigDownloadComplete();
	
	public enum UpgradeModes:int{ CONFIG_DOWNLOAD, ASSETBUNDLE_DOWNLOAD };
	
	public class UpgradeManager
	{
		public event OnComplete OnCompleteEvent;
		//public event OnConfigDownloadStart OnConfigDownloadStartEvent;
		//public event OnConfigDownloadComplete OnConfigDownloadCompleteEvent;
		
		public UpgradeModes mode = UpgradeModes.CONFIG_DOWNLOAD;
		
		private GameObject gameObject;
		
		public UpgradeManager()
		{
			gameObject = new GameObject("UpgradeManagerCoroutineObject");
			gameObject.AddComponent<UpgradeManagerLoader>();
			gameObject.GetComponent<UpgradeManagerLoader>().OnConfigLoadedEvent += delegate(string xml) 
			{
				DoUpdates(xml);
			};
		
			//Create an assetloader if one has not been created yet
			if(AssetLoader.Instance == null){
				GameObject loader = new GameObject("AssetLoader");
				loader.AddComponent<AssetLoader>();
				
				// force clear cache for now
				AssetLoader.Instance.editorPlatform = Platforms.IOS;
			}
		}
		
		public void CheckForUpdates()
		{
			//TODO - send GetStoredVersions to the RMS server and wait response.  The next line simulates a response received.
			gameObject.GetComponent<UpgradeManagerLoader>().LoadXMLConfig();
		}
		
		public void DoUpdates(string xml)
		{			
			Debug.Log("XML: " + xml);
			
			Dictionary<string, string> bundlesToDownload = new Dictionary<string, string>();
			
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(xml);			
			XmlNodeList bundleList = xmlDoc.GetElementsByTagName("bundle");
			
			if( bundleList.Count > 0 )
			{
				XmlNodeList applicationList = xmlDoc.GetElementsByTagName("application");
				AssetLoader.Instance.clearCacheFirst = applicationList[0].Attributes["forceUpdate"].Value == "true" ? true : false;
			
				Dictionary<string, string> versionsFromRMSList = new Dictionary<string, string>();
				
				foreach (XmlNode bundleInfo in bundleList)
	  			{
					bundlesToDownload.Add(bundleInfo.Attributes["name"].Value, bundleInfo.Attributes["version"].Value);
				}
				
				Debug.Log("BUNDLE LIST LENGTH: " + bundleList.Count + ", forcupdate? " + applicationList[0].Attributes["forceUpdate"].Value);
							
				DownloadUpdatedAssetBundles(bundlesToDownload);
			}
			else
			{
				// if there are no bundles to update, just dispatch the loaded complete event to clean up and let the app move along
				HandleAssetLoaderInstanceOnInitialAssetBundlesLoadedEvent();
			}
		}
		
		private Dictionary<string, string> GetStoredVersions()
		{
			Dictionary<string, string> storedBundles = new Dictionary<string, string>();
			
			string bundleVersions = PlayerPrefs.GetString("BundleVersions");
			string[] bundles = bundleVersions.Split(',');
			
			foreach( string s in bundles )
			{
				string[] a = s.Split('|');
				storedBundles.Add(a[0], a[1]);
			}
			
			return storedBundles;
		}
		
		private void UpdateVersionsAndStore(Dictionary<string, string> bundles)
		{
			if( PlayerPrefs.HasKey("BundleVersions") )
			{
				// update and store
				Dictionary<string, string> storedVersions = GetStoredVersions();
				
				foreach( KeyValuePair<string,string> pair in bundles )
				{
					if( storedVersions.ContainsKey(pair.Key) ) storedVersions[pair.Key] = pair.Value;
				}
				
				string str = "";
				foreach( KeyValuePair<string,string> pair in storedVersions )
				{
					str += pair.Key + "|" + pair.Value + ",";
				}
				
				PlayerPrefs.SetString("BundleVersions", str);
			}
			else
			{
				string str = "";
				// this is the first load ever
				foreach( KeyValuePair<string,string> pair in bundles )
				{
					str += pair.Key + "|" + pair.Value + ",";
				}
				
				PlayerPrefs.SetString("BundleVersions", str);
			}
		}
		
		public void DownloadUpdatedAssetBundles(Dictionary<string, string> list) // bundleName, version
		{
			AssetLoader.Instance.OnInitialAssetBundlesLoadedEvent += HandleAssetLoaderInstanceOnInitialAssetBundlesLoadedEvent;
			AssetLoader.Instance.LoadAssetBundleList(list);
			mode = UpgradeModes.ASSETBUNDLE_DOWNLOAD;
		}
		
		void HandleAssetLoaderInstanceOnInitialAssetBundlesLoadedEvent ()
		{
			if( OnCompleteEvent != null ) OnCompleteEvent(true);
			GameObject.Destroy(gameObject);
		}
	}
}