using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate void OnUpdatesReceived(UpdatesDataObject data);

/// <summary>
/// Data manager. Responsible for communication with RMS in and out.
/// </summary>
public class DataManager : MonoBehaviour
{
	private static DataManager instance;
	public static DataManager Instance
	{
		get
		{
			return instance;
		}

		set
		{
			instance = value;
		}
	}

	public void Awake()
	{
		instance = this;
	}

	public void Start ()
	{

	}
	
	
	/// <summary>
	/// Gets the updates from RMS
	/// </summary>
	public void GetUpdates()
	{
		
	}
}

