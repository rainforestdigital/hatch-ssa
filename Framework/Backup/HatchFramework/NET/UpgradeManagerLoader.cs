using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace HatchFramework
{
	public delegate void OnConfigLoaded(string xml);
	public delegate void OnConfigLoadFail(string err);
	
	public class UpgradeManagerLoader : MonoBehaviour
	{
		private static UpgradeManagerLoader instance;
		public static UpgradeManagerLoader Instance
		{
			get
			{
				return instance;
			}
	
			set
			{
				instance = value;
			}
		}
		
		public event OnConfigLoaded OnConfigLoadedEvent;
		public event OnConfigLoadFail OnConfigLoadFailEvent;
		
		public void Awake()
		{
			instance = this;
		}
	
		public void Start ()
		{
			
		}
	
		public void LoadXMLConfig()
		{
			Object o = Resources.Load("Config/RMSResponse");
			
			if( o != null)
			{
				TextAsset txt = GameObject.Instantiate(o) as TextAsset;
				if( OnConfigLoadedEvent != null ) OnConfigLoadedEvent( txt.text );	
			}
			else
			{
				Debug.LogWarning("NO XML FOUND FOR THIS APPLICATION -- LOCATING LOCAL RMSResponse.xml");	
				if( OnConfigLoadFailEvent != null ) OnConfigLoadFailEvent("NO XML FOUND FOR THIS APPLICATION -- LOCATING LOCAL RMSResponse.xml");
			}
			
			//StartCoroutine( DoLoadXMLConfig() );
		}
		
		private WWW www = null;
		private IEnumerator DoLoadXMLConfig()
		{
			//if( OnConfigDownloadStartEvent != null ) OnConfigDownloadStartEvent();
			
			www = new WWW("http://www.rockonflash.com/infrared5/clients/hatch/RMSResponse.xml");
			
			yield return www;
			
			if( !string.IsNullOrEmpty(www.error) )
			{
				Debug.LogError("WWW ERROR WHILE LOADING CONFIG FROM RMS: " + www.error);
			}
			else
			{
				
			}
			
		}
	}
}
