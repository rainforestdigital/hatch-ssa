using System;
using UnityEngine;
using System.Collections;

namespace HatchFramework
{
	public delegate void DownloadComplete();
	public class DownloadConfig : MonoBehaviour
	{
		public string URL = "http://www.google.com";
		public string config = "";
		public string error = "";
		public event DownloadComplete OnDownloadComplete;
		
		public DownloadConfig ()
		{
			
		}
		
		public void GetConfig(){
		
			Debug.Log ("Getting config");
			StartCoroutine(DownloadConfigFile());
		}
		
		public IEnumerator DownloadConfigFile(){
		
			WWW www = new WWW(URL);
			yield return www;
			error = www.error;
			config = www.text;
			
			Debug.Log (config);
			if(OnDownloadComplete != null)
				OnDownloadComplete();
				
			
		}
		
		
	}
}

