using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Asset bundle loader.  Responsible for managing the loading and extraction of bundle assets
/// </summary>
namespace HatchFramework
{
	public delegate void OnAssetBundleLoadProgress(float percentage);
	public delegate void OnAssetBundleLoadComplete(AssetBundle bundle, string bundleName, string assetPath);
	
	public class AssetBundleLoader : MonoBehaviour
	{
		public event OnAssetBundleLoadProgress OnAssetBundleLoadProgressEvent;
		public event OnAssetBundleLoadComplete OnAssetBundleLoadCompleteEvent;
		
		public string id = "GlobalAssets";
		public AssetBundle bundle;
		public int version;
		
		public float progress = 0;
		
		public bool isDownloadingBundle = false;
		public bool isDownloaded = false;
		
		public WWW www;
		
		private string wwwPath = "http://www.rockonflash.com/infrared5/clients/hatch/";
		private string localPath;
		
		public void Awake()
		{
			localPath = "file://" + PlatformUtils.GetBundlePathForProject();
		}
		
		private int GetVersionFromString(string version)
		{
			return int.Parse(string.Join("", version.Split('_')));
		}
		
		public void UnloadAssetBundle()
		{
			if( bundle != null ) bundle.Unload(false);
		}
		
		/// <summary>
		/// Gets the asset bundle from cache.  This only works if it's been cached
		/// </summary>
		/// <returns>
		/// The asset bundle from cache.
		/// </returns>
		/// <param name='fullBundleName'>
		/// Full bundle name.
		/// </param>
		public AssetBundle GetAssetBundleFromCache(string fullBundleName)
		{
			if( bundle != null ) return bundle;
			
			string pathToSWF = (AssetLoader.Instance.loadAssetBundlesLocally ? localPath : wwwPath) + fullBundleName + ".unity3d";
			
			www = WWW.LoadFromCacheOrDownload(pathToSWF, version);
			
			bundle = www.assetBundle;
			
			return bundle;
		}
		
		public void LoadAssetBundle(string fullBundleName, string bundleName, string version, string assetPath = "")
		{			
			StartCoroutine( DoLoadAssetBundle(fullBundleName, bundleName, GetVersionFromString(version), assetPath) );
		}
		
		private IEnumerator DoLoadAssetBundle(string fullBundleName, string bundleName, int version, string assetPath)
		{
			id = bundleName;
			this.version = version;
			isDownloadingBundle = true;
			isDownloaded = false;
			
			string pathToSWF = (AssetLoader.Instance.loadAssetBundlesLocally ? localPath : wwwPath) + fullBundleName + ".unity3d";
			
			www = WWW.LoadFromCacheOrDownload(pathToSWF, version);
			
			StartCoroutine( DoProgressCheck() );
			
			yield return www;			
			
			if( !string.IsNullOrEmpty(www.error) )
			{
				Debug.LogError("WWW ERROR WHILE LOADING "+ pathToSWF + " : " + www.error);
			}
			else
			{
				bundle = www.assetBundle;
				isDownloadingBundle = false;
				isDownloaded = true;
				
				Debug.Log("ASSETS LOADED FROM " + pathToSWF + " : " + id.ToString());
				if( OnAssetBundleLoadCompleteEvent != null ) OnAssetBundleLoadCompleteEvent(bundle, bundleName, assetPath);
			}
		}
		
		private IEnumerator DoProgressCheck()
		{
			while( !www.isDone && isDownloadingBundle && www.progress < 1 )
			{	
				if( OnAssetBundleLoadProgressEvent != null ) OnAssetBundleLoadProgressEvent(www.progress);
				progress = www.progress;
				Debug.Log("loading progress: " + www.progress);
				yield return new WaitForSeconds(.025f);
			}	
			
			progress = 1;
			isDownloaded = true;
			
			// we pass this so that it gets the final "1" as in full load 
			if( OnAssetBundleLoadProgressEvent != null ) OnAssetBundleLoadProgressEvent(1f);
		}
	}
}