using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace HatchFramework
{
	public enum Platforms:int
	{
		IOS,
		IOSRETINA,
		ANDROID,
		WIN
	}
	
	public class PlatformUtils : MonoBehaviour
	{
		public static Platforms GetPlatform()
		{
			switch(Application.platform)
			{
				case RuntimePlatform.Android:
					return Platforms.ANDROID;
				
				case RuntimePlatform.WindowsPlayer:
					return Platforms.WIN;			
				
				case RuntimePlatform.IPhonePlayer:
					if( Screen.width > 1024 ) return Platforms.IOSRETINA;
					else return Platforms.IOS;			
			}
			
			return Platforms.WIN;
		}
		
		public static string GetBundlePathForProject()
		{
			// /Users/NeoRiley/Documents/Infrared5/clients/Hatch/UnityConversion/Project/Apps/Loader/Assets
			int appsLoc = Application.dataPath.IndexOf("Apps");
			string path = Application.dataPath.Substring(0, appsLoc) + "AssetBundles/";
			Debug.Log("path : " + path);
			return path;
		}
	}
}