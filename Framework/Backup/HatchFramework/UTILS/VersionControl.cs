using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Versioning;

namespace HatchFramework
{
	public class VersionControl : MonoBehaviour 
	{
		//public static Version CurrentVersion{ get { return new Version(GlobalConfig.GetProperty("version"));}}
		public static Version CurrentVersion{ get { return new Version("0.0.0.0");}}
		
		public static int CurrentVersionInt
		{
			get
			{
				return int.Parse (VersionControl.CurrentVersion.Major+""+VersionControl.CurrentVersion.Minor+""+VersionControl.CurrentVersion.Build+""+(int)VersionControl.CurrentVersion.MinorRevision);	
			}
		}
	}
}