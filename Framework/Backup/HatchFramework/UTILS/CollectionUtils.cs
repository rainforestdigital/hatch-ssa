using System;
using System.Collections.Generic;

namespace HatchFramework
{
	public static class CollectionUtils
	{
		public static void Shuffle<T>(this IList<T> list)
		{
			// standard Fisher-Yates shuffle
			var rand = new Random();
			for(int i = list.Count-1; i > 0; --i)
			{
				int j = rand.Next(0, i+1);
				var temp = list[i];
				list[i] = list[j];
				list[j] = temp;
			}
		}
		
		public static IList<T> RandomSample<T>(this ICollection<T> list, int sampleSize)
		{
			// Use a reservoir sample to select K elements
			List<T> result = new List<T>(sampleSize);
				
			var rand = new Random();
			int index = 0;
			foreach(var elem in list)
			{
				if(index < sampleSize)
				{
					result.Add(elem);
				}
				else
				{
					int j = rand.Next(0, index+1);
					if(j < sampleSize)
					{
						result[j] = elem;
					}
				}
				index++;
			}
			
			// This algorithm generates a random sample, but the order
			// is not guaranteed to be random, especially when N < k,
			// so explicitly shuffle the results
			result.Shuffle();
			
			return result;
		}
		
		public static IList<T> RandomSampleWith<T>(this ICollection<T> list, int sampleSize, T ensure)
		{
			var result = list.RandomSample(sampleSize);
			if(!result.Contains(ensure)) 
			{
				var rand = new Random();
				result[rand.Next(result.Count)] = ensure;
			}
			
			return result;
		}
	}
}

