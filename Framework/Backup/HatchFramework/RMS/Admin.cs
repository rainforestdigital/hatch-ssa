﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace HatchFramework.RMS
{
	public class Admin
	{
		// An event for clients using admin to know when it's closed
		public Action ExitedEvent;
		private BrowserHandlerBase browser;
		private string defaultURL = "https://rmsdev.hatchearlychildhood.com/ssg.php?path=/report/classoverview";
		
		public Admin ()
		{
			switch (Application.platform) {
				
			case RuntimePlatform.Android:
				browser = new BrowserHandlerAndroid ();
				break;
			case RuntimePlatform.IPhonePlayer:
				browser = new BrowserHandleriOS ();
				break;
			case RuntimePlatform.WindowsEditor:
			case RuntimePlatform.WindowsPlayer:
				browser = new BrowserHandlerWindows ();
				break;
			case RuntimePlatform.OSXEditor:
			case RuntimePlatform.OSXPlayer:
				browser = new BrowserHandlerOSX ();
				break;
			default:
				Debug.Log ("RMS.Admin: platform unimplemented");
				browser = new BrowserHandlerBase ();
				break;
			}
			browser.ExitedEvent += NotifyAdminExited;
		}
		
		void NotifyAdminExited(){
			if(ExitedEvent != null){
				ExitedEvent();	
			}
		}
		
	
		public bool Open ()
		{
			return browser.OpenURL (defaultURL);
		}

		public bool IsOpen ()
		{
			return browser.IsOpen();
		}
		
		public bool Close(){
			return browser.Close();	
		}
	}
}
