using System;
using UnityEngine;


namespace HatchFramework.RMS
{
	public class BrowserHandlerWindows : BrowserHandlerBase
	{
		System.Diagnostics.Process self;
		System.Diagnostics.Process browserProcess;
		private bool adminOpen = false;
		private bool goFullscreen = false;
		
		public override bool OpenURL (string url)
		{
			self = System.Diagnostics.Process.GetCurrentProcess ();
			browserProcess = new System.Diagnostics.Process ();
			browserProcess.EnableRaisingEvents = true;
			browserProcess.StartInfo.FileName = "Chromium\\Chrome.exe";
			browserProcess.StartInfo.Arguments = "google.com --kiosk";
			browserProcess.Exited += BrowserExited;
			
			adminOpen = true;
			browserProcess.Start ();
			Screen.fullScreen = false;
			return true;
		}
		
		void BrowserExited (object obj, System.EventArgs args)
		{
			Debug.Log ("[rms admin] Browser Exited");
			if (ExitedEvent != null) {
				ExitedEvent ();	
				goFullscreen = true;
			}
			browserProcess.Exited -= BrowserExited;
		}
	
		
		public override void Update ()
		{
			// We can't modify fullScreen in a callback outside of the 
			// main thread
			if (goFullscreen) {
				goFullscreen = false;
				Screen.fullScreen = true;
				Debug.Log ("Going full screen");
			}
		}
		
		public override bool IsOpen ()
		{
			return adminOpen;
		}
		
	}
}

