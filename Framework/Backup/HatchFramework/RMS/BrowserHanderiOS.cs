using System;
using UnityEngine;


namespace HatchFramework.RMS

{
	public class BrowserHandleriOS : BrowserHandlerBase
	{
		private WebViewObjectiOS webViewObject;

			public BrowserHandleriOS ()
		{
#if !UNITY_IPHONE
			Debug.Log ("HatchFramework DLL was not built with iOS support");		
#endif
		}
		
		public override bool OpenURL (string url)
		{
			webViewObject =
			(new GameObject ("WebViewObjectiOS")).AddComponent<WebViewObjectiOS> ();
			webViewObject.Init ((msg) => {
				Debug.Log (string.Format ("CallFromJS[{0}]", msg));
			});

			webViewObject.LoadURL (url);
			webViewObject.SetVisibility (true);
			return true;
		}
	}
}

