using System;
using UnityEngine;

namespace HatchFramework.RMS
{
	public class BrowserHandlerAndroid : BrowserHandlerBase
	{
#if UNITY_ANDROID
		private WebViewObjectAndroid webViewObject;
#endif		
		public BrowserHandlerAndroid ()
		{
#if !UNITY_ANDROID
			Debug.Log ("HatchFramework DLL was not built with Android support");		
#endif
		}
			
		public override bool OpenURL (string url)
		{
#if UNITY_ANDROID
			webViewObject =
			(new GameObject ("WebViewObjectAndroid")).AddComponent<WebViewObjectAndroid> ();
			webViewObject.Init ((msg) => {
				Debug.Log (string.Format ("CallFromJS[{0}]", msg));
			});

			webViewObject.LoadURL (url);
			webViewObject.SetVisibility (true);
			return true;
#else
			return false;
#endif
		}
	}
}

