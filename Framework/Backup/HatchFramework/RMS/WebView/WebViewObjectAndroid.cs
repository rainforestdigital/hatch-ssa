#if UNITY_ANDROID
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Callback = System.Action<string>;

namespace HatchFramework.RMS
{
	public class WebViewObjectAndroid : MonoBehaviour
	{
	
		Callback callback;
		AndroidJavaObject webView;
		Vector2 offset;

		public void Init (Callback cb = null)
		{
			callback = cb;
			offset = new Vector2 (0, 0);

			webView = new AndroidJavaObject ("net.gree.unitywebview.WebViewPlugin");
			webView.Call ("Init", name);
		}

		void OnDestroy ()
		{
			if (webView == null)
				return;
			webView.Call ("Destroy");
		}

		public void SetMargins (int left, int top, int right, int bottom)
		{

			if (webView == null)
				return;
			offset = new Vector2 (left, top);
			webView.Call ("SetMargins", left, top, right, bottom);
		}

		public void SetVisibility (bool v)
		{

			if (webView == null)
				return;
			webView.Call ("SetVisibility", v);
		}

		public void LoadURL (string url)
		{

			if (webView == null)
				return;
			webView.Call ("LoadURL", url);
		}

		public void EvaluateJS (string js)
		{

			if (webView == null)
				return;
			webView.Call ("LoadURL", "javascript:" + js);
		}

		public void CallFromJS (string message)
		{
			if (callback != null)
				callback (message);
		}

	}

}
#endif


