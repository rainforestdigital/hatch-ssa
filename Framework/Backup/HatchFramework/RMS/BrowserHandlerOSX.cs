using System;
using UnityEngine;


namespace HatchFramework.RMS

{
	public class BrowserHandlerOSX : BrowserHandlerBase
	{
		private WebViewObjectOSX webViewObject;
		

		public override bool OpenURL (string url)
		{
			webViewObject =
			(new GameObject ("WebViewObjectOSX")).AddComponent<WebViewObjectOSX> ();
			webViewObject.Init ((msg) => {
				Debug.Log (string.Format ("CallFromJS[{0}]", msg));
			});
			
			Debug.Log("Loading url: "+url);
			Debug.Log(webViewObject);
			webViewObject.LoadURL (url);
			webViewObject.SetVisibility (true);
			return true;
		}
		
		public override bool IsOpen ()
		{
			if(webViewObject != null){
				return webViewObject.visibility;
			} 
			return false;
		}
		
		public override bool Close ()
		{
			webViewObject.SetVisibility(false);
			return true;
		}
	}
}

