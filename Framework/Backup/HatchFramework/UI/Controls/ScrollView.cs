using UnityEngine;
using System.Collections;

using pumpkin;
using pumpkin.display;
using pumpkin.events;


namespace HatchFramework
{

	public class ScrollView : DisplayObjectContainer
	{
		
		protected DisplayObjectContainer content_mc;
		
		protected DisplayObjectContainer container_mc;
		
		protected Rect _mask;
				
		/// <summary>
		/// Initializes a new instance of the <see cref="ScrollView"/> class.
		/// </summary>
		/// <param name='content'>
		/// Content to be scrollable. The mask is created based on this MovieClip's size
		/// </param>
		public ScrollView( DisplayObjectContainer content ) : base( )
		{
			content_mc = content;
			_mask = new Rect(content.x, content.y, content.width, content.height);
			init();
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ScrollView"/> class.
		/// </summary>
		/// <param name='content'>
		/// Content to be scrollable.
		/// </param>
		/// <param name='mask'>
		/// Mask rect.
		/// </param>
		public ScrollView( DisplayObjectContainer content, Rect _mask ) : base(  )
		{
			content_mc = content;
			this._mask = new Rect( _mask.x, _mask.y, _mask.width, _mask.height );
			init();
		}
			
		private void init()
		{
			
			container_mc = new DisplayObjectContainer();
			
			addChild( container_mc );
			container_mc.addChild( content_mc );
								
			container_mc.addEventListener( MouseEvent.MOUSE_DOWN, onListDown );
			addEventListener( MouseEvent.MOUSE_UP, onListUp );
			
			container_mc.clipRect = _mask;
			
		}
		
		private void update( CEvent e )
		{
			float containerPos = Mathf.Clamp(mouseY - mouseDistance, -(_mask.height), 0);
			content_mc.y = containerPos;
		}
		
		private float mouseDistance;
		private void onListDown( CEvent e )
		{
			mouseDistance = mouseY - content_mc.y;
			addEventListener( CEvent.ENTER_FRAME, update );
		}
		
		private void onListUp( CEvent e )
		{
			removeEventListener( CEvent.ENTER_FRAME, update );
		}
		
		public void SetScrollValue( float val )
		{
			content_mc.y = 0 - (content_mc.srcHeight * val);
		}
		
	}
	
}

