using UnityEngine;
using System.Collections;

using pumpkin;
using pumpkin.display;
using pumpkin.events;


namespace HatchFramework
{
	
	public class BasicButton : MovieClip
	{
		
		
		protected MovieClip root;
		/// <summary>
		/// Gets the root movieclip.
		/// </summary>
		/// <value>
		/// Used only if a MovieClip instance was passed to the constructor
		/// </value>
		public MovieClip Root
		{
			get
			{
				return root;
			}
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="BasicButton"/> class.
		/// </summary>
		/// <param name='linkage'>
		/// Full swf and Symbolname string.
		/// </param>
		public BasicButton( string linkage ) : base( linkage )
		{
			
			root = this;
			
			init();
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="BasicButton"/> class.
		/// </summary>
		/// <param name='swf'>
		/// Swf.
		/// </param>
		/// <param name='symbolname'>
		/// Symbolname.
		/// </param>
		public BasicButton( string swf, string symbolname ) : base( swf, symbolname )
		{
			
			root = this;
			init();
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="BasicButton"/> class.
		/// </summary>
		/// <param name='rootMC'>
		/// When passing <see cref="BasicButton"/> a MovieClip class as the constructor param, use '.Root' when altering the base MovieClip.
		/// Example: yourInstance.Root.visible = true;
		/// </param>
		public BasicButton( MovieClip root ) : base()
		{
			
			this.root = root;
			this.root.mouseEnabled = true;
			init();
			
		}
		
		protected virtual void init()
		{
			root.gotoAndStop(1);
			root.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			root.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			root.addEventListener(MouseEvent.MOUSE_ENTER, onMouseOver);
			root.addEventListener(MouseEvent.MOUSE_LEAVE, onMouseOut);
			root.mouseChildrenEnabled = false;
		}
		
		protected virtual void onMouseDown( CEvent e )
		{
			root.gotoAndStop("down");
		}
		
		protected virtual void onMouseUp( CEvent e )
		{
			root.gotoAndStop("up");
		}
		
		protected virtual void onMouseOver( CEvent e )
		{
			root.gotoAndStop("over");
		}
		
		protected virtual void onMouseOut( CEvent e )
		{
			root.gotoAndStop("up");
		}
		
		
	}
}
