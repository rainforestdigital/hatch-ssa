using UnityEngine;
using System.Collections;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.text;

namespace HatchFramework
{
	public class DropDownListItem : BasicButton
	{
		
		private DropDown _parent;
		
		private TextField label_txt;
		
		public string Label
		{
			get{ return label_txt.text; }
			
			set
			{
				label_txt.text = value;
			}
			
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="DropDownListItem"/> class.
		/// </summary>
		/// <param name='linkage'>
		/// Linkage.
		/// </param>
		/// <param name='_parent'>
		/// parent <see cref="DropDown"/> object that this list item is attached to.
		/// </param>
		public DropDownListItem( string linkage, DropDown _parent  ) : base( linkage )
		{
			root = this;
			this._parent = _parent;
			init();
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="DropDownListItem"/> class.
		/// </summary>
		/// <param name='swf'>
		/// Swf.
		/// </param>
		/// <param name='symbolName'>
		/// Symbol name.
		/// </param>
		/// <param name='_parent'>
		///parent <see cref="DropDown"/> object that this list item is attached to.
		/// </param>
		public DropDownListItem( string swf, string symbolName, DropDown _parent  ) : base( swf, symbolName )
		{
			root = this;
			this._parent = _parent;
			init();
		}
		
		protected override void init()
		{
			
			base.init();
			
			label_txt = getChildByName<TextField>( "label_txt" );
			
		}
		
		private float mouseYPosition;
		protected override void onMouseDown( CEvent e )
		{
			mouseYPosition = stage.mouseY;
		}
		
		protected override void onMouseUp( CEvent e )
		{		
			float mathDistance = Mathf.Abs(stage.mouseY - mouseYPosition);
			if(	mathDistance <= 2)
				_parent.Label = Label;
		}
		
		
	}
}
