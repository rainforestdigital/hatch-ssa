using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.text;

namespace HatchFramework
{
	
	public class DropDown : MovieClip
	{
		
		private string swfName;
		private string listItemName;
		private int maxViewableItems;
		private string[] items;
		
		private List<DropDownListItem> item_mcs;
		
		private DisplayObjectContainer contentContainer;
		private MovieClip base_mc;
		private TextField label_txt;
		public string Label
		{
			get{ 
				return label_txt.text; 
			}
			
			set
			{
				isOpen = false;
				label_txt.text = value;
				dispatchEvent( new CEvent(CEvent.CHANGED) );
			}
			
		}
		
		private Rect listMask;
		
		private ScrollView scrollView;
		
		private bool _isOpen = false;
		public bool isOpen
		{
			get{ return _isOpen; }
			
			set
			{
				_isOpen = value;
				updateListItems();
			}
		}
		
		public DropDown(string swf, string symbolname, string listItemName, int maxViewableItems, params string[] items) : base( swf, symbolname )
		{
			swfName = swf;
			this.items = items;
			this.listItemName = listItemName;
			this.maxViewableItems = maxViewableItems;
			init();
		}
		
		public DropDown(string swf, string symbolname, string listItemName, int maxViewableItems, List<string> items) : base( swf, symbolname )
		{
			swfName = swf;
			this.items = items.ToArray();
			this.listItemName = listItemName;
			this.maxViewableItems = maxViewableItems;
			init();
		}
		
		private void init()
		{
			
			item_mcs = new List<DropDownListItem>();
			
			contentContainer = new DisplayObjectContainer();
			
			base_mc = getChildByName<MovieClip>( "base" );
			label_txt = getChildByName<TextField>( "label_txt" );
			
			for(int i = 0; i < items.Length; i++)
			{
				DropDownListItem newItem = new DropDownListItem( swfName, listItemName, this );
				newItem.Label = items[i];
				item_mcs.Add( newItem );
				newItem.y = this.height + (i * newItem.height);
				contentContainer.addChild(newItem);
			}
			
			listMask = new Rect( 0, item_mcs[0].y, item_mcs[0].width, item_mcs[0].height * maxViewableItems );
			
			scrollView = new ScrollView( contentContainer, listMask );
			scrollView.visible = isOpen;
			addChild( scrollView );
						
			base_mc.addEventListener( MouseEvent.MOUSE_DOWN, onMouseDown );
			base_mc.addEventListener( MouseEvent.MOUSE_UP, onMouseUp );
			
		}
		
		private float mouseDownTime;
		private void onMouseDown( CEvent e )
		{
			mouseDownTime = Time.time;
		}
		
		private void onMouseUp( CEvent e )
		{
			mouseDownTime = Time.time - mouseDownTime;
			if(mouseDownTime < 0.2f) isOpen = !isOpen;
		}
		
		private void updateListItems()
		{
			scrollView.visible = _isOpen;
		}
		
	}
	
} 
