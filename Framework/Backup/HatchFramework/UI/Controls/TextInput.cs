using UnityEngine;
using System.Collections;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.text;

namespace HatchFramework
{
	public class TextInput : MovieClip
	{ 
		
		private TextField textField;
		
		private bool hasFocus = false;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="HatchFramework.TextInput"/> class.
		/// Used to activate mobile platform's native keyboard when activating input fields.
		/// </summary>
		/// <param name='tf'>
		/// The UniSWF <see cref="TextField"/> input object to use for the keyboard input.
		/// </param>
		/// <param name='type'>
		/// TouchScreenKeyboardType.
		/// </param>
		/// <param name='keyboard'>
		/// TouchScreenKeyboard object.
		/// </param>
#if UNITY_IPHONE || UNITY_ANDROID
		
		private TouchScreenKeyboardType keyboardType;
		private TouchScreenKeyboard keyboard;
		
		public TextInput( TextField tf, TouchScreenKeyboardType type, TouchScreenKeyboard keyboard ) : base()
		{
			textField = tf;
			keyboardType = type;
			this.keyboard = keyboard;
			init();
	
		}
		
		private void onFocusIn( CEvent e )
		{
			
			if(hasFocus) return;
			
			hasFocus = true;
			
			keyboard = TouchScreenKeyboard.Open( textField.text, keyboardType );
			keyboard.text = textField.text;
			textField.addEventListener( CEvent.ENTER_FRAME, update );
			
		}
		
#else
		public TextInput( TextField tf ) : base()
		{
			textField = tf;
			init();
	
		}
		
		private void onFocusIn( CEvent e )
		{
			
			if(hasFocus) return;
			
			hasFocus = true;
		}
		
#endif
		
		private void init()
		{
			textField.addEventListener( FocusEvent.FOCUS_IN, onFocusIn );
			textField.addEventListener( FocusEvent.FOCUS_OUT, onFocusOut );
		}
		
		/// <summary>
		/// Event Handler for when textfield gains focus. 
		/// If the textfield doesn't already have focus, it checks the current application platform,
		/// and sets the keyboard ONLY if it is on Android or iOS.
		/// </summary>
		
		/// <summary>
		/// Event Handler for when textfield loses focus.
		/// </summary>
		private void onFocusOut( CEvent e )
		{
			hasFocus = false;
			
			Debug.Log("Focus Out");
#if UNITY_IPHONE || UNITY_ANDROID
			textField.removeEventListener( CEvent.ENTER_FRAME, update );
			keyboard = null;
#endif			
			
		}
		
		/// <summary>
		/// Updates the textfield to the keyboard's text.
		/// </summary>
		private void update( CEvent e )
		{
#if UNITY_IPHONE || UNITY_ANDROID
			if( keyboard != null)
				textField.text = keyboard.text;
#endif
		}
		
	}
}
