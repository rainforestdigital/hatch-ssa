using UnityEngine;
using System.Collections;

using pumpkin;
using pumpkin.display;
using pumpkin.events;

namespace HatchFramework
{ 
	public class ScrollBar : MovieClip
	{
		
		private ScrollView view;
		
		private string knobName = "knob";
		private string trackName = "track";
		private string upArrowName = "arrowUpBtn_mc";
		private string downArrowName = "arrowDownBtn_mc";
		
		private BasicButton arrowUpBtn_mc;
		private BasicButton arrowDownBtn_mc;
		
		private BasicButton knob;
		
		private MovieClip track;
		
		private float track_top;
		private float track_bottom;
		
		private float percentage;
		public float Percentage
		{
			get{ return percentage; }
			private set
			{
				percentage = value;
				view.SetScrollValue(percentage);
			}
		}
		
		public ScrollBar( string linkage ) : base( linkage )
		{
			
		}
		
		public ScrollBar( string swf, string symbolName ) : base( swf, symbolName)
		{
			
		}
		
		public ScrollBar( string swf, string symbolName, string _knobName, string _trackName, string _upArrowName, string _downArrowName ) : base( swf, symbolName)
		{
			knobName = _knobName;
			trackName = _trackName;
			upArrowName = _upArrowName;
			downArrowName = _downArrowName;
		}
		
		public void Init( ScrollView view )
		{
			
			this.view = view;
			
			arrowUpBtn_mc = new BasicButton( getChildByName<MovieClip>( upArrowName ) );
			arrowUpBtn_mc.Root.addEventListener( MouseEvent.CLICK, onUpArrowClick);
			
			arrowDownBtn_mc = new BasicButton( getChildByName<MovieClip>( downArrowName ) );
			arrowDownBtn_mc.Root.addEventListener( MouseEvent.CLICK, onDownArrowClick);
			
			knob = new BasicButton( getChildByName<MovieClip>( knobName ) );
			
			track = getChildByName<MovieClip>( trackName );
					
			knob.Root.addEventListener( MouseEvent.MOUSE_DOWN, onKnobDown );
			knob.Root.addEventListener( MouseEvent.MOUSE_UP, onKnobUp );
			
			track_top = track.y + 5;
			track_bottom = (track.y + track.height) - knob.Root.height - 5;
			
			knob.Root.y = track_top;
			
		}
		
		private void update( CEvent e )
		{
			float knobPos = Mathf.Clamp(mouseY - knobDistance, track_top, track_bottom);
			Percentage = (knobPos - track_top) / (track_bottom - track_top);
			knob.Root.y = knobPos;
		}
		
		private float knobDistance; // Used to calulate mouse distance at time of click. This keeps the slider knob from jumping to the mouse position.
		private void onKnobDown( CEvent e )
		{
			knobDistance = mouseY - knob.Root.y;
			addEventListener( CEvent.ENTER_FRAME, update );
		}
		
		private void onKnobUp( CEvent e )
		{
			removeEventListener( CEvent.ENTER_FRAME, update );
		}
		
		private int jumpAmt = 30; // The amount the scroll value will change every arrow press
		private void onUpArrowClick( CEvent e )
		{
			float knobPos = Mathf.Clamp(knob.Root.y - jumpAmt, track_top, track_bottom);
			Percentage = (knobPos - track_top) / (track_bottom - track_top);
			knob.Root.y = knobPos;
		}
		
		private void onDownArrowClick( CEvent e )
		{
			float knobPos = Mathf.Clamp(knob.Root.y + jumpAmt, track_top, track_bottom);
			Percentage = (knobPos - track_top) / (track_bottom - track_top);
			knob.Root.y = knobPos;
		}
		
	}
}
