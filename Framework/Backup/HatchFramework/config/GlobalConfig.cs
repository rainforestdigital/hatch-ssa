using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace HatchFramework
{
public class GlobalConfig 
{
	/* HOW TO SETUP CONFIG FILE
	 * 
	 * 1 KVP per line
	 * 
	 * //comments
	 * #regions
	 * [propertyname] = [value]
	 * 
	 * Currently supported properties:
	 * username		//string
	 * password		//string... you know
	 * allowOffline //bool - allow outside of facebook
	 * QA			//bool - force QA mode
	 * */

	protected static TextAsset configFile;
	protected static Dictionary<string, string> configs = new Dictionary<string, string>();
	
	
	protected static void GetConfig(){
		
		if(configFile != null)
			return;
		
		Object o = Resources.Load("Config/BuildConfig");
		
		
		
		if( o != null){
			configFile = GameObject.Instantiate(o) as TextAsset;	
			ReadConfig(configFile);
			return;
			
		}else{
			Debug.LogWarning("NO CONFIG FOUND FOR THIS APPLICATION -- LOCATING LOCAL CONFIG");	
		}
		
		
		o = Resources.Load("Config/LocalConfig");
		if(o != null){
			ReadConfig(GameObject.Instantiate(o) as TextAsset);
		}else{
			Debug.Log ("NO CONFIG FOUND FOR LOCAL FILE");		
		}
		
		
	}
	
	public static void ReadConfig(TextAsset configFile){
		string[] items = configFile.text.Split('\n');
			
			//build the list from the variables listed
			foreach(string item in items){
				if(item.IndexOf("//") == 0 || item.IndexOf("#") == 0 || item.Trim().Length < 1) continue;
				string[] props = item.Split('=');	
				if(props.Length < 2){
					Debug.LogError("MALFORMED CONFIG FILE - please adjust to [property]=[value]");
					continue;
				}
				configs[props[0].Trim()] =   props[1].Trim();
				
			}
	}
	
	
	public static bool HasConfig(){
	
		GetConfig();
		
		if(configFile != null)
			return true;
			
		return false;
	}
	
	public static string GetProperty(string property){
		
		GetConfig();
		
		if(configFile != null){
			if(configs.ContainsKey(property))
				return configs[property];	
			else
				return null;
		}
		
		return null;
	}
	
	public static T GetProperty<T>(string property){
		
		string val = GetProperty(property);
		if(string.IsNullOrEmpty(val))
			return default(T);
		
		return (T)System.Convert.ChangeType(val, typeof(T));
	}
	
	
}


}

