using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace HatchFramework
{
	public delegate void OnAudioWordEvent(string eventID);
	
	public class SoundEngine : MonoBehaviour
	{
		
		//TODO: - ADRIEL - Add OnAudioComplete
		//TODO: - ADRIEL - Add OnAudioStart
		public event OnAudioWordEvent OnAudioWord;
		
		protected static SoundEngine instance;
		
		
		private CancelToken cancelToken = new CancelToken();
		
		public static SoundEngine Instance{
			get{
				if(instance == null){
					GameObject soundManager = new GameObject("Sound Manager");
					soundManager.AddComponent<AudioSource>();
					GameObject.DontDestroyOnLoad(soundManager);
					instance = soundManager.AddComponent<SoundEngine>();
					
				}
				return instance;
			}
		}
		
		public void StopAll()
		{
			audio.Stop();
			cancelToken.Cancel();
			cancelToken = new CancelToken();
		}
		
		
	
		/// <summary>
		/// Sound bundle. - Stores list of clips to play in order and events to dispatch REALTIVE to the clip index
		/// 
		/// SOundBundle.AddClip(word1);
		/// SoundBundle.AddCLip(word2_door, "DOOR", 1.5f);
		/// SoundBundle.AddCLip(word3);
		/// 
		/// </summary>
		public class SoundBundle{
			
			public class ClipInfo{
			
				public AudioClip clip;
				public float eventDispatchDelay = -1;
				public string eventToDispatch;
				
				
				
				public ClipInfo(){}
				
				public ClipInfo(AudioClip clip){
					this.clip = clip;	
				}
				
				public ClipInfo(AudioClip clip, float delay, string eventToDispatch){
				
					this.clip = clip;
					this.eventDispatchDelay = delay;
					this.eventToDispatch = eventToDispatch;
				}
				
				
				
			}
			
			public List<ClipInfo> clips = new List<ClipInfo>();

			
			public void AddClip(AudioClip clipToPlayNext){
				ClipInfo newClip = new ClipInfo(clipToPlayNext);
				clips.Add(newClip);
			}
			
			public void AddClip(AudioClip clipToPlayNext, string eventName, float delayFromClipStart){
				ClipInfo newClip = new ClipInfo(clipToPlayNext, delayFromClipStart, eventName);
				clips.Add(newClip);
			}
			
		}
		
		
		public void PlayBundle(SoundBundle bundle){
			StartCoroutine(PlaySoundBundle(bundle, cancelToken));
		}
		
		private IEnumerator PlaySoundBundle(SoundBundle bundle, CancelToken token){
		
			
			foreach(var clip in bundle.clips){
				if(token.Cancelled) yield break;
				audio.PlayOneShot(clip.clip);
				
				if(clip.eventToDispatch != null) {
					yield return new WaitForSeconds(clip.eventDispatchDelay);
					
					if(token.Cancelled) yield break;
					if(OnAudioWord != null) {
						OnAudioWord(clip.eventToDispatch);
					}
				}
				
				yield return new WaitForSeconds(clip.clip.length - clip.eventDispatchDelay);
			}
		}
		
		private class CancelToken
		{
			private bool cancelled;
			public bool Cancelled { get { return cancelled; } }
			public void Cancel() {
				cancelled = true;
			}
		}
		
	}
}

