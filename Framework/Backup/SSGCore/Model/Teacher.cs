using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Teacher
{
	
	private string _name;
	public string Name{ get{ return _name; } }
	
	private Texture _image;
	public Texture Image{ get{ return _image; } }
	
	public Teacher( string name, Texture image )
	{
		_name = name;
		_image = image;
	}
	
}

