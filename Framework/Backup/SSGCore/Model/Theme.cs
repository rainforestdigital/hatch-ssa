using System;

namespace SSGCore
{
	public class Theme
	{
		public Theme (string baseName)
		{
			this.buttonSymbol = Main.SWF + ":" + baseName + "_Button";
			this.soundPath = baseName.ToLowerInvariant();
		}
		
		// current assumption is that a reference to
		// a loaded symbol for the button can be derived
		// from theme data
		private string buttonSymbol;
		public string ButtonSymbol
		{
			get { return buttonSymbol; }
		}
		
		private string soundPath;
		public string SoundPath
		{
			get { return soundPath; }
		}
	}
}

