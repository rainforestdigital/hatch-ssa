using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using LitJson;

namespace SSGCore
{

	public class Classroom
	{
		public IList<Child> Children
		{ 
			get; 
			private set; 
		}
		
		public Teacher Teacher
		{
			get;
			private set;
		}
		
		public string Name
		{
			get;
			private set;
		}
		
		public Texture Image
		{
			get;
			private set;
		}
		
		public ICollection<Theme> Themes
		{
			get;
			private set;
		}
		
		public Classroom( string name, Texture image, Teacher teacher, IList<Child> children, ICollection<Theme> themes)
		{
			Name = name;
			Image = image;
			Teacher = teacher;
			Children = children;
			Themes = themes;
		}
		
	}
	
}

