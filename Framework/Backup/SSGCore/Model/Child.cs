using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SSGCore;

public class Child : User
{
		
	private string _name;
	public string Name{ get{ return _name; } }
	
	private int _age;
	public int Age{ get{ return _age; } }
	
	private Texture _texture;
	public Texture Texture { get { return _texture; } }
		
	/// <summary>
	/// Initializes a new instance of the <see cref="Child"/> class. 
	/// This class is a value holder for child data.
	/// </summary>
	/// <param name='_name'>
	/// Name of the Child.
	/// </param>
	/// <param name='_age'>
	/// Age of the Child.
	/// </param>
	public Child( string _name, int _age )
	{
		this._name = _name;
		this._age = _age ;
	}
	
	public Child( string _name, string _age )
		: this(_name, int.Parse( _age ))
	{
	}
	
	public Child( string _name, Texture texture )
		: this(_name, 0)
	{
		_texture = texture;
	}
	
}

