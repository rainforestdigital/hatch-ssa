using System;
using UnityEngine;

namespace SSGCore
{
	// Remove this if it ends up that Child is the only thing that implements it
	public interface User
	{
		string Name { get; }
		Texture Texture { get; }
	}
}

