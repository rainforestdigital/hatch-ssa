using System;
using pumpkin.display;
using System.Collections.Generic;
using UnityEngine;

namespace SSGCore
{
	public static class LayoutUtils
	{
		
		public static void LayoutTiled<T>(this DisplayObjectContainer container, IList<T> tiles, float maxScale = 20) 
			where T : DisplayObject
		{
			if(tiles.Count == 0) {
				return;
			}
			
			// assume tiles are uniform in size
			T firstTile = tiles[0];
			int tileHeight = (int)firstTile.height;
			int tileWidth = (int)firstTile.width;
			
			int containerHeight = (int)container.height;
			int containerWidth = (int)container.width;
			int padding = 20;
			
			int maxTilesPerRow = Math.Max(1, containerWidth / (tileWidth+padding));
			//int maxTilesPerColumn = containerHeight / (tileHeight+padding);
			
			bool fitsOnOneRow = tiles.Count <= maxTilesPerRow;
			int columns = Math.Min(tiles.Count, maxTilesPerRow);
			int rows = fitsOnOneRow ? 1 : ((tiles.Count + columns - 1) / columns);
			
			float scale = 1;
			if(tiles.Count < maxTilesPerRow) {
				float rowWidth = tileWidth*columns + padding*(columns-1);
				scale = (float)((containerWidth*0.75) / rowWidth);
				scale = Math.Min((containerHeight*0.75f)/tileHeight, scale);
				scale = Math.Min(maxScale, scale);
				scale = Math.Max(1, scale);
			}
			
			var startX = (containerWidth - tileWidth*scale*columns - padding*(columns-1))/2;
			var startY = (containerHeight - tileHeight*scale*rows - padding*(rows-1))/2;
			
			var offsetX = tileWidth*scale + padding;
			var offsetY = tileHeight*scale + padding;
			
			int r = 0;
			int c = 0;
			foreach(T tile in tiles) {
				tile.x = startX + c*offsetX;
				tile.y = startY + r*offsetY;
				tile.scaleX = tile.scaleY = scale;
				container.addChild(tile);
				
				if(++c == columns) {
					r++;
					c = 0;
				}
			}
			
		}
		
		public static void FitToParent(this DisplayObject obj)
		{
			var parent = obj.parent;
			if(parent == null) {
				return;
			}
			
			var scaleX = parent.width / obj.width;
			var scaleY = parent.height / obj.height;
			var scale = Math.Min(scaleX, scaleY);
			
			obj.scaleX = scale;
			obj.scaleY = scale;
			obj.x = (parent.width - obj.width*scale)/2;
			obj.y = (parent.height - obj.height*scale)/2;
		}
		
		public static void ScaleAsIfFullScreen(this DisplayObject obj, float scaleCoefficient)
		{
			var parent = obj.parent;
			if(parent == null) {
				return;
			}
			
			var scaleX = parent.width / MainUI.STAGE_WIDTH;
			var scaleY = parent.height / MainUI.STAGE_HEIGHT;
			var scale = scaleCoefficient * Math.Min(scaleX, scaleY);
			
			obj.scaleX = scale;
			obj.scaleY = scale;
		}
		
		public static void ScaleCentered(this DisplayObject obj, float scale)
		{
			var parent = obj.parent;
			if(parent == null) {
				return;
			}
			
			var rect = obj.getBounds(parent);
			
			obj.x += (rect.width - rect.width*(scale/obj.scaleX))/2;
			obj.y += (rect.height - rect.height*(scale/obj.scaleY))/2;
			
			obj.scaleX = scale;
			obj.scaleY = scale;
		}
		
		public static void FillParent(this DisplayObject obj)
		{
			var parent = obj.parent;
			if(parent == null) {
				return;
			}
			
			var rect = obj.getBounds(parent);
			
			var scaleX = parent.width / rect.width;
			var scaleY = parent.height / rect.height;
			var scale = Math.Max(scaleX, scaleY);
			
			obj.scaleX *= scale;
			obj.scaleY *= scale;
			obj.x = (parent.width - rect.width*scale)/2;
			obj.y = (parent.height - rect.height*scale)/2;
		}
		
		public static void AlignBottomLeft(this DisplayObject obj, float padding=5)
		{
			var parent = obj.parent;
			if(parent == null) {
				return;
			}
			
			
			var rect = obj.getBounds(parent);
			obj.x = padding;
			obj.y = parent.height - (rect.height+padding);
		}
		
		public static void StretchToWidth(this DisplayObject obj)
		{
			var parent = obj.parent;
			if(parent == null) {
				return;
			}
			
			var rect = obj.getBounds(parent);
			obj.scaleX *= parent.width / rect.width;
		}
		
		public static void RemoveAll<T>(this DisplayObjectContainer parent, IEnumerable<T> objects)
			where T : DisplayObject
		{
			foreach( var obj in objects) {
				parent.removeChild(obj);
			}
		}
	}
}

