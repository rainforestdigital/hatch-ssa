using System;
using UnityEngine;
using HatchFramework;

namespace SSGCore
{
	public static class SoundUtils
	{
		public static void PlaySimpleSound(string path)
		{
			var clip = LoadGlobalSound(path);
			if(clip != null)
			{
				var bundle = new SoundEngine.SoundBundle();
				bundle.AddClip(clip);
				SoundEngine.Instance.PlayBundle(bundle);
			}
		}
		
		public static AudioClip LoadGlobalSound(string path)
		{
			// hack to load without asset bundles for now
			AudioClip clip = null;
			
			if(AssetLoader.Instance != null) {
				clip = AssetLoader.Instance.GetAudioClip(MovieClipFactory.GLOBAL, path);
			}
			
			if(clip == null) {
				clip = (AudioClip)Resources.Load("SSGAssets/UNIVERSAL/"+path, typeof(AudioClip));
			}
			
			return clip;
		}
	}
}

