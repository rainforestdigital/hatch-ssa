using System;

using System.Collections.Generic;

using pumpkin;
using pumpkin.swf;
using pumpkin.display;
using pumpkin.events;
using UnityEngine;
using HatchFramework;

namespace SSGCore
{
	
	
	public class MainUI : MovieClip
	{
		public static int STAGE_WIDTH = 1920;
		public static int STAGE_HEIGHT = 1080;
		
		private IList<Classroom> classrooms;
		private Classroom currentClassroom;
		private Child currentChild;
		
		private MovieClip background_mc;
		private MovieClip bar_mc;
		private GameScreen screen;
		
		public SSGEngine Engine{get;set;}
		
		public MainUI(string linkage):base(linkage){
		
			Initialize();
		}
		
		public MainUI(string swf, string symbol):base(swf, symbol){
			Initialize();
		}
		
		/// <summary>
		/// Initialize this instance;
		/// </summary>
		protected void Initialize(){
			
			background_mc = new MovieClip(Main.SWF, "Menu_Background");
			addChild(background_mc);

			bar_mc = new MovieClip(Main.SWF, "Menu_Bar");
			addChild(bar_mc);
			
			// any texture is as good as any other at this point
			Texture testImage = null;
			if(AssetLoader.Instance != null) {
				testImage = MovieClipFactory.GetTestIOSFile();
			}
			
			classrooms = MockData.GenerateTestClassrooms(testImage);
			GoToSelectClassroom();
			
			var stage = MovieClipOverlayCameraBehaviour.instance.stage;
			stage.addEventListener(MouseEvent.RESIZE, HandleResize);
			
			HandleResize(null);
		}
		
		/// <summary>
		/// Shows the user status - visibility = true;
		/// </summary>
		public void ShowUserStatus(){
			
		}
		
		/// <summary>
		/// Hides the user status - visibility = false;
		/// </summary>
		public void HideUserStatus(){
		
			
		}
		
		
		
		
	
		public void ShowOverlay(){
			
		}
		
		public void HideOverlay(){
			
		}
		
		/// <summary>
		/// Shows the teacher mode MC - Instantiate and hide things should not show
		/// </summary>
	
		public void ShowTeacherMode(){
			//fadeOutDisplayObject( helpFlag_mc ); 										----TODO
			//fadeInDisplayObject( teacherMode_mc );
			//themeCast.showTutorialBG();
			
		}
		
		/// <summary>
		/// Hides the teacher mode - remove entirely!
		/// </summary>
		public void HideTeacherMode(){
			//fadeOutDisplayObject( teacherMode_mc );
			//themeCast.hideTutorialBG();
		}
		
		public void ShowViews()
		{
			//views.visible = true;
		}
		
		public void HideViews()
		{
			//views.visible = false;
		}
		
		private void HandleResize(CEvent e)
		{
			width = Screen.width;
			height = Screen.height;
			
			background_mc.FillParent();
			
			bar_mc.ScaleAsIfFullScreen(2);
			bar_mc.StretchToWidth();
			bar_mc.y = height - bar_mc.height;
			
			screen.Resize(Screen.width, Screen.height);
		}
		
		private void GoToSelectClassroom()
		{
			var s = new SelectClassroomScreen(classrooms);
			s.ClassroomSelected += (Classroom c) => {
				currentClassroom = c;
				GoToSelectChild();
			};
			SetScreen(s);
		}
		
		private void GoToSelectChild()
		{
			var s = new SelectChildScreen(currentClassroom.Children);
			s.ChildSelected += OnChildSelected;
			s.BackPressed += GoToSelectClassroom;
			SetScreen(s);
		}
		
		private void OnChildSelected(Child c)
		{
			currentChild = c;
			var themes = currentClassroom.Themes;
			if(themes.Count == 1)
			{
				UnimplementedScreen();
			}
			else
			{
				GoToSelectTheme();
			}
		}
		
		private void GoToSelectTheme()
		{
			var s = new SelectThemeScreen(currentChild, currentClassroom.Themes);
			s.ThemeSelected += (Theme theme) => UnimplementedScreen();
			s.BackPressed += GoToSelectChild;
			SetScreen(s);
		}
		
		private void UnimplementedScreen()
		{
			GoToSelectClassroom();
		}
		
		private void SetScreen(GameScreen newScreen)
		{
			if(screen != null)
			{
				this.removeChild(screen.View);
				screen.Dispose();
			}
			
			screen = newScreen;
			
			screen.Start();
			screen.Resize(Screen.width, Screen.height);
			this.addChild(screen.View);
		}
	}
}

