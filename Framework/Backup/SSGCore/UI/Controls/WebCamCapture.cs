using UnityEngine;
using System.Collections;

using pumpkin;
using pumpkin.display;
using pumpkin.events;

namespace SSGCore
{

	public class WebCamCapture : MovieClip
	{
		
		private int _width;
		private int _height;
		
		//	Web Cam
		private WebCamDevice[] devices;
		private string deviceName;
		private WebCamTexture camTexture;
		
		//	Display Texture
		private Texture2D displayTexture;
		private Rect srcRect;
		private Rect drawRect;
		
		private Texture2D snapshot = null;
		
		/// <summary>
		/// Gets the snap shot byte array.
		/// </summary>
		public byte[] SnapShotByteArray
		{
			get{ return snapshot.EncodeToPNG(); }
		}
		
		
		/// <summary>
		/// Gets a value indicating whether this <see cref="SSGCore.WebCamCapture"/> has available devices.
		/// </summary>
		/// <value>
		/// <c>true</c> if devices are available; otherwise, <c>false</c>.
		/// </value>
		public bool isAvailable
		{
			get{ return (devices.Length > 0); }
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="SSGCore.WebCamCapture"/> class. 
		/// This class captures the web cam stream, and renders/converts it to a <see cref="Texture2D"/> that is drawn to the screen.
		/// </summary>
		/// <param name='_width'>
		/// Width of the movieclip.
		/// </param>
		/// <param name='_height'>
		/// Height of the movieclip.
		/// </param>
		public 	WebCamCapture( int _width, int _height ) : base()
		{
			
			this._width = _width;
			this._height = _height;
			
			init();
			
		}
		
		private void init()
		{		
			
			devices = WebCamTexture.devices;
			
			if(!isAvailable)
			{
				Debug.LogError( "No Web Cams exist on this device. You will not be able take/save pictures." );
				return;
			}
			
			//Set default decive name
			deviceName = devices[0].name;
			
			srcRect = new Rect( 0, 0, 1, 1 );
			drawRect = new Rect( 0, 0, _width, _height );
			
#if UNITY_IPHONE || UNITY_ANDROID
			//Find first rear-facing camera
			for(int i = 0; i < devices.Length; i++)
			{
				if(!devices[i].isFrontFacing)
				{
					deviceName = devices[i].name;
					break;
				}
			}
			
			srcRect = new Rect( 0, 0, 2, 2 );
#endif
			
			camTexture = new WebCamTexture( deviceName, _width, _height );
					
		}
		
		/// <summary>
		/// Starts the web cam and renders the texture to this movieclip.
		/// </summary>
		public void Start()
		{
			if(!isAvailable) return;
			this.graphics.clear();
			camTexture.Play();
			this.addEventListener( CEvent.ENTER_FRAME, update );
		}
		
		/// <summary>
		/// Stops the web cam and clears the graphics layer.
		/// </summary>
		public void Stop()
		{
			if(!isAvailable) return;
			this.removeEventListener( CEvent.ENTER_FRAME, update );
			this.graphics.clear();
		}
		
		/// <summary>
		/// Takes a snap shot from the web cam texture. Calls Stop() and re-draws the last frame.
		/// </summary>
		public Texture2D TakeSnapShot()
		{
			if(!isAvailable) return null;
			displayTexture = new Texture2D( camTexture.width, camTexture.height, TextureFormat.ARGB32, false );
			displayTexture.SetPixels(camTexture.GetPixels(0, 0, camTexture.width, camTexture.height));
			displayTexture.Apply();
			Stop();
			this.graphics.drawRectUV( displayTexture, srcRect, drawRect );
			return displayTexture;
		}
	
		/// <summary>
		/// Clears the graphics layer, updates the display texture, and re-draws it to the graphics layer.
		/// </summary>
		/// <param name='e'>
		/// Called from an ENTER_FRAME listner. Pass 'null' if calling from elsewhere.
		/// </param>
		private void update( CEvent e )
		{
			this.graphics.clear();
			this.graphics.drawRectUV( camTexture, srcRect, drawRect );
		}		
	}

}
