using System;
using System.Collections.Generic;
using pumpkin.display;
using pumpkin.events;
using UnityEngine;
using HatchFramework;

namespace SSGCore
{
	public class SelectChildScreen : GameScreen
	{
		private DisplayObjectContainer view = new DisplayObjectContainer();
		private DisplayObjectContainer childrenView = new DisplayObjectContainer();
		private List<HatchUserButton> buttons = new List<HatchUserButton>();
		private Child selectedChild;
		private BasicButton back_btn;
		
		public event Action<Child> ChildSelected;
		public event Action BackPressed;
		
		public SelectChildScreen (IEnumerable<Child> children)
		{
			childrenView.width = Main.STAGE_WIDTH*0.75f;
			childrenView.height = Main.STAGE_HEIGHT*0.75f;
			view.addChild(childrenView);
			
			foreach(Child child in children) {
				var button = new HatchUserButton();
				button.User = child;
				button.addEventListener(MouseEvent.CLICK, OnChildClick);
				buttons.Add(button);
			}
			
			// since it only occurs in two places, and the functionality
			// is relatively simple, the back button is internal to the
			// screens on which it appears. This may change if the circumstances
			// alter.
			back_btn = new BasicButton(Main.SWF, "Back_button");
			back_btn.addEventListener(MouseEvent.CLICK, OnBackClick);
			view.addChild(back_btn);
		}
		
		public void Start()
		{
			childrenView.LayoutTiled(buttons);
			AnimationUtils.GrowIn(buttons);
			
			SoundUtils.PlaySimpleSound("touch_to_play");
		}

		public DisplayObject View 
		{
			get { return view; }
		}
				
		private void OnChildClick(CEvent e)
		{
			var button = (HatchUserButton)e.currentTarget;
			var child = button.User;
			
			
			if(selectedChild == null) {
				selectedChild = (Child)child;
				
				const int optionCount = 4;
				var optionButtons = buttons.RandomSampleWith(optionCount, button);
				childrenView.RemoveAll(buttons);
				childrenView.LayoutTiled(optionButtons);
				AnimationUtils.GrowIn(optionButtons);
				SoundEngine.Instance.StopAll();
				SoundUtils.PlaySimpleSound("touch_again");
			}
			else if(child == selectedChild) {
				Debug.Log("Selected Child: " + child.Name);
				if(ChildSelected != null) {
					ChildSelected(selectedChild);
				}
			}
			else {
				// cancel selection, and start over
				selectedChild = null;
				childrenView.RemoveAll(buttons);
				childrenView.LayoutTiled(buttons);
				AnimationUtils.GrowIn(buttons);
				
				SoundEngine.Instance.StopAll();
				var bundle = new SoundEngine.SoundBundle();
				bundle.AddClip(SoundUtils.LoadGlobalSound("not_you"));
				bundle.AddClip(SoundUtils.LoadGlobalSound("touch_to_play"));
				SoundEngine.Instance.PlayBundle(bundle);
			}
				
		}
		
		private void OnBackClick(CEvent e)
		{
			if(BackPressed != null)
			{
				BackPressed();
			}
		}

		public void Resize (int width, int height)
		{
			view.width = width;
			view.height = height;
			childrenView.FitToParent();
			back_btn.ScaleAsIfFullScreen(2);
			back_btn.AlignBottomLeft();
		}
		
		public void Dispose()
		{
			SoundEngine.Instance.StopAll();
		}
	}
}

