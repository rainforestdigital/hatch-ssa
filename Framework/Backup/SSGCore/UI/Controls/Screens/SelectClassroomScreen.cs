using System;
using System.Collections.Generic;
using pumpkin.display;
using pumpkin.events;
using HatchFramework;

namespace SSGCore
{
	public class SelectClassroomScreen : GameScreen
	{
		private DisplayObjectContainer view = new DisplayObjectContainer();
		private DisplayObjectContainer container = new DisplayObjectContainer();
		private List<ClassroomButton> buttons = new List<ClassroomButton>();
		
		public SelectClassroomScreen (IEnumerable<Classroom> classrooms)
		{
			container.width = MainUI.STAGE_WIDTH;
			container.height = MainUI.STAGE_HEIGHT;
			view.addChild(container);
			
			foreach(var classroom in classrooms) {
				var button = new ClassroomButton(classroom);
				button.addEventListener(MouseEvent.CLICK, OnClassroomClick);
				buttons.Add(button);
			}
		}
		
		public void Start()
		{
			container.LayoutTiled(buttons);
			AnimationUtils.GrowIn(buttons);
		}
		
		public event Action<Classroom> ClassroomSelected;

		public DisplayObject View 
		{
			get { return view; }
		}
				
		private void OnClassroomClick(CEvent e)
		{
			var button = (ClassroomButton)e.currentTarget;
			var theme = button.Classroom;
			if(ClassroomSelected != null)
			{
				ClassroomSelected(theme);
			}
		}

		public void Resize (int width, int height)
		{
			view.width = width;
			view.height = height;
			container.FitToParent();
		}
		
		public void Dispose()
		{
			SoundEngine.Instance.StopAll();
		}
	}
}

