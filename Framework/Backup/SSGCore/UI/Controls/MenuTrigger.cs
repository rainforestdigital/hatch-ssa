using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;

namespace SSGCore
{
	public class MenuTrigger : MovieClip
	{
		 		
		private MovieClip trigger_btn;
		
		private MovieClip options;
		
		private LabeledButton option_TL;
		private LabeledButton option_TR;
		private LabeledButton option_BL;
		private LabeledButton option_BR;
		
		private List<LabeledButton> option_btns;
		private List<int> option_numbers;
		
		public MenuTrigger( string linkage) : base( linkage )
		{
			init();
		}
		
		public MenuTrigger( string swf, string symbolName ) : base( swf, symbolName )
		{
			init();
		}
		
		private void init()
		{
			
			option_btns = new List<LabeledButton>();
			
			option_numbers = new List<int>{1, 2, 3, 4};
			
			trigger_btn = getChildByName<MovieClip>( "trigger_btn" );
			
			options = getChildByName<MovieClip>( "options" );
			options.visible = false;
			
			option_TL = new LabeledButton( Main.SWF, "MenuTrigger_OptionButton" );
			option_TL.addEventListener( MouseEvent.CLICK, onOptionClick );
			option_btns.Add( option_TL );
			options.addChild( option_TL );
			
			option_TR = new LabeledButton( Main.SWF, "MenuTrigger_OptionButton" );
			option_TR.addEventListener( MouseEvent.CLICK, onOptionClick );
			option_btns.Add( option_TR );
			option_TR.x = 1920 - option_TR.width;
			options.addChild( option_TR );
			
			option_BL = new LabeledButton( Main.SWF, "MenuTrigger_OptionButton" );
			option_BL.addEventListener( MouseEvent.CLICK, onOptionClick );
			option_btns.Add( option_BL );
			option_BL.y = 1080 - option_BL.height;
			options.addChild( option_BL );
			
			option_BR = new LabeledButton( Main.SWF, "MenuTrigger_OptionButton" );
			option_BR.addEventListener( MouseEvent.CLICK, onOptionClick );
			option_btns.Add( option_BR );
			option_BR.x = 1920 - option_BR.width;
			option_BR.y = 1080 - option_BR.height;
			options.addChild( option_BR );
			
			applyEventListeners();
			
		}
		
		private void applyEventListeners()
		{
			
			trigger_btn.addEventListener( MouseEvent.MOUSE_DOWN, onTriggerDown );
			trigger_btn.addEventListener( MouseEvent.MOUSE_UP, onTriggerUp );
			
		}
		
		private void update( CEvent e )
		{
			
			float timeDiffernence = Time.time - startTime;
			
			if(timeDiffernence >= 3)
			{
				onTriggerUp(null);
				startCombinationMenu();
			}
			
		}
		
		private float startTime;
		private void onTriggerDown( CEvent e )
		{
			
			startTime = Time.time;
			
			addEventListener( CEvent.ENTER_FRAME, update );
			
		}
		
		private void onTriggerUp( CEvent e )
		{
			removeEventListener( CEvent.ENTER_FRAME, update );
		}
		
		private void onOptionClick( CEvent e )
		{
			
			LabeledButton btn = e.currentTarget as LabeledButton;
			
			int clickNumber = int.Parse( btn.Label );
			
			if(currentCombination + 1 == clickNumber)
			{
				currentCombination++;
			}
			else
			{
				stopCombinationMenu();
				return;
			}
			
			if(currentCombination == 4)
			{
				stopCombinationMenu();
				//Main.Instance.ShowViews();
				//Main.Instance.Views.CurrentView = ViewTypes.TEACHER_MENU;
			}
			
		}
		
		private int currentCombination = 0;
		//Starts the touch combination process for showing the Teacher Menu
		private void startCombinationMenu()
		{
			
			//Main.Instance.ShowOverlay();
			//Main.Instance.HideViews();
			
			for(int i = 0; i < option_btns.Count; i++)
			{
				
				int labelIndex = 1;
				if(i == 0)
				{
					do{
						labelIndex = Mathf.RoundToInt( Random.Range(0, option_numbers.Count - 1) );
					}
					while(labelIndex == 1);
				}
				else
				{
					labelIndex = Mathf.RoundToInt( Random.Range(0, option_numbers.Count - 1) );
				}
				option_btns[i].Label = option_numbers[ labelIndex ].ToString();
				option_numbers.RemoveAt( labelIndex );
				
			}
			
			option_numbers.Clear();
			option_numbers = new List<int>{1, 2, 3, 4};
			
			currentCombination = 0;
			trigger_btn.visible = false;
			options.visible = true;
				
		}
		
		private void stopCombinationMenu()
		{
			options.visible = false;
			trigger_btn.visible = true;
		}
		
	}
}
