using UnityEngine;
using System.Collections;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.text;

namespace SSGCore
{
	public class HatchUserButton : BasicButton
	{
		private User user;
		public User User
		{
			get { return user; }
			set
			{
				user = value;
				if(user != null)
				{
					NameText = user.Name;
					SetImage(user.Texture);
				}
				else
				{
					NameText = "";
					SetImage(null);
				}
			}
		}
		
		public void SetImage(Texture texture)
		{
			var graphics = imageContainer_mc.graphics;
			graphics.clear();
			
			// TODO: determine more elegant way of sizing
			// using size of clip was tried, but graphics in
			// the clip appeared to interfere with the drawing.
			// Possibly use proxy.
			if(texture != null) {
				graphics.drawRectUV(texture,
					new Rect(0, 0, 1, 1),
					new Rect(0, 0, 125, 116));
			}
		}
		
		private TextField name_txt;
		public string NameText
		{ 
			get{ return name_txt.text; } 
			
			set
			{
				name_txt.text = value;
			}
			
		}
			
		private int buttonID;
		public int ButtonID
		{
			get{ return buttonID; }
			
			set{ buttonID = value; }
			
		}
		
		private MovieClip imageContainer_mc;
		public MovieClip ImageContainer{ get{ return imageContainer_mc; } }
		
		//		Data Objects
		//--------------------------
		//private string _classrooms;
		//private string _data;     
		
		public HatchUserButton() : base( Main.SWF, "HatchUserButton" )
		{
			
		}
		
		public HatchUserButton( string linkage ) : base( linkage )
		{
			init();
		}
		
		public HatchUserButton( string swf, string symbolName ) : base( swf, symbolName )
		{
			init();
		}
		
		protected override void init()
		{
			
			base.init();
			
			name_txt = getChildByName<MovieClip>( "name_mc" ).getChildByName<TextField>("name_txt");
					
			imageContainer_mc = getChildByName<MovieClip>( "imageContainer_mc" );
			
		}
		
	}
}
