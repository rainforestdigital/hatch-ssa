using UnityEngine;
using System.Collections;

using pumpkin;
using pumpkin.display;
using pumpkin.text;

public enum LevelTypes
{
	
	BLANK,
	TT,
	EG,
	DG,
	DD,
	CD,
	LOCKED
	
}

namespace SSGCore
{

	public class UserStatus : MovieClip
	{
		
		private MovieClip root;
		public MovieClip Root
		{
			get{ return root; } 
		}
		
		private TextField title_text;
		
		private MovieClip level_mc;
		
		private LevelTypes levelType;
		public LevelTypes LevelType
		{
			get
			{
				return levelType;
			}
			
			set
			{
				levelType = value;
				updateLevelType();
			}
		}
		
		public UserStatus( string linkage ) : base( linkage )
		{
			root = this;
			init();
		}
		
		public UserStatus( string swf, string symbolName ) : base( swf, symbolName )
		{
			root = this;
			init();
		}
		
		public UserStatus( MovieClip mc ) : base()
		{
			root = mc;
			init();
		}
		
		private void init()
		{
		
			levelType = LevelTypes.DG;
			
			title_text = root.getChildByName<TextField>( "title_txt" );
			title_text.text = "None";
			
			level_mc = root.getChildByName<MovieClip>( "level_mc" );
			level_mc.gotoAndStop( levelType.ToString() );
		}
		
		private void updateLevelType()
		{
			level_mc.gotoAndStop( levelType.ToString() );
		}
		
	}

}