using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;

namespace SSGCore
{
	
	public enum Months
	{
		January,
		February,
		March,
		April,
		May,
		June,
		July,
		August,
		September,
		October,
		November,
		December
	}
	
	public class DatePicker : MovieClip
	{
			
		private DateTime currentDate;
		
		private DatePickerCalendar calendar;
		
		private int currentMonth;
		public int CurrentMonth{ get{ return currentMonth; } }
		
		private int currentYear;
		public int CurrentYear{ get{ return currentYear; } }
		
		private DateTime selectedDate;
		public DateTime SelectedDate{ get{ return selectedDate; } }
					
		private DropDown monthSelect;
		public Vector2 MonthSelectPosition
		{
			get{ return new Vector2(monthSelect.x, monthSelect.y); }
			
			set
			{
				monthSelect.x = value.x;
				monthSelect.y = value.y;
			}
			
		}
		
		private DropDown yearSelect;
		public Vector2 YearSelectPosition
		{
			get{ return new Vector2(yearSelect.x, yearSelect.y); }
			
			set
			{
				yearSelect.x = value.x;
				yearSelect.y = value.y;
			}
			
		}
				
		/// <summary>
		/// Initializes a new instance of the <see cref="HatchFramework.DatePicker"/> class.
		/// </summary>
		/// <param name='swf'>
		/// SWF used for the base DatePicker MovieClip.
		/// </param>
		/// <param name='symbolname'>
		/// Symbolname of the DatePicker MovieClip.
		/// </param>
		/// <param name='btnsymbolname'>
		/// The symbol name for the date button movie clips.
		/// </param>
		public DatePicker( string swf, string symbolname, string btnsymbolname) : base( swf, symbolname )
		{
			init();
		}
			
		//	Init the DatePicker. Creates all the date buttons, adds the drop down menus for month and year.
		private void init()
		{
				
			currentDate = DateTime.Now;
			
			currentMonth = currentDate.Month;
			currentYear = currentDate.Year;
			
			calendar = new DatePickerCalendar();
			calendar.addEventListener( CEvent.CHANGED, onDateClick );
			calendar.x = 60;
			calendar.y = 100;
			addChild( calendar );
			
			string[] months = new string[12]{"January", "February", "March", "April", "May", "June", "July", "August","September", "October", "November", "December"};
			monthSelect = MovieClipFactory.CreateDropDown( "DatePicker_DropDown", "DatePicker_DropDown_ListItem", 6, months );
			monthSelect.addEventListener( CEvent.CHANGED, onMonthChange );
			monthSelect.Label = ((Months)currentMonth - 1).ToString();
			monthSelect.x = 20;
			monthSelect.y = 20;
			addChild( monthSelect );
			
			List<string> yearRange = new List<string>();
			
			for(int y = currentYear; y > currentYear - 12; y--)
			{
				yearRange.Add( y.ToString() );
			}
			
			yearSelect = MovieClipFactory.CreateDropDown( "DatePicker_DropDown", "DatePicker_DropDown_ListItem", 6, yearRange.ToArray() );
			yearSelect.addEventListener( CEvent.CHANGED, onYearChange );
			yearSelect.Label = currentYear.ToString();
			yearSelect.x = monthSelect.x + yearSelect.width + 20;
			yearSelect.y = 20;
			addChild( yearSelect );
			
		}
		
		//	Click handler for date buttons. Updates selectedDate.
		private void onDateClick( CEvent e )
		{
			selectedDate = calendar.SelectedDate;
		}
		
		//	Change handler for monthSelect. Updates calendar.
		private void onMonthChange( CEvent e )
		{
			currentMonth = ((int)Enum.Parse( typeof(Months), monthSelect.Label )) + 1;
			calendar.UpdateCalendar(currentYear, currentMonth + 1);
		}
		
		//	Change handler for yearSelect. Updates calendar.
		private void onYearChange( CEvent e )
		{
			currentYear = int.Parse( yearSelect.Label );
			calendar.UpdateCalendar(currentYear, currentMonth + 1);
		}
				
	}

}