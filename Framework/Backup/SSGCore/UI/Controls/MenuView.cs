using UnityEngine;
using System.Collections;

using pumpkin;
using pumpkin.display;
using pumpkin.events;

namespace SSGCore
{
	public class MenuView : MovieClip
	{
			
		protected bool isShowing = false;
		
		public MenuView( string linkage ) : base( linkage )
		{
			init();
		}
		
		public MenuView( string swf, string symbolName ) : base( swf, symbolName )
		{
			init();
		}
		
		protected virtual void init()
		{
			
		}
		
		public virtual void Show(bool _connected)
		{
			
			if(isShowing) return;
			isShowing = true;
			
		}
		
		public virtual void Hide()
		{
			
			if(!isShowing) return;
			isShowing = false;
			
		}
		
	}
}
