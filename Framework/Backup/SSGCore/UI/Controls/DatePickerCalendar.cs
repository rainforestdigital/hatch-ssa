using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;

namespace SSGCore
{

	public class DatePickerCalendar : DisplayObjectContainer
	{
	
		private List<LabeledButton> buttons;
		
		private int currentYear;
		private int currentMonth;
		private int currentDay = 1;
		
		public DateTime SelectedDate{ 
			get{
				Debug.Log(currentYear + " " + currentMonth + " " + currentDay);
				return new DateTime(currentYear, currentMonth, currentDay); 
			}
		}
		
		public DatePickerCalendar() : base()
		{
			
			buttons = new List<LabeledButton>();
			
			int row = 0;
			int column = 0;
			
			for(int i = 0; i < 35; i++)
			{
				if(column == 7)
				{
					row++;
					column = 0;
				}
				
				LabeledButton newDateBtn = MovieClipFactory.CreateLabeledButton( "DatePicker_Button" );
				
				newDateBtn.x = (column * newDateBtn.width);
				newDateBtn.y = (row * newDateBtn.height);
				newDateBtn.Label = (i + 1).ToString();
				addChild( newDateBtn );
				newDateBtn.visible = false;
				buttons.Add( newDateBtn );
				newDateBtn.addEventListener( MouseEvent.CLICK, onDateClick );
				
				column++;

			}
			
		}
		
		public void UpdateCalendar( int _currentYear, int _currentMonth )
		{
			
			currentYear = _currentYear;
			currentMonth = _currentMonth;
			
			DateTime beginningOfMonth = new DateTime(_currentYear, _currentMonth, 1);
			int beginningIndex = (int)beginningOfMonth.DayOfWeek;
			int days = beginningIndex + DateTime.DaysInMonth(currentYear, currentMonth);
			int dayNumber = 1;
		
			for(int i = 0; i < buttons.Count; i++)
			{
				if(i >= beginningIndex && i < days)
				{
					buttons[i].visible = true;
					buttons[i].Label = dayNumber.ToString();
					dayNumber++;
				}
				else 
					buttons[i].visible = false;
			}
			
		}
		
		private void onDateClick( CEvent e )
		{
			LabeledButton button = e.currentTarget as LabeledButton;
			currentDay = int.Parse( button.Label );
			dispatchEvent( new CEvent(CEvent.CHANGED) );
		}
		
	}
	
}