using UnityEngine;
using System.Collections;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;

namespace SSGCore
{
	public class ConfirmationMenu : MovieClip
	{
		
		private MovieClip root;
		public MovieClip Root
		{
			get{ return root; } 
		}
		
		private BasicButton yesBtn_mc;
		public BasicButton YesButton
		{
			get
			{
				return yesBtn_mc;
			}
		}
		
		private BasicButton noBtn_mc;
		public BasicButton NoButton
		{
			get
			{
				return noBtn_mc;
			}
		}
		
		public ConfirmationMenu( string linkage ) : base( linkage )
		{
			root = this;
			init();
		}
		
		public ConfirmationMenu( string swf, string symbolName ) : base( swf, symbolName )
		{
			root = this;
			init();
		}
		
		public ConfirmationMenu( MovieClip mc ) : base()
		{
			root = mc;
			init();
		}
		
		private void init()
		{
			
			yesBtn_mc = new BasicButton( root.getChildByName<MovieClip>( "yesBtn_mc" ) );
			noBtn_mc = new BasicButton( root.getChildByName<MovieClip>( "noBtn_mc" ) );
			
		}
		
	}
}