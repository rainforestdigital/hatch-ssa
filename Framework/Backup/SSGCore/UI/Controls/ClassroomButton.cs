using UnityEngine;
using System.Collections;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.text;

namespace SSGCore
{
	public class ClassroomButton : BasicButton
	{
		private Classroom classroom;
		public Classroom Classroom
		{
			get { return classroom; }
			set
			{
				if(classroom != value)
				{
					classroom = value;
					if(classroom != null)
					{
						name_txt.text = classroom.Name;
						UpdateImage();
					}
					else
					{
						name_txt.text = "";
					}
				}
				
			}
		}
		
		private TextField name_txt;
		private MovieClip imageContainer_mc;
		
		private void UpdateImage()
		{
			var graphics = imageContainer_mc.graphics;
			graphics.clear();
			
			// TODO: determine more elegant way of sizing
			// using size of clip was tried, but graphics in
			// the clip appeared to interfere with the drawing.
			// Possibly use proxy.
			
			DrawImage(classroom.Teacher.Image, new Rect(0, 0, 125, 116));
			DrawImage(classroom.Image, new Rect(125-32, 116-32, 32, 32));
		}
		
		private void DrawImage(Texture texture, Rect rect)
		{
			if(texture != null) {
				var graphics = imageContainer_mc.graphics;
				graphics.drawRectUV(texture,
					new Rect(0, 0, 1, 1),
					rect);
			}
		}
		
		public ClassroomButton(Classroom classroom) : base( Main.SWF, "HatchUserButton" )
		{
			name_txt = getChildByName<MovieClip>( "name_mc" ).getChildByName<TextField>("name_txt");
			imageContainer_mc = getChildByName<MovieClip>( "imageContainer_mc" );
			
			Classroom = classroom;
		}
		
	}
}
