using System;
using pumpkin;
using UnityEngine;
using System.Collections.Generic;

namespace SSGCore
{
	public class FilterMovieClipContainer : pumpkin.display.MovieClip
	{
		protected pumpkin.display.MovieClip root;
		
			
		
		static Material m_Material = null;
		public static Material BlurMaterial {
			get {
				if (m_Material == null) {
					m_Material = new Material(Shader.Find("Hidden/BlurEffectConeTap"));
					m_Material.hideFlags = HideFlags.DontSave;
				}
				return m_Material;
			} 
		}
		
		public Texture texture;
		public void SetTexture(Texture texture){
		
			if(root != null)
				removeChild(root);
			
			this.texture = texture;
			root = MovieClipFactory.CreateEmptyMovieClip();
			root.graphics.drawRectUV(texture, new Rect(0, 0, 1, 1), new Rect(-texture.width*0.5f, -texture.height*0.5f, texture.width, texture.height));
			root.alpha = 0.25f;
			this.addChild(root);
		}
		
		
		public List<MCEffect> effects;
		public FilterMovieClipContainer(string swf, string linkage):base(swf, linkage){
		
			effects = new List<MCEffect>();
			
				
		}
		
		public void AddEffect(MCEffect effect){
			
			effect.SetContainer(this);
			effects.Add(effect);
		}
		
		public void ApplyEffects(){
		
			foreach(MCEffect e in effects){
				e.ApplyEffect(texture);	
			}
		}
		
		
		
		
	}
	
	public class BlurEffect : MCEffect{
		
		
		
		public int iterations = 6;
		public float blurSpread = 0.9f;
		public Vector2 offset = Vector2.zero;
		public float bmpScale;
		
		public BlurEffect(int iterations, float spread, float bmpScale) : base(){
		
			this.iterations = iterations;
			this.blurSpread = spread;
			this.bmpScale = bmpScale;
			offset.x = 500;
			
		}
		
		public override void SetContainer (FilterMovieClipContainer container)
		{
			base.SetContainer (container);
			movieClip.x = offset.x;
			movieClip.y = offset.y;
		}
		
		public override void ApplyEffect (Texture  sampleImage)
		{
			
			Material cmaterial = new Material(Shader.Find ("Filters/CenterTexture"));
			
			float scale = 2f;
			float w = (float)sampleImage.width;
			float h = (float)sampleImage.height;
			RenderTexture rt = new RenderTexture((int)(w*scale), (int)(h*scale), 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Default);
			
			cmaterial.SetFloat("inputScale", 1f/scale);
			cmaterial.SetVector("offsetPosition", new Vector4(1f/scale, 1f/scale, 0, 0));
			Graphics.Blit (sampleImage, rt, cmaterial);
			//CreateBlur(rt, rt);
			
			movieClip.graphics.drawRectUV(rt, new Rect(0, 0, 1, 1), new Rect(-rt.width*0.5f, -rt.height*0.5f, rt.width, rt.height));
			
			
			
		}
		
		
			// Performs one blur iteration.
	public void FourTapCone (RenderTexture source, RenderTexture dest, int iteration)
	{
		float off = 0.5f + iteration*blurSpread;
		Graphics.BlitMultiTap (source, dest, FilterMovieClipContainer.BlurMaterial,
			new Vector2(-off, -off),
			new Vector2(-off,  off),
			new Vector2( off,  off),
			new Vector2( off, -off)
		);
	}
	
	// Downsamples the texture to a quarter resolution.
	private void DownSample4x (Texture source, RenderTexture dest)
	{
		float off = 1.0f;
		Graphics.BlitMultiTap (source, dest, FilterMovieClipContainer.BlurMaterial,
			new Vector2(-off, -off),
			new Vector2(-off,  off),
			new Vector2( off,  off),
			new Vector2( off, -off)
		); 
	}
	
	void CreateBlur (Texture source, RenderTexture destination) {		
		RenderTexture buffer = RenderTexture.GetTemporary(source.width/4, source.height/4, 0);
		RenderTexture buffer2 = RenderTexture.GetTemporary(source.width/4, source.height/4, 0);
		
		// Copy source to the 4x4 smaller texture.
		DownSample4x (source, buffer);
		
		// Blur the small texture 
		bool oddEven = true;
		for(int i = 0; i < iterations; i++)
		{
			if( oddEven )
				FourTapCone (buffer, buffer2, i); 
			else
				FourTapCone (buffer2, buffer, i);
			oddEven = !oddEven;
		}
		if( oddEven )
			Graphics.Blit(buffer, destination);
		else
			Graphics.Blit(buffer2, destination);
		
		RenderTexture.ReleaseTemporary(buffer);
		RenderTexture.ReleaseTemporary(buffer2);
	}	
		
	}
	
	public class MCEffect{
	
		protected pumpkin.display.MovieClip movieClip;
		public FilterMovieClipContainer container;
		
		public MCEffect(){
			
		}
		
		public virtual void SetContainer(FilterMovieClipContainer container){
		
			this.container = container;
			movieClip = MovieClipFactory.CreateEmptyMovieClip();
			container.addChild(movieClip);
			
		}
		
		public virtual void ApplyEffect(Texture sampleImage){
			
		}
	}
}

