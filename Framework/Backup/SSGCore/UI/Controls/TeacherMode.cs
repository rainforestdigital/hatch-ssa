using UnityEngine;
using System.Collections;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;

namespace SSGCore
{
	public class TeacherMode : MovieClip
	{
		
		private MovieClip root;
		public MovieClip Root
		{
			get{ return root; } 
		}
		
		private BasicButton exitBtn_mc;
		public BasicButton ExitButton
		{
			get
			{
				return exitBtn_mc;
			}
		}
		
		public TeacherMode( string linkage ) : base( linkage )
		{
			root = this;
			init();		
		}
		
		public TeacherMode( string swf, string symbolName ) : base( swf, symbolName )
		{
			root = this;
			init();
		}
		
		public TeacherMode( MovieClip mc ) : base(  )
		{
			
			root = mc;
			init();
		}
		
		public void init()
		{
			exitBtn_mc = new BasicButton( Main.SWF, "Exit_Button" );
		}
			
	}
}
