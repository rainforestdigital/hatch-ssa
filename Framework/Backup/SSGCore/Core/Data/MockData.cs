using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSGCore
{
	public class MockData
	{
				
		//------------------------------------------------------------------------------------------------------Temporary User Data
		private static string temp_teacher_data = @"{""teachers"":  
											[{""name"":""Jim Jones"", ""children"":[
												{""name"":""Sally Fields"", ""age"":""5""},
												{""name"":""Timmy Dalton"", ""age"":""6""},
												{""name"":""Billy Kidd"", ""age"":""5""},
												{""name"":""Mace Windu"", ""age"":""6""},
												{""name"":""Giorgio Moroder"", ""age"":""4""},
												{""name"":""Marauder Shields"", ""age"":""6""},
												{""name"":""Ron Swanson"", ""age"":""5""}
											]}, 
											{""name"":""Kim Kones"", ""children"":[
												{""name"":""Boba Fett"", ""age"":""7""},
												{""name"":""Dothraki Horselord"", ""age"":""6""},
												{""name"":""Roland DeChain"", ""age"":""5""}
											]}]
										}";
		public static string TempTeacherData{ get{ return temp_teacher_data; } }
		
		public static IList<Child> GenerateTestChildren(Texture texture) {
			return new List<Child> {
				new Child("Sally Fields", texture),
				new Child("Timmy Dalton", texture),
				new Child("Zort Helkon", texture),
				new Child("Gleef Bligely", texture),
				new Child("Mace Windu", texture),
				new Child("Giorgio Moroder", texture),
				new Child("Ron Swanson", texture),
				new Child("Aubergine Caruthers", texture),
				new Child("George Masterstone", texture),
				new Child("Acute Intestinal Bleeding", texture)
			};
		}
		
		public static IList<Classroom> GenerateTestClassrooms(Texture texture) {
			var themes = GenerateTestThemes();
			return new List<Classroom> {
				new Classroom("Garden Palace", texture,
					new Teacher("Teach Teachly", texture),
					new List<Child> {
						new Child("Timmy Dalton", texture),
						new Child("Zort Helkon", texture),
						new Child("Gleef Bligely", texture)
					},
					new List<Theme> {
						themes[0]
				}),
				new Classroom("Overgrown Sneeze", texture,
					new Teacher("Onizuka", texture),
					new List<Child> {
						new Child("Sally Fields", texture),
						new Child("Timmy Dalton", texture),
						new Child("Zort Helkon", texture),
						new Child("Gleef Bligely", texture),
						new Child("Mace Windu", texture),
						new Child("Giorgio Moroder", texture),
						new Child("Ron Swanson", texture),
						new Child("Aubergine Caruthers", texture),
						new Child("George Masterstone", texture),
						new Child("Acute Intestinal Bleeding", texture)
					},
					themes),
				new Classroom("Too Many!", texture,
					new Teacher("Overwhelmed", texture),
					new List<Child> {
						new Child("Sally Fields", texture),
						new Child("Timmy Dalton", texture),
						new Child("Zort Helkon", texture),
						new Child("Gleef Bligely", texture),
						new Child("Mace Windu", texture),
						new Child("Giorgio Moroder", texture),
						new Child("Ron Swanson", texture),
						new Child("Aubergine Caruthers", texture),
						new Child("George Masterstone", texture),
						new Child("Acute Intestinal Bleeding", texture),
						new Child("Kid 1", texture),
						new Child("Kid 2", texture),
						new Child("Kid 3", texture),
						new Child("Kid 4", texture),
						new Child("Kid 5", texture),
						new Child("Kid 6", texture),
						new Child("Kid 7", texture),
						new Child("Kid 8", texture),
						new Child("Kid 9", texture),
						new Child("Kid 10", texture),
						new Child("Kid 11", texture),
						new Child("Kid 12", texture),
						new Child("Kid 13", texture)
					},
					themes),
			};
		}
		
		public static IList<Theme> GenerateTestThemes() {
			return new List<Theme> {
				new Theme("Travel"),
				new Theme("Garden"),
				new Theme("Safari")
			};
		}
	}
}

