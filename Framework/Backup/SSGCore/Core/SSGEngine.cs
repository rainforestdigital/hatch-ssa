using System;
using UnityEngine;
using HatchFramework;
using pumpkin.display;
using pumpkin.events;

namespace SSGCore
{
			
	/// <summary>
	/// Core engine for functionality
	/// </summary>
	[RequireComponent(typeof(MovieClipOverlayCameraBehaviour))]
	public class SSGEngine : MonoBehaviour
	{
		
		protected MainUI main;
		protected UpgradeManager upgradeManager;
		protected Stage stage;

		// -------------------------------------
		// 			INACTIVITY TIMER
		// -------------------------------------	
		private const int inactivityTime = 60;
		private float currentInactivityTime;
		private int inactivityCount = 0;
		
		public void Awake()
		{
			
		}
		
		public void Start()
		{
			stage = MovieClipOverlayCameraBehaviour.instance.stage;
			
			
			
			upgradeManager = new UpgradeManager(); 
			upgradeManager.OnCompleteEvent += HandleUpgradeManagerOnCompleteEvent;
			
			// delete later - tempGUI
			frameImg = Resources.Load("UI/Frame250x100") as Texture2D;
			AssetLoader.Instance.OnTotalProgressUpdateEvent += delegate(float percentLoaded) {
				percent = percentLoaded;
			};
			AssetLoader.Instance.OnInitialAssetBundlesLoadedEvent += delegate()
			{
				canShowGUI = false;
			};
			
			upgradeManager.CheckForUpdates();
		}

		void HandleUpgradeManagerOnCompleteEvent (bool success)
		{
			upgradeManager.OnCompleteEvent -= HandleUpgradeManagerOnCompleteEvent;
			upgradeManager = null;
			LoadMainUI();			
		}
		
		/// <summary>
		/// Loads the main UI and setup timers
		/// </summary>
		void LoadMainUI()
		{		
			main = MovieClipFactory.CreateMainUI();
			main.Engine = this;
			stage.addChild(main);
			
			inactivityCount = 0;
			currentInactivityTime = inactivityTime;
			
			stage.addEventListener(CEvent.ENTER_FRAME, OnUpdate);
		}
		
		public void Quit(){
		
			Application.Quit();
		}
		
		/// <summary>
		/// Handles Update event from the stage
		/// </summary>
		/// <param name='e'>
		/// E. 
		/// </param>
		private void OnUpdate( CEvent e )
		{
					
			//_databaseManager.update();
						
			//Decrease currentInactivityTime by ~1 per second
			currentInactivityTime -= Time.deltaTime * 2;
					
			if(currentInactivityTime <= 0)
			{
				inactivityCount++;
				currentInactivityTime = inactivityTime;
				
				switch(inactivityCount)
				{
					case 4:
						//onStopClick( null );
					break;
					
					case 5:
						//TODO: log user out
					break;
					
				}
				
			}
			
			//TODO: Hook in DB manager when that's all done
			
		}
		
		// remove later - temp GUI for client release
		float percent = 0;		
		Texture2D frameImg;
		bool canShowGUI = true;
		public void OnGUI()
		{
			if( !canShowGUI ) return;
			
			if( frameImg != null )
			{
				GUILayout.BeginArea(new Rect((Screen.width*.5f) - (frameImg.width*.5f), (Screen.height*.5f)-(frameImg.height*.5f), frameImg.width, frameImg.height), frameImg);
					GUILayout.BeginArea(new Rect(35, 25, 180, 50));
					
						GUI.contentColor = Color.black;
						if( upgradeManager.mode == UpgradeModes.CONFIG_DOWNLOAD )
							GUILayout.Label("Downloading Update Information");
						else
						{
							GUILayout.Label("Downloading Assets");
							GUILayout.Label("Loaded: " + Mathf.FloorToInt(percent*100).ToString() + "%");
						}
					
					GUILayout.EndArea();
				GUILayout.EndArea();
			}
		}
		
		
		#region GETTERS/SETTERS
		//------------------------------------------------
		//  GETTERS AND SETTERS
		//------------------------------------------------		
		
		public bool DemoMode{get;set;}
		
		private bool _teacherMode = false;
		
		public bool TeacherMode
		{
			get{ return _teacherMode; }
			
			set
			{
				if(_teacherMode != value)
				{
					
					_teacherMode = value;
					
					if(_teacherMode){
						main.ShowTeacherMode();
						//_pauseOpportunity = skillCast.currentOpportunity;
					}else{
						main.HideTeacherMode();
					}
					
					//loadNewSkill( _currentSkill );
									
				}
			}
		}
		#endregion
	}
}

