using System;
using HatchFramework;
using pumpkin.swf;
using pumpkin.display;
using UnityEngine;

namespace SSGCore
{
	public class MovieClipFactory
	{
		public static string PATH_MAIN = "Menu.swf";
		public static string GLOBAL = "GlobalAssets";
		public static string ADDITION = "Addition";
		public static string NUMERAL_RECOGNITION = "NumeralRecognition";
		
		public static MainUI CreateMainUI()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			loader.addAssetBundle(bundle);
			
			MainUI ui = new MainUI(PATH_MAIN, "Empty");
			
			return ui;	
		}
		
		// leaving this in as an example of how to construct a method to return an asset
		public static Texture2D GetTestIOSFile()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			if( bundle.Contains("iOSTexture") )
			{
				return bundle.Load("iOSTexture") as Texture2D;
			}
			else return null;
		}
				
		public static FilterMovieClipContainer CreateFilterMovieClipContainer(){
			
			return new FilterMovieClipContainer(PATH_MAIN, "Empty");
		}
		
		public static MovieClip CreateEmptyMovieClip(){
		
			return new MovieClip(PATH_MAIN, "Empty");
			
		}
		
		public static MovieClip CreateMovieClip( string swf, string symbolname ){
		
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(swf);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( swf + ".swf", symbolname);
			
			return ui;
			
		}
		
		public static DatePicker CreateDatePicker(){
		
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			loader.addAssetBundle(bundle);
			DatePicker ui = new DatePicker( PATH_MAIN, "DatePicker", "DatePicker_Button" );
			
			return ui;
			
		}
		
		public static DropDown CreateDropDown( string buttonSymbolName, string listItemSymbolName, int maxViewableItems, string[] listItems)
		{
			
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			loader.addAssetBundle(bundle);
			DropDown ui = new DropDown( PATH_MAIN, buttonSymbolName, listItemSymbolName, maxViewableItems, listItems);
			
			return ui;
			
		}
		
		
		public static BasicButton CreateBasicButton( string symbolName )
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			loader.addAssetBundle(bundle);
			BasicButton ui = new BasicButton( PATH_MAIN, symbolName );
			
			return ui;
		}
		
		public static LabeledButton CreateLabeledButton( string symbolName )
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			loader.addAssetBundle(bundle);
			LabeledButton ui = new LabeledButton( PATH_MAIN, symbolName );
			
			return ui;
		}
		
		public static LabeledButton CreateLabeledButton( string swf, string symbolName )
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(swf);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			loader.addAssetBundle(bundle);
			LabeledButton ui = new LabeledButton( swf + ".swf", symbolName );
			
			return ui;
		}
		
		public static ScrollBar CreateScrollBar( string symbolName )
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			loader.addAssetBundle(bundle);
			ScrollBar ui = new ScrollBar( PATH_MAIN, symbolName );
			
			return ui;	
		}
		
	}
}

