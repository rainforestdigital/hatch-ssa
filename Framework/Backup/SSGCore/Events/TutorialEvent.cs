using UnityEngine;
using System.Collections;

using pumpkin;
using pumpkin.events;

namespace SSGCore
{

	public class TutorialEvent : CEvent
	{
	
		public static string START = "Tutorial_Start";
		public new static string COMPLETE = "Tutorial_Complete";
		
		public TutorialEvent( string _type ) : base( _type )
		{
			
		}
		
		public TutorialEvent( string _type, bool _bubbles, bool _cancelable ) : base( _type, _bubbles, _cancelable )
		{
			
		}
		
	}
	
}