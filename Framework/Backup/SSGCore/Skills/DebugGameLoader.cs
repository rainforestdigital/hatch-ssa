using System;
using System.Collections.Generic;
using UnityEngine;
using HatchFramework;
using SSGCore;
using pumpkin.display;
using pumpkin;

namespace SSGCore
{
	
	public enum Skills
	{
		
		ADDITION,
		NUMERAL_RECOGNITION
		
	}
	
	[RequireComponent(typeof(MovieClipOverlayCameraBehaviour))]
	public class DebugGameLoader : MonoBehaviour
	{
		
		
		protected UpgradeManager upgradeManager;
		protected Stage stage;
		
		public Skills skill;
		
		public void Awake()
		{
			gameObject.AddComponent<AssetLoader>();
		}
		
		public void Start()
		{
		
			stage = MovieClipOverlayCameraBehaviour.instance.stage;
			
			AssetLoader.Instance.loadAssetBundlesLocally = true;
			
			// if we need to test downloading, uncomment next line to clear cache and force download
			//AssetLoader.Instance.clearCacheFirst = true;
			
			upgradeManager = new UpgradeManager();
			upgradeManager.OnCompleteEvent += HandleUpgradeManagerOnCompleteEvent;
			upgradeManager.CheckForUpdates();
						
		}
		
		void HandleUpgradeManagerOnCompleteEvent (bool success)
		{
			upgradeManager.OnCompleteEvent -= HandleUpgradeManagerOnCompleteEvent;
			upgradeManager = null;
			
			//BaseSkill currentSkill = getSkill();
			
			//stage.addChild(currentSkill);
			//currentSkill.Init(null);
			
			VideoPlayer.Instance.PlayVideo( VideoPlayer.Instance.IntroPath );
			
		}
		
		private BaseSkill getSkill()
		{
			
			switch(skill)
			{
				case Skills.ADDITION:
					return new Addition();
				
				case Skills.NUMERAL_RECOGNITION:
					return new Numeral_Recognition();
				
				default:
					return new Addition();
				
			}
				
			
		}
		
	}
}

