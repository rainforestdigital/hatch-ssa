using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;

namespace SSGCore
{
	
	public enum NumberLabels
	{
		ONE,
		TWO,
		THREE,
		FOUR,
		FIVE,
		SIX,
		SEVEN,
		EIGHT,
		NINE,
		TEN,
		ELEVEN,
		TWELVE,
		THIRTEEN,
		FOURTEEN,
		FIFTEEN
	}
	
	public class Numeral_Recognition : BaseSkill
	{
	
		#region Public Variables
		public string host = "PLATTIE"; //will probably need to change
		public float percent;
		public int totalOpportunities = 10;
		#endregion
		
		#region Private Variables
		//private bool _initialized = false; //Might not be needed
		private bool _complete = false;
		private bool opportunityComplete = false;
		private bool opportunityCorrect = false;
		private bool opportunityAnswered = false;
		
		private string _level = "TT";
		
		private int totalTime = 5000;
		private float timer;
		private int timeoutCount = 0;
		private int _currentOpportunity = 0;
		private int _numCorrect = 0;
		private int _numIncorrect = 0;
		private int currentNumber = 0;
		
		private List<int> usedNumbers;
		
		private List<LabeledButton> _cards;
		private LabeledButton _incorrectCard;
		
		//		Display Objects
		private MovieClip tutorial_mc = null;
		private MovieClip instructions_mc = null;
		private DisplayObjectContainer cardContainer_mc;
		#endregion
		
		#region Getter/Setters
		public int numCards
		{
			get{ return _cards.Count; }
			set
			{
				int i;
				for(i = 0; i < _cards.Count; i++)
				{
					_cards[i].visible = false;
					_cards[i].removeEventListener( MouseEvent.CLICK, onCardClick );
					cardContainer_mc.removeChild( _cards[i] );
				}
				
				_cards.Clear();
				
				LabeledButton answerCard = MovieClipFactory.CreateLabeledButton( "TODO" );
				answerCard.Label = currentNumber.ToString();
				answerCard.mouseChildrenEnabled = false;
				answerCard.buttonMode = true;
				answerCard.addEventListener( MouseEvent.CLICK, onCardClick );
				cardContainer_mc.addChild( answerCard );
				_cards.Add( answerCard );
				
				List<int> availableAnswers = new List<int>();
				for(i = 1; i <= 15; i++)
				{
					if( i != currentNumber )
						availableAnswers.Add( i );
				}
				
				while(_cards.Count < value)
				{
					LabeledButton tempCard = new LabeledButton( "TODO" );
					int ranAnswer = Mathf.FloorToInt( Random.Range(0, availableAnswers.Count) );
					tempCard.Label = availableAnswers[ranAnswer].ToString();
					availableAnswers.RemoveAt(ranAnswer);
					tempCard.mouseChildrenEnabled = false;
					tempCard.buttonMode = true;
					tempCard.addEventListener( MouseEvent.CLICK, onCardClick );
					cardContainer_mc.addChild( tempCard );
					_cards.Add( tempCard );
				}
				
				switch(_level)
				{
					case "TT":
						_cards = new List<LabeledButton>(){_cards[0], answerCard};
						_cards[0].visible = false;
					break;
					default:
						_cards.Shuffle();
					break;
				}
				
				for(i = 0; i < _cards.Count; i++)
				{
					_cards[i].x = (_cards[0].width * _cards.Count + 20 * (_cards.Count - 1)) / 2 + (_cards[0].width + 20) * i;
					_cards[i].y = _cards[i].height / 2;
				}
				
			}
		}
				
		public string level
		{
			get{ return _level; }
			set{ _level = value; }
		}
		
		public int currentOpportunity
		{
			get{ return _currentOpportunity; }
			set
			{ 
				_currentOpportunity = value;
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_CHANGE, true, false) );
			}
		}
		
		public int numCorrect
		{
			get{ return _numCorrect; }
			set{ _numCorrect = value; }
		}
		
		public int numIncorrect
		{
			get{ return _numIncorrect; }
			set{ _numIncorrect = value; }
		}
		#endregion
		
		public Numeral_Recognition( string swf ) : base( swf + ":Skill" )
		{
			
		}
		
		public override void Init (MovieClip parent)
		{
			base.Init (parent);
			//_initialized = true;
			
			cardContainer_mc = new DisplayObjectContainer();
			addChild( cardContainer_mc );
			
			_cards = new List<LabeledButton>();
			usedNumbers = new List<int>();
			
			timer = totalTime;
			
			
			//SoundEngine.Instance.OnAudioWord += eventHandlerHere
			
			switch(_level)
			{
				case "TT":
					totalOpportunities = 1;
					opportunityComplete = false;
					tutorial_mc.addEventListener( TutorialEvent.START, onTutorialStart );
					tutorial_mc.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
				break;
				
				case "EG":
				case "DG":
				case "DD":
				case "CD":
					totalOpportunities = 10;
					
					if(_currentOpportunity == 0)
					{
						
					}
					else
					{
						
					}
				break;
			}
		}
		
		private void startTimer()
		{
			this.addEventListener( CEvent.ENTER_FRAME, onTimer );
		}
		
		private void stopTimer()
		{
			this.removeEventListener( CEvent.ENTER_FRAME, onTimer );
			timer = totalTime;
		}
		
		private void onTimer( CEvent e )
		{
			
			timer -= Time.deltaTime * 2;
			
			if(timer <= 0)
			{
				onTimerComplete();
			}
			
		}
		
		public void transitionNextOpportunity()
		{
			
			Tweener.addTween(cardContainer_mc, Tweener.Hash( "time",0.35f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuint )).OnComplete( nextOpportunity );
			
		}
		
		private void nextOpportunity()
		{
		
			switch(_level)
			{
				case "TT":
					if(_currentOpportunity > 0)
					{
						if(opportunityCorrect)
						{
							_numCorrect++;
						}
						else 
						{
							_numIncorrect++;
						}
						if(_complete == false)
						{
							_complete = true;
							dispatchEvent( new SkillEvent( SkillEvent.SKILL_COMPLETE, true, false ) );
						}
					}
					else
					{
						_currentOpportunity++;
						currentNumber = 4;
						numCards = 2;
						
						fadeInCardContainer();
					
					}
				break;
				
				case "EG":
					setNextOpportunity(1, 5, 3);
				break;
				
				case "DG":
					setNextOpportunity(6, 10, 4);
				break;
				
				case "DD":
					setNextOpportunity(11, 15, 5);
				break;
				
				case "CD":
					setNextOpportunity(11, 15, 5);
				break;
				
			}
			
		}
		
		private void setNextOpportunity( int min, int max, int cards )
		{
			
			List<int> answersArray;
			int i;
			
			if(_currentOpportunity > 0)
					{
						if(opportunityComplete)
						{
							if(opportunityCorrect)
							{
								numCorrect++;
							}
							else
							{
								numIncorrect++;
							}
						}
						else
						{
							numIncorrect++;
						}
					}
					if(currentOpportunity < totalOpportunities)
					{
						opportunityComplete = false;
						opportunityCorrect = false;
						opportunityAnswered = false;
					
						currentOpportunity++;
						
						answersArray = new List<int>();
					
						for(i = min; i <= max; i++)
						{
							if(usedNumbers.IndexOf(i) == -1)
							{
								answersArray.Add(i);
							}
						}
					
						if(answersArray.Count == 0)
						{
							usedNumbers.Clear();
							for(i = min; i <= max; i++)
							{
								answersArray.Add(i);
							}
						}
					
						currentNumber = answersArray[Mathf.FloorToInt(Random.Range(0, answersArray.Count))];
						usedNumbers.Add(currentNumber);
					
						numCards = cards;
					
						fadeInCardContainer();
					
					}
					else
					{
						percent = Mathf.Round((numCorrect / (numCorrect + numIncorrect)) * 100);
						//TODO timer.stop;
						numCards = 0;
						if(_complete == false)
						{
							_complete = true;
							dispatchEvent( new SkillEvent( SkillEvent.SKILL_COMPLETE, true, false ) );
						}
					}
		}
		
		public void CorrectOpportunityContinue()
		{
			//TODO - youFound_mc.gotoAndPlay(numberLabels[currentNumber - 1]);
			
		}
		
		public void CriticismContinue()
		{
			for(int i = 0; i < _cards.Count; i++)
			{
				if(_cards[i] != _incorrectCard && _cards[i].visible)
				{
					enableCardButtons();
				}
			}
			
			//TODO - youFound_mc.gotoAndPlay(numberLabels[int(_incorrectCard.label) - 1]);
			
		}
		
		public void OpportunityStart()
		{
			switch(level)
			{
				case "TT":
					//TODO instructions_mc.gotoAndPlay("NOW_ITS");
				break;
			default:
					//TODO pleaseFind_mc.gotoAndPlay(numberLabels[currentNumber - 1]);
				break;
			}
			
			enableCardButtons();
			
			timeoutCount = 0;
			
			dispatchEvent( new SkillEvent( SkillEvent.OPPORTUNITY_START, true, false ) );
			
		}
		
		private void disableCardButtons()
		{
			for(int i = 0; i < _cards.Count; i++)
			{
				_cards[i].mouseEnabled = false;
			}
		}
		
		private void enableCardButtons()
		{
			for(int i = 0; i < _cards.Count; i++)
			{
				_cards[i].mouseEnabled = true;
			}
		}
		
		private void fadeInCardContainer()
		{
			Tweener.addTween(cardContainer_mc, Tweener.Hash("time", 0.35f, "alpha", 1, "transition", Tweener.TransitionType.easeInOutQuint) );
		}
		
		#region Event Handlers
		
		//	Audio Handlers
		private void onAudioLibComplete()
		{
			
		}
		
		private void onAudioLibStart()
		{
			
		}
		
		private void onAudioLibWord()
		{
			
		}
		
		public void onTutorialStart( CEvent e )
		{
			//Not needed?
		}
		
		public void onTutorialComplete( CEvent e )
		{
			tutorial_mc.removeEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.removeEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			tutorial_mc.gotoAndStop( 1 );
			nextOpportunity();
		}
		
		
		public void onTimerComplete()
		{
			int i;
			timeoutCount++;
			
			switch(timeoutCount)
			{
				case 1:
					if(level == "TT")
					{
						//TODO instructions_mc.gotoAndPlay("NOW_ITS");
					}
					else
					{
						//TODO pleaseFind_mc.gotoAndPlay(numberLabels[currentNumber - 1]);
					}
				break;
				
				case 2:
					if(level == "TT")
					{
						disableCardButtons();
						
						if(opportunityAnswered == false)
						{
							opportunityAnswered = true;
							opportunityCorrect = false;
						}
					
						opportunityComplete = true;
						//TODO instructions_mc.gotoAndPlay("LETS");
						
					}
					else
					{
						//TODO instructions_mc.gotoAndPlay("TOUCH");
					}
				break;
				
				case 3:
					if(opportunityAnswered == false)
					{
						opportunityAnswered = true;
						opportunityCorrect = false;
					}
					
					for(i = 0; i < _cards.Count; i++)
					{
						if(_cards[i].Label == currentNumber.ToString())
						{
							//TODO Glow this object
							break;
						}
					}
				
				//TODO pleaseFind_mc.gotoAndPlay(numberLabels[currentNumber - 1]);
				
				break;
				
				case 4:
					opportunityComplete = true;
				
					for(i = 0; i < _cards.Count; i++)
					{
						_cards[i].mouseEnabled = false;
						if(_cards[i].Label != currentNumber.ToString())
						{
							Tweener.addTween( _cards[i], Tweener.Hash("time",0.5f, "alpha",0, "visible",false, "transition",Tweener.TransitionType.easeOutQuad) );
						}
					}
					
					//TODO thisIs_mc.gotoAndPlay(numberLabels[currentNumber - 1]);
				
				break;
				default:
				break;
				
			}
			
		}
		
		public void onCardClick( CEvent e )
		{
			LabeledButton button = e.currentTarget as LabeledButton;
			
			if(button.visible == true) //not sure if needed
			{
				Tweener.removeTweens(this);
				instructions_mc.stop();
				
				//stopTimer
				timeoutCount = 0;
				
				if(_incorrectCard != null)
				{
					_incorrectCard.scaleX = 1;
					_incorrectCard.scaleY = 1;
					Tweener.addTween(_incorrectCard, Tweener.Hash( 	
						"time",0.5f, "x", 
						_incorrectCard.x + (_incorrectCard.width / 2) - ((_incorrectCard.width * 0.4f) / 2),
						"y", _incorrectCard.y + (_incorrectCard.height / 2) - ((_incorrectCard.height * 0.4f) / 2),
						"scaleX", 0.4f, "scaleY", 0.4f, "alpha",0, "visible",false, "transition",Tweener.TransitionType.easeInOutQuad 
						));
				}
				
				cardContainer_mc.setChildIndex( button, cardContainer_mc.numChildren - 1 );
				button.scaleX = 1;
				button.scaleY = 1;
				Tweener.addTween( button, Tweener.Hash(
					"time",0.5f,
					"x", button.x + (button.width / 2) - ((button.width * 1.4f) / 2),
					"y", button.y + (button.height / 2) - ((button.height * 1.4f) / 2),
					"scaleX", 1.4f, "scaleY", 1.4f, "transition", Tweener.TransitionType.easeOutQuad
					));
				
				if(button.Label == currentNumber.ToString())
				{
					if(opportunityAnswered == false)
					{
						opportunityAnswered = true;
						opportunityCorrect = true;
					}
					opportunityComplete = true;
					
					disableCardButtons();
					
					switch(level)
					{
						case "TT":
							if(parentClip == null)
							{
								//TODO youFound_mc.gotoAndPlay(numberLabels[currentNumber - 1]);
							}
							else
							{
								dispatchEvent(new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false));
							}
						break;
						default:
							if(parentClip == null)
							{
								//TODO youFound_mc.gotoAndPlay(numberLabels[currentNumber - 1]);
							}
							else
							{
								dispatchEvent(new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false));
							}
						break;
					}
					
				}
				else
				{
					_incorrectCard = button;
					
					disableCardButtons();
					
					if(opportunityAnswered == false)
					{
						opportunityAnswered = true;
						opportunityCorrect = true;
					}
					
					if(parentClip == null)
					{
						CriticismContinue();
					}
					else 
					{
						dispatchEvent( new SkillEvent( SkillEvent.CRITICISM_REQUEST, true, false ) );
					}
					
				}
				
			}
		}
		
		#endregion
		
	}
	
}