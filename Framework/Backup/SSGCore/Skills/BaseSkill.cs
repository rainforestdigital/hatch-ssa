using System;
using pumpkin.display;

namespace SSGCore
{
	public class BaseSkill : DisplayObjectContainer
	{
		
		
		protected MovieClip parentClip;
		public BaseSkill () : base()
		{
				
		}
		
		public virtual void Init( MovieClip parent){
		
			this.parentClip = parent;
			
		}
		
		public virtual void Resize( int width, int height )
		{
			
		}
		
	}
}

