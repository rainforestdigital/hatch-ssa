using System;
using pumpkin.display;

namespace SSGCore
{

	public interface Tutorial
	{
	
		DisplayObject View{ get; }
		void OnStart();
		void OnComplete();
		void Resize( int width, int height );
	}

}