using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;

namespace SSGCore
{

	public class Numeral_Recognition_Tutorial : Tutorial
	{
	
		private DisplayObjectContainer view = new DisplayObjectContainer();
		
		private LabeledButton card_1;
		private LabeledButton card_2;
		
		private MovieClip plattie_arm;
		
		private MovieClip plattie_forearm;
		private MovieClip plattie_hand;
		
		private List<LabeledButton> cards;
		
		private DisplayObjectContainer card_container;
				
		public Numeral_Recognition_Tutorial()
		{
				
			//SOUND ENGINE
			SoundEngine.Instance.OnAudioWord += OnAudioWord;
			
			cards = new List<LabeledButton>();
			
			card_1 = MovieClipFactory.CreateLabeledButton( MovieClipFactory.NUMERAL_RECOGNITION, "Card" );
			card_1.Label = "2";
			card_2 = MovieClipFactory.CreateLabeledButton( MovieClipFactory.NUMERAL_RECOGNITION, "Card" );
			card_2.Label = "4";
			
			cards.Add( card_1 );
			cards.Add( card_2 );
					
			card_container = new DisplayObjectContainer();
			card_container.width = Screen.width;
			card_container.height = Screen.height;
			view.addChild( card_container );
						
			plattie_arm = MovieClipFactory.CreateMovieClip( MovieClipFactory.NUMERAL_RECOGNITION, "Plattie_Arm" );
			view.addChild( plattie_arm );
			plattie_arm.x = card_container.x + card_1.x + (card_1.width * 0.5f);
			plattie_arm.y = Screen.height;
			
			plattie_forearm = plattie_arm.getChildByName<MovieClip>("bicep").getChildByName<MovieClip>("forearm");
			plattie_hand = plattie_forearm.getChildByName<MovieClip>("hand");
			
			LayoutUtils.LayoutTiled(card_container, cards);
			
			Resize( Screen.width, Screen.height );
			
			plattie_arm.rotation = -180;
			
			plattie_arm.x = card_container.x + card_1.x + 100;
			plattie_arm.y = card_1.y + plattie_arm.height + 500;
			
		}
		
		public void OnStart()
		{
			
			view.dispatchEvent( new TutorialEvent(TutorialEvent.START) );
						
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip_1 = SoundUtils.LoadGlobalSound("touch_to_play");
			AudioClip clip_2 = SoundUtils.LoadGlobalSound("touch_again");
			bundle.AddClip(clip_1);
			bundle.AddClip(clip_2, "TUTORIAL_BEGIN", clip_2.length);
			SoundEngine.Instance.PlayBundle(bundle);
									
		}
		
		public void OnComplete()
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
		}
		
		public void OnAudioWord( string word )
		{
			switch(word)
			{
				case "TUTORIAL_BEGIN":
					MoveArmToCard();
				break;
			}
		}
		
		private void rotateObject(DisplayObject obj, float rotation, float time, float delay)
		{
			Tweener.addTween(obj, Tweener.Hash("time",time, "delay",delay, "rotation",rotation, "transition",Tweener.TransitionType.easeOutQuad ));
		}
			
		private TweenerObj moveObject(DisplayObject obj, Vector2 pos, float time, float delay)
		{
			return Tweener.addTween(obj, Tweener.Hash("time",time, "delay",delay, "x",pos.x, "y",pos.y , "transition",Tweener.TransitionType.easeOutQuad ));
			
		}
		
		public void Resize( int width, int height )
		{
			view.width = width;
			view.height = height;
			card_container.FitToParent();
			plattie_arm.FitToParent();
		}
		
		public DisplayObject View
		{
			get{ return view; }
		}
		
		private void MoveArmToCard()
		{
			Tweener.addTween(plattie_arm, Tweener.Hash("time",0.5f, "delay",1, "x",card_container.x + card_1.x + 50, "y",card_1.y + plattie_arm.height , "transition",Tweener.TransitionType.easeOutQuad )).OnComplete( MoveArmAway );
		}
		
		private void MoveArmAway()
		{
			
			view.dispatchEvent( new SkillEvent(SkillEvent.CORRECT, true, false) );
						
			Debug.Log("moving away");
			Tweener.addTween(plattie_arm, Tweener.Hash("time",0.5f, "delay",0.5f, "rotation",-90, "x",plattie_arm.x - 500, "y",card_1.y + plattie_arm.height + 500 , "transition",Tweener.TransitionType.easeOutQuad )).OnComplete( OnComplete );
		}
		
	}
	
}