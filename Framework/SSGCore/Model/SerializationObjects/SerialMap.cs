using System;
using System.Collections.Generic;
using HatchFramework;
using LitJson;

namespace SSGCore
{
	public class SerialMap
	{
		public int student;
		public int skill;
		public int level;
		public List<SerialSessionMap> sessions;
		
		public JsonData toJson()
		{
			JsonData dataOut = new JsonData();
			
			dataOut["student"] = student;
			dataOut["skill"] = skill;
			dataOut["level"] = level;
			
			if( sessions != null && sessions.Count > 0 )
			{
				JsonData sessionsOut = new JsonData();
				foreach( SerialSessionMap ssm in sessions )
				{
					sessionsOut.Add( ssm.toJson() );
				}
				dataOut["sessions"] = sessionsOut;
			}
			else
				dataOut["sessions"] = new JsonData( null );
			
			return dataOut;
		}
		
		public string toString()
		{
			return toJson().ToJson();
		}
	}
}

