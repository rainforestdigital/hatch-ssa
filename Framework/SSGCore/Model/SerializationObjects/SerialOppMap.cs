using System;
using System.Collections.Generic;
using HatchFramework;
using LitJson;

namespace SSGCore
{
	public class SerialOppMap
	{
		public int count;
		public double start;
		public int audio;
		public List<SerialObjectMap> objects;
		public List<SerialActionMap> actions;
		
		public JsonData toJson()
		{
			JsonData dataOut = new JsonData();
			
			dataOut["count"] = count;
			dataOut["start"] = start;
			dataOut["audio"] = audio;
			
			if(objects != null && objects.Count > 0)
			{
				JsonData objOut = new JsonData();
				foreach( SerialObjectMap som in objects )
				{
					objOut.Add( som.toJson() );
				}
				dataOut["objects"] = objOut;
			}
			else
				dataOut["objects"] = new JsonData( null );
			
			if(actions != null && actions.Count > 0)
			{
				JsonData actOut = new JsonData();
				foreach( SerialActionMap sam in actions )
				{
					actOut.Add( sam.toJson() );
				}
				dataOut["actions"] = actOut;
			}
			else
				dataOut["actions"] = new JsonData( null );
			
			return dataOut;
		}
	}
}

