using System;
using System.Collections.Generic;
using HatchFramework;
using LitJson;

namespace SSGCore
{
	public class SerialSessionMap
	{
		public string time;
		public bool help;
		public bool teacher;
		public bool timed;
		public List<int> screen;
		public List<SerialOppMap> opps;
		
		public JsonData toJson()
		{
			JsonData dataOut = new JsonData();
			
			dataOut["time"] = time;
			dataOut["help"] = help;
			dataOut["teacher"] = teacher;
			dataOut["timed"] = timed;
			
			if( screen != null && screen.Count > 1 )
			{
				JsonData screenOut = new JsonData();
				screenOut.Add( new JsonData( screen[0] ) );
				screenOut.Add( new JsonData( screen[1] ) );
				dataOut["screen"] = screenOut;
			}
			else
				dataOut["screen"] = new JsonData( null );
			
			if( opps != null && opps.Count > 0 )
			{
				JsonData oppsOut = new JsonData();
				foreach( SerialOppMap som in opps )
				{
					oppsOut.Add( som.toJson() );
				}
				dataOut["opps"] = oppsOut;
			}
			else
				dataOut["opps"] = new JsonData( null );
			
			return dataOut;
		}
	}
}

