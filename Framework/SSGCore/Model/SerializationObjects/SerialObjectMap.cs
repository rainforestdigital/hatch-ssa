using System;
using System.Collections.Generic;
using HatchFramework;
using LitJson;

namespace SSGCore
{
	public class SerialObjectMap
	{
		public string id;
		public List<double> pos;
		public bool? correct;
		
		public JsonData toJson()
		{
			JsonData dataOut = new JsonData();
			
			dataOut["id"] = id;
			
			if( pos != null && pos.Count > 1 )
			{
				JsonData posObj = new JsonData();
				posObj.Add( new JsonData( pos[0] ) );
				posObj.Add( new JsonData( pos[1] ) );
				dataOut["pos"] = posObj;
			}
			else
				dataOut["pos"] = new JsonData( null );
			
			if( correct != null )
				dataOut["correct"] = correct;
			
			return dataOut;
		}
	}
}

