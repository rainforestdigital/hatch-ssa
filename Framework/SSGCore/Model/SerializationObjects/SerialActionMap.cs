using System;
using System.Collections.Generic;
using HatchFramework;
using LitJson;

namespace SSGCore
{
	public class SerialActionMap
	{
		public double time;
		public string type;
		public int obj;
		public List<double> pos;
		public bool? correct;
		public int audio;
		
		public JsonData toJson()
		{
			JsonData dataOut = new JsonData();
			
			dataOut["time"] = time;
			dataOut["type"] = type;
			
			switch(type){
			case "tap":
			case "snap":
			case "move":
				dataOut["obj"] = obj;
				if( pos != null && pos.Count > 1 )
				{
					JsonData posObj = new JsonData();
					posObj.Add( new JsonData( pos[0] ) );
					posObj.Add( new JsonData( pos[1] ) );
					dataOut["pos"] = posObj;
				}
				else
					dataOut["pos"] = new JsonData( null );
				if( correct != null )
					dataOut["correct"] = correct;
				break;
			case "time":
			case "comp":
			case "rtry":
				dataOut["audio"] = audio;
				break;
			}
			
			return dataOut;
		}
	}
}

