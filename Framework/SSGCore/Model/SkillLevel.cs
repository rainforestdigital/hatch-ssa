using System;

namespace SSGCore
{
	// This is represented as an enum for now rather than a full class from
	// data, since the skill selection logic has a number of special cases for
	// specific
	public enum SkillLevel
	{
		Tutorial,
		Emerging,
		Developing,
		Developed,
		Completed
	}
	
	public class SkillLevelUtil{
	
		public static SkillLevel GetLevel(string sl){
			switch(sl){
				case "TT": return SkillLevel.Tutorial;	
				case "EG": return SkillLevel.Emerging;	
				case "DG": return SkillLevel.Developing;	
				case "DD": return SkillLevel.Developed;	
				case "CD": return SkillLevel.Completed;	
				default: return SkillLevel.Tutorial;
			}
			
		}
		
		public static SkillLevel FromId(int id){
			switch(id){
				case 1: return SkillLevel.Tutorial;	
				case 2: return SkillLevel.Emerging;	
				case 3: return SkillLevel.Developing;	
				case 4: return SkillLevel.Developed;	
				case 5: return SkillLevel.Completed;	
				default: return SkillLevel.Tutorial;
			}
		}
		
		public static int FromId(SkillLevel level){
			return ((int)level) + 1;
		}
		
		public static string GetCode(SkillLevel level){
			switch(level){
				case SkillLevel.Tutorial: return "TT";	
				case SkillLevel.Emerging: return "EG" ;	
				case SkillLevel.Developing: return "DG";	
				case SkillLevel.Developed: return "DD";	
				case SkillLevel.Completed: return "CD";	
				default: return "TT";
			}
		}
	}
}

