using System;

namespace SSGCore
{
	public class SkillFamily
	{
		public string Name { get; private set; }
		public int ID {get; private set;}
		public int SPN_ID = -1;
		
		public SkillFamily (string name, int id)
		{
			Name = name;
			ID = id;
		}
		
		public SkillFamily(FamilyDB row){
			this.Name = row.Name;
			this.ID = row.ID;
		}
	}
}

