using System;

namespace SSGCore
{
	public class Admin
	{
		public string Name { get; set; }
		public string Organization { get; set; }
		public string PhoneNumber { get; set; }
		public string Email { get; set; }
		public Admin ()
		{
		}
		
		public Admin ( RMS.Webservice.Admin other )
		{
			Name = other.name;
			Organization = other.organization;
			PhoneNumber = other.phone;
			Email = other.email;
		}
	}
}

