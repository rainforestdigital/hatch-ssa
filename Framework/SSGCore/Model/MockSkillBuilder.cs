using System;
using System.Collections.Generic;

namespace SSGCore
{
	public class MockSkillBuilder
	{
		private List<SkillLevel> allLevels = new List<SkillLevel> { 
			SkillLevel.Tutorial,
			SkillLevel.Emerging,
			SkillLevel.Developing,
			SkillLevel.Developed,
			SkillLevel.Completed
		};
		
		private List<SkillFamily> families = new List<SkillFamily>();
		private List<Skill> skills = new List<Skill>();
		
		public List<SkillFamily> Families { get { return families; } }
		public List<Skill> Skills { get { return skills; } }
		
		private int nextId = 0;
		private int nextFamilyId = 0;
		
		public MockSkillBuilder ()
		{
		}
		
		void AddFamilyWithSkills(string familyName, params string[] skillNames)
		{
			nextFamilyId++;
			SkillFamily family = new SkillFamily(familyName, nextFamilyId);
			families.Add(family);
			
			Skill previousSkill = null;
			int familyLevel = -1;
			foreach(string name in skillNames) {
				nextId++;
				familyLevel++;
				Skill skill = new Skill(name, 
					family,
					previousSkill,
					allLevels,
					WeightsForFamilyLevel(familyLevel));
				skill.ID = nextId;
				skills.Add(skill);				
				previousSkill = skill;
			}
		}
		
		void SetFamilySPN( string familyName )
		{
			foreach( Skill s in skills )
			{
				if( s.Family.Name == familyName )
				{
					nextId++;
					s.SPN_ID = nextId;
				}
			}
			
			foreach( SkillFamily family in families )
			{
				if( family.Name == familyName )
				{
					nextFamilyId++;
					family.SPN_ID = nextFamilyId;
					return;
				}
			}
		}
		
		void addSkillToFamily( string familyName, string skillName )
		{
			Skill previousSkill = null;
			SkillFamily family = null;
			int familyLevel = -1;
			for( int i = 0; i < skills.Count; i++ )
			{
				if( skills[i].Family.Name == familyName )
				{
					previousSkill = skills[i];
					familyLevel++;
				}
			}
			if( previousSkill != null )
				family = previousSkill.Family;
			
			nextId++;
			Skill skill = new Skill(skillName, 
				family,
				previousSkill,
				allLevels,
				WeightsForFamilyLevel(familyLevel));
			skill.ID = nextId;
			skills.Add(skill);
		}
		
		private Dictionary<SkillLevel, int> WeightsForFamilyLevel(int familyLevel) {
			var dict = new Dictionary<SkillLevel, int>();
			foreach(var level in allLevels) {
				int unscaled = 10 - (int)level;
				dict[level] = Math.Max(1, (unscaled*unscaled*unscaled) / (familyLevel+1));
			}
			
			return dict;
		}
		
		public static MockSkillBuilder CreateForTest()
		{
			var builder = new MockSkillBuilder();
			builder.AddFamilyWithSkills("PhonologicalAwareness", 
				"SentenceSegmenting",
				"InitialSounds",
				"BlendingCompoundWords",
				"SegmentingCompoundWords",
				"OnsetRime"
			);
			builder.AddFamilyWithSkills("NumericalOperations", 
				"CountingFoundations",
				"NumeralRecognition",
				"SequenceCounting",
				"ObjectsInASet",
				"Addition",
				"Subtraction"
			);
			builder.AddFamilyWithSkills("LanguageDevelopment", 
				"LanguageVocabulary",
				"SpatialSkills",
				"Measurement"
			);
			builder.AddFamilyWithSkills("AlphabetKnowledge", 
				"LetterRecognition"
			);
			builder.AddFamilyWithSkills("LogicAndReasoning", 
				"CommonShapes",
				"Sorting",
				"Patterning"
			);
			
			builder.SetFamilySPN( "NumericalOperations" );
			builder.SetFamilySPN( "LanguageDevelopment" );
			builder.SetFamilySPN( "AlphabetKnowledge" );
			builder.SetFamilySPN( "LogicAndReasoning" );
			
			builder.addSkillToFamily( "PhonologicalAwareness", "DeletingOnsetAndRime" );
			builder.addSkillToFamily( "PhonologicalAwareness", "BlendingSoundsInWords" );
			builder.addSkillToFamily( "NumericalOperations", "AdditionUpToTen" );
			builder.addSkillToFamily( "NumericalOperations", "SubtractionFromTen" );
			
			return builder;
		}
		
		public static Skill CreateSkillForName(string name)
		{
			var builder = CreateForTest();
			var skill = builder.Skills.Find( s => s.linkage == name);
			if( skill == null ) {
				skill = new Skill(name, 
					new SkillFamily(name, -1),
					null,
					builder.allLevels,
					builder.WeightsForFamilyLevel(0));
			}
			
			return skill;
		}
	}
}

