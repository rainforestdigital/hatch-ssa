using System;

namespace SSGCore
{
	public class Theme
	{
		public string themeName;
		public string title = "";
		public int ID{get; private set;}
		
		public Theme (string baseName)
		{
			this.themeName = baseName;
			this.buttonSymbol = Main.SWF + ":" + baseName + "_Button";
			this.soundPath = baseName.ToLowerInvariant();
		}
		
		public Theme(ThemeDB row):this(row.Name){
		
			this.themeName = row.Name;
			this.title = row.Title;
			this.ID = row.ID;
		}
		
		// current assumption is that a reference to
		// a loaded symbol for the button can be derived
		// from theme data
		private string buttonSymbol;
		public string ButtonSymbol
		{
			get { return buttonSymbol; }
		}
		
		private string soundPath;
		public string SoundPath
		{
			get { return soundPath; }
		}
	}
}

