using System;
using UnityEngine;
using System.IO;
using System.Collections.Generic;
using HatchFramework;

namespace SSGCore
{
	public class ImageCache
	{
		private class Entry
		{
			public Texture2D texture = new Texture2D(0, 0, TextureFormat.RGB24, false);
			public DateTime accessDate = DateTime.MinValue;
			public bool marked = false;
		}
		
		private Dictionary<string, Entry> cache = new Dictionary<string, Entry>();
		readonly string folder = Path.Combine(PlatformUtils.InternalDataPath, GlobalConfig.GetProperty<string>("LOCAL_DIR"));
		
		public ImageCache ()
		{
			
		}
		
		public Texture2D GetTexture(string key)
		{
			if(key == null) {
				return null;
			}
			Entry entry = EntryForKey(key);
			UpdateEntry (key, entry);
			
			return entry.texture;
		}
		
		public void ReloadCache()
		{
			foreach(var pair in cache) {
				UpdateEntry(pair.Key, pair.Value);
			}
		}
		
		public void Destroy()
		{
			foreach(var entry in cache.Values) {
				UnityEngine.Object.Destroy(entry.texture);
			}
			cache.Clear();
		}
		
		public void Mark(string key)
		{
			if(key == null) {
				return;
			}
			
			Entry entry = null;
			if(cache.TryGetValue(key, out entry)) {
				entry.marked = true;
			}
		}
		
		public void Sweep()
		{
			var keysToRemove = new List<string>();
			foreach(var pair in cache) {
				var entry = pair.Value;
				if(!entry.marked) {
					UnityEngine.Object.Destroy(entry.texture);
					keysToRemove.Add(pair.Key);
				}
				entry.marked = false;
			}
			
			foreach(var key in keysToRemove) {
				cache.Remove(key);
			}
		}
		
		private string PathForKey(string key)
		{
			if(key.StartsWith("/")) {
				key = key.Substring(1);
			}
			
			return Path.Combine(folder, key);
		}
		
		private Entry EntryForKey(string key)
		{
			if(!cache.ContainsKey(key)) {
				var entry = new Entry();
				entry.texture.name = Path.GetFileName(key);
				cache[key] = entry;
			}
			return cache[key];
		}

		void UpdateEntry (string key, Entry entry)
		{
			string path = PathForKey(key);
			var file = new FileInfo(path);
			if(!file.Exists) {
				DebugConsole.LogWarning("Image does not exist: {0}", path);
				return;
			}
			
			var lastWriteTime = file.LastWriteTime;
			if(lastWriteTime > entry.accessDate) {
				var imageData = File.ReadAllBytes(path);
				bool success = entry.texture.LoadImage(imageData);
				if(!success) {
					DebugConsole.LogError("Failed to load image: {0}", path);
				}
				entry.accessDate = lastWriteTime;
			}
		}
	}
}

