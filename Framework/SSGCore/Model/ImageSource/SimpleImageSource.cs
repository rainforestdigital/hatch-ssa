using System;
using UnityEngine;

namespace SSGCore
{
	public class SimpleImageSource : ImageSource
	{
		Texture texture;
		public SimpleImageSource (Texture texture)
		{
			this.texture = texture;
		}

		public Texture GetImage ()
		{
			return texture;
		}
	}
}

