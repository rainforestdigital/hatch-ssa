using System;
using UnityEngine;

namespace SSGCore
{
	public interface ImageSource
	{
		Texture GetImage();
	}
}

