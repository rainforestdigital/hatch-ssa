using System;
using UnityEngine;

namespace SSGCore
{
	public class DemoImageSource : ImageSource
	{
		private string path;
		Texture cachedTexture;
		
		public DemoImageSource (String path)
		{
			this.path = path;
		}

		public Texture GetImage ()
		{
			if(cachedTexture == null) {
				cachedTexture = (Texture2D)Resources.Load(path, typeof(Texture2D));
			}
			return cachedTexture;
		}
	}
}

