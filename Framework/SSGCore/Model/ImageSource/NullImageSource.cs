using System;
using UnityEngine;

namespace SSGCore
{
	public class NullImageSource : ImageSource
	{
		private NullImageSource ()
		{
		}
		
		private static NullImageSource instance = new NullImageSource();
		public static NullImageSource Instance
		{
			get { return instance; }
		}
		
		public Texture GetImage()
		{
			return null;
		}
	}
}

