using System;

namespace SSGCore
{
	public class CacheImageSource : ImageSource
	{
		private ImageCache cache;
		private string path;
		
		public CacheImageSource (ImageCache cache, string path)
		{
			this.cache = cache;
			this.path = path;
		}

		public UnityEngine.Texture GetImage ()
		{
			return cache.GetTexture(path);
		}
	}
}

