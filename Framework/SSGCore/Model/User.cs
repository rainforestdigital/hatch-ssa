using System;
using UnityEngine;

namespace SSGCore
{
	// Remove this if it ends up that Child is the only thing that implements it
	public interface User
	{
		string Name { get; }
		Texture Texture { get; }
		string FirstName{ get; }
		string LastName{ get; }
		string BirthDate{ get; }
		string CustomId { get; }
		int ID{get;}
		bool HasImage{get;}
	}
}

