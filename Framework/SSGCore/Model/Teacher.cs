using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SSGCore;

public class Teacher : Child
{

	public Teacher(int ID, string firstName, string lastName, ImageSource texture, string birthDate, string customId, string uid ):base(ID, firstName, lastName,texture,birthDate,customId, uid)
	{
	
	}
	
	// for compatibility with test constructors
	public Teacher(int ID, string name, ImageSource texture):base(ID, name, texture)
	{
		
	}
	
}

