using System;
using HatchFramework;
using LitJson;

namespace SSGCore
{
	public class SessionInfo{
		public static bool FORCE_SPN = false;
		
		public bool teacherMode;
		public bool demoMode;
		public bool helpMode;
		public bool sessionResume;
		public SkillLevelPair skillPair;
		public SkillFamily family;
		public Child user;
		public Theme theme;
		
//		public JsonData SerializedData;
		public SerialMap SerializedData;
		
		public double gameTime;
		public bool paused;
		
		public int correct;
		public int incorrect;
		public int totalOpportunities;
		
		public int initialOpportunity;
		
		public double StartTime;
		public Classroom classroom;
		
		private double intervalStart;
		private bool timing;
		
		/// <summary>
		/// Arbitrary json data the session might want saved
		/// </summary>
		public string sessionData;
		public string themeData;
		public bool skipIntro;
		public bool skipHelpFlag;
		
		public bool SPN_FLAG = false;
		
		public void StartTiming() {
			if(!timing) {
				intervalStart = DateUtils.ConvertToUnixTimestamp(DateTime.UtcNow);
				timing = true;
			}
		}
		
		public double currentTime {
			get{
				if(!timing) return gameTime;
				var now = DateUtils.ConvertToUnixTimestamp(DateTime.UtcNow);
				return gameTime + (now - intervalStart);
			}
		}
		
		public double intervalTime{
			get{
				double raw = currentTime;
				return (double)(UnityEngine.Mathf.FloorToInt((float)raw*100f)/100f);
			}
		}
		
		public void StopTiming() {
			if(timing) {
				gameTime = currentTime;
				timing = false;
			}
		}
		
		public float Percent {
			get {
				return ((float)correct / (float)(correct+incorrect)) * 100;
			}
		}
		
		public float Result {
			get {
				return ((float)correct / (float)(correct+incorrect));
			}
		}
		
		public SerialSessionMap currentSerializedSession {
			get {
				return SerializedData.sessions[SerializedData.sessions.Count - 1];
			}
		}
		
		public int lastSessionCount{
			get {
				return SerializedData.sessions.Count - 1;
			}
		}
		
		public void ResetSessionStats(){
		
			gameTime = 0;
			paused = false;
			correct = 0;
			incorrect = 0;
			totalOpportunities = 0;
			StartTime = DateUtils.ConvertToUnixTimestamp(DateTime.UtcNow);
			intervalStart = StartTime;
			initialOpportunity = 0;
			timing = true;
			sessionResume = false;
			sessionData = "";
			themeData = "";
			skipHelpFlag = false;
		}
		
		public void CopyFrom(SessionInfo other)
		{
			this.teacherMode = other.teacherMode;
			this.demoMode = other.demoMode;
			this.helpMode = other.helpMode;
			this.sessionResume = other.sessionResume;
			this.skillPair = other.skillPair;
			this.family = other.family;
			this.user = other.user;
			//this.theme = other.theme;
			this.SerializedData = other.SerializedData;
			this.gameTime = other.gameTime;
			this.paused = other.paused;
			this.correct = other.correct;
			this.incorrect = other.incorrect;
			this.totalOpportunities = other.totalOpportunities;
			this.initialOpportunity = other.initialOpportunity;
			this.StartTime = other.StartTime;
			this.classroom = other.classroom;
			this.intervalStart = other.intervalStart;
			this.timing = other.timing;
			this.sessionData = other.sessionData;
			this.themeData = other.themeData;
			this.skipIntro = other.skipIntro;
			this.SPN_FLAG = other.SPN_FLAG;
			this.skipHelpFlag = other.skipHelpFlag;
		}
		
		public SessionInfo Clone(){
			SessionInfo clone = new SessionInfo();
			clone.CopyFrom(this);
			return clone;
		}
		
		public string Serialize()
		{
			string data = skillPair.Skill.linkage + ";" +
				((int)skillPair.Level).ToString() + ";" +
				currentTime.ToString() + ";" +
				correct.ToString() + ";" +
				incorrect.ToString() + ";" +
				totalOpportunities.ToString() + ";" +
				initialOpportunity.ToString() + ";" +
				StartTime.ToString() + ";" +
				SerializedJsonToString();
			
			return data;
		}
		
		public void Deserialize(string data, string themeIn, string skillIn)
		{
			var sData = data.Split(';');
			
			teacherMode = false;
			demoMode = false;
			helpMode = false;
			sessionResume = true;
			skillPair = new SkillLevelPair( MockSkillBuilder.CreateSkillForName(sData[0]), (SkillLevel)(int.Parse(sData[1])) );
			family = skillPair.Skill.Family;
			gameTime = double.Parse(sData[2]);
			paused = false;
			correct = int.Parse(sData[3]);
			incorrect = int.Parse(sData[4]);
			totalOpportunities = int.Parse(sData[5]);
			initialOpportunity = int.Parse(sData[6]);
			StartTime = double.Parse(sData[7]);
			
			if( sData.Length > 8 )
				StringtoSerializedJson( sData[8] );
			
			intervalStart = DateUtils.ConvertToUnixTimestamp(DateTime.UtcNow);
			
			sessionData = skillIn;
			themeData = themeIn;
		}
		
		public void ClearSerialization()
		{
			sessionResume = false;
			initialOpportunity = 0;
			sessionData = "";
			themeData = "";
		}
		
		public string SerializedJsonToString()
		{
			string dataOut = SerializedData.toString();
			
			//escape ";"
			dataOut = dataOut.Replace( ";", "+s+" );
			
			return dataOut;
		}
		
		private void StringtoSerializedJson( string dataIn )
		{
			//escaped ";"
			dataIn = dataIn.Replace( "+s+", ";" );
			//cover for bad data in previous itteration
			dataIn = dataIn.Replace( "\"opps\":}", "\"opps\":null}" );
			
			SerializedData = JsonMapper.ToObject<SerialMap>( dataIn );
		}
	}
}

