using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SSGCore;
using LitJson;

public class Child : User
{
		
	public int ID{get; set;}
	
	private string _firstName;
	public string FirstName{ get{ return _firstName; } }
	
	private string _lastName;
	public string LastName{ get{ return _lastName; } }
	
	private string _name;
	public string Name{ get{ return _name; } }
	
	private string _abbreviatedName;
	public string AbbreviatedName{ get{ return _abbreviatedName; } }
	
	private string _birthDate;
	public string BirthDate{ get{ return _birthDate; } }
	
	private string _bookmark;
	public JsonData Bookmark{ get{ return string.IsNullOrEmpty(_bookmark) ? null : JsonMapper.ToObject(_bookmark); } }
	public string BookmarkRaw{ get{ return _bookmark; } }
	
	bool _hasImage = true;
	public bool HasImage { 
		get { return _hasImage; }
		set { _hasImage = value; }
	}
	
	private Texture _texture;
	private ImageSource _imageSource;
	public ImageSource ImageSource {
		get { return _imageSource; }
		set {
			_imageSource = value;
			_texture = null;
		}
	}
	
	public Texture Texture { 
		get { 
			if(_texture == null && _imageSource != null) { 
				_texture = _imageSource.GetImage();
			}
			return _texture;
		}
	}
	
	private string _customId = "";
	public string CustomId { 
		get { return _customId; }
	}
	
	private System.Action<Child> changeHandler;
	public event System.Action<Child> Change
	{
		add { changeHandler += value; }
		remove { changeHandler -= value; }
	}
	
	private string _uid = "";
	public string UID{
		get{
			if(string.IsNullOrEmpty(_uid) || "0" == _uid) {
				return null;
			}
			return _uid;
		}
	}
	
	private LanguageProfile _profile = LanguageProfile.English;
	public LanguageProfile Profile{
		get { return _profile; }	
	}
	
	private void BuildName() 
	{
		_name = _firstName + " " + _lastName;
		_abbreviatedName = _firstName + " " + _lastName.Substring(0, 1);
	}
		
	/// <summary>
	/// Initializes a new instance of the <see cref="Child"/> class. 
	/// This class is a value holder for child data.
	/// </summary>
	/// <param name='_name'>
	/// Name of the Child.
	/// </param>
	/// <param name='_age'>
	/// Age of the Child.
	/// </param>
	public Child(int id, string firstName, string lastName, ImageSource source, string birthDate, string customId , string uid)
	{
		_uid = uid;
		ID = id;
		_firstName = firstName;
		_lastName = lastName;
		_imageSource = source;
		_birthDate = birthDate;
		_customId = customId;
		_bookmark = null;
		
		BuildName();
	}
	public Child(int id, string firstName, string lastName, ImageSource source, string birthDate, string customId , string uid, string bookmark)
	{
		_uid = uid;
		ID = id;
		_firstName = firstName;
		_lastName = lastName;
		_imageSource = source;
		_birthDate = birthDate;
		_customId = customId;
		_bookmark = bookmark;
		
		BuildName();
	}
	
	public Child(int id, string firstName, string lastName, ImageSource source, string birthDate, string customId , string uid, string bookmark, int profile)
	{
		_uid = uid;
		ID = id;
		_firstName = firstName;
		_lastName = lastName;
		_imageSource = source;
		_birthDate = birthDate;
		_customId = customId;
		_bookmark = bookmark;
		_profile = LanguageProfileUtility.FromId( profile );
		
		BuildName();
	} 
	
	public void UpdateFrom(UserDB user)
	{
		ID = user.ID;
		_uid = user.UID;
		
		bool dirty = false;
		if(_firstName != user.FirstName) {
			_firstName = user.FirstName;
			dirty = true;
		}
		
		if(_lastName != user.LastName) {
			_lastName = user.LastName;
			dirty = true;
		}
		
		if(dirty) {
			BuildName();
		}
		
		_bookmark = user.Bookmark;
		
		_profile = LanguageProfileUtility.FromId( user.Profile );
		
		_birthDate = user.BirthDate;
		_customId = user.CustomID;
	}

	// for compatibility with test constructors
	public Child(int id, string name, ImageSource imageSource)
	{
		ID = id;
		_name = name;
		string[] nameParts = name.Split(new char[] { ' '}, 2);
		_firstName = nameParts[0];
		_lastName = nameParts[1];
		_imageSource = imageSource;
		_birthDate = "01/01/2009";
		BuildName();
	}

	// for compatibility with test constructors
	public Child(int id, string name, ImageSource imageSource, LanguageProfile profile)
	{
		ID = id;
		_name = name;
		string[] nameParts = name.Split(new char[] { ' '}, 2);
		_firstName = nameParts[0];
		_lastName = nameParts[1];
		_imageSource = imageSource;
		_birthDate = "01/01/2009";
		_profile = profile;
		BuildName();
	}
	
	public override string ToString ()
	{
		return AbbreviatedName;
	}
	
}

