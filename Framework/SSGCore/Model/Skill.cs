using System;
using System.Collections.Generic;

namespace SSGCore
{
	public class Skill
	{
		public int ID{get;set;}
		public int SPN_ID;
		public string swf;
		public string linkage;
		
		public SkillFamily Family { get; private set; }
		public Skill Predecessor { get; private set; }
		public bool FirstInFamily { get { return Predecessor == null; } }
		
		private IList<SkillLevel> levels;
		private IDictionary<SkillLevel, int> weights;
		
		public Skill (string linkage)
		{
			
			this.linkage = linkage;
		}
		
		public Skill (string linkage, SkillFamily family, Skill predecessor, IList<SkillLevel> levels, IDictionary<SkillLevel, int> weights)
		{
			this.linkage = linkage;
			Family = family;
			Predecessor = predecessor;
			this.levels = levels;
			this.weights = weights;
		}
		
		public static Skill FromName(string name, SkillFamily family, Skill predecessor, IList<SkillLevel> levels, IDictionary<SkillLevel, int> weights)
		{
			var linkage = LinkageFromName(name);
			var skill = new Skill(linkage);
			skill.linkage = linkage;
			skill.Family = family;
			skill.Predecessor = predecessor;
			skill.levels = levels;
			skill.weights = weights;
			return skill;
		}
		
		private static string LinkageFromName(string name) {
			var words = name.Split(' ');
			for (int i = 0; i < words.Length; ++i) {
				words[i] = CapitalizeFirstLetter(words[i]);
			}
			var linkage = String.Join("", words);
			
			// special hack for spatial skills spelling
			/*if(linkage == "SpatialSkills") {
				linkage = "SpacialSkills";
			}*/

			return linkage;
		}
		
		private static string CapitalizeFirstLetter(string word) {
			if(word.Length == 0 || Char.IsUpper(word[0])) {
				return word;
			}
			
			return word.Substring(0, 1).ToUpper() + word.Substring(1);
		}
		
		public bool IsLastLevel(SkillLevel level)
		{
			return levels[levels.Count-1] == level;
		}
		
		public SkillLevel LevelAfter(SkillLevel level)
		{
			var index = levels.IndexOf(level);
			if(index < 0 || IsLastLevel(level)) {
				throw new ArgumentException("Skill level not in Skill, or is last level");
			}
			return levels[index+1];
		}
		
		public SkillLevel LevelBefore(SkillLevel level)
		{
			var index = levels.IndexOf(level);
			if(index < 1) {
				throw new ArgumentException("Skill level not in Skill, or is first level");
			}
			return levels[index-1];
		}
		
		public int WeightAtLevel(SkillLevel level)
		{
			return weights[level];
		}
		
		public override string ToString ()
		{
			return linkage;
		}
	}
}

