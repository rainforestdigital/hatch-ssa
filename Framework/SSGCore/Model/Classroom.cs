using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

using LitJson;

namespace SSGCore
{

	public class Classroom
	{
		public IList<User> Children
		{ 
			get; 
			private set; 
		}
		
		private ImageSource teacherImageSource;
		public ImageSource TeacherImageSource 
		{
			get { return teacherImageSource; }
			set { teacherImageSource = value; }
		}
		public Texture TeacherImage
		{
			get { return teacherImageSource.GetImage(); }
		}
		
		public string Name
		{
			get;
			set;
		}
		
		private ImageSource imageSource;
		
		public ImageSource ImageSource 
		{
			get { return imageSource; }
			set {imageSource = value; }
		}
		
		public bool HasChildren
		{
			get { return Children.Count > 0 && Children.Any(c => c.HasImage); }
		}
		
		public Texture Image
		{
			get { return imageSource.GetImage(); }
		}
		
		public ICollection<Theme> Themes
		{
			get;
			private set;
		}
		
		public int ID {get; private set;}
		
		private bool valid = true;
		public bool Valid { get { return valid; } }
		
		public void Invalidate()
		{
			valid = false;
		}
		
		
		public Classroom( string name, ImageSource image, ImageSource teacherImage, IList<User> children, ICollection<Theme> themes)
		{
			Name = name;
			imageSource = image;
			teacherImageSource = teacherImage;
			Children = children;
			Themes = themes;
		}
		
		public Classroom( int id, string name, ImageSource image, ImageSource teacherImage, IList<User> children, ICollection<Theme> themes)
		{
			ID = id;
			Name = name;
			imageSource = image;
			teacherImageSource = teacherImage;
			Children = children;
			Themes = themes;
		}
		
		public override string ToString ()
		{
			return Name;
		}
		
	}
	
}

