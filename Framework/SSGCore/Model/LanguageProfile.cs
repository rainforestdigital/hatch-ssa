using System;

namespace SSGCore
{
	public enum LanguageProfile
	{
		English,
		Spanish,
		SpanishMath,
		SpanishLit
	}
	
	public class LanguageProfileUtility
	{
		public static LanguageProfile FromId(int id){
			switch(id){
				case 1: return LanguageProfile.English;	
				case 2: return LanguageProfile.Spanish;	
				case 3: return LanguageProfile.SpanishMath;	
				case 4: return LanguageProfile.SpanishLit;
				default: return LanguageProfile.English;
			}
		}
		
		public static int FromId(LanguageProfile profile){
			return ((int)profile) + 1;
		}
	}
}

