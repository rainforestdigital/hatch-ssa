using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using pumpkin.display;
using pumpkin.events;
using pumpkin.swf;
using System;
using System.Linq;
using pumpkin.tweener;
using pumpkin.text;
using HatchFramework;

namespace SSGCore
{
	//public enum AnimalTypes {ELEPHANT, CHEETAH, GIRAFFE, LION, ZEBRA, HYENA, GAZELLE, MEERKAT, HIPPO, RHINO}
	public enum _photoTypes {WASHINGTON, WHITEHOUSE, SMITHSONIAN, LINCOLN, CAPITOL, JEFFERSON, PENTAGON, LIBRARY, BUS, METRO}

	public class TravelThemeView : ThemeView
	{
		public string[] _photoNames = new string[]{"Washington Monument", "White House", "Smithsonian", "Lincoln Memorial", "Capitol Building", "Jefferson Memorial", "Pentagon", "Library of Congress", "Bus tour", "Metro"};
	
		private int[] _photoNumbers;
		private int[] _photosHorizontal = new int[]{1, 3, 5, 6, 7, 8, 9};
		private int[] _photosVertical = new int[]{0, 2, 4};
		private List<MovieClip> _photoClips;
		private MovieClip _photoClip;
		private int _photoNumber;
		private int _photosCollected = 0;
		private MovieClip _photoContainer;
		private MovieClip _placeholderClip;
		private MovieClip background;
		private MovieClip bgContainer;
		
		private List<MovieClip> photoList;
		private List<MovieClip> circlingPhotos;
		private List<MovieClip> placeholderClips;

		MovieClip photoUp;
		
		public int payoffCount;
		public int opportunitiesCompleted;
		
		public Dictionary<AnimalTypes, MovieClip> animalClips;
		
		private float angle = 0;
		private bool oscillate = false;


		
		public TravelThemeView ()
		{
			Init ();
		}
		
		public TravelThemeView(string swf, string symbol):base(swf, symbol)
		{
			Init ();
		}
		
		public override void Init()
		{
			
			base.Init();
			
			bgContainer = getChildByName<MovieClip>("bgContainer");
			background = MovieClipFactory.GetTravelBackground();
			bgContainer.addChild(background);
			bgContainer.x = bgContainer.y = 0; //have to set to zero's - for some reason it was being set to the book's position inside of the bgContainer.  weird.

			Tweener.addTween(mc_overlay, Tweener.Hash("time", 0.35, "alpha", 0, "visible", false, "transition", Tweener.TransitionType.easeOutQuad));
			
			//DESERIALIZATION POINT
			//Badge Randomization (photo numbers)
			randomizePhotos();
			
			//DESERIALIZATION POINT
			//Counts
			payoffCount = 1;
			opportunitiesCompleted = 1;

			DebugConsole.Log("Travel View || Overlay child index: {0}", this.getChildIndex(mc_overlay));
			
			_photoClips = new List<MovieClip>();

			_photoContainer = getChildByName<MovieClip>("photo_container");
			_photoContainer.scaleX = _photoContainer.scaleY = 1;
			_photoContainer.x = _photoContainer.y = 0;
			//_photoContainer.x = 200;
			
			//book_mc = getChildByName<MovieClip>("book_mc");
			//bookAnim_mc = getChildByName<MovieClip>("bookAnim_mc");
			//bookAnim_mc.stopAll();
			//bookAnim_mc.visible = false;
			photoUp = getChildByName<MovieClip>("photoUp");
			photoUp.stopAll();
			//photoZoomed = getChildByName<MovieClip>("zoomed");
			//photoZoomed.stopAll();
			
			setPhotoList();
			
			circlingPhotos = new List<MovieClip>();
			for(int i=1; i<=5; i++)
			{
				circlingPhotos.Add(this.getChildByName<MovieClip>("circling"+i));
			}
			
			placeholderClips = new List<MovieClip>();
			for(int i=1; i<=5; i++)
			{
				placeholderClips.Add(this.getChildByName<MovieClip>("placeholder"+i));
			}

			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			DebugConsole.Log("Travel View 4");
			
			if(!GlobalConfig.GetProperty<bool>("MuteThemeSong")){
				AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.TRAVEL_THEME, TravelThemeController.THEME_SONG);
				SoundEngine.Instance.PlayTheme(clip);
			}
			
			this.scaleX = (float)Screen.width/mc_overlay.width;
			this.scaleY = (float)Screen.height/mc_overlay.height;
			
			//root.removeChild(mc_overlay);
			//root.addChild(mc_overlay);			
		}

		private void randomizePhotos()
		{
			System.Random rnd = new System.Random();
			for (int i = _photosHorizontal.Length; i > 1; i--) {
				int pos = rnd.Next(i);
				var x = _photosHorizontal[i - 1];
				_photosHorizontal[i - 1] = _photosHorizontal[pos];
				_photosHorizontal[pos] = x;
			}
			for (int i = _photosVertical.Length; i > 1; i--) {
				int pos = rnd.Next(i);
				var x = _photosVertical[i - 1];
				_photosVertical[i - 1] = _photosVertical[pos];
				_photosVertical[pos] = x;
			}
			_photoNumbers = new int[]{_photosVertical[0], _photosVertical[1], _photosHorizontal[0], _photosHorizontal[1], _photosHorizontal[2]};
		}

		private void setPhotoList()
		{
			photoList = new List<MovieClip>();


			for(int i=1; i<=5; i++)
			{
				//photoList.Add(this.getChildByName<MovieClip>("photo"+i));
				MovieClip newPhoto = MovieClipFactory.CreateTravelClip("Photo");
				photoList.Add(newPhoto);
				newPhoto.x = -(newPhoto.width + 200);
				newPhoto.getChildByName<MovieClip>("textfield_mc").visible = false;
				//addChild (newPhoto);
				DebugConsole.LogWarning("Tavel Theme - Init || newPhoto scale: {0}", newPhoto.scaleX);
				addChildAt(newPhoto, this.getChildIndex(tutorialBG_mc)-1);
			}

		}

		private void OnAudioWordComplete( string word )
		{
			Debug.Log("####### audio word complete: "+word);
			if(word == "PAYOFF_100")
			{
				if(currentSession.helpMode)
				{
					dispatchEvent(new ThemeEvent(ThemeEvent.COMPLETE, true, false));
					return;
				}
				PhotoCollect();
				ShowOverlay();
			}
			
			if(word == "PAYOFF_50")
			{
				dispatchEvent(new ThemeEvent(ThemeEvent.COMPLETE, true, false));
				if(opportunitiesCompleted <= 10) ShowOverlay();
			}
			if(word == "THEME_END")
			{
				dispatchEvent( new SkillEvent( SkillEvent.SKILL_COMPLETE, true, false ) );
			}
			
			//Game starting!
			if(word == "INTRO")
			{
				if( currentSession.skillPair.Level == SkillLevel.Tutorial ) ShowTutorialBG();
				else ShowOverlay();
			}
			
			if(word == "THEME_INTRO")
			{
				// these games do not have an intro audio, so their showing of the tutorial/overlay is handled after THEME_INTRO
				if( (currentSession.skillPair.Skill.linkage == "Sorting" ||
					 currentSession.skillPair.Skill.linkage == "SegmentingCompoundWords" ||
					 currentSession.skillPair.Skill.linkage == "BlendingCompoundWords") && currentSession.skillPair.Level == SkillLevel.Tutorial ) ShowTutorialBG();
				else if( (currentSession.skillPair.Skill.linkage == "SegmentingCompoundWords" ||
						  currentSession.skillPair.Skill.linkage == "BlendingCompoundWords") && currentSession.skillPair.Level != SkillLevel.Tutorial ) ShowOverlay();
				else HideOverlay();
			}
			
			
		}
		
		public override void ResetTheme()
		{
			deserialized = false;

			DebugConsole.LogError ("RESETTING THEME");

			payoffCount = 1;
			opportunitiesCompleted = 1;
			_photosCollected = 0;
			
			removeEventListener(CEvent.ENTER_FRAME, waveRewards);

			photoUp.gotoAndStop("vertical");

			foreach(MovieClip photo in photoList)
			{
				photo.visible = false;
				photo.scaleX = photo.scaleY = 1;
			}
			
			for(int i = 0; i < _photoClips.Count; i++)

			{
				_photoClips[i].alpha = 0;
				_photoClips[i].scaleX = _photoClips[i].scaleY = 1;
				_photoClips[i].getChildByName<MovieClip> ("text_bg").removeAllChildren();
			}
			_photoClips.Clear();

			randomizePhotos();
			setPhotoList();

		}
		
		/*
		public void openBook()
		{
			bookAnim_mc.gotoAndPlay("IN");
			Tweener.addTween(book_mc, Tweener.Hash("time", 0.35f, "delay", 0.5f, "alpha", 1.0f, "transition", Tweener.TransitionType.easeOutQuad));
			foreach(MovieClip placeholder in placeholderClips)
			{
				Tweener.addTween(placeholder, Tweener.Hash("time", 0.35f, "delay", 0.5f, "alpha", 1.0f, "transition", Tweener.TransitionType.easeOutQuad));
			}
		}
		
		public void bookOut()
		{
			bookAnim_mc.gotoAndPlay("OUT");
		}
		
		public void closeBook()
		{
			Tweener.addTween(book_mc, Tweener.Hash("time", 0.35f, "alpha", 0.0f, "transition", Tweener.TransitionType.easeOutQuad)).OnComplete(bookOut);
			foreach(MovieClip placeholder in placeholderClips)
			{
				Tweener.addTween(placeholder, Tweener.Hash("time", 0.35f, "alpha", 0.0f, "transition", Tweener.TransitionType.easeOutQuad));
			}
		}
		
		public void resetBook()
		{
			Tweener.addTween(this, Tweener.Hash("time", 1.0f)).OnComplete(openBook);
			Tweener.addTween(book_mc, Tweener.Hash("time", 0.35f, "alpha", 0.0f, "transition", Tweener.TransitionType.easeOutQuad)).OnComplete(bookOut);
			foreach(MovieClip placeholder in placeholderClips)
			{
				Tweener.addTween(placeholder, Tweener.Hash("time", 0.35f, "alpha", 0.0f, "transition", Tweener.TransitionType.easeOutQuad));
			}
		}
		*/
	
		public override void CorrectOpportunity( bool correct )
		{
			base.CorrectOpportunity(correct);
			
			if(opportunitiesCompleted <= 10)
			{
				
				opportunitiesCompleted++;
				DebugConsole.LogWarning ("CorrectOpportunity || opportuniriescompleted++");

				if(!correct)
				{
					dispatchEvent(new ThemeEvent(ThemeEvent.COMPLETE, true, false));
					//ShowOverlay();
					return;
				}
				
				HideOverlay();
				DebugConsole.LogWarning ("CorrectOpportunity || hide overlay");
				payoffCount++;
				if(payoffCount%2 ==0)
				{
					DebugConsole.LogWarning ("CorrectOpportunity || payoffcount % 2 = 0");
					//if Help Mode
					if(currentSession.helpMode)
					{
						OnAudioWordComplete("PAYOFF_50");
						return;
					}

					DebugConsole.LogWarning ("CorrectOpportunity || payoff 50");

					_photoNumber = _photoNumbers[Mathf.FloorToInt((payoffCount-1)/2)];
					_photoClip = photoList[Mathf.FloorToInt((payoffCount-1)/2)];
					_photoClip.getChildByName<MovieClip>("photos").getChildByName<MovieClip>("frame").visible = false;
					_placeholderClip = getChildByName<MovieClip>("placeholder"+(payoffCount/2));
					
					_photoClip.x = photoUp.x;
					_photoClip.y = photoUp.y;
					
					_photoClip.rotation = photoUp.rotation;
					_photoClip.visible = true;
					_photoClip.alpha = 1;
					_photoClip.stopAll();

					DebugConsole.LogWarning ("CorrectOpportunity || _photoClip defined");

					int i = 0;
					
					float xPos = 0;
					float yPos = 0;
					
					float hardWidth = 550;
					
					if (_photoNumber == 0 || _photoNumber == 2 || _photoNumber == 4){
						_photoClip.gotoAndStop("showHalfVertical");
						DebugConsole.LogWarning ("CorrectOpportunity || _photoClip vertical");
						_photoClip.getChildByName<MovieClip>("photos").getChildByName<MovieClip>("frame").visible = false;
						hardWidth = _photoClip.width - (_photoClip.width * 0.12f);

						DebugConsole.Log ("photoUp.width: {0}, hardWidth: {1} ", _photoClip.width, hardWidth);

						for(i = 1; i <= 6; i++)
						{
							
							if(i == 1 || i == 4 || i == 5)
							{
								switch(i)
								{
									case 1:
									xPos = _photoClip.getChildByName<MovieClip>("puzzle2").x;
									yPos = _photoClip.getChildByName<MovieClip>("puzzle1").y;
									break;
									case 4:
									xPos = _photoClip.getChildByName<MovieClip>("puzzle1").x;
									yPos = _photoClip.getChildByName<MovieClip>("puzzle2").y;
									break;
									case 5:
									xPos = _photoClip.getChildByName<MovieClip>("puzzle1").x;
									yPos = _photoClip.getChildByName<MovieClip>("puzzle3").y;
									break;
								}
								
								MovieClip puzzlePiece = MovieClipFactory.CreateTravelClip( "Puzzle_" + i );
								DebugConsole.LogWarning("puzzlePiece is null: {0}", (puzzlePiece == null));
								_photoClip.addChild( puzzlePiece );
								puzzlePiece.x = xPos;
								puzzlePiece.y = yPos;
								Tweener.addTween( puzzlePiece, Tweener.Hash("y",Screen.height + 200, "time",0.5f, "rotation", 120, "transition",Tweener.TransitionType.easeInCubic ) ).OnComplete(removePuzzlePop, puzzlePiece);
							}
							
							if(i < 4)
							{
								_photoClip.getChildByName<MovieClip>("puzzle" + i).visible = true;
								
							}
							else
							{
								DebugConsole.Log("_photoClip.getChildByName<MovieClip>(puzzlei) null: {0}", (_photoClip.getChildByName<MovieClip>("puzzle" + i) == null));
								_photoClip.getChildByName<MovieClip>("puzzle" + i).visible = false;
							}
							
							DebugConsole.LogWarning("END FOR LOOP");
							
						}
						
					} 
					else {
						_photoClip.gotoAndStop("showHalfHorizontal");
						_photoClip.getChildByName<MovieClip>("photos").getChildByName<MovieClip>("frame").visible = false;
						DebugConsole.LogWarning ("CorrectOpportunity || _photoClip horizontal");
						hardWidth = _photoClip.width + (_photoClip.width * 0.2f);
						
						for(i = 1; i <= 6; i++)
						{
							
							if(i == 2 || i == 3 || i == 6)
							{
																
								switch(i)
								{
									case 2:
									xPos = _photoClip.getChildByName<MovieClip>("puzzle6").x;
									yPos = _photoClip.getChildByName<MovieClip>("puzzle5").y;
									break;
									case 3:
									xPos = _photoClip.getChildByName<MovieClip>("puzzle5").x;
									yPos = _photoClip.getChildByName<MovieClip>("puzzle4").y;
									break;
									case 6:
									xPos = _photoClip.getChildByName<MovieClip>("puzzle4").x;
									yPos = _photoClip.getChildByName<MovieClip>("puzzle5").y;
									break;
								}
								
								MovieClip puzzlePiece = MovieClipFactory.CreateTravelClip( "Puzzle_" + i );
								_photoClip.addChild( puzzlePiece );
								puzzlePiece.x = xPos;
								puzzlePiece.y = yPos;
								puzzlePiece.rotation = 90;
								Tweener.addTween( puzzlePiece, Tweener.Hash("y",Screen.height + 200, "time",0.5f, "rotation", 120, "transition",Tweener.TransitionType.easeInCubic ) ).OnComplete(removePuzzlePop, puzzlePiece);
							}
							
							if(i >= 4)
							{
								_photoClip.getChildByName<MovieClip>("puzzle" + i).visible = true;
							}
							else
							{
								_photoClip.getChildByName<MovieClip>("puzzle" + i).visible = false;
							}
							
						}
					}
					
					//589.95f beging the width of the image itself, could change
					float photoUpScale = Mathf.Abs( (photoUp.width + (photoUp.width * 0.3f)) / hardWidth);
					DebugConsole.LogWarning ("photoUpScale: {0}", photoUpScale);
					DebugConsole.LogWarning ("photoUp.width: {0}, hardWidth: {1}", photoUp.width, hardWidth);
					_photoClip.scaleX = _photoClip.scaleY = photoUpScale;

					DebugConsole.LogWarning ("CorrectOpportunity || _photoup scale: {0}", photoUpScale);
					DebugConsole.LogWarning ("CorrectOpportunity || _photoClip x: {0}, y: {1}, width: {2}, height: {3}", _photoClip.x, _photoClip.y, _photoClip.width, _photoClip.height);

					_photoClip.getChildByName<MovieClip>("photos").gotoAndStop(_photoNumber + 1);
					_photoClip.getChildByName<MovieClip>("photos").getChildByName<MovieClip>("frame").visible = false;
					_photoClip.getChildByName<MovieClip>("textfield_mc").visible = false;
			
					SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
					//peek noise
					
					AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.TRAVEL_THEME, TravelThemeController.POP);
					bundle.AddClip( clip, "POP", clip.length );
					//payoff speach 1
					int payoffAudio = UnityEngine.Random.Range(1,4);

					string audioPath = TravelThemeController.BASE_PATH + hostName + "/" + TravelThemeController.PAYOFF50_PATH + "/" + TravelThemeController.PAYOFF50 + payoffAudio.ToString();
					Debug.Log("audio path: " + audioPath);
					
					clip = SoundUtils.LoadGlobalSound(MovieClipFactory.TRAVEL_THEME, audioPath);
					if(clip != null){
						bundle.AddClip( clip, "PAYOFF_50", clip.length );
						SoundEngine.Instance.PlayBundle( bundle );
					}else{
					
						OnAudioWordComplete("PAYOFF_50");
					}
					
					
				}
				else
				{
					//if Help Mode
					if(currentSession.helpMode)
					{
						OnAudioWordComplete("PAYOFF_100");
						return;
					}
					DebugConsole.LogWarning ("CorrectOpportunity || payoff100");
					SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
					//scenery thump
					AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.TRAVEL_THEME, TravelThemeController.CAMERA);
					bundle.AddClip( clip, "CAMERA", clip.length );
					//payoff speach 1
					int payoffAudio = UnityEngine.Random.Range(1,4);
					string audioPath = TravelThemeController.BASE_PATH + hostName + "/" + TravelThemeController.PAYOFF100_PATH + "/" + TravelThemeController.PAYOFF100 + _photoNames[_photoNumber].ToLower().Replace(" ", "-")+ payoffAudio.ToString();
					Debug.Log("audio path: " + audioPath);
					clip = SoundUtils.LoadGlobalSound(MovieClipFactory.TRAVEL_THEME, audioPath);
					
					if(clip != null){
						bundle.AddClip( clip, "PAYOFF_100", clip.length );
						SoundEngine.Instance.PlayBundle( bundle );
					}else{
						OnAudioWordComplete("PAYOFF_100");
					}

					PhotoZoom();
				}
			}
			else
			{
				//PhotoCircle();
				// if this method is called more than 10 times skip the payoff and dispatch ThemeEvent.CORRECT_OPPORTUNITY_COMPLETE
				dispatchEvent(new ThemeEvent(ThemeEvent.COMPLETE, true, false));
				//CompleteSession ();
			}
						
		}
		
		private void removePuzzlePop( object pieceObj )
		{
			MovieClip piece = pieceObj as MovieClip;
			_photoClip.removeChild( piece );
		}

		private int originalIndex;
		private void PhotoZoom()//(event:BadgeEvent):void
		{
			// move badge to center of stage and scale to 100% 
			
			if (_photoNumber == 0 || _photoNumber == 2 || _photoNumber == 4){
				_photoClip.gotoAndStop("showFullVertical");
				_photoClip.getChildByName<MovieClip>("photos").getChildByName<MovieClip>("frame").visible = false;
				
			} else {
				_photoClip.gotoAndStop("showFullHorizontal");
				_photoClip.getChildByName<MovieClip>("photos").getChildByName<MovieClip>("frame").visible = false;
			}
			_photoClip.stopAll();
			
			_photoClip.getChildByName<MovieClip>("textfield_mc").visible = true;
			_photoClip.getChildByName<MovieClip>("textfield_mc").getChildByName<TextField>("tf").text = _photoNames[_photoNumber];
			_photoClip.getChildByName<MovieClip>("photos").gotoAndStop(_photoNumber + 1);
			_photoClip.getChildByName<MovieClip>("photos").getChildByName<MovieClip>("frame").visible = false;
			
			for(int i = 1; i <= 6; i++)
			{
				_photoClip.getChildByName<MovieClip>("puzzle" + i).visible = false;
			}
			
			DebugConsole.Log("Text Field || width: {0}, hieght: {1}", _photoClip.getChildByName<MovieClip>("textfield_mc").width, _photoClip.getChildByName<MovieClip>("textfield_mc").height);
			
			int textLength = _photoClip.getChildByName<MovieClip>("textfield_mc").getChildByName<TextField>("tf").text.Length;
			
			int tempBGWidth = textLength * 85;
			int tempBGHeight = 165;
			
			MovieClip tempbackground = new MovieClip();
			tempbackground.graphics.drawSolidRectangle(Color.white, -(tempBGWidth * 0.5f),
																	-(tempBGHeight * 0.5f),
																	tempBGWidth,
																	tempBGHeight);
			_photoClip.getChildByName<MovieClip>("text_bg").addChild(tempbackground);
			
			_photoClip.getChildByName<MovieClip>("textfield_mc").scaleX = 0.75f;
			_photoClip.getChildByName<MovieClip>("textfield_mc").scaleY = 0.75f;
			_photoClip.getChildByName<MovieClip>("text_bg").scaleX = 0.75f;
			_photoClip.getChildByName<MovieClip>("text_bg").scaleY = 0.75f;

			originalIndex = this.getChildIndex(_photoClip);

			removeChild( _photoClip );
			addChild( _photoClip );

			Tweener.addTween(_photoClip, Tweener.Hash("x", (mc_overlay.width * 0.5f), "y", (mc_overlay.height * 0.5f) - (mc_overlay.height * 0.1f), "scaleX", 1, "scaleY", 1, "time", 1f, "transition", Tweener.TransitionType.easeOutQuad));//.OnComplete(badgeCollect);
			
		}
		
		// move badge to bottom of screen
		private void PhotoCollect()
		{
			
			//float normalizedPlaceHolderParentScale = (1 - _placeholderClip.parent.scaleX) + 1;
			
		
			_photoClip.getChildByName<MovieClip>("textfield_mc").visible = false;
			_photoClip.getChildByName<MovieClip>("text_bg").visible = false;
			
			float photoRotation;
			
			DebugConsole.Log("(payoffCount/2): " + (payoffCount/2));

			float hardWidth = 168.75f;
			switch( (payoffCount/2) )
			{
				case 1:
					photoRotation = -3;
				break;
				
				case 2:
					photoRotation = 0;
				break;
				
				case 3:
					photoRotation = 3;
					hardWidth = 226.25f;
				break;
				
				case 4:
					photoRotation = -5;
					hardWidth = 226.25f;
				break;
				
				case 5:
					photoRotation = 3;
					hardWidth = 226.25f;
				break;
				
			default:
				photoRotation = 0.0f;
				break;
			}

			float photoUpScale = Mathf.Abs( hardWidth / (_photoClip.width + (_photoClip.width * 0.3f))	);
			if(PlatformUtils.GetPlatform() == Platforms.WIN) photoUpScale *= 1.2f;

			DebugConsole.LogWarning ("photoUpScale photo collected: {0}", photoUpScale);
			_photoClips.Add( _photoClip );

			removeChild( _photoClip );
			addChildAt( _photoClip, originalIndex );

			Tweener.removeTweens (_photoClip);
			TweenerObj obj = Tweener.addTween(_photoClip, Tweener.Hash("x", _placeholderClip.x, "y", _placeholderClip.y, "scaleX", photoUpScale, "scaleY", photoUpScale, "rotation", photoRotation, "time", 0.5, "transition", Tweener.TransitionType.easeOutQuad)).OnComplete(PhotoCollected);			
			Debug.Log("$$$$$ DID TWEENER OBJ GET CREATED: " + (obj == null)); 
		}
		
		// continue once badge is collected
		private void PhotoCollected()
		{
			_photosCollected++;
			DebugConsole.LogError("PHOTO COLLECTED: " + _photosCollected);
			if (_photosCollected == 2){
				photoUp.gotoAndStop("horizontal");
			}
			
			dispatchEvent(new ThemeEvent(ThemeEvent.COMPLETE, true, false));
		}
		
		// tween badges above character at end
		private void PhotoCircle()
		{

			HideOverlay ();

			oscillate = true;
			Debug.Log("PHOTOS CIRCLING");
			//sparklesChannel = sparklesSound.play();
			//overlay2_mc.alpha = 0.0;
			//Tweener.addTween(overlay2_mc, Tweener.Hash("time", 0.35, "alpha", 1.0, "visible", true, "transition", Tweener.TransitionType.easeOutQuad));
			//showOverlay();
			
			float scale = 0.3f;
			
			for(int i = 0; i < photoList.Count; i++)
			{
				photoList[i].getChildByName<MovieClip>("photos").getChildByName<MovieClip>("frame").visible = true;
				photoList[i].getChildByName<MovieClip>("textfield_mc").visible = true;
				photoList[i].getChildByName<MovieClip>("text_bg").visible = true;
				photoList[i].rotation = 0;
				if (_photosCollected > i)
					Tweener.addTween(photoList[i], Tweener.Hash("x", circlingPhotos[i].x, "y", circlingPhotos[i].y, "scaleX",scale, "scaleY", scale, "time", 1, "transition", Tweener.TransitionType.easeOutQuad));
				else
					photoList[i].visible = false;
			}

			if (mc_overlay2 != null) 
			{
				mc_overlay2.visible = true;
				mc_overlay2.alpha = 0;
				Tweener.addTween (mc_overlay2, Tweener.Hash ("time", 0.35, "alpha", 1.0, "transition", Tweener.TransitionType.easeOutQuad));
			}
			else
				DebugConsole.LogError ("mc_overlay2 is null");
					
			addEventListener(CEvent.ENTER_FRAME, waveRewards);
			
		}
		
		float rotationIncrement = 6;
		private float currentRotation = 0;
		private void waveRewards( CEvent e )
		{
			
			
			currentRotation += rotationIncrement;
			currentRotation %= 360;
			
			for(int i = 0; i < photoList.Count; i++)
			{
				if (_photosCollected > i)
				{
					photoList[i].y = circlingPhotos[i].y + (float)(Mathf.Sin(Mathf.PI / 180 * (currentRotation + (45 * i))) * 100);
				}
			}
		
		}
		
		public override void CompleteSession()
		{
			float percent = currentSession.Percent;
			base.CompleteSession();
			
			PhotoCircle();
			
			if(payoffCount > 2)
			{
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				//sparkle
				AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.TRAVEL_THEME, TravelThemeController.POP);
				bundle.AddClip( clip, "POP", clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
			}
			
			Tweener.addTween(shadow_mc, Tweener.Hash("time", 0.35, "alpha", 0.0, "transition", Tweener.TransitionType.easeOutQuad));
			
			int randDialogue = UnityEngine.Random.Range(1, 3);
			
			if(percent < 60f)
			{
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				
				string audioPath = TravelThemeController.BASE_PATH + hostName + "/" + TravelThemeController.END_PATH + "/" + TravelThemeController.END_BEGINNER + randDialogue.ToString();
				AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.TRAVEL_THEME, audioPath);
				bundle.AddClip( clip, "THEME_END", clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else if(percent >=60 && percent < 80)
			{
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				
				string audioPath = TravelThemeController.BASE_PATH + hostName + "/" + TravelThemeController.END_PATH + "/" + TravelThemeController.END_INTERMEDIATE + randDialogue.ToString();
				AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.TRAVEL_THEME, audioPath);
				bundle.AddClip( clip, "THEME_END", clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else
			{
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				
				string audioPath = TravelThemeController.BASE_PATH + hostName + "/" + TravelThemeController.END_PATH + "/" + TravelThemeController.END_MASTER + randDialogue.ToString();
				AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.TRAVEL_THEME, audioPath);
				bundle.AddClip( clip, "THEME_END", clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
			}

			payoffCount = 1;
		}
		
		private void onOpportunityComplete()
		{
			dispatchEvent ( new ThemeEvent(ThemeEvent.CONTINUE) );
		}
		
		public override void Unload()
		{
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
		}
		
		private void Update()
		{
			if(oscillate)
			{
				
				if (_photosCollected > 0)
				{
					photoList[0].y = circlingPhotos[2].y + Mathf.Sin(angle) * 50.0f;
				}
				if (_photosCollected > 1)
				{
					photoList[0].y = circlingPhotos[1].y + Mathf.Sin(angle) * 50.0f;
					photoList[1].y = circlingPhotos[3].y + Mathf.Sin(angle) * 50.0f;
				
					
				}
				if (_photosCollected > 2)
				{
					photoList[0].y = circlingPhotos[0].y + Mathf.Sin(angle) * 50.0f;
					photoList[1].y = circlingPhotos[2].y + Mathf.Sin(angle) * 50.0f;
					photoList[2].y = circlingPhotos[4].y + Mathf.Sin(angle) * 50.0f;
				}
				if (_photosCollected > 3)
				{
					photoList[0].y = circlingPhotos[0].y + Mathf.Sin(angle) * 50.0f;
					photoList[1].y = circlingPhotos[1].y + Mathf.Sin(angle) * 50.0f;
					photoList[2].y = circlingPhotos[3].y + Mathf.Sin(angle) * 50.0f;
					photoList[3].y = circlingPhotos[4].y + Mathf.Sin(angle) * 50.0f;
				}
				if (_photosCollected > 4)
				{
					photoList[0].y = circlingPhotos[0].y + Mathf.Sin(angle) * 50.0f;
					photoList[1].y = circlingPhotos[1].y + Mathf.Sin(angle) * 50.0f;
					photoList[2].y = circlingPhotos[2].y + Mathf.Sin(angle) * 50.0f;
					photoList[3].y = circlingPhotos[3].y + Mathf.Sin(angle) * 50.0f;
					photoList[4].y = circlingPhotos[4].y + Mathf.Sin(angle) * 50.0f;
				}
			
				
				/*
				angle += 0.05f;
				
				int tx;// = _baskets[item].posX + (((960.0 - totalWidth / 2) + totalWidth * (int(item) / _baskets.length)) - _baskets[item].posX) * 0.5;
				int ty;

			
				if (badgesCollected == 1){
					if(Tweener.(badge1) == false)
					{
						badge1.y = circling3.y + Math.sin(angle) * 50.0;
					}
					//Tweener.addTween(badge1, {x:circling3.x, y:circling3.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
				} 
				
				else if (_badgesCollected == 2){
					if(Tweener.isTweening(badge1) == false)
					{
						badge1.y = circling2.y + Math.sin(_angle) * 50.0;
					}
					if(Tweener.isTweening(badge2) == false)
					{
						badge2.y = circling4.y + Math.sin(_angle - Math.PI / 2) * 50.0;
					}
					//Tweener.addTween(badge1, {x:circling2.x, y:circling2.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge2, {x:circling4.x, y:circling4.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
				} else if (_badgesCollected == 3){
					if(Tweener.isTweening(badge1) == false)
					{
						badge1.y = circling1.y + Math.sin(_angle) * 50.0;
					}
					if(Tweener.isTweening(badge2) == false)
					{
						badge2.y = circling3.y + Math.sin(_angle - Math.PI / 3) * 50.0;
					}
					if(Tweener.isTweening(badge3) == false)
					{
						badge3.y = circling5.y + Math.sin(_angle - Math.PI / 1.5) * 50.0;
					}
					//Tweener.addTween(badge1, {x:circling1.x, y:circling1.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge2, {x:circling3.x, y:circling3.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge3, {x:circling5.x, y:circling5.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
				} else if (_badgesCollected == 4){
					if(Tweener.isTweening(badge1) == false)
					{
						badge1.y = circling1.y + Math.sin(_angle) * 50.0;
					}
					if(Tweener.isTweening(badge2) == false)
					{
						badge2.y = circling2.y + Math.sin(_angle - Math.PI / 4) * 50.0;
					}
					if(Tweener.isTweening(badge3) == false)
					{
						badge3.y = circling4.y + Math.sin(_angle - Math.PI / 2) * 50.0;
					}
					if(Tweener.isTweening(badge4) == false)
					{
						badge4.y = circling5.y + Math.sin(_angle - Math.PI) * 50.0;
					}
					//Tweener.addTween(badge1, {x:circling1.x, y:circling1.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge2, {x:circling2.x, y:circling2.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge3, {x:circling4.x, y:circling4.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge4, {x:circling5.x, y:circling5.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
				} else {
					if(Tweener.isTweening(badge1) == false)
					{
						badge1.y = circling1.y + Math.sin(_angle) * 50.0;
					}
					if(Tweener.isTweening(badge2) == false)
					{
						badge2.y = circling2.y + Math.sin(_angle - (Math.PI / 5)) * 50.0;
					}
					if(Tweener.isTweening(badge3) == false)
					{
						badge3.y = circling3.y + Math.sin(_angle - (Math.PI / 5) * 2) * 50.0;
					}
					if(Tweener.isTweening(badge4) == false)
					{
						badge4.y = circling4.y + Math.sin(_angle - (Math.PI / 5) * 3) * 50.0;
					}
					if(Tweener.isTweening(badge5) == false)
					{
						badge5.y = circling5.y + Math.sin(_angle - (Math.PI / 5) * 4) * 50.0;
					}
					//Tweener.addTween(badge1, {x:circling1.x, y:circling1.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge2, {x:circling2.x, y:circling2.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge3, {x:circling3.x, y:circling3.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge4, {x:circling4.x, y:circling4.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge5, {x:circling5.x, y:circling5.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
				}*/
				/*if(_baskets.length > 0)
				{
					
					var totalWidth:Number = _baskets[0].pwidth * _baskets.length + 20.0 * (_baskets.length - 1);
					for(var item in _baskets)
					{
						var tx:Number = _baskets[item].posX + (((960.0 - totalWidth / 2) + totalWidth * (int(item) / _baskets.length)) - _baskets[item].posX) * 0.5;
						var ty:Number = _baskets[item].posY + (300.0 - _baskets[item].posY) * 0.5 + Math.sin(_angle + 1.0 * int(item)) * 50.0;
						//_baskets[item].posX += (((960.0 - totalWidth / 2) + totalWidth * (int(item) / _baskets.length)) - _baskets[item].posX) * 0.5;
						//_baskets[item].posY += (300.0 - _baskets[item].posY) * 0.5;
						_baskets[item].posX = tx;
						_baskets[item].posY = ty;
					}
				}*/
			}
		}
		
		public override string Serialize ()
		{
			string data = "";
			
			for( int i = 0; i < _photoNumbers.Length; i++)
			{
				if( i > 0 ) data = data + "-";
				data = data + _photoNumbers[i].ToString();
			}
			data = data + ";" + payoffCount.ToString() + ";" + opportunitiesCompleted.ToString() + ";" + _photosCollected.ToString();
			
			return data;
		}
		
		public override string PullBadges ()
		{
			string badgeList = "[";
			
			for( int i = 0; i < _photosCollected; i++ )
			{
				if( i != 0 )
					badgeList = badgeList + ",";
				badgeList = badgeList + "\"" + ((_photoTypes) _photoNumbers[i]).ToString() + "\"";
			}
			badgeList = badgeList + "]";
			
			return badgeList;
		}
		
		public override void Deserialize( string data )
		{
			string[] sData = data.Split( ';' );
			
			payoffCount = int.Parse(sData[1]);
			opportunitiesCompleted = int.Parse(sData[2]);
			_photosCollected = int.Parse(sData[3]);
			
			sData = sData[0].Split('-');
			for( int k = 0; k < sData.Length; k++)
			{
				_photoNumbers[k] = int.Parse(sData[k]);
			}
			
			//show badges earned
			for( int j = 0; j < _photosCollected; j++)
			{
				_photoNumber = _photoNumbers[j];
				_photoClip = photoList[j];
				_photoClip.getChildByName<MovieClip>("photos").getChildByName<MovieClip>("frame").visible = false;
				_placeholderClip = getChildByName<MovieClip>("placeholder"+(j+1));
				
				if (_photoNumber == 0 || _photoNumber == 2 || _photoNumber == 4){
					_photoClip.gotoAndStop("showFullVertical");
					_photoClip.getChildByName<MovieClip>("photos").getChildByName<MovieClip>("frame").visible = false;
					
				} else {
					_photoClip.gotoAndStop("showFullHorizontal");
					_photoClip.getChildByName<MovieClip>("photos").getChildByName<MovieClip>("frame").visible = false;
				}
				_photoClip.stopAll();
				
				_photoClip.getChildByName<MovieClip>("photos").gotoAndStop(_photoNumber + 1);
				_photoClip.getChildByName<MovieClip>("photos").getChildByName<MovieClip>("frame").visible = false;
				
				_photoClip.getChildByName<MovieClip>("textfield_mc").getChildByName<TextField>("tf").text = _photoNames[_photoNumber];
				
				int textLength = _photoClip.getChildByName<MovieClip>("textfield_mc").getChildByName<TextField>("tf").text.Length;
				int tempBGWidth = textLength * 85;
				int tempBGHeight = 165;
				MovieClip tempbackground = new MovieClip();
				tempbackground.graphics.drawSolidRectangle(Color.white, -(tempBGWidth * 0.5f), -(tempBGHeight * 0.5f), tempBGWidth, tempBGHeight);
				_photoClip.getChildByName<MovieClip>("text_bg").addChild(tempbackground);
				_photoClip.getChildByName<MovieClip>("textfield_mc").scaleX = 0.75f;
				_photoClip.getChildByName<MovieClip>("textfield_mc").scaleY = 0.75f;
				_photoClip.getChildByName<MovieClip>("text_bg").scaleX = 0.75f;
				_photoClip.getChildByName<MovieClip>("text_bg").scaleY = 0.75f;
				
				_photoClip.getChildByName<MovieClip>("text_bg").visible = false;
				_photoClip.getChildByName<MovieClip>("textfield_mc").visible = false;
				
				for(int i = 1; i <= 6; i++)
				{
					_photoClip.getChildByName<MovieClip>("puzzle" + i).visible = false;
				}
				
				float photoRotation;
	
				float hardWidth = 168.75f;
				switch( (payoffCount/2) )
				{
					case 1:
						photoRotation = -3;
					break;
					
					case 2:
						photoRotation = 0;
					break;
					
					case 3:
						photoRotation = 3;
						hardWidth = 226.25f;
					break;
					
					case 4:
						photoRotation = -5;
						hardWidth = 226.25f;
					break;
					
					case 5:
						photoRotation = 3;
						hardWidth = 226.25f;
					break;
					
					default:
						photoRotation = 0.0f;
					break;
				}
	
				float photoUpScale = Mathf.Abs( hardWidth / (_photoClip.width + (_photoClip.width * 0.3f))	);
				if(PlatformUtils.GetPlatform() == Platforms.WIN) photoUpScale *= 1.2f;
				_photoClips.Add( _photoClip );
				
				_photoClip.x = _placeholderClip.x;
				_photoClip.y = _placeholderClip.y;
				_photoClip.scaleX = _photoClip.scaleY = photoUpScale;
				_photoClip.rotation = photoRotation;
			}
			
			if (_photosCollected >= 2){
				photoUp.gotoAndStop("horizontal");
			}
			
			//show partial reward
			if(payoffCount % 2 == 0)
			{
				_photoNumber = _photoNumbers[Mathf.FloorToInt((payoffCount-1)/2)];
				_photoClip = photoList[Mathf.FloorToInt((payoffCount-1)/2)];
				_photoClip.getChildByName<MovieClip>("photos").getChildByName<MovieClip>("frame").visible = false;
				_placeholderClip = getChildByName<MovieClip>("placeholder"+(payoffCount/2));
				
				_photoClip.x = photoUp.x;
				_photoClip.y = photoUp.y;
				
				_photoClip.rotation = photoUp.rotation;
				_photoClip.visible = true;
				_photoClip.alpha = 1;
				_photoClip.stopAll();

				int i = 0;
				
				float hardWidth = 550;
				
				if (_photoNumber == 0 || _photoNumber == 2 || _photoNumber == 4){
					_photoClip.gotoAndStop("showHalfVertical");
					DebugConsole.LogWarning ("CorrectOpportunity || _photoClip vertical");
					_photoClip.getChildByName<MovieClip>("photos").getChildByName<MovieClip>("frame").visible = false;
					hardWidth = _photoClip.width - (_photoClip.width * 0.12f);

					DebugConsole.Log ("photoUp.width: {0}, hardWidth: {1} ", _photoClip.width, hardWidth);

					for(i = 1; i <= 6; i++)
					{
						if(i < 4)
						{
							_photoClip.getChildByName<MovieClip>("puzzle" + i).visible = true;
							
						}
						else
						{
							_photoClip.getChildByName<MovieClip>("puzzle" + i).visible = false;
						}
					}
				} 
				else {
					_photoClip.gotoAndStop("showHalfHorizontal");
					_photoClip.getChildByName<MovieClip>("photos").getChildByName<MovieClip>("frame").visible = false;
					hardWidth = _photoClip.width + (_photoClip.width * 0.2f);
					
					for(i = 1; i <= 6; i++)
					{
						if(i >= 4)
						{
							_photoClip.getChildByName<MovieClip>("puzzle" + i).visible = true;
						}
						else
						{
							_photoClip.getChildByName<MovieClip>("puzzle" + i).visible = false;
						}
					}
				}
				
				//589.95f beging the width of the image itself, could change
				float photoUpScale = Mathf.Abs( (photoUp.width + (photoUp.width * 0.3f)) / hardWidth);
				DebugConsole.LogWarning ("photoUpScale: {0}", photoUpScale);
				DebugConsole.LogWarning ("photoUp.width: {0}, hardWidth: {1}", photoUp.width, hardWidth);
				_photoClip.scaleX = _photoClip.scaleY = photoUpScale;
				
				_photoClip.getChildByName<MovieClip>("photos").gotoAndStop(_photoNumber + 1);
				_photoClip.getChildByName<MovieClip>("photos").getChildByName<MovieClip>("frame").visible = false;
				_photoClip.getChildByName<MovieClip>("textfield_mc").getChildByName<TextField>("tf").text = _photoNames[_photoNumber];
				
				int textLength1 = _photoClip.getChildByName<MovieClip>("textfield_mc").getChildByName<TextField>("tf").text.Length;
				int tempBGWidth1 = textLength1 * 85;
				int tempBGHeight1 = 165;
				MovieClip tempbackground = new MovieClip();
				tempbackground.graphics.drawSolidRectangle(Color.white, -(tempBGWidth1 * 0.5f), -(tempBGHeight1 * 0.5f), tempBGWidth1, tempBGHeight1);
				_photoClip.getChildByName<MovieClip>("text_bg").addChild(tempbackground);
				_photoClip.getChildByName<MovieClip>("textfield_mc").scaleX = 0.75f;
				_photoClip.getChildByName<MovieClip>("textfield_mc").scaleY = 0.75f;
				_photoClip.getChildByName<MovieClip>("text_bg").scaleX = 0.75f;
				_photoClip.getChildByName<MovieClip>("text_bg").scaleY = 0.75f;
				
				_photoClip.getChildByName<MovieClip>("text_bg").visible = false;
				_photoClip.getChildByName<MovieClip>("textfield_mc").visible = false;
			}
		}
	}
}
