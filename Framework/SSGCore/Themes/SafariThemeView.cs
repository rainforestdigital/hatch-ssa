using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using pumpkin.display;
using pumpkin.events;
using pumpkin.swf;
using System;
using System.Linq;
using pumpkin.tweener;
using pumpkin.text;
using HatchFramework;

namespace SSGCore
{
	public enum AnimalTypes {ELEPHANT, CHEETAH, GIRAFFE, LION, ZEBRA, HYENA, GAZELLE, MEERKAT, HIPPO, RHINO}
		
	public class SafariThemeView : ThemeView
	{
		
		static MovieClip mc_elephantTree;
		static MovieClip mc_cheetahTree;
		static MovieClip mc_giraffeTree;
		static MovieClip mc_lionRock;
		static MovieClip mc_zebraRock;
		static MovieClip mc_hyenaBush;
		static MovieClip mc_gazelleBush;
		static MovieClip mc_meerKatBush;
		static MovieClip mc_hippoTree;
		static MovieClip mc_rhinoTree;
		
		public int payoffCount;
		public int opportunitiesCompleted;
		
		public Dictionary<AnimalTypes, MovieClip> animalClips;
		
		private MovieClip animalClip;
		private MovieClip badgeClip;
		private int badgeNumber;
		private int badgesCollected = 0;
		private List<MovieClip> badges;
		private List<MovieClip> circlingBadges;
		private List<MovieClip> startClips;
		private List<Vector2> badgeStartPositions;
		
		private MovieClip treeLeft;
		private MovieClip treeRight;
		private MovieClip rock;
		private MovieClip bush;
		
		private MovieClip background;
		private MovieClip bgContainer;

		private bool oscillate = false;
		
		Vector2 badgePos;
		Vector2 badgeScale;
		
		int animalNumber;
		List<AnimalTypes> animals;
		List<int> animalNumbers;
		public MovieClip safariRoot;
		
		/*
		private var popSound:Sound = new Pop();
		private var popChannel:SoundChannel;
		private var childrenSound:Sound = new Children();
		private var childrenChannel:SoundChannel;
		private var sparklesSound:Sound = new Sparkles();
		private var sparklesChannel:SoundChannel;
		private var themeSong:Sound = new ThemeSong();
		private var themeChannel:SoundChannel;
		*/
		
		public SafariThemeView ()
		{
			Init ();
		}
		
		public SafariThemeView(string swf, string symbol):base(swf, symbol)
		{
			Init ();
		}
		
		public override void Init()
		{
			badgeScale = new Vector2(1, 1);
			
			root = this;
			
			safariRoot = root.getChildByName<MovieClip>("SafariTheme");
			
			bgContainer = safariRoot.getChildByName<MovieClip>("bgContainer");
			
			background = MovieClipFactory.GetSafariBackground();
			
			bgContainer.addChild(background);
			
			shadow_mc = safariRoot.getChildByName<MovieClip>( "tutorialBG_mc" );
			shadow_mc.visible = false;
			
			tutorialBG_mc = safariRoot.getChildByName<MovieClip>( "tutorialBG_mc" );
			tutorialBG_mc.visible = false; 
			
			mc_overlay = safariRoot.getChildByName<MovieClip>( "overlay_mc" );
			mc_overlay.visible = false;
			
			mc_overlay2 = safariRoot.getChildByName<MovieClip>("overlay2_mc");
			if( mc_overlay2 != null ) mc_overlay2.visible = false;

			Tweener.addTween(mc_overlay, Tweener.Hash("time", 0.35, "alpha", 0, "visible", false, "transition", Tweener.TransitionType.easeOutQuad));
			//scaleY = (Mathf.Round(Screen.height) / Mathf.Round(MainUI.STAGE_HEIGHT));
			//scaleX = (Mathf.Round(Screen.width) / Mathf.Round(MainUI.STAGE_WIDTH));
			
			mc_elephantTree =  safariRoot.getChildByName<MovieClip>( "mc_elephantTree" );
			mc_cheetahTree =  safariRoot.getChildByName<MovieClip>( "mc_cheetahTree" );
			mc_giraffeTree =  safariRoot.getChildByName<MovieClip>( "mc_giraffeTree" );	
			mc_lionRock =  safariRoot.getChildByName<MovieClip>( "mc_lionRock" );		
			mc_zebraRock =  safariRoot.getChildByName<MovieClip>( "mc_zebraRock" );
			mc_hyenaBush =  safariRoot.getChildByName<MovieClip>( "mc_hyenaBush" );
			mc_gazelleBush =  safariRoot.getChildByName<MovieClip>( "mc_gazelleBush" );
			mc_meerKatBush =  safariRoot.getChildByName<MovieClip>( "mc_meerKatBush" );			
			mc_hippoTree =  safariRoot.getChildByName<MovieClip>( "mc_hippoTree" );
			mc_rhinoTree =  safariRoot.getChildByName<MovieClip>( "mc_rhinoTree" );
			
			animalClips = new Dictionary<AnimalTypes, MovieClip>();
		    animalClips[AnimalTypes.ELEPHANT] = mc_elephantTree;
		    animalClips[AnimalTypes.CHEETAH] = mc_cheetahTree;
			animalClips[AnimalTypes.GIRAFFE] = mc_giraffeTree;
			animalClips[AnimalTypes.LION] = mc_lionRock;
			animalClips[AnimalTypes.ZEBRA] = mc_zebraRock;
			animalClips[AnimalTypes.HYENA] = mc_hyenaBush;
			animalClips[AnimalTypes.GAZELLE] = mc_gazelleBush;
			animalClips[AnimalTypes.MEERKAT] = mc_meerKatBush;
			animalClips[AnimalTypes.HIPPO] = mc_hippoTree;
			animalClips[AnimalTypes.RHINO] = mc_rhinoTree;
			
			animals = Enum.GetValues(typeof(AnimalTypes)).Cast<AnimalTypes>().ToList();
			animalNumbers = new List<int>(){0,1,2,3,4,5,6,7,8,9};
			
			//DESERIALIZATION POINT
			//Badges order
			System.Random rnd = new System.Random();
			for (int i = animalNumbers.Count; i > 1; i--) {
			  int pos = rnd.Next(i);
			  var x = animalNumbers[i - 1];
			  animalNumbers[i - 1] = animalNumbers[pos];
			  animalNumbers[pos] = x;
			}
			
			//DESERIALIZATION POINT
			//Counts
			payoffCount = 1;
			opportunitiesCompleted = 1;

			foreach(AnimalTypes a in animals)
			{			
				animalClips[a].gotoAndStop(0);
				if(a.ToString().ToLower() == "meerkat")
					animalClips[a].getChildByName("mc_meerKat").visible = false;
				else
					animalClips[a].getChildByName("mc_"+a.ToString().ToLower()).visible = false;			
			}

			animalClips[AnimalTypes.CHEETAH].clipRect = new Rect(420.25f, 294.50f, 312.45f, 297.45f);
			animalClips[AnimalTypes.ELEPHANT].clipRect = new Rect(420.25f, 214.50f, 312.45f, 297.45f);
			animalClips[AnimalTypes.GAZELLE].clipRect = new Rect(93.10f, -8.45f, 206.95f, 238.10f);
			if( PlatformUtils.GetClosestRatio() == ScreenRatio.FOURxTHREE )
			{
				animalClips[AnimalTypes.GAZELLE].x -= 45;
			}
			animalClips[AnimalTypes.GIRAFFE].clipRect = new Rect(420.25f, 294.50f, 214.35f, 297.45f);
			animalClips[AnimalTypes.HIPPO].clipRect = new Rect(214.25f, 220.30f, 122.20f, 157.25f);
			//animalClips[AnimalTypes.HYENA].clipRect = new Rect(36.3f, 5.60f, 139.95f, 133.0f);
			//animalClips[AnimalTypes.LION].clipRect = new Rect(25.10f, 22.85f, 197.30f, 156.20f);
			animalClips[AnimalTypes.MEERKAT].clipRect = new Rect(93.10f, 54.60f, 206.95f, 175.05f);
			animalClips[AnimalTypes.RHINO].clipRect = new Rect(214.25f, 190.30f, 122.20f, 157.25f);
			animalClips[AnimalTypes.ZEBRA].clipRect = new Rect(25.10f, 22.85f, 197.30f, 156.20f);
			
			badgeStartPositions = new List<Vector2>();
			badges = new List<MovieClip>();
			for(int i=1; i<=5; i++)
			{
				MovieClip badge = safariRoot.getChildByName<MovieClip>("badge"+i);
				badges.Add(badge);
				badgeStartPositions.Add( new Vector2(badge.x, badge.y) );
			}
			
			circlingBadges = new List<MovieClip>();
			for(int i=1; i<=5; i++)
			{
				circlingBadges.Add(safariRoot.getChildByName<MovieClip>("circling"+i));
			}
			
			startClips = new List<MovieClip>();
			for(int i=1;i<=4;i++)
			{
				startClips.Add(safariRoot.getChildByName<MovieClip>("start"+i));
			}

			treeLeft =  safariRoot.getChildByName<MovieClip>( "mc_TreeLeft" );
			treeRight =  safariRoot.getChildByName<MovieClip>( "mc_TreeRight" );
			bush =  safariRoot.getChildByName<MovieClip>( "mc_Bush" );
			rock =  safariRoot.getChildByName<MovieClip>( "mc_Rock" );
			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			/*
			addEventListener(AnimalEvent.PEEK_COMPLETE, onPeekComplete);
			addEventListener(BadgeEvent.ZOOM, badgeZoom);
			addEventListener(BadgeEvent.COLLECT, badgeCollect);
			this.addEventListener(Event.ENTER_FRAME, Update);
			*/	
			if(!GlobalConfig.GetProperty<bool>("MuteThemeSong")){
				AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SAFARI_THEME, SafariThemeController.THEME_SONG);
				SoundEngine.Instance.PlayTheme(clip);
			}
			
			this.scaleY = this.scaleX = (float)Screen.height/mc_overlay.height; // maintain the ratio, the height is always smaller or the measurements for this app
		}
		
		public override MovieClip GetHostAnchor ()
		{
			return safariRoot.getChildByName<MovieClip>("mc_hostAnchor");
		}
		
		private void OnAudioWordComplete( string word )
		{
			//Debug.Log("audio word complete: "+word);
			if(word == "PAYOFF_100")
			{
				if(currentSession.helpMode)
				{
					dispatchEvent(new ThemeEvent(ThemeEvent.COMPLETE, true, false));
					return;
				}
				badgeCollect();
				ShowOverlay();
			}
			
			if(word == "PAYOFF_50")
			{
				dispatchEvent(new ThemeEvent(ThemeEvent.COMPLETE, true, false));
				if(opportunitiesCompleted <= 10) ShowOverlay();
			}
			if(word == "THEME_END")
			{
				dispatchEvent( new SkillEvent( SkillEvent.SKILL_COMPLETE, true, false ) );
			}
			
			//Game starting!
			if(word == "INTRO")
			{
				if( currentSession.skillPair.Level == SkillLevel.Tutorial ) ShowTutorialBG();
				else ShowOverlay();
			}
			
			if(word == "THEME_INTRO")
			{
				// these games do not have an intro audio, so their showing of the tutorial/overlay is handled after THEME_INTRO
				if( (currentSession.skillPair.Skill.linkage == "Sorting" || 
					 currentSession.skillPair.Skill.linkage == "SegmentingCompoundWords" || 
					 currentSession.skillPair.Skill.linkage == "BlendingCompoundWords") && currentSession.skillPair.Level == SkillLevel.Tutorial ) ShowTutorialBG();
				else if( (currentSession.skillPair.Skill.linkage == "SegmentingCompoundWords" || 
						  currentSession.skillPair.Skill.linkage == "BlendingCompoundWords") && currentSession.skillPair.Level != SkillLevel.Tutorial ) ShowOverlay();
				else HideOverlay();
			}
		}
		
		public override void ResetTheme()
		{
			deserialized = false;
			
			payoffCount = 1;
			opportunitiesCompleted = 1;

			oscillate = false;
			badgesCollected = 0;
			
			int i;
			
			removeEventListener(CEvent.ENTER_FRAME, waveRewards);
			
			for(i = 0; i < badges.Count; i++)
			{
				badges[i].x = badgeStartPositions[i].x;
				badges[i].y = badgeStartPositions[i].y;
				
				DebugConsole.Log("ResetTheme || badge {0} current scale: {1}, badgeScaleX: {2}", i, badges[i].scaleX, badgeScale.x);
				
				badges[i].scaleX = badgeScale.x;
				badges[i].scaleY = badgeScale.y;
				badges[i].visible = false;
			}
			
			foreach(AnimalTypes a in animals)
			{			
				animalClips[a].gotoAndStop(0);
				if(a.ToString().ToLower() == "meerkat")
					animalClips[a].getChildByName("mc_meerKat").visible = false;
				else
					animalClips[a].getChildByName("mc_"+a.ToString().ToLower()).visible = false;		
			}
		}
		
		public override void CorrectOpportunity( bool correct )
		{
			base.CorrectOpportunity(correct);
			
			if(opportunitiesCompleted <= 10)
			{
				opportunitiesCompleted++;
				
				if(!correct)
				{
					dispatchEvent(new ThemeEvent(ThemeEvent.COMPLETE, true, false));
					return;
				}
				
				HideOverlay();
				
				payoffCount++;
								
				if(payoffCount%2 ==0)
				{
					//if Help Mode
					if(currentSession.helpMode)
					{
						OnAudioWordComplete("PAYOFF_50");
						return;
					}
					
					animalNumber = animalNumbers[Mathf.FloorToInt((payoffCount-1)/2)];
					animalClip = animalClips[animals[animalNumber]];
					animalClip.visible = true;
					animalClip.gotoAndPlay(2);

					SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
					//peek noise
					
					AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SAFARI_THEME, SafariThemeController.PEEK);
					bundle.AddClip( clip, "PEEK", clip.length );
					//payoff speach 1
					int payoffAudio = UnityEngine.Random.Range(1,4);
					
					string audioPath = SafariThemeController.BASE_PATH + hostName + "/" + SafariThemeController.PAYOFF50_PATH + "/" + SafariThemeController.PAYOFF50 + payoffAudio.ToString();
					Debug.Log("audio path: " + audioPath);
					
					clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SAFARI_THEME, audioPath);
					if(clip != null){
						bundle.AddClip( clip, "PAYOFF_50", clip.length );
						SoundEngine.Instance.PlayBundle( bundle );
					}else{
					
						OnAudioWordComplete("PAYOFF_50");
					}
					
					animalClip.addFrameScript(100, delegate(CEvent e) {
						animalClip.gotoAndStop(100);
					});
					
				}
				else
				{
					//if Help Mode
					if(currentSession.helpMode)
					{
						OnAudioWordComplete("PAYOFF_100");
						return;
					}
					
					animalNumber = animalNumbers[(payoffCount-1)/2 - 1];
					animalClip = animalClips[animals[animalNumber]];
					animalClip.visible = true;
					animalClip.play();		
					animalClips[animals[animalNumber]].visible = false;
					if(animalNumber < 3 )
						Tweener.addTween(treeLeft, Tweener.Hash("delay",0.5, "scaleY",-.3, "time",0.5, "transition",Tweener.TransitionType.easeOutQuad));
					else if(animalNumber < 5 )
						Tweener.addTween(rock, Tweener.Hash("delay",0.5, "scaleY",-.3, "time",0.5, "transition",Tweener.TransitionType.easeOutQuad));
					else if(animalNumber < 8 )
						Tweener.addTween(bush, Tweener.Hash("delay",0.5, "scaleY",-.3, "time",0.5, "transition",Tweener.TransitionType.easeOutQuad));
					else
						Tweener.addTween(treeRight, Tweener.Hash("delay",0.5, "scaleY",-.3, "time",0.5, "transition",Tweener.TransitionType.easeOutQuad));

					badgeNumber =  Mathf.Clamp((payoffCount-1)/2 -1, 0, badges.Count - 1);
					badgeClip = badges[badgeNumber];
					
					
					
					SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
					//scenery thump
					AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SAFARI_THEME, SafariThemeController.THUMP);
					bundle.AddClip( clip, "THUMP", clip.length );
					//payoff speach 1
					int payoffAudio = UnityEngine.Random.Range(1,4);
					
					string audioPath = SafariThemeController.BASE_PATH + hostName + "/" + SafariThemeController.PAYOFF100_PATH + "/" + SafariThemeController.PAYOFF100 + animals[animalNumber].ToString().ToLower() + payoffAudio.ToString();
					Debug.Log("audio path: " + audioPath);
					
					clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SAFARI_THEME, audioPath);
					if(clip != null){
						bundle.AddClip( clip, "PAYOFF_100", clip.length );
						SoundEngine.Instance.PlayBundle( bundle );
					}else{
						OnAudioWordComplete("PAYOFF_100");
					}
					ShowBadge();
				}
			}
			else
			{
				//badgeCircle();
				// if this method is called more than 10 times skip the payoff and dispatch ThemeEvent.CORRECT_OPPORTUNITY_COMPLETE
				dispatchEvent(new ThemeEvent(ThemeEvent.COMPLETE, true, false));
			}
						
		}
		
		public void ShowBadge()
		{
			MovieClip startClip;
			
			if (animalNumber < 3){
				startClip = startClips[0];
			} else if (animalNumber < 5){
				startClip = startClips[1];
			} else if (animalNumber < 8){
				startClip = startClips[2];
			} else {
				startClip = startClips[3];
			}
			
			// place and scale badge based on hidden badge start clips
			
			badgePos = new Vector2(badgeClip.x, badgeClip.y);
			badgeClip.x = startClip.x;
			badgeClip.y = startClip.y;
			badgeClip.width = startClip.width;
			badgeClip.height = startClip.height;
			
			badgeClip.visible = true;
			// show correct animal, color circle and text
			badgeClip.getChildByName<MovieClip>("animals").gotoAndStop(animals[animalNumber].ToString());
			badgeClip.getChildByName<MovieClip>("circles").gotoAndStop(badgeNumber);
			badgeClip.getChildByName<MovieClip>("textfield_mc").getChildByName<TextField>("tf").text = animals[animalNumber].ToString().ToLower();
			// fade in animal - adjust frames in badge clip to tweak delay
			badgeClip.gotoAndPlay("appear");	
			badgeClip.addFrameScript(60, delegate(CEvent e) {
				badgeClip.gotoAndStop(60);
				//badgeClip.getChildByName<MovieClip>("circles").gotoAndStop(60);
				badgeClip.getChildByName<MovieClip>("circles").gotoAndStop(badgeNumber);
				BadgeZoom();
			});
				
		}
		
		private void BadgeZoom()//(event:BadgeEvent):void
		{
			// move badge to center of stage and scale to 100% 
			badgeScale = new Vector2((float)badgeClip.scaleX, (float)badgeClip.scaleY);
			Debug.Log("*******SCALES: " + badgeScale.x + " " + badgeScale.y);
			Tweener.addTween(badgeClip, Tweener.Hash("x", 0, "y", -((badgeClip.height * 0.1f)+50), "scaleX", .8f, "scaleY", .8f, "time", 1f, "transition", Tweener.TransitionType.easeOutQuad));//.OnComplete(badgeCollect);
			//Tweener.addTween(badgeClip, Tweener.Hash("x", 0, "y", 0, "time", 1f, "transition", Tweener.TransitionType.easeOutQuad));
		}
		
		// move badge to bottom of screen
		private void badgeCollect()
		{
			
			//trace("badgeCollect");
			//badge swish
			/*
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound(SafariThemeController.SWISH);
			bundle.AddClip( clip, "SWISH", clip.length );
			SoundEngine.Instance.PlayBundle( bundle );
			*/
			Debug.Log("Collecting badge");
			
			if(badgeClip != null)
			{
			
				Tweener.addTween(badgeClip, Tweener.Hash("x", badgePos.x, "y", badgePos.y,  "scaleX", badgeScale.x, "scaleY", badgeScale.y, "time", 0.5f, "transition", Tweener.TransitionType.easeOutQuad)).OnComplete(badgeCollected);
				badgeClip.gotoAndPlay("fadeText");
				badgeClip.addFrameScript(85, delegate(CEvent e) {
					Debug.Log ("frame script triggered");
					badgeClip.gotoAndStop(85);
				});
			
			}
			Debug.Log ("After badge collect");
			
		}
		
		// continue once badge is collected
		private void badgeCollected()
		{
			Debug.Log ("BadgeCollected");
			// raise tree, rock or bush
			if(animalNumber < 3 )
				Tweener.addTween(treeLeft, Tweener.Hash("delay", 0.1, "scaleY", 1, "time", 0.5f, "transition", Tweener.TransitionType.easeOutQuad));
			else if(animalNumber < 5 )
				Tweener.addTween(rock, Tweener.Hash("delay", 0.1, "scaleY", 1, "time", 0.5f, "transition", Tweener.TransitionType.easeOutQuad));
			else if(animalNumber < 8 )
				Tweener.addTween(bush, Tweener.Hash("delay", 0.1, "scaleY", 1, "time", 0.5f, "transition", Tweener.TransitionType.easeOutQuad));
			else
				Tweener.addTween(treeRight, Tweener.Hash("delay", 0.1, "scaleY", 1, "time", 0.5f, "transition", Tweener.TransitionType.easeOutQuad));

			
			//dispatchEvent(new ThemeEvent(ThemeEvent.CORRECT_OPPORTUNITY_COMPLETE, true, false));
			badgesCollected++;
			dispatchEvent(new ThemeEvent(ThemeEvent.COMPLETE, true, false));
			
		
		}
		
		// tween badges above character at end
		private void badgeCircle()
		{
			oscillate = true;
			//sparklesChannel = sparklesSound.play();
			//overlay2_mc.alpha = 0.0;
			//Tweener.addTween(overlay2_mc, Tweener.Hash("time", 0.35, "alpha", 1.0, "visible", true, "transition", Tweener.TransitionType.easeOutQuad));
			//showOverlay();

			HideOverlay ();
			if (mc_overlay2 != null) 
			{
				mc_overlay2.visible = true;
				mc_overlay2.alpha = 0;
				Tweener.addTween (mc_overlay2, Tweener.Hash ("time", 0.35, "alpha", 1.0, "transition", Tweener.TransitionType.easeOutQuad));
			}
			else
				DebugConsole.LogError ("mc_overlay2 is null");

			if (badgesCollected > 0)
				Tweener.addTween(badges[0], Tweener.Hash("x", circlingBadges[0].x, "y", circlingBadges[0].y, "scaleX", badgeScale.x*1.5f, "scaleY", badgeScale.y*1.5f, "time", 1, "transition", Tweener.TransitionType.easeOutQuad));
			if (badgesCollected > 1)
				Tweener.addTween(badges[1], Tweener.Hash("x", circlingBadges[1].x, "y", circlingBadges[1].y, "scaleX", badgeScale.x*1.5f, "scaleY", badgeScale.y*1.5f, "time", 1, "transition", Tweener.TransitionType.easeOutQuad));
			if (badgesCollected > 2)
				Tweener.addTween(badges[2], Tweener.Hash("x", circlingBadges[2].x, "y", circlingBadges[2].y, "scaleX", badgeScale.x*1.5f, "scaleY", badgeScale.y*1.5f, "time", 1, "transition", Tweener.TransitionType.easeOutQuad));
			if (badgesCollected > 3)
				Tweener.addTween(badges[3], Tweener.Hash("x", circlingBadges[3].x, "y", circlingBadges[3].y, "scaleX", badgeScale.x*1.5f, "scaleY", badgeScale.y*1.5f, "time", 1, "transition", Tweener.TransitionType.easeOutQuad));
			if (badgesCollected > 4)
				Tweener.addTween(badges[4], Tweener.Hash("x", circlingBadges[4].x, "y", circlingBadges[4].y, "scaleX", badgeScale.x*1.5f, "scaleY", badgeScale.y*1.5f, "time", 1, "transition", Tweener.TransitionType.easeOutQuad));
			
			addEventListener(CEvent.ENTER_FRAME, waveRewards);
			
		}
		
		float rotationIncrement = 6;
		private float currentRotation = 0;
		private void waveRewards( CEvent e )
		{
			
			
			currentRotation += rotationIncrement;
			currentRotation %= 360;
			
			for(int i = 0; i < badges.Count; i++)
			{
				
				badges[i].y = circlingBadges[i].y + (float)(Mathf.Sin(Mathf.PI / 180 * (currentRotation + (45 * i))) * 100);
				
			}
					
		}
		
		public override void CompleteSession()
		{
			float percent = currentSession.Percent;
			DebugConsole.Log("Safari || CompleteSession percent: {0}", percent);
			base.CompleteSession();
			
			badgeCircle();
			
			if(payoffCount > 2)
			{
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				//sparkle
				AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SAFARI_THEME, SafariThemeController.SPARKLES);
				bundle.AddClip( clip, "SPARKLE", clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
			}
			
			Tweener.addTween(shadow_mc, Tweener.Hash("time", 0.35, "alpha", 0.0, "transition", Tweener.TransitionType.easeOutQuad));
			
			int randDialogue = UnityEngine.Random.Range(1, 3);
			
			if(percent < 60f)
			{
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				
				string audioPath = SafariThemeController.BASE_PATH + hostName + "/" + SafariThemeController.END_PATH + "/" + SafariThemeController.END_BEGINNER + randDialogue.ToString();
				AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SAFARI_THEME, audioPath);
				bundle.AddClip( clip, "THEME_END", clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else if(percent >=60 && percent < 80)
			{
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				
				string audioPath = SafariThemeController.BASE_PATH + hostName + "/" + SafariThemeController.END_PATH + "/" + SafariThemeController.END_INTERMEDIATE + randDialogue.ToString();
				AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SAFARI_THEME, audioPath);
				bundle.AddClip( clip, "THEME_END", clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else
			{
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				
				string audioPath = SafariThemeController.BASE_PATH + hostName + "/" + SafariThemeController.END_PATH + "/" + SafariThemeController.END_MASTER + randDialogue.ToString();
				AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SAFARI_THEME, audioPath);
				bundle.AddClip( clip, "THEME_END", clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
			}
			/*
			switch(_host)
			{
				case "CAMI":
					Tweener.addTween(cami_mc, {time:1.0, x:864.55, y:606.25, scaleX:3.14, scaleY:3.14, transition:"easeInQuad"});
					
					if(_payoffCount > 2)
					{
						cami_mc.payoffStart();
					}
					
					if($percent < 60.0)
					{
						camiLibrary_mc.gotoAndPlay("LESS60");
					}
					else if($percent >= 60.0 && $percent < 80.0)
					{
						camiLibrary_mc.gotoAndPlay("GREATER60");
					}
					else
					{
						camiLibrary_mc.gotoAndPlay("GREATER80");
					}
					
				break;
				case "HENRY":
					
					Tweener.addTween(henry_mc, {time:1.0, x:780.15, y:646.55, scaleX:3.14, scaleY:3.14, transition:"easeInQuad"});
					
					if(_payoffCount > 2)
					{
						henry_mc.payoffStart();
					}
					
					if($percent < 60.0)
					{
						henryLibrary_mc.gotoAndPlay("LESS60");
					}
					else if($percent >= 60.0 && $percent < 80.0)
					{
						henryLibrary_mc.gotoAndPlay("GREATER60");
					}
					else
					{
						henryLibrary_mc.gotoAndPlay("GREATER80");
					}
					
				break;
				case "PLATTIE":
					
					Tweener.addTween(plattie_mc, {time:1.0, x:672.4, y:522.45, scaleX:3.14, scaleY:3.14, transition:"easeInQuad"});
					
					if(_payoffCount > 2)
					{
						plattie_mc.payoffStart();
					}
					
					if($percent < 60.0)
					{
						plattieLibrary_mc.gotoAndPlay("LESS60");
					}
					else if($percent >= 60.0 && $percent < 80.0)
					{
						plattieLibrary_mc.gotoAndPlay("GREATER60");
					}
					else
					{
						plattieLibrary_mc.gotoAndPlay("GREATER80");
					}
					
				break;
				default:
				
				break;
			}
			*/
			payoffCount = 1;
		}
		
		private void onOpportunityComplete()
		{
			dispatchEvent ( new ThemeEvent(ThemeEvent.CONTINUE) );
		}
		
		public override void Unload()
		{
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
		}
		
		private void Update()
		{
			if(oscillate)
			{
				/*
				angle += 0.05f;
				
				int tx;// = _baskets[item].posX + (((960.0 - totalWidth / 2) + totalWidth * (int(item) / _baskets.length)) - _baskets[item].posX) * 0.5;
				int ty;
				
				if (badgesCollected == 1){
					if(Tweener.(badge1) == false)
					{
						badge1.y = circling3.y + Math.sin(angle) * 50.0;
					}
					//Tweener.addTween(badge1, {x:circling3.x, y:circling3.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
				} 
				
				else if (_badgesCollected == 2){
					if(Tweener.isTweening(badge1) == false)
					{
						badge1.y = circling2.y + Math.sin(_angle) * 50.0;
					}
					if(Tweener.isTweening(badge2) == false)
					{
						badge2.y = circling4.y + Math.sin(_angle - Math.PI / 2) * 50.0;
					}
					//Tweener.addTween(badge1, {x:circling2.x, y:circling2.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge2, {x:circling4.x, y:circling4.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
				} else if (_badgesCollected == 3){
					if(Tweener.isTweening(badge1) == false)
					{
						badge1.y = circling1.y + Math.sin(_angle) * 50.0;
					}
					if(Tweener.isTweening(badge2) == false)
					{
						badge2.y = circling3.y + Math.sin(_angle - Math.PI / 3) * 50.0;
					}
					if(Tweener.isTweening(badge3) == false)
					{
						badge3.y = circling5.y + Math.sin(_angle - Math.PI / 1.5) * 50.0;
					}
					//Tweener.addTween(badge1, {x:circling1.x, y:circling1.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge2, {x:circling3.x, y:circling3.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge3, {x:circling5.x, y:circling5.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
				} else if (_badgesCollected == 4){
					if(Tweener.isTweening(badge1) == false)
					{
						badge1.y = circling1.y + Math.sin(_angle) * 50.0;
					}
					if(Tweener.isTweening(badge2) == false)
					{
						badge2.y = circling2.y + Math.sin(_angle - Math.PI / 4) * 50.0;
					}
					if(Tweener.isTweening(badge3) == false)
					{
						badge3.y = circling4.y + Math.sin(_angle - Math.PI / 2) * 50.0;
					}
					if(Tweener.isTweening(badge4) == false)
					{
						badge4.y = circling5.y + Math.sin(_angle - Math.PI) * 50.0;
					}
					//Tweener.addTween(badge1, {x:circling1.x, y:circling1.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge2, {x:circling2.x, y:circling2.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge3, {x:circling4.x, y:circling4.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge4, {x:circling5.x, y:circling5.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
				} else {
					if(Tweener.isTweening(badge1) == false)
					{
						badge1.y = circling1.y + Math.sin(_angle) * 50.0;
					}
					if(Tweener.isTweening(badge2) == false)
					{
						badge2.y = circling2.y + Math.sin(_angle - (Math.PI / 5)) * 50.0;
					}
					if(Tweener.isTweening(badge3) == false)
					{
						badge3.y = circling3.y + Math.sin(_angle - (Math.PI / 5) * 2) * 50.0;
					}
					if(Tweener.isTweening(badge4) == false)
					{
						badge4.y = circling4.y + Math.sin(_angle - (Math.PI / 5) * 3) * 50.0;
					}
					if(Tweener.isTweening(badge5) == false)
					{
						badge5.y = circling5.y + Math.sin(_angle - (Math.PI / 5) * 4) * 50.0;
					}
					//Tweener.addTween(badge1, {x:circling1.x, y:circling1.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge2, {x:circling2.x, y:circling2.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge3, {x:circling3.x, y:circling3.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge4, {x:circling4.x, y:circling4.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
					//Tweener.addTween(badge5, {x:circling5.x, y:circling5.y, scaleX:circling1.scaleX, scaleY:circling1.scaleY, time:1, transition:"easeOutQuad"});
				}*/
				/*if(_baskets.length > 0)
				{
					
					var totalWidth:Number = _baskets[0].pwidth * _baskets.length + 20.0 * (_baskets.length - 1);
					for(var item in _baskets)
					{
						var tx:Number = _baskets[item].posX + (((960.0 - totalWidth / 2) + totalWidth * (int(item) / _baskets.length)) - _baskets[item].posX) * 0.5;
						var ty:Number = _baskets[item].posY + (300.0 - _baskets[item].posY) * 0.5 + Math.sin(_angle + 1.0 * int(item)) * 50.0;
						//_baskets[item].posX += (((960.0 - totalWidth / 2) + totalWidth * (int(item) / _baskets.length)) - _baskets[item].posX) * 0.5;
						//_baskets[item].posY += (300.0 - _baskets[item].posY) * 0.5;
						_baskets[item].posX = tx;
						_baskets[item].posY = ty;
					}
				}*/
			}
		}
		
		public override string Serialize ()
		{
			string data = "";
			
			for( int i = 0; i < animalNumbers.Count; i++)
			{
				if( i > 0 ) data = data + "-";
				data = data + animalNumbers[i].ToString();
			}
			data = data + ";" + payoffCount.ToString() + ";" + opportunitiesCompleted.ToString() + ";" + badgesCollected.ToString();
			
			return data;
		}
		
		public override string PullBadges ()
		{
			string badgeList = "[";
			
			for( int i = 0; i < badgesCollected; i++ )
			{
				if( i != 0 )
					badgeList = badgeList + ",";
				badgeList = badgeList + "\"" + ((AnimalTypes) animalNumbers[i]).ToString() + "\"";
			}
			badgeList = badgeList + "]";
			
			return badgeList;
		}
		
		public override void Deserialize( string data )
		{
			string[] sData = data.Split( ';' );
			
			payoffCount = int.Parse(sData[1]);
			opportunitiesCompleted = int.Parse(sData[2]);
			badgesCollected = int.Parse(sData[3]);
			
			sData = sData[0].Split('-');
			animalNumbers = new List<int>();
			for( int i = 0; i < sData.Length; i++)
			{
				animalNumbers.Add( int.Parse(sData[i]) );
			}
			
			//show badges earned
			for( int j = 0; j < badgesCollected; j++)
			{
				animalNumber = animalNumbers[j];
				
				MovieClip startClip;
				if (animalNumber < 3){
					startClip = startClips[0];
				} else if (animalNumber < 5){
					startClip = startClips[1];
				} else if (animalNumber < 8){
					startClip = startClips[2];
				} else {
					startClip = startClips[3];
				}
				
				badgeClip = badges[j];
				badgeClip.width = startClip.width;
				badgeClip.height = startClip.height;
				
				badgeScale = new Vector2(badgeClip.scaleX, badgeClip.scaleY);
				
				badgeClip.gotoAndStop(85);
				badgeClip.visible = true;
				badgeClip.getChildByName<MovieClip>("animals").gotoAndStop(animals[animalNumber].ToString());
				badgeClip.getChildByName<MovieClip>("circles").gotoAndStop(j);
				badgeClip.getChildByName<MovieClip>("textfield_mc").getChildByName<TextField>("tf").text = animals[animalNumber].ToString().ToLower();
			}
			
			//show partial reward
			if(payoffCount % 2 == 0)
			{
				animalNumber = animalNumbers[Mathf.FloorToInt((payoffCount-1)/2)];
				animalClip = animalClips[animals[animalNumber]];
				animalClip.visible = true;
				animalClip.gotoAndStop(100);
			}
		}
	}
}
