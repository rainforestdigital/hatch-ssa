using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using pumpkin.display;
using pumpkin.events;
using pumpkin.swf;
using System;
using System.Linq;
using pumpkin.tweener;
using pumpkin.text;
using HatchFramework;

namespace SSGCore
{
		
	public class GardenThemeView : ThemeView
	{

		public int payoffCount;
		public int opportunitiesCompleted;		
		
		private MovieClip vegetableContainer;
		
		private List<MovieClip> plantContainers;
		private List<MovieClip> currentPlants;
		private List<MovieClip> currentWholePlants;
		
		private MovieClip currentSign;
		private MovieClip background;
		private MovieClip bgContainer;
		
		private string currentPlantType;
		
		private List<Vector2> basketEndPositions;
		
		private int basketsCollected = 0;
		//private string[] _basketTypes = {"Radish", "Radish","Radish","Radish","Radish","Radish","Radish","Radish","Radish","Radish"};  		
		//private string[] _basketTypes = {"Broccoli", "Broccoli","Broccoli","Broccoli","Broccoli","Broccoli","Broccoli","Broccoli","Broccoli","Broccoli"};  		
		private string[] _basketTypes = {"Tomato", "Lettuce","Carrot","Pumpkin","Corn","Celery","Broccoli","Squash","Onion","Radish"};
		private MovieClip[] _basketPositions;
		
		//private string[] _availableTypes;
		private List<DisplayObjectContainer> _baskets;
		private DisplayObjectContainer currentBasket;
		private MovieClip currentBasketFront;
		private MovieClip currentBasketBack;

		
		private MovieClip displayWholePlant;
		private MovieClip displayWholePlantText;
		
		public GardenThemeView ()
		{
			Init ();
		}
		
		public GardenThemeView(string swf, string symbol):base(swf, symbol)
		{
			Init ();
		}
		
		public override void Init()
		{
			
			base.Init();
			
			bgContainer = getChildByName<MovieClip>("bgContainer");
			background = MovieClipFactory.GetGardenBackground();
			bgContainer.addChild(background);
	
			_baskets = new List<DisplayObjectContainer>();
			
			_basketPositions = new MovieClip[] {
				
				getChildByName<MovieClip>("basketContainer_1"),
				getChildByName<MovieClip>("basketContainer_2"),
				getChildByName<MovieClip>("basketContainer_3"),
				getChildByName<MovieClip>("basketContainer_4"),
				getChildByName<MovieClip>("basketContainer_5")
				
			};
			
			vegetableContainer = getChildByName<MovieClip>("veg_container");
			
			DebugConsole.Log("vegetableContainer is NULL: {0}", (vegetableContainer == null));
			
			currentWholePlants = new List<MovieClip>(); 
			
			basketEndPositions = new List<Vector2>();
			
			plantContainers = new List<MovieClip>();
			
			currentPlants = new List<MovieClip>();
			
			//DESERIALIZATION POINT
			//Badge list
			_basketTypes.Shuffle();
			//_availableTypes = new string[]{ _basketTypes[0], _basketTypes[1], _basketTypes[2], _basketTypes[3], _basketTypes[4]};
			
			//DESERIALIZATION POINT
			//counts
			payoffCount = 1;
			opportunitiesCompleted = 1;
			
			Tweener.addTween(mc_overlay, Tweener.Hash("time", 0.35, "alpha", 0, "visible", false, "transition", Tweener.TransitionType.easeOutQuad));
			//scaleY = (Mathf.Round(Screen.height) / Mathf.Round(MainUI.STAGE_HEIGHT));
			//scaleX = (Mathf.Round(Screen.width) / Mathf.Round(MainUI.STAGE_WIDTH));
			
			tutorialBG_mc.visible = false;
			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			if(!GlobalConfig.GetProperty<bool>("MuteThemeSong"))
			{
				AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.GARDEN_THEME, "Sound Effects/" + GardenThemeController.THEME_SONG);
				SoundEngine.Instance.PlayTheme(clip);
			}
			
			this.scaleX = (float)Screen.width/mc_overlay.width;
			this.scaleY = (float)Screen.height/mc_overlay.height;
		}
		
		private void OnAudioWordComplete( string word )
		{
			//Debug.Log("audio word complete: "+word);
			if(word == "PAYOFF_100")
			{
				
				DebugConsole.LogWarning("PAYOFF_100 Complete");
				
				if(currentSession.helpMode)
				{
					dispatchEvent(new ThemeEvent(ThemeEvent.COMPLETE, true, false));
					return;
				}
				//badgeCollect();
				//dispatchEvent(new ThemeEvent(ThemeEvent.COMPLETE, true, false)); //Temp

				showBasket();
				
				//ShowOverlay();
			}
			
			if(word == "PAYOFF_50")
			{
				dispatchEvent(new ThemeEvent(ThemeEvent.COMPLETE, true, false));
				if(opportunitiesCompleted <= 10) ShowOverlay();
			}
			if(word == "THEME_END")
			{
				dispatchEvent( new SkillEvent( SkillEvent.SKILL_COMPLETE, true, false ) );
			}
			
			//Game starting!
			if(word == "INTRO")
			{
				if( currentSession.skillPair.Level == SkillLevel.Tutorial ) ShowTutorialBG();
				else ShowOverlay();
			}
			
			if(word == "THEME_INTRO")
			{
				// these games do not have an intro audio, so their showing of the tutorial/overlay is handled after THEME_INTRO
				if( (currentSession.skillPair.Skill.linkage == "Sorting" || 
					 currentSession.skillPair.Skill.linkage == "SegmentingCompoundWords" || 
					 currentSession.skillPair.Skill.linkage == "BlendingCompoundWords") && currentSession.skillPair.Level == SkillLevel.Tutorial ) ShowTutorialBG();
				else if( (currentSession.skillPair.Skill.linkage == "SegmentingCompoundWords" || 
						  currentSession.skillPair.Skill.linkage == "BlendingCompoundWords") && currentSession.skillPair.Level != SkillLevel.Tutorial ) ShowOverlay();
				else HideOverlay();
			}
			
		}
		
		public override void ResetTheme()
		{
			deserialized = false;
			
			removeEventListener(CEvent.ENTER_FRAME, waveRewards);
			
			payoffCount = 1;
			opportunitiesCompleted = 1;
			clearPlantContainers();
			basketsCollected = 0;
			
			for(int i = 0; i < _baskets.Count; i++)
			{
				vegetableContainer.removeChild(_baskets[i]);
				
			}
			
			_baskets.Clear();
			
		}
		
		
		public override void CorrectOpportunity( bool correct )
		{
			base.CorrectOpportunity(correct);
			
			int i;
			
			if(opportunitiesCompleted <= 10)
			{
				
				opportunitiesCompleted++;
				
				if(!correct)
				{
					dispatchEvent(new ThemeEvent(ThemeEvent.COMPLETE, true, false));
					//ShowOverlay();
					return;
				}
				
				HideOverlay();
				
				payoffCount++;
				if(payoffCount%2 ==0)
				{
					
					//if Help Mode
					if(currentSession.helpMode)
					{
						OnAudioWordComplete("PAYOFF_50");
						return;
					}
					
					//string 
					//TODO 
					clearPlantContainers();
					currentPlants.Clear();
					currentPlantType = _basketTypes[Mathf.FloorToInt((payoffCount-1)/2)];
					
					plantContainers.Clear();
					for(i = 1; i <= 10; i++)
					{
						plantContainers.Add(getChildByName<MovieClip>(currentPlantType.ToLower() + "Plant" + i + "_mc"));
					}
										
					for(i = 0; i < plantContainers.Count; i++)
					{
						MovieClip newPlant = MovieClipFactory.CreateGardenClip(currentPlantType);
						newPlant.getChildByName<MovieClip>("content").addFrameScript("HALF", stopPlant);
						currentPlants.Add(newPlant);
						plantContainers[i].addChild(newPlant);
					}
					//reset basket and water bucket 
					
					SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
					AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.GARDEN_THEME, "Sound Effects/" + GardenThemeController.HALF_GROWN);
					bundle.AddClip( clip, "PEEK", clip.length );
					//payoff speach 1
					int payoffAudio = UnityEngine.Random.Range(1,4);
					
					string audioPath = GardenThemeController.BASE_PATH + hostName + "/" + GardenThemeController.PAYOFF50_PATH + "/" + GardenThemeController.PAYOFF50 + payoffAudio.ToString();
					Debug.Log("audio path: " + audioPath);
					
					clip = SoundUtils.LoadGlobalSound(MovieClipFactory.GARDEN_THEME, "Narration/" + audioPath);
					if(clip != null){
						bundle.AddClip( clip, "PAYOFF_50", clip.length ); 
						SoundEngine.Instance.PlayBundle( bundle );
					}else{
					
						OnAudioWordComplete("PAYOFF_50");
					}
					
				}
				else
				{
					
					//if Help Mode
					if(currentSession.helpMode)
					{
						OnAudioWordComplete("PAYOFF_100");
						return;
					}
					
					for(i = 0; i < currentPlants.Count; i++)
					{
						MovieClip plant = currentPlants[i].getChildByName<MovieClip>("content");
						DebugConsole.Log("plant is null: " + (plant == null));
						plant.addFrameScript(plant.totalFrames, stopPlant);	
						plant.play();
					}
						
					Tweener.addTween( this, Tweener.Hash("time", 0, "delay",2.5f)).OnComplete( displayPlant );
					
					SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
					//scenery thump
					AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.GARDEN_THEME, "Sound Effects/" + GardenThemeController.FULL_GROWN);
					bundle.AddClip( clip, "THUMP", clip.length );
					//payoff speach 1
					int payoffAudio = UnityEngine.Random.Range(1,4);
					string audioPath = GardenThemeController.BASE_PATH + hostName + "/" + GardenThemeController.PAYOFF100_PATH + "/" + GardenThemeController.PAYOFF100 + currentPlantType.ToLower() + payoffAudio.ToString();
					Debug.Log("audio path: " + audioPath);
					clip = SoundUtils.LoadGlobalSound(MovieClipFactory.GARDEN_THEME, "Narration/" + audioPath);
					if(clip != null){
						bundle.AddClip( clip, "PAYOFF_100", clip.length );
						SoundEngine.Instance.PlayBundle( bundle );
					}else{
						OnAudioWordComplete("PAYOFF_100");
					}
					
					basketsCollected++;
					
					showSign();
					
				}
			}
			else
			{
				//badgeCircle();
				// if this method is called more than 10 times skip the payoff and dispatch ThemeEvent.CORRECT_OPPORTUNITY_COMPLETE
				dispatchEvent(new ThemeEvent(ThemeEvent.COMPLETE, true, false));
			}
						
		}
		
		private void showSign()
		{
			
			currentSign = MovieClipFactory.CreateGardenClip("Sign");
			currentSign.gotoAndStop(currentPlantType.ToUpper());
			currentSign.x = getChildByName<MovieClip>("sign_container").x;
			currentSign.y = -(100 + currentSign.height);
			
			vegetableContainer.addChild(currentSign);
			
			Tweener.addTween( currentSign, Tweener.Hash("y", getChildByName<MovieClip>("sign_container").y, "time", 1) );
			
		}
		
		private void showBasket()
		{
			
			//TEMP
			/*
			sign.visible = false;
			dispatchEvent(new ThemeEvent(ThemeEvent.COMPLETE, true, false));
			return;
			*/
			
			int i = 0;
			
			DebugConsole.LogWarning("showBasket() || baskets Collected: {0}, _basketPos null: {1}", basketsCollected, (_basketPositions[0] == null));
			
			currentBasket = new DisplayObjectContainer();
			currentBasket.x = _basketPositions[basketsCollected - 1].x;
			currentBasket.y = _basketPositions[basketsCollected - 1].y;
			vegetableContainer.addChild( currentBasket );
			
			DebugConsole.LogWarning("showBasket || baskets Collected: {0}", basketsCollected);
			
			currentBasketBack = MovieClipFactory.CreateGardenClip("BasketBack");
			currentBasketBack.x = currentBasket.x;
			currentBasketBack.y = currentBasket.y;
			vegetableContainer.addChild(currentBasketBack);
												
			clearPlantContainers();
			currentWholePlants.Clear();
			basketEndPositions.Clear();
			
			//float scale = Mathf.Abs( (float)Screen.width / (float)MainUI.STAGE_WIDTH );
			
			float startRotation = 0;	
			float rotateBy = 0;
			float xOffset = 0;
			float yOffset = 0;
			
			switch(currentPlantType)
			{
				
				case "Carrot":
				startRotation = -50;
				rotateBy = 15;
				break;
				case "Corn":
				startRotation = -30;
				rotateBy = 15;
				break;
				case "Celery":
				startRotation = -160;
				rotateBy = 12;
				yOffset = 20;
				break;
				case "Broccoli":
				startRotation = -30;
				rotateBy = 15;
				break;
				case "Squash":
				startRotation = -135;
				rotateBy = 15;
				//xOffset = 40;
				break;
				case "Onion":
				startRotation = -30;
				rotateBy = 15;
				yOffset = 30;
				break;
				case "Radish":
				startRotation = -70;
				rotateBy = 10;
				//xOffset = ;
				yOffset = 20;
				break;
			}
			
			//{"Tomato", "Lettuce","Carrot","Pumpkin","Corn","Celery","Broccoli","Squash","Onion","Radish"};

			playVegetableCollectSound();

			for(i = 1; i <= 5; i++)
			{
				MovieClip endObject = getChildByName<MovieClip>(currentPlantType.ToLower() + "_end_" + i);
				Vector2 endPos = new Vector2((_basketPositions[ basketsCollected - 1 ].x + endObject.x) - xOffset, (_basketPositions[ basketsCollected - 1 ].y + endObject.y) - yOffset);
				MovieClip plant = MovieClipFactory.CreateGardenClip("Whole_" + currentPlantType);
				vegetableContainer.addChild(plant);
				plant.x =  displayWholePlant.x;//plantContainers[i].x;
				plant.y = displayWholePlant.y;//plantContainers[i].y;
				plant.scaleX = 4f;
				plant.scaleY = 4f;
				plant.rotation = getDisplayRotation();
				currentWholePlants.Add(plant);
				Tweener.addTween(plant, Tweener.Hash("x",currentBasket.x + ((currentBasketBack.width / 5) * i), "y",currentBasket.y - 300, "rotation",startRotation + (i * rotateBy), "delay",(i-1) * 0.2f, "scaleX",1, "scaleY",1, "time", 0.5f, "transition", Tweener.TransitionType.easeOutQuad ));
				basketEndPositions.Add(endPos);
			}

			vegetableContainer.removeChild( displayWholePlant );
			vegetableContainer.removeChild( displayWholePlantText );
			
			vegetableContainer.removeChild(currentSign);
			vegetableContainer.addChild(currentSign);
			
			currentBasketFront = MovieClipFactory.CreateGardenClip("BasketFront");
			currentBasketFront.x = currentBasket.x;
			currentBasketFront.y = currentBasket.y;
			vegetableContainer.addChild(currentBasketFront);
			
			Tweener.addTween( currentSign, Tweener.Hash("x", currentBasket.x + (currentSign.width * 0.25f) + (currentBasketBack.width * 0.5f) - 20, "y", (currentBasket.y) - 30, "rotation",(currentSign.rotation * 4) + UnityEngine.Random.Range(-20, 20) , "time",0.2f, "transition", Tweener.TransitionType.easeOutQuad) );
			Tweener.addTween(this, Tweener.Hash("delay", 0.1f * 5) ).OnComplete( finishBasket );
			
		}

		private void playVegetableCollectSound()
		{

			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.GARDEN_THEME, "Sound Effects/" + GardenThemeController.CLONES);

			bundle.AddClip( clip, "CLONES", clip.length );

			SoundEngine.Instance.PlayBundle( bundle );

		}

		private void finishBasket()
		{
						
			for(int i = 0; i < 5; i++)
			{
				MovieClip plant = currentWholePlants[i];
				Tweener.addTween(plant, Tweener.Hash("x",basketEndPositions[i].x, "y",basketEndPositions[i].y, "delay",(i) * 0.1f, "time", 0.1f, "transition", Tweener.TransitionType.easeOutQuad));
			}
			
			Tweener.addTween(this, Tweener.Hash("delay", 0.75f) ).OnComplete( completeTheme );
		}
		
		private void completeTheme()
		{
			//basket.visible = false;
			//sign.visible = false;
			
			vegetableContainer.removeChild(currentBasketBack);
			vegetableContainer.removeChild(currentSign);
			vegetableContainer.removeChild(currentBasketFront);
			
			currentBasket.addChild(currentBasketBack);
			currentBasketBack.x -= currentBasket.x;
			currentBasketBack.y -= currentBasket.y;
			
			for(int i = 0; i < 5; i++)
			{
				MovieClip plant = currentWholePlants[i];
				vegetableContainer.removeChild(plant);
				currentBasket.addChild(plant);
				plant.x -= currentBasket.x;
				plant.y -= currentBasket.y;
			}
			
			currentBasket.width = currentBasketFront.width;
			currentBasket.addChild(currentSign);
			currentSign.x -= currentBasket.x;
			currentSign.y -= currentBasket.y;
			currentBasket.addChild(currentBasketFront);
			currentBasketFront.x -= currentBasket.x;
			currentBasketFront.y -= currentBasket.y;
			
			_baskets.Add( currentBasket );
			vegetableContainer.addChild( _baskets[_baskets.Count - 1 ] );
			//currentBasket.visible = false;
						
			currentSign = null;
			currentWholePlants.Clear();
			currentBasketFront = null;
			currentBasketBack = null;
			currentBasket = null;
			
			DebugConsole.Log("COMPLETE THEME");
			
			ShowOverlay();
			dispatchEvent(new ThemeEvent(ThemeEvent.COMPLETE, true, false));
		}
		
		private void stopPlant(CEvent e)
		{
			MovieClip plant = e.currentTarget as MovieClip;
			plant.stop();
		}
		
		private void displayPlant()
		{
			
			DebugConsole.LogWarning("stopPlantWhole");
			
			float plantRotation = getDisplayRotation();
						
			displayWholePlant = MovieClipFactory.CreateGardenClip("Whole_" + currentPlantType);
			displayWholePlant.x = currentSign.x;
			displayWholePlant.y = currentSign.y;
			displayWholePlant.rotation = plantRotation;
			vegetableContainer.addChild( displayWholePlant );
			
			displayWholePlantText = MovieClipFactory.CreateGardenClip("Vegetable_Text");
			displayWholePlantText.x = (mc_overlay.width * 0.5f);
			displayWholePlantText.y = (mc_overlay.height * 0.5f);
			displayWholePlantText.getChildByName<TextField>("tf").text = currentPlantType.ToLower();
			vegetableContainer.addChild(displayWholePlantText);

			int rot = 0;
			if(currentPlantType == "Broccoli")
			{
				rot = 60;
			}

			Tweener.addTween( displayWholePlant, Tweener.Hash("x",(mc_overlay.width * 0.5f), "y",(mc_overlay.height * 0.25f), "time",1f, "rotation",rot, "scaleX",4f, "scaleY",4f, "transition",Tweener.TransitionType.easeOutQuad) );
			
		}
		
		private float getDisplayRotation()
		{
			switch(currentPlantType)
			{
				
				case "Carrot":
				return -90f;

				case "Corn":
				return -90f;

				case "Celery":
				return -180f;

				case "Onion":
				return -90f;

				default:	
				return 0f;

			}
			
		}
		
		private void clearPlantContainers()
		{
			for(int i = 0; i < plantContainers.Count; i++)
			{
				plantContainers[i].removeAllChildren();
			}
		}
		
		public override void CompleteSession()
		{
			float percent = currentSession.Percent;
			base.CompleteSession();
			
			basketCircle();
			
			if(payoffCount > 2)
			{
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				//sparkle
				AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.GARDEN_THEME, "Sound Effects/" + GardenThemeController.SPARKLES);
				bundle.AddClip( clip, "SPARKLE", clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
			}
			
			Tweener.addTween(shadow_mc, Tweener.Hash("time", 0.35, "alpha", 0.0, "transition", Tweener.TransitionType.easeOutQuad));
			
			int randDialogue = UnityEngine.Random.Range(1, 3);
			
			if(percent < 60f)
			{
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				
				string audioPath = GardenThemeController.BASE_PATH + hostName + "/" + GardenThemeController.END_PATH + "/" + GardenThemeController.END_BEGINNER + randDialogue.ToString();
				AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.GARDEN_THEME, "Narration/" + audioPath);
				bundle.AddClip( clip, "THEME_END", clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else if(percent >=60 && percent < 80)
			{
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				
				string audioPath = GardenThemeController.BASE_PATH + hostName + "/" + GardenThemeController.END_PATH + "/" + GardenThemeController.END_INTERMEDIATE + randDialogue.ToString();
				AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.GARDEN_THEME, "Narration/" + audioPath);
				bundle.AddClip( clip, "THEME_END", clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else
			{
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				
				string audioPath = GardenThemeController.BASE_PATH + hostName + "/" + GardenThemeController.END_PATH + "/" + GardenThemeController.END_MASTER + randDialogue.ToString();
				AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.GARDEN_THEME, "Narration/" + audioPath);
				bundle.AddClip( clip, "THEME_END", clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
			}
			/*
			switch(_host)
			{
				case "CAMI":
					Tweener.addTween(cami_mc, {time:1.0, x:864.55, y:606.25, scaleX:3.14, scaleY:3.14, transition:"easeInQuad"});
					
					if(_payoffCount > 2)
					{
						cami_mc.payoffStart();
					}
					
					if($percent < 60.0)
					{
						camiLibrary_mc.gotoAndPlay("LESS60");
					}
					else if($percent >= 60.0 && $percent < 80.0)
					{
						camiLibrary_mc.gotoAndPlay("GREATER60");
					}
					else
					{
						camiLibrary_mc.gotoAndPlay("GREATER80");
					}
					
				break;
				case "HENRY":
					
					Tweener.addTween(henry_mc, {time:1.0, x:780.15, y:646.55, scaleX:3.14, scaleY:3.14, transition:"easeInQuad"});
					
					if(_payoffCount > 2)
					{
						henry_mc.payoffStart();
					}
					
					if($percent < 60.0)
					{
						henryLibrary_mc.gotoAndPlay("LESS60");
					}
					else if($percent >= 60.0 && $percent < 80.0)
					{
						henryLibrary_mc.gotoAndPlay("GREATER60");
					}
					else
					{
						henryLibrary_mc.gotoAndPlay("GREATER80");
					}
					
				break;
				case "PLATTIE":
					
					Tweener.addTween(plattie_mc, {time:1.0, x:672.4, y:522.45, scaleX:3.14, scaleY:3.14, transition:"easeInQuad"});
					
					if(_payoffCount > 2)
					{
						plattie_mc.payoffStart();
					}
					
					if($percent < 60.0)
					{
						plattieLibrary_mc.gotoAndPlay("LESS60");
					}
					else if($percent >= 60.0 && $percent < 80.0)
					{
						plattieLibrary_mc.gotoAndPlay("GREATER60");
					}
					else
					{
						plattieLibrary_mc.gotoAndPlay("GREATER80");
					}
					
				break;
				default:
				
				break;
			}
			*/
			payoffCount = 1;
		}
		
		private void basketCircle()
		{
			
			DebugConsole.Log("Basket Circle || basketsCollected: {0}", basketsCollected);
			
			addChildAt( vegetableContainer, getChildIndex(mc_overlay2) + 1 );
			
			for(int i = 0; i < _baskets.Count; i++)
			{
				_baskets[i].visible = true;
								
				MovieClip circle = getChildByName<MovieClip>("circling" + (i + 1));
				
				float targX = circle.x - (_baskets[i].width * 0.5f);
				float targScale = 1.5f;
				if( PlatformUtils.GetPlatform() == Platforms.IOS)
				{
					targX *= 0.85f;
					targScale *= 0.85f;
				}
				
				Tweener.addTween( _baskets[i], Tweener.Hash("scaleX", targScale, "scaleY", targScale, "x", targX, "y",circle.y, "time", 1, "transition", Tweener.TransitionType.easeOutQuad));
				
			}

			HideOverlay ();
			if (mc_overlay2 != null) 
			{
				mc_overlay2.visible = true;
				mc_overlay2.alpha = 0;
				Tweener.addTween (mc_overlay2, Tweener.Hash ("time", 0.35, "alpha", 1.0, "transition", Tweener.TransitionType.easeOutQuad));
			}
			else
				DebugConsole.LogError ("mc_overlay2 is null");

			addEventListener(CEvent.ENTER_FRAME, waveRewards);
			
		}
		
		float rotationIncrement = 6;
		private float currentRotation = 0;
		private void waveRewards( CEvent e )
		{
			
			
			currentRotation += rotationIncrement;
			currentRotation %= 360;
			
			for(int i = 0; i < _baskets.Count; i++)
			{
				
				MovieClip circle = getChildByName<MovieClip>("circling" + (i + 1));
				
				_baskets[i].y = circle.y + (float)(Mathf.Sin(Mathf.PI / 180 * (currentRotation + (45 * i))) * 100);
				
			}
		
		}
		
		private void onOpportunityComplete()
		{
			dispatchEvent ( new ThemeEvent(ThemeEvent.CONTINUE) );
		}
		
		public override MovieClip GetHostAnchor ()
		{
			return getChildByName<MovieClip>("mc_hostAnchor");
		}
		
		public override void Unload()
		{
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
		}
		
		public override string Serialize ()
		{
			string data = "";
			
			for( int i = 0; i < _basketTypes.Length; i++)
			{
				if( i > 0 ) data = data + "-";
				data = data + _basketTypes[i];
			}
			data = data + ";" + payoffCount.ToString() + ";" + opportunitiesCompleted.ToString() + ";" + basketsCollected.ToString();
			
			return data;
		}
		
		public override string PullBadges ()
		{
			string badgeList = "[";
			
			for( int i = 0; i < basketsCollected; i++ )
			{
				if( i != 0 )
					badgeList = badgeList + ",";
				badgeList = badgeList + "\"" + _basketTypes[i] + "\"";
			}
			badgeList = badgeList + "]";
			
			return badgeList;
		}
		
		public override void Deserialize( string data )
		{
			int i = 0;
			
			string[] sData = data.Split( ';' );
			
			payoffCount = int.Parse(sData[1]);
			opportunitiesCompleted = int.Parse(sData[2]);
			basketsCollected = int.Parse(sData[3]);
			
			sData = sData[0].Split('-');
			for( i = 0; i < sData.Length; i++)
			{
				_basketTypes[i] = sData[i];
			}
			
			//show badges earned
			for( int j = 0; j < basketsCollected; j++)
			{
				clearPlantContainers();
				currentPlants.Clear();
				currentPlantType = _basketTypes[j];
				
				currentSign = MovieClipFactory.CreateGardenClip("Sign");
				currentSign.gotoAndStop(currentPlantType.ToUpper());
				
				currentBasket = new DisplayObjectContainer();
				currentBasket.x = _basketPositions[j].x;
				currentBasket.y = _basketPositions[j].y;
				
				currentBasketBack = MovieClipFactory.CreateGardenClip("BasketBack");
				currentBasketBack.x = currentBasketBack.y = 0;
				
				float startRotation = 0;	
				float rotateBy = 0;
				float xOffset = 0;
				float yOffset = 0;
				
				switch(currentPlantType)
				{
					
					case "Carrot":
					startRotation = -50;
					rotateBy = 15;
					break;
					case "Corn":
					startRotation = -30;
					rotateBy = 15;
					break;
					case "Celery":
					startRotation = -160;
					rotateBy = 12;
					yOffset = 20;
					break;
					case "Broccoli":
					startRotation = -30;
					rotateBy = 15;
					break;
					case "Squash":
					startRotation = -135;
					rotateBy = 15;
					break;
					case "Onion":
					startRotation = -30;
					rotateBy = 15;
					yOffset = 30;
					break;
					case "Radish":
					startRotation = -70;
					rotateBy = 10;
					yOffset = 20;
					break;
				}
				
				currentBasketFront = MovieClipFactory.CreateGardenClip("BasketFront");
				currentBasketFront.x = currentBasketFront.y = 0;
				
				currentSign.x = (currentSign.width * 0.25f) + (currentBasketBack.width * 0.5f) - 20;
				currentSign.y = -30;
				currentSign.rotation = (currentSign.rotation * 4) + UnityEngine.Random.Range(-20, 20);
				
				currentBasket.addChild(currentBasketBack);
				
				for(i = 0; i < 5; i++)
				{
					MovieClip endObject = getChildByName<MovieClip>(currentPlantType.ToLower() + "_end_" + (i+1));
					MovieClip plant = MovieClipFactory.CreateGardenClip("Whole_" + currentPlantType);
					plant.x = endObject.x - xOffset;
					plant.y = endObject.y - yOffset;
					plant.scaleX = plant.scaleY = 1f;
					plant.rotation = startRotation + (i * rotateBy);
					currentBasket.addChild(plant);
				}
				
				currentBasket.width = currentBasketFront.width;
				currentBasket.addChild(currentSign);
				currentBasket.addChild(currentBasketFront);
				
				_baskets.Add( currentBasket );
				vegetableContainer.addChild( _baskets[_baskets.Count - 1 ] );
							
				currentSign = null;
				currentWholePlants.Clear();
				currentBasketFront = null;
				currentBasketBack = null;
				currentBasket = null;
			}
			
			//show partial reward
			if(payoffCount % 2 == 0)
			{
				clearPlantContainers();
				currentPlants.Clear();
				currentPlantType = _basketTypes[Mathf.FloorToInt((payoffCount-1)/2)];
				
				plantContainers.Clear();
				for(i = 1; i <= 10; i++)
				{
					plantContainers.Add(getChildByName<MovieClip>(currentPlantType.ToLower() + "Plant" + i + "_mc"));
				}
									
				for(i = 0; i < plantContainers.Count; i++)
				{
					MovieClip newPlant = MovieClipFactory.CreateGardenClip(currentPlantType);
					newPlant.getChildByName<MovieClip>("content").gotoAndStop("HALF");
					currentPlants.Add(newPlant);
					plantContainers[i].addChild(newPlant);
				}
			}
		}
	}
}
