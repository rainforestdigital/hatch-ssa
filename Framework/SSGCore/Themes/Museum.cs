using System;
using UnityEngine;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using HatchFramework;
using System.Collections.Generic;
using pumpkin.geom;
using pumpkin.swf;

namespace SSGCore
{
	public class Museum : DisplayObjectContainer, GameScreen
	{
		public const string MUTTER = "Museum Muttering";
		public const string INTRO = "Museum Intro";
		public const string IDLE = "Museum Idle";
		public const string RETURN = "Museum Return";
		public const string REWARD = "Museum Reward";
		public const string PROMPT = "Museum Prompt";
		public const string INTERACT = "Museum Interacted";
		public const string PROGRESS = "Museum Automatic Progression";
		public const string ENDANIMATION = "Museum End Animation";
		
		private MovieClip background;
		private Sprite orwellLayer;
		private Sprite nicheLayer;
		private FilterMovieClip door;
		private List<GameObject> loadedAnimations;
		private Base3dCharacter orwellAnim;
		private TimerObject Timeout;
		private float totalTime = 15f;
		private float glowScale;
		private Rect glowRect;
		private int pulseCount;
		private int exploreCount;
		private MovieClip bigNiche;
		private Sprite greenFade;
		private FilterMovieClip clickArtifact;
		public DisplayObject View {  get{ return this; } }
		private Rect orwellRect = new Rect( 0, 1f/5f, 1f, 1f );
		private List<MovieClip> artifacts;
		private List<string> hasExplored;
		private string endAnimFlag;
		private MovieClip endAnimation;
		private MovieClip stopBtn;
		private string language;
		private string audioPrefix;

		public Museum ()
		{
			if( !string.IsNullOrEmpty( MuseumDataObject.nextSkill ) && MuseumDataObject.user != null )
				audioPrefix = SkillImplementationUtil.getLangTagforSkill( MuseumDataObject.nextSkill, LanguageProfileUtility.FromId( MuseumDataObject.user.Profile ) ).Replace('-', '/');
			
			this.language = audioPrefix == "" ? "en" : "sp";
			
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle( "Museum", OnAssetsLoaded ));
		}
		
		private void OnAssetsLoaded ( AssetBundle bundle )
		{
			MovieClipFactory.reloadTextureSpecificSkill( "Museum" );
			
			loadedAnimations = new List<GameObject>();
			loadedAnimations.Add( (GameObject)Resources.Load( "Characters/Orwell/Orwell01a" ) );
			loadedAnimations.Add( (GameObject)Resources.Load( "Characters/Orwell/Orwell01b" ) );
			loadedAnimations.Add( (GameObject)Resources.Load( "Characters/Orwell/Orwell01c" ) );
			loadedAnimations.Add( (GameObject)Resources.Load( "Characters/Orwell/Orwell01d" ) );
			loadedAnimations.Add( (GameObject)Resources.Load( "Characters/Orwell/Orwell02" ) );
			loadedAnimations.Add( (GameObject)Resources.Load( "Characters/Orwell/Orwell03" ) );
			
			dispatchEvent( new MuseumEvent( MuseumEvent.LOADED, true, false ) );
			
			background = MovieClipFactory.GetMuseum();
			addChild( background );
			
			for( int i = 0; i < background.numChildren; i++ )
			{
				background.getChildAt<MovieClip>( i ).stop();
			}
			
			door = new FilterMovieClip( background.getChildByName<MovieClip>( "Door" ) );
			
			door.x = door.Target.x;
			door.y = door.Target.y;
			door.Target.x = door.Target.y = 0;
			
			Rectangle doorTarg = door.Target.getBounds( door );
			door.Target.scaleX = door.Target.scaleY = 1.5f;
			door.Target.scaleX = door.Target.scaleY = 1f;
			door.scaleX = door.scaleY = doorTarg.width / door.width;
			
			door.Target.stop();
			background.addChild(door);
			
			orwellLayer = new Sprite();
			addChild(orwellLayer);
			nicheLayer = new Sprite();
			addChild(nicheLayer);
			
			artifacts = new List<MovieClip>();
			hasExplored = new List<string>();
			
			if( MuseumDataObject.awardingSkill != null )
			{
				SkillLevel currentLevel = MuseumDataObject.awardingLevel;
				if( currentLevel != SkillLevel.Completed )
				{
					currentLevel = SkillLevelUtil.FromId( SkillLevelUtil.FromId( currentLevel ) + 1 );
					if( currentLevel == SkillLevel.Emerging )
					{
						DebugConsole.LogError( "ERROR - How did you return to the musem from the Tutorial?" );
						
					}
					else
						MuseumDataObject.levelOf[MuseumDataObject.awardingSkill] = currentLevel;
					
					if( currentLevel == SkillLevel.Completed )
					{
						//check if all other skills are currently DD, set flag if so
						endAnimFlag = "DD";
						foreach( KeyValuePair<string, SkillLevel> pair in MuseumDataObject.levelOf )
						{
							if( pair.Value != SkillLevel.Completed )
							{
								endAnimFlag = "";
								break;
							}
						}
					}
				}
				else if( !MuseumDataObject.fullCompletes.Contains( MuseumDataObject.awardingSkill ) )
				{
					MuseumDataObject.fullCompletes.Add( MuseumDataObject.awardingSkill );
					
					//check if all skills are currently CD, set flag if so
					if( MuseumDataObject.fullCompletes.Count == MuseumDataObject.skillCount )
						endAnimFlag = "CD";
				}
				
				showFragment( MuseumDataObject.awardingSkill );
				
				rewardPath();
			}
			else
			{
				introPath();
				door.addEventListener( MouseEvent.CLICK, moveAlong );
			}
			
			foreach( KeyValuePair<string, SkillLevel> pair in MuseumDataObject.levelOf )
			{
				if( MuseumDataObject.awardingSkill == null || pair.Key != MuseumDataObject.awardingSkill )
					showFragment( pair.Key );
			}
			
			SoundEngine.Instance.OnAudioWordEvent += AudioWordIn;
			
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.MUSEUM, "Background" );
			if( clip != null )
				SoundEngine.Instance.PlayTheme( clip );
			
			Resize( Screen.width, Screen.height );
			
			float scale = 1;
			if( Screen.width == 1024 ) scale = .75f;
			else if( PlatformUtils.GetPlatform() == Platforms.IOSRETINA ) scale = 1.5f;
			var userPortrait = new HatchUserButton();
			
			userPortrait.User = MuseumDataObject.user;
			userPortrait.mouseChildrenEnabled = false;
			userPortrait.mouseEnabled = false;
			userPortrait.scaleX = userPortrait.scaleY = ( 80f / userPortrait.width ) * scale;
			userPortrait.x = userPortrait.y = 25f;
			addChild( userPortrait );
			
			MovieClip MO = new MovieClip( "MenuOverlay.swf", "MenuOverlay" );
			stopBtn = MO.getChildByName<MovieClip>("stopBtn");

			stopBtn.gotoAndStop(language);

			stopBtn.x = 10;
			stopBtn.y = ( Screen.height - ((stopBtn.height * scale) + 10) ) - stopBtn.getBounds(stopBtn).y;
			stopBtn.scaleX = stopBtn.scaleY = scale;
			
			stopBtn.addEventListener( MouseEvent.CLICK, stopHit );
			addChild(stopBtn);
			
			pulseCount = 0;
			exploreCount = 0;
		}
		
		public void AudioWordIn( string word )
		{
			SoundEngine.SoundBundle bundle;
			AudioClip clip;
				
			switch( word )
			{
			case MUTTER:
				orwellAnim.addEventListener( CEvent.COMPLETE, introPaceFinish );
				break;
				
			case INTRO:
			case PROGRESS:
				orwellAnim.loops = false;
//				orwellAnim.addEventListener( CEvent.COMPLETE, speechFinish);
				createOrwell(3);
				TimerUtils.SetTimeout( 0.5f, moveAlong );
				break;
				
			case PROMPT:
			case IDLE:
				stopTimer();
				orwellAnim.loops = false;
//				orwellAnim.addEventListener( CEvent.COMPLETE, speechFinish);
				createOrwell(4);
				doorPulse();
				
				Timeout = TimerUtils.SetTimeout( totalTime, idleAnim );
				if( !door.hasEventListener( MouseEvent.CLICK ) )
					door.addEventListener( MouseEvent.CLICK, moveAlong );
				break;
				
			case RETURN:
				fadeToReward();
				break;
				
			case REWARD:
				if( !string.IsNullOrEmpty( endAnimFlag ) )
				{
					returnFade();
					
					TimerUtils.SetTimeout( 0.20f, endAdventurePlay );
				}
				else
					AudioWordIn( INTERACT );
				break;
				
			case ENDANIMATION:
				//check if animation has finished (mc.isplaying) if not, set flag so animation will trigger such
				// - if flag has already been set once, assume that the animation is finished
				if( endAnimFlag == "waiting" && endAnimation.isPlaying )
					endAnimFlag = "goForIt";
				else
				{
					// - if animation IS finished, show artifacts, hide animation, then AudioWordIn( INTERACT );
					foreach( MovieClip mc in artifacts )
					{
						mc.alpha = 1;
					}
					removeChild( endAnimation );
					endAnimation = null;
					
					AudioWordIn( INTERACT );
				}
				break;
				
			case INTERACT:
				returnFade();
				
				bundle = new SoundEngine.SoundBundle();
				
				if( !orwellAnim.loops )
				{
					createOrwell(2);
					orwellAnim.loops = true;
				}
				
				if( exploreCount < 5 )
				{
					exploreActive();
				
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.MUSEUM, audioPrefix + "exploration-invite" );
					bundle.AddClip( clip, PROMPT, clip.length );
				}
				else
				{
					int i = (int)UnityEngine.Random.Range( 1, 6 );
					if( i > 5 ) i--;
					
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.MUSEUM, audioPrefix + "automatic-progression-" + i );
					bundle.AddClip( clip, PROGRESS, clip.length );
				}
				
				SoundEngine.Instance.PlayBundle( bundle );
				break;
				
			default:
				break;
			}
		}
		
		private void idleAnim()
		{
			stopTimer();
			
			createOrwell(2);
			orwellAnim.loops = true;
			orwellAnim.addEventListener( CEvent.COMPLETE, idleAnimFinish );
			
			int i = (int)UnityEngine.Random.Range( 1f, 6f );
			if( i > 5 ) i--;
			
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.MUSEUM, audioPrefix + "generic-progression-" + i );
			if( clip == null )
			{
				DebugConsole.LogError( "Museum Idle Prompt not found" );
				return;
			}
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			bundle.AddClip( clip, IDLE, clip.length );
			
			SoundEngine.Instance.PlayBundle( bundle );
			
//			doorPulse();
		}
		
		private void idleAnimFinish( CEvent e )
		{
			DebugConsole.Log( "Orwell Idle Animation Finish" );
		}
		
		private void doorPulse()
		{
			if( door.filters.Count < 1 )
			{
				door.AddFilter( FiltersFactory.GetYellowGlowFilter() );
				Sprite temp = door.filters[0].GetSprite();
				glowScale = temp.scaleX;
				glowRect = new Rect(temp.x, temp.y, temp.width * 0.05f, temp.height * 0.05f);
			}
			
			float gwide = glowRect.width;
			float ghigh = glowRect.height;
			float gx = glowRect.x;
			float gy = glowRect.y;
			
			//System.Collections.Hashtable hash;
			switch( pulseCount )
			{
			case 0:
			case 2:
			case 4:
			//	hash = Tweener.Hash( "time", 0.1f, "scaleX", glowScale * 1.1f, "scaleY", glowScale * 1.1f, "x", gx - gwide, "y", gy - ghigh );
			//	Tweener.addTween( door.filters[0].GetSprite(), hash ).OnComplete( doorPulse );

				Tweener.addTween( door.filters[0].GetSprite(), Tweener.Hash( "time", 0.1f, "scaleX", glowScale * 1.1f, "scaleY", glowScale * 1.1f, "x", gx - gwide, "y", gy - ghigh ) ).OnComplete( doorPulse );
				break;
				
			case 1:
			case 3:
			case 5:
			//	hash = Tweener.Hash( "time", 0.1f, "scaleX", glowScale * 1f, "scaleY", glowScale * 1f, "x", gx, "y", gy );
			//	Tweener.addTween( door.filters[0].GetSprite(), hash ).OnComplete( doorPulse );

				Tweener.addTween( door.filters[0].GetSprite(), Tweener.Hash( "time", 0.1f, "scaleX", glowScale * 1f, "scaleY", glowScale * 1f, "x", gx, "y", gy ) ).OnComplete( doorPulse );
				break;
				
			default:
				door.filters[0].Destroy();
				door.RemoveFilter( door.filters[0] );
				pulseCount = -1;
				break;
			}
			pulseCount++;
		}
		
		private void introPath()
		{
			createOrwell( 0 );
			orwellAnim.loops = true;
			
			orwellAnim.animController.AnimStart += () => {
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.MUSEUM, audioPrefix + "worried-muttering" );
				bundle.AddClip(clip, MUTTER, clip.length - 1.15f);
				
				SoundEngine.Instance.PlayBundle(bundle);
			};
		}
		
		private void introPaceFinish( CEvent e )
		{
			DebugConsole.Log( "Orwell01a Finished" );
			
			createOrwell( 1 );
			orwellAnim.addEventListener( CEvent.COMPLETE, surpriseFinish );
			
			float time = audioPrefix == "" ? 1.25f : 2.25f;
			Timeout = TimerUtils.SetTimeout( time, surpriseSpeech );
		}
			
		private void surpriseSpeech()
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.MUSEUM, audioPrefix + "welcome" );
			bundle.AddClip(clip);
			clip = SoundUtils.LoadGlobalSound( MovieClipFactory.MUSEUM, audioPrefix + "story-introduction" );
			bundle.AddClip( clip, INTRO, clip.length - 0.5f );
			
			SoundEngine.Instance.PlayBundle(bundle);
		}
		
		private void surpriseFinish( CEvent e )
		{
			DebugConsole.Log( "Orwell01b Finished" );
			
			createOrwell( 2 );
			orwellAnim.loops = true;
		}
		
		private void speechFinish( CEvent e )
		{
			DebugConsole.Log("Orwell01c Finished" );
			
			createOrwell( 3 );
		}
		
		private void rewardPath()
		{
			createOrwell( 5 );
			orwellAnim.addEventListener( CEvent.COMPLETE, rewardAnimFinished );
			
			MovieClip mc = background.getChildByName<MovieClip>( MuseumDataObject.awardingSkill );
			mc.getChildAt( mc.numChildren - 1 ).alpha = 0;
			
			orwellAnim.animController.AnimStart += () => { Timeout = TimerUtils.SetTimeout( 4.55f, rewardContinue ); };
		}
		
		private void rewardContinue()
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			
			int i = (int)UnityEngine.Random.Range( 1, 5 );
			if( i > 4 ) i--;
			
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.MUSEUM, audioPrefix + "generic-happy-" + i );
			bundle.AddClip(clip, RETURN, clip.length);
			clip = getArtfactNaration( MuseumDataObject.awardingSkill, MuseumDataObject.awardingLevel );
			bundle.AddClip(clip, REWARD, clip.length);
			
			SoundEngine.Instance.PlayBundle(bundle);
		}
		
		private void createNiche( string skillName, SkillLevel lvl )
		{
			greenFade = new Sprite();
			greenFade.graphics.drawSolidRectangle( new Color( 41f/255f, 162f/255f, 0 ), 0, 0, Screen.width, Screen.height );
			greenFade.alpha = 0;
			
			bigNiche = MovieClipFactory.GetCloseArtifact( skillName );
			float scale = (float)Screen.width / bigNiche.width;
			bigNiche.y = ( Screen.height - ( bigNiche.height * scale ) ) / 2f;
			bigNiche.gotoAndStop( SkillLevelUtil.FromId( lvl ) );
			bigNiche.scaleX = bigNiche.scaleY = scale;
			
			greenFade.addChild( bigNiche );
			nicheLayer.addChild( greenFade );
		}
		
		private void fadeToReward()
		{
			createNiche( MuseumDataObject.awardingSkill, MuseumDataObject.awardingLevel );
			
			clickArtifact = new FilterMovieClip( bigNiche.getChildAt<MovieClip>(bigNiche.numChildren - 1) );
			clickArtifact.y += 25;
			clickArtifact.x += 25;
			clickArtifact.alpha = 0;
			bigNiche.addChild( clickArtifact );
			
			Tweener.addTween( greenFade, Tweener.Hash( "time", 0.25f, "alpha", 1f ) ).OnComplete( showShard );
		}
		
		private void showShard()
		{
			Tweener.addTween( clickArtifact, Tweener.Hash( "time", 0.25f, "alpha", 1f, "x", clickArtifact.x - 25, "y", clickArtifact.y - 25 ) ).OnComplete( glowShard );
			
			
			MovieClip mc = background.getChildByName<MovieClip>( MuseumDataObject.awardingSkill );
			mc.getChildAt( mc.numChildren - 1 ).alpha = 1f;

			FXController.instance.Start (this);
			FXController.instance.EnableLightShaft (this, new Vector2 (Screen.width * 0.5f, Screen.height));
		}
		
		private void glowShard()
		{
			//clickArtifact.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
			SoundUtils.PlaySimpleEffect("click");
		}
		
		private void returnFade()
		{
			if( greenFade != null )
			{
				Tweener.removeTweens( greenFade );
				Tweener.addTween( greenFade, Tweener.Hash( "time", 0.25f, "alpha", 0f ) ).OnComplete( clearFade );
			}
		}
		
		private void clearFade()
		{
			FXController.instance.Dispose ();
			
			nicheLayer.removeChild( greenFade );
			bigNiche = null;
			greenFade = null;
			
			if( clickArtifact != null )
			{
				clickArtifact.Destroy();
				clickArtifact = null;
			}
			
			MuseumDataObject.awardingSkill = null;
		}
		
		private void rewardAnimFinished( CEvent e )
		{
			DebugConsole.Log( "Orwell03 Finished" );
			
			createOrwell( 2 );
			orwellAnim.loops = true;
		}
		
		private void endAdventurePlay()
		{
			//hide artifacts
			foreach( MovieClip mc in artifacts )
			{
				mc.alpha = 0;
			}
			//create animation
			endAnimation = MovieClipFactory.GetEndAnim( endAnimFlag );
			endAnimation.y = background.y;
			endAnimation.scaleX = endAnimation.scaleY = background.scaleX;
			addChildAt( endAnimation, getChildIndex(orwellLayer) );
			//add frame script to prevent loop
			// - also check if the audio has finished (reuse endAnimFlag?) is so: then AudioWordIn( ENDANIMATION );
			endAnimation.addFrameScript( endAnimation.totalFrames, (e) => { endAnimation.stop(); if( endAnimFlag == "goForIt" ) AudioWordIn( ENDANIMATION ); } );
			//play audio
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.MUSEUM, audioPrefix + "end-of-adventure-" + endAnimFlag );
			bundle.AddClip( clip, ENDANIMATION, clip.length );
			SoundEngine.Instance.PlayBundle( bundle );
			
			endAnimFlag = "waiting";
		}
		
		private void exploreActive()
		{
			foreach( MovieClip mc in artifacts )
			{
				if( mc.hasEventListener( MouseEvent.CLICK ) ) mc.removeAllEventListeners( MouseEvent.CLICK );
				
				if( !hasExplored.Contains( mc.name ) )
					mc.addEventListener( MouseEvent.CLICK, playExploration );
			}
		}
		
		private void playExploration( CEvent e )
		{
			string skillName = (e.currentTarget as MovieClip).name;
			SkillLevel lvl = MuseumDataObject.levelOf[skillName];
			
			if( lvl == SkillLevel.Tutorial || lvl == SkillLevel.Emerging )
				return;
			
			if( lvl != SkillLevel.Completed || !MuseumDataObject.fullCompletes.Contains( skillName ) )
				lvl = SkillLevelUtil.FromId( (int)lvl );
			
			stopTimer();
			
			exploreCount++;
			hasExplored.Add( skillName );
			
			foreach( MovieClip mc in artifacts )
			{
				if( mc.hasEventListener( MouseEvent.CLICK ) ) mc.removeAllEventListeners( MouseEvent.CLICK );
			}
			
			AudioClip clip = getArtfactNaration( skillName, lvl );
			
			if( clip == null )
				DebugConsole.LogError( "Audio not found: " + skillName.ToLower() + "-" + SkillLevelUtil.GetCode( lvl ) );
			else
			{
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				bundle.AddClip( clip, INTERACT, clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
				
				createOrwell( 2 );
				orwellAnim.loops = true;
				
				createNiche( skillName, lvl );
			
				Tweener.addTween( greenFade, Tweener.Hash( "time", 0.25f, "alpha", 1f ) );
			}
		}
		
		private AudioClip getArtfactNaration( string skillName, SkillLevel level )
		{
			string lvl = SkillLevelUtil.GetCode( level );
			
			string prefix = SkillImplementationUtil.getLangTagforSkill( skillName, LanguageProfileUtility.FromId( MuseumDataObject.user.Profile ) ).Replace('-', '/');
			
			return SoundUtils.LoadGlobalSound( MovieClipFactory.MUSEUM, prefix + skillName.ToLower() + "-" + lvl );
		}
		
		private void showFragment( string s )
		{
			int lvl = (int)MuseumDataObject.levelOf[ s ];
			if( MuseumDataObject.fullCompletes.Contains( s ) )
				lvl++;
			
			MovieClip clip = background.getChildByName<MovieClip>( s );
			if( clip == null )
			{
				DebugConsole.Log( "Clip for skill not found: " + s );
				return;
			}
			
			for( int i = 1; i < lvl; i++ )
			{
				MovieClip sub = MovieClipFactory.GetArtifactShard( s );
				sub.gotoAndStop( i + 1 );
				sub.removeChildAt( 0 );
				clip.addChild( sub );
			}
			
			artifacts.Add( clip );
		}
		
		private void createOrwell( int animNum )
		{
			if( orwellAnim != null )
				orwellAnim.Dispose();
			
			orwellAnim = new Base3dCharacter( loadedAnimations[ animNum ], 20f );
			orwellAnim.camSize = 20f;
			orwellAnim.camTarg = orwellRect;
			orwellLayer.addChild( orwellAnim );
		}
		
		public void moveAlong() { moveAlong(null); }
		public void moveAlong( CEvent e )
		{
			SoundEngine.Instance.StopAll();
			dispatchEvent( new MuseumEvent( MuseumEvent.COMPLETED, true, false ) );
		}
		
		public void Resize(int _width, int _height)
		{
			if( background == null )
				return;
			
			Rect clipping;
			if( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA )
				clipping = new Rect( 0, 0, 2048f, 1536f );
			else
				clipping = new Rect( 0, 0, 1024f, 768f );
			
			float scale = (float)_width / (clipping.width);
			background.x = 0;
			background.y = ( (float)_height - ( (clipping.height) * scale ) ) / 2f;
			background.scaleX = background.scaleY = scale;
		}
		
		public void Start(){}
		
		private void stopTimer()
		{
			if( Timeout != null )
			{ 
				Timeout.Unload();
				Timeout = null;
			}
		}
		
		private ConfirmationMenu cm;
		private void stopHit( CEvent e )
		{
			if(cm != null) return;

			cm = MovieClipFactory.CreateConfirmationMenu(language);
			addChild(cm);
			cm.CenterInScreen();
			SoundEngine.Instance.SendPauseToken();
			Time.timeScale = 0;
			
			cm.Reject = () => 
			{
				Resume(); 
			};
			
			cm.Accept = () =>
			{
				SoundEngine.Instance.StopAll();
				SoundEngine.Instance.PlayTheme(null);
				Resume();	

				dispatchEvent( new MuseumEvent( MuseumEvent.STOPPED, true, false ) );
			};
		}
		
		private void Resume()
		{
			if (cm != null) {
				removeChild(cm);
				cm.Unload();
				cm = null;
			}
			Time.timeScale = 1;
			SoundEngine.Instance.SendUnPauseToken();
		}
			
		public void Dispose()
		{	
			if( orwellAnim != null )
				orwellAnim.Dispose();
			
			if( bigNiche != null )
				Tweener.removeTweens( bigNiche );
			
			if( clickArtifact != null ){
				Tweener.removeTweens( clickArtifact );
				clickArtifact.Destroy();
			}
			
			if( stopBtn != null ){
				stopBtn.removeEventListener( MouseEvent.CLICK, stopHit );
			}
			
			if( door != null){
				Tweener.removeTweens( door );
				door.removeAllEventListeners( MouseEvent.CLICK );
				door.Destroy();
			}
			
			if( artifacts != null )
			{
				foreach( MovieClip mc in artifacts )
				{
					if( mc.hasEventListener( MouseEvent.CLICK ) ) mc.removeAllEventListeners( MouseEvent.CLICK );
				}
			}
			
			if( endAnimation != null )
				endAnimation.stop();
			
			if( loadedAnimations != null )
				loadedAnimations.Clear();
			
			stopTimer();
			
			if( this.parent != null )
				this.parent.removeChild( this );
			if( this.View.parent != null )
				this.View.parent.removeChild( this.View );

			FXController.instance.Dispose ();
			
			SoundEngine.Instance.OnAudioWordEvent -= AudioWordIn;

			BuiltinResourceLoader loader = pumpkin.display.MovieClip.rootResourceLoader as BuiltinResourceLoader;
			loader.unloadSWF("Museum.swf");
			
			Timeout = TimerUtils.SetTimeout( 0.25f, timedClear );
//			timedClear ();
			
			MuseumDataObject.clearAwarding();
		}
		
		public void timedClear()
		{
			//stopTimer();

			MovieClipFactory.clearTextureSpecificSkill( "Museum" );

			AssetLoader.Instance.UnloadBundle( "Museum" );
			
			Resources.UnloadUnusedAssets();

			Debug.Log ("CLEANED UP THE MUSEUM");
		}
	}
	
	public class MuseumDataObject
	{
		public static Dictionary<string, SkillLevel> levelOf;
		public static List<string> fullCompletes;
		public static string awardingSkill;
		public static SkillLevel awardingLevel;
		public static Child user;
		public static string nextSkill;
		protected static SkillSessionContext context;
		private static DBObject dbo;
		public static int skillCount = 18;
		
		public static void loadDB ( DBObject obj )
		{
			dbo = obj;
		}
		
		public static void initiate ( Child c )
		{
			DebugConsole.Log( "Initiating Museum Data Object" );
			levelOf = new Dictionary<string, SkillLevel>();
			fullCompletes = new List<string>();
			context = new DatabaseSkillContext( dbo, c );
			user = c;
		}
		
		public static void populate ()
		{
			DebugConsole.Log( "Populating Museum Data Object" );
			
			List<Skill> skillsList = dbo.LoadSkills();
			fullCompletes = new List<string>();
			
			skillCount = 0;
			
			foreach( Skill s in skillsList )
			{
				if( populate( s ) )
					skillCount++;
			}
		}
		
		public static bool populate ( Skill s ) { return populate( s, true ); }
		public static bool populate ( Skill s, bool fillFC )
		{
			string skillName = s.linkage;
			if( !SkillImplementationUtil.isSkillValidforProfile( skillName, LanguageProfileUtility.FromId( user.Profile ) ) )
				return false;
			
			SkillLevel lvl = context.GetLevel( s );
	    	levelOf[skillName] = lvl;
			
			if( lvl == SkillLevel.Completed )
			{
				if( context.hasMasteredCompleted( s ) )
				{
					if( fillFC && !fullCompletes.Contains(skillName) )
						fullCompletes.Add(skillName);
				}
				else if( fullCompletes.Contains(skillName) )
					fullCompletes.Remove(skillName);
			}
			
			return true;
		}
		
		public static void updateSkill ( string skillName )
		{
			updateSkill( dbo.LoadSkills().Find( (obj) => obj.linkage == skillName ) );
		}
		public static void updateSkill ( Skill s )
		{
			context.Update();
			populate( s, false );
		}
		
		public static void clearAwarding()
		{
			awardingSkill = null;
		}
	}
	
	public class MuseumEvent : CEvent
	{
		public static string COMPLETED = "MuseumComplete";
		public static string STOPPED = "MuseumStop";
		public static string LOADED = "MuseumLoaded";
		
		public MuseumEvent( string _type ) : base( _type )
		{
		}
		
		public MuseumEvent( string _type, bool _bubbles, bool _cancelable ) : base( _type, _bubbles, _cancelable )
		{
		}
	}
}
