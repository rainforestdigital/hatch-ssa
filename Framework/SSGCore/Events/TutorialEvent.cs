using UnityEngine;
using System.Collections;

using pumpkin;
using pumpkin.events;

namespace SSGCore
{

	public class TutorialEvent : CEvent
	{
	
		public static string START = "Tutorial_Start";
		public static new string COMPLETE = "Tutorial_Complete";
		public static string YOUR_TURN = "yourTurn";
		public static string STOP_TIMER = "stopTimer";
		
		public TutorialEvent( string _type ) : base( _type )
		{
			
		}
		
		public TutorialEvent( string _type, bool _bubbles, bool _cancelable ) : base( _type, _bubbles, _cancelable )
		{
			
		}
		
	}
	
}