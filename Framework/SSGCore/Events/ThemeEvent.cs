using UnityEngine;
using System.Collections;

using pumpkin;
using pumpkin.events;

namespace SSGCore
{

	public class ThemeEvent : CEvent
	{
	
		public static string START = "Theme_Start";
		public static new string COMPLETE = "Theme_Complete";
		public static string CONTINUE = "Theme_Continue";
		public static string OPPORTUNITY_COMPLETE = "Theme_Opportunity_Complete";

		public static string SESSION_COMPLETE = "Session_Complete";
		
		public ThemeEvent( string _type ) : base( _type )
		{
			
		}
		
		public ThemeEvent( string _type, bool _bubbles, bool _cancelable ) : base( _type, _bubbles, _cancelable )
		{
			
		}
	}

}