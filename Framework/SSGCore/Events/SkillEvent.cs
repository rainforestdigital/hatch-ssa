using UnityEngine;
using System.Collections;

using pumpkin;
using pumpkin.events;

namespace SSGCore
{

	public class SkillEvent : CEvent
	{
	
		public static string DEBUG = "Skill_Debug";
		public static string INITIALIZED = "Skill_Initialized";
		public static string SKILL_COMPLETE = "Skill_Complete";
		public static string COMPLIMENT_REQUEST = "Skill_Compliment_Request";
		public static string CRITICISM_REQUEST = "Skill_Criticism_Request";
		public static string INCORRECT = "Skill_Incorrect";
		public static string CORRECT = "Skill_Correct";
		public static string OPPORTUNITY_CHANGE = "Skill_Opportunity_Change";
		public static string OPPORTUNITY_START = "Skill_Opportunity_Start";
		public static string OPPORTUNITY_COMPLETE = "Skill_Opportunity_Complete";
		public static string FLAG_TEACHER = "Skill_Flag_Teacher";
		public static string END_THEME_REQUEST = "End_Theme_Request";
		public static string ANIMATION_REQUEST = "Skill_Animation_Request";
		public static string INSTRUCTION_START = "Skill_Instruction_Start";
		public static string INSTRUCTION_END = "Skill_Instruction_End";
		public static string HELP_END = "Help_Tutorial_End";
		public static string TIMER_COMPLETE = "TimerComplete";
		public static string BOOKMARK_NOW = "Calling_For_A_Bookmark";
		public static string TIMEOUT_END = "Special_Timeout_Scenario";
		
		private object val;
		public object Value{ get{ return val; } }
		
		public SkillEvent( string _type ) : base( _type )
		{
			
		}
		
		public SkillEvent( string _type, bool _bubbles, bool _cancelable ) : base( _type, _bubbles, _cancelable )
		{
			
		}
		
		public SkillEvent( string _type, bool _bubbles, bool _cancelable, object value ) : base( _type, _bubbles, _cancelable )
		{
			this.val = value;
		}
		
	}
	
}