using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using SSGCore.RMS.Webservice;
using HatchFramework;

namespace SSGCore.RMS
{
	public class DataSync : MonoBehaviour
	{		
		
		private SSGCore.DBObject db;
		
		#region Auto-instantiation
		protected static DataSync instance;
		
		public static DataSync Instance {
			get {
				if (instance == null) {
					GameObject rmsDataSync = new GameObject ("RMS Data Sync");
					instance = rmsDataSync.AddComponent<DataSync> ();
				}
				return instance;
			}
		}
		#endregion
		
		public delegate void CompletedCallback (bool status);
	
		private CompletedCallback syncClassroomsCallback;
		private CompletedCallback syncEulaCallback;
		private CompletedCallback syncSettingsCallback;
		private CompletedCallback syncConfigCallback;
		private CompletedCallback syncSessionsCallback;
		private CompletedCallback syncLocksCallback;
		private CompletedCallback syncStaticsCallback;
		private CompletedCallback syncRegistrationCallback;
		private CompletedCallback syncBundleCallback;
		private CompletedCallback syncWeightsCallback;
		private CompletedCallback syncNewContentCallback;
		private CompletedCallback syncStudentsCallback;
		private CompletedCallback syncRefocusCallback;
	
		
		#region Classrooms
		public void SyncClassrooms (DBObject db, string device_id, CompletedCallback callback)
		{
			syncClassroomsCallback = callback;
			(new Webservice.Classroom ()).Fetch (device_id, GlobalConfig.GetProperty("version"), OnFetchClassroomsComplete);
		}
		
		private void OnFetchClassroomsComplete (bool status)
		{
			if(status) {
				db.ReplaceClassrooms(Webservice.Classroom.fetched);
				db.LoadClassrooms();
			}

			syncClassroomsCallback (status);
		}
		#endregion
		
		
		
		
		public void SyncNewContent (DBObject db, string device_id, CompletedCallback callback)
		{
			this.db = db;
			syncNewContentCallback = callback;
			// clear current user id, so all sessions and locks are loaded
			// TODO: consider doing explicit query for just need_sync=1
			db.currentUserID = -1;
			db.LoadUsers();
			db.LoadSessions();
			db.LoadLocks();
			var dirtyUsers = db.Users.FindAll(x=>x.NeedSync == 1);
			var dirtySessions = db.Sessions.FindAll(x=>x.NeedSync == 1);
			var dirtyLocks = db.Locks.FindAll(x=>x.NeedSync == 1);
			
			if((dirtyUsers.Count == 0) && (dirtySessions.Count == 0) && (dirtyLocks.Count == 0)) {
				DebugConsole.Log("Nothing changed, skipping upload of new content");
				callback(true);
				return;
			}
			
			 (new Webservice.Sync()).Save(db.GetDeviceUid(), 
				dirtyUsers, 
				dirtySessions, 
				dirtyLocks, GlobalConfig.GetProperty("version"), "", OnSaveNewContentComplete);
		}
		
		private void OnSaveNewContentComplete (bool status)
		{
			if(status) {
				//retarget the new users!
				DebugConsole.Log("RETARGETING NEW STUDENT ID");
				Dictionary<string, string> newIDs = Webservice.Sync.newStudentIDs;
				db.ApplyNewUsers(newIDs);
				
			}else{
				DebugConsole.LogError("FAILED TO GET NEW STUDENT RECORDS");	
			}

			syncNewContentCallback (status);
		}
		
		#region RegistrationCheck
		public void CheckRegistration (string device_id, CompletedCallback callback)
		{
			syncRegistrationCallback = callback;
			(new Webservice.IsDeviceRegistered ()).Fetch (device_id, GlobalConfig.GetProperty("version"), OnCheckRegistrationComplete);
		}
		
		private void OnCheckRegistrationComplete (bool status)
		{
			if(status) {
				if(!Webservice.IsDeviceRegistered.isRegistered) {
					Debug.LogWarning("Device not registered, clearing db");
					db.ClearNonstatic();
				}
			}

			syncRegistrationCallback (status);
		}
		#endregion
		
		#region SaveEulaAgreement
		public void SyncEula (string device_id, string username, DateTime date, CompletedCallback callback)
		{
			syncEulaCallback = callback;
			(new Webservice.SaveAgreement ()).Save(device_id, username, date, OnSyncEula);
		}
		
		private void OnSyncEula (bool status)
		{
			if(status) {
				DebugConsole.Log("Eula agreement synced");
			}

			syncEulaCallback (status);
		}
		#endregion
		
		#region Statics
		public void SyncStatics (DBObject db, string device_id, CompletedCallback callback)
		{
			syncStaticsCallback = callback;
			(new Webservice.Statics ()).Fetch (device_id, OnFetchStaticsComplete);
		}
		
		private void OnFetchStaticsComplete (bool status)
		{
			if(status) {
				db.BeginTransaction();
				db.ClearStatics ();
				foreach (RMS.Webservice.Statics.Family f in RMS.Webservice.Statics.fetched.families) {
					db.InsertFamily (Convert.ToInt32 (f.id), f.name);
				}
				
				foreach (RMS.Webservice.Statics.Level l in RMS.Webservice.Statics.fetched.levels) {
					db.InsertLevel (Convert.ToInt32 (l.id), l.code, l.name);
				}
				db.EndTransaction();
			}

			syncStaticsCallback (status);
		}
		#endregion
		
		#region Weights
		public void SyncWeights (DBObject db, string device_id, CompletedCallback callback)
		{
			syncWeightsCallback = callback;
			(new Webservice.GetAllSSGActivities ()).Fetch (device_id, GlobalConfig.GetProperty("version"), OnFetchWeightsComplete);
		}
		
		private void OnFetchWeightsComplete (bool status)
		{
			if(status) {
				db.ReplaceWeights(Webservice.GetAllSSGActivities.fetched);
			}

			syncWeightsCallback (status);
		}
		#endregion
		
		#region AppBundles
		
		public void SyncBundles (CompletedCallback callback)
		{
			syncBundleCallback = callback;
			(new Webservice.GetAppBundle()).Fetch(PlatformUtils.GetDeviceModel(), GlobalConfig.GetProperty("version"), OnFetchBundlesComplete);
			
		}
		
		private void OnFetchBundlesComplete (bool status)
		{
			
			syncBundleCallback (status);
		}
		#endregion
		
		#region Sessions
		public void SyncRefocus (DBObject db, string device_id, CompletedCallback callback)
		{
			syncRefocusCallback = callback;
			(new Webservice.Refocus()).Fetch(device_id, OnFetchRefocusComplete);
			//(new Webservice.Session()).Fetch(device_id, OnFetchSessionsComplete);
		}
		
		private void OnFetchRefocusComplete (bool status)
		{
			if (status) {
				db.BeginTransaction();
				db.ClearRefocus();
				List<Refocus> refocuses = Webservice.Refocus.fetched;
				foreach(Refocus r in refocuses){
					db.InsertRefocusActivity(r.user_id, r.start_date, r.expiration_date, r.families_id);
				}
				db.EndTransaction();
				
			}
			syncRefocusCallback (status);
		}
		#endregion
		
		#region Sessions
		public void SyncSessions (DBObject db, string device_id, CompletedCallback callback)
		{
			syncSessionsCallback = callback;
			(new Webservice.Session()).Fetch(device_id, OnFetchSessionsComplete);
		}
		
		private void OnFetchSessionsComplete (bool status)
		{
			if (status) {
				db.ReplaceSessions(Webservice.Session.fetched);
			}
			syncSessionsCallback (status);
		}
		#endregion
		
		#region Sessions
		public void SyncLocks(DBObject db, string device_id, CompletedCallback callback)
		{
			syncLocksCallback = callback;
			(new Webservice.Lock()).Fetch(device_id, OnFetchLocksComplete);
		}
		
		private void OnFetchLocksComplete (bool status)
		{
			if (status) {
				db.ReplaceLocks(Webservice.Lock.fetched);
				
			}
			syncLocksCallback (status);
		}
		#endregion
		
		
		#region Settings
		public void SyncSettings (DBObject db, string device_id, CompletedCallback callback)
		{
			syncSettingsCallback = callback;
			var binaryVersion = GlobalConfig.GetProperty("version");
			(new Webservice.Settings ()).Fetch (device_id, binaryVersion, "", "", OnFetchSettingsComplete);
		}
		
		private void OnFetchSettingsComplete (bool status)
		{
			if (status) {
				//db.ClearSettings ();
				//Webservice.Settings s = Webservice.Settings.fetched;
				//DebugConsole.LogWarning("NOT USING LIVE SETTINGS CURRENTLY");
				//db.InsertSetting (s.id, s.rms_url, s.device_id, s.device_uid, s.organization_id, s.school_id, "salt", "app version", "db version", s.device_alias);
				
				// Just Set device alias for now
				Webservice.Settings s = Webservice.Settings.fetched;
				db.SetDeviceAlias(s.device_alias);
			}
			syncSettingsCallback (status);
		}
		#endregion
		
		#region Settings
		public void SyncConfiguration (DBObject db, string device_id, CompletedCallback callback)
		{
			syncConfigCallback = callback;
			(new Webservice.GetDeviceConfiguration()).Fetch(device_id, GlobalConfig.GetProperty("version"), OnFetchConfigComplete);
			
		}
		
		private void OnFetchConfigComplete (bool status)
		{
			if (status) {
			
				// Just Set device alias for now
				Webservice.GetDeviceConfiguration s = Webservice.GetDeviceConfiguration.fetched;
				DebugConsole.Log("SETTING THE RMS URL: "+s.RMSURL);
				
				if(GlobalConfig.GetProperty("buildMode") != "Debug"){

					GlobalConfig.SetProperty("RMS_URL", s.RMSURL+"/");
					//Enable or disable the console!
					DebugConsole.Instance.enabled = !(s.ShowDebugConsole == "0");
				}else{
					DebugConsole.LogWarning("NOT USING SERVER CONFIGURATION - IN DEBUG MODE");	
				}
				
				
			}
			syncConfigCallback (status);
		}
		#endregion
		
		#region Students
		private string syncStudentsDeviceID;

		public void SyncStudents (DBObject db, string device_id, CompletedCallback callback)
		{
			syncStudentsCallback = callback;
			
			(new Webservice.Student ()).Fetch (device_id, GlobalConfig.GetProperty("version"), OnFetchStudentsComplete);
		}
		
		private void OnFetchStudentsComplete (bool status)
		{			
			if (status) {
				db.ReplaceUsers(Webservice.Student.fetched);
				db.LoadUsers();
			}
			syncStudentsCallback (status);
		}
		
		#endregion
		
		#region SyncStudentPictures
		
		protected CompletedCallback studentPicturesCallback;
		// picture_urls is a list of strings such as you would get 
		// in student records from from (new Webservice.Student ()).Fetch(....);
		protected void SyncStudentPictures (DBObject db, string device_id, CompletedCallback callback)
		{			

			syncStudentsDeviceID = device_id;
			studentPicturesCallback = callback;
			
			(new Webservice.ImageTimestamp()).Fetch(device_id, db.GetAllImagePaths(), OnImageTimestampsComplete);
		
		}
		
		
		protected void OnImageTimestampsComplete(bool status){
		
			if(status == false) {
				DebugConsole.LogError("FAILED IMAGE TIMESTAMPS");
				OnMultiEncodeComplete(false);
				return;
			}
			
			List<string> urls = new List<string>();
			foreach(ImageTimestamp itm in Webservice.ImageTimestamp.fetched){
				if(ImageUtility.CheckFileOutdated(itm.url, DateTime.Parse(itm.timestamp)))
					urls.Add(itm.url);
			}
			
			if(urls.Count == 0) {
				DebugConsole.Log("All images up to date, continuing");
				OnMultiEncodeComplete(true);
				return;
			}
			
			(new Webservice.MultiEncode ()).Fetch (syncStudentsDeviceID, urls.ToArray(), OnMultiEncodeComplete);
		}
				
		protected void OnMultiEncodeComplete (bool status)
		{			
			//student images shoud jsut be updated
			
			syncRemaining--;
			if(status == false)
				DebugConsole.LogError("FAILED MULTIENCODE");
			
			studentPicturesCallback(status);
			
		}

		#endregion
		
		#region Testing
		public Vector2 buttonsScrollPosition;

		public void TestGUI (DBObject db)
		{
			this.db = db;
			GUILayout.BeginVertical ("box");
			GUILayout.Label ("Data Sync Tests");
			buttonsScrollPosition = GUILayout.BeginScrollView (buttonsScrollPosition, GUILayout.Width (500), GUILayout.Height (200));
			
			if(GUILayout.Button("Startup Sync")){
			
				StartCoroutine(SyncStartup(db));
			}
			
			if (GUILayout.Button ("Sync Classrooms")) {
				SSGCore.RMS.DataSync.instance.SyncClassrooms (db, "qaaa-aaaa-aaaa-aaa1", ExampleOnSyncComplete);
			}
			
				
			if (GUILayout.Button ("Sync Settings")) {
				SSGCore.RMS.DataSync.instance.SyncSettings (db, "qaaa-aaaa-aaaa-aaa1", ExampleOnSyncComplete);
			}
					
			
			if (GUILayout.Button ("Sync Statics")) {
				SSGCore.RMS.DataSync.instance.SyncStatics (db, "qaaa-aaaa-aaaa-aaa1", ExampleOnSyncComplete);
			}
			if (GUILayout.Button ("Sync Students")) {
				Debug.Log (db.Users.Count);
				SSGCore.RMS.DataSync.instance.SyncStudents (db, "qaaa-aaaa-aaaa-aaa1", ExampleOnSyncComplete);
			}
//			if (GUILayout.Button ("Sync Student Images")) {
//				SSGCore.RMS.DataSync.instance.SyncStudentPictures (db, "2222-2222-2222-2222", new int[]{56495,56495}, new string[]{"/~ziba/www/html/images/users/56495_picture.jpeg","/~ziba/www/html/images/users/56495_picture.jpeg"}, ExampleOnSyncComplete);
//			}
			GUILayout.EndScrollView ();	
			GUILayout.EndVertical ();

		}
		
		private CompletedCallback MakeHandler(string operationName)
		{
			return (bool status) => {
				syncRemaining--;
				failed |= !status;
				if(!status)
					DebugConsole.LogError("FAILED TO SYNC " + operationName);
			};
		}
		
		public void ExampleOnSyncComplete (bool status)
		{
			Debug.Log ("Thanks for syncing! status: " + status);
			Debug.Log (db.Users.Count); 
		}
		#endregion
		
		protected int syncRemaining = 0;
		private bool failed = false;
		private bool lastSyncSucceeded = false;
		
		// is this the best way to represent this?
		public bool LastSyncSucceeded
		{
			get { return lastSyncSucceeded; }
		}
		
		public IEnumerator SyncStartup(DBObject db) {
			this.db = db;
			
			failed = false;
			
			string register_device_id = db.GetDeviceUid();
			syncRemaining = 1;
			CheckRegistration(register_device_id, MakeHandler("Check Registration"));
			
			while(syncRemaining > 0) yield return new WaitForSeconds(0.05f);
			
			string device_id = db.GetDeviceUid();
			
			if(string.IsNullOrEmpty(device_id)) {
				yield break;
			}
			
			var eula = Eula.LoadEulaInfo();
			if(eula.shouldSync) {
				DebugConsole.Log("Syncing Eula Agreement");
				syncRemaining = 1;
				SyncEula(device_id, eula.username, eula.date, MakeHandler("Eula"));
				
				while(syncRemaining > 0) yield return new WaitForSeconds(0.05f);
				
				if(!failed) {
					Eula.StoreEulaSync(eula.username, eula.date, device_id);
				}
			}

			syncRemaining = 1;
			SyncNewContent(db, device_id, MakeHandler("New Content"));
			
			while(syncRemaining > 0) yield return new WaitForSeconds(0.05f);
			
			if(failed) {
				lastSyncSucceeded = false;
				db.UpdateDataset();
				yield break;
			}
			
			syncRemaining = 4;
			SyncConfiguration(db, device_id, MakeHandler("Configuration"));
			SyncStatics(db, device_id, MakeHandler("Statics"));
			SyncWeights(db, device_id, MakeHandler("Weights"));
			SyncSettings (db, device_id, MakeHandler("Settings"));
			
			while(syncRemaining > 0) yield return new WaitForSeconds(0.05f);
			
			syncRemaining = 5;
			SyncClassrooms (db, device_id, MakeHandler("Classrooms"));
			SyncStudents (db, device_id, MakeHandler("Students"));
			SyncSessions(db, device_id, MakeHandler("Sessions"));
			SyncLocks(db, device_id, MakeHandler("Locks"));
			SyncRefocus(db, device_id, MakeHandler("Refocus"));
			
			while(syncRemaining > 0) yield return new WaitForSeconds(0.05f);
			
			syncRemaining = 1;
			this.SyncStudentPictures (db, device_id, MakeHandler("Student Pictures"));
			
			while(syncRemaining > 0) yield return new WaitForSeconds(0.05f);
			
			db.UpdateDataset();
			
			lastSyncSucceeded = !failed;
			DebugConsole.Log("Completed startup sync");
		}
		
		public IEnumerator SyncNormal(DBObject db) {
			this.db = db;
			
			failed = false;
			string device_id = db.GetDeviceUid();

			syncRemaining = 1;
			SyncNewContent(db, device_id, MakeHandler("New Content"));
			
			while(syncRemaining > 0) yield return new WaitForSeconds(0.05f);
			
			if(failed) {
				lastSyncSucceeded = false;
				yield break;
			}
			
			syncRemaining = 5;
			SyncClassrooms (db, device_id, MakeHandler("Classrooms"));
			SyncStudents (db, device_id, MakeHandler("Students"));
			SyncSessions(db, device_id, MakeHandler("Sessions"));
			SyncLocks(db, device_id, MakeHandler("Locks"));
			SyncRefocus(db, device_id, MakeHandler("Refocus"));
			
			while(syncRemaining > 0) yield return new WaitForSeconds(0.05f);
			
			syncRemaining = 1;
			this.SyncStudentPictures (db, device_id, MakeHandler("Student Pictures"));
			
			while(syncRemaining > 0) yield return new WaitForSeconds(0.05f);
			
			db.UpdateDataset();
			
			lastSyncSucceeded = !failed;
			DebugConsole.Log("Completed normal sync");
		}
		
		
		
	}
	
}
