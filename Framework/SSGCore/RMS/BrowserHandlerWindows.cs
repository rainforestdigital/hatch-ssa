using System;
using UnityEngine;


namespace SSGCore.RMS
{
	public class BrowserHandlerWindows : BrowserHandlerBase
	{
		
		private WebViewObjectWindows webViewObject;
						
	
		public System.Diagnostics.Process browserProcess;
		private bool adminOpen = false;
		
		public override bool OpenURL (string url)
		{
				
			webViewObject =
			(new GameObject ("WebViewObjectWindows")).AddComponent<WebViewObjectWindows> ();
			
			//NOTE: I wasn't able to test this, but it fixes bug 1206 and will need to be uncommented and tested on windows along with the 2 EvaluateJS
			/*webViewObject.Init ((msg) => {
				Debug.Log (string.Format ("CallFromJS[{0}]", msg));
			});*/
			
			webViewObject.browserHandler = this;		
				

			browserProcess = new System.Diagnostics.Process ();
			//browserProcess.EnableRaisingEvents = true;
			browserProcess.StartInfo.UseShellExecute = false;

			browserProcess.StartInfo.FileName = "Chromium\\Chrome.exe";
			browserProcess.StartInfo.Arguments = url+" --kiosk";
			//browserProcess.Exited += BrowserExited;
			
			//NOTE: To make a call from HTML to Unity, you have to have this first prototype established for the unity object.			
			/*webViewObject.EvaluateJS(
				"window.addEventListener('load', function() {" +
				"	window.Unity = {" +
				"		call:function(msg) {" +
				"			var iframe = document.createElement('IFRAME');" +
				"			iframe.setAttribute('src', 'unity:' + msg);" +
				"			document.documentElement.appendChild(iframe);" +
				"			iframe.parentNode.removeChild(iframe);" +
				"			iframe = null;" +
				"		}" +
				"	}" +
				"}, false);");
			*/
			
			// NOW, set up a prototype that Javascript can call and pass a string value.
			// in javascript, you would call window.ExitWebView();
			/*webViewObject.EvaluateJS(
				"window.addEventListener('load', function() {" +
				"	window.ExitWebView = function() {" +
				"		Unity.call('ExitWebView');" +
				"	}" +
				"}, false);");
			*/
			
			try{
				browserProcess.Start ();
			}catch(System.ComponentModel.Win32Exception ex){
				DebugConsole.LogError("Failed to open Chrome: WIN32EXCEPTION - " + ex.ErrorCode + ":" + ex.Message);
				DebugConsole.Log("Attempting to kill all Chrome instances and start again.");
				
				foreach(System.Diagnostics.Process prs in System.Diagnostics.Process.GetProcessesByName("Chrome"))
				{
					prs.Kill();
				}
				
				try{
					browserProcess.Start();
				}catch(System.ComponentModel.Win32Exception ex2){
					DebugConsole.LogError("Failed to open Chrome, Again: WIN32EXCEPTION - " + ex2.ErrorCode + ":" + ex2.Message);
					DebugConsole.Log("Givng up, Chrome isn't opening.");
				
					return false;
				}
			}
			
			adminOpen = true;
			
			Screen.fullScreen = false;
			return true;
		}
		
		void BrowserExited (object obj, System.EventArgs args)
		{
			OnBrowserExit();
		}
	
		
		public void OnBrowserExit(){
			Debug.Log ("[rms admin] Browser Exited");
			if (ExitedEvent != null) {
				ExitedEvent ();	
				webViewObject.goFullscreen = true;
			}
			adminOpen = false;
			browserProcess.Exited -= BrowserExited;
		}
		
		
		public override bool IsOpen ()
		{
			return adminOpen;
		}
		
	}
}

