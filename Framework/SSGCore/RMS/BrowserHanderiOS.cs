using System;
using UnityEngine;


namespace SSGCore.RMS

{
	public class BrowserHandleriOS : BrowserHandlerBase
	{
		private WebViewObjectiOS webViewObject;

			public BrowserHandleriOS ()
		{
#if !UNITY_IPHONE
			Debug.Log ("SSGCore DLL was not built with iOS support");		
#endif
		}
		
		public override bool OpenURL (string url)
		{
			webViewObject =
			(new GameObject ("WebViewObjectiOS")).AddComponent<WebViewObjectiOS> ();
			webViewObject.Init ((msg) => {
				Debug.Log (string.Format ("CallFromJS[{0}]", msg));
				
				if( msg == "ExitWebView" )
				{
					Close ();
				}
			});
			webViewObject.browserHandler = this;
			webViewObject.LoadURL (url);
			webViewObject.SetVisibility (true);
			webViewObject.SetMargins(0,0,0,0);
			
			//NOTE: To make a call from HTML to Unity, you have to have this first prototype established for the unity object.			
			webViewObject.EvaluateJS(
				"window.addEventListener('load', function() {" +
				"	window.Unity = {" +
				"		call:function(msg) {" +
				"			var iframe = document.createElement('IFRAME');" +
				"			iframe.setAttribute('src', 'unity:' + msg);" +
				"			document.documentElement.appendChild(iframe);" +
				"			iframe.parentNode.removeChild(iframe);" +
				"			iframe = null;" +
				"		}" +
				"	}" +
				"}, false);");
			
			// NOW, set up a prototype that Javascript can call and pass a string value.
			// in javascript, you would call window.ExitWebView();
			webViewObject.EvaluateJS(
				"window.addEventListener('load', function() {" +
				"	window.ExitWebView = function() {" +
				"		Unity.call('ExitWebView');" +
				"	}" +
				"}, false);");
			
			return true;
		}
	
		public override bool Close ()
		{
			webViewObject.SetVisibility(false);
			if (ExitedEvent != null) {
				ExitedEvent ();	
			}
			UnityEngine.GameObject.Destroy(webViewObject.gameObject, 0.5f);
			return true;
		}
	}
}

