using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;

namespace SSGCore.RMS.Webservice
{
	public class Refocus : RequestBase
	{
		public string id;
		public string user_id;
		public string start_date;
		public string expiration_date;
		public string families_id;
		public static List<Refocus> fetched;
		
		public void Fetch (string device_uid, OnCompleteDelegate callback)
		{
			OnComplete = callback;
			string url = Webservice.Controller.Instance.baseURL + "refocus?device_uid=" + device_uid;
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			Webservice.Controller.Instance.Log ("Refocus fetched: ");
			
			if (response.Length == 0) {
				Webservice.Controller.Instance.Log ("No results");
				fetched = new List<Refocus> ();
			} else {
				using(var reader = new StreamReader(new MemoryStream(response, false))) {
					fetched = JsonMapper.ToObject<List<Refocus>> (reader);
				}
				
				foreach (Refocus c in fetched) {
					Webservice.Controller.Instance.Log (c.id.ToString ());
				}
			}
			status = true;
			return base.OnFetchComplete(response);
		}
	}
}

