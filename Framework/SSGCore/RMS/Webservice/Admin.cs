using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;

namespace SSGCore.RMS.Webservice
{
	public class Admin : RequestBase
	{
		public string id;
		public string name;
		public string organization;
		public string phone;
		public string email;
		public static List<Admin> fetched;
		
		public void Fetch (string device_uid, OnCompleteDelegate callback)
		{
			OnComplete = callback;
			string url = Webservice.Controller.Instance.baseURL + "admins?device_uid=" + device_uid;
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			try {
				using(var reader = new StreamReader(new MemoryStream(response, false))) {
					fetched = JsonMapper.ToObject<List<Admin>> (reader);
				}
				
				Webservice.Controller.Instance.Log ("Admins fetched: ");
				foreach (Admin c in fetched) {
					Webservice.Controller.Instance.Log (c.id.ToString ());
				}
				status = fetched.Count > 0;
			} catch (Exception e) {
				Debug.LogException(e);
				status = false;
			}
			return base.OnFetchComplete(response);

		}
	}
}

