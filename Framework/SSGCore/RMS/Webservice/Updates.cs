using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;

namespace SSGCore.RMS.Webservice
{
	public class Updates : RequestBase
	{
		public string version;
		public string preview;
		public string whats_new;
		public string download_url;
		public string md5sum;
		
		public static Updates fetched;
		
		
		public void Fetch (string device_uid, OnCompleteDelegate callback )
		{
			OnComplete = callback;
			string url = Webservice.Controller.Instance.baseURL + "updatesV2?device_uid=" + device_uid;
			Webservice.Controller.Instance.StartCoroutine (Webservice.Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		public void FetchAIO(string device_uid, OnCompleteDelegate callback)
		{
			OnComplete = callback;
			string url = Webservice.Controller.Instance.baseURL + "updates_aioV2?device_uid=" + device_uid;
			Webservice.Controller.Instance.StartCoroutine (Webservice.Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			try {
				using(var reader = new StreamReader(new MemoryStream(response, false))) {
					fetched = JsonMapper.ToObject<Updates> (reader);
				}
				status = true;
			} catch {
				status =  false;
			}
			return base.OnFetchComplete(response);
		}
	}
}

