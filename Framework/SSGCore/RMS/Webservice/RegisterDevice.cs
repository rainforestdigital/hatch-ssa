using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.Text;

namespace SSGCore.RMS.Webservice
{
	public class RegisterDevice : RequestBase
	{
		public delegate void OnRegistrationCompleteDelegate (bool status, Dictionary<string, string> deviceSettings);
		
		public OnRegistrationCompleteDelegate OnRegistrationComplete;
		
		public static bool valid = false;
		
		public void Fetch (string license_key, string serial_num, int model, OnRegistrationCompleteDelegate callback)
		{			
			OnRegistrationComplete = callback;
			
			string url  = Webservice.Controller.Instance.baseURL + "RegisterDevicev2?licenses_key='" + license_key + "'&serial_number='" + serial_num + "'&model_number=" + model;
			DebugConsole.Log ("REGISTERING DEVICE: " + url);
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			/*
			{"org_name":"Hatch","school_name":"Big Dog Day Care","class_name":"Ms Terri's","device_code":"AASC-2X99-38YR-VIH8","serial_number":"'BB541260-5C13-5679-8326-5D2120596FA3'"}
			*/
			
			Dictionary<string, string> deviceSettings = new Dictionary<string, string>();
			deviceSettings.Add("org_name", "");
			deviceSettings.Add("school_name", "");
			deviceSettings.Add("class_name", "");
			deviceSettings.Add("device_code", "");
			deviceSettings.Add("serial_number", "");			
			
			var stringResponse = new UTF8Encoding().GetString(response);
			Debug.Log("ON FETCH COMPLETE FOR REG: " + stringResponse);
			if (!stringResponse.ToLower().Contains("error")) 
			{
				Webservice.Controller.Instance.Log ("Device License Valid");
				valid = true;
				
				JsonReader r = new LitJson.JsonReader(stringResponse);
				if( r.Read() ) //get rid of the first empty row
				{
					string nextKey = "";
					while(r.Read())
					{				
						string str = r.Value as string;
						if( !string.IsNullOrEmpty(str) )
						{
							if( string.IsNullOrEmpty(nextKey) ) nextKey = str;
							else 
							{
								deviceSettings[nextKey] = str;
								nextKey = "";
							}
		     				Debug.Log(str);
						}
					}
				}
			} 
			else 
			{
				Webservice.Controller.Instance.Log ("Device License NOT valid");
				valid = false;			
			}
			
			status = valid;
			
			OnRegistrationComplete(status, deviceSettings);
			return base.OnFetchComplete(response);
		}
		
		protected override void OnFetchFailed (string error)
		{
			base.OnFetchFailed(error);
			OnRegistrationComplete(false, null);
		}
	}
}

