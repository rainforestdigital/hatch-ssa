using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;
using HatchFramework;

namespace SSGCore.RMS.Webservice
{
	public class Session : RequestBase
	{
		public int id;
		public int user_id;
		public string started;
		public int duration;
		public int skill_id;
		public int skill_level_id;
		public string skill_level;
		public double percent;
		public string opportunities;
		public int bookmarked;
		public static List<Session> fetched;
		
		public void Fetch (string device_uid, OnCompleteDelegate callback)
		{
			OnComplete = callback;
			string url = Webservice.Controller.Instance.baseURL + "sessions?device_uid=" + device_uid + "&application=SSA&version=" + GlobalConfig.GetProperty("version");
			url += "&serial=" + HatchFramework.PlatformUtils.GetSerial();
			Webservice.Controller.Instance.StartCoroutine (Webservice.Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			DebugConsole.Log ("Sessions fetched: ");
			if (response.Length == 0) {
				Webservice.Controller.Instance.Log ("No results");
				fetched = new List<Session> ();
				status = false;
			} else {
				try {
					using(var reader = new StreamReader(new MemoryStream(response, false))) {
						fetched = JsonMapper.ToObject<List<Session>> (reader);
					}
					foreach (Session c in fetched) {
						Webservice.Controller.Instance.Log (c.id.ToString ());
//						if( c.skill_id > 18 )
//							DebugConsole.Log( "Spanish Skill: " + c.skill_id + " ; For user: " + c.user_id );
					}
					status = true;
				} catch (Exception e) {
					Debug.LogException(e);
					status = false;
				}
			}
			var result = base.OnFetchComplete (response);
			return result;
		}
		
			
	}
}

