using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;

namespace SSGCore.RMS.Webservice
{
	public class GetAllSSGActivities : RequestBase
	{
		
		public string id;
		public string skill_id;
		public string skill_level_id;
		public string skill_level_code;
		public string weight;
	
		
		
		public static List<GetAllSSGActivities> fetched;
		
		public void Fetch (string device_uid, OnCompleteDelegate callback ){ Fetch(device_uid, "5.6.1.0+", callback); }
		public void Fetch (string device_uid, string version, OnCompleteDelegate callback )
		{
			OnComplete = callback;
			string url = Controller.Instance.baseURL + "GetAllSSAActivities?device_uid='" + device_uid+"'&serial_num=" + HatchFramework.PlatformUtils.GetSerial() + "&version=" + version ;
			Debug.Log (url);
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			try {
				using(var reader = new StreamReader(new MemoryStream(response, false))) {
					fetched = JsonMapper.ToObject<List<GetAllSSGActivities>> (reader);
				}
				
				status = true;
			} catch {
				status = false;
			}
			return base.OnFetchComplete(response);
		}
	}
}

