using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.Text;

namespace SSGCore.RMS.Webservice
{
	public class CheckSerialNum : RequestBase
	{
		public static bool valid = false;
		
		public void Fetch (string serial_num, OnCompleteDelegate callback)
		{
			OnComplete = callback;
			string url = Webservice.Controller.Instance.baseURL + "checkserialnum&serial_num=" + serial_num;
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			var stringResponse = new UTF8Encoding().GetString(response);
			if (stringResponse == "true") {
				Webservice.Controller.Instance.Log ("Serial Valid");
				valid = true;	
			} else {
				Webservice.Controller.Instance.Log ("Serial not valid");
				valid = false;	
			}
			status = true;
			return base.OnFetchComplete(response);
		}
	}
}

