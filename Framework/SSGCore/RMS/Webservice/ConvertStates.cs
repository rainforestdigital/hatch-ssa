using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

namespace SSGCore.RMS.Webservice
{
	public class ConvertStates : RequestBase
	{
	
		public void Convert (OnCompleteDelegate callback)
		{
			OnComplete = callback;
			string url = Webservice.Controller.Instance.baseURL + "convertstates";
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			//Expects empty response
			Webservice.Controller.Instance.Log ("ConvertStates: "+response);
			status = true;
			return base.OnFetchComplete(response);
		}
	}
}

