using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.Text;
using System.IO;
using SSGCore;
using HatchFramework;

namespace SSGCore.RMS.Webservice
{
	public class GetAppBundle : RequestBase
	{
		
		public static string error = "";
		public static string fetched = "";
		
		public void Fetch (int model, string version,  OnCompleteDelegate callback)
		{			
			OnComplete = callback;
			string url  = Webservice.Controller.Instance.baseURL + "GetAppBundlev2?model_id="+model.ToString()+"&app_version="+GlobalConfig.GetProperty("version");
			url = url + "&device_uid=" + SSGEngine.databaseObject.GetDeviceUid() + "&serial_num=" + HatchFramework.PlatformUtils.GetSerial();
			Debug.Log (url);
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			try {
				TextReader reader = new StreamReader(new MemoryStream(response, false));
				fetched = reader.ReadToEnd();
				
				DebugConsole.Log ("AppBundles fetched: "+fetched);
				reader.Close();
				status = true;
			} catch {
				status =  false;
			}
			return base.OnFetchComplete(response);
		}
		
		protected override void OnFetchFailed (string error)
		{
			GetAppBundle.error = error;
			base.OnFetchFailed (error);
		}
	}
}
