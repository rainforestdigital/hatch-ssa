using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;
using System.Text;

namespace SSGCore.RMS.Webservice
{
	public class IsDeviceRegistered : RequestBase
	{
		
	
		public static bool isRegistered = false;
		
		public void Fetch (string device_uid, OnCompleteDelegate callback ){ Fetch(device_uid, "5.6.1.0+", callback); }
		public void Fetch (string device_uid, string version, OnCompleteDelegate callback )
		{
			DebugConsole.Log("Checking registration for: " + device_uid);
			OnComplete = callback;
			string url = Controller.Instance.baseURL + "IsDeviceRegisteredv2?device_uid='" + device_uid+"'&serial_num=" + HatchFramework.PlatformUtils.GetSerial() + "&version=" + version ;
			
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			try {
				var stringResponse = new UTF8Encoding().GetString(response);
				status = true;
				// want to be very conservative in what is accepted
				switch(stringResponse) {
				case "true":
					isRegistered = true;
					break;
				case "false":
					isRegistered = false;
					break;
				default:
					status = false;
					break;
				}
				
			} catch {
				status = false;
			}
			return base.OnFetchComplete(response);
		}
	}
}
