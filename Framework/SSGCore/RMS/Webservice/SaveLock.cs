using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

namespace SSGCore.RMS.Webservice
{
	public class SaveLock : RequestBase
	{
		
		
		public class RawDataStruture
		{
			public string device_uid;
			public string user_id;
			public string skill_id;
			public int is_enabled;
			public string created;

		}
		
		public void Fetch (string device_uid, string user_id, string skill_id, int is_enabled, OnCompleteDelegate callback)
		{
			OnComplete = callback;
			
			string url = Webservice.Controller.Instance.baseURL + "savelock?device_uid=" + device_uid;
			RawDataStruture raw = new RawDataStruture ();
			raw.device_uid = device_uid;
			raw.user_id = user_id;
			raw.skill_id = skill_id;
			raw.is_enabled = is_enabled;
			raw.created = DateTime.UtcNow.ToString ("yyyy-MM-dd HH:mm:ss");
			string json = JsonMapper.ToJson (raw);

			Controller.Instance.StartCoroutine (Webservice.Controller.Instance.RawPostRoutine (url, OnFetchComplete, OnFetchFailed, StrToByteArray (json)));
			
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			Webservice.Controller.Instance.Log ("Save fetched: ");
			if (response.Length == 0) {
				// Success has no results
				Webservice.Controller.Instance.Log ("Lock save silent success");
				status = true;
			} else /*if (response == "400 Lock is not saved")*/ {
				Webservice.Controller.Instance.Log ("Lock save error");
				status = false;
			} 
			return base.OnFetchComplete(response);
		}
	}
}

