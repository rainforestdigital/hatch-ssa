using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;

namespace SSGCore.RMS.Webservice
{
	public class Lock : RequestBase
	{
		public string id;
		public string user_id;
		public string skill_id;
		public string is_enabled;
		public string uid;
		public string created;
		public static List<Lock> fetched;
		
		public void Fetch (string device_uid, OnCompleteDelegate callback)
		{
			OnComplete = callback;
			string url = Webservice.Controller.Instance.baseURL + "locks?device_uid=" + device_uid;
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			Webservice.Controller.Instance.Log ("Locks fetched: ");
			if (response.Length == 0) {
				Webservice.Controller.Instance.Log ("No results");
				fetched = new List<Lock> ();
				status = false;
			} else {
				try {
					using(var reader = new StreamReader(new MemoryStream(response, false))) {
						fetched = JsonMapper.ToObject<List<Lock>> (reader);
					}
					foreach (Lock c in fetched) {
						Webservice.Controller.Instance.Log (c.id.ToString ());
					}
					status = true;
				} catch (Exception e) {
					Debug.LogException(e);
					status = false;
				}
			}
			return base.OnFetchComplete(response);

		}
	}
}

