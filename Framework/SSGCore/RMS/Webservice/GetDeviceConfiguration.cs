using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;

namespace SSGCore.RMS.Webservice
{
	public class GetDeviceConfiguration : RequestBase
	{
		
		public string id = "";
		public string device_id = "";
		public string device_uid="";
		public string debug="";
		public string RMSURL="";
		public string SyncEnabled="";
		public string ShowDebugConsole="";
		public string error="";
	
		public static GetDeviceConfiguration fetched;
		
		public void Fetch (string device_uid, OnCompleteDelegate callback ){ Fetch(device_uid, "5.6.1.0+", callback); }
		public void Fetch (string device_uid, string version, OnCompleteDelegate callback )
		{
			OnComplete = callback;
			string url = Controller.Instance.baseURL + "GetDeviceConfiguration?device_uid='" + device_uid+"'&serial_num=" + HatchFramework.PlatformUtils.GetSerial() + "&version=" + version;
			DebugConsole.Log (url);
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		public override string ToString ()
		{
			return string.Format ("[GetDeviceConfiguration]\n" +
				"UID: " +device_uid+"\n"+
				"debug: " +debug+"\n"+
				"SyncEnabled: " +SyncEnabled+"\n"+
				"ShowDebug: " +ShowDebugConsole+"\n"+
				"");
			
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			try {
				
				bool success = false;
				using(var reader = new StreamReader(new MemoryStream(response, false))) {
					
					string resp = reader.ReadToEnd();
					Debug.Log (resp);
					if(resp.Contains("error")){
						success = false;
						DebugConsole.LogError("Failed to get configuration :"+resp);
					}else{
						fetched = JsonMapper.ToObject<List<GetDeviceConfiguration>> (resp)[0];
						success = true;
					}
					
				}
				status = success;
				
			} catch (Exception e){
				status = false;
				Debug.LogError(e.Message);
			}
			return base.OnFetchComplete(response);
		}
	}
}



