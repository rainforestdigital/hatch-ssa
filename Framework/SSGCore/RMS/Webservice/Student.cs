using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;

namespace SSGCore.RMS.Webservice
{
	public class Student : RequestBase
	{
		public int id;
		public string custom_id = "";
		public string first_name = "";
		public string last_name = "";
		public string birth_date = "";
		public string picture = "";
		public string language = "1";
		public int need_sync;
		public int classroom_id;
		public int uid;
		public int organization_id;
		public int school_id;
		public string bookmark_session_date;
		public JsonData bookmark_session; //(<type>)(session_bookmark["key"])
		public static List<Student> fetched;
		
		public void Fetch (string device_uid, OnCompleteDelegate callback){ Fetch(device_uid, "5.6.1.0+", callback); }
		public void Fetch (string device_uid, string version, OnCompleteDelegate callback)
		{
			OnComplete = callback;
			string url = Webservice.Controller.Instance.baseURL + "studentsSSA?device_uid=" + device_uid + "&bookmarks=1"+"'&serial_num=" + HatchFramework.PlatformUtils.GetSerial() + "&version=" + version;
			Webservice.Controller.Instance.StartCoroutine (Webservice.Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
//		public void Fetch ( string classroom_id, string version, OnCompleteDelegate callback)
//		{
//			OnComplete = callback;
//			string url = Webservice.Controller.Instance.baseURL + "studentsSSA?device_uid=" + SSGEngine.databaseObject.GetDeviceUid() + "&id=" + classroom_id + "&bookmarks=1"+"'&serial=" + HatchFramework.PlatformUtils.GetSerial() + "&version=" + version;
//			Webservice.Controller.Instance.StartCoroutine (Webservice.Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
//		}
			
		protected override bool OnFetchComplete (byte[] response)
		{
			string strResp = System.Text.Encoding.UTF8.GetString(response);
			strResp = strResp.Replace( "\"language\":null,", "\"language\":\"1\"," );
			Webservice.Controller.Instance.Log("Student sync response: " + strResp );
			try {
				fetched = JsonMapper.ToObject<List<Student>> (strResp);
				
				Webservice.Controller.Instance.Log ("Students fetched: ");
				foreach (Student c in fetched) {
					Webservice.Controller.Instance.Log (c.id.ToString ());
				}
				status = true;
			} catch {
				status = false;
			}
			return base.OnFetchComplete (response);
		}
	}
}

