using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

namespace SSGCore.RMS.Webservice
{
	public class SaveStudentBookmark : RequestBase
	{
		public void Save( string device_uid, string bookmark_session, OnCompleteDelegate callback )
		{
			OnComplete = callback;
			
			string url = Webservice.Controller.Instance.baseURL + "SaveStudentBookmark";
			
			WWWForm postData = new WWWForm();
			postData.AddField( "device_uid", device_uid );
			postData.AddField( "bookmark_session", bookmark_session );
			
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed, postData));
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			string pResp = System.Text.Encoding.Default.GetString(response).Normalize();
			
			JsonData data = JsonMapper.ToObject( pResp );
			
//			try{DebugConsole.LogWarning( "SaveStudentBookmark return value: " + data.ToJson() );}catch{ DebugConsole.LogError(""); }
			
			/*Webservice.Controller.Instance.Log*/ Debug.Log("SaveStudentBookmark: ");
			if ((bool)data["success"]) {
				/*Webservice.Controller.Instance.Log*/ Debug.Log("SaveStudentBookmark success");
				status = true;
			} else {
				/*Webservice.Controller.Instance.Log*/ Debug.LogError("SaveStudentBookmark error: " + (string)data["message"]);
				status = false;
			}
			
			return base.OnFetchComplete (response);
		}
	}
}

