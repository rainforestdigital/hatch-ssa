using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using HatchFramework;

namespace SSGCore.RMS.Webservice
{
	public class Controller : MonoBehaviour
	{
		
		public string baseURL = GlobalConfig.GetProperty<string>("RMS_URL") + "WebService/";
		//public string baseURL = "http://localhost/~ziba/www/html/?r=WebService/";
		private bool webserviceIsUsingPrettyURLs = true;
		
		public enum States
		{
			READY,
			FETCHING,
			ERROR
		};
		public States state = States.READY;
		
		public bool lastStatus = false;
		
		#region Auto Instantiation
		protected static Controller instance;
		
		public static Controller Instance {
			get {
				if (instance == null) {
					GameObject webserviceInstance = new GameObject ("RMS Webservice");
					instance = webserviceInstance.AddComponent<Controller> ();
				}
				return instance;
			}
		}
		#endregion
	

		#region Fetches
		
		public delegate bool FetchComplete (byte[] response);
		public delegate void FetchFailed (string error);
		
		
		
		public IEnumerator FetchRoutine (string url, FetchComplete callback, FetchFailed failed)
		{
			return FetchRoutine(url, callback, failed, null);
		}
		
		public IEnumerator FetchRoutine (string url, FetchComplete callback, FetchFailed failed, WWWForm form)
		{	
			return FetchRoutine(url, callback, failed, form, null);
		}
		
		public IEnumerator RawPostRoutine(string url, FetchComplete callback, FetchFailed failed, byte[] data){
			return FetchRoutine(url, callback, failed, null, data);
		}
		
		public IEnumerator FetchRoutine (string url, FetchComplete callback, FetchFailed failed, WWWForm form, byte[] data)
		{
			if(!webserviceIsUsingPrettyURLs){
				url = url.Replace("?device_uid","&device_uid");	
			}
			state = States.FETCHING;
			Log ("Fetching: " + url);
			WWW www;
			if(data != null){
				 www = new WWW (url, data);
			}
			else if(form != null){
				 www = new WWW (url, form);
			} else {
				 www = new WWW (url);
			}
			yield return www;

			if (!String.IsNullOrEmpty (www.error)) {
				state = States.ERROR;
				DebugConsole.Log ("response text: "+www.text);
				DebugConsole.LogError("Webservice error: "+www.error);
				failed(www.error);
			} else {
				lastStatus = callback (www.bytes);
				state = States.READY;
			}
		}
	
		#endregion	
		
		
		#region Debugging Properties
		private bool debugOn = false;
		public List<string> log = new List<string> ();
		#endregion
		
		
		
		public void Log (string msg)
		{
			if (debugOn) {
				log.Add (msg);
				DebugConsole.Log(msg);
			}
		}
		
	}
}

