using System;
using UnityEngine;

namespace SSGCore.RMS.Webservice
{
	public class RequestBase
	{
		
		public delegate void OnCompleteDelegate (bool status);
		
		public OnCompleteDelegate OnComplete;
		protected bool status = false;

		public static byte[] StrToByteArray (string str)
		{
			System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding ();
			return encoding.GetBytes (str);
		}
		
		protected virtual bool OnFetchComplete (byte[] response)
		{
			if (OnComplete != null) {
				OnComplete (status);	
			} else {
				Controller.Instance.Log ("No callback for fetch complete"+this);
			}
			return status;
		}
		
		protected virtual void OnFetchFailed (string error)
		{
			DebugConsole.LogError("Failed to fetch with error: "+error);
			status = false;
			
			Controller.Instance.Log (error);
			if (OnComplete != null) {
				OnComplete (false);	
			} else {
				Controller.Instance.Log ("No callback for fetch failed");
			}
		}
		
	}
}

