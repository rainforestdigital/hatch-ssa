using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

namespace SSGCore.RMS.Webservice
{
	public class Downloads : RequestBase
	{

		
		public void Fetch (string device_uid, string current_binary_version, string current_content_version, string new_binary_version, string new_content_version, OnCompleteDelegate callback)
		{
			OnComplete = callback;
			string url = Webservice.Controller.Instance.baseURL + "downloads?device_uid=" + device_uid;
			WWWForm form = new WWWForm ();
			if (current_binary_version != "") {
				form.AddField ("current_bin_ver", current_binary_version);
			}
			if (current_content_version != "") {
				form.AddField ("current_content_ver", current_content_version);
			}
			form.AddField ("new_bin_ver", new_binary_version);
			form.AddField ("new_content_ver", new_content_version);
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed, form));
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			if (response.Length == 0) {
				// Success has no results
				Webservice.Controller.Instance.Log ("Downloads silent success");
				status = true;
			} else {
				status = false;
			}
			return base.OnFetchComplete(response);
		}
	}
}

