using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;

namespace SSGCore.RMS.Webservice
{
	public class ImageTimestamp : RequestBase
	{
		public string url;
		public string timestamp;
		public static List<ImageTimestamp> fetched;
		
		public void Fetch (string device_uid, string[] imageURLs, OnCompleteDelegate callback)
		{
			OnComplete = callback;
			string url = Webservice.Controller.Instance.baseURL + "imagetimestamps?device_uid=" + device_uid;
			WWWForm form = new WWWForm ();
			foreach (string s in imageURLs) {
				string tmp = s;
				if (s [0] != '/') {
					tmp = "/" + s;	
				}
				form.AddField ("urls[]", tmp);
			}
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed, form));

		}
		
		
		protected override bool OnFetchComplete (byte[] response)
		{
			
			Webservice.Controller.Instance.Log ("ImageTimestamps fetched: ");
			if (response.Length == 0) {
				Webservice.Controller.Instance.Log ("No results");
				fetched = new List<ImageTimestamp> ();
				status = false;
			} else {
				try {
					using(var reader = new StreamReader(new MemoryStream(response, false))) {
						fetched = JsonMapper.ToObject<List<ImageTimestamp>> (reader);
					}
					foreach (ImageTimestamp c in fetched) {
						Webservice.Controller.Instance.Log (c.url.ToString ());
					}
					status = true;
				} catch (Exception ex) {
					Debug.LogException(ex);
					status = false;
				}
			}
			return base.OnFetchComplete(response);

		}
	}
}

