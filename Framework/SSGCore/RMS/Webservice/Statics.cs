using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;

namespace SSGCore.RMS.Webservice
{
	public class Statics : RequestBase
	{
		public int id;
		public int device_id;
		public string device_uid;
		public int organization_id;
		public int school_id;
		public string rms_url;
		public string device_alias;
		public List<Family> families;
		public List<Level> levels;
		//public List<Weight> weights;
		
		public class Family
		{
			public string id;
			public string name;
		}
		
		public class Level
		{
			public string id;
			public string code;
			public string name;
		}
		/*
		public class Weight
		{
			public string id;
			public string skillID;
			public string skillLevel;
			public string weight;
		}
		*/
		
		public static Statics fetched;
		
		public void Fetch (string device_uid, OnCompleteDelegate callback )
		{
			OnComplete = callback;
			string url = Controller.Instance.baseURL + "statics?device_uid=" + device_uid;
			Debug.Log("static fetch, callback: "+callback);
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			try {
				using(var reader = new StreamReader(new MemoryStream(response, false))) {
					fetched = JsonMapper.ToObject<Statics> (reader);
				}
				Controller.Instance.Log ("Statics fetched: ");
				Controller.Instance.Log (fetched.families [0].name);
				status = true;
			} catch {
				status = false;
			}
			return base.OnFetchComplete(response);
		}
	}
}

