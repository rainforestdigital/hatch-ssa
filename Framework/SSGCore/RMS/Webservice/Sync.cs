using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;
using System.Text;

namespace SSGCore.RMS.Webservice
{
	public class Sync : RequestBase
	{
		
		public static Dictionary<string, string> newStudentIDs;
				
		public void Save (string device_uid, List<UserDB> students, List<SessionDB> sessions, List<LockDB> locks, string content_version, string binary_version, OnCompleteDelegate callback)
		{
			this.OnComplete = callback;
			newStudentIDs = new Dictionary<string, string>();
			if(students.Count == 0 && locks.Count == 0 && sessions.Count == 0){
				callback(true);
				return;
			}
			
			string url = Webservice.Controller.Instance.baseURL + "sync?device_uid=" + device_uid + "&application=SSA&version=" + HatchFramework.GlobalConfig.GetProperty("version");
			url += "&serial=" + HatchFramework.PlatformUtils.GetSerial();
			WWWForm form = new WWWForm ();
			
			int i = 0;
			foreach (UserDB s in students) {
				
				DebugConsole.Log("Adding new student to RMS: "+s.UID);
				if (s.UID != "0" && !string.IsNullOrEmpty(s.UID)) {
					form.AddField ("students[" + i + "][uid]", s.UID);	
				}				
				if (s.ID != 0) {
					form.AddField ("students[" + i + "][id]", s.ID);	
				}
				form.AddField ("students[" + i + "][first_name]", s.FirstName);	
				form.AddField ("students[" + i + "][last_name]", s.LastName);	
				form.AddField ("students[" + i + "][birth_date]", s.BirthDate);	
			
				if (s.ClassroomID != 0) {
					form.AddField ("students[" + i + "][classroom_id]", s.ClassroomID);	
				}
				
//				if (s. != 0) {
//					form.AddField ("students[" + i + "][organization_id]", s.organization_id);	
//				}
				
//				if (s.school_id != 0) {
//					form.AddField ("students[" + i + "][school_id]", s.school_id);	
//				}
					
				if (s.Picture != null) {
					string path = ImageUtility.PathForFilename(s.Picture);
					string encodedImage = ImageUtility.FilenameToBase64Encoded(path);
					if(!String.IsNullOrEmpty(encodedImage)) {
						form.AddField ("students[" + i + "][picture_data]", encodedImage);
					}
				}
						
				if (s.CustomID != null) {
					form.AddField ("students[" + i + "][custom_id]", s.CustomID);	
				}

				i++;
			}
			
			i = 0;
			foreach (SessionDB s in sessions) {
				form.AddField ("sessions[" + i + "][user_id]", s.UserID);
				if (s.UID != "0" && !string.IsNullOrEmpty(s.UID)) {
					form.AddField ("sessions[" + i + "][uid]", s.UID);
				}
				form.AddField ("sessions[" + i + "][skill_id]", s.SkillID);	
				form.AddField ("sessions[" + i + "][skill_level_id]", s.SkillLevelID);	
		
			
		
				if (s.Started != null && s.Started != "") {
					form.AddField ("sessions[" + i + "][started]", s.Started);	
				}
			
				if (s.Duration != 0) {
					form.AddField ("sessions[" + i + "][duration]", s.Duration);	
				}
			
				form.AddField ("sessions[" + i + "][percent]", s.Percent.ToString());	
			
				if (s.Opportunities != null && s.Opportunities != "") {
					form.AddField ("sessions[" + i + "][opportunities]", s.Opportunities);	
				}	
				
				if ( !string.IsNullOrEmpty( s.serialized ) ) {
					form.AddField ("sessions[" + i + "][serialization]", s.serialized);
				}
				
				i++;
			}
			
			i = 0;
			foreach(LockDB l in locks){
				
				if(!string.IsNullOrEmpty(l.UID) && l.UID != "0"){
					form.AddField("locks["+i+"][uid]", l.UID);
				}
				if(l.UserID != 0){
					form.AddField("locks["+i+"][user_id]", l.UserID);
				}
				form.AddField("locks["+i+"][skill_id]", l.SkillID);
				form.AddField("locks["+i+"][is_enabled]", l.Enabled);
				form.AddField("locks["+i+"][created]", l.Created);
				i++;
			}
			
			
			if(content_version != ""){
				form.AddField("content_version", content_version);
			}	
			
			if(binary_version != ""){
				form.AddField("binary_version", binary_version);	
			}
			
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed, form));

		}
			
		protected override bool OnFetchComplete (byte[] response)
		{
			DebugConsole.Log ("Fetch complete: {0}", response);
			
			if(response.Length == 2 && response[0] == (byte)'[' && response[1] == (int)']') {
				status = true;
			} else {
				try {
					using(var reader = new StreamReader(new MemoryStream(response, false))) {
						newStudentIDs = JsonMapper.ToObject<Dictionary<string,string>> (reader);
					}
				
					DebugConsole.Log ("Sync sent. New Students: "+newStudentIDs.Count);
					foreach(KeyValuePair<string,string> kvp in newStudentIDs){
						Webservice.Controller.Instance.Log (kvp.Key+" "+kvp.Value);
					}
					status = true;
				} catch (Exception e) {
					Debug.LogException(e);
					DebugConsole.LogError("Invalid Sync Response: {0}", new UTF8Encoding().GetString(response));
					status = false;
				}
			}
			return base.OnFetchComplete(response);
		}
		
		
	}
}

