using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;
namespace SSGCore.RMS.Webservice
{
	public class Classroom : RequestBase
	{
		public int id;
		public string name;
		public string user_defined_id;
		public int teacher_id;
		public string classroom_pic;
		public string teacher_pic;
		public string themes;
		
		
		public static List<Classroom> fetched;
		
		public void Fetch (string device_uid, OnCompleteDelegate callback ){ Fetch(device_uid, "5.6.1.0+", callback); }
		public void Fetch (string device_uid, string version, OnCompleteDelegate callback ){
			OnComplete = callback;
			string url = Webservice.Controller.Instance.baseURL + "classrooms?device_uid=" + device_uid + "&version=" + version;
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			try {
				using(var reader = new StreamReader(new MemoryStream(response, false))) {
					fetched = JsonMapper.ToObject<List<Classroom>> (reader);
				}
				
				Webservice.Controller.Instance.Log ("Classrooms fetched: ");
				foreach (Classroom c in fetched) {
					Webservice.Controller.Instance.Log (c.id.ToString ());
				}
				status = true;
			} catch (Exception e) {
				Debug.LogException(e);
				status = false;
			}
			return base.OnFetchComplete(response);
		}
	}
}

