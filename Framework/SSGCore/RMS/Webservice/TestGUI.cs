using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using HatchFramework;

namespace SSGCore.RMS.Webservice
{
	public class TestGUI : MonoBehaviour
	{
	
		

		public Vector2 logScrollPosition;
		public Vector2 buttonsScrollPosition;
		string device_uid = "qaaa-aaaa-aaaa-aaa1";
		
			
		#region Auto Instantiation
		protected static TestGUI instance;
		
		public static TestGUI Instance {
			get {
				if (instance == null) {
					GameObject webserviceInstance = new GameObject ("RMS Webservice TestGUI");
					instance = webserviceInstance.AddComponent<TestGUI> ();
				}
				return instance;
			}
		}
		#endregion
		
		private void OnRegFetchReturn(bool val, Dictionary<string, string> deviceSettings)
		{
			DebugConsole.Log("FETCH FOR REG RETURNED: " + val + ", deviceSettings: " + deviceSettings.ToString());
		}
		
		public void GUI ()
		{
		
			GUILayout.BeginVertical ("box");
			GUILayout.Label ("Webservice Tests");
			GUILayout.Label ("State: " + Controller.Instance.state);
			GUILayout.BeginHorizontal ();
			GUILayout.Label ("Device UID:");
			device_uid = GUILayout.TextField (device_uid);			
			
			GUILayout.Label ("Last Call Status: " + Controller.Instance.lastStatus);

			GUILayout.EndHorizontal ();
			
			buttonsScrollPosition = GUILayout.BeginScrollView (buttonsScrollPosition, GUILayout.Width (500), GUILayout.Height (200));
			
			
			if (GUILayout.Button ("Fetch Downloads (saves version info)")) {
				(new Downloads()).Fetch (device_uid, "2.0.33.8", "51", "3", "33", null);
			}
			if (GUILayout.Button ("Register Device: TMPLICENSE")) {
				(new RegisterDevice()).Fetch(GlobalConfig.GetProperty("TEST_LICENSE"), SystemInfo.deviceUniqueIdentifier, 11, OnRegFetchReturn);
			}
			
			if (GUILayout.Button ("Check Serial 1")) {
				(new CheckSerialNum()).Fetch ("1", null);
			}	
			if (GUILayout.Button ("Check Serial 12341234134")) {
				(new CheckSerialNum()).Fetch ("12341234134", null);
			}	
			if (GUILayout.Button ("Reset Levels")) {
				(new ResetLevels()).Reset (null);
			}	
			if (GUILayout.Button ("Save Agreement")) {
				(new SaveAgreement()).Save (device_uid, "Fred Fredrickson", DateTime.Now, null);
			}	
			if (GUILayout.Button ("Convert States")) {
				(new ConvertStates()).Convert (null);
			}	
			if (GUILayout.Button ("Fetch Statics")) {
				(new Statics()).Fetch (device_uid, null);
			}	
			if (GUILayout.Button ("Fetch Updates")) {
				(new Updates ()).Fetch (device_uid, null);
			}
			if (GUILayout.Button ("Fetch Updates AIO")) {
				(new Updates ()).FetchAIO (device_uid, null);
			}
			
			if (GUILayout.Button ("Fetch SSGActivities")) {
				(new GetAllSSGActivities ()).Fetch (device_uid, null);
			}
			
			if (GUILayout.Button ("Fetch Classrooms")) {
				(new Classroom ()).Fetch (device_uid, null);
			}
				
			if (GUILayout.Button ("Fetch Settings")) {
				(new Settings ()).Fetch (device_uid, "1", "1", "", null);
			}
			
			if (GUILayout.Button ("Fetch Students")) {
				(new Student ()).Fetch (device_uid, null);

			}
			
			if (GUILayout.Button ("Fetch Admins")) {
				(new Admin()).Fetch (device_uid, null);
			}
			
			if (GUILayout.Button ("Fetch Refocus")) {
				(new Refocus()).Fetch (device_uid, null);
			}
			
			if (GUILayout.Button ("Fetch Locks")) {
				(new Lock()).Fetch (device_uid, null);
			}
			
			if (GUILayout.Button ("Fetch Sessions")) {
				(new Session ()).Fetch (device_uid, null);

			}
				
			if (GUILayout.Button ("Fetch \"Encode\"")) {
				(new Encode()).Fetch (device_uid, "images/photo.png", null);
			}
					
			if (GUILayout.Button ("Fetch MultiEncode")) {
				(new MultiEncode()).Fetch (device_uid, new string[]{"images/picture.jpg","images/photo.png"}, onFetchImage);
			}			
			
			if (GUILayout.Button ("Fetch ImageTimestamps")) {
				(new ImageTimestamp()).Fetch (device_uid, new string[]{"images/photo.png","images/photo.png"}, null);
			}
					
			//if (GUILayout.Button ("Fetch Images and Timestamps By User IDs")) {
			//	(new ImageTimestamp()).Fetch (device_uid, new string[]{"56495","56501"}, null);
			//}
						
			if (GUILayout.Button ("SaveLock")) {
				(new SaveLock()).Fetch (device_uid, "222", "4", 0, null);
			}
							
			if (GUILayout.Button ("Create Child")) {
				(new Save()).CreateChild (device_uid, "4", "Jose"+(UnityEngine.Random.Range(0,999999)).ToString(), "Rambutan", "captain-pancake", "1980-12-01", "2", "", "", "", null);
			}
			
			if (GUILayout.Button ("Update Child")) {
				(new Save()).UpdateChild (device_uid, "56495", "Zooba", "Scoot", "captain-pancake-" + UnityEngine.Random.Range (0, 100).ToString (), "1977-12-05", "2", "", null);
			}
			
			if (GUILayout.Button ("Update Child with Picture")) {
				string image_file = UnityEngine.Application.dataPath + "/ziba.jpg";

				(new Save()).UpdateChild (device_uid, "56495", "Ziba", "Scoot", "captain-pancake-" + UnityEngine.Random.Range (0, 100).ToString (), "1977-12-05", "2", image_file, null);
			}
			
			if (GUILayout.Button ("Save Session")) {
				(new SaveSession()).Save (device_uid, "56495", 1, 1, DateTime.UtcNow.ToString ("yyyy-MM-dd HH:mm:ss"), 25, 0.5f, "1,2", null);
			}
			
			if (GUILayout.Button ("Register Device")) {
				(new RegisterDevice()).Fetch("xxxx-xxxx-xxxx-xxxx", "xxxx-xxxx-xxxx-xxxx", 1, null);
			}
			
			if (GUILayout.Button ("IsDeviceRegistered")) {
				(new IsDeviceRegistered()).Fetch("qaaa-aaaa-aaaa-aaa1", null);
			}
			
			if (GUILayout.Button ("GetDeviceConfiguration")) {
				(new GetDeviceConfiguration()).Fetch("qaaa-aaaa-aaaa-aaa1", null);
			}
				
			if (GUILayout.Button ("Sync (Save Students, Sessions, Locks, Content Version, Binary Version)")) {
				List<Webservice.Student> students = new List<Webservice.Student> ();
				Webservice.Student s = new Webservice.Student ();
				s.birth_date = "1992-12-01";
				s.classroom_id = 2;
				s.first_name = "Tammy2";
				//s.id = 56495;
				s.uid = 7;
				s.last_name = "DropTables";
				//s.picture = UnityEngine.Application.dataPath + "/ziba.jpg";
				students.Add (s);
				
				s = new Webservice.Student ();
				s.birth_date = "1993-12-01";
				s.classroom_id = 2;
				s.first_name = "Toomy2";
				//s.id = 56495;
				s.uid = 8;

				s.last_name = "DropTables2";
				s.picture = ImageUtility.FilenameToBase64Encoded (UnityEngine.Application.dataPath + "/ziba.jpg");
				students.Add (s);
				
				
				List<Webservice.Session> sessions = new List<Webservice.Session> ();
				Webservice.Session se = new Webservice.Session ();
				se.user_id = 56495;
				se.skill_id = 1;
				se.skill_level_id = 1;
				se.started = DateTime.UtcNow.ToString ("yyyy-MM-dd HH:mm:ss");
				se.duration = 25;
				se.percent = 0.75f;
				se.opportunities = "1,2";
				sessions.Add (se);
				
				List<Webservice.Lock> locks = new List<Webservice.Lock> ();
				Webservice.Lock l = new Webservice.Lock ();
				l.user_id = "222";
				l.skill_id = "4";
				l.is_enabled = "1";
				locks.Add (l);
				
				//(new Sync()).Save (device_uid, students, sessions, locks, "3.0", "52", null);
			}
			GUILayout.EndScrollView ();
			
			string logString = "";
			foreach (string msg in Controller.Instance.log) {
				logString = msg + "\n" + logString;
			}
			
			logScrollPosition = GUILayout.BeginScrollView (logScrollPosition, GUILayout.Width (300), GUILayout.Height (200));
			GUILayout.TextArea (logString);
			GUILayout.EndScrollView ();
			GUILayout.EndVertical ();
			
		}
		
		
		void onFetchImage(bool success){
		
			Debug.Log ("GOT THE IMAGE");
		}
		
	}
}

