using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.Text;
using System.IO;

namespace SSGCore.RMS.Webservice
{
	public class ManualRegisterDevice : RequestBase
	{
		public delegate void OnRegistrationCompleteDelegate (bool status, Dictionary<string, string> deviceSettings);
		
		public OnRegistrationCompleteDelegate OnRegistrationComplete;
		
		public void Fetch (string orderNumber, string serial_num, int model, OnRegistrationCompleteDelegate callback)
		{			
			OnRegistrationComplete = callback;
			
			string url  = Webservice.Controller.Instance.baseURL + "ManualRegisterDevicev2?SalesOrderNumber=" + orderNumber + "&DeviceSerialNumber=" + serial_num + "&DeviceTypeID=" + model;
			Debug.Log(url);
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			/*
			{"org_name":"Hatch","school_name":"Big Dog Day Care","class_name":"Ms Terri's","device_code":"AASC-2X99-38YR-VIH8","serial_number":"'BB541260-5C13-5679-8326-5D2120596FA3'"}
			*/
			
			Dictionary<string, string> deviceSettings = null;
			
			try {
				using(var reader = new StreamReader(new MemoryStream(response, false))) {
					deviceSettings = JsonMapper.ToObject<Dictionary<string,string>> (reader);
				}
				
				status = !deviceSettings.ContainsKey("error");
			} catch (Exception e) {
				Debug.LogException(e);
				DebugConsole.LogError("Invalid Sync Response: {0}", new UTF8Encoding().GetString(response));
				status = false;
			}
			
			OnRegistrationComplete(status, deviceSettings);
			return base.OnFetchComplete(response);
		}
		
		protected override void OnFetchFailed (string error)
		{
			base.OnFetchFailed(error);
			OnRegistrationComplete(false, null);
		}
	}
}

