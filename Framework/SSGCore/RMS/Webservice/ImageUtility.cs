using System;
using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using HatchFramework;

namespace SSGCore
{
	public class ImageUtility
	{
		
			
		public static string FilenameToBase64Encoded (string filename)
		{
			if (!File.Exists (filename)) {
				Debug.Log("Missing file: "+filename);
				return ""; 
			}
			byte[] bytes = File.ReadAllBytes(filename);
		
			string encoded = Convert.ToBase64String(bytes);
		
			return GetDataURI(filename)+encoded;
		}
		
		public static bool CheckFileOutdated(string filename, DateTime timeToCompare){
			//if (PlatformUtils.GetPlatform() == Platforms.IOS || PlatformUtils.GetPlatform() == Platforms.IOSRETINA) {
			//	return true;
			//} else {
				DateTime dt = File.GetLastWriteTime (PathForFilename (filename));
				return dt < timeToCompare;	
			//}
		}
		
		public static bool Base64EncodedPngToFile(string filename, string picture_data, string timestamp){
		
			
			DateTime dt = DateTime.Parse(timestamp);
			
			if(!CheckFileOutdated(filename, dt))
				return false;
			
			DebugConsole.Log ("Updating file: "+filename);
			
			string path = filename.Substring(0, filename.LastIndexOf('/'));
			if(!Directory.Exists(path))
				Directory.CreateDirectory(path);
			
		
			byte[] bytes = Convert.FromBase64String(picture_data.Split (',')[1]);
			
			FileStream fs = File.Open(filename, FileMode.Create);
			
			fs.Write(bytes, 0, bytes.Length);
			fs.Close();
			
			File.SetLastWriteTime(filename, dt);
			
			return true;
			
			
		}
		
		public static bool TextureToFile(string filename, Texture2D texture)
		{
			DebugConsole.Log ("Updating file: {0}", filename);
			
			string path = Path.GetDirectoryName(filename);
			if(!Directory.Exists(path))
				Directory.CreateDirectory(path);
			
		
			byte[] bytes = texture.EncodeToPNG();
			File.WriteAllBytes(filename, bytes);
			
			UnityEngine.Object.Destroy(texture);
			
			return true;
		}
		
		  public static string GetDataURI(string imgFile)
        {
			//TODO: test if jpg vs jpeg is a problem
            return "data:image/"+ Path.GetExtension(imgFile).Replace(".","") + ";base64,";
        }
		
		public static string PathForFilename(string filename) 
		{
			if(filename.StartsWith("/")) {
				filename = filename.Substring(1);
			}
			
			string folder = Path.Combine(PlatformUtils.InternalDataPath, GlobalConfig.GetProperty<string>("LOCAL_DIR"));
			return Path.Combine(folder, filename);
		}

		
	}
}

