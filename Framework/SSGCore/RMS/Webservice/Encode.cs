using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;

/*
 * This one is a little odd.
 * It takes a remote image url and returns that same url and a timestamp.
 * It does not encode.
 * image url requires leading /
 * 
 */
namespace SSGCore.RMS.Webservice
{
	public class Encode : RequestBase
	{
		public string url;
		public string timestamp;
		public static Encode fetched = new Encode();
		
		public void Fetch (string device_uid, string image_url, OnCompleteDelegate callback)
		{
			OnComplete = callback;
			if(image_url[0] != '/'){
				image_url = "/"+image_url;	
			}
			string url = Webservice.Controller.Instance.baseURL + "encode?device_uid=" + device_uid + "&image_url=" + image_url;
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			
			Webservice.Controller.Instance.Log ("Encode fetched: ");
			if (response.Length == 0) {
				Webservice.Controller.Instance.Log ("No results");
				status = false;
			} else {
				using(var reader = new StreamReader(new MemoryStream(response, false))) {
					fetched = JsonMapper.ToObject<Encode> (reader);
				}
				Webservice.Controller.Instance.Log (fetched.timestamp.ToString ());
				status = true;
			}
			return base.OnFetchComplete(response);

		}
	}
}

