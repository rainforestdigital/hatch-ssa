using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;
using HatchFramework;

namespace SSGCore.RMS.Webservice
{
	public class MultiEncode : RequestBase
	{
		public string url;
		public string timestamp;
		public string picture_data;
		public static List<MultiEncode> fetched;
		
		public void Fetch (string device_uid, string[] imageURLs, OnCompleteDelegate callback)
		{
			OnComplete = callback;
			string url = Webservice.Controller.Instance.baseURL + "multiencode?device_uid=" + device_uid;
			WWWForm form = new WWWForm ();
			foreach (string s in imageURLs) {
				string tmp = s;
				if (s [0] != '/') {
					tmp = "/" + s;	
				}
				form.AddField ("urls[]", tmp);
			}
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed, form));

		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			
			Webservice.Controller.Instance.Log ("MultiEncode fetched: ");
			if (response.Length == 0) {
				Webservice.Controller.Instance.Log ("No results");
				fetched = new List<MultiEncode> ();
				status = false;
			} else {
				try {
					using(var reader = new StreamReader(new MemoryStream(response, false))) {
						fetched = JsonMapper.ToObject<List<MultiEncode>> (reader);
					}
					foreach (MultiEncode c in fetched) {
						
						
						Debug.Log(PlatformUtils.InternalDataPath);
						Debug.Log ("URL: "+c.url+" "+c.timestamp);
						string path = Path.Combine(PlatformUtils.InternalDataPath, GlobalConfig.GetProperty<string>("LOCAL_DIR"));
						ImageUtility.Base64EncodedPngToFile(path+c.url, c.picture_data, c.timestamp);
						//Webservice.ImageUtility.FilenameToBase64Encoded
						//c.picture_data
						Webservice.Controller.Instance.Log (c.url.ToString ());
					}
					status = true;
				} catch (Exception e) {
					Debug.LogException(e);
					status = false;
				}
			}
			return base.OnFetchComplete(response);

		}
	}
}

