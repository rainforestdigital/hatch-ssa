using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.Text;
using System.Xml;

namespace SSGCore.RMS.Webservice
{
	public class GetDeviceLicense : RequestBase
	{
		public delegate void OnGetLicenseCompleteDelegate (bool status, DateTime expirationDate, Dictionary<string,string> info);
		
		public OnGetLicenseCompleteDelegate OnGetLicenseComplete;
		
		public static bool valid = false;
		
		public void Fetch( string serial_num, OnGetLicenseCompleteDelegate callback)
		{
			OnGetLicenseComplete = callback;
			
			string url  = Webservice.Controller.Instance.baseURL + "GetDeviceLicense?hatch_asset_id=" + serial_num;
			DebugConsole.Log ("VERIFYING LICENSE: " + url);
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			var stringResponse = new UTF8Encoding().GetString(response);
			Debug.Log("ON FETCH COMPLETE FOR LICENSE: " + stringResponse);
			DateTime dateOut = DateTime.Now;
			Dictionary< string, string > information = null;
			if(!stringResponse.ToLower().Contains("error")) 
			{
				DebugConsole.Log("No Error Returned");
				valid = true;
				
				XmlDocument xmlResponse = new XmlDocument();
				try{ xmlResponse.LoadXml( stringResponse ); }
				catch{
					DebugConsole.LogError("Invalid XML Structure returned");
					valid = false;
				}
				
				if( valid )
				{
					information = new Dictionary<string, string>();
					XmlNode devNode = xmlResponse["deviceinfo"]["device"];
					information["name"] = devNode["name"].InnerText;
					information["unique_id"] = devNode["unique_id"].InnerText;
					information["sale"] = devNode["sales_num"].InnerText;
					information["orginization"] = devNode["organization_name"].InnerText;
					
					XmlNodeList list = xmlResponse["deviceinfo"]["deviceLicenses"].GetElementsByTagName("deviceLicense");
					string expireDate = "";
					foreach( XmlNode n in list )
					{
						information["license"] = n["license_key"].InnerText;
						if( n["application_name"].InnerText == "Shell Squad Adventures" )
						{
							expireDate = n["license_expiration_date"].InnerText;
							break;
						}
					}
					
					if( string.IsNullOrEmpty( expireDate ) )
					{
						DebugConsole.LogError("No License found for Shell Squad Adventures");
						dateOut = DateTime.MinValue;
						information["dateString"] = dateOut.ToShortDateString();
						valid = false;
					}
					else if( !DateTime.TryParseExact( expireDate, "yyyy'-'MM'-'dd' 'HH':'mm':'ss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dateOut ) )
					{
						DebugConsole.LogError("Invalid expiration date structure returned");
						valid = false;
					}
					else
					{
						DebugConsole.Log( "License expiration date returned: " + dateOut.ToShortDateString() );
						information["dateString"] = dateOut.ToShortDateString();
					}
				}
			}
			else
			{
				DebugConsole.LogError("LicenseError Returned");
				valid = false;			
			}
			
			status = valid;
			if( status )
				OnGetLicenseComplete(status, dateOut, information);
			else
				OnGetLicenseComplete(status, DateTime.UtcNow , null);
			return base.OnFetchComplete(response);
		}
		
		protected override void OnFetchFailed (string error)
		{
			OnGetLicenseComplete(false, DateTime.UtcNow, null);
			base.OnFetchFailed(error);
		}
	}
}
