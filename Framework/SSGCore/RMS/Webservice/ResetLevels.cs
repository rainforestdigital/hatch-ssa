using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

namespace SSGCore.RMS.Webservice
{
	// I couldn't properly test this because it makes the webserver blow the memory limit on my dev machine.
	// But it's a very simple call that returns nothing by design.
	public class ResetLevels : RequestBase
	{
	
		public void Reset (OnCompleteDelegate callback)
		{
			OnComplete = callback;
			string url = Webservice.Controller.Instance.baseURL + "resetlevels";
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			Webservice.Controller.Instance.Log ("ResetLevels: "+response);
			status = true;
			return base.OnFetchComplete(response);

		}
	}
}

