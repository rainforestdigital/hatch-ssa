using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

namespace SSGCore.RMS.Webservice
{
	public class SaveAgreement : RequestBase
	{
		
		public void Save (string device_uid, string username, DateTime date, OnCompleteDelegate callback)
		{
			OnComplete = callback;
			
			string dateString = date.ToString("yyyy-MM-dd");
			string url = Webservice.Controller.Instance.baseURL + "Saveagreement?device_uid=" + device_uid
					+ "&eula_username=" + WWW.EscapeURL(username)
					+ "&eula_date=" + dateString;
			Controller.Instance.StartCoroutine (Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}
		
		
		protected override bool OnFetchComplete (byte[] response)
		{
			Webservice.Controller.Instance.Log ("SaveAgreement: "+response);
			status = true;
			return base.OnFetchComplete(response);
		}
		
		
	}
}

