using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

namespace SSGCore.RMS.Webservice
{
	public class SaveSession : RequestBase
	{
		
		// duration minimum 24 max 2700
		// Started is flexibly converted into time by the server. see php's strtotime function.
		// percent should be 0 to 1
		// opportunities is a comma separated list of numbers
		
		public void Save (string device_uid, string user_id, int skill_id, int skill_level_id, string started, int duration, float percent, string opportunities, OnCompleteDelegate callback)
		{
			
			OnComplete = callback;
			string url = Webservice.Controller.Instance.baseURL + "session&device_uid=" + device_uid;
			System.Text.StringBuilder sb = new System.Text.StringBuilder ();
			JsonWriter writer = new JsonWriter (sb);

			writer.WriteObjectStart ();
			
			writer.WritePropertyName ("device_uid");
			writer.Write (device_uid);

			writer.WritePropertyName ("user_id");
			writer.Write (user_id);
			
			writer.WritePropertyName ("skill_id");
			writer.Write (skill_id);
			
			writer.WritePropertyName ("skill_level_id");
			writer.Write (skill_level_id);
			
		
			if (started != null && started != "") {
				writer.WritePropertyName ("started");
				writer.Write (started);
			}
			

			writer.WritePropertyName ("duration");
			writer.Write (duration);
			
			

			writer.WritePropertyName ("percent");
			writer.Write (percent);

			
			if (opportunities != null && opportunities != "") {
				writer.WritePropertyName ("opportunities");
				writer.Write (opportunities);
			}
			
			//TODO: Add bookmarked=1 if it's from a bookmarked session
			
			writer.WriteObjectEnd();
			
			Controller.Instance.StartCoroutine (Controller.Instance.RawPostRoutine (url, OnFetchComplete, OnFetchFailed, StrToByteArray (sb.ToString ())));	
			
		}
		
		protected override bool OnFetchComplete (byte[] response)
		{
			Webservice.Controller.Instance.Log ("SaveSession: ");
			if (response.Length == 0) {
				// Success has no results
				Webservice.Controller.Instance.Log ("SaveSession silent success");
				status = true;
			} else /*if (response == "400 Lock is not saved")*/ {
				Webservice.Controller.Instance.Log ("SaveSession error");
				status = false;
			} 
			return base.OnFetchComplete(response);
		}
	
	
	
	}
}

