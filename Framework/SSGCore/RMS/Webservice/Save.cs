using System;
using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

namespace SSGCore.RMS.Webservice
{
	// Should be called SaveStudent.  But the webservice just calls it save. So it's named save to maintain parity.
	public class Save : RequestBase
	{	
		public string uid;
		public string first_name;
		public string last_name;
		public string custom_id;
		public string birth_date;
		public string classroom_id;
		public string device_uid;
		public string picture_data;
		public int id;
		
		public static Save fetched;
				
		
		
		// Instead of mapping an object to write as json, we use this writer.  
		// Because the webservice is expecting missing keys, not null
		// values for properties that do not have a value.
		public void CreateChild (string device_uid, string uid, string first_name, string last_name, string custom_id, string birth_date, string classroom_id, string picture_filename, string organization_id, string school_id, OnCompleteDelegate callback)
		{
			CreateChild ( device_uid, uid, first_name, last_name, custom_id, birth_date, classroom_id, picture_filename, 1, callback);
		}
		public void CreateChild (string device_uid, string uid, string first_name, string last_name, string custom_id, string birth_date, string classroom_id, string picture_filename, int profile, OnCompleteDelegate callback)
		{
			OnComplete = callback;
			string url = Webservice.Controller.Instance.baseURL + "saveSSA?device_uid=" + device_uid;

			System.Text.StringBuilder sb = new System.Text.StringBuilder ();
			JsonWriter writer = new JsonWriter (sb);

			writer.WriteObjectStart ();
			
			writer.WritePropertyName ("device_uid");
			writer.Write (device_uid);
			
			writer.WritePropertyName ("uid");
			writer.Write (uid);

			writer.WritePropertyName ("first_name");
			writer.Write (first_name);
			
			writer.WritePropertyName ("last_name");
			writer.Write (last_name);
			
			writer.WritePropertyName ("custom_id");
			writer.Write (custom_id);
			
			writer.WritePropertyName ("birth_date");
			writer.Write (birth_date);
			
			writer.WritePropertyName ("language");
			writer.Write (profile);
			
			if (classroom_id != null && classroom_id != "") {
				writer.WritePropertyName ("classroom_id");
				writer.Write (classroom_id);
			}
//			if (organization_id != null && organization_id != "") {
//				writer.WritePropertyName ("organization_id");
//				writer.Write (organization_id);
//			}
//			if (school_id != null && school_id != "") {
//				writer.WritePropertyName ("school_id");
//				writer.Write (school_id);
//			}
			
			if (picture_filename != null && picture_filename != "") {
				string encoded = ImageUtility.FilenameToBase64Encoded (picture_filename);				
				if(encoded != ""){
					writer.WritePropertyName ("picture_data");
					writer.Write (encoded);
				}
			}
			
			writer.WriteObjectEnd ();
			Controller.Instance.StartCoroutine (Controller.Instance.RawPostRoutine (url, OnFetchComplete, OnFetchFailed, StrToByteArray (sb.ToString ())));	
		}
			
		public void UpdateChild (string device_uid, string id, string first_name, string last_name, string custom_id, string birth_date, string classroom_id, string picture_filename, OnCompleteDelegate callback)
		{
			OnComplete = callback;
			string url = Webservice.Controller.Instance.baseURL + "save&device_uid=" + device_uid;

			System.Text.StringBuilder sb = new System.Text.StringBuilder ();
			JsonWriter writer = new JsonWriter (sb);

			writer.WriteObjectStart ();
			
			writer.WritePropertyName ("device_uid");
			writer.Write (device_uid);

			writer.WritePropertyName ("id");
			writer.Write (id);

			writer.WritePropertyName ("first_name");
			writer.Write (first_name);
			
			writer.WritePropertyName ("last_name");
			writer.Write (last_name);
			
			writer.WritePropertyName ("custom_id");
			writer.Write (custom_id);
			
			writer.WritePropertyName ("birth_date");
			writer.Write (birth_date);
			
			if (classroom_id != null && classroom_id != "") {
				writer.WritePropertyName ("classroom_id");
				writer.Write (classroom_id);
			}
	
			
			if (picture_filename != null && picture_filename != "") {
				string encoded = ImageUtility.FilenameToBase64Encoded (picture_filename);				
				if(encoded != ""){
					writer.WritePropertyName ("picture_data");
					writer.Write (encoded);
				}
			}
			writer.WriteObjectEnd ();
			
			
			Webservice.Controller.Instance.StartCoroutine (Webservice.Controller.Instance.RawPostRoutine (url, OnFetchComplete, OnFetchFailed, StrToByteArray (sb.ToString ())));	
		}
	
		
		protected override bool OnFetchComplete (byte[] response)
		{
			if (response.Length == 0) {
				Webservice.Controller.Instance.Log ("Saved");
				using(var reader = new StreamReader(new MemoryStream(response, false))) {
					fetched = JsonMapper.ToObject<Save> (reader);
				}
				Webservice.Controller.Instance.Log ("uid: "+fetched.uid);
				status = true;
			} else /*if (response == "500 User is not saved")*/ {
				status = false;
			} 
			return base.OnFetchComplete(response);

		}
	}
}

