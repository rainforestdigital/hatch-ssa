using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;

namespace SSGCore.RMS.Webservice
{
	public class Settings : RequestBase
	{
		public int id;
		public int device_id;
		public string device_uid;
		public int organization_id;
		public int school_id;
		public string rms_url;
		public string device_alias;
		public static Settings fetched;
		
		public void Fetch (string device_uid, string binary_version, string content_version, string serial_num, OnCompleteDelegate callback )
		{
			OnComplete = callback;
			string url = Webservice.Controller.Instance.baseURL + "settings?device_uid=" + device_uid + "&version=" + binary_version;
			if(!String.IsNullOrEmpty(content_version)) {
				url += "&content_version=" + content_version;
			}
//			if(!String.IsNullOrEmpty(serial_num)) {
//				url += "&serial_num=" + serial_num;
//			}
			url += "&serial_num=" + HatchFramework.PlatformUtils.GetSerial();
			
			Webservice.Controller.Instance.StartCoroutine (Webservice.Controller.Instance.FetchRoutine (url, OnFetchComplete, OnFetchFailed));
		}

		
		protected override bool OnFetchComplete (byte[] response)
		{
			try {
				using(var reader = new StreamReader(new MemoryStream(response, false))) {
					fetched = JsonMapper.ToObject<Settings> (reader);
				}
				Webservice.Controller.Instance.Log ("Settings fetched: ");
				Webservice.Controller.Instance.Log (fetched.device_id.ToString ());
				status = true;
			} catch {
				status =  false;
			}
			return base.OnFetchComplete(response);
		}
	}
}

