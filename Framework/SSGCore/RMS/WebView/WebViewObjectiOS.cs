using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Callback = System.Action<string>;

namespace SSGCore.RMS
{
	public class WebViewObjectiOS : MonoBehaviour
	{
		Callback callback;
		public BrowserHandleriOS browserHandler;
	IntPtr webView;

	[DllImport("__Internal")]
	private static extern IntPtr _WebViewPlugin_Init(string gameObject);
	[DllImport("__Internal")]
	private static extern int _WebViewPlugin_Destroy(IntPtr instance);
	[DllImport("__Internal")]
	private static extern void _WebViewPlugin_SetMargins(
		IntPtr instance, int left, int top, int right, int bottom);
	[DllImport("__Internal")]
	private static extern void _WebViewPlugin_SetVisibility(
		IntPtr instance, bool visibility);
	[DllImport("__Internal")]
	private static extern void _WebViewPlugin_LoadURL(
		IntPtr instance, string url);
	[DllImport("__Internal")]
	private static extern void _WebViewPlugin_EvaluateJS(
		IntPtr instance, string url);

	public void Init(Callback cb)
	{
		callback = cb;

		webView = _WebViewPlugin_Init(name);

	}

	void OnDestroy()
	{

		if (webView == IntPtr.Zero)
			return;
		_WebViewPlugin_Destroy(webView);
	}

	public void SetMargins(int left, int top, int right, int bottom)
	{
		if (webView == IntPtr.Zero)
			return;
		_WebViewPlugin_SetMargins(webView, left, top, right, bottom);
	}

	public void SetVisibility(bool v)
	{
		if (webView == IntPtr.Zero)
			return;
		_WebViewPlugin_SetVisibility(webView, v);
	}

	public void LoadURL(string url)
	{
			Debug.Log ("LOADING WEBVIEW WITH URL: " + url);
		if (webView == IntPtr.Zero)
			return;
		_WebViewPlugin_LoadURL(webView, url);
	}

	public void EvaluateJS(string js)
	{
		if (webView == IntPtr.Zero)
			return;
		_WebViewPlugin_EvaluateJS(webView, js);
	}

	public void CallFromJS(string message)
	{
		if (callback != null)
			callback(message);
	}
		
		
		
		public void Close (string message)
		{
			browserHandler.Close();
		}
	}
}

