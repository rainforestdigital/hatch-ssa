using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace SSGCore.RMS
{
	public class WebViewObjectWindows : MonoBehaviour
	{
		
		public BrowserHandlerWindows browserHandler;
		
		public bool goFullscreen = false;
	
		
		public WebViewObjectWindows ()
		{
		}
		
		
		public void Update ()
		{
			bool hasExited = true;
			
			try{hasExited = browserHandler.browserProcess.HasExited;} catch{}
			
			if(hasExited && browserHandler.IsOpen()){
				browserHandler.OnBrowserExit();	
			}
			
			// We can't modify fullScreen in a callback outside of the 
			// main thread
			if (goFullscreen) {
				goFullscreen = false;
				Screen.fullScreen = true;
				Debug.Log ("Going full screen");
			}
		}
	}
}

