﻿using UnityEngine;
using System;
using System.Collections.Generic;
using HatchFramework;
using System.IO;

namespace SSGCore.RMS
{
	public class Admin
	{
		// An event for clients using admin to know when it's closed
		public Action ExitedEvent;
		private BrowserHandlerBase browser;
		private string defaultURL = GlobalConfig.GetProperty<string>("RMS_URL") + "ssg.php";
		private bool updateRequired = false;
		
		
		public Admin ()
		{
			switch (Application.platform) {
				
			case RuntimePlatform.Android:
				browser = new BrowserHandlerAndroid ();
				break;
			case RuntimePlatform.IPhonePlayer:
				browser = new BrowserHandleriOS ();
				break;
			case RuntimePlatform.WindowsEditor:
			case RuntimePlatform.WindowsPlayer:
				updateRequired = true;
				browser = new BrowserHandlerWindows ();
				break;
			case RuntimePlatform.OSXEditor:
			case RuntimePlatform.OSXPlayer:
				updateRequired = true;
				browser = new BrowserHandlerOSX ();
				break;
			default:
				Debug.Log ("RMS.Admin: platform unimplemented");
				browser = new BrowserHandlerBase ();
				break;
			}
			browser.ExitedEvent += NotifyAdminExited;
		}
		
		void NotifyAdminExited(){
			if(ExitedEvent != null){
				ExitedEvent();	
			}
		}
		
		public void Update(){
			if(updateRequired){
				browser.Update();
			}
		}
		
	
		public bool Open ()
		{
			return browser.OpenURL (defaultURL);
		}
		
		public bool Open (string redirect)
		{
			string url = defaultURL + "?path=" + Uri.EscapeUriString(redirect);
			return browser.OpenURL (url);
		}
		
		public bool OpenWizard (string uid)
		{
			string url = defaultURL + "?path=DeviceWizard/registerDevice/device_uid/" + uid;
			DebugConsole.Log("Opening \"Registration\" Wizard at: " + url);
			return browser.OpenURL(url);
		}
		
		public bool OpenWhoAreYou (string uid)
		{
			string url = defaultURL + "?path=whoAreYou/index/device_uid/" + uid;
			DebugConsole.Log("Opening \"Who Are You\" Wizard at: " + url);
			return browser.OpenURL(url);
		}
		
		private string PageForRedirect(string redirect) {
			switch (redirect) {
			case "wizard":
				return "wizard.html";
			case "/report/classoverview":
				return "classoverview.html";
			case "/child/admin":
				return "childadmin.html";
			default:
				return "index.html";
			}
		}
		
		public bool OpenLocal(string redirect)
		{
			string baseUrl = Application.streamingAssetsPath;
			if(Application.platform == RuntimePlatform.Android) {
				baseUrl = "file:///android_asset";
			} else if(Application.platform == RuntimePlatform.WindowsPlayer) {
				// Note(zfk): this is probably fine universally, but I can't test on iOS locally
				baseUrl = "file://" + baseUrl;
			}
			
			string url = baseUrl + "/static_admin/" + PageForRedirect(redirect);
			return browser.OpenURL (url);
		}

		public bool IsOpen ()
		{
			return browser.IsOpen();
		}
		
		public bool Close(){
			return browser.Close();	
		}
	}
}
