using System;
using UnityEngine;

namespace SSGCore.RMS
{
	public class BrowserHandlerAndroid : BrowserHandlerBase
	{
#if UNITY_ANDROID
		private UniWebView webViewObject;
#endif		
		public BrowserHandlerAndroid ()
		{
#if !UNITY_ANDROID
			Debug.Log ("SSGCore DLL was not built with Android support");		
#endif
		}
			
		public override bool OpenURL (string url)
		{
#if UNITY_ANDROID
			webViewObject =
			(new GameObject ("WebViewObjectAndroid")).AddComponent<UniWebView> ();
			webViewObject.Load(url);
			webViewObject.Show();
			webViewObject.OnWebViewShouldClose += v => {
				Close ();
				return false;
			};

			// Disable uniSWF touches while browser is open
			MovieClipOverlayCameraBehaviour.instance.enableMouse = false;
			
			return true;
#else
			return false;
#endif
		}
		
		
		public override bool Close ()
		{
			if (ExitedEvent != null) {
				ExitedEvent ();	
			}
#if UNITY_ANDROID
			webViewObject.Hide();

			
			UnityEngine.GameObject.Destroy(webViewObject.gameObject, 0.5f);

			// re-enabled uniSWF touches when browser closes
			MovieClipOverlayCameraBehaviour.instance.enableMouse = true;
#endif
			return true;
		}
	}
}

