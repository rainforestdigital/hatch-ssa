using System;
using UnityEngine;

namespace SSGCore.RMS
{
	public class BrowserHandlerBase
	{
		/// <summary>
		/// Event for the admin object to know when the browser is closed
		/// </summary
		public Action ExitedEvent;
		
		public virtual bool OpenURL(string url){Debug.Log("Platform unimplemented"); return false;}
		public virtual bool IsOpen(){return false;}
		public virtual bool Close(){return false;}
		public virtual void Update(){} //Todo: eliminate the need for this
	}
}

