using System;
using UnityEngine;


namespace SSGCore.RMS

{
	public class BrowserHandlerOSX : BrowserHandlerBase
	{
		private WebViewObjectOSX webViewObject;
		

		public override bool OpenURL (string url)
		{
			webViewObject =
			(new GameObject ("WebViewObjectOSX")).AddComponent<WebViewObjectOSX> ();
			webViewObject.Init ((msg) => {
				Debug.Log (string.Format ("CallFromJS[{0}]", msg));
				
				if( msg == "ExitWebView" )
				{
					Close ();
				}
			});
			webViewObject.browserHandler = this;
			Debug.Log("Loading url: "+url);
			Debug.Log(webViewObject);
			webViewObject.LoadURL (url);
			webViewObject.SetMargins(0,0,0,0);
			webViewObject.SetVisibility (true);
			
			return true;
		}
		
		public override bool IsOpen ()
		{
			if(webViewObject != null){
				return webViewObject.visibility;
			} 
			return false;
		}
		
		public override bool Close ()
		{
			webViewObject.SetVisibility(false);
			if (ExitedEvent != null) {
				ExitedEvent ();	
			}
			UnityEngine.GameObject.Destroy(webViewObject.gameObject, 0.5f);
			return true;
		}
	}
}

