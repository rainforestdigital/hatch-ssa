#if !UNITY_IPHONE
#define ENCRYPT_DATABASE
#endif

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using HatchFramework;
using System.Linq;

namespace SSGCore
{ 
	public class DBObject {
		private static bool databaseLocked = false;
		
		private SQLiteDB db = null;
		public int currentUserID = -1;
		public int currentReportingYear;
		public int endOfCurrentReportingYear;
		
		private SQLiteQuery selectClassrooms;
		private SQLiteQuery selectUsers;
		private SQLiteQuery selectSessions;
		private SQLiteQuery selectLocks;
		private SQLiteQuery selectRefocusActivities;
		
		private List<ClassroomDB> resultClassrooms;
		private List<UserDB> resultUsers;
		private List<SessionDB> resultSessions;
		private List<LockDB> resultLocks;
		private List<RefocusActivityDB> resultRefocusActivities;
		
		private Dataset dataset;
		private Dictionary<int, int> idRemaps;
		private List<Skill> loadedSkills;
		
		public List<ClassroomDB> Classrooms{
			get{
				return resultClassrooms;	
			}
		}
		
		public List<UserDB> Users{
		
			get{
				return resultUsers;
			}
				
		}
		
		public List<SessionDB> Sessions{
			get{
				return resultSessions;
			}
				
		}
		
		public List<LockDB> Locks{
			get{
				return resultLocks;
			}
		}
		
		public List<RefocusActivityDB> RefocusActivities{
			get{
				return resultRefocusActivities;
			}
				
		}
		
		private Action OnDatasetChangedInvoker;
		public event Action OnDatasetChanged
		{
			add { OnDatasetChangedInvoker += value; }
			remove { OnDatasetChangedInvoker -= value; }
		}
		
		public DBObject()
		{
			currentReportingYear = GetReportingYear();	
	
		}
		
		void Update () {
			// Updating methods when SELECT runs are being executed on the database.  Allows for screen redraws in between periods of processing the returned rows.
		}
		
		void CreateHatchTables(SQLiteDB db)
		{
			if(GlobalConfig.GetProperty<bool>("UseTestDatabase")) {
				StaticData.CreateDemoDatabase(db);
			} else {
				StaticData.CreateDatabase(db);
			}
		}
		
		protected bool _debug = false;
		public void SetDebugMode(bool debug){
			_debug = debug;
			Open ();
		}
		
		
		
		public void Open ()
		{
			
			DebugConsole.Instance.RegisterBehaviour(this);
			
			if(db != null) {
				db.Close();
				
			}
			
			string dir = Path.Combine(PlatformUtils.InternalDataPath, GlobalConfig.GetProperty<string>("LOCAL_DIR"));
			Directory.CreateDirectory(dir);
			
#if ENCRYPT_DATABASE
			string filename = "/Hatch.dbenc";
#else
			string filename = "/Hatch.db";
#endif
			
			db = new SQLiteDB();
			string path = dir + filename;
		
			
			try{
				db.Open(path);
#if ENCRYPT_DATABASE
				db.Key("H@tch123");
#endif
				
				CreateHatchTables(db);
				
			} catch (Exception e){
				DebugConsole.LogError(e.ToString());
				CreateHatchTables(db);
			}
		}
				
		
		public string[] GetAllImagePaths(){
		
			SQLiteQuery sq = new SQLiteQuery(db, String.Format("SELECT DISTINCT picture FROM user"));
			List<string> str = new List<string>();
			while(sq.Step()){
				if(!sq.IsNULL("picture")) {
					str.Add(sq.GetString("picture"));
				}
			}
			sq.Step();
			sq.Release();
			
			
			sq = new SQLiteQuery(db, String.Format("SELECT DISTINCT classroom_pic FROM classroom"));
			
			while(sq.Step()){
				if(!sq.IsNULL("classroom_pic")) {
					str.Add(sq.GetString("classroom_pic"));
				}
			}
			sq.Step();
			sq.Release();
			
			sq = new SQLiteQuery(db, String.Format("SELECT DISTINCT teacher_pic FROM classroom"));
			
			while(sq.Step()){
				if(!sq.IsNULL("teacher_pic")) {
					str.Add(sq.GetString("teacher_pic"));
				}
			}
			sq.Step();
			sq.Release();
					
			return str.ToArray();
			
		}
		
		public int GetReportingYear()
		{
			DateTime date = System.DateTime.UtcNow;
			if(date.Month < 6)
				return date.Year-1;
			else
				return date.Year;
		}
	
		public void LoadClassrooms()
		{
			selectClassrooms = new SQLiteQuery(db, "SELECT * FROM classroom ORDER BY name");
			SetClassrooms();
			selectClassrooms.Step();												
			selectClassrooms.Release();		
		}
		
		public void ClearUsers(){
			SQLiteQuery sq = new SQLiteQuery(db, "DELETE FROM user");
			sq.Step();
			sq.Release();
		}
		
		public void LoadUsers()
		{
			selectUsers = new SQLiteQuery(db, "SELECT * FROM user ORDER BY last_name");
			SetUsers();
			selectUsers.Step();												
			selectUsers.Release();
		}	
		
		public void LoadSessions()
		{
			if(currentUserID == -1)
			{
				selectSessions = new SQLiteQuery(db, "SELECT * FROM session");
			}
			else
			{
				selectSessions = new SQLiteQuery(db, "SELECT * FROM session WHERE user_id = ? ORDER BY started DESC");
				selectSessions.Bind(currentUserID);
			}
			SetSessions();
			selectSessions.Step();												
			selectSessions.Release();
			
		}
		
		public void LoadLocks()
		{
			if(currentUserID == -1)
			{
				selectLocks = new SQLiteQuery(db, "SELECT * FROM lock");
			}
			else
			{
				selectLocks = new SQLiteQuery(db, "SELECT * FROM lock WHERE user_id = ?");
				selectLocks.Bind(currentUserID);
			}
			SetLocks();
			selectLocks.Step();												
			selectLocks.Release(); 
			
			
		}
		
		public void LoadRefocusActivities()
		{
			if(currentUserID == -1)
			{
				selectRefocusActivities = new SQLiteQuery(db, "SELECT * FROM refocus WHERE start_date >= " + currentReportingYear + " AND start_date <= " + endOfCurrentReportingYear);
			}
			else
			{
				selectRefocusActivities = new SQLiteQuery(db, "SELECT * FROM refocus WHERE user_id = " + currentUserID);
			}

			SetRefocusActivities();
			selectRefocusActivities.Step();												
			selectRefocusActivities.Release(); 
		}
	
		private void SetClassrooms()
		{
			resultClassrooms = new List<ClassroomDB>();
			while(selectClassrooms.Step())
			{
				int id = selectClassrooms.GetInteger("id");
				int teacherID = selectClassrooms.GetInteger("teacher_id");
				
				string name = selectClassrooms.GetString("name");
				string userDefinedID = selectClassrooms.GetString("user_defined_id");
				string classroomPic = selectClassrooms.GetString("classroom_pic");
				string teacherPic = selectClassrooms.GetString("teacher_pic");
				string themes = selectClassrooms.GetString("themes");
				resultClassrooms.Add(new ClassroomDB(id, teacherID, name, userDefinedID, classroomPic, teacherPic, themes));
			}	
		}
	
		private void SetUsers()
		{
			resultUsers = new List<UserDB>();
			while(selectUsers.Step())
			{
				int id = selectUsers.GetInteger("id");
				string birthDate = selectUsers.GetString("birth_date");
				int needSync = selectUsers.IsNULL("need_sync") ? 0 : selectUsers.GetInteger("need_sync");
				int classroomID = selectUsers.GetInteger("classroom_id");
				
				string uid = selectUsers.GetString("uid");
				string customID = selectUsers.GetString("custom_id");
				string firstName = selectUsers.GetString("first_name");
				string lastName = selectUsers.GetString("last_name");
				string picture = selectUsers.GetString("picture");
				string pictureData = selectUsers.GetString("picture_data");
				string bookmark = null;
				try{ bookmark = selectUsers.GetString("sessionBookmark"); }
				catch{ bookmark = ""; }
				int profile = 0;
				try{ profile =  selectUsers.GetInteger("language"); }
				catch{}
				
				resultUsers.Add(new UserDB(id,classroomID, birthDate, needSync, firstName, lastName, customID, uid, picture, pictureData, bookmark, profile));
			}
		}	
		
		private void SetSessions()
		{
			resultSessions = new List<SessionDB>();
			while(selectSessions.Step())
			{
				int id = selectSessions.GetInteger("id");
				int userID = selectSessions.GetInteger("user_id");
				int duration = selectSessions.GetInteger("duration");
				int skillID = selectSessions.GetInteger("skill_id");
				int skillLevelID = selectSessions.GetInteger("skill_level_id");
				int needSync = selectSessions.GetInteger("need_sync");
				float percent = 0;
				if(selectSessions.IsNULL("percent")) {
					DebugConsole.LogWarning("Session has null percent for skill {0} level {1}", skillID, skillLevelID);
				} else {
					percent = (float)selectSessions.GetDouble("percent");
				}
				
				string uid = selectSessions.GetString("uid");
				string started = selectSessions.GetString("started");
				string skillLevel = selectSessions.GetString("skill_level");
				string opportunities = selectSessions.GetString("opportunities");
				
				SessionDB newSession = new SessionDB(id, userID, skillID, duration, skillLevelID, needSync, percent, started, skillLevel, opportunities, uid);
				
				try{ newSession.serialized = selectSessions.GetString( "serialized_data" ); }
				catch{}
				
				resultSessions.Add( newSession );
			}				
		}
		
		private void SetLocks()
		{
			resultLocks = new List<LockDB>();
			while(selectLocks.Step())
			{
				int id = selectLocks.GetInteger("id");
				int userID = selectLocks.GetInteger("user_id");
				int skillID = selectLocks.GetInteger("skill_id");
				int enabled = selectLocks.GetInteger("is_enabled");
				int needSync = selectLocks.GetInteger("need_sync");
				
				string uid = selectLocks.GetString("uid");
				string created = selectLocks.GetString("created");
				
				resultLocks.Add(new LockDB(id, userID, skillID, enabled, needSync, uid, created));
			}				
		}
		
		private void SetRefocusActivities()
		{
			resultRefocusActivities = new List<RefocusActivityDB>();
			while(selectRefocusActivities.Step())
			{
				int id = selectRefocusActivities.GetInteger("id");
				int userID = selectRefocusActivities.GetInteger("user_id");
				
				string startDate = selectRefocusActivities.GetString("start_date");
				string ExpirationDate = selectRefocusActivities.GetString("expiration_date");
				string familyIDs = selectRefocusActivities.GetString("families_id");
				
				resultRefocusActivities.Add(new RefocusActivityDB(id, userID, startDate, ExpirationDate, familyIDs));
			}				
		}
		
		public void ReplaceClassrooms(List<RMS.Webservice.Classroom> fetched)
		{
			using(var transaaction = CreateTransaction()) {
				ExecuteCommand("DELETE FROM classroom");
				
				SQLiteQuery sq = new SQLiteQuery(db, "INSERT INTO classroom (id, name, user_defined_id, teacher_id, classroom_pic, teacher_pic, themes) VALUES (?, ?, ?, ?, ?, ?, ?);");
				foreach(var classroom in fetched) {
					sq.Bind(classroom.id);
					sq.Bind(classroom.name);
					sq.Bind(classroom.user_defined_id);
					sq.Bind(classroom.teacher_id);
					sq.Bind(classroom.classroom_pic);
					sq.Bind(classroom.teacher_pic);
					sq.Bind(classroom.themes);
					sq.Step();
					sq.Reset();
				}
				
				transaaction.MarkSuccess();
			}
		}
	
		
	
		public void InsertClassroom(int id, string name, string userDefinedID, int teacherID, string classroomPic, string teacherPic, string themes_available)
		{
			Debug.Log ("Inserting classroom: "+id+" "+name+" "+userDefinedID+" "+teacherID );
			SQLiteQuery sq = new SQLiteQuery(db, String.Format("INSERT INTO classroom VALUES ({0}, \"{1}\", \"{2}\", {3}, \"{4}\", \"{5}\", \"{6}\")", id, name, userDefinedID, teacherID, classroomPic, teacherPic, themes_available));
			sq.Step();
			sq.Release();
			
			
			//TODO encrypt name?
			//LoadClassrooms(); //commented out by ziba.  I see there is an explicit load database elsewhere.  Why call loadclassroom after every insert? Inserts will happen in batches.
		}
		
		public void InsertRefocusActivity(string user_id, string startDate, string expirationDate, string familyIDs)
		{
			SQLiteQuery sq = new SQLiteQuery(db, String.Format("INSERT INTO refocus VALUES (NULL, {0}, '{1}', '{2}', '{3}')", int.Parse(user_id), startDate, expirationDate, familyIDs));
			sq.Step();
			sq.Release();
			LoadRefocusActivities();
		}
		
		public void InsertEulaAgreement( string user_name )
		{
			DebugConsole.Log("NOT SAVING EULA TO DB CURRENTLY");
			/*
			SQLiteQuery sq = new SQLiteQuery(db, "INSERT INTO eulas VALUES (?, ?, 0)");
			sq.Bind(user_name);
			sq.Bind(DateUtils.ConvertToUnixTimestamp(DateTime.UtCNow));
			sq.Step();
			sq.Release();
			*/
		}
		
		public void InsertUser(Child child, Classroom classroom)
		{
			
			string folder = Path.Combine(PlatformUtils.InternalDataPath, GlobalConfig.GetProperty<string>("LOCAL_DIR"));
			string fileName = "images/users/" + child.UID+ ".png";
			string imagePath = Path.Combine(folder, fileName);
			
			ImageUtility.TextureToFile(imagePath, (Texture2D)child.Texture);
			
			try{ ExecuteCommand("ALTER TABLE user ADD sessionBookmark TEXT null"); }catch{}
			try{ ExecuteCommand("ALTER TABLE user ADD language INTEGER(1)"); }catch{}

			SQLiteQuery sq = new SQLiteQuery(db, "INSERT INTO user (uid, custom_id, first_name, last_name, birth_date, picture, classroom_id, sessionBookmark, language, need_sync) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 0)");
			sq.Bind(child.UID);
			sq.Bind(child.CustomId);
			sq.Bind(child.FirstName);
			sq.Bind(child.LastName);
			sq.Bind(child.BirthDate);
			sq.Bind(fileName);
			sq.Bind(classroom.ID);
			sq.Bind(child.BookmarkRaw);
			sq.Bind(LanguageProfileUtility.FromId( child.Profile ));
			sq.Step();
			sq.Release();
			LoadUsers();
			UpdateDataset();
		}
		
		[DebugConsoleMethod("insert_user", "insert new user: id")]
		public void InsertTestUser(int userIDNum){
		
			InsertUser(userIDNum, "TEST_USER_"+System.Guid.NewGuid().ToString(), "", "TEST"+userIDNum, userIDNum+"Test", 0, "/images/users/test_user_10.png", "2008-06-01", 977, 1);
			LoadUsers();
			UpdateDataset();
		}
		
		public void ReplaceUsers (List<SSGCore.RMS.Webservice.Student> fetched)
		{
			using (var transaction = CreateTransaction())
			{
			
				ExecuteCommand("DELETE FROM user");
				
				try{ ExecuteCommand("ALTER TABLE user ADD sessionBookmark TEXT null"); }catch{}
				try{ ExecuteCommand("ALTER TABLE user ADD language INTEGER(1)"); }catch{}
			
				var sq = new SQLiteQuery(db, "INSERT INTO user (uid, id, custom_id, first_name, last_name, birth_date, picture, classroom_id, sessionBookmark, language, need_sync) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0)");
				foreach(var user in fetched) {
					int langBind;
					if( !int.TryParse( user.language, out langBind ) )
						langBind = 1;
					
					sq.Bind(user.uid);
					sq.Bind(user.id);
					sq.Bind(user.custom_id);
					sq.Bind(user.first_name);
					sq.Bind(user.last_name);
					sq.Bind(user.birth_date);
					sq.Bind(user.picture);
					sq.Bind(user.classroom_id);
					sq.Bind(user.bookmark_session != null ? user.bookmark_session.ToJson() : "" );
					sq.Bind( langBind );
					sq.Step();
					sq.Reset();
				}
				
				transaction.MarkSuccess();
			}
		}
		
		public void InsertUser(int id, string uid, string customId, string firstName, string lastName, int epoch, string filename, string birthdate, int classroom, int need_sync){
			InsertUser(id, uid,customId,firstName,lastName, epoch, filename, birthdate, classroom, need_sync, "", LanguageProfile.English);
		}
		public void InsertUser(int id, string uid, string customId, string firstName, string lastName, int epoch, string filename, string birthdate, int classroom, int need_sync, string bookmark){
			InsertUser(id, uid,customId,firstName,lastName, epoch, filename, birthdate, classroom, need_sync, bookmark, LanguageProfile.English);
		}
		public void InsertUser(int id, string uid, string customId, string firstName, string lastName, int epoch, string filename, string birthdate, int classroom, int need_sync, LanguageProfile profile){
			InsertUser(id, uid,customId,firstName,lastName, epoch, filename, birthdate, classroom, need_sync, "", profile);
		}
		public void InsertUser(int id, string uid, string customId, string firstName, string lastName, int epoch, string filename, string birthdate, int classroom, int need_sync, string bookmark, LanguageProfile profile)
		{
			try{ ExecuteCommand("ALTER TABLE user ADD sessionBookmark TEXT null"); }catch{}
			try{ ExecuteCommand("ALTER TABLE user ADD language INTEGER(1)"); }catch{}
			
			SQLiteQuery sq = new SQLiteQuery(db, "INSERT INTO user (uid, id, custom_id, first_name, last_name, birth_date, picture, classroom_id, sessionBookmark, language, need_sync) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			sq.Bind(uid);
			sq.Bind(id);
			sq.Bind(customId);
			sq.Bind(firstName);
			sq.Bind(lastName);
			sq.Bind(birthdate);
			sq.Bind(filename);
			sq.Bind(classroom);
			sq.Bind(bookmark);
			sq.Bind(LanguageProfileUtility.FromId( profile ));
			sq.Bind(need_sync);
			sq.Step();
			sq.Release();
		}
	
		public void UpdateClassroom(ClassroomDB classroom)
		{
			SQLiteQuery sq = new SQLiteQuery(db, String.Format("UPDATE classroom SET name={0}, user_defined_id='{1}', teacher_id='{2}', classroom_pic='{3}', teacher_pic='{4}', themes='{5}' WHERE id={6}", classroom.Name, classroom.UserDefinedID, classroom.TeacherID, classroom.ClassroomPic, classroom.TeacherPic, classroom.Themes, classroom.ID));
			sq.Step();
			sq.Release();
			LoadClassrooms();
		}
		
		public void UpdateDataset()
		{
			UpdateDataset(idRemaps);
			idRemaps = null;
		}
		
		private void UpdateDataset(Dictionary<int, int> idChanges)
		{
			if(dataset != null) {
				dataset.Update(this, idChanges);
				
				if(OnDatasetChangedInvoker != null) {
					OnDatasetChangedInvoker();
				}
			}
		}
		
		private bool IsImageReferenced(string filename)
		{
			SQLiteQuery sq;
			
			try {
				sq = new SQLiteQuery(db, String.Format("SELECT * FROM user where picture = ?"));
				sq.Bind(filename);
				
				if(sq.Step())
				{
					sq.Release();
					return true;
				}
				
				sq.Release();
			} catch {}
			
			try {
				sq = new SQLiteQuery(db, String.Format("SELECT * FROM classroom where classroom_pic = ? OR teacher_pic = ?"));
				sq.Bind(filename);
				sq.Bind(filename);
				
				if(sq.Step())
				{
					sq.Release();
					return true;
				}
				
				sq.Release();
			} catch {}
			
			return false;
		}
		
		public void SetUserImage(Child child, Texture2D image)
		{		
			UserDB user = Users.FirstOrDefault(u => u.ID == child.ID);
			if(user == null) {
				DebugConsole.LogWarning("Could not find user in database to set image: {0}", child.ID);
				return;
			}
			
			// generate new file name when updating picture, since image could be shared by multiple users
			string uid = String.IsNullOrEmpty(child.UID) ? Guid.NewGuid().ToString() : child.UID;
			string folder = Path.Combine(PlatformUtils.InternalDataPath, GlobalConfig.GetProperty<string>("LOCAL_DIR"));
			string fileName = "images/users/" + uid + ".png";
			string imagePath = Path.Combine(folder, fileName);
			
			ImageUtility.TextureToFile(imagePath, image);

			// update image path, and set need_sync to true
			SQLiteQuery sq = new SQLiteQuery(db, "UPDATE user SET picture=?, need_sync=1 WHERE id=?");
			sq.Bind(fileName);
			sq.Bind(child.ID);
			sq.Step();
			sq.Release();
			
			// remove old image if no longer referenced
			if(user.Picture != fileName && !IsImageReferenced(user.Picture)) {
				var oldFilename = user.Picture.StartsWith("/") ? user.Picture.Substring(1) : user.Picture;
				var oldImagePath = Path.Combine(folder, oldFilename);
				DebugConsole.Log("Deleting: {0}", oldImagePath);
				if(File.Exists(oldImagePath)) {
					File.Delete(oldImagePath);
				}
			}
			
			LoadUsers();
			UpdateDataset();// This is a bit overkill, but it is a good exercise of the code
		}
		
		public void InsertLock(LockDB gameLock)
		{
			SQLiteQuery sq = new SQLiteQuery(db, "INSERT INTO lock (user_id, uid, skill_id, is_enabled, created, need_sync) VALUES (?, ?, ?, ?, ?, ?)");
			sq.Bind(gameLock.UserID);
			sq.Bind(gameLock.UID);
			sq.Bind(gameLock.SkillID);
			sq.Bind(gameLock.Enabled);
			sq.Bind(System.DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"));
			sq.Bind(gameLock.NeedSync);
			sq.Step();
			sq.Release();
			//LoadLocks();
		}

		void ClearLocks ()
		{
			ExecuteCommand("DELETE FROM lock");
		}

		public void ReplaceLocks (List<SSGCore.RMS.Webservice.Lock> locks)
		{
			using (var transaction = CreateTransaction())
			{
			
				ClearLocks ();
			
				var sq = new SQLiteQuery(db, "INSERT INTO lock (user_id, uid, skill_id, is_enabled, created, need_sync) VALUES (?, ?, ?, ?, ?, 0)");
				foreach(var fetchedLock in locks) {
					sq.Bind(Int32.Parse(fetchedLock.user_id));
					sq.Bind(fetchedLock.uid);
					sq.Bind(Int32.Parse(fetchedLock.skill_id));
					sq.Bind(Int32.Parse(fetchedLock.is_enabled));
					sq.Bind(fetchedLock.created);
					sq.Step();
					sq.Reset();
				}
				
				transaction.MarkSuccess();
			}
		}
		
		public void UpdateLock(LockDB gameLock)
		{
			SQLiteQuery sq = new SQLiteQuery(db, "UPDATE lock SET is_enabled=?, need_sync=1 WHERE id=?");
			sq.Bind(gameLock.Enabled);
			sq.Bind(gameLock.ID);
			sq.Step();
			sq.Release();
			//LoadLocks();
		}
		
		public void UnlockSkill(int userId, int skillId)
		{
			SQLiteQuery sq = new SQLiteQuery(db, "UPDATE lock SET is_enabled=0, need_sync=1 WHERE user_id=? and skill_id=? and is_enabled=1");
			sq.Bind(userId);
			sq.Bind(skillId);
			sq.Step();
			sq.Release();
			//LoadLocks();
		}
		
		public void deleteUser(UserDB user)
		{
			/*
			var tempFile:File = new File($data.image[0].text().toString());
			try
			{
				tempFile.deleteFile();
			}
			catch(error:IOError)
			{
				// file doesn't exist
			}
			*/	
			SQLBatch batch = new SQLBatch();
			batch.AddStatement("DELETE FROM users WHERE id=" + user.ID);
			batch.AddStatement("DELETE FROM sessions WHERE user_id=" + user.ID);
			batch.AddStatement("DELETE FROM locks WHERE user_id=" + user.ID);
			batch.AddStatement("DELETE FROM refocus_activities WHERE user_id=" + user.ID);

			LoadUsers();
			LoadSessions();
			LoadLocks();
			LoadRefocusActivities();
		}
		
		public void ResetUserLeveling(int id)
		{
			SQLBatch batch = new SQLBatch();
			batch.AddStatement("DELETE FROM sessions WHERE user_id=" + id);
			batch.AddStatement("DELETE FROM locks WHERE user_id=" + id);
			batch.RunStatements(db);
			LoadSessions();
			LoadLocks();
		}
		
		#region Statics
		
		public void ClearStatics(){
			SQLBatch batch = new SQLBatch();
			batch.AddStatement("DELETE FROM family");
			batch.AddStatement("DELETE FROM level");
			batch.RunStatements(db);
		}
		
		public void InsertFamily(int key, string name){
			string sql = String.Format ("INSERT INTO family VALUES ({0}, \"{1}\")", key, name);
			SQLiteQuery sq = new SQLiteQuery(db, sql);
			sq.Step();
			sq.Release();
		}
		
		public void InsertLevel(int key, string code, string name){
			string sql = String.Format ("INSERT INTO level VALUES ({0}, \"{1}\", \"{2}\")", key, code, name);
			SQLiteQuery sq = new SQLiteQuery(db, sql);
			sq.Step();
			sq.Release();
		}
		#endregion
		
		#region Settings
		public void ClearSettings(){
			SQLBatch batch = new SQLBatch();
			batch.AddStatement("DELETE FROM settings");
			batch.RunStatements(db);
		}
		
		public void ClearSessions(){
			SQLBatch batch = new SQLBatch();
			batch.AddStatement("DELETE FROM session");
			batch.RunStatements(db);
		}
		
		public void ClearRefocus(){
			SQLBatch batch = new SQLBatch();
			batch.AddStatement("DELETE FROM refocus");
			batch.RunStatements(db);
		}
		
		public void ClearClassrooms(){
			SQLBatch batch = new SQLBatch();
			batch.AddStatement("DELETE FROM classroom");
			batch.RunStatements(db);
		}
		
		public void SetDeviceAlias(string deviceAlias) {
			var query = new SQLiteQuery(db, "UPDATE settings SET device_alias=?");
			query.Bind(deviceAlias);
			query.Step();
			query.Release();
		}
		
		public void ClearNonstatic() {
			databaseLocked = true;
			
//			using (var transaction = CreateTransaction())
//			{
//				ClearSettings();
//				ClearUsers();
//				ClearClassrooms();
//				ClearSessions();
//				ClearRefocus();
//				ClearLocks();
//				transaction.MarkSuccess();
//			}
		}
		
		public void InsertSetting(string device_uid){
			
			InsertSetting(0,
				"", // rms_url
				0, // device_id
				device_uid, //device_uid
				0, //organization_id
				0, //school id
				"", //salt
				GlobalConfig.GetProperty("version"), //app_version
				GlobalConfig.GetProperty("dbversion"), //db version
				"" // device_alias
			);
			
			databaseLocked = false;
		}
		
		public void InsertSetting(int id, string rms_url, int device_id, string device_uid, int organization_id, int school_id, string salt, string app_version, string db_version, string device_alias){
			
			
			//rms_url varchar, device_id int, device_uid varchar, organization_id int, school_id int, salt varchar, app_version varchar, db_version varchar, device_alias varchar);",
			string sql = "INSERT INTO settings VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?)"; 
			
			SQLiteQuery sq = new SQLiteQuery(db, sql);
			sq.Bind (rms_url);
			sq.Bind(device_id);
			sq.Bind(device_uid);
			sq.Bind(organization_id);
			sq.Bind(school_id);
			sq.Bind(salt);
			sq.Bind(app_version);
			sq.Bind(db_version);
			sq.Bind(device_alias);
			sq.Step();
			sq.Release();
			
		}
		
		public string GetDeviceUid() {
			if( databaseLocked )
				return "";
			
			SQLiteQuery sq = new SQLiteQuery(db, "SELECT device_uid FROM settings LIMIT 1");
			string deviceUid = null;
			while(sq.Step()) {
				deviceUid = sq.GetString("device_uid");
			}
			sq.Release();
			return deviceUid;
		}
		
		public string GetDeviceAlias() {
			SQLiteQuery sq = new SQLiteQuery(db, "SELECT device_alias FROM settings LIMIT 1");
			string alias = null;
			while(sq.Step()) {
				alias = sq.GetString("device_alias");
			}
			sq.Release();
			return alias;
		}
		#endregion
		
		public void BeginTransaction() {
			var sq = new SQLiteQuery(db, "BEGIN TRANSACTION");
			sq.Step();
			sq.Release();
		}
		
		public void EndTransaction() {
			var sq = new SQLiteQuery(db, "COMMIT TRANSACTION");
			sq.Step();
			sq.Release();
		}
		
		public Transaction CreateTransaction() {
			return new Transaction(db);
		}
		
		private void ExecuteCommand(string command) {
			var sq = new SQLiteQuery(db, command);
			sq.Step();
			sq.Release();
		}
		
		public void ApplyNewUsers(Dictionary<string, string> newIDs) {
			
			idRemaps = new Dictionary<int, int>();
			BeginTransaction();
			
			ExecuteCommand("UPDATE session SET need_sync=0 WHERE need_sync=1");
			ExecuteCommand("UPDATE lock SET need_sync=0 WHERE need_sync=1");
			
			//string IDs
			//replace anything referncing the current ID with the new ID
			foreach(string key in newIDs.Keys){
			
				string uID = key;
				
				UserDB cUser = Users.Find(x=>x.UID == uID);
				if(cUser == null) {
					DebugConsole.Log("Cant find user with uid: "+uID);
					continue;
				}
				
				
				int newID = Int32.Parse(newIDs[key]);
				idRemaps[cUser.ID] = newID;
				cUser.ID = newID;
				
				var sq = new SQLiteQuery(db, "UPDATE session SET user_id=? WHERE uid=?");
				sq.Bind(newID);
				sq.Bind(uID);
				sq.Step();
				sq.Release();
				
				sq = new SQLiteQuery(db, "UPDATE lock SET user_id=? WHERE uid=?");
				sq.Bind(newID);
				sq.Bind(uID);
				sq.Step();
				sq.Release();
			}
			
			//clear out the old users
			if(newIDs.Count > 0) {
				ClearUsers();
				foreach(UserDB u in Users){
					InsertUser(u.ID, u.UID, u.CustomID, u.FirstName, u.LastName, 0, u.Picture, u.BirthDate, u.ClassroomID, 0, u.Bookmark);	
				}
			} else {
				ExecuteCommand("UPDATE user SET need_sync=0 WHERE need_sync=1");
			}
			
			EndTransaction();
			
		}

		public void ReplaceWeights (List<SSGCore.RMS.Webservice.GetAllSSGActivities> fetched)
		{
			using (var transaction = CreateTransaction())
			{
				ExecuteCommand("DELETE FROM weight");
				
				var insert = new SQLiteQuery(db, "INSERT INTO weight (id, skill_id, skill_level, weight) VALUES (?, ?, ?, ?)");
				int id = 0;
				foreach (var info in fetched) {
					int weight = (int)Math.Ceiling(Convert.ToDouble(info.weight));
					int skillId = Convert.ToInt32(info.skill_id);
					int skillLevelId = Convert.ToInt32(info.skill_level_id);
					insert.Bind(id);
					insert.Bind(skillId);
					insert.Bind(skillLevelId);
					insert.Bind(weight);
					insert.Step();
					insert.Reset();
					id++;
				}
				insert.Release();
				
				transaction.MarkSuccess();
			}
		}
		
		public List<Skill> LoadSkills() {
			if(loadedSkills == null || loadedSkills.Count == 0) {
				loadedSkills = DoLoadSkills();
			}
			return loadedSkills;
		}
		
		public List<Skill> DoLoadSkills() {
			
				
			var families = new Dictionary<int, SkillFamily>();
			var skillsForFamily = new Dictionary<int, List<Skill> >();
			var query = new SQLiteQuery(db, "SELECT id, name FROM family");
			while(query.Step()) {
				var id = query.GetInteger("id");
				var name = query.GetString("name");
				if( name.Contains( " - Spanish" ) )
				{
					foreach( KeyValuePair<int, SkillFamily> f in families )
					{
						if( name.Contains( f.Value.Name ) )
						{
							f.Value.SPN_ID = id;
							break;
						}
					}
				}
				else
				{
					families.Add(id, new SkillFamily(name, id));
					skillsForFamily.Add(id, new List<Skill>() );
				}
			}
			query.Release();
			
			var skills = new List<Skill>();
			query = new SQLiteQuery(db, "SELECT id, name, family_id, spanish_id FROM skill ORDER BY id");
			while(query.Step()) {
				var id = query.GetInteger("id");
				var name = query.GetString("name");
				var family_id = query.GetInteger("family_id");
				var spanish_id = query.GetInteger("spanish_id");
				
				if(!families.ContainsKey(family_id)) {
					DebugConsole.LogError("Unknown family id {0} for skill '{1}'", family_id, name);
					continue;
				}
				var family = families[family_id];
				var previousSkills = skillsForFamily[family_id];
				var previousSkill = previousSkills.Count > 0 
					? previousSkills[previousSkills.Count-1]
					: null;
				
				var levels = new List<SkillLevel>();
				var weights = new Dictionary<SkillLevel, int>();
				
				var weightQuery = new SQLiteQuery(db, "SELECT skill_level, weight FROM weight WHERE skill_id = ?");
				weightQuery.Bind(id);
				while (weightQuery.Step()) {
					
					//Debug.Log(weightQuery.GetFieldType("skill_level"));
					var levelId = weightQuery.GetIntegerForced("skill_level");
					var level = SkillLevelUtil.FromId(levelId);
					var weight = weightQuery.GetInteger("weight");
					
					if(levels.Contains(level)) {
						DebugConsole.LogWarning("Unknown skill level {0} for skill {1}", levelId, id);
						continue;
					}
					
					levels.Add(level);
					weights.Add(level, weight);
				}
				weightQuery.Release();
				levels.Sort();
				
				// generate weights if none available
				if(levels.Count == 0) {
					AddDefaultWeights(levels, weights, previousSkills.Count);
				}
				
				var skill = Skill.FromName(name,
					family,
					previousSkill,
					levels,
					weights);
				skill.ID = id;
				skill.SPN_ID = spanish_id;
				
				skills.Add(skill);
				previousSkills.Add(skill);
				
				skillsForFamily[family_id] = previousSkills;
			}
			query.Release();
			
			return skills;
		}
		
		private void AddDefaultWeights(List<SkillLevel> levels, Dictionary<SkillLevel, int> dict, int familyLevel) {
			levels.Add(SkillLevel.Tutorial);
			levels.Add(SkillLevel.Emerging);
			levels.Add(SkillLevel.Developing);
			levels.Add(SkillLevel.Developed);
			levels.Add(SkillLevel.Completed);
			
			foreach(var level in levels) {
				int unscaled = 10 - (int)level;
				dict[level] = Math.Max(1, (unscaled*unscaled*unscaled) / (familyLevel+1));
			}
		}
		
		public void ClearSessionSync(int sessionID){
			//SQLiteQuery sq = new SQLiteQuery(db, "UPDATE lock SET is_enabled=?, need_sync=1 WHERE id=?");
			SQLiteQuery sq = new SQLiteQuery(db, "UPDATE session SET need_sync=0 WHERE id=?");
			sq.Bind(sessionID);
			sq.Step();
			sq.Release();
		}
		
		public void ReplaceSessions (List<SSGCore.RMS.Webservice.Session> fetched)
		{
			using (var transaction = CreateTransaction())
			{
				ExecuteCommand("DELETE FROM session");
				try{ ExecuteCommand("ALTER TABLE session ADD bookmarked INTEGER(1) null"); }catch{ }
				
				var sq = new SQLiteQuery(db, "INSERT INTO session (user_id, started, duration, skill_id, skill_level_id, skill_level, percent, bookmarked, need_sync) VALUES (?, ?, ?, ?, ?, ?, ?, ?, 0)");
				foreach (var session in fetched) {
					sq.Bind(session.user_id);
					sq.Bind(session.started);
					sq.Bind(session.duration);
					sq.Bind(session.skill_id);
					sq.Bind(session.skill_level_id);
					sq.Bind(session.skill_level);
					sq.Bind(session.percent);
					sq.Bind(session.bookmarked);
					sq.Step();
					sq.Reset();
				}
				sq.Release();
				
				transaction.MarkSuccess();
			}
		}
		
		public void InsertSession(int user_id, string uid, string starttime, int duration, string skill_id, int skill_level_id, string skill_level, float percent, string opportunities)
		{
			try{ ExecuteCommand("ALTER TABLE session ADD bookmarked INTEGER(1) null"); }catch{ }
			
			SQLiteQuery sq = new SQLiteQuery(db, "INSERT INTO session (user_id, uid, started, duration, skill_id, skill_level_id, skill_level, percent, opportunities, bookmarked, need_sync) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 0, 0)");
					sq.Bind(user_id);
					sq.Bind(uid);
					sq.Bind(starttime);
					sq.Bind(duration);
					sq.Bind(skill_id);
					sq.Bind(skill_level_id);
					sq.Bind(skill_level);
					sq.Bind(percent);
					sq.Bind(opportunities); // TODO: opportunities
					sq.Step();
					sq.Release();
		}
		
		public void InsertSession(SessionInfo currentSession){
		
			if(currentSession != null && currentSession.StartTime != -1)
			{
				if(currentSession.skillPair.Skill.ID != -1 && currentSession.user.ID != -1) {
					
					try{ ExecuteCommand("ALTER TABLE session ADD serialized_data TEXT null"); }catch{ }
					
					var startTime = DateUtils.ConvertFromUnixTimestamp(currentSession.StartTime);
					var startTimeString = startTime.ToString("yyyy-MM-dd HH:mm:ss");
					
					currentSession.StopTiming();
				
					float percent = currentSession.Result;
					DebugConsole.Log ("DB percentage logged: " + percent);
					SQLiteQuery sq = new SQLiteQuery(db, "INSERT INTO session (user_id, uid, started, duration, skill_id, skill_level_id, skill_level, percent, opportunities, bookmarked, serialized_data, need_sync) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1)");
					sq.Bind(currentSession.user.ID);
					sq.Bind(currentSession.user.UID);
					sq.Bind(startTimeString);
					sq.Bind((int)currentSession.gameTime);
					sq.Bind( currentSession.SPN_FLAG ? currentSession.skillPair.Skill.SPN_ID : currentSession.skillPair.Skill.ID );
					Debug.LogWarning( "Sending ID of " + currentSession.skillPair.Skill.linkage + " as: " + (currentSession.SPN_FLAG ? currentSession.skillPair.Skill.SPN_ID : currentSession.skillPair.Skill.ID) + "; for User ID: " + currentSession.user.ID + "," + currentSession.user.UID);
					sq.Bind((int)currentSession.skillPair.Level + 1);
					sq.Bind(SkillLevelUtil.GetCode(currentSession.skillPair.Level));
					sq.Bind(percent);
					sq.Bind(""); // TODO: opportunities
					sq.Bind( currentSession.sessionResume ? 1 : 0 );
					sq.Bind( currentSession.SerializedData.toString() );
					sq.Step();
					sq.Release();
				}
			}
		}
		
		public void pushBookmark(string session_bookmark, int user_id){ pushBookmark(session_bookmark, user_id, null); }
		public void pushBookmark(string session_bookmark, int user_id, SSGCore.RMS.DataSync.CompletedCallback callback)
		{
			//ExecuteCommand("UPDATE user SET sessionBookmark=" + session_bookmark + " WHERE id=" + user_id);
			
			var sq = new SQLiteQuery(db, "UPDATE user SET sessionBookmark=? WHERE id=?");
				sq.Bind(session_bookmark);
				sq.Bind(user_id);
				sq.Step();
				sq.Release();
			
			( new RMS.Webservice.SaveStudentBookmark() ).Save( GetDeviceUid(), session_bookmark, (bool status) => {
				if(!status){
					DebugConsole.LogError("FAILED TO PUSH SaveStudentBookmark");
					
				}
				else
					DebugConsole.Log("Bookmark Pushed Successfully");
				
				if( callback != null )
					callback(status);
			});
		}
		
		public List<Classroom> BuildDataset(){
			if(dataset != null) {
				dataset.Destroy();
				idRemaps = null;
			}
			
			dataset = new Dataset(this);
			return dataset.Classrooms;
		}
		
		protected SQLiteDB DB{
			get{
				return db;	
			}
		}
	}
}