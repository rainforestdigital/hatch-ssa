using System;

namespace SSGCore
{
	public class SessionDB: DBModel
	{
	
		private int id;
		public int ID{ get{ return id; } }
		
		private int userID;
		public int UserID{ get{ return userID; } }
		
		private int duration;
		public int Duration { get { return duration; } }
		
		private int skillID;
		public int SkillID { get { return skillID; } }
		
		private int skillLevelID;
		public int SkillLevelID { get { return skillLevelID; } }
		
		private int needSync;
		public int NeedSync { get { return needSync; } }
		
		private float percent;
		public float Percent { get { return percent; } }
		
		private string started;
		public string Started { get { return started; } }
		
		private string skillLevel;
		public string SkillLevel { get { return skillLevel; } }
		
		private string opportunities;
		public string Opportunities { get { return opportunities; } }
		
		private string uid;
		public string UID { get { return uid; } }
		
		public string serialized;
		
		public SessionDB ()
		{
			id = -1;
			userID = -1;
			skillID = -1;
			duration = -1;
			skillLevelID = -1;
			needSync = -1;
			percent = 0.0F;
			started = "";
			skillLevel = "";
			opportunities = "";
			uid = "";
		}
		
		public SessionDB (int _id, int _userID, int _skillID, int _duration, int _skillLevelID, int _needSync, float _percent, string _started, string _skillLevel, string _opportunities, string _uid)
		{
			id = _id;
			userID = _userID;
			skillID = _skillID;
			duration = _duration;
			skillLevelID = _skillLevelID;
			needSync = _needSync;
			percent = _percent;
			started = _started;
			skillLevel = _skillLevel;
			opportunities = _opportunities;
			uid = _uid;
		}
	}
}

