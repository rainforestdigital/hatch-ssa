using System;

namespace SSGCore
{
	public class SettingsDB: DBModel
	{
		private int id;
		public int ID{ get{ return id; } }
		
		private int deviceID;
		public int DeviceID{ get{ return deviceID; } }
		
		private int organizationID;
		public int OrganizationID { get { return organizationID; } }
		
		private int schoolID;
		public int SchoolID { get { return schoolID; } }

		private string rmsURL;
		public string RmsURL { get { return rmsURL; } }
		
		private string deviceUID;
		public string DeviceUID { get { return deviceUID; } }
		
		private string salt;
		public string Salt { get { return salt; } }
		
		private string appVersion;
		public string AppVersion { get { return appVersion; } }
		
		private string dbVersion;
		public string DBVersion { get { return dbVersion; } }
		
		private string deviceAlias;
		public string DeviceAlias { get { return deviceAlias; } }
		
		public SettingsDB ()
		{
			id = -1;
			deviceID = -1;
			organizationID = -1;
			schoolID = -1;
			rmsURL = "";
			deviceUID = "";
			salt = "";
			appVersion = "";
			dbVersion = "";
			deviceAlias = "";
		}
		
		public SettingsDB (int _id, int _deviceID, int _organizationID, int _schoolID, string _rmsURL, string _deviceUID, string _salt, string _appVersion, string _dbVersion, string _deviceAlias)
		{
			id = _id;
			deviceID = _deviceID;
			organizationID = _organizationID;
			schoolID = _schoolID;
			rmsURL = _rmsURL;
			deviceUID = _deviceUID;
			salt = _salt;
			appVersion = _appVersion;
			dbVersion = _dbVersion;
			deviceAlias = _deviceAlias;
		}
	}
}

