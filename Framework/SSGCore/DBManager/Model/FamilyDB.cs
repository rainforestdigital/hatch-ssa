using System;

namespace SSGCore
{
	public class FamilyDB: DBModel
	{
		private int id;
		public int ID{ get{ return id; } }
			
		private string name;
		public string Name { get { return name; } }
		
		public FamilyDB ()
		{
			id = -1;
			name = "";
		}
		
		public FamilyDB (int _id, string _name)
		{
			id = _id;
			name = _name;
		}
	}
}

