using System;

namespace SSGCore
{
	public class UserDB: DBModel
	{
		private int id;
		public int ID{ get{ return id; } set{ id=value; }}
		
		private int classroomID;
		public int ClassroomID { get { return classroomID; } }
		
		private string birthDate;
		public string BirthDate{ get{return birthDate; } }
		
		private int needSync;
		public int NeedSync{ get{return needSync; } }
		
		private string uid;
		public string UID{ get{return uid; } }
		
		private string customID;
		public string CustomID{ get{return customID; } }
		
		private string firstName;
		public string FirstName{ get{return firstName; } }
		
		private string lastName;
		public string LastName{ get{return lastName; } }
		
		private string picture;
		public string Picture{ get{return picture; } }
		
		private string pictureData;
		public string PictureData{ get{return pictureData; } }		
		
		private string bookmark;
		public string Bookmark{ get{return string.IsNullOrEmpty(bookmark) ? bookmark : bookmark.Replace("\"badges\":\"session_duration\"", "\"badges\":[],\"session_duration\""); } }
		
		private int profile;
		public int Profile{ get{return profile;} }
		
		public UserDB () : this( -1, -1, "", -1, "", "", "", "", "", "", ""){}
		
		public UserDB (int _id, int _classroomID, string _birthDate, int _needSync, string _firstName, string _lastName, string _customID, string _uid, string _picture, string _pictureData) :
			this(_id, _classroomID, _birthDate, _needSync, _firstName, _lastName, _customID, _uid, _picture, _pictureData, ""){}
		
		public UserDB (int _id, int _classroomID, string _birthDate, int _needSync, string _firstName, string _lastName, string _customID, string _uid, string _picture, string _pictureData, int _profile) :
			this(_id, _classroomID, _birthDate, _needSync, _firstName, _lastName, _customID, _uid, _picture, _pictureData, "", _profile){}
		
		public UserDB (int _id, int _classroomID, string _birthDate, int _needSync, string _firstName, string _lastName, string _customID, string _uid, string _picture, string _pictureData, string _bookmark) :
			this(_id, _classroomID, _birthDate, _needSync, _firstName, _lastName, _customID, _uid, _picture, _pictureData, _bookmark, 1){}
		
		public UserDB (int _id, int _classroomID, string _birthDate, int _needSync, string _firstName, string _lastName, string _customID, string _uid, string _picture, string _pictureData, string _bookmark, int _profile)
		{
			id = _id;
			classroomID = _classroomID;
			birthDate = _birthDate;
			needSync = _needSync;
			firstName = _firstName;
			lastName = _lastName;
			customID = _customID;
			uid = _uid;
			picture = _picture;
			pictureData = _pictureData;
			bookmark = _bookmark;
			profile = _profile;
		}
	}
}

