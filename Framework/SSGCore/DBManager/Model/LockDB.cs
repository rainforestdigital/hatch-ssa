using System;

namespace SSGCore
{
	public class LockDB : DBModel
	{
		private int id;
		public int ID{ get{ return id; } }
		
		private int userID;
		public int UserID{ get{ return userID; } }
		
		private int skillID;
		public int SkillID{ get{ return skillID; } }
		
		private int enabled;
		public int Enabled { get { return enabled; } set{ enabled = value;}}
		
		private int needSync;
		public int NeedSync { get { return needSync; } }
		
		private string uid;
		public string UID { get { return uid; } }
		
		private string created;
		public string Created { get { return created; } }
		
		public LockDB ()
		{
			id = -1;
			userID = -1;
			skillID = -1;
			enabled = -1;
			needSync = -1;
			uid = "";
			created = "";
		}
		
		public LockDB(int _id, int _userID, int _skillID, int _enabled, int _needSync, string _uid, string _created)
		{
			id = _id;
			userID = _userID;
			skillID = _skillID;
			enabled = _enabled;
			needSync = _needSync;
			uid = _uid;
			created = _created;
		}
	}
}

