using System;

namespace SSGCore
{
	public class LevelDB: DBModel
	{
		private int id;
		public int ID{ get{ return id; } }
		
		private string code;
		public string Code { get { return code; } }
		
		private string name;
		public string Name { get { return name; } }
		
		public LevelDB ()
		{
			id = -1;
			code = "";
			name = "";
		}
		
		public LevelDB (int _id, string _code, string _name)
		{
			id = _id;
			code = _code;
			name = _name;
		}
	}
}

