
using System;

namespace SSGCore
{
	public class SkillDB: DBModel
	{
		private int id;
		public int ID{ get{ return id; } }
		
		private int familyID;
		public int FamilyID{ get{ return familyID; } }
		
		private string name;
		public string Name { get { return name; } }
		
		public SkillDB ()
		{
			id = -1;
			familyID = -1;
			name = "";
		}
		
		public SkillDB (int _id, int _familyID, string _name)
		{
			id = _id;
			familyID = _familyID;
			name = _name;
		}
	}
}

