using System;

namespace SSGCore
{
	public class ClassroomDB: DBModel
	{
		private int id;
		public int ID{ get{ return id; } }
		
		private int teacherID;
		public int TeacherID{ get{return teacherID; } }
				
		private string name;
		public string Name{ get{return name; } }
		
		private string userDefinedID;
		public string UserDefinedID{ get{return userDefinedID; } }
		
		private string classroomPic;
		public string ClassroomPic{ get{ return classroomPic; } }
		
		private string teacherPic;
		public string TeacherPic { get { return teacherPic; } }
		
		private string themes;
		public string Themes{ get{return themes; } }
		
		public ClassroomDB ()
		{
			id = -1;
			teacherID = -1;
			name = "";
			userDefinedID = "";
			classroomPic = "";
			teacherPic = "";
			themes = "";
		}
		
		public ClassroomDB (int _id, int _teacherID, string _name, string _userDefinedID, string _classroomPic, string _teacherPic, string _themes)
		{
			id = _id;
			teacherID = _teacherID;
			name = _name;
			userDefinedID = _userDefinedID;
			classroomPic = _classroomPic;
			teacherPic = _teacherPic;
			themes = _themes;
		}
	}
}
