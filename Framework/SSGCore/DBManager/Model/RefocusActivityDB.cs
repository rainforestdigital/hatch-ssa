using System;
using System.Collections.Generic;
using System.Linq;

namespace SSGCore
{
	public class RefocusActivityDB: DBModel
	{
		private int id;
		public int ID{ get{ return id; } }
		
		private int userID;
		public int UserID{ get{ return userID; } }
		
		private string startDate;
		public string StartDate{ get{ return startDate; } }
		
		private string expirationDate;
		public string ExpirationDate{ get{ return expirationDate; } }
		
		private string familyIDs;
		public string FamilyIDs { get { return familyIDs; } }
		
		public bool HasValidExpiration()
		{
			DateTime expiration;
			if(!DateTime.TryParse(expirationDate, out expiration)) {
				DebugConsole.LogWarning("Could not parse date: {0}", expirationDate);
				return false;
			}
				
			return (expiration >= DateTime.UtcNow);
		}
		
		public void OutputFamilyIds(ICollection<int> outFamilyIds) {
			foreach(var family in familyIDs.Split(',')) {
				int familyId = 0;
				if(Int32.TryParse(family, out familyId)) {
					outFamilyIds.Add(familyId);
				}
			}
		}
		
		public RefocusActivityDB ()
		{
			id = -1;
			userID = -1;
			startDate = "";
			expirationDate = "";
			familyIDs = "";
		}
		
		public RefocusActivityDB (int _id, int _userID, string _startDate, string _expirationDate, string _familyIDs)
		{
			id = _id;
			userID = _userID;
			startDate = _startDate;
			expirationDate = _expirationDate;
			familyIDs = _familyIDs;
		}
	}
}

