
using System;

namespace SSGCore
{
	public class ThemeDB : DBModel
	{
		private int id;
		public int ID{ get{ return id; } }
		
		private string name;
		public string Name{ get{ return name; } }
		
		private string title;
		public string Title{ get{ return title; } }
		
		public ThemeDB ()
		{
			id = -1;
			name = "";
			title = "";
		}
		
		public ThemeDB(int _id, string _name, string _title)
		{
			id = _id;
			name = _name;
			title = _title;
		}
	}
}

