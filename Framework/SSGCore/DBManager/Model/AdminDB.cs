using System;

namespace SSGCore
{
	public class AdminDB: DBModel
	{
		private int id;
		public int ID{ get{ return id; } }
		
		private string name;
		public string Name { get { return name; } }
		
		private string organization;
		public string Organization { get { return organization; } }
		
		private string phone;
		public string Phone { get { return phone; } }
		
		private string email;
		public string Email { get { return email; } }
		
		public AdminDB ()
		{
			id = -1;
			name = "";
			organization = "";
			phone = "";
			email = "";
		}
		
		public AdminDB (int _id, string _name, string _organization, string _phone, string _email)
		{
			id = _id;
			name = _name;
			organization = _organization;
			phone = _phone;
			email = _email;
		}
	}
}

