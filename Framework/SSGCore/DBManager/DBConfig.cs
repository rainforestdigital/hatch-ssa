using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace SSGCore{
	
	[Serializable, XmlRoot(ElementName = "data")]
	public class DBConfig {
		public List<ThemeConfig> themes;
		public List<FamilyConfig> families;
		public List<SkillConfig> skills;
	}
	
	[Serializable]
	public class ThemeConfig {
		public 	int id;
		public string name;
		public string thumbnail;
		public string files;
		
		public ThemeConfig(){}
		
	}
	
	[Serializable]
	public class FamilyConfig {
		public 	int id;
		public string name;
		
		public FamilyConfig(){}
		
	}
	
	[Serializable]
	public class SkillConfig {
		
		public 	int id;
		public int family_id;
		public string name;
		public string file;
		public List<LevelConfig> levels;
		
		
		public SkillConfig(){}
		
	}
	
	[Serializable]
	public class LevelConfig {
		[XmlText]
		public string text { get; set; }
		
		public int weight;
	}
	
}