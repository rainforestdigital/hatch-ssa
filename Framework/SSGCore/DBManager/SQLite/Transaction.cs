using System;

namespace SSGCore
{
	public class Transaction : IDisposable
	{
		private SQLiteDB db;
		private bool succeeded;
		
		public Transaction (SQLiteDB db)
		{
			this.db = db;
			var query = new SQLiteQuery(db, "BEGIN TRANSACTION");
			query.Step();
			query.Release();
		}
		
		public void MarkSuccess() {
			succeeded = true;
		}
		
		
		#region IDisposable implementation
		public void Dispose ()
		{
			var query = new SQLiteQuery(db, succeeded ? "COMMIT TRANSACTION" : "ROLLBACK");
			query.Step();
			query.Release();
		}
		#endregion
	}
}

