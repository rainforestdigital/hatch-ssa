using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SQLBatch {
	
	List<string> statements = new List<string>();
	
	public SQLBatch()
	{
		statements = new List<string>();	
	}
	
	public void AddStatement(string sql)
	{
		statements.Add(sql);
		
	}
	
	public void RunStatements(SQLiteDB db)
	{
		SQLiteQuery qr;
		foreach(string s in statements)
		{
			qr = new SQLiteQuery(db, s);
			qr.Step();												
			qr.Release();     
		}
		
	}
}
