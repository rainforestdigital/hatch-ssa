using System;
using System.Linq;
using System.Collections.Generic;

namespace SSGCore
{
	public class Dataset
	{
		private Dictionary<int, Child> idToChild = new Dictionary<int, Child>();
		private Dictionary<Child, int> childToClass = new Dictionary<Child, int>();
		private Dictionary<int, Theme> themeCache = new Dictionary<int, Theme>();
		private List<Classroom> classrooms = new List<Classroom>();
		private List<int> childIdsToRemove = new List<int>();
		private ImageCache imageCache = new ImageCache();
		
		public List<Classroom> Classrooms
		{
			get { return classrooms; }
		}
		
		
		public Dataset (DBObject source)
		{
			PopulateThemes();
			
			foreach(var classroom in source.Classrooms) {
				AddClassroom(classroom);
			}
			
			foreach(var child in source.Users) {
				AddChild(child);
			}
		}
		
		public void Update(DBObject source, IDictionary<int, int> userIdChanges)
		{
			MigrateUserIds(userIdChanges);
			
			foreach(var classroom in source.Classrooms) {
				UpdateClassroom(classroom);
			}
			// TODO: more efficient way of removing classrooms
			for (int i = classrooms.Count - 1; i >= 0; i--)
			{
				var classroom = classrooms[i];
			    if(source.Classrooms.All( x => classroom.ID != x.ID)) {
					classroom.Invalidate();
					classrooms.RemoveAt(i);
				}
			}
			
			foreach(var childId in idToChild.Keys) {
				childIdsToRemove.Add(childId);
			}
			
			foreach(var child in source.Users) {
				UpdateChild(child);
				childIdsToRemove.Remove(child.ID);
			}
			
			foreach(var childId in childIdsToRemove) {
				RemoveChild (childId);
			}
			childIdsToRemove.Clear();
			imageCache.Sweep();
			imageCache.ReloadCache();
		}
		
		public void Destroy()
		{
			imageCache.Destroy();
		}

		void RemoveChild (int childId)
		{
			var child = idToChild[childId];
			var classroomId = childToClass[child];
			var classroom = ClassroomForId(classroomId);
			if(classroom != null) {
				classroom.Children.Remove(child);
			}
			idToChild.Remove(childId);
			childToClass.Remove(child);
		}

		void MigrateUserIds (IDictionary<int, int> userIdChanges)
		{
			if(userIdChanges == null || userIdChanges.Count == 0) {
				return;
			}
			
			// copy id mapping in case ids are mapped onto existing ids
			var initialMapping = new Dictionary<int, Child>(idToChild);
			foreach(var change in userIdChanges) {
				var startId = change.Key;
				var endId = change.Value;
				if(!initialMapping.ContainsKey(startId)) {
					DebugConsole.LogWarning("Migrating unknown user id: {0}", startId);
					continue;
				}
				
				DebugConsole.Log("Migrating id {0} to {1}", startId, endId);
				var child = initialMapping[startId];
				child.ID = endId;
				idToChild[endId] = child;
				if((startId != endId) && idToChild.ContainsKey(startId) && idToChild[startId] == child) {
					DebugConsole.Log("Removing old reference to {0}", startId);
					idToChild.Remove(startId);
				}
			}
		}
		
		ImageSource ImageForPath(string path)
		{
			imageCache.Mark(path);
			return new CacheImageSource(imageCache, path);
		}
		
		bool IsValidImage(string path)
		{
			return path != null && !path.EndsWith("child_default_pic.png");
		}
		
		void AddClassroom (ClassroomDB c)
		{
			var newClass = new Classroom(c.ID,
				c.Name, 
				ImageForPath(c.ClassroomPic), // classroom image
				ImageForPath(c.TeacherPic), // teacher image
				new List<User>(),
				ThemesFromString(c.Themes));
			
			classrooms.Add(newClass);
		}
		
		void UpdateClassroom (ClassroomDB c)
		{
			var classroom = ClassroomForId(c.ID);
			if(classroom == null) {
				AddClassroom(c);
				return;
			}
			
			classroom.Name = c.Name;
			classroom.ImageSource = ImageForPath(c.ClassroomPic);
			classroom.TeacherImageSource = ImageForPath(c.TeacherPic);
			ThemesFromString(c.Themes, classroom.Themes);
		}
		
		void UpdateChild (UserDB user)
		{
			if(!idToChild.ContainsKey(user.ID)) {
				AddChild(user);
				return;
			}
			
			var child = idToChild[user.ID];
			child.UpdateFrom(user);
			child.ImageSource = ImageForPath(user.Picture);
			child.HasImage = IsValidImage(user.Picture);
			
			var oldClassroomId = childToClass[child];
			if(oldClassroomId != user.ClassroomID) {
				//remove from old class
				var oldClass = ClassroomForId(oldClassroomId);
				if(oldClass != null) {
					oldClass.Children.Remove(child);
				}
				
				var newClass = ClassroomForId(user.ClassroomID);
				if(newClass != null) {
					newClass.Children.Add(child);
				}
				childToClass[child] = user.ClassroomID;
			}
		}
		
		void AddChild (UserDB user)
		{
			var newChild = new Child(user.ID,
				user.FirstName,
				user.LastName,
				ImageForPath(user.Picture),
				user.BirthDate,
				user.CustomID,
				user.UID,
				user.Bookmark,
				user.Profile
			);
			
			
			newChild.HasImage = IsValidImage(user.Picture);
			childToClass[newChild] = user.ClassroomID;
			idToChild[user.ID] = newChild;
			var classroom = ClassroomForId(user.ClassroomID);
			if(classroom != null) {
				classroom.Children.Add(newChild);
			}
		}
		
		private Classroom ClassroomForId(int classroomId)
		{
			return classrooms.FirstOrDefault(c => c.ID == classroomId);
		}
		
		private ICollection<Theme> ThemesFromString(string themeString)
		{
			return ThemesFromString(themeString, new List<Theme>());
		}
		
		private ICollection<Theme> ThemesFromString(string themeString, ICollection<Theme> result)
		{
			result.Clear();
			foreach(string s in themeString.Split(',')) {
				int id = 0;
				if(!Int32.TryParse(s, out id)) {
					DebugConsole.LogError("Failed to parse theme id: {0}", s);
					continue;
				}
				if(!themeCache.ContainsKey(id)) {
					DebugConsole.LogError("Unknown theme id: {0}", id);
					continue;
				}
				result.Add(themeCache[id]);	
			}
			return result;
		}
		
		private void PopulateThemes()
		{
			themeCache[1] = new Theme("Travel");
			themeCache[2] = new Theme("Garden");
			themeCache[3] = new Theme("Safari");
		}
	}
}

