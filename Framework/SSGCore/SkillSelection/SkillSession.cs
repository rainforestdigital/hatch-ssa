using System;
using HatchFramework;
using System.Collections.Generic;
using UnityEngine;

namespace SSGCore
{
	public class SkillSession
	{
		private SkillSessionContext context;
		private Dictionary<string, SkillLevel> usedSkills = new Dictionary<string, SkillLevel>();
		
		public SkillSession (SkillSessionContext context)
		{
			this.context = context;
			DebugConsole.Instance.RegisterBehaviour(this);
		}
		
		public SkillLevelPair ChooseFirstSkill()
		{
			// If there is a recently unlocked skill, use that
			if(context.RecentlyUnlockedSkill != null) {
				var skill = context.RecentlyUnlockedSkill;
				context.ClearRecentlyUnlockedSkill();
				return new SkillLevelPair(skill, context.GetLevel(skill));
			}
			
			var skills = UncompletedSkills().ToListUtil();
			if(skills.Count < 1)
				skills = UnmasteredSkills().ToListUtil();
			
			if(skills.Count < 1)
				skills = CandidateSkills().ToListUtil();
			
			if(skills.Count > 0) {
				return ChooseRandomSkill(skills);
			} else {
				return null;
			}
		}
		
		public SkillLevelPair ChooseSkillForResult(Skill skill, SkillLevel level, float result) {
			context.Update();
			if(context.RecentlyUnlockedSkill != null) {
				context.ClearRecentlyUnlockedSkill();
				Reset();
			}
			
			usedSkills[skill.linkage] = level;
			
			ICollection<Skill> skills;
			if(result >= SkillProgress.ProficiencyThreshold) {
				skills = ChooseFromSameFamily(skill, level);
			} else {
				skills = ChooseFromDifferentFamily(skill);
				if( skills.Count < 1 )
					skills = ChooseFromDifferentFamilyNoLimit(skill);
			}
			
			if(skills.Count > 0) {
				return ChooseRandomSkill(skills);
			} else {
				// If no skills found, reset and choose as if starting session
				Reset();
				return ChooseFirstSkill();
			}
		}
		
		public void UpdateProgress(Skill skill, SkillLevel level, float result) {
			context.Update();
			if(result >= SkillProgress.ProficiencyThreshold) {
				Debug.Log("Increasing Skill");
				IncreaseSkill (skill, level, result);
			}
			else {
				Debug.Log("Decreasing Skill");
				PenalizeSkill (skill, level);
			}
		}
		
		void PenalizeSkill (Skill skill, SkillLevel level)
		{

			int failureCount = context.IncrementFailureCount(skill);
			DebugConsole.Log( "context failureCount: {0}", failureCount );

			if(failureCount >= 2) {
				context.ResetFailureCount(skill);

				if(level == SkillLevel.Tutorial || level == SkillLevel.Emerging) {
					DebugConsole.Log("Locking skill: {0}", skill);
					context.LockSkill(skill);
				} else {
					DebugConsole.Log("Degrading skill: {0}", skill);
					SkillProgress progress = SkillProgress.Minimum;
					progress = new SkillProgress(skill.LevelBefore(level), 0);
					context.SetProgress(skill, progress);
				}
			}
		}

		void IncreaseSkill (Skill skill, SkillLevel level, float result)
		{
			SkillProgress progress = SkillProgress.Minimum;
			if(result >= SkillProgress.MasteryThreshold && !skill.IsLastLevel(level)) {
				progress = new SkillProgress(skill.LevelAfter(level), 0);
				context.ResetFailureCount(skill);
			} else {
				var current = context.GetProgress(skill);
				progress = new SkillProgress(level, Math.Max(result, current.Progress));
			}
			context.SetProgress(skill, progress);
		}
		
		/// <summary>
		/// Validate if a skill is in the usedSkills dictionary
		/// </summary>
		/// <param name='skill'>
		/// If set to <c>true</c> skill.
		/// </param>
		private bool Used(Skill skill)
		{
			return usedSkills.ContainsKey(skill.linkage);
		}
		
		private bool IsCandidateSkill(Skill skill)
		{
			return context.IsValidForRefocus(skill)
				&& context.MeetsPrerequisite(skill)
				&& !context.IsLocked(skill)
				&& !Used(skill);
		}
		
		
		
		private IEnumerable<Skill> CandidateSkills()
		{
			return context.AllSkills.Filter(IsCandidateSkill);
		}
		
		private bool Uncompleted(Skill skill)
		{
			return context.GetLevel(skill) != SkillLevel.Completed;
		}
		
		private bool Unmastered(Skill skill)
		{
			return context.GetProgress(skill).Level != SkillLevel.Completed || context.GetProgress(skill).Progress < SkillProgress.MasteryThreshold;
		}
		
		private IEnumerable<Skill> UncompletedSkills()
		{
			return CandidateSkills().Filter(Uncompleted);
		}
		
		private IEnumerable<Skill> UnmasteredSkills()
		{
			return CandidateSkills().Filter(Unmastered);
		}
		
		private SkillLevelPair ChooseRandomSkill(ICollection<Skill> skills)
		{
			var skill = skills.SelectWeighted((Skill s) => s.WeightAtLevel(context.GetLevel(s)));
			return new SkillLevelPair(skill, context.GetLevel(skill));
		}
		
		private Skill SkillAfter(List<Skill> list, Skill after)
		{
			if(list.Count == 0) {
				return null;
			}
			if(list.Count == 1) {
				return list[0];
			}
			
			List<Skill> familySkills = context.AllSkills.Filter( (Skill s) => s.Family.ID == after.Family.ID ).ToListUtil();
			
			//Sort so that skill you're looking at is first
			//then each skill in the list has the one before as a predecessor
			familySkills.Sort((x, y) => {
				if( x.FirstInFamily )
					return -1;
				if( y.FirstInFamily )
					return 1;
				
				Skill pre = y.Predecessor;
				while( pre.linkage != x.linkage && !pre.FirstInFamily )
					pre = pre.Predecessor;
				
				if( pre.linkage == x.linkage )
					return -1;
				else
					return 1;
			});
			
			while( familySkills[0].linkage != after.linkage )
			{
				Skill sk = familySkills[0];
				familySkills.RemoveAt(0);
				familySkills.Add( sk );
			}
			
			familySkills.RemoveAt(0);
			
			int i;		
			for( i = 0; i < familySkills.Count; i++ )
			{
				//check each skill in succession to se if it's in the list of available skills
				if( list.Filter( (Skill s) => s.linkage == familySkills[i].linkage ).ToListUtil().Count > 0 )
				{
					break;
				}
			}
			
			return i < familySkills.Count ? familySkills[i] : null;
		}
		
		private ICollection<Skill> ChooseFromSameFamily(Skill skill, SkillLevel level)
		{			
			if(level == SkillLevel.Tutorial) {
				// if completed tutorial, play skill again
				return new List<Skill>() { skill };
			}
			else {
				ICollection<Skill> resultList;
				
				SkillFamily targetFamily = skill.Family;
				List<Skill> skillsInFamily = UncompletedSkills()
					.Filter( (Skill s) => s.Family.ID == targetFamily.ID  )
					.ToListUtil();
				
				// choose next skill in family unless there are none left
				Skill result = SkillAfter(skillsInFamily, skill);
				if(result != null) {
					resultList = new List<Skill>() { result };
				} else {
					resultList = ChooseFromDifferentFamily(skill);
				}
				
				//If there are no acceptable uncompleted skills, search for completed skills that aren't mastered
				if( resultList != null && resultList.Count > 0 )
				{
					return resultList;
				}
				else
				{
					skillsInFamily = UnmasteredSkills()
					.Filter( (Skill s) => s.Family.ID == targetFamily.ID  )
					.ToListUtil();
				
					// choose next skill in family unless there are none left
					result = SkillAfter(skillsInFamily, skill);
					if(result != null) {
						resultList = new List<Skill>() { result };
					} else {
						resultList = ChooseFromDifferentFamilyNoLimit(skill);
					}
					
					return resultList;
				}
			}
		}
					
		private ICollection<Skill> ChooseFromDifferentFamily(Skill skill)
		{
			return UncompletedSkills()
					.Filter((Skill s) => s.Family.ID != skill.Family.ID)
					.ToListUtil();
		}
		
		private ICollection<Skill> ChooseFromDifferentFamilyNoLimit(Skill skill)
		{
			return UnmasteredSkills()
					.Filter((Skill s) => s.Family.ID != skill.Family.ID)
					.ToListUtil();
		}
		
		private void Reset()
		{
			usedSkills.Clear();
		}
		
		
		
		private bool DebugPrereqSkill(Skill skill)
		{
			return context.IsValidForRefocus(skill)
				&& context.MeetsPrerequisite(skill);
		}
		
		[DebugConsoleMethod("uncompleted_skills", "-list all uncompleted skills")]
		public void ListUncompletedSkills(){
		
			
				ListDebugSkill("Uncompleted Skills:", UncompletedSkills());
			
		}
		
		[DebugConsoleMethod("pre_req_skills", "-list all skills meeting pre-reqs")]
		public void ListPrereqSkills(){
		
			ListDebugSkill("Prereqs: ", context.AllSkills.Filter(DebugPrereqSkill));
		}
		
		private void ListDebugSkill(string header, IEnumerable<Skill> skills){
			string ret = header+"\n";
			foreach(Skill s in skills){
				ret += s.ID+" "+s.linkage+"\n";
			}
			DebugConsole.Log(DebugConsole.LogLevel.DEBUG, "", ret);
		}
	}
}

