using System;
using System.Collections.Generic;

namespace SSGCore
{
	// An abstraction for the external, mutable, persistent state
	// used by the SkillSession. Any user-specific lookups or
	// updates are made here.
	public interface SkillSessionContext
	{
		// TODO: evaluate whether this is the most appropriate place to put
		// this. Since it is static data, it could be passed directly to the
		// session.
		ICollection<Skill> AllSkills { get; }
		
		Skill RecentlyUnlockedSkill { get; }
		void ClearRecentlyUnlockedSkill();
		
		// called before choosing skill
		void Update();
		
		
		// since these are used together, they could be combined
		bool IsLocked(Skill skill);
		bool MeetsPrerequisite(Skill skill);
		bool IsValidForRefocus(Skill skill);
		
		SkillProgress GetProgress(Skill skill);
		SkillLevel GetLevel(Skill skill);
		bool hasMasteredCompleted (Skill skill);
		
		void SetProgress(Skill skill, SkillProgress progress);
		void LockSkill(Skill skill);
		int IncrementFailureCount(Skill skill);
		void ResetFailureCount(Skill skill);
	}
}

