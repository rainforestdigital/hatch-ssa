using System;
using System.Collections.Generic;

namespace SSGCore
{
	public class TestSkillContext : SkillSessionContext
	{
		private ICollection<Skill> lockedSkills = new List<Skill>();
		private ICollection<SkillFamily> refocusFamilies = new List<SkillFamily>();
		private Dictionary<Skill, SkillProgress> skillProgress = new Dictionary<Skill, SkillProgress>();
		private Dictionary<Skill, int> failureCount = new Dictionary<Skill, int>();
		private ICollection<Skill> allSkills;
		
		public TestSkillContext ()
		{
			var skillBuilder = MockSkillBuilder.CreateForTest();
			allSkills = skillBuilder.Skills;
		}
		
		public ICollection<Skill> AllSkills
		{
			get { return allSkills; }
		}
		
		public bool IsLocked(Skill skill)
		{
			return lockedSkills.Contains(skill);
		}
		
		public bool MeetsPrerequisite(Skill skill)
		{
			return skill.FirstInFamily || GetProgress(skill.Predecessor) > SkillProgress.NextSkillThreshold;
		}
		
		public bool IsValidForRefocus(Skill skill)
		{
			return refocusFamilies.Count == 0 || refocusFamilies.Contains(skill.Family);
		}
		
		public SkillProgress GetProgress(Skill skill)
		{
			SkillProgress progress = SkillProgress.Minimum;
			skillProgress.TryGetValue(skill, out progress);
			return progress;
		}
		
		public bool hasMasteredCompleted (Skill skill)
		{
			return false;
		}
		
		public SkillLevel GetLevel(Skill skill)
		{
			return GetProgress(skill).Level;
		}
		
		public void SetProgress(Skill skill, SkillProgress progress)
		{
			skillProgress[skill] = progress;
		}
		
		public void LockSkill(Skill skill)
		{
			lockedSkills.Add(skill);
		}
		
		public int IncrementFailureCount(Skill skill)
		{
			int startValue = 0;
			failureCount.TryGetValue(skill, out startValue);
			startValue++;
			failureCount[skill] = startValue;
			return startValue;
		}
		
		public void ResetFailureCount(Skill skill)
		{
			failureCount[skill] = 0;
		}
		
		public Skill RecentlyUnlockedSkill { get { return null; } }
		public void ClearRecentlyUnlockedSkill()
		{
		}
		
		public void Update()
		{
		}
	}
}

