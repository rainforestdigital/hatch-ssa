using System;

namespace SSGCore
{
	public class SkillLevelPair
	{
		private Skill skill;
		private SkillLevel level;
		
		public Skill Skill { get { return skill; } }
		public SkillLevel Level { get { return level; } }
		
		public SkillLevelPair(Skill skill, SkillLevel level)
		{
			this.skill = skill;
			this.level = level;
		}
		
		public override string ToString()
		{
			return skill + ":" + level;
		}
	}
}

