using System;

namespace SSGCore
{
	public struct SkillProgress
	{
		private SkillLevel level;
		private float progress;
		
		public SkillLevel Level { get { return level; } }
		public float Progress { get { return progress; } }
		
		public const float ProficiencyThreshold = 0.6f;
		public const float MasteryThreshold = 0.8f;
		
		public static readonly SkillProgress Minimum = new SkillProgress(SkillLevel.Tutorial, 0);
		public static readonly SkillProgress NextSkillThreshold = new SkillProgress(SkillLevel.Emerging, ProficiencyThreshold);
		
		public SkillProgress(SkillLevel level, float progress)
		{
			this.level = level;
			this.progress = progress;
		}
		
		public static bool operator <(SkillProgress a, SkillProgress b)
		{
			if(a.Level != b.Level) {
				return a.Level < b.Level;
			}
			
			return a.Progress < b.Progress;
		}
		
		public static bool operator >(SkillProgress a, SkillProgress b)
		{
			return b < a;
		}
		
		public static bool operator <=(SkillProgress a, SkillProgress b)
		{
			return !(b < a);
		}
		
		public static bool operator >=(SkillProgress a, SkillProgress b)
		{
			return !(a < b);
		}
	}
}

