using System;

namespace SSGCore
{
	public class TeacherSkillContext : SkillSessionContext
	{
		SkillSessionContext context;
		
		public TeacherSkillContext (DBObject dbObject, Child user)
		{
			context = new DatabaseSkillContext(dbObject, user);
		}

		#region SkillSessionContext implementation
		
		public bool IsLocked (Skill skill)
		{
			// hack to exclude unimplemented skills
			return !SessionController.HACK_IsSkillImplemented(skill);
		}
		
		public void LockSkill (Skill skill)
		{
			// don't lock in teacher mode
		}
		
		public void ClearRecentlyUnlockedSkill ()
		{
			context.ClearRecentlyUnlockedSkill();
		}

		public void Update ()
		{
			context.Update();
		}

		public bool MeetsPrerequisite (Skill skill)
		{
			return context.MeetsPrerequisite(skill);
		}

		public bool IsValidForRefocus (Skill skill)
		{
			return context.IsValidForRefocus(skill);
		}

		public SkillProgress GetProgress (Skill skill)
		{
			return context.GetProgress(skill);
		}
		
		public bool hasMasteredCompleted (Skill skill)
		{
			return context.hasMasteredCompleted(skill);
		}

		public SkillLevel GetLevel (Skill skill)
		{
			return context.GetLevel(skill);
		}

		public void SetProgress (Skill skill, SkillProgress progress)
		{
			context.SetProgress(skill, progress);
		}

		public int IncrementFailureCount (Skill skill)
		{
			// don't regress in teacher mode
			return 0;
		}

		public void ResetFailureCount (Skill skill)
		{
			context.ResetFailureCount(skill);
		}

		public System.Collections.Generic.ICollection<Skill> AllSkills {
			get {
				return context.AllSkills;
			}
		}

		public Skill RecentlyUnlockedSkill {
			get {
				return context.RecentlyUnlockedSkill;
			}
		}
		#endregion
	}
}

