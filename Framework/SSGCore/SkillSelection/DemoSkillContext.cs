using System;
using System.Collections.Generic;

namespace SSGCore
{
	public class DemoSkillContext : SkillSessionContext
	{
		private Dictionary<string, SkillProgress> skillProgress = new Dictionary<string, SkillProgress>();
		private Dictionary<Skill, int> failureCount = new Dictionary<Skill, int>();
		private ICollection<Skill> allSkills;
		private SkillFamily targetFamily;
		private Skill initialSkill;
		
		public DemoSkillContext (Child demoChild)
		{
			var skillBuilder = MockSkillBuilder.CreateForTest();
			allSkills = skillBuilder.Skills;
			
			DebugConsole.Log("ID: {0}", demoChild.ID);
			var initialAssignment = StaticData.GetDemoAssignment(demoChild.ID, allSkills);
			initialSkill = initialAssignment.Skill;
			DebugConsole.Log("Initial Skill: {0}", initialSkill);
			targetFamily = initialSkill.Family;
			SetProgress(initialSkill, new SkillProgress(initialAssignment.Level, 0));
			
			// set prerequisites to next skill threshold
//			for(var skill = initialSkill.Predecessor; skill != null; skill = skill.Predecessor) {
//				SetProgress(skill, SkillProgress.NextSkillThreshold); 
//			}
		}
		
		public ICollection<Skill> AllSkills
		{
			get { return allSkills; }
		}
		
		public bool IsLocked(Skill skill)
		{
			// do not lock in demo mode
			return !SessionController.HACK_IsSkillImplemented(skill);
		}
		
		public bool MeetsPrerequisite(Skill skill)
		{
			return skill.FirstInFamily || GetProgress(skill.Predecessor) > SkillProgress.NextSkillThreshold;
		}
		
		public bool IsValidForRefocus(Skill skill)
		{
			return skill.Family == targetFamily;
		}
		
		public SkillProgress GetProgress(Skill skill)
		{
			if( skillProgress.ContainsKey( skill.linkage ) )
				return skillProgress[skill.linkage];
			
			return SkillProgress.Minimum;
		}
		
		public bool hasMasteredCompleted (Skill skill)
		{
			return false;
		}
		
		public SkillLevel GetLevel(Skill skill)
		{
			return GetProgress(skill).Level;
		}
		
		public void SetProgress(Skill skill, SkillProgress progress)
		{
			if( progress.Progress >= SkillProgress.MasteryThreshold )
				skillProgress[skill.linkage] = new SkillProgress( SkillLevelUtil.FromId( SkillLevelUtil.FromId( progress.Level ) + 1 ), 0 );
			else
				skillProgress[skill.linkage] = progress;
		}
		
		public void LockSkill(Skill skill)
		{
		}
		
		public int IncrementFailureCount(Skill skill)
		{
			int startValue = 0;
			failureCount.TryGetValue(skill, out startValue);
			startValue++;
			failureCount[skill] = startValue;
			return startValue;
		}
		
		public void ResetFailureCount(Skill skill)
		{
			failureCount[skill] = 0;
		}
		
		public Skill RecentlyUnlockedSkill { 
			get { 
				return initialSkill; 
			} 
		}
		
		public void ClearRecentlyUnlockedSkill()
		{
			initialSkill = null;
		}
		
		public void Update()
		{
		}
	}
}

