using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using HatchFramework;

namespace SSGCore
{
	public class DatabaseSkillContext : SkillSessionContext
	{
		private Dictionary<int, SkillProgress> skillProgress = new Dictionary<int, SkillProgress>();
		private ICollection<Skill> allSkills;
		private List<int> refocusFamilyIds = new List<int>();
		
		protected DBObject dbObject;
		private Child child;
		
		List<SessionDB> sessions;
		List<LockDB> locks;
		
		public DatabaseSkillContext (DBObject dbObject, Child user)
		{
			allSkills = dbObject.LoadSkills();
			
			this.dbObject = dbObject;
			this.child = user;
			Update();
		}
		
		public void Update()
		{
			// Refreshing data each time is not the most efficient
			// way of doing it, but it is relatively easy to
			// reason about.
			dbObject.currentUserID = child.ID;
			dbObject.LoadSessions();
			dbObject.LoadLocks();
			dbObject.LoadRefocusActivities();
			
			sessions = dbObject.Sessions;
			locks = dbObject.Locks;
			PopulateRefocusIds();
			skillProgress.Clear();

		}

		void PopulateRefocusIds ()
		{
			refocusFamilyIds.Clear();
			
			foreach(var refocus in dbObject.RefocusActivities) {
				if(refocus.HasValidExpiration()) {
					refocus.OutputFamilyIds(refocusFamilyIds);
				}
			}
		}

		#region SkillSessionContext implementation
		public void ClearRecentlyUnlockedSkill ()
		{
			//if teacher unlocks skill then go to that skill specifically - remove from the list...
			throw new NotImplementedException ();
		}


		public bool IsLocked (Skill skill)
		{
			string langTag = SkillImplementationUtil.getLangTagforSkill( skill.linkage, LanguageProfileUtility.FromId(child.Profile) );
			// hack to exclude unimplemented skills
			if( langTag.Contains( "BAD" ) ) {
				return true;
			}
			
			//return if there are locks on this skill
			if( langTag == SkillImplementationUtil.ENG_LANG_TAG )
				return locks.Any( x => x.SkillID == skill.ID && x.Enabled == 1 );
			else
				return locks.Any( x => x.SkillID == skill.SPN_ID && x.Enabled == 1 );
		}

		public bool MeetsPrerequisite (Skill skill)
		{
			return skill.FirstInFamily || GetProgress(skill.Predecessor) > SkillProgress.NextSkillThreshold;
		}
		
		public bool IsValidForRefocus (Skill skill)
		{
			//true if no refocus
			//if refocus then true if skill family is one of the refocus families
			//false if not in refocus families
			if(refocusFamilyIds.Count == 0)
				return true;
			
			return refocusFamilyIds.Contains(skill.Family.ID) || refocusFamilyIds.Contains(skill.Family.SPN_ID);
		}
		
		private SkillProgress CalculateProgress(Skill skill)
		{
			List<SessionDB> sessionsForSkill;
			
			if( SkillImplementationUtil.getLangTagforSkill( skill.linkage, LanguageProfileUtility.FromId( child.Profile ) ) == SkillImplementationUtil.ENG_LANG_TAG )
				sessionsForSkill = sessions.Where(x => x.SkillID == skill.ID )
											.Take(2).ToList();
			else
				sessionsForSkill = sessions.Where(x => x.SkillID == skill.SPN_ID )
											.Take(2).ToList();
			
			// no sessions for the skill
			if(sessionsForSkill.Count == 0)
			{
				return new SkillProgress(SkillLevel.Tutorial, 0);
			}
			
			SessionDB session = sessionsForSkill[0];
			float result = session.Percent;
			SkillLevel level = SkillLevelUtil.GetLevel(session.SkillLevel);
			
			// if result was above mastery threshold, and there is
			// a next skill level, progress to next level
			if(result >= SkillProgress.MasteryThreshold )
			{
				if(!skill.IsLastLevel(level)) 
					return new SkillProgress(skill.LevelAfter(level), 0);
				else
					return new SkillProgress(level, result);
			}
			
			// If result was proficient, or too low to regress
			// stay the same
			if(result >= SkillProgress.ProficiencyThreshold
				|| level <= SkillLevel.Emerging
				|| sessionsForSkill.Count < 2) {
				return new SkillProgress(level, result);
			}
			
			SessionDB prevSession = sessionsForSkill[1];
			float prevResult = prevSession.Percent;
			SkillLevel prevLevel = SkillLevelUtil.GetLevel(prevSession.SkillLevel);

			DebugConsole.Log( "prevLevel: {0}, sessionsForSkill[0]: {1}", prevLevel, sessionsForSkill[0] );

			// if both failed, and have same skill level, regress
			if( level == prevLevel
				&& prevResult < SkillProgress.ProficiencyThreshold ) {
				DebugConsole.Log ("THE SKILL IS REGRESSED: " + skill.linkage + " " + level);
				return new SkillProgress(skill.LevelBefore(level), 1);
			}
			
			return new SkillProgress(level, result);
		}

		public SkillProgress GetProgress (Skill skill)
		{
			if(!skillProgress.ContainsKey(skill.ID)) {
				skillProgress[skill.ID] = CalculateProgress(skill);
			}
			
			return skillProgress[skill.ID];
		}

		public SkillLevel GetLevel (Skill skill)
		{
			return GetProgress(skill).Level;
		}
		
		public bool hasMasteredCompleted (Skill skill)
		{
			return GetProgress(skill).Level == SkillLevel.Completed && GetProgress(skill).Progress >= SkillProgress.MasteryThreshold;
		}

		public void SetProgress (Skill skill, SkillProgress progress)
		{
			//does nothing here...
			Update();
		}

		/// <summary>
		/// Locks the skill.  If exists sets enabled to true, else inserts new lock
		/// </summary>
		/// <param name='skill'>
		/// Skill.
		/// </param>
		public void LockSkill (Skill skill)
		{
			int sID = SkillImplementationUtil.getLangTagforSkill( skill.linkage, LanguageProfileUtility.FromId( child.Profile ) ) == SkillImplementationUtil.SPN_LANG_TAG ? skill.SPN_ID : skill.ID;
			LockDB lck = locks.FirstOrDefault( x => x.SkillID == sID );
			if(lck != null){
				lck.Enabled = 1;
				dbObject.UpdateLock(lck);
				DebugConsole.Log("Updating Lock");
			} else {
				lck = new LockDB(0, child.ID, sID, 1, 1, child.UID, null);
				dbObject.InsertLock(lck);
				DebugConsole.Log("Inserting Lock");
			}

			Update();

		}

		/// <summary>
		/// Get the failure count for the current skill
		/// </summary>
		/// <returns>
		/// The failure count.
		/// </returns>
		/// <param name='skill'>
		/// Skill.
		/// </param>
		public int IncrementFailureCount (Skill skill)
		{
			//Look at last 2 sessions - if session levels are the same, and are failing, increment failure count
			List<SessionDB> sessionsForSkill;
			if( SkillImplementationUtil.getLangTagforSkill( skill.linkage, LanguageProfileUtility.FromId( child.Profile ) ) == SkillImplementationUtil.ENG_LANG_TAG )
				sessionsForSkill = sessions.Where( x => x.SkillID == skill.ID )
											.Take(2).ToList();
			else
				sessionsForSkill = sessions.Where( x => x.SkillID == skill.SPN_ID )
											.Take(2).ToList();
			
			int failureCount = 0;
			int currentLevelID = -1;
			
			foreach(var session in sessionsForSkill) {
				if(currentLevelID >= 0 && session.SkillLevelID != currentLevelID)
					break;
				
				currentLevelID = session.SkillLevelID;
				
				if(session.Percent <= SkillProgress.ProficiencyThreshold)
					failureCount++;
			}
			
			return failureCount;
		}

		public void ResetFailureCount (Skill skill)
		{
			//do nothing - database shows failure count
		}

		public ICollection<Skill> AllSkills 
		{
			get {
				return allSkills;
			}
		}

		public Skill RecentlyUnlockedSkill
		{
			get {
				//return unlocked skill
				DebugConsole.LogWarning("Not checking for recently unlocked skills");
				return null;
			}
		}
		#endregion
	}
}

