using pumpkin.display;
using pumpkin.events;
using UnityEngine;
using System.Collections.Generic;
namespace SSGCore
{
	public class ParticleSystem2D : DisplayObjectContainer
	{
		public static string FADE_IN_COMPLETE = "fadeInComplete";
		public static string FADE_OUT_COMPLETE = "fadeOutComplete";

		private bool _fadingIn = false;
		public bool fadingIn {
			get { return _fadingIn; }
			set { 
				if(value == true && _fadingIn == false) {
					this.alpha = 0f;
				}
				_fadingIn = value; 
				_fadingOut = false;
			}
		}
		private bool _fadingOut = false;
		public bool fadingOut {
			get { return _fadingOut; }
			set { 
				if(value == true && _fadingOut == false) {
					this.alpha = 1f;
				}
				_fadingOut = value; 
				_fadingIn = false;
			}
		}
		public IParticle2DController controller;
		private LinkedList<Particle2D> particles;
		private float deviceScale;

		public ParticleSystem2D (float deviceScale)
		{
			this.deviceScale = deviceScale;
		}

		public ParticleSystem2D ()
		{
			this.deviceScale = 1f;
		}

		public void Init(DisplayObject particle, IParticle2DController controller) {
			this.controller = controller;
			particles = new LinkedList<Particle2D>(); 

			Sprite display = new Sprite ();
//			particle.x -= particle.width * 0.5f;
//			particle.y -= particle.height * 0.5f;
			display.addChild (particle);
			Particle2D p = new Particle2D(display);
			controller.Layout(p, 0, deviceScale);
			addChild(p.display);
			particles.AddLast(p);
		}

		public void Init(float numParticles, Rect displayRect, Color displayColor, IParticle2DController controller) {
			this.controller = controller;
			particles = new LinkedList<Particle2D>(); 
			
			for (int i = 0; i < numParticles; i++)
			{
				Sprite display = new Sprite();
				display.graphics.drawSolidRectangle(displayColor, displayRect.x, displayRect.y, displayRect.width, displayRect.height);
				Particle2D p = new Particle2D(display);
				controller.Layout(p, i, deviceScale);
				addChild(p.display);
				particles.AddLast(p);
			}
		}

		public void Init(float numParticles, Texture2D particleTexture, IParticle2DController controller) {
			Init (numParticles, particleTexture, controller, new Rect(0, 0, particleTexture.width, particleTexture.height));
		}

		public void Init(float numParticles, Texture2D particleTexture, IParticle2DController controller, Rect particleRect) {
			this.controller = controller;
			particles = new LinkedList<Particle2D>(); 

			for (int i = 0; i < numParticles; i++)
			{
				Particle2D p;

				p = new Particle2D(particleTexture, particleRect);
				controller.Layout(p, i, deviceScale);
				addChild(p.display);
				particles.AddLast(p);
			}
		}

		public void Play(bool fadeIn)
		{
			this.fadingIn = fadeIn;
			addEventListener (CEvent.ENTER_FRAME, frameHandler);
		}

		public void Play() 
		{
			addEventListener (CEvent.ENTER_FRAME, frameHandler);
		}

		public void Pause() 
		{
			removeEventListener (CEvent.ENTER_FRAME, frameHandler);
		}

		private void frameHandler(CEvent e)
		{
			Update (Time.deltaTime);
		}

		public void Update(float delta) 
		{
			if (_fadingIn) {
				this.alpha += delta * 2f;
				if (this.alpha >= 1f) {
					this.alpha = 1f;
					fadingIn = false;
					dispatchEvent(new CEvent(FADE_IN_COMPLETE));
				}
			}
			
			if (_fadingOut) {
				this.alpha -= delta * 2f;
				if (this.alpha <= 0f) {
					this.alpha = 0f;
					fadingOut = false;
					dispatchEvent(new CEvent(FADE_OUT_COMPLETE));
				}
			}

			foreach (Particle2D p in particles) {
				controller.Update(p, delta, deviceScale);
			}
		
		}
		
		public void Dispose()
		{
			if( particles != null )
			{
				foreach( Particle2D p in particles )
					p.Dispose();
			}
			particles.Clear();
			particles = null;
		}
	}

	public class Particle2D
	{
		public Sprite display;
		private Material displayMaterial;
		public float xCenter;
		public float yCenter;
		public float aCenter;
		public float scaleCenter;
		public float xRange;
		public float yRange;
		public float aRange;
		public float scaleRange;
		public float xAngle;
		public float yAngle;
		public float aAngle;
		public float scaleAngle;
		public float xSpeed;
		public float ySpeed;
		public float aSpeed;
		public float scaleSpeed;
		public float elapsedTime;
		public float totalTime;

		public Particle2D(Texture2D texture, Rect particleRect) 
		{
			displayMaterial = new Material( Shader.Find("Sprites/Default") );
			displayMaterial.mainTexture = texture;
			
			display = new Sprite ();
			display.graphics.drawRectUV( displayMaterial, new Rect(0, 0, 1, 1), particleRect );
		}

		public Particle2D(Sprite display) 
		{
			this.display = display;
		}
		
		public void Dispose()
		{
			display.graphics.clear();
			if( displayMaterial != null )
				UnityEngine.Object.Destroy( displayMaterial );
		}
	}
}

