using System;
using UnityEngine;
namespace SSGCore
{
	public interface IParticle2DController
	{
		void Layout(Particle2D p, int index, float scale);
		void Update(Particle2D p, float deltaTime, float scale);
		void SetVariation(int variation);
	}
}


