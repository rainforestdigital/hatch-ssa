using UnityEngine;
namespace SSGCore
{
	public class DustParticleUpdater : IParticle2DController
	{
		private float height;
		private float width;

		public DustParticleUpdater (float width, float height)
		{
			this.height = height;
			this.width = width;
		}

		public void Layout(Particle2D p, int index, float deviceScale) {
			// x
			p.xSpeed = Random.Range (0.1f, 0.5f);
			p.xAngle = Random.Range (0f, Mathf.PI * 2.0f);
			p.xRange = Random.Range (0, width * 0.5f);
			p.xCenter = Random.Range (-10f, 10f);

			// y
			p.display.y = Random.Range (0, height);
			p.ySpeed = Random.Range (0.2f, 5f);

			// scale
			p.display.scaleX = p.display.scaleY = Random.Range (0.25f, 0.5f);
			//p.scaleRange = Random.Range (0.05f, 0.3f);
			//p.scaleCenter = 1f;

			// alpha
			p.aRange = 0.5f;
			p.aAngle = Random.Range (0f, Mathf.PI * 2.0f);
			p.aSpeed = Random.Range(1.5f, 8.5f);
			p.aCenter = 0.5f;
		}

		public void Update(Particle2D p, float deltaTime, float deviceScale) {
			// x
			p.display.x = p.xCenter + Mathf.Cos (p.xAngle) * p.xRange;
			p.xAngle += p.xSpeed * deltaTime;

			// y
			p.display.y += p.ySpeed * deltaTime;
			if (p.display.y > height) {
				p.display.y = -p.display.height;
			}
			// alpha
			p.display.alpha = (p.aCenter + Mathf.Sin(p.aAngle) * p.aRange) * (1f - p.display.y / height);
			p.aAngle += p.aSpeed * deltaTime;

			// scale
			//p.display.scaleX = p.display.scaleX = p.scaleCenter + Mathf.Sin (p.xAngle) * p.scaleRange;
		}

		public void SetVariation(int variation) {
			
		}
	}
}

