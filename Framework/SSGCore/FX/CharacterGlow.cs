using System;
using UnityEngine;
using HatchFramework;
namespace SSGCore
{
	public class CharacterGlow : ICharacterEffect
	{
		public CharacterAnimation parent = null;
		private Material horizontalBlurMaterial;
		private Material verticalBlurMaterial;
		private Material colorMaterial;
		private Material passthroughMaterial;
		private float glowAngle = 0;
		private float colorAngle = 0;
		private float color;
		private float colorAlpha;
		private int downsampleWidth;
		private int downsampleHeight;
		//private float blurStrength;

		public CharacterGlow ()
		{
			int downsample = (PlatformUtils.GetPlatform () == Platforms.IOSRETINA || PlatformUtils.GetPlatform () == Platforms.WIN) ? 8 : 4;
			downsampleWidth = Screen.width / downsample;
			downsampleHeight = Screen.height / downsample;

			horizontalBlurMaterial = new Material ((Shader)Resources.Load ("Shaders/HorizontalBlur", typeof(Shader)));
			verticalBlurMaterial = new Material ((Shader)Resources.Load ("Shaders/VerticalBlur", typeof(Shader)));
			horizontalBlurMaterial.SetFloat ("strength", 0.25f);
			verticalBlurMaterial.SetFloat ("strength", 0.25f);
			
			colorMaterial = new Material ((Shader)Resources.Load ("Shaders/ColorTint", typeof(Shader)));
			
			passthroughMaterial = new Material ((Shader)Resources.Load ("Shaders/UnlitAlpha", typeof(Shader)));
		}

		public void Init(CharacterAnimation parent) {
			this.parent = parent;
			parent.Effect = this;
		}

		public void Dispose() {
			if (this.parent != null) {
				this.parent.Effect = null;
				this.parent = null;
			}
		}

		public void Update(float delta) {

			horizontalBlurMaterial.SetFloat ("strength", 0.225f);
			verticalBlurMaterial.SetFloat ("strength", 0.225f);

			//glowAngle += 0.05f * delta;
			//blurStrength = 0.25f + Mathf.Sin (glowAngle) * 0.025f;
			//horizontalBlurMaterial.SetFloat ("strength", blurStrength);
			//verticalBlurMaterial.SetFloat ("strength", blurStrength);
			
			glowAngle += Mathf.PI * 0.025f;
			colorAlpha = 0.7f + Mathf.Sin (glowAngle) * 0.05f;
			color = 0.95f + Mathf.Sin (colorAngle) * 0.1f;
			colorMaterial.color = new Color(color, 1f, color, colorAlpha);
			colorAngle += Mathf.PI * 0.0025f;
		}

		public void Render(RenderTexture sourceTexture, RenderTexture destTexture) {
			UnityEngine.Graphics.Blit (sourceTexture, destTexture);
			//return;
			RenderTexture tempTexture01 = RenderTexture.GetTemporary(downsampleWidth, downsampleHeight);
			RenderTexture tempTexture02 = RenderTexture.GetTemporary(downsampleWidth, downsampleHeight);
			UnityEngine.Graphics.Blit (sourceTexture, tempTexture01);

			for (int i = 0; i < 3; i++) {
				UnityEngine.Graphics.Blit (tempTexture01, tempTexture02, horizontalBlurMaterial);
				UnityEngine.Graphics.Blit (tempTexture02, tempTexture01, verticalBlurMaterial);
			}

			UnityEngine.Graphics.Blit (tempTexture01, destTexture, colorMaterial);
			UnityEngine.Graphics.Blit (sourceTexture, destTexture, passthroughMaterial);

			RenderTexture.ReleaseTemporary(tempTexture01);
			RenderTexture.ReleaseTemporary(tempTexture02);
		}
	}
}

