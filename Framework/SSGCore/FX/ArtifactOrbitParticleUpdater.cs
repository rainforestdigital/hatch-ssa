using UnityEngine;
namespace SSGCore
{
	public class ArtifactOrbitParticleUpdater : IParticle2DController
	{
		public static int ORBIT = 0;
		public static int OUT_ANIM_DELAY = 1;
		public static int OUT_ANIM = 2;
		
		private int OUT_ANIM_DELAY_FRAMES = 15;
		private float OUT_ANIM_TIME = 1.25f;

		private int currentVariation = ORBIT;

		public bool orbiting = true;
		public float translationPercent = 0;
		private Vector2 orbitStartPos;
		private Vector2 artifactStartPos;
		private Vector2 distFromOrbit;
		private float radius;
		private float translationTime;
		private float translationElapsedTime = 0;
		private float delay = 0;
		private float elapsedDelay = 0;
		private float distFromCenter;
		private float outAnimDist;
		private float outAnimStartY = -1;
		private float outAnimElapsedTime = 0;
		private int outAnimElapsedFrames = 0;

		public ArtifactOrbitParticleUpdater (float radius, float distFromCenter, Vector2 artifactStartPos, float translationTime, float delay, float outAnimDist)
		{
			this.radius = radius;
			this.orbitStartPos = new Vector2 (-distFromCenter, distFromCenter);
			this.artifactStartPos = artifactStartPos;
			this.orbiting = false;
			this.translationTime = translationTime;
			this.distFromOrbit = orbitStartPos - artifactStartPos;
			this.distFromCenter = distFromCenter;
			this.delay = delay;
			this.outAnimDist = outAnimDist;
		}

		public ArtifactOrbitParticleUpdater (float radius, float distFromCenter, float outAnimDist)
		{
			this.radius = radius;
			this.distFromCenter = distFromCenter;
			this.orbitStartPos = new Vector2 (Mathf.Cos (Mathf.PI) * radius, Mathf.Sin (Mathf.PI) * radius);
			this.outAnimDist = outAnimDist;
		}

		public void Layout(Particle2D p, int index, float deviceScale) {
			// x
			p.yAngle = Mathf.PI * 0.5f;
			p.yRange = radius;
			p.yCenter = distFromCenter;
			p.ySpeed = 8.05f;

			p.xAngle = Mathf.PI * 0.5f;
			p.xRange = 5f;
			p.xSpeed = 4.025f;
			p.xCenter = -distFromCenter;
			p.display.x = -distFromCenter;


			// scale
			p.scaleCenter = p.display.scaleX;//0.35f;
			p.scaleRange = 0.05f;
			p.scaleAngle = Mathf.PI * 0.5f;
			p.scaleSpeed = 4f;

			// alpha
			p.aCenter = 0.5f;
			p.aRange = 0.5f;

			if (!orbiting) {
				p.display.x = artifactStartPos.x;
				p.display.y = artifactStartPos.y;
				p.display.scaleX = p.display.scaleY = p.scaleCenter + p.scaleRange;
			}
		}

		public void Update(Particle2D p, float deltaTime, float deviceScale) {
			if (currentVariation == ORBIT) {
				if (orbiting) {
					p.display.y = p.yCenter + Mathf.Cos (p.yAngle) * p.yRange;
					p.display.x = p.xCenter + Mathf.Sin (p.xAngle) * p.xRange;
					p.yAngle += p.ySpeed * deltaTime;
					p.xAngle += p.xSpeed * deltaTime;
				} else {
					float t = translationElapsedTime / translationTime;
					translationPercent = Mathf.Min (t, 1f);
					p.display.x = artifactStartPos.x + distFromOrbit.x * translationPercent;
					p.display.y = artifactStartPos.y + distFromOrbit.y * translationPercent;
					if (translationPercent >= 1f) {
						translationElapsedTime = 0;
						orbiting = true;
					} else if (elapsedDelay >= delay) {
						translationElapsedTime += deltaTime;
					} else {
						elapsedDelay += deltaTime;
					}
				}
			} else if (currentVariation == OUT_ANIM_DELAY) {
				outAnimElapsedFrames++;
				if(outAnimElapsedFrames >= OUT_ANIM_DELAY_FRAMES) {
					outAnimElapsedTime = 0;
					currentVariation = OUT_ANIM;
				}
			} else if (currentVariation == OUT_ANIM) {
				if(outAnimStartY == -1) {
					outAnimStartY = p.display.y;
					outAnimElapsedFrames = 0;
				}
				outAnimElapsedTime += deltaTime;
				float complete = Mathf.Min(1f, outAnimElapsedTime / OUT_ANIM_TIME);
				p.display.y = outAnimStartY - outAnimDist * complete;
				p.display.alpha = 1f - complete;//complete <= 0.5f ? 1f : 0.5f + (1f - complete) * 0.5f;
			}

		}

		public void SetVariation(int variation) {
			currentVariation = variation;
			if (currentVariation == OUT_ANIM_DELAY) {
				outAnimStartY = -1;
				outAnimElapsedFrames = 0;
			}
		}
	}
}

