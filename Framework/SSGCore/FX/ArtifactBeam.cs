using System;
using UnityEngine;
using System.Collections.Generic;

using HatchFramework;

using pumpkin.display;
using pumpkin.events;

namespace SSGCore
{
	public class ArtifactBeam : DisplayObjectContainer
	{
		private ArtifactOrbitParticleUpdater updater;
		private Sprite beam;
		private Sprite beamContainer;
		private Material beamMaterial;
		//private float animAngle = 0;
		private Vector2 beamDist;

		public ArtifactBeam (ArtifactOrbitParticleUpdater updater, Vector2 beamStart, Vector2 beamEnd, float scale)
		{
			this.updater = updater;
			Texture2D beamTexture = (Texture2D)Resources.Load ("Textures/Beam", typeof(Texture2D));
			this.beamDist = beamStart - beamEnd;
			
			beamMaterial = new Material( Shader.Find("Sprites/Default") );
			beamMaterial.mainTexture = beamTexture;
			
			beam = new Sprite ();
			float beamWidth = beamTexture.width * scale;
			beam.graphics.drawRectUV (beamMaterial, new Rect (0, 0, 1, 1), new Rect (beamWidth * -0.5f, -beamDist.magnitude, beamWidth, beamDist.magnitude));
			beam.rotation = ((Mathf.PI * 0.5f + Mathf.Atan2 (beamDist.y, beamDist.x)) / Mathf.PI) * 180f;
			beam.alpha = 0f;

			beamContainer = new Sprite ();
			beamContainer.x = beamEnd.x;
			beamContainer.y = beamEnd.y;
		
			beamContainer.addChild (beam);

			addChild (beamContainer);
		}

		public void Update(float deltaTime) {
			if (updater.translationPercent < 1f) {
				if(updater.translationPercent < 0.2f) {
					float s = updater.translationPercent / 0.2f;
					beam.alpha = 0.5f * s;
					beam.scaleX = 1.4f * s;
				}
				else {
					float s = 1f - updater.translationPercent;
					beam.alpha = 0.0f + 0.5f * s;
					beam.scaleX = 0.3f + 1.4f * s;
				}
				//animAngle += 5f * deltaTime;
				beam.scaleY = 1.4f - updater.translationPercent;
			} else if(beam != null) {
				removeChild(beamContainer);
				beam = null;
				beamContainer = null;
			}
		}
		
		public void Dispose()
		{
			if( beamMaterial != null )
				UnityEngine.Object.Destroy( beamMaterial );
		}
	}
}

