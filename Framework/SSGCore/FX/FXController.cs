using System;
using UnityEngine;
using System.Collections.Generic;

using HatchFramework;

using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;


namespace SSGCore
{
	public class FXController
	{
		private DisplayObjectContainer target;
		private Vector2 lunaCoords;
		private CharacterGlow lunaGlow = null;
		private ParticleSystem2D lunaParticles;
		private LightShaft lightShaft;
		private ParticleSystem2D artifactParticle;
		private ArtifactBeam artifactBeam;
		private bool lunaGlowActive = false;
		private bool lunaParticlesActive = false;
		private bool lightShaftActive = false;
		private bool artifactParticleActive = false;
		private bool artifactBeamActive = false;
		private float previousTime = -1;
		private float deltaTime = 0;

		private static FXController _instance = null;
		public static FXController instance {
			get{
				if(_instance == null) {
					_instance = new FXController();
				}
				return _instance;
			}
		}

		private FXController ()
		{
		}

		public void Start(DisplayObjectContainer target) {
			this.target = target;
			target.addEventListener (CEvent.ENTER_FRAME, frameHandler);
			previousTime = Time.time;
		}

		public void Stop() {
			if (target != null) {
				target.removeEventListener (CEvent.ENTER_FRAME, frameHandler);
				this.target = null;
			}
		}
		
		private void frameHandler(CEvent e) {
			deltaTime = Time.time - previousTime;
			if (lunaParticlesActive) {
				lunaParticles.Update(deltaTime);
			}
			if (lightShaftActive) {
				lightShaft.Update(deltaTime);
			}
			if (artifactParticleActive) {
				artifactParticle.Update(deltaTime);
			}
			if (artifactBeamActive) {
				artifactBeam.Update(deltaTime);
			}
			previousTime = Time.time;
		}

		public void Dispose() {
			Stop ();
			
			if( lunaParticles != null )
				lunaParticles.Dispose();
			if( artifactParticle != null )
				artifactParticle.Dispose();
			if( lightShaft != null )
				lightShaft.Dispose();
			if( artifactBeam != null )
				artifactBeam.Dispose();
			
			DisableLunaFX ();
			DisableLightShaft();
			
			lunaParticles = null;
			artifactParticle = null;
			lightShaft = null;
			artifactBeam = null;
			
			_instance = null;
		}

		public void EnableLunaParticles(Base3dCharacter luna) {	
			if (lunaParticlesActive) {
				DisableLunaParticles();
			}
			Texture2D lunaParticleTexture = (Texture2D)Resources.Load ("Textures/Particle", typeof(Texture2D));
			lunaParticles = new ParticleSystem2D (getScale ());
			lunaParticles.Init (48, lunaParticleTexture, new OrbitParticleUpdater (Screen.width * 0.15f));

			calculateLunaCoords (luna);
			lunaParticles.x = lunaCoords.x;
			lunaParticles.y = lunaCoords.y;
			lunaParticles.fadingIn = true;
			lunaParticlesActive = true;
			luna.addChild (lunaParticles);
		}

		public void EnableLunaGlow(Base3dCharacter luna) {
			if (lunaGlow == null) {
				lunaGlow = new CharacterGlow ();
			}
			lunaGlow.Init(luna.animController);
			lunaGlowActive = true;
		}

		public void EnableLightShaft(DisplayObjectContainer targetContainer, Vector2 targetCoords) {
			if (lightShaftActive) {
				DisableLightShaft();
			}
			lightShaft = new LightShaft ();
			lightShaft.Init (targetCoords.y, getScale());
			lightShaft.x = targetCoords.x;
			lightShaft.y = 0;
			targetContainer.addChild (lightShaft);
			lightShaftActive = true;
		}

		public void EnableLunaArtifactBeamAndOrbit(MovieClip artifact, Vector2 artifactStartPos, Base3dCharacter luna, float beamDelay, SkillLevel skill) {
			if (artifactParticleActive) {
				DisableLunaArtifactOrbit();
			}
			artifactParticle = new ParticleSystem2D (getScale());
			calculateLunaCoords (luna);
			artifactParticle.x = lunaCoords.x;
			artifactParticle.y = lunaCoords.y;
			ArtifactOrbitParticleUpdater updater = new ArtifactOrbitParticleUpdater (Screen.width * 0.005f, Screen.width * 0.06f, artifactParticle.globalToLocal (artifactStartPos), 1.5f, beamDelay, lunaCoords.y * 1.5f);
			artifactParticle.Init(artifact, updater);
			artifactParticle.fadingIn = true;
			artifactParticleActive = true;
			luna.addChild (artifactParticle);

			if (artifactBeamActive) {
				DisableArtifactBeam();
			}
			artifactBeam = new ArtifactBeam (
				updater, 
				new Vector2(artifactStartPos.x + Screen.width * 0.03f, artifactStartPos.y + Screen.width * 0.03f), 
				new Vector2(lunaCoords.x + Screen.width * 0.03f, lunaCoords.y + Screen.width * 0.03f), 
				getScale());
			luna.addChild (artifactBeam);
			artifactBeamActive = true;
		}

		public void EnableLunaArtifactOrbit(MovieClip artifact, Base3dCharacter luna, SkillLevel skill) {
			if (artifactParticleActive) {
				DisableLunaArtifactOrbit();
			}
			artifactParticle = new ParticleSystem2D (getScale());
			artifactParticle.Init(artifact, new ArtifactOrbitParticleUpdater(Screen.width * 0.005f, Screen.width * 0.06f, lunaCoords.y * 1.5f));
			calculateLunaCoords (luna);
			artifactParticle.x = (lunaCoords.x * 0.9f) - artifact.x;
			artifactParticle.y = (lunaCoords.y * 1.1f) - artifact.y;
			artifactParticle.fadingIn = true;
			artifactParticleActive = true;
			luna.addChild (artifactParticle);
		}

		public void DisconnectArtifactOrbit() {
			if (artifactParticleActive && artifactParticle != null && artifactParticle.parent != null) {
				artifactParticle.parent.removeChild (artifactParticle);
			}
		}

		public void MoveArtifactOrbitToNewLuna(Base3dCharacter luna) {
			if (artifactParticleActive && artifactParticle != null) {
				luna.addChild(artifactParticle);
			}
		}

		public void DoParticleOutAnim() {
			if (artifactParticleActive && artifactParticle != null) {
				artifactParticle.controller.SetVariation(ArtifactOrbitParticleUpdater.OUT_ANIM_DELAY);
			}
		}

		private int getArtifactFrame(SkillLevel skill) {
			int frame;
			switch( skill ){
			case SkillLevel.Developing:
				frame = 2;
				break;
			case SkillLevel.Developed:
			case SkillLevel.Completed:
				frame = 3;
				break;
			default:
				frame = 1;
				break;
			}
			return frame;
		}

		public void DisableLightShaft() {
			if (lightShaftActive && lightShaft != null) {
				if( lightShaft.parent != null )
					lightShaft.parent.removeChild (lightShaft);
				lightShaft = null;
				lightShaftActive = false;
			}
		}

		public void DisableLunaFX() {
			DisableLunaArtifactOrbit ();
			DisableLunaGlow ();
			DisableLunaParticles ();
			DisableArtifactBeam ();
		}

		public void DisableLunaParticles() {
			if (lunaParticlesActive && lunaParticles != null) {
				if( lunaParticles.parent != null )
					lunaParticles.parent.removeChild (lunaParticles);
				lunaParticles = null;
				lunaParticlesActive = false;
			}
		}
		
		public void DisableLunaGlow() {
			if(lunaGlowActive && lunaGlow != null) {
				lunaGlow.Dispose ();
			}
		}

		public void DisableArtifactBeam() {
			if (artifactBeamActive && artifactBeam != null) {
				if( artifactBeam.parent != null )
					artifactBeam.parent.removeChild (artifactBeam);
				artifactBeam = null;
				artifactBeamActive = false;
			}
		}

		public void DisableLunaArtifactOrbit() {
			if (artifactParticleActive && artifactParticle != null) {
				if( artifactParticle.parent != null )
					artifactParticle.parent.removeChild (artifactParticle);
				artifactParticle = null;
				artifactParticleActive = false;
			}
		}

		// Obtained using magic numbers! There has to be some reliable method for getting this position at runtime, but it was becoming a huge time sink
		private void calculateLunaCoords(Base3dCharacter luna) {
			if(lunaCoords.magnitude == 0) {
				Vector3 pos = luna.animController.pos;
				Vector3 coords = luna.cam.WorldToScreenPoint( new Vector3(pos.x + 21f, pos.y + 12f, pos.z + 11f) );
				lunaCoords = new Vector2(coords.x, Screen.height - coords.y);
			}
		}

		public float getScale() {
			return ( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA ) ? 1f : 0.5f;
		}
	}
}

