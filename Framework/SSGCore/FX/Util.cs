using System;
using UnityEngine;
namespace SSGCore
{
	public class Util
	{
		public static float GetBellCurvePoint(float percentage, float midpoint)
		{
			if (percentage > midpoint) {
				percentage = 1f - percentage;
				return 1f - ((percentage - ((1f - percentage) * percentage)) * (1f / (1f - midpoint)));
			} else {
				return (percentage - ((1f - percentage) * percentage)) * (1f / midpoint);
			}
		}
	}
}

