using UnityEngine;
namespace SSGCore
{
	public class SparkleParticleUpdater : IParticle2DController
	{
		private const float TOTAL_FADE_TIME = 1f;
		private float height;
		private float width;

		public SparkleParticleUpdater (float width, float height)
		{
			this.height = height;
			this.width = width;
		}

		public void Layout(Particle2D p, int index, float deviceScale) {

			// scale
			p.scaleRange = Random.Range (0.05f, 0.3f);
			p.scaleCenter = 0.5f;
			p.scaleAngle = Random.Range (0f, Mathf.PI);

			// alpha
			p.display.alpha = 0f;
			
			p.totalTime = Random.Range (TOTAL_FADE_TIME * 0.5f, TOTAL_FADE_TIME);
			p.elapsedTime = Random.Range (0f, p.totalTime);
		}

		public void Update(Particle2D p, float deltaTime, float deviceScale) {
			p.elapsedTime += deltaTime;

			// alpha
			float elapsed = Mathf.Min (1f, (p.elapsedTime / p.totalTime));
			float a = Mathf.Sin (elapsed * Mathf.PI);
			p.display.alpha = a;

			if (p.elapsedTime >= p.totalTime) {
				p.elapsedTime = 0f;
				p.display.x = Random.Range (0f, width);
				p.display.y = Random.Range (0f, height);
			}

			// scale
			p.display.scaleX = p.display.scaleY = p.scaleCenter + Mathf.Sin (p.scaleAngle) * p.scaleRange;
		}

		public void SetVariation(int variation) {
			
		}
	}
}

