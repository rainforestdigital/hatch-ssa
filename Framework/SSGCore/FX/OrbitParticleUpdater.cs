using UnityEngine;
namespace SSGCore
{
	public class OrbitParticleUpdater : IParticle2DController
	{
		private float radius;

		public OrbitParticleUpdater (float radius)
		{
			this.radius = radius;
		}

		public void Layout(Particle2D p, int index, float deviceScale) {
			float dist = Random.Range (0.05f, 1f);

			// x
			p.xAngle = Random.Range (0f, Mathf.PI * 2.0f);
			p.xRange = radius * dist;
			p.xSpeed = (1f - dist) * Random.Range(1f, 3f);

			// y
			p.yRange = p.xRange;

			// scale
			p.scaleCenter = (1f - dist) * 2.0f * deviceScale;

			// a
			p.aRange = 0.5f;
			p.aAngle = Random.Range (0f, Mathf.PI * 2.0f);
			p.aSpeed = Random.Range(5f, 10f);
			p.aCenter = 0.5f;
		}

		public void Update(Particle2D p, float deltaTime, float deviceScale) {
			p.display.x = Mathf.Cos (p.xAngle) * p.xRange;
			p.display.y = Mathf.Sin (p.xAngle) * p.yRange;
			p.display.scaleX = p.display.scaleY = p.scaleCenter;
			p.display.alpha = p.aCenter + Mathf.Sin (p.aAngle) * p.aRange;
			p.xAngle += p.xSpeed * deltaTime;
			p.aAngle += p.aSpeed * deltaTime;
		}
		
		public void SetVariation(int variation) {
		}
	}
}

