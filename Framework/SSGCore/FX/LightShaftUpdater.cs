using UnityEngine;
namespace SSGCore
{
	public class LightShaftUpdater : IParticle2DController
	{
		private int numShafts;
		private float particleWidth;
		private float totalWidth;

		public LightShaftUpdater (int numShafts, float particleWidth)
		{
			this.numShafts = numShafts;
			this.particleWidth = particleWidth;
			this.totalWidth = numShafts * particleWidth;
		}

		public void Layout(Particle2D p, int index, float deviceScale) {
			float curve = Mathf.Sin((float)index / (float)(numShafts - 1)* Mathf.PI);
			// x
			p.xSpeed = Random.Range (0.5f, 5f);
			p.xAngle = Random.Range (0f, Mathf.PI * 2.0f);
			p.xCenter = (totalWidth * -0.5f + index * particleWidth) * deviceScale;
			p.xRange = Random.Range (1f, 3f);

			// scale
			p.scaleRange = Random.Range (12f, 15f);
			p.scaleCenter = 1f + curve * 3f;
			p.scaleSpeed = Random.Range (0.5f, 5f);
			p.scaleAngle = Random.Range (0f, Mathf.PI * 2.0f);

			// alpha
			p.aRange = Random.Range (curve * 0.05f, 0.15f);
			p.aCenter = 0.05f + curve * 0.4f;
			p.aSpeed = Random.Range (1.5f, 5f);
			p.aAngle = Random.Range (0f, Mathf.PI * 2.0f);
		}

		public void Update(Particle2D p, float deltaTime, float deviceScale) {
			// x
			p.display.x = p.xCenter + Mathf.Cos(p.xAngle) * p.xRange;
			p.xAngle += p.xSpeed * deltaTime;

			// scale
			p.display.scaleX = p.scaleCenter + Mathf.Sin (p.scaleAngle) * p.scaleRange;
			p.scaleAngle += p.scaleSpeed * deltaTime;

			// alpha
			p.display.alpha = p.aCenter + Mathf.Sin (p.aAngle) * p.aRange;
			p.aAngle += p.aSpeed * deltaTime;
		}
		
		public void SetVariation(int variation) {
			
		}
	}
}

