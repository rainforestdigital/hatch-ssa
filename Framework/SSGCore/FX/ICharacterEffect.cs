using System;
using UnityEngine;
namespace SSGCore
{
	public interface ICharacterEffect
	{
		void Update(float delta);
		void Render (RenderTexture sourceTexture, RenderTexture destTexture);
	}
}

