using System;
using UnityEngine;
using System.Collections.Generic;

using HatchFramework;

using pumpkin.display;
using pumpkin.events;

namespace SSGCore
{
	public class LightShaft : DisplayObjectContainer
	{
		private Sprite lightBeam;
		private ParticleSystem2D sparkles;
		private ParticleSystem2D dustRound;
		private Texture2D lightTexture;
		private Material lightMaterial;
		private Texture2D sparkleTexture;
		private Texture2D dustTextureRound;
		private bool initialized = false;

		public LightShaft ()
		{
			lightTexture = (Texture2D)Resources.Load ("Textures/LightShaft", typeof(Texture2D));
			sparkleTexture = (Texture2D)Resources.Load ("Textures/SparkleParticle", typeof(Texture2D));
			dustTextureRound = (Texture2D)Resources.Load ("Textures/SparkleParticleRound", typeof(Texture2D));
		}

		public void Init(float totalHeight, float scale) 
		{
			if (initialized) {
				removeChild(sparkles);
				removeChild(dustRound);
				removeChild(lightBeam);

				sparkles = null;
				lightBeam = null;
			}
			
			lightMaterial = new Material( Shader.Find("Sprites/Default") ); 
			lightMaterial.mainTexture = lightTexture;
			
			lightBeam = new Sprite ();
			lightBeam.graphics.drawRectUV (lightMaterial, new Rect (0, 0, 1, 1), new Rect ((Screen.width * 0.8f) * -0.5f, 0, Screen.width * 0.8f, totalHeight));
			addChild (lightBeam);
			
			sparkles = new ParticleSystem2D ();
			sparkles.Init (4, sparkleTexture, new SparkleParticleUpdater(Screen.width * 0.4f, totalHeight));
			sparkles.x = Screen.width * -0.2f;
			addChild (sparkles);
			sparkles.fadingIn = true;
			
			dustRound = new ParticleSystem2D ();
			dustRound.Init (32, dustTextureRound, new DustParticleUpdater(Screen.width * 0.4f, totalHeight));
			addChild (dustRound);
			dustRound.fadingIn = true;
		}

		public void Update(float deltaTime) {
			sparkles.Update (deltaTime);
			dustRound.Update (deltaTime);
		}
		
		public void Dispose()
		{
			if( lightMaterial != null )
				UnityEngine.Object.Destroy( lightMaterial );
			
			if( sparkles != null )
				sparkles.Dispose();
			
			if( dustRound != null )
				dustRound.Dispose();
		}
	}
}

