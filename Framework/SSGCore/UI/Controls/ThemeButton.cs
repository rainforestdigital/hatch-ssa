using System;
using HatchFramework;
using UnityEngine;
using pumpkin.events;

namespace SSGCore
{
	public class ThemeButton : BasicButton
	{
		public ThemeButton (Theme theme)
			: base (theme.ButtonSymbol)
		{
			this.theme = theme;
		}
		
		private Theme theme;
		public Theme Theme
		{
			get { return theme; }
		}
		
		protected override void onMouseDown( CEvent e )
		{
			this.colorTransform = Color.yellow;
		}
		
		protected override void onMouseUp( CEvent e )
		{
			this.colorTransform = Color.gray;
		}
		
		protected override void onMouseOver( CEvent e )
		{
			this.colorTransform = Color.gray;
		}
		
		protected override void onMouseOut( CEvent e )
		{
			this.colorTransform = Color.white;
		}
	}
}

