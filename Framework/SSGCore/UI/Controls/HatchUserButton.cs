using UnityEngine;
using System.Collections;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.text;

namespace SSGCore
{
	public class HatchUserButton : BasicButton
	{
		private Child user;
		private Material userMat;
		public Child User
		{
			get { return user; }
			set
			{
				user = value;
				if(user != null)
				{
					NameText = user.Name;
					SetImage();
				}
				else
				{
					NameText = "";
					ClearGraphics();
				}
			}
		}
		
		public void SetImage()
		{
			ClearGraphics();
			
			// TODO: determine more elegant way of sizing
			// using size of clip was tried, but graphics in
			// the clip appeared to interfere with the drawing.
			// Possibly use proxy.				
			if( user != null ) {
				if( userMat == null )
					userMat = new Material( Shader.Find("Sprites/Default") );
				userMat.mainTexture = user.Texture;
				
				imageContainer_mc.graphics.drawRectUV( userMat,
					new Rect(0, 0, 1, 1),
					new Rect(0, 0, 125, 116));
			}
		}
		
		private TextField name_txt;
		public string NameText
		{ 
			get{ return name_txt.text; } 
			
			set
			{
				name_txt.text = value;
			}
			
		}
			
		private int buttonID;
		public int ButtonID
		{
			get{ return buttonID; }
			
			set{ buttonID = value; }
			
		}
		
		private MovieClip imageContainer_mc;
		
		public HatchUserButton() : base( Main.SWF, "HatchUserButton" )
		{
			
		}
		
		protected override void init()
		{
			
			base.init();
			
			name_txt = getChildByName<MovieClip>( "name_mc" ).getChildByName<TextField>("name_txt");
					
			imageContainer_mc = getChildByName<MovieClip>( "imageContainer_mc" );
		}
		
		public void Refresh()
		{
			if(user != null) {
				//NameText = user.Name;
				//SetImage();
			}
		}
		
		public void ClearGraphics()
		{
			imageContainer_mc.graphics.clear();
			
			if( userMat != null )
				Object.Destroy( userMat );
		}
		
		public void Dispose()
		{
			ClearGraphics();
			
			user = null;
		}
	}
}
