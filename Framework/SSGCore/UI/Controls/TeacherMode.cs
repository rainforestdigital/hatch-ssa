using System;
using UnityEngine;
using System.Collections;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;

namespace SSGCore
{
	public class TeacherMode : DisplayObjectContainer
	{
		
		private MovieClip root;
		
		private BasicButton exitBtn_mc;
		public BasicButton ExitButton
		{
			get
			{
				return exitBtn_mc;
			}
		}
		
		public SessionInfo sessionConfig;
		
		public Action OnExitClicked;
				
		public TeacherMode() : base()
		{
			root = MovieClipFactory.CreateTeacherModeBar();
			addChild( root );
			
			init();
			
		}
				
		public void init()
		{
			exitBtn_mc = new BasicButton( root.getChildByName<MovieClip>("exitBtn_mc") );
			exitBtn_mc.Root.addEventListener( MouseEvent.CLICK, onExitClick );
			addEventListener (CEvent.ADDED_TO_STAGE, stageHandler);
		}

		private void stageHandler(CEvent e) {
			removeEventListener (CEvent.ADDED_TO_STAGE, stageHandler);

			float yScale = 1f;
			switch(PlatformUtils.GetPlatform()) {
			case Platforms.IOSRETINA:
				yScale = 1.5f;
				break;

			case Platforms.IOS:
				yScale = 0.65f;
				break;

			case Platforms.ANDROID:
				yScale = 0.5f;
				break;

			case Platforms.WIN:
				yScale = 0.9f;
				break;
			}

			float dispHeight = 138 * yScale;

			DisplayObject background = root.getChildByName ("teacherMode_background");
			float sWidth = getBounds(stage).width;
			background.scaleX = Screen.width/sWidth;
			background.scaleY = yScale;
			background.x = 0;
			background.y = dispHeight;
			background.alpha = 0.75f;

			DisplayObject exitButton = root.getChildByName ("exitBtn_mc");
			exitButton.scaleX = exitButton.scaleY = background.scaleY;
			exitButton.x = Screen.width * 0.5f - 168f * exitButton.scaleX;
			exitButton.y = dispHeight * 0.5f + 32f * exitButton.scaleY;

			DisplayObject text = root.getChildByName ("teacher_mode_txt");
			text.scaleX = text.scaleY = background.scaleY;
			text.x = Screen.width * 0.5f - 465f * text.scaleX;
			text.y = dispHeight * 0.5f + 68f * text.scaleY;

			this.x = Screen.width * 0.5f;
			this.y = -dispHeight * 0.5f;
		}

		private void onExitClick( CEvent e )
		{
			if(OnExitClicked != null)
			{
				OnExitClicked();
			}
		}

		public void Resize( int width, int height )
		{
			
			//this.width = width;
			//this.height = height;
			
			//root.FitToParent();
			
			root.x = 0;
			root.y = 0;
			
			/*DisplayObjectContainer parent = this.parent;
			
			float scale = float.Parse(this.width.ToString()) / float.Parse(root.height.ToString())
			
			this.scaleX = scale;
			this.scaleY = scale;
			this.x = (parent.width - this.width*scale)/2;
			this.y = (parent.height - this.height*scale)/2;
			
			root.scaleX = scale;
			root.scaleY = scale;
			*/
		}
		
	}
}
