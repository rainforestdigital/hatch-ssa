using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.text;
using pumpkin.tweener;

namespace SSGCore
{

	public class DateScroller : DisplayObjectContainer
	{
	
		private MovieClip view;
		
		private DisplayObjectContainer maskContainer;
		private DisplayObjectContainer listContainer;
		
		private List<MovieClip> listItems;
		
		private float listItemWidth = 95.1f;
		private float listItemHeight = 51.6f;
		
		private Rect maskRect;
		
		private int currentValueIndex = 0;
		public int CurrentValueIndex{ get{ return currentValueIndex; } }
		
		public DateScroller()
		{
			
			view = MovieClipFactory.CreateDateScrollerView();
			addChild( view );
			
			maskContainer = new DisplayObjectContainer();
			maskContainer.x = 10f;
			maskContainer.y = 17f;
			view.addChildAt( maskContainer, 0 );
			
			listContainer = new DisplayObjectContainer();
			maskContainer.addChild( listContainer );
					
		}
		
		public void Init(List<string> items)
		{
			Init(items.ToArray());
		}
		
		public void Init(string[] items)
		{
			
			if(listItems != null)
			{
				foreach(MovieClip mc in listItems)
				{
					listContainer.removeChild(mc);
				}
				listItems.Clear();
			}
			else
				listItems = new List<MovieClip>();
						
			for(int i = 0; i < items.Length; i++)
			{
				MovieClip newItem = MovieClipFactory.CreateDateScrollerListItem();
				newItem.y = (i + 1) * listItemHeight;
				newItem.getChildByName<TextField>("label_txt").text = items[i];
				listContainer.addChild(newItem);
				listItems.Add(newItem);
			}
			
			maskRect = new Rect(view.x, view.y, listItemWidth, listItemHeight * 3);
			
			maskContainer.clipRect = maskRect;
			
			addEventListener( MouseEvent.MOUSE_DOWN, onListDown );
			
			addEventListener( CEvent.ADDED_TO_STAGE, OnAddedToStage );
			addEventListener( CEvent.REMOVED_FROM_STAGE, OnRemovedFromStage );
			
			updateItemAlphas();
			
		}
		
		private float mouseDistance;
		private void onListDown( CEvent e )
		{
			mouseDistance = mouseY - listContainer.y;
			
			Tweener.removeTweens(listContainer);
			
			addEventListener( CEvent.ENTER_FRAME, update );
		}
		
		private void onListUp( CEvent e )
		{
			removeEventListener( CEvent.ENTER_FRAME, update );
			
			int index = Mathf.Abs(Mathf.RoundToInt( listContainer.y / listItemHeight ));
			float endPosition = -(index * listItemHeight);
			Tweener.addTween(listContainer, Tweener.Hash("y",endPosition, "time",0.5f, "transition",Tweener.TransitionType.easeInOutQuad) );
			
			currentValueIndex = index;
			
			updateItemAlphas();
			
			dispatchEvent( new CEvent(CEvent.CHANGED) );
			
		}
		
		void OnAddedToStage ( CEvent e )
		{
			stage.addEventListener( MouseEvent.MOUSE_UP, onListUp );
		}

		void OnRemovedFromStage ( CEvent e )
		{
			stage.removeEventListener( MouseEvent.MOUSE_UP, onListUp );
		}
		
		private void updateItemAlphas()
		{
			for(int i = 0; i < listItems.Count; i++)
			{
				if(currentValueIndex == i)
					listItems[i].alpha = 1;
				else
					listItems[i].alpha = 0.5f;
			}
		}
		
		private void update( CEvent e )
		{
			float containerPos = Mathf.Clamp(mouseY - mouseDistance, -(listItemHeight * (listItems.Count - 1)), 0);
			listContainer.y = containerPos;
		}
		
		public void SetValue(int index)
		{
			currentValueIndex = index;
			float endPosition = -(index * listItemHeight);
			Tweener.addTween(listContainer, Tweener.Hash("y",endPosition, "time",0.5f, "transition",Tweener.TransitionType.easeInOutQuad) ).OnComplete( updateItemAlphas );
		}
		
		public void SetValueImmediate(int index)
		{
			currentValueIndex = index;
			float endPosition = -(index * listItemHeight);
			listContainer.y = endPosition;
			updateItemAlphas();
		}
		
	}

}