using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.text;
using pumpkin.events;

namespace SSGCore
{
	public class TimeoutMenu : ConfirmationMenu
	{
		private Child user;
		private AbsoluteTimeoutObject timeout;

		public TimeoutMenu( string linkage, Child _user, string language ) : base( linkage, language )
		{
			user = _user;
			showInfo();
		}
		public TimeoutMenu( string swf, string symbolName, Child _user, string language ) : base( swf, symbolName, language )
		{
			user = _user;
			showInfo();
		}
		public TimeoutMenu( MovieClip mc, Child _user, string language ) : base( mc, language )
		{
			user = _user;
			showInfo();
		}
		
		private Material userMaterial;
		private MovieClip childPhoto;
		private TextField nameField;
		private int timeoutCount;
		private void showInfo()
		{
			childPhoto = Root.getChildByName<MovieClip>( "userImg" );
			nameField = Root.getChildByName<TextField>( "userName" );
			nameField.y += 15f;
			nameField.text = user.Name;
			
			var graphics = childPhoto.graphics;
			graphics.clear();
			if(user.Texture != null)
			{
				if( userMaterial == null )
					userMaterial = new Material( Shader.Find("Sprites/Default") );
				userMaterial.mainTexture = user.Texture;
				graphics.drawRectUV(userMaterial,
					new Rect(0, 0, 1, 1),
					new Rect(0, 0, 111, 105));
			}
			
			timeoutCount = 0;
			timeout = new AbsoluteTimeoutObject(15, onTimeout);
		}
		protected override void playMessage ()
		{
			if( audioPlayer != null )
				GameObject.Destroy(audioPlayer);
			audioPlayer = new GameObject("audioPlayerForConfirmationMenu");
			audioPlayer.AddComponent<AudioSource>();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.GLOBAL , (language == "sp" ? "SPN-" : "") + "timeout-message" );
			audioPlayer.audio.clip = clip;
			audioPlayer.audio.ignoreListenerPause = true;
			audioPlayer.audio.Play();
		}
		
		public override void Unload ()
		{
			base.Unload ();
			
			if( timeout != null )
				timeout.Cancel();
			
			if( userMaterial != null )
				UnityEngine.Object.Destroy( userMaterial );
		}
		
		private void onTimeout()
		{
			timeoutCount++;
			
			SSGEngine.Instance.ResetInactivity();
			if(timeoutCount > 20 && Reject != null)
			{
				Reject();
			}
			else
			{
				//Replay Audio
				resetAnim();
				playMessage();
				
				timeout.resetTimeout(15, onTimeout);
			}
		}
	}
}