using UnityEngine;
using System.Collections;
using HatchFramework;
using pumpkin;
using pumpkin.display;
using pumpkin.events;

namespace SSGCore
{

	public class WebCamCapture : MovieClip
	{
		
		private int _width;
		private int _height;
		private float _textureScale = 0.375f;
		
		//	Web Cam
		private WebCamDevice[] devices;
		private string deviceName;
		private WebCamTexture camTexture;
		
		//	Display Texture
		private Texture2D displayTexture;
		private Texture2D tempCamTexture = null;
		private Rect srcRect;
		private Rect drawRect;
		private Material flipMaterial;
		
		private Material camMaterial;
		private Material displayMaterial;
		private Material previewMaterial;

		public bool HasTexture
		{
			get { return displayTexture != null; }
		}
		
		public Texture2D MoveTexture()
		{
			var tex = displayTexture;
			displayTexture = null;
			return tex;
		}
		
		private Texture2D snapshot = null;
		
		/// <summary>
		/// Gets the snap shot byte array.
		/// </summary>
		public byte[] SnapShotByteArray
		{
			get{ return snapshot.EncodeToPNG(); }
		}
		
		
		/// <summary>
		/// Gets a value indicating whether this <see cref="SSGCore.WebCamCapture"/> has available devices.
		/// </summary>
		/// <value>
		/// <c>true</c> if devices are available; otherwise, <c>false</c>.
		/// </value>
		public bool isAvailable
		{
			get{ return (devices.Length > 0); }
		}
		
		/// <summary>
		/// Gets a value indicating whether this <see cref="SSGCore.WebCamCapture"/> is playing.
		/// </summary>
		/// <value>
		/// <c>true</c> if camTexture is streaming; otherwise, <c>false</c>.
		/// </value>
		public bool isCamPlaying
		{
			get
			{
				if(camTexture == null) return false;
				return camTexture.isPlaying;
			}
		}
		
		
		
		
		/// <summary>
		/// Initializes a new instance of the <see cref="SSGCore.WebCamCapture"/> class. 
		/// This class captures the web cam stream, and renders/converts it to a <see cref="Texture2D"/> that is drawn to the screen.
		/// </summary>
		/// <param name='_width'>
		/// Width of the movieclip.
		/// </param>
		/// <param name='_height'>
		/// Height of the movieclip.
		/// </param>
		public 	WebCamCapture( int _width, int _height ) : base()
		{
			
			this._width = _width;
			this._height = _height;
			
			init();
			
		}
		
		private void init()
		{		
			
			devices = WebCamTexture.devices;
			
			if(!isAvailable)
			{
				Debug.LogError( "No Web Cams exist on this device. You will not be able take/save pictures." );
				return;
			}
			
			//Set default decive name
			deviceName = devices[0].name;
			
			srcRect = new Rect( 0, 0, 1, 1 );
			drawRect = new Rect( 0, 0, _width, _height );
		}
		
		/// <summary>
		/// Starts the web cam and renders the texture to this movieclip.
		/// </summary>
		public void Start()
		{
			if(!isAvailable || isCamPlaying) return;
			
			tempCamTexture = new Texture2D ((int)(_width*_textureScale), (int)(_height*_textureScale), TextureFormat.ARGB32, false);
			
			if (PlatformUtils.GetPlatform () == Platforms.IOS || PlatformUtils.GetPlatform () == Platforms.IOSRETINA) {
				flipMaterial = new Material ((Shader)Resources.Load ("Shaders/FlipVertical", typeof(Shader)));
			}
			
			if( camMaterial == null )
				camMaterial = new Material( Shader.Find("Sprites/Default") );
			camMaterial.mainTexture = tempCamTexture;
			
			this.graphics.clear();
			this.graphics.drawRectUV( camMaterial, srcRect, drawRect );
			
			camTexture = new WebCamTexture( deviceName, (int)(_width*_textureScale), (int)(_height*_textureScale) );
			camTexture.name = "webcam";
			camTexture.Play();
			
			this.addEventListener( CEvent.ENTER_FRAME, update );
		}
		
		/// <summary>
		/// Stops the web cam and clears the graphics layer.
		/// </summary>
		public void Stop()
		{
			if(!isAvailable) return;
			
			if( camTexture != null )
			{
				camTexture.Stop();
				UnityEngine.Object.Destroy(camTexture);
			}
			
			this.removeEventListener( CEvent.ENTER_FRAME, update );
			this.graphics.clear();
		}
		
		/// <summary>
		/// Takes a snap shot from the web cam texture. Calls Stop() and re-draws the last frame.
		/// </summary>
		public void TakeSnapShot()
		{
			if(!isAvailable) return;
			ClearTexture ();
			
			displayTexture = getCamTexture();
			displayTexture.name = "webcam_capture";
			
			Stop();
			
			if( displayMaterial == null )
				displayMaterial = new Material( Shader.Find("Sprites/Default") );
			displayMaterial.mainTexture = displayTexture;
			
			this.graphics.drawRectUV( displayMaterial, srcRect, drawRect );
		}

		public void ClearTexture ()
		{
			if(displayTexture != null) {
				Object.Destroy(displayTexture);
				displayTexture = null;
			}
		}
		
		public void SetPreviewTexture(Texture texture)
		{
			Stop();
			ClearTexture ();
			this.graphics.clear();
			
			if( previewMaterial == null )
				previewMaterial = new Material( Shader.Find("Sprites/Default") );
			previewMaterial.mainTexture = texture;
			
			this.graphics.drawRectUV( previewMaterial, srcRect, drawRect );
		}
		
		public void Destroy()
		{
			TimerUtils.SetTimeout( 0.25f, () => {
				if( camTexture != null )
					UnityEngine.Object.Destroy(camTexture);
				if( tempCamTexture != null )
					UnityEngine.Object.Destroy(tempCamTexture);
					
				ClearTexture();
				
				if( flipMaterial != null )
					UnityEngine.Object.Destroy( flipMaterial );
				
				if( camMaterial != null )
					UnityEngine.Object.Destroy( camMaterial );
				
				if( displayMaterial != null )
					UnityEngine.Object.Destroy( displayMaterial );
				
				if( previewMaterial != null )
					UnityEngine.Object.Destroy( previewMaterial );
			} );
		}
	
		/// <summary>
		/// Clears the graphics layer, updates the display texture, and re-draws it to the graphics layer.
		/// </summary>
		/// <param name='e'>
		/// Called from an ENTER_FRAME listner. Pass 'null' if calling from elsewhere.
		/// </param>
		private void update( CEvent e )
		{
			//this.graphics.clear();
			//this.graphics.drawRectUV( getCamTexture(), srcRect, drawRect );
			getCamTexture();
		}
		
		public void Resize( int width, int height )
		{
			_width = width;
			_height = height;
			drawRect = new Rect( 0, 0, _width, _height );
		}

		// This a necessary workaround to get around the iOS camera bugs in 4.6.3
		// http://issuetracker.unity3d.com/issues/ios-memory-leak-in-getpixels-and-getpixels32
		private Texture2D getCamTexture(){
			RenderTexture renderTexture = RenderTexture.GetTemporary (camTexture.width, camTexture.height);
			if (PlatformUtils.GetPlatform () == Platforms.IOS || PlatformUtils.GetPlatform () == Platforms.IOSRETINA) {
				UnityEngine.Graphics.Blit (camTexture, renderTexture, flipMaterial);
			} else {
				UnityEngine.Graphics.Blit (camTexture, renderTexture);
			}
			tempCamTexture.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
			tempCamTexture.Apply();
			RenderTexture.ReleaseTemporary (renderTexture);
			return tempCamTexture;
		}
	}

}
