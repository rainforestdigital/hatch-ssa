using UnityEngine;
using System.Collections;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.text;

namespace SSGCore
{
	public class ClassroomButton : BasicButton
	{
		private Classroom classroom;
		private Material teacherMat;
		private Material classMat;
		public Classroom Classroom
		{
			get { return classroom; }
			set
			{
				if(classroom != value)
				{
					classroom = value;
					if(classroom != null)
					{						
						name_txt.text = classroom.Name;
						UpdateImage();
					}
					else
					{
						name_txt.text = "";
					}
				}
				
			}
		}
		
		private TextField name_txt;
		private MovieClip imageContainer_mc;
		
		private void UpdateImage()
		{
			ClearGraphics();
			
			// TODO: determine more elegant way of sizing
			// using size of clip was tried, but graphics in
			// the clip appeared to interfere with the drawing.
			// Possibly use proxy.
			
			//DrawImage(classroom.TeacherImage, new Rect(0, 0, 125, 116));
			//DrawImage(classroom.Image, new Rect(125-32, 116-32, 32, 32));
			if( classroom.TeacherImage != null )
			{
				if( teacherMat == null )
					teacherMat = new Material( Shader.Find("Sprites/Default") );
				teacherMat.mainTexture = classroom.TeacherImage;
				
				imageContainer_mc.graphics.drawRectUV( teacherMat, new Rect(0, 0, 1, 1), new Rect(0, 0, 125, 116) );
			}
			if( classroom.Image != null )
			{
				if( classMat == null )
					classMat = new Material( Shader.Find("Sprites/Default") );
				classMat.mainTexture = classroom.Image;
				
				imageContainer_mc.graphics.drawRectUV( classMat, new Rect(0, 0, 1, 1), new Rect(125-32, 116-32, 32, 32) );
			}
		}
//		
//		private void DrawImage(Texture texture, Rect rect)
//		{
//			if(texture != null) {
//				imageContainer_mc.graphics.drawRectUV(texture,
//					new Rect(0, 0, 1, 1),
//					rect);
//			}
//		}
		
		public ClassroomButton(Classroom classroom) : base( Main.SWF, "HatchUserButton" )
		{
			name_txt = getChildByName<MovieClip>( "name_mc" ).getChildByName<TextField>("name_txt");
			imageContainer_mc = getChildByName<MovieClip>( "imageContainer_mc" );
			
			Classroom = classroom;
		}
		
		public void Refresh()
		{
			if(classroom != null)
			{
				name_txt.text = classroom.Name;
				//UpdateImage();
			}
		}
		
		public void ClearGraphics()
		{
			imageContainer_mc.graphics.clear();
			if( teacherMat != null )
				Object.Destroy( teacherMat );
			if( classMat != null )
				Object.Destroy( classMat );
		}
		
		public void Dispose()
		{
			ClearGraphics();
			
			classroom = null;
		}
	}
}
