using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;

namespace SSGCore
{
	public class TryAgainMenu : MovieClip
	{
		public Action Again;
		public Action Another;
		
		public TryAgainMenu (String language) : base ( MovieClipFactory.PATH_MAIN, "TryAgainScreen" )
		{
			SoundEngine.Instance.StopAll();
			SoundEngine.Instance.PlayTheme( null );
			
			MovieClip sameSkill = getChildByName<MovieClip>( "TryAgain" );
			MovieClip newSkill = getChildByName<MovieClip>( "NewGame" );

			sameSkill.gotoAndStop( language );
			newSkill.gotoAndStop( language );

			sameSkill.addEventListener( MouseEvent.CLICK, (CEvent e) => { Again(); } );
			newSkill.addEventListener( MouseEvent.CLICK, (CEvent e) => { Another(); } );
			
			float scale = (float)Screen.width / this.width;
			y = (Screen.height - (this.height * scale)) / 2f;
			scaleX = scaleY = scale;
			
			AudioClip clip = SoundUtils.LoadGlobalSound( (language == "sp" ? "SPN-" : "") + "Narration/Luna Universal/Play-Again-Screen" );
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			bundle.AddClip(clip);
			SoundEngine.Instance.PlayBundle( bundle );
		}
	}
}
