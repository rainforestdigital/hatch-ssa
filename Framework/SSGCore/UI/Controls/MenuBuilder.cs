using System;
using HatchFramework;
using pumpkin.events;
using pumpkin.display;
using pumpkin.text;
using UnityEngine;

namespace SSGCore
{
	internal class MenuBuilder
	{
		
		private MovieClip container;
		private float lastY = 100;
		private BasicButton close_btn;
		
		public MenuBuilder ()
		{
			
			container = new MovieClip(Main.SWF, "MenuBG_Top");
			
			var close = container.getChildByName<MovieClip>("close_btn");
			if(close != null) {
				close_btn = new BasicButton(close);
				close_btn.Root.visible = false;
			}
		}
		
		public string Title
		{
			set
			{
				TextField title = container.getChildByName<TextField>("title_txt");
				if(title != null) {
					title.text = value;
				}
			}
		}
		
		public string Subtitle
		{
			set
			{
				TextField subtitle = container.getChildByName<TextField>("subtitle_txt");
				if(subtitle != null) {
					subtitle.text = value;
					lastY += 7 + Mathf.Ceil(subtitle.numLines)*26;
				}
			}
		}
		
		public MenuButton AddButton(string label, EventDispatcher.EventCallback handler)
		{
			return AddButtonInternal("MenuButton_Blue", label, handler);
		}
		
		public MenuButton AddRedButton(string label, EventDispatcher.EventCallback handler)
		{
			return AddButtonInternal("MenuButton_Red", label, handler);
		}
		
		public MenuButton AddButton(string label, string subtitle, EventDispatcher.EventCallback handler)
		{
			var button = AddButton(label, handler);
			button.SubText = subtitle;
			return button;
		}
		
		public void AddCloseButton(EventDispatcher.EventCallback handler)
		{
			if(close_btn != null) {
				close_btn.Root.visible = true;
				close_btn.Root.addEventListener(MouseEvent.CLICK, handler);
			}
			else {
				AddButton("Close", handler);
			}
		}
		
		private MenuButton AddButtonInternal(string symbol, string label, EventDispatcher.EventCallback handler)
		{
			var button = new MenuButton(Main.SWF, symbol);
			button.Label = label;
			button.y = lastY;
			button.x = (container.width - button.width) * 0.5f;
			//lastY += button.height + 10;
			// TODO: replace hard coding as soon as measurement can be done properly again
			lastY += 80;
			container.addChild(button);
			button.addEventListener(MouseEvent.CLICK, handler);
			return button;
		}

		void ResizeBackground ()
		{
			var background = container.getChildByName<MovieClip>("background_mc");
			if(background != null) {
				background.height = container.height;
				
				for(int i = 0; i < background.numChildren; ++i) {
					var child = background.getChildAt<Sprite>(i);
					
					var op = child.graphics.drawOPs[0];
					var srcRect = op.srcRect;
					float srcScale = srcRect.width / child.width;
					
					var graphics = background.graphics;
					graphics.drawRectUV(op.material, new Rect(srcRect.x, srcRect.y, srcRect.width, 40*srcScale),
						new Rect(0, 0, background.width, 40));
					graphics.drawRectUV(op.material, new Rect(srcRect.x, srcRect.y+40*srcScale, srcRect.width, 10*srcScale),
						new Rect(0, 40, background.width, background.height-80));
					graphics.drawRectUV(op.material, new Rect(srcRect.x, srcRect.yMax-40*srcScale, srcRect.width, 42*srcScale),
						new Rect(0, background.height-40, background.width, 42));
					background.removeChild(child);
				}
			}
		}
		
		public MovieClip Build()
		{
			container.height = lastY+40;
			ResizeBackground ();
			return container;
		}
	}
}

