using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using pumpkin.display;
using pumpkin;
using pumpkin.events;
using pumpkin.text;

namespace SSGCore
{
	public class MenuOverlayFrameView
	{
		public MovieClip target;
		public MovieClip TL;
		public MovieClip ML;
		public MovieClip BL;
		public MovieClip BM;
		public MovieClip BR;
		public MovieClip MR;
		public MovieClip TR;
		public MovieClip TM;
		
		public MenuOverlayFrameView (MovieClip swf, float _scale)
		{
			target = swf;
			scale = _scale;
			
			TL = target.getChildByName<MovieClip>("TL");
			ML = target.getChildByName<MovieClip>("ML");
			BL = target.getChildByName<MovieClip>("BL");
			BM = target.getChildByName<MovieClip>("BM");
			BR = target.getChildByName<MovieClip>("BR");
			MR = target.getChildByName<MovieClip>("MR");
			TR = target.getChildByName<MovieClip>("TR");
			TM = target.getChildByName<MovieClip>("TM");
			
			TL.mouseEnabled = false;
			ML.mouseEnabled = false;
			BL.mouseEnabled = false;
			BM.mouseEnabled = false;
			BR.mouseEnabled = false;
			MR.mouseEnabled = false;
			TR.mouseEnabled = false;
			TM.mouseEnabled = false;
			
			UpdateLayout();
		}
		
		private float scale = 1;		
		public void UpdateLayout()
		{
			TL.x = -1;
			TL.y = 0;
			
			BL.x = 0;
			BL.y = (Screen.height - (BL.height * scale)) - (BL.getBounds(BL).y * scale);
			
			BR.x = Screen.width - (BR.width * scale);
			BR.y = BL.y;
			
			TR.x = Screen.width - (TR.width * scale);
			TR.y = 0;
			
			float midWidth = ( Screen.width - (TR.width * scale) - (TL.width * scale) ) + 3f;
			
			TL.scaleX = TL.scaleY = scale;
			BL.scaleX = BL.scaleY = scale;
			BR.scaleX = BR.scaleY = scale;
			TR.scaleX = TR.scaleY = scale;
			
			ML.scaleX = ML.scaleY = scale;
			BM.scaleX = BM.scaleY = scale;
			TM.scaleX = TM.scaleY = scale;
			MR.scaleX = MR.scaleY = scale;
			
			ML.x = 0;
			ML.y = TL.y + TL.getBounds(TL.parent).height-1;
			float bump = scale > 1 ? 50 : 5;
			ML.scaleY = (float)(Screen.height+bump)/ML.getBounds(ML.parent).height;
			
			BM.x = BL.width-1;
			BM.y = BL.y;
			BM.scaleX *= midWidth / BM.getBounds(BM.parent).width;

			TM.x = TL.width-1;
			TM.y = 0;
			TM.scaleX *= midWidth / TM.getBounds(TM.parent).width;
			
			MR.x = TR.x;
			MR.y = TR.height;
			MR.scaleY = ML.scaleY;
		}
	}
}
