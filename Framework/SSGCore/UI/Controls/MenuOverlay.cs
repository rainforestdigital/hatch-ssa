using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin.display;
using pumpkin;
using pumpkin.events;
using pumpkin.text;
using pumpkin.swf;

namespace SSGCore
{
	public class MenuOverlay : MovieClip
	{		
		public MovieClip helpBtn;
		public MovieClip stopBtn;
		public MovieClip helpContinueBtn;
		public UserStatus userStatus;
		public MenuOverlayFrameView frame;
		public TeacherMode tBar;
		
		private SessionController sessionControl;
		private float scale = 0.9f;
		
		private string language;

		public bool HelpSelectedThisSession = false;

		public MenuOverlay( string swf, string symbolName, SessionController _sessionControl) : base( swf, symbolName )
		{			
			sessionControl = _sessionControl;
			HelpSelectedThisSession = false;

			// if we're on the ipad2
			if( Screen.width == 1024 ) scale = .75f;
			else if( PlatformUtils.GetPlatform() == Platforms.IOSRETINA ) scale = 1.5f;
			else if( PlatformUtils.GetPlatform() == Platforms.WIN ) scale = Screen.width/1920f;
			else scale = Screen.width/1280f * 0.75f;
			
			userStatus = new UserStatus(getChildByName<MovieClip>("userInfo"));
			userStatus.Root.scaleX = userStatus.Root.scaleY = scale;
			userStatus.init(sessionControl.CurrentConfig.user.Name, sessionControl.CurrentConfig.user.Texture, "");
			
			helpBtn = getChildByName<MovieClip>("helpBtn");
			stopBtn = getChildByName<MovieClip>("stopBtn");

			if( sessionControl.CurrentConfig.SPN_FLAG )
				SetMenuSpn();
			else
				SetMenuEng();
			 
			helpBtn.scaleX = helpBtn.scaleY = scale;
			stopBtn.scaleX = stopBtn.scaleY = scale;
			
			helpContinueBtn = MovieClipFactory.CreateHelpContinueButton();
			helpContinueBtn.x = Screen.width - helpContinueBtn.width - (Screen.width * 0.05f);
			helpContinueBtn.y = Screen.height * 0.2f;
			helpContinueBtn.alpha = .75f;
			helpContinueBtn.visible = false;
			helpContinueBtn.FitToParent();
			
			helpBtn.addEventListener(MouseEvent.CLICK, HandleHelpClick);
			stopBtn.addEventListener(MouseEvent.CLICK, HandleStopClick);
			helpContinueBtn.addEventListener( MouseEvent.CLICK, HandleHelpContinueClick );
			
			helpBtn.x = Screen.width - (helpBtn.width + 10);
			helpBtn.y = 10;
			
			stopBtn.x = 10;
			stopBtn.y = Screen.height - (stopBtn.height + 10);	
			
			frame = new MenuOverlayFrameView(getChildByName<MovieClip>("BG"), scale);
			
			addChild( helpContinueBtn );
		}
		
		public void hide()
		{
			visible = false;
			mouseChildrenEnabled = false;
		}
		
		public void show()
		{
			visible = true;
			mouseChildrenEnabled = true;
		}
	
		public void Unload()
		{
			removeChild( helpContinueBtn );
			helpBtn.removeEventListener(MouseEvent.CLICK, HandleHelpClick);
			stopBtn.removeEventListener(MouseEvent.CLICK, HandleStopClick);
			helpContinueBtn.removeEventListener( MouseEvent.CLICK, HandleHelpContinueClick );
			userStatus.Destroy();
		}
		
		public void StartMonitorProgress()
		{
			canMonitor = true;
			TimerUtils.SetTimeout(1f, MonitorProgress);
		}
		
		public void MonitorProgress()
		{
			if(canMonitor && userStatus != null && sessionControl.CurrentSkill != null)
			{
				userStatus.UpdateProgress(sessionControl.CurrentSkill.GetPercentComplete());
			 	TimerUtils.SetTimeout(1f, MonitorProgress);
			}
		}
		
		public void SetMenuEng()
		{
			stopBtn.gotoAndStop ("en");
			helpBtn.gotoAndStop ("en");
			language = "";
		}
		
		public void SetMenuSpn()
		{
			stopBtn.gotoAndStop ("sp");
			helpBtn.gotoAndStop ("sp");
			language = " - Spanish";
		}
		
		private bool canMonitor = false;
		public void CancelMonitor()
		{
			canMonitor = false;
		}
		
		public void SetSkillName(string skill)
		{
			userStatus.SetSkillTitle( skill + language );
		}
		
		public string GetSkillName()
		{
			return userStatus.GetSkillTitle();
		}
		
		public void SetSkillLevel(SkillLevel level)
		{
			switch(level)	
			{
				case SkillLevel.Completed:
					userStatus.LevelType = LevelTypes.CD;
				break;
				
				case SkillLevel.Developed:
					userStatus.LevelType = LevelTypes.DD;
				break;
				
				case SkillLevel.Developing:
					userStatus.LevelType = LevelTypes.DG;
				break;
				
				case SkillLevel.Emerging:
					userStatus.LevelType = LevelTypes.EG;
				break;
				
				case SkillLevel.Tutorial:
					userStatus.LevelType = LevelTypes.TT;
				break;
			}
			
			userStatus.UpdateProgress(0);
		}
		
		private void HandleHelpClick(CEvent e)
		{
			DebugConsole.Log("HELP CLICKED");
			sessionControl.HelpMode = true;
			HelpSelectedThisSession = true;
		}
		
		private void HandleStopClick(CEvent e)
		{
			DebugConsole.Log("STOP CLICKED");
			sessionControl.Stop();
			
		}
		
		private void HandleHelpContinueClick( CEvent e )
		{
			DebugConsole.Log("HELP CONTINUE CLICKED");
			if(Time.time - helpWait >= 1.65f)
				sessionControl.HelpMode = false;
			else
			{
				helpContinueBtn.mouseChildrenEnabled = false;
				helpContinueBtn.mouseEnabled = false;
				SSGEngine.Instance.StartCoroutine(waitForClearHelp());
			}
		}
		
		public void clickOff()
		{
			helpBtn.mouseEnabled = false;
			helpBtn.mouseChildrenEnabled = false;
			
			stopBtn.mouseEnabled = false;
			stopBtn.mouseChildrenEnabled = false;
			
			helpContinueBtn.mouseEnabled = false;
			helpContinueBtn.mouseChildrenEnabled = false;
		}
		
		public void clickOn()
		{
			helpBtn.mouseEnabled = true;
			helpBtn.mouseChildrenEnabled = true;
			
			stopBtn.mouseEnabled = true;
			stopBtn.mouseChildrenEnabled = true;
			
			helpContinueBtn.mouseEnabled = true;
			helpContinueBtn.mouseChildrenEnabled = true;
		}
		
		private IEnumerator waitForClearHelp()
		{
			yield return new WaitForSeconds( 1.65f - (Time.time - helpWait) );
			
			sessionControl.HelpMode = false;
			helpContinueBtn.mouseChildrenEnabled = true;
			helpContinueBtn.mouseEnabled = true;
		}
		
		public void HelpButtonEnabled( bool isEnabled )
		{
			helpBtn.mouseEnabled = ( HelpSelectedThisSession ) ? false : isEnabled;
			
			if(helpBtn.mouseEnabled)
			{
				helpBtn.alpha = 1;
			}
			else
			{
				helpBtn.alpha = 0.5f;
			}
			
		}

		public void StopButtonEnabled( bool isEnabled )
		{
			stopBtn.mouseEnabled = isEnabled;
			
			if(stopBtn.mouseEnabled)
			{
				stopBtn.alpha = 1;
			}
			else
			{
				stopBtn.alpha = 0.5f;
			}
			
		}
		
		private float helpWait;
		public void HelpContinueButtonEnabled( bool isEnabled )
		{
			helpWait = Time.time;
			
			helpContinueBtn.visible = isEnabled;
		}
		
		public void addTeacherMode( TeacherMode _bar )
		{
			tBar = _bar;


			addChild(tBar);
			removeChild(userStatus.Root);
			addChild(userStatus.Root);
		}
		
		public void removeTeacherMode()
		{
			if(tBar != null)
				removeChild(tBar);
			tBar = null;
		}
	}
}
