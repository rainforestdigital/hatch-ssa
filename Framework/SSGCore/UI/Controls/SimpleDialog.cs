using System;
using pumpkin.display;
using HatchFramework;
using pumpkin.text;
using pumpkin.events;
using UnityEngine;

namespace SSGCore
{
	// A simple, self-closing dialog view
	public class SimpleDialog : DisplayObjectContainer
	{
		private BasicButton cancel_btn;
		private BasicButton okay_btn;
		
		public Action OkayClicked;
		public Action CancelClicked;
		
		public SimpleDialog (DisplayObjectContainer clip, string title, string description)
		{
			width = Screen.width;
			height = Screen.height;
			
			MovieClip blocker = new MovieClip();
			blocker.graphics.drawSolidRectangle(new Color(0, 0, 0, 0.5f), 0, 0, width, height);
			addChild(blocker);
			
			addChild(clip);
			clip.ScaleRelativeToHeight();
			const float scale = 1.5f;
			clip.scaleY *= scale;
			clip.scaleX *= scale;
			clip.AlignCentered();
			
			var okay = clip.getChildByName<MovieClip>("okay_btn");
			if(okay != null) {
				okay_btn = new BasicButton(okay);
				okay_btn.Root.addEventListener(MouseEvent.CLICK, OnOkayClick);
			}
			
			var cancel = clip.getChildByName<MovieClip>("cancel_btn");
			if(cancel != null) {
				cancel_btn = new BasicButton(cancel);
				cancel_btn.Root.addEventListener(MouseEvent.CLICK, OnCancelClick);
			}
			
			var title_txt = clip.getChildByName<TextField>("title_txt");
			if(title_txt != null && title != null) {
				title_txt.text = title;
			}
			
			var description_txt = clip.getChildByName<TextField>("description_txt");
			if(description_txt != null && description != null) {
				description_txt.text = description;
			}
		}
		
		private void RemoveFromParent()
		{
			if(parent != null) {
				parent.removeChild(this);
			}
		}
		
		private void OnOkayClick(CEvent e)
		{
			if(OkayClicked != null) {
				OkayClicked();
			}
			RemoveFromParent();
		}
		
		private void OnCancelClick(CEvent e)
		{
			if(CancelClicked != null) {
				CancelClicked();
			}
			RemoveFromParent();
		}
				
			
	}
}

