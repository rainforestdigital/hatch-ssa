using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;

namespace SSGCore
{
	public class MenuTrigger : DisplayObjectContainer
	{
		
		private MovieClip blocker_mc;
		private MovieClip prompt_mc;
		
		private DisplayObjectContainer options;
		
		private LabeledButton option_TL;
		private LabeledButton option_TR;
		private LabeledButton option_BL;
		private LabeledButton option_BR;
		
		private List<LabeledButton> option_btns;
		private List<int> option_numbers;
		
		private int currentCombination = 0;
		private float startTime;
		private bool enabled = true;
		public Action OnTriggered;
		public Action OnComboMenu;
		public Action OnComboClear;
		
		public MenuTrigger()
		{
			init();
		}
		
		public void SetEnabled(bool newEnabled)
		{
			enabled = newEnabled;
		}
		
		private void init()
		{
			option_btns = new List<LabeledButton>();
			
			option_numbers = new List<int>{1, 2, 3, 4};
			
			blocker_mc = new MovieClip();
			blocker_mc.addEventListener( MouseEvent.CLICK, onBackgroundClick );
			addChild(blocker_mc);
			blocker_mc.visible = false;
			
			prompt_mc = new MovieClip( Main.SWF, "MenuTrigger_Prompt" );
			addChild(prompt_mc);
			prompt_mc.visible = false;
			prompt_mc.mouseEnabled = false;
			
			options = new DisplayObjectContainer();
			addChild(options);
			options.visible = false;
			
			option_TL = new LabeledButton( Main.SWF, "MenuTrigger_OptionButton" );
			option_TL.addEventListener( MouseEvent.MOUSE_DOWN, onOptionClick );
			option_btns.Add( option_TL );
			options.addChild( option_TL );
			
			option_TR = new LabeledButton( Main.SWF, "MenuTrigger_OptionButton" );
			option_TR.addEventListener( MouseEvent.MOUSE_DOWN, onOptionClick );
			option_btns.Add( option_TR );
			options.addChild( option_TR );
			
			option_BL = new LabeledButton( Main.SWF, "MenuTrigger_OptionButton" );
			option_BL.addEventListener( MouseEvent.MOUSE_DOWN, onOptionClick );
			option_btns.Add( option_BL );
			options.addChild( option_BL );
			
			option_BR = new LabeledButton( Main.SWF, "MenuTrigger_OptionButton" );
			option_BR.addEventListener( MouseEvent.MOUSE_DOWN, onOptionClick );
			option_btns.Add( option_BR );
			options.addChild( option_BR );
			
			addEventListener( CEvent.ENTER_FRAME, update );
			
			
		}
		
		public void Resize(int width, int height)
		{
			this.width = width;
			this.height = height;
			
			
			options.width = width;
			options.height = height;
			
			blocker_mc.width = width;
			blocker_mc.height = height;
			
			prompt_mc.ScaleAsIfFullScreen(1.5f);
			prompt_mc.AlignCentered();
			
			foreach(var button in option_btns) {
				button.ScaleAsIfFullScreen(1);
			}
			
			option_TR.AlignTopRight();
			option_BL.AlignBottomLeft();
			option_BR.AlignBottomRight();
		}
		
		private bool OverTriggerArea(){
			
			float size = Screen.height*0.25f;
			return Input.mousePosition.x < size && (Screen.height-Input.mousePosition.y) < size;
		}
		
		private bool CanShowTrigger()
		{
			return enabled && !options.visible;
		}

		private void update( CEvent e )
		{
			if(CanShowTrigger() && Input.GetMouseButton(0) && OverTriggerArea()){
				
				if(startTime == 0) {
					Debug.Log ("Finger is down: "+stage.stageMouseX+" "+stage.stageMouseY);
					startTime = Time.time;
				}
			
				float dt = Time.time - startTime;
				
				if(dt >= 3)
				{
					startTime = 0;
					startCombinationMenu();
				}
			}else{
				startTime = 0;
			}
		}
		
		private void onOptionClick( CEvent e )
		{
			LabeledButton btn = e.currentTarget as LabeledButton;
			int clickNumber = int.Parse( btn.Label );
			
			if(currentCombination + 1 == clickNumber)
			{
				SoundUtils.PlaySimpleEffect("click");
				currentCombination++;
			}
			else
			{
				SoundUtils.PlaySimpleEffect("pop");
				stopCombinationMenu();
				return;
			}
			
			if(currentCombination == 4)
			{
				OnComboClear = null;
				stopCombinationMenu();
				if(OnTriggered != null) {
					OnTriggered();
				}
			}
		}
		
		private void onBackgroundClick( CEvent e )
		{
			SoundUtils.PlaySimpleEffect("pop");
			stopCombinationMenu();
		}
		
		//Starts the touch combination process for showing the Teacher Menu
		private void startCombinationMenu()
		{
			if( OnComboMenu != null )
				OnComboMenu();
			
			while(option_numbers[0] == 1)
			{
				option_numbers.Shuffle();
			}
			
			for(int i = 0; i < option_btns.Count; i++)
			{
				option_btns[i].Label = option_numbers[ i ].ToString();
			}
			
			currentCombination = 0;
			
			options.visible = true;
			
			
			blocker_mc.visible = true;
			blocker_mc.graphics.clear();
			blocker_mc.graphics.drawSolidRectangle(new Color(0,0,0,0.75f), 0, 0, width, height);
			prompt_mc.visible = true;
		}
		
		private void stopCombinationMenu()
		{
			if( OnComboClear != null )
				OnComboClear();
			
			blocker_mc.visible = false;
			options.visible = false;
			prompt_mc.visible = false;
		}
	}
}
