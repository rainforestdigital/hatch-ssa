using UnityEngine;
using System.Collections;

using pumpkin;
using pumpkin.display;
using pumpkin.text;

public enum LevelTypes
{
	BLANK,
	TT,
	EG,
	DG,
	DD,
	CD,
	LOCKED
}

namespace SSGCore
{

	public class UserStatus : MovieClip
	{
		
		private MovieClip root;
		public MovieClip Root
		{
			get{ return root; } 
		}
		
		private TextField title_text;
		private TextField userName;
		private MovieClip level_mc;
		private MovieClip userImg;
		private MovieClip progressBar;
		private Sprite userBlock;
		private Material userMaterial;
		
		private LevelTypes levelType;
		public LevelTypes LevelType
		{
			get
			{
				return levelType;
			}
			
			set
			{
				levelType = value;
				updateLevelType();
			}
		}
		
		public UserStatus( string linkage ) : base( linkage )
		{
			root = this;
		}
		
		public UserStatus( string swf, string symbolName ) : base( swf, symbolName )
		{
			root = this;
		}
		
		public UserStatus( MovieClip mc ) : base()
		{
			root = mc;
		}
		
		public void init(string _userName, Texture img, string skill)
		{
		
			levelType = LevelTypes.DG;
			
			progressBar = root.getChildByName<MovieClip>("progressBar");
			
			title_text = root.getChildByName<TextField>( "title_txt" );
			title_text.text = skill;
			
			level_mc = root.getChildByName<MovieClip>( "level_mc" );
			level_mc.gotoAndStop( levelType.ToString() );

			userName = root.getChildByName<TextField>("userName");
			SetUserName(_userName);

			userImg = root.getChildByName<MovieClip>("userImg");
			if( img != null ) SetImage(img);
			
			userBlock = new Sprite();
			root.addChild( userBlock );
			userBlock.addChild( userImg );
			userBlock.addChild( userName );
			userImg.y = 0;
			userName.y = userImg.height + 5f;

			userBlock.y = root.getBounds (root).height * 0.5f - userBlock.getBounds( root ).height * 0.5f;
		}
		
		public void UpdateProgress(float p)
		{
			progressBar.gotoAndStop(Mathf.FloorToInt(p*100));
		}
		
		public void SetSkillTitle(string _title)
		{
			title_text.text = _title;
		}
		
		public string GetSkillTitle()
		{
			return title_text.text;
		}
		
		public void SetUserName(string _name)
		{
			userName.text = _name;
		}
		
		public void SetImage(Texture texture)
		{
			var graphics = userImg.graphics;
			graphics.clear();
			
			if(texture != null) {
				if( userMaterial == null )
					userMaterial = new Material( Shader.Find("Sprites/Default") );
				userMaterial.mainTexture = texture;
				
				graphics.drawRectUV(userMaterial,
					new Rect(0, 0, 1, 1),
					new Rect(0, 0, 75, 70));
			}
		}
		
		private void updateLevelType()
		{
			level_mc.gotoAndStop( levelType.ToString() );

			progressBar.getChildByName<MovieClip>("bg").gotoAndStop( levelType.ToString() );
			progressBar.gotoAndStop(1);
		}
		
		public void Destroy()
		{
			if( userMaterial != null )
				UnityEngine.Object.Destroy( userMaterial );
		}
	}
}
