using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;

namespace SSGCore
{
	public class ConfirmationMenu : MovieClip
	{
		public Action Accept;
		public Action Reject;
		
		private MovieClip root;
		public MovieClip Root
		{
			get{ return root; } 
		}
		
		private BasicButton yesBtn;
		public BasicButton YesButton
		{
			get
			{
				return yesBtn;
			}
		}
		
		private BasicButton noBtn;
		public BasicButton NoButton
		{
			get
			{
				return noBtn;
			}
		}
		
		public ConfirmationMenu( string linkage, string _language ) : base( linkage )
		{
			root = this;
			this.language = _language;
			init();
		}

		public ConfirmationMenu( string swf, string symbolName, string _language  ) : base( swf, symbolName )
		{
			root = this;
			this.language = _language;
			init();
		}
		
		public ConfirmationMenu( MovieClip mc, string _language ) : base()
		{
			root = mc;
			this.language = _language;
			init();
		}
		
		public virtual void Unload()
		{
			if( yesTimer != null ) yesTimer.Unload();
			if( noTimer != null ) noTimer.Unload();
			if( overallTimer != null ) overallTimer.Unload();
			GameObject.Destroy(audioPlayer);
		}
		
		protected GameObject audioPlayer;
		protected string language;
		private MovieClip henryYes;
		private MovieClip henryNo;
		private TimerObject yesTimer;
		private TimerObject noTimer;
		private TimerObject overallTimer;
		private void init()
		{
			root.gotoAndStop( language );

			yesBtn = new BasicButton( root.getChildByName<MovieClip>( "yesBtn" ) );
			noBtn = new BasicButton( root.getChildByName<MovieClip>( "noBtn" ) );
			henryYes = root.getChildByName<MovieClip>( "henryYes" );
			henryNo = root.getChildByName<MovieClip>( "henryNo" );
			
			henryYes.gotoAndStop(1);
			henryNo.gotoAndStop(1);
			
			yesBtn.Root.addEventListener(MouseEvent.CLICK, HandleYesClicked);
			noBtn.Root.addEventListener(MouseEvent.CLICK, HandleNoClicked);
			
			timerCounter_0 = 0;
			timerCounter_1 = 0;
			
			playMessage();
			
			overallTimer = TimerUtils.SetGUITimer(HandleOverAllTimer);
			startTime = Time.realtimeSinceStartup;
			
			float scale = Mathf.Abs((float)Screen.height / (float)MainUI.STAGE_HEIGHT);
			
			DebugConsole.LogWarning("Confirmation Scale: {0}", scale);
			
			root.scaleX = root.scaleY = scale * 1.5f;
			
		}
		
		protected virtual void playMessage()
		{
			if( audioPlayer != null )
				GameObject.Destroy( audioPlayer );
			audioPlayer = new GameObject("audioPlayerForConfirmationMenu");
			audioPlayer.AddComponent<AudioSource>();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.GLOBAL , (language == "sp" ? "SPN-" : "") + "exit-message" );
			audioPlayer.audio.clip = clip;
			audioPlayer.audio.ignoreListenerPause = true;
			audioPlayer.audio.Play();
		}
		
		private float startTime = 0;
		private float lastFrameTime = 0;
		private bool hasNodded = false;
		private bool hasShaked = false;
		private void HandleOverAllTimer()
		{
			if( Time.realtimeSinceStartup - lastFrameTime >= 1f/60f){
				if( Time.realtimeSinceStartup - startTime >= 2.6f && Time.realtimeSinceStartup - startTime < 3f && !hasNodded )
				{
					hasNodded = true;
					HandleNodTimer();
					lastFrameTime = Time.realtimeSinceStartup;
				}
				else if( Time.realtimeSinceStartup - startTime >= 3.3f && Time.realtimeSinceStartup - startTime < 4f && !hasShaked )
				{
					hasShaked = true;
					HandleShakeTimer();
					lastFrameTime = Time.realtimeSinceStartup;
				}
			}
		}
		
		protected void resetAnim()
		{
			hasNodded = false;
			hasShaked = false;
			timerCounter_0 = 0;
			yesCounter = 0;
			timerCounter_1 = 0;
			noCounter = 0;
			startTime = Time.realtimeSinceStartup;
		}
		
		private void HandleNodTimer()
		{
			yesTimer = TimerUtils.SetGUITimer(HandleYesFrameTimer);
		}
		
		private void HandleShakeTimer()
		{
			noTimer = TimerUtils.SetGUITimer(HandleNoFrameTimer);
		}
		
		private float timerCounter_0 = 0;
		private int yesCounter = 0;
		private void HandleYesFrameTimer()
		{
			if( yesCounter == 2 )
			{
				yesTimer.StopTimer();
				yesTimer.Unload();
				return;
			}
			
			timerCounter_0++;
			if( timerCounter_0 % 2 != 0 ) return;
			
			int ycf = henryYes.currentFrame == henryYes.totalFrames ? 1 : henryYes.currentFrame + 1;
			yesCounter = henryYes.currentFrame == henryYes.totalFrames ? yesCounter + 1 : yesCounter;
			henryYes.gotoAndStop(ycf);
			
		}
		
		private float timerCounter_1 = 0;
		private int noCounter = 0;
		private void HandleNoFrameTimer()
		{
			if( noCounter == 2 )
			{
				noTimer.StopTimer();
				noTimer.Unload();
				return;
			}
			
			timerCounter_1++;
			if( timerCounter_1 % 2 != 0 ) return;
			
			int ncf = henryNo.currentFrame == henryNo.totalFrames ? 1 : henryNo.currentFrame + 1;
			noCounter = henryNo.currentFrame == henryNo.totalFrames ? noCounter + 1 : noCounter;
			henryNo.gotoAndStop(ncf);
		}
		
		private void HandleYesClicked(CEvent e)
		{
			DebugConsole.Log("YES CLICKED");
			if( audioPlayer != null ) audioPlayer.audio.Stop();
			Accept();
		}
		
		private void HandleNoClicked(CEvent e)
		{
			DebugConsole.Log("NO CLICKED");
			if( audioPlayer != null ) audioPlayer.audio.Stop();
			Reject();
		}
	}
}