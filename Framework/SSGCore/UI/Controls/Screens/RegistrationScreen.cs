using System;
using pumpkin.display;
using pumpkin.events;
using HatchFramework;
using pumpkin.text;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using SSGCore.RMS.Webservice;

namespace SSGCore
{
	public class RegistrationScreen : GameScreen
	{
		private DisplayObjectContainer view = new DisplayObjectContainer();
		private DisplayObjectContainer container;
		
		private BasicButton accept_btn;
		private BasicButton exitBtn;
		
		public Action Reject;
		public Action<RegistrationResult> Accept;
		public Action ManualRegistration;
		
		private RegistrationError errorDialog;
		private TextInput licenseTxt;
		private MovieClip darkBG;
		private Vector2 originalPos;
		
		private bool _waiting;
		private bool waiting
		{
			set
			{ 
				_waiting = value;
				if(_waiting)
				{
					if( accept_btn.Root.hasEventListener(MouseEvent.CLICK) ) accept_btn.Root.removeEventListener(MouseEvent.CLICK, OnAcceptClick);
					accept_btn.Root.alpha = 0.5f;
				}
				else
				{
					if( !accept_btn.Root.hasEventListener(MouseEvent.CLICK) ) accept_btn.Root.addEventListener(MouseEvent.CLICK, OnAcceptClick);
					accept_btn.Root.alpha = 1f;
				}
			}
			get{ return _waiting; }
		}
		
		public RegistrationScreen ()
		{
			container = new DisplayObjectContainer();
			container.width = Main.STAGE_WIDTH;
			container.height = Main.STAGE_HEIGHT;
			view.addChild(container);
			
			var clip = new MovieClip(Main.SWF, "RegisterView");
			darkBG = DisplayObjectUtils.CreateBlocker();
			darkBG.visible = false;
			
			container.addChild(clip);
			
			if( PlatformUtils.GetPlatform() != Platforms.IOS && PlatformUtils.GetPlatform() != Platforms.IOSRETINA )
			{
				exitBtn = new BasicButton(clip.getChildByName<MovieClip>("exitBtn"));
				exitBtn.Root.addEventListener(MouseEvent.CLICK, OnRejectClick);
			}
			else
				clip.getChildByName<MovieClip>("exitBtn").alpha = 0;
			
			accept_btn = new BasicButton(clip.getChildByName<MovieClip>("accept_btn"));
			accept_btn.Root.addEventListener(MouseEvent.CLICK, OnAcceptClick);
			
			licenseTxt = new TextInput(clip.getChildByName<TextField>("license_txt"));
			licenseTxt.Text = GlobalConfig.GetProperty("RegistrationLicense");

			errorDialog = new RegistrationError(clip.getChildByName<MovieClip>("errorDialog"));
			clip.AlignCentered();

#if UNITY_IPHONE || UNITY_ANDROID			
			licenseTxt.OnHasFocus = (bool hasFocus) =>
			{
				darkBG.visible = hasFocus;
				if( hasFocus )
				{
					clip.y = 10;
				}
				else
				{
					clip.y =  originalPos.y;
				}
			};
			
			originalPos = new Vector2(clip.x, clip.y);
			view.addChild(darkBG);
			
#endif
			view.addChild(errorDialog.view);
			
			if(!InternetConnectivity.hasConnection)
				ShowInternetError();
			
			waiting = false;
		}
		
		
		void ShowInternetError(){
		
			var mc = new MovieClip(Main.SWF, "PromptDialog");
			var dialog = new SimpleDialog(mc, "Internet Connection Required", "An internet connection is required for registration.  Please connect and restart the application");
			dialog.OkayClicked = OnRejectClick;
			view.addChild(dialog);
		
		}
		
		void OnRejectClick(){
			OnRejectClick(null);	
		}
		
		
		void OnRejectClick(CEvent e)
		{
			if( errorDialog.IsShowing ) return;
			DebugConsole.Log("EXIT CLICKED");
			if(Reject != null) {
				Reject();
			}
		}
		
		void OnAcceptClick(CEvent e)
		{
			if( errorDialog.IsShowing ) return;
			//(new RegisterDevice()).Fetch(GlobalConfig.GetProperty("TEST_LICENSE"), "95170359212376064", "test", OnRegAccepted);
			(new RegisterDevice()).Fetch(licenseTxt.Text, PlatformUtils.GetSerial(), PlatformUtils.GetDeviceModel(), OnRegAccepted);
			//(new CheckSerialNum()).Fetch (licenseTxt.text, OnRegAccepted);
			waiting = true;
		}
		
		private float triggerCompleteTime = 0;
		private bool OverTrigger() {
			float size = Screen.height*0.25f;
			return Input.GetMouseButton(0) && (Screen.width - Input.mousePosition.x) < size && (Screen.height-Input.mousePosition.y) < size;
		}
		
		private void CheckForManualTrigger(CEvent e)
		{			
			if(OverTrigger()) {
				if(triggerCompleteTime == 0) {
					triggerCompleteTime = Time.time + GlobalConfig.GetProperty<int>("ManualRegistrationDelay", 3);
				}
				
				if(Time.time >= triggerCompleteTime) {
					DebugConsole.Log("Manual Registration trigger");
					if(ManualRegistration != null) {
						ManualRegistration();
					}
					view.removeEventListener(CEvent.ENTER_FRAME, CheckForManualTrigger);
				}
			} else {
				triggerCompleteTime = 0;
			}
		}
		
		private void OnRegAccepted(bool status, Dictionary<string, string> deviceSettings)
		{			
			waiting = false;
			DebugConsole.Log("ON REG ACCEPTED: " + status);
			if( status ) 
			{
				Accept(new RegistrationResult(deviceSettings));
			}
			else
			{
				// show dialog
				errorDialog.Show();
			}
		}
		
		public DisplayObject View{ get { return view; } }

		public void Resize (int width, int height)
		{
			view.width = width;
			view.height = height;
			container.FillParent();
		}

		public void Start(){
#if !UNITY_IOS
			view.addEventListener(CEvent.ENTER_FRAME, CheckForManualTrigger);
#endif
		}
		
		public void Dispose ()
		{
			
			view.removeEventListener(CEvent.ENTER_FRAME, CheckForManualTrigger);
		}
	}
}

