using System;
using pumpkin.display;
using pumpkin.events;
using HatchFramework;
using pumpkin.text;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using SSGCore.RMS.Webservice;

namespace SSGCore
{
	public class ManualRegistrationScreen : GameScreen
	{
		private DisplayObjectContainer view = new DisplayObjectContainer();
		private DisplayObjectContainer container;
		private MovieClip clip;
		
		public Action Cancel;
		public Action<RegistrationResult> Registered;
		public Action ManualRegistration;
		
		private TextInput orderNumberTxt;
		private TextInput serialNumberTxt;
		private float clipY;
		
		private BasicButton cancelBtn;
		private BasicButton accept_btn;
		
		private bool _waiting;
		private bool waiting
		{
			set
			{ 
				_waiting = value;
				if(_waiting)
				{
					if( accept_btn.Root.hasEventListener(MouseEvent.CLICK) ) accept_btn.Root.removeEventListener(MouseEvent.CLICK, OnAcceptClick);
					accept_btn.Root.alpha = 0.5f;
				}
				else
				{
					if( !accept_btn.Root.hasEventListener(MouseEvent.CLICK) ) accept_btn.Root.addEventListener(MouseEvent.CLICK, OnAcceptClick);
					accept_btn.Root.alpha = 1f;
				}
			}
			get{ return _waiting; }
		}
		
		public ManualRegistrationScreen ()
		{
			container = new DisplayObjectContainer();
			container.width = Main.STAGE_WIDTH;
			container.height = Main.STAGE_HEIGHT;
			view.addChild(container);
			
			clip = new MovieClip(Main.SWF, "ManualRegisterView");
			
			container.addChild(clip);
			clip.AlignCentered();
			clipY = clip.y;
			
			cancelBtn = new BasicButton(clip.getChildByName<MovieClip>("cancelBtn"));
			cancelBtn.Root.addEventListener(MouseEvent.CLICK, OnCancelClick);
			
			accept_btn = new BasicButton(clip.getChildByName<MovieClip>("accept_btn"));
			accept_btn.Root.addEventListener(MouseEvent.CLICK, OnAcceptClick);
			
			orderNumberTxt = new TextInput(clip.getChildByName<TextField>("orderNumber_txt"));
			orderNumberTxt.Text = "";
			orderNumberTxt.OnHasFocus = OnTextFocus;
			
			var serialTF = clip.getChildByName<TextField>("serial_txt");
			serialNumberTxt = new TextInput(serialTF);
			serialNumberTxt.Text = "";
			serialNumberTxt.OnHasFocus = OnTextFocus;
			
#if UNITY_ANDROID
			serialNumberTxt.Text = PlatformUtils.GetSerial();
			serialTF.type = TextFieldType.DYNAMIC;
#endif
			waiting = false;
		}
		
		private void OnTextFocus(bool focused)
		{
#if UNITY_IPHONE || UNITY_ANDROID			
			clip.y = focused ? 10 : clipY;
#endif
		}
		
		
		void OnCancelClick(CEvent e)
		{
			if(Cancel != null) {
				Cancel();
			}
		}
		
		void OnAcceptClick(CEvent e)
		{
			if(string.IsNullOrEmpty(orderNumberTxt.Text)) {
				ShowErrorDialog("Order Number Required", "");
				return;
			}
			
			if(string.IsNullOrEmpty(serialNumberTxt.Text)) {
				ShowErrorDialog("Serial Number Required", "");
				return;
			}
			
			(new ManualRegisterDevice()).Fetch(orderNumberTxt.Text, serialNumberTxt.Text, PlatformUtils.GetDeviceModel(), OnRegAccepted);
			
			waiting = true;
		}

		void ShowErrorDialog (string title, string description)
		{
			var mc = new MovieClip(Main.SWF, "ConfirmationDialog");
			var dialog = new SimpleDialog(mc, title, description);
			view.addChild(dialog);
		}
		
		private void OnRegAccepted(bool status, Dictionary<string, string> deviceSettings)
		{			
			waiting = false;
			DebugConsole.Log("ON REG ACCEPTED: " + status);
			if( status ) 
			{
				var result = new RegistrationResult(deviceSettings);
				result.orderNumber = orderNumberTxt.Text;
				if(Registered != null) Registered( result );
			}
			else if(deviceSettings != null && deviceSettings.ContainsKey("error"))
			{
				ShowErrorDialog ("Error Processing Request", "Unable to update the RMS. Error: " + deviceSettings["error"]);
			}
			else
			{
				ShowErrorDialog ("Error Processing Request", "Unable to connect to RMS");
			}
		}
		
		public DisplayObject View{ get { return view; } }

		public void Resize (int width, int height)
		{
			view.width = width;
			view.height = height;
			container.FillParent();
		}

		public void Start() {
		}
		
		public void Dispose ()
		{
		}
	}
}

