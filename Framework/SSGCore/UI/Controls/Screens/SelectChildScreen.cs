using System;
using System.Collections.Generic;
using pumpkin.display;
using pumpkin.events;
using UnityEngine;
using HatchFramework;

namespace SSGCore
{
	public class SelectChildScreen : GameScreen, Refreshable
	{
		private DisplayObjectContainer view = new DisplayObjectContainer();
		private DisplayObjectContainer childrenView = new DisplayObjectContainer();
		private List<HatchUserButton> buttons = new List<HatchUserButton>();
		private Child selectedChild;
		private BasicButton back_btn;
		private Classroom classroom;
		
		Action<Child> ChildSelectedInvoker;
		public event Action<Child> ChildSelected{ add{ChildSelectedInvoker += value;} remove{ChildSelectedInvoker -= value;}}
		
		Action BackPressedInvoker;
		public event Action BackPressed{ add{BackPressedInvoker += value;} remove{BackPressedInvoker -= value;}}
		
		Action InvalidatedInvoker;
		public event Action Invalidated{ add{InvalidatedInvoker += value;} remove{InvalidatedInvoker -= value;}}
		
		
		public SelectChildScreen (Classroom classroom)
		{
			childrenView.width = Main.STAGE_WIDTH*0.75f;
			childrenView.height = Main.STAGE_HEIGHT*0.80f;
			view.addChild(childrenView);
			
			this.classroom = classroom;
			
			AddChildButtons (classroom.Children);
			
			// since it only occurs in two places, and the functionality
			// is relatively simple, the back button is internal to the
			// screens on which it appears. This may change if the circumstances
			// alter.
			back_btn = new BasicButton(Main.SWF, "Back_button");
			back_btn.addEventListener(MouseEvent.CLICK, OnBackClick);
			view.addChild(back_btn);
			Refresh();
		}
		
		public void Start()
		{			
			AnimationUtils.GrowIn(buttons);
			
			SoundUtils.PlaySimpleSound("touch_to_play");
		}

		public DisplayObject View 
		{
			get { return view; }
		}

		private void AddChildButtons (IEnumerable<User> children)
		{
			foreach(Child child in children) {
				if(buttons.Count < 32 && child.HasImage) {
					var button = new HatchUserButton();
					button.User = child;
					button.addEventListener(MouseEvent.CLICK, OnChildClick);
					buttons.Add(button);
				}
			}
		}
		public void Refresh()
		{
			foreach(var button in buttons) {
				button.Refresh();
			}
			
			// don't change if on verifier screen for now
			if(selectedChild == null) {
				childrenView.RemoveAll(buttons);
				
				foreach( HatchUserButton b in buttons )
				{
					b.Dispose();
				}
				
				buttons.Clear();
				AddChildButtons(classroom.Children);
				LayoutUtils.ResetScale(buttons);
				childrenView.LayoutTiled (buttons, 1f, 1f);//, Screen.height);
			
				Resize(Screen.width, Screen.height);
			}
			
			if((!classroom.Valid || !classroom.HasChildren) && InvalidatedInvoker != null) {
				InvalidatedInvoker();
			}
		}
				
		private void OnChildClick(CEvent e)
		{
			var button = (HatchUserButton)e.currentTarget;
			var child = button.User;
			
			
			if(selectedChild == null) {
				selectedChild = (Child)child;
				
				const int optionCount = 4;
				var optionButtons = buttons.RandomSampleWith(optionCount, button);
				childrenView.RemoveAll(buttons);
				LayoutUtils.ResetScale(buttons);
				childrenView.LayoutTiled(optionButtons);
				AnimationUtils.GrowIn(optionButtons);
				SoundEngine.Instance.StopAll();
				SoundUtils.PlaySimpleSound("touch_again");
				
				MuseumDataObject.initiate( selectedChild );
			}
			else if(child == selectedChild) {
				Debug.Log("Selected Child: " + child.Name);
				SoundEngine.Instance.StopAll();
				if(ChildSelectedInvoker != null) {
					ChildSelectedInvoker(selectedChild);
				}
			}
			else {
				// cancel selection, and start over
				selectedChild = null;
				childrenView.RemoveAll(buttons);
				LayoutUtils.ResetScale(buttons);
				childrenView.LayoutTiled(buttons);
				AnimationUtils.GrowIn(buttons);
				
				SoundEngine.Instance.StopAll();
				var bundle = new SoundEngine.SoundBundle();
				bundle.AddClip(SoundUtils.LoadGlobalSound("not_you"));
				bundle.AddClip(SoundUtils.LoadGlobalSound("touch_to_play"));
				SoundEngine.Instance.PlayBundle(bundle);
			}
				
		}
		
		private void OnBackClick(CEvent e)
		{
			if(BackPressedInvoker != null)
			{
				BackPressedInvoker();
			}
		}

		public void Resize (int width, int height)
		{
			view.width = width;
			view.height = height - 100;
			childrenView.FitToParent();
			if( PlatformUtils.GetPlatform() != Platforms.IOS && PlatformUtils.GetPlatform() != Platforms.IOSRETINA )
				childrenView.y += 15;
			back_btn.ScaleAsIfFullScreen(2);
			back_btn.scaleY = back_btn.scaleX *= 0.5f;
			back_btn.AlignBottomLeft(5);
			back_btn.y += 100;
		}
		
		public void Dispose()
		{
			foreach( HatchUserButton b in buttons )
			{
				b.Dispose();
			}
		}
	}
}

