using System;
using System.Collections.Generic;
using System.Linq;
using pumpkin.display;
using pumpkin.events;
using HatchFramework;

namespace SSGCore
{
	public class SelectClassroomScreen : GameScreen, Refreshable
	{
		private DisplayObjectContainer view = new DisplayObjectContainer();
		private DisplayObjectContainer container = new DisplayObjectContainer();
		private List<ClassroomButton> buttons = new List<ClassroomButton>();
		private ICollection<Classroom> classrooms;
		
		public SelectClassroomScreen (ICollection<Classroom> classrooms)
		{
			container.width = MainUI.STAGE_WIDTH;
			container.height = MainUI.STAGE_HEIGHT;
			view.addChild(container);
			
			this.classrooms = classrooms;
			CreateButtons ();
		}
		
		public void Start()
		{
			float scale;
			if(PlatformUtils.GetPlatform() == Platforms.ANDROID)
				scale = 1.75f;
			else if(PlatformUtils.GetPlatform() == Platforms.IOSRETINA)
				scale = 2.25f;
			else if(PlatformUtils.GetPlatform() == Platforms.WIN)
				scale = 2f;
			else
				scale = 2f;
			container.LayoutTiled(buttons, scale, scale);
			AnimationUtils.GrowIn(buttons);
		}
		
		Action<Classroom> ClassroomSelectedInvoker;
		public event Action<Classroom> ClassroomSelected{ add{ClassroomSelectedInvoker += value;} remove{ClassroomSelectedInvoker -= value;}}

		Action NoClassroomsInvoker;
		public event Action NoClassrooms{ add{NoClassroomsInvoker += value;} remove{NoClassroomsInvoker -= value;}}

		
		public DisplayObject View 
		{
			get { return view; }
		}

		void CreateButtons ()
		{
			foreach(var classroom in classrooms) {
				if(classroom.HasChildren) {
					var button = new ClassroomButton(classroom);
					button.addEventListener(MouseEvent.CLICK, OnClassroomClick);
					buttons.Add(button);
				}
			}
		}
		
		public void Refresh()
		{
			container.RemoveAll(buttons);
			
			foreach( ClassroomButton b in buttons )
			{
				b.ClearGraphics();
			}
			
			buttons.Clear();
			CreateButtons();
			
			LayoutUtils.ResetScale(buttons);
			float scale;
			if(PlatformUtils.GetPlatform() == Platforms.ANDROID)
				scale = 2f;
			else if(PlatformUtils.GetPlatform() == Platforms.IOSRETINA)
				scale = 2.25f;
			else if(PlatformUtils.GetPlatform() == Platforms.WIN)
				scale = 2f;
			else
				scale = 2f;
			container.LayoutTiled(buttons, scale, scale);
			
			if(!classrooms.Any(c => c.HasChildren) && NoClassroomsInvoker != null) {
				NoClassroomsInvoker();
			}
		}
				
		private void OnClassroomClick(CEvent e)
		{
			var button = (ClassroomButton)e.currentTarget;
			var theme = button.Classroom;
			if(ClassroomSelectedInvoker != null)
			{
				ClassroomSelectedInvoker(theme);
			}
		}

		public void Resize (int width, int height)
		{
			view.width = width;
			view.height = height;
			container.FitToParent();
		}
		
		public void Dispose()
		{
			SoundEngine.Instance.StopAll();
			foreach( ClassroomButton b in buttons )
			{
				b.ClearGraphics();
			}
		}
	}
}

