using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.text;
using System.Globalization;
using System;

namespace SSGCore
{

	public class EditChildScreen : GameScreen
	{
		public delegate void EditChildPhotoHandler(Child child, Texture2D newTexture);
	
		private DisplayObjectContainer view;
		public DisplayObject View{ get{ return view; } }
		
		private DisplayObjectContainer displayObjectContainer;
		
		//	Drop Downs
		private DropDown<Classroom> selectClass_DropDown;
		private DropDown<User> selectStudent_DropDown;
		
		//	Text Inputs
		private TextField firstName_txt;
		private TextField lastName_txt;
		private TextField birthDate_txt;
		private TextField language_txt;
		
		//	Buttons
		private BasicButton captureImage_btn;
		private BasicButton clear_btn;
		private BasicButton saveChild_btn;
		private BasicButton cancel_btn;
		
		//	Display objects
		private MovieClip frame;
		private MovieClip blankChildIcon;
		private MovieClip childPictureGuide;
		
		//	WebCam
		private WebCamCapture webCam;
		
		//	Data
		private IList<Classroom> classrooms;
		private Classroom currentClassroom;
		private Child currentChild;
		
		public System.Action CloseClicked;
		public EditChildPhotoHandler PhotoChanged;
		
		public EditChildScreen( IList<Classroom> classrooms )
		{
			view = new DisplayObjectContainer();
			displayObjectContainer = new DisplayObjectContainer();
			displayObjectContainer.width = MainUI.STAGE_WIDTH;
			displayObjectContainer.height = MainUI.STAGE_HEIGHT;
			
			var frameBackground = new MovieClip();
			frameBackground.graphics.drawSolidRectangle(new Color(0, 0, 0, 0.5f), 0, 0, Screen.width, Screen.height );
			view.addChild( frameBackground );
			
			view.addChild( displayObjectContainer );
			
			frame = MovieClipFactory.CreateEditChildScreen();
			frame.x = MainUI.STAGE_WIDTH * 0.5f;
			frame.y = MainUI.STAGE_HEIGHT * 0.5f;
			displayObjectContainer.addChild( frame );
						
			blankChildIcon = frame.getChildByName ("icon_container") as MovieClip;

			webCam = new WebCamCapture( Mathf.RoundToInt(blankChildIcon.width) - 2, Mathf.RoundToInt(blankChildIcon.height) - 2 );
			webCam.x = blankChildIcon.x + 1;
			webCam.y = blankChildIcon.y + 1;
			frame.addChild( webCam );

			DisplayObject guide = frame.getChildByName ("guide");
			childPictureGuide = MovieClipFactory.CreateChildPictureGuideIcon();
			childPictureGuide.x = guide.x;
			childPictureGuide.y = guide.y;
			childPictureGuide.scaleX = childPictureGuide.scaleY = 1.25f;
			childPictureGuide.visible = false;
			frame.addChild( childPictureGuide );

			float platformScale = 1f;
			switch (PlatformUtils.GetPlatform ()) {
			case Platforms.IOSRETINA:
				platformScale = 1.5f;
				break;

			case Platforms.WIN:
				platformScale = 2f;
				break;

			case Platforms.ANDROID: 
				platformScale = 1.6f;
				break;

			case Platforms.IOS:
				platformScale = 1.5f;
				break;
			}
			frame.scaleX = frame.scaleY = platformScale;

			this.classrooms = classrooms;
			
			addButtons();
			addInputs();
		}
		
		private void addButtons()
		{
			DisplayObject refButton = frame.getChildByName ("capture_image_btn");
			frame.removeChild (refButton);
			captureImage_btn = MovieClipFactory.CreateCaptureImageButton();
			captureImage_btn.addEventListener( MouseEvent.CLICK, onCaptureClick );
			captureImage_btn.x = refButton.x;
			captureImage_btn.y = refButton.y;
			frame.addChild( captureImage_btn );

			refButton = frame.getChildByName ("clear_image_btn");
			frame.removeChild (refButton);
			clear_btn = MovieClipFactory.CreateClearImageButton();
			clear_btn.addEventListener( MouseEvent.CLICK, onClearClick );
			clear_btn.x = refButton.x;
			clear_btn.y = refButton.y;
			frame.addChild( clear_btn );

			refButton = frame.getChildByName ("save_child_btn");
			frame.removeChild (refButton);
			saveChild_btn = MovieClipFactory.CreateSaveChildButton();
			saveChild_btn.addEventListener( MouseEvent.CLICK, onSaveClick );
			saveChild_btn.x = refButton.x;
			saveChild_btn.y = refButton.y;
			frame.addChild( saveChild_btn );

			refButton = frame.getChildByName ("cancel_btn");
			frame.removeChild (refButton);
			cancel_btn = MovieClipFactory.CreateCancelChildButton();
			cancel_btn.addEventListener( MouseEvent.CLICK, onCancelClick );
			cancel_btn.x = refButton.x;
			cancel_btn.y = refButton.y;
			frame.addChild( cancel_btn );
		}
		
		private void addInputs()
		{
			firstName_txt = frame.getChildByName<TextField>( "firstName_txt" );
			lastName_txt = frame.getChildByName<TextField>( "lastName_txt" );
			birthDate_txt = frame.getChildByName<TextField>( "datePicker_txt" );
			language_txt = frame.getChildByName<TextField>( "language_txt" );
			firstName_txt.text = lastName_txt.text = birthDate_txt.text = language_txt.text = "";

			DisplayObject dropdownRef = frame.getChildByName ("student_dropdown");
			frame.removeChild (dropdownRef);
			selectStudent_DropDown = MovieClipFactory.CreateEditChildDropDown<User>();
			selectStudent_DropDown.x = dropdownRef.x;
			selectStudent_DropDown.y = dropdownRef.y;
			selectStudent_DropDown.DefaultText = "Select Student";
			selectStudent_DropDown.addEventListener( CEvent.CHANGED, onStudentChanged );
			frame.addChild( selectStudent_DropDown );

			dropdownRef = frame.getChildByName ("class_dropdown");
			frame.removeChild (dropdownRef);
			selectClass_DropDown = MovieClipFactory.CreateEditChildDropDown<Classroom>();
			selectClass_DropDown.DefaultText = "Select Classroom";
			selectClass_DropDown.SetInfo( 5, classrooms );
			selectClass_DropDown.addEventListener( CEvent.CHANGED, onClassroomChanged );
			frame.addChild( selectClass_DropDown );
			selectClass_DropDown.x = dropdownRef.x;
			selectClass_DropDown.y = dropdownRef.y;
			
		}
		
		private void onClassroomChanged( CEvent e )
		{
			currentClassroom = selectClass_DropDown.SelectedItem;
			selectStudent_DropDown.SetInfo( 5, currentClassroom.Children );
			selectStudent_DropDown.SelectedItem = null;
		}
		
		private void onStudentChanged( CEvent e )
		{
			if(currentClassroom == null) return;
			currentChild = selectStudent_DropDown.SelectedItem as Child;
			
			if(currentChild != null) {
				firstName_txt.text = currentChild.FirstName;
				lastName_txt.text = currentChild.LastName.Substring(0, 1);
				language_txt.text = Enum.GetName(typeof(LanguageProfile), currentChild.Profile);
				var birthDate = System.DateTime.Parse(currentChild.BirthDate, CultureInfo.InvariantCulture);
				birthDate_txt.text = birthDate.ToString("MM/yyyy", CultureInfo.InvariantCulture);
				
				webCam.Stop();
				webCam.SetPreviewTexture(currentChild.Texture);
			} else {
				firstName_txt.text = "";
				lastName_txt.text = "";
				birthDate_txt.text = "";
				webCam.Stop();
			}
			
			childPictureGuide.visible = false;
		}
		
		public void Start()
		{
			
		}
		
		public void Dispose()
		{
			webCam.Stop();
			webCam.Destroy();
			
		}
		
		public void Resize( int width, int height )
		{
			view.width = width;
			view.height = height;
			
			displayObjectContainer.FillParent();
			
		}
		
		//Button Handlers
		private void onCaptureClick( CEvent e )
		{
			if(!webCam.isCamPlaying)
			{
				onClearClick(e);
			}
			else
			{
				webCam.TakeSnapShot();
				childPictureGuide.visible = false;
			}
		}
		
		private void onClearClick( CEvent e )
		{
			webCam.Start();
			childPictureGuide.visible = true;
		}
		
		private void onSaveClick( CEvent e )
		{
			if(currentChild == null) {
				ShowErrorDialog("No Child Selected", "Cannot Save image without child to save image to");
				return;
			}
			if(!webCam.HasTexture) {
				ShowErrorDialog("New Picture Required", "Take a picture by hitting the Capture Image button");
				return;
			}
			
			if(PhotoChanged != null) {
				saveChild_btn.removeEventListener( MouseEvent.CLICK, onSaveClick );
				PhotoChanged(currentChild, webCam.MoveTexture());
			}
		}
		
		private void onCancelClick( CEvent e )
		{
			if(CloseClicked != null) {
				CloseClicked();
			}
		}
		
		void ShowErrorDialog (string title, string message)
		{
			var mc = new MovieClip(Main.SWF, "ConfirmationDialog");
			var dialog = new SimpleDialog(mc, title, message);
			view.addChild(dialog);
		}
	}
	
}