using System;
using pumpkin.display;

namespace SSGCore
{
	public interface GameScreen
	{
		DisplayObject View { get; }
		void Resize(int width, int height);
		void Start();
		void Dispose();
	}
}

