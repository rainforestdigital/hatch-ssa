using System;
using pumpkin.display;

namespace SSGCore
{
	public class ConfirmationDialog : GameScreen
	{
		private SimpleDialog dialog;
		
		public Action OkayClicked
		{
			set { dialog.OkayClicked = value; }
		}
		
		public Action CancelClicked
		{
			set { dialog.CancelClicked = value; }
		}
		
		public ConfirmationDialog (string title, string message)
		{
			var mc = new MovieClip(Main.SWF, "PromptDialog");
			dialog = new SimpleDialog(mc, title, message);
		}

		#region GameScreen implementation
		public void Resize (int width, int height)
		{
		}

		public void Start ()
		{
		}

		public void Dispose ()
		{
		}

		public pumpkin.display.DisplayObject View 
		{
			get { return dialog; }
		}
		#endregion
	}
}

