using System;
using pumpkin.display;
using pumpkin.events;
using HatchFramework;
using pumpkin.text;
using UnityEngine;

using SSGCore.RMS.Webservice;

namespace SSGCore
{
	public class RegistrationSuccessScreen : GameScreen
	{		
		private DisplayObjectContainer view = new DisplayObjectContainer();
		private DisplayObjectContainer container;
		
		public Action Reject;
		public Action Accept;
		
		public RegistrationSuccessScreen (RegistrationResult result)
		{
			container = new DisplayObjectContainer();
			container.width = Main.STAGE_WIDTH;
			container.height = Main.STAGE_HEIGHT;
			view.addChild(container);
			
			var clip = new MovieClip(Main.SWF, "RegisterConfirmationView");
			container.addChild(clip);
			
			if( PlatformUtils.GetPlatform() != Platforms.IOS && PlatformUtils.GetPlatform() != Platforms.IOSRETINA )
			{
				var exitBtn = new BasicButton(clip.getChildByName<MovieClip>("exitBtn"));
				exitBtn.Root.addEventListener(MouseEvent.CLICK, OnRejectClick);
			}
			else
				clip.getChildByName<MovieClip>("exitBtn").alpha = 0;
			
			var continueBtn = new BasicButton(clip.getChildByName<MovieClip>("continueBtn"));
			continueBtn.Root.addEventListener(MouseEvent.CLICK, OnAcceptClick);
			
			clip.getChildByName<TextField>("organizationTxt").text = result.organization;
			clip.getChildByName<TextField>("schoolTxt").text = result.school;
			clip.getChildByName<TextField>("classroomTxt").text = result.classroom;
			clip.getChildByName<TextField>("deviceCodeTxt").text = result.deviceCode;
			clip.getChildByName<TextField>("license_txt").text = result.serialNumber;
			
			clip.AlignCentered();
		}
		
		void OnRejectClick(CEvent e)
		{
			DebugConsole.Log("EXIT CLICKED");
			if(Reject != null) {
				Reject();
			}
		}
		
		void OnAcceptClick(CEvent e)
		{
			Accept();			
		}
		
		public DisplayObject View{ get { return view; } }

		public void Resize (int width, int height)
		{
			view.width = width;
			view.height = height;
			container.FillParent();
		}

		public void Start ()
		{
		}

		public void Dispose ()
		{
		}
	}
}

