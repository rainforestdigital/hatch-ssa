using System;
using pumpkin.display;
using pumpkin.events;
using HatchFramework;
using pumpkin.text;
using UnityEngine;

namespace SSGCore
{
	public class TeacherMenu : GameScreen
	{
		public class Params
		{
			public bool inGame;
			public bool canClose;
			public bool hasInternet;
			public bool inTeacherMode;
			public bool inDemoMode;
			public bool startupPrompt;
			public bool firstNoChildren;
		}
		
		private DisplayObjectContainer view = new DisplayObjectContainer();
		private MovieClip container;
		
		public Action CloseClicked;
		public Action AddChildClicked;
		public Action EditChildImageClicked;
		public Action EditChildClicked;
		public Action ClassOverviewClicked;
		public Action UnlockSkillClicked;
		public Action WizardTrigger;
		public Action DualLanguageClicked;
		public Action WhoAreYouTrigger;
		public Action TeacherModeClicked;
		public Action DemoModeClicked;
		public Action ExitGameClicked;
		
		public TeacherMenu (Params param)
		{
			Init(param);
		}
		private void Init ( Params param )
		{

			bool hasInternet = HasInternet();

			if(container != null)
			{
				view.removeChild(container);
				container = null;
			}
			
			var builder = new MenuBuilder();
			
			if(!param.firstNoChildren)
			{
				if(param.startupPrompt) {
					builder.Subtitle = "No children have been assigned to this class or they do not have photos that can be displayed so they can login. Please \"Add Child\"" +
						" or \"Add or Change Child Photo\".\nTo add new classes to this device, select \"Add a Class to Device\".\nTo play the games as pre-configured children without saving data, select \"Demo Mode\"";
					//builder.AddButton("Setup Wizard", OnWizardTrigger);
				}

				DebugConsole.Log("Showing teacher menu with internet: {0}", hasInternet);

				builder.AddButton("Add Child", OnAddChildClicked);
				builder.AddButton("Add or Change Child Photo", OnEditChildImageClicked);

				if(hasInternet && param.startupPrompt){
					builder.AddButton("Add a Class to Device", OnWizardTrigger);
					builder.AddButton("Change a Class Name", OnWizardTrigger);
				}


				if(!param.startupPrompt && (hasInternet || param.inDemoMode)) {
					builder.AddButton("Edit Child", OnEditChildClicked);
					builder.AddButton("Class Overview", OnClassOverviewClicked);
					builder.AddButton("Unlock Child Skill", OnUnlockSkillClicked);
					
					builder.AddButton("Add a Class to Device", OnWizardTrigger);
					builder.AddButton("Change a Class Name", OnWizardTrigger);
				}
				
				if(param.inGame) {
					string teacherModeCaption = (param.inTeacherMode ? "Exit Teacher Mode" : "Teacher Mode");
					builder.AddButton(teacherModeCaption, OnTeacherModeClicked);
				}

				builder.AddButton("Dual Language Center", OnDualLanguageClicked);

			}
			else
			{
				builder.Subtitle = "No children have been assigned to a class on this device or there are no child photos that can be displayed for children to press to login.\n\n" +
					"To add children or their photos, press the \"Add Child or Photo\" button.\n\n" +
					"To Play in Demo Mode as pre-configured children without saving data, press the \"Demo Mode\" button.";
				
				builder.AddButton("Add Child or Photo", OnWhoAreYouTrigger);
			}
			
			if(GlobalConfig.GetProperty<bool>("AllowDemoMode", true)) {
				string demoModePrompt = param.inDemoMode ? "Exit Demo Mode" : "Demo Mode";
				builder.AddButton(demoModePrompt, OnDemoModeClicked);
			}
			
			if(param.canClose) {
				builder.AddCloseButton(OnCloseClicked);
			}
			if( PlatformUtils.GetPlatform() != Platforms.IOS && PlatformUtils.GetPlatform() != Platforms.IOSRETINA )
				builder.AddRedButton("Exit Game", OnExitGameClicked);
			
			container = builder.Build();
			view.addChild(container);
		}

		private bool HasInternet()
		{
			return Application.internetReachability != NetworkReachability.NotReachable
				&& InternetConnectivity.hasConnection;
		}
		
		private void OnCloseClicked(CEvent e)
		{
			if(CloseClicked != null) {
				CloseClicked();
			}
		}
		
		private void OnAddChildClicked(CEvent e)
		{
			if(AddChildClicked != null) {
				AddChildClicked();
			}
		}
		
		private void OnEditChildClicked(CEvent e)
		{
			if(EditChildClicked != null) {
				EditChildClicked();
			}
		}
		
		private void OnUnlockSkillClicked(CEvent e)
		{
			if(UnlockSkillClicked != null) {
				UnlockSkillClicked();
			}
		}
		
		private void OnClassOverviewClicked(CEvent e)
		{
			if(ClassOverviewClicked != null) {
				ClassOverviewClicked();
			}
		}
		
		private void OnWizardTrigger(CEvent e)
		{
			if(WizardTrigger != null){
				WizardTrigger();
			}
		}

		private void OnDualLanguageClicked(CEvent e)
		{
			if(DualLanguageClicked != null){
				DualLanguageClicked();
			}
		}
		
		private void OnWhoAreYouTrigger(CEvent e)
		{
			if(WhoAreYouTrigger != null)
			{
				WhoAreYouTrigger();
				
				SSGEngine.Instance.StartCoroutine(WaitAndRefreshMenu());
			}
		}
		
		private System.Collections.IEnumerator WaitAndRefreshMenu()
		{
			yield return new UnityEngine.WaitForSeconds(1.5f);
			var param = new TeacherMenu.Params() {
				startupPrompt = true,
				canClose = false
			};
			float tempX = container.x;
			float tempY = container.y;
			float tempWide = container.scaleX;
			float tempHigh = container.scaleY;
			float tempHeight = container.height;
			Init(param);
			container.scaleX = tempWide;
			container.scaleY = tempHigh;
			container.x = tempX;
			container.y = tempY - (((container.height - tempHeight) * tempHigh)/2);
		}
		
		private void OnTeacherModeClicked(CEvent e)
		{
			if(TeacherModeClicked != null) {
				TeacherModeClicked();
			}
		}
		
		private void OnEditChildImageClicked(CEvent e)
		{
			if(EditChildImageClicked != null) {
				EditChildImageClicked();
			}
		}
		
		private void OnDemoModeClicked(CEvent e)
		{
			if(DemoModeClicked != null) {
				DemoModeClicked();
			}
		}
		
		private void OnExitGameClicked(CEvent e)
		{
			if(ExitGameClicked != null) {
				ExitGameClicked();
			}
		}
		
		
		#region GameScreen implementation
		public DisplayObject View{ get { return view; } }

		public void Resize (int width, int height)
		{
			view.width = width;
			view.height = height;
			container.ScaleRelativeToHeight();
			container.AlignCentered();
		}

		public void Start ()
		{
		}

		public void Dispose ()
		{
			SSGEngine.Instance.ResetInactivity ();
		}
		#endregion
	}
}

