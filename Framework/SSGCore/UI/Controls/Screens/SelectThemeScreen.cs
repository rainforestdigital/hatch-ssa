using System;
using System.Collections.Generic;
using pumpkin.display;
using pumpkin.events;
using HatchFramework;

namespace SSGCore
{
	public class SelectThemeScreen : GameScreen
	{
		private DisplayObjectContainer view = new DisplayObjectContainer();
		private DisplayObjectContainer themeView = new DisplayObjectContainer();
		private List<ThemeButton> buttons = new List<ThemeButton>();
		private BasicButton back_btn;
		private HatchUserButton childIndicator;
		
		public SelectThemeScreen (Child currentChild, IEnumerable<Theme> themes)
		{
			themeView.width = MainUI.STAGE_WIDTH;
			themeView.height = MainUI.STAGE_HEIGHT;
			view.addChild(themeView);
			
			childIndicator = new HatchUserButton();
			childIndicator.User = currentChild;
			childIndicator.mouseEnabled = false;
			view.addChild(childIndicator);
			
			foreach(Theme theme in themes) {
				var button = new ThemeButton(theme);
				button.addEventListener(MouseEvent.CLICK, OnThemeClick);
				buttons.Add(button);
			}
			
			back_btn = new BasicButton(Main.SWF, "Back_button");
			back_btn.addEventListener(MouseEvent.CLICK, OnBackClick);
			view.addChild(back_btn);
		}
		
		public void Start()
		{
			themeView.LayoutTiled(buttons, 1);
			AnimationUtils.GrowIn(buttons);
			
			var bundle = new SoundEngine.SoundBundle();
			bundle.AddClip(SoundUtils.LoadGlobalSound("themes_start"));
			foreach(var button in buttons) {
				var path = button.Theme.SoundPath;
				bundle.AddClip(SoundUtils.LoadGlobalSound(path), path, 0);
			}
			bundle.AddClip(SoundUtils.LoadGlobalSound("themes_end"), "themes_end", 0);
			SoundEngine.Instance.PlayBundle(bundle);
			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWord;
		}

		void OnAudioWord(string word)
		{
			foreach(var button in buttons)
			{
				if(word == button.Theme.SoundPath) {
					themeView.setChildIndex(button, buttons.Count-1);
					button.ScaleCentered(1.4f);
				} else {
					button.ScaleCentered(1f);
				}
			}
		}
		
		Action<Theme> ThemeSelectedInvoker;
		public event Action<Theme> ThemeSelected{ add{ThemeSelectedInvoker += value;} remove{ThemeSelectedInvoker -= value;}}
		Action BackPressedInvoker;
		public event Action BackPressed{ add{BackPressedInvoker += value;} remove{BackPressedInvoker -= value;}}

		public DisplayObject View 
		{
			get { return view; }
		}
				
		private void OnThemeClick(CEvent e)
		{
			var button = (ThemeButton)e.currentTarget;
			var theme = button.Theme;
			if(ThemeSelectedInvoker != null)
			{
				ThemeSelectedInvoker(theme);
			}
		}
		
		private void OnBackClick(CEvent e)
		{
			if(BackPressedInvoker != null)
			{
				BackPressedInvoker();
			}
		}

		public void Resize (int width, int height)
		{
			view.width = width;
			view.height = height;
			themeView.FitToParent();
			back_btn.ScaleAsIfFullScreen(2);
			back_btn.AlignBottomLeft(5);
			childIndicator.ScaleAsIfFullScreen(1);
		}
		
		public void Dispose()
		{
			SoundEngine.Instance.StopAll();
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWord;
		}
	}
}

