using System;
using pumpkin.display;
using pumpkin.events;
using HatchFramework;
using pumpkin.text;
using UnityEngine;

using SSGCore.RMS.Webservice;

namespace SSGCore
{
	public class ManualRegistrationSuccessScreen : GameScreen
	{
		
		private DisplayObjectContainer view = new DisplayObjectContainer();
		private DisplayObjectContainer container;
		
		public Action Exit;
		
		public ManualRegistrationSuccessScreen (RegistrationResult result)
		{
			container = new DisplayObjectContainer();
			container.width = Main.STAGE_WIDTH;
			container.height = Main.STAGE_HEIGHT;
			view.addChild(container);
			
			var clip = new MovieClip(Main.SWF, "ManualRegisterConfirmationView");
			container.addChild(clip);
			
			if( PlatformUtils.GetPlatform() != Platforms.IOS && PlatformUtils.GetPlatform() != Platforms.IOSRETINA )
			{
				var exitBtn = new BasicButton(clip.getChildByName<MovieClip>("exitBtn"));
				exitBtn.Root.addEventListener(MouseEvent.CLICK, OnExitClick);
			}
			else
				clip.getChildByName<MovieClip>("exitBtn").alpha = 0;
			
			clip.getChildByName<TextField>("organizationTxt").text = result.organization;
			clip.getChildByName<TextField>("schoolTxt").text = result.school;
			clip.getChildByName<TextField>("classroomTxt").text = result.classroom;
			clip.getChildByName<TextField>("deviceCodeTxt").text = result.deviceCode;
			clip.getChildByName<TextField>("license_txt").text = result.serialNumber;
			clip.getChildByName<TextField>("orderNumber_txt").text = result.orderNumber;
			
			clip.AlignCentered();
		}
		
		void OnExitClick(CEvent e)
		{
			DebugConsole.Log("EXIT CLICKED");
			if(Exit != null) {
				Exit();
			}
		}
		
		public DisplayObject View{ get { return view; } }

		public void Resize (int width, int height)
		{
			view.width = width;
			view.height = height;
			container.FillParent();
		}

		public void Start ()
		{
		}

		public void Dispose ()
		{
		}
	}
}
