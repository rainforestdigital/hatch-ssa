using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.text;
using System;

namespace SSGCore
{
	
	// TODO: This currently has a great deal of overlap with the edit child screen
	// Possibly look into a way of reducing duplication
	public class AddChildScreen : GameScreen
	{
		const int MAX_CHILDREN = 32;
		public delegate void AddChildHandler(Child child, Classroom classroom);
	
		private DisplayObjectContainer view;
		public DisplayObject View{ get{ return view; } }
		
		private DisplayObjectContainer displayObjectContainer;
		
		//	Drop Downs
		private DropDown<Classroom> selectClass_DropDown;
		
		//	Text Inputs
		private TextInput firstName_Input;
		private TextInput lastName_Input;
		private TextInput childID_Input;
		private TextField birthDate_Text;
		
		//	Buttons
		private BasicButton captureImage_btn;
		private BasicButton clear_btn;
		private BasicButton saveChild_btn;
		private BasicButton cancel_btn;
		private BasicButton datePicker_btn;
		
		//	Display objects
		private MovieClip frame;
		private MovieClip blankChildIcon;
		private MovieClip childPictureGuide;
		private float frameY;
		
		//	Date Picker
		private DatePicker datePicker;
		private DateTime birthDate;
		
		//	WebCam
		private WebCamCapture webCam;
		
		//	Data
		private IList<Classroom> classrooms;
		
		public System.Action CloseClicked;
		public AddChildHandler ChildAdded;

		private MovieClip langSelectEn;
		private MovieClip langSelectSp;
		private MovieClip langSelectDualMathSp;
		private MovieClip langSelectDualMathEn;
		private int currentLanguageProfile;

		public AddChildScreen( IList<Classroom> classrooms )
		{
			view = new DisplayObjectContainer();
			displayObjectContainer = new DisplayObjectContainer();
			displayObjectContainer.width = MainUI.STAGE_WIDTH;
			displayObjectContainer.height = MainUI.STAGE_HEIGHT;
			
			var frameBackground = new MovieClip();
			frameBackground.graphics.drawSolidRectangle(new Color(0, 0, 0, 0.5f), 0, 0, Screen.width, Screen.height );
			view.addChild( frameBackground );
			
			view.addChild( displayObjectContainer );
			
			frame = new MovieClip(Main.SWF, "AddChild_Screen");
			frame.x = MainUI.STAGE_WIDTH * 0.5f;
			frame.y = MainUI.STAGE_HEIGHT * 0.5f;
			frameY = frame.y;
			displayObjectContainer.addChild( frame );

			blankChildIcon = frame.getChildByName ("icon_container") as MovieClip;

			webCam = new WebCamCapture( Mathf.RoundToInt(blankChildIcon.width) - 2, Mathf.RoundToInt(blankChildIcon.height) - 2 );
			webCam.x = blankChildIcon.x + 1;
			webCam.y = blankChildIcon.y + 1;
			frame.addChild( webCam );

			DisplayObject guide = frame.getChildByName ("guide");
			childPictureGuide = MovieClipFactory.CreateChildPictureGuideIcon();
			childPictureGuide.x = guide.x;
			childPictureGuide.y = guide.y;
			childPictureGuide.scaleX = childPictureGuide.scaleY = 1.25f;
			childPictureGuide.visible = false;
			frame.addChild( childPictureGuide );
			
			datePicker = MovieClipFactory.CreateDatePicker();
			datePicker.x = MainUI.STAGE_WIDTH * 0.5f;
			datePicker.y = MainUI.STAGE_HEIGHT * 0.5f;
			datePicker.visible = false;
			displayObjectContainer.addChild( datePicker );
			
			datePicker.onDoneClick = OnDateSelected;
			
			this.classrooms = classrooms;
			
			addButtons();
			addInputs();

			float platformScale = 1f;
			switch (PlatformUtils.GetPlatform ()) {
			case Platforms.IOSRETINA:
				platformScale = 1.5f;
				break;

			case Platforms.WIN:
				platformScale = 2f;
				break;

			case Platforms.ANDROID: 
				platformScale = 1.6f;
				break;

			case Platforms.IOS:
				platformScale = 1.5f;
				break;
			}

			frame.scaleX = frame.scaleY = platformScale;
			datePicker.scaleX = datePicker.scaleY = frame.scaleX;

			view.addEventListener( CEvent.ENTER_FRAME, frameProper );
		}
		
		private void addButtons()
		{
			DisplayObject refButton = frame.getChildByName ("capture_image_btn");
			frame.removeChild (refButton);
			captureImage_btn = MovieClipFactory.CreateCaptureImageButton();
			captureImage_btn.addEventListener( MouseEvent.CLICK, onCaptureClick );
			captureImage_btn.x = refButton.x;
			captureImage_btn.y = refButton.y;
			frame.addChild( captureImage_btn );

			refButton = frame.getChildByName ("clear_image_btn");
			frame.removeChild (refButton);
			clear_btn = MovieClipFactory.CreateClearImageButton();
			clear_btn.addEventListener( MouseEvent.CLICK, onClearClick );
			clear_btn.x = refButton.x;
			clear_btn.y = refButton.y;
			frame.addChild( clear_btn );

			refButton = frame.getChildByName ("save_child_btn");
			frame.removeChild (refButton);
			saveChild_btn = MovieClipFactory.CreateSaveChildButton();
			saveChild_btn.addEventListener( MouseEvent.CLICK, onSaveClick );
			saveChild_btn.x = refButton.x;
			saveChild_btn.y = refButton.y;
			frame.addChild( saveChild_btn );

			refButton = frame.getChildByName ("cancel_btn");
			frame.removeChild (refButton);
			cancel_btn = MovieClipFactory.CreateCancelChildButton();
			cancel_btn.addEventListener( MouseEvent.CLICK, onCancelClick );
			cancel_btn.x = refButton.x;
			cancel_btn.y = refButton.y;
			frame.addChild( cancel_btn );

			refButton = frame.getChildByName ("date_picker_btn");
			frame.removeChild (refButton);
			datePicker_btn = MovieClipFactory.CreateDatePickerButton();
			datePicker_btn.x = refButton.x;
			datePicker_btn.y = refButton.y;
			datePicker_btn.addEventListener( MouseEvent.CLICK, onDateButtonClick );
			frame.addChild( datePicker_btn );

			langSelectEn = frame.getChildByName ("lang_select_eng") as MovieClip;
			langSelectEn.stopAll ();
			langSelectSp = frame.getChildByName("lang_select_spn") as MovieClip;
			langSelectSp.stopAll ();
			langSelectDualMathSp = frame.getChildByName("lang_select_dual_math_span") as MovieClip;
			langSelectDualMathSp.stopAll ();
			langSelectDualMathEn = frame.getChildByName("lang_select_math_eng") as MovieClip;
			langSelectDualMathEn.stopAll ();

			langSelectEn.addEventListener (MouseEvent.CLICK, onLanguageSelect);
			langSelectSp.addEventListener (MouseEvent.CLICK, onLanguageSelect);
			langSelectDualMathSp.addEventListener (MouseEvent.CLICK, onLanguageSelect);
			langSelectDualMathEn.addEventListener (MouseEvent.CLICK, onLanguageSelect);

			setCurrentLanguage (1);

			var childIdInfo = frame.getChildByName<MovieClip>("childIDInfo_mc");
			if(childIdInfo != null) {
				childIdInfo.addEventListener(MouseEvent.MOUSE_DOWN, OnChildIdInfoClick);
			}
		}

		private void onLanguageSelect(CEvent e) {

			switch (((DisplayObject)e.currentTarget).name) {
			case "lang_select_eng":
				setCurrentLanguage (1);
				break;

			case "lang_select_spn":
				setCurrentLanguage (2);
				break;

			case "lang_select_dual_math_span":
				setCurrentLanguage (3);
				break;

			case "lang_select_math_eng":
				setCurrentLanguage (4);
				break;
			}
				
		}

		private void setCurrentLanguage(int profile) {
			langSelectEn.gotoAndStop("inactive");
			langSelectSp.gotoAndStop("inactive");
			langSelectDualMathSp.gotoAndStop("inactive");
			langSelectDualMathEn.gotoAndStop("inactive");

			switch (profile) {
			case 1:
				langSelectEn.gotoAndStop("active");
				break;

			case 2:
				langSelectSp.gotoAndStop("active");
				break;

			case 3:
				langSelectDualMathSp.gotoAndStop("active");
				break;

			case 4:
				langSelectDualMathEn.gotoAndStop("active");
				break;

			}

			currentLanguageProfile = profile;
		}

		private void OnChildIdInfoClick(CEvent e)
		{
			ShowErrorDialog("Child ID", "Use this field to enter your organization- specific ID for the child to better reference your existing records.");
		}
		
		private void addInputs()
		{
			firstName_Input = new TextInput( frame.getChildByName<TextField>( "firstName_txt" ) );
			firstName_Input.Text = "";
			//firstName_Input.OnHasFocus = OnTextFocus;
			lastName_Input = new TextInput( frame.getChildByName<TextField>( "lastName_txt" ) );
			lastName_Input.Text = "";
			//lastName_Input.OnHasFocus = OnTextFocus;
			childID_Input = new TextInput( frame.getChildByName<TextField>( "childID_txt" ) );
			childID_Input.Text = "";
			//childID_Input.OnHasFocus = OnTextFocus;
			birthDate_Text = frame.getChildByName<TextField>( "datePicker_txt" );
			birthDate_Text.text = "";
			
			DisplayObject dropdownRef = frame.getChildByName ("class_dropdown");
			dropdownRef.visible = false;
			selectClass_DropDown = MovieClipFactory.CreateEditChildDropDown<Classroom>();
			selectClass_DropDown.SetInfo( 5, classrooms );
			selectClass_DropDown.SelectedItem = classrooms[0];
			frame.addChild( selectClass_DropDown );
			selectClass_DropDown.x = dropdownRef.x;
			selectClass_DropDown.y = dropdownRef.y;
		}
		
		
		
		private void OnTextFocus(bool focused)
		{
#if UNITY_IPHONE || UNITY_ANDROID
			frame.y = focused ? frameY - MainUI.STAGE_HEIGHT / 9 : frameY;
#endif
		}
		
		private void frameProper( CEvent e )
		{
#if UNITY_IPHONE || UNITY_ANDROID
			frame.y = TouchScreenKeyboard.visible ? frameY - MainUI.STAGE_HEIGHT / 9: frameY;
#endif
		}
		
		private void OnDateSelected()
		{
			datePicker.visible = false;
			birthDate_Text.text = datePicker.SelectedDate.ToString("MM/dd/yyyy");
			birthDate = datePicker.SelectedDate;
		}
		
		public void Start()
		{
			webCam.Start();
			childPictureGuide.visible = true;
		}
		
		public void Dispose()
		{
			webCam.Stop();
			webCam.Destroy();
		}
		
		public void Resize( int width, int height )
		{
			view.width = width;
			view.height = height;
			
			displayObjectContainer.FillParent();
		}
		
		//Button Handlers
		private void onCaptureClick( CEvent e )
		{
			if(!webCam.isCamPlaying)
			{
				webCam.Start();
				childPictureGuide.visible = true;
			}
			else
			{
				webCam.TakeSnapShot();
				childPictureGuide.visible = false;
			}
		}
		
		private void onClearClick( CEvent e )
		{
			webCam.Start();
			childPictureGuide.visible = true;
		}

		void ShowMissingFieldDialog (string message)
		{
			ShowErrorDialog("Missing Field", message);
		}
		
		void ShowErrorDialog (string title, string message)
		{
			var mc = new MovieClip(Main.SWF, "ConfirmationDialog");
			var dialog = new SimpleDialog(mc, title, message);
			view.addChild(dialog);
		}
		
		void ShowPromptDialog (string title, string message, Action onOkay)
		{
			var mc = new MovieClip(Main.SWF, "PromptDialog");
			var dialog = new SimpleDialog(mc, title, message);
			dialog.OkayClicked = onOkay;
			view.addChild(dialog);
		}
		
		IEnumerable<Child> ChildrenWithSameName()
		{
			foreach (var classroom in classrooms) {
				foreach( Child child in classroom.Children) {
					if(child.FirstName == firstName_Input.Text && child.LastName == lastName_Input.Text) {
						yield return child;
					}
				}
			}
		}
		
		bool HasChildWithSameName()
		{
			foreach (var child in ChildrenWithSameName()) {
				return true;
			}
			return false;
		}
		
		bool HasChildWithSameNameAndBirthday()
		{
			foreach (var child in ChildrenWithSameName()) {
				if(birthDate_Text.text == child.BirthDate) {
					return true;
				}
			}
			return false;
		}
		
		struct Confirmation {
			public string title;
			public string description;
			
			public Confirmation(string title, string description)
			{
				this.title = title;
				this.description = description;
			}
		}
		
		private IEnumerator<Confirmation> CheckData()
		{
			var minBirthDate = DateTime.UtcNow.AddYears(-99);
			var maxBirthDate = DateTime.UtcNow.AddYears(-2);
			var underage = DateTime.UtcNow.AddYears(-3).AddMonths(-5);
			
			if(selectClass_DropDown.SelectedItem.Children.Count >= MAX_CHILDREN)
			{
				ShowErrorDialog("Classroom is Full", string.Format("The selected classroom already has {0} children, and cannot fit any more.", MAX_CHILDREN));
			}
			else if(firstName_Input.Text.Length < 2)
			{
				ShowMissingFieldDialog ("First Name Required");
			}
			else if(lastName_Input.Text.Length < 2)
			{
				ShowMissingFieldDialog ("Last Name Required");
			}
			else if(birthDate_Text.text.Length == 0)
			{
				ShowMissingFieldDialog ("Birth Date Required");
			}
			else if(webCam.isCamPlaying)
			{
				ShowErrorDialog("Picture Required", "Take a picture by hitting the Capture Image button");
			}
			else if(birthDate < minBirthDate || birthDate > maxBirthDate)
			{
				ShowErrorDialog("Invalid Birth Date", "The child must be between 2 and 99 years old.");
			}
			else if(HasChildWithSameNameAndBirthday())
			{
				ShowErrorDialog("Child Already Exists", "A child with the same name and birthdate already exists");
			}
			else
			{
				if(birthDate > underage) {
					var dateAtAge = birthDate.AddYears(3).AddMonths(5);
					var message = String.Format("Progress monitoring will not be displayed for this child until {0}. At that time, the child's skill development levels and time on task will be displayed in reports.",
						dateAtAge.ToString("MMMM dd, yyyy"));
					yield return new Confirmation("Underage Child", message);
				}
				
				if(HasChildWithSameName()) {
					yield return new Confirmation("Child with Same Name", 
						"There is already a child with the same name, but a different birthdate added. Do you want to continue?");
				}

				var birthDateString = birthDate.ToString("yyyy-MM-dd");
				var child = new Child(-1, firstName_Input.Text, lastName_Input.Text, new SimpleImageSource(webCam.MoveTexture()),
					birthDateString, childID_Input.Text, System.Guid.NewGuid().ToString(), null, currentLanguageProfile);
				if(ChildAdded != null) {
					ChildAdded(child, selectClass_DropDown.SelectedItem);
				}
			}
		}
		
		void RunConfirmations(IEnumerator<Confirmation> e) {
			Action runner = null;
			runner = () => {
				if(e.MoveNext()) {
					var confirmation = e.Current;
					ShowPromptDialog(confirmation.title, confirmation.description, runner);
				}
			};
			
			runner();
		}
		
		private void onSaveClick( CEvent e )
		{
			RunConfirmations(CheckData());
		}
		
		private void onCancelClick( CEvent e )
		{
			if(CloseClicked != null) {
				CloseClicked();
			}
		}
		
		private void onDateButtonClick( CEvent e )
		{
			datePicker.visible = !datePicker.visible;
		}
		
	}
	
}