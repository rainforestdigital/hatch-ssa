using System;
using pumpkin.display;
using pumpkin.events;
using pumpkin.text;
using HatchFramework;
using UnityEngine;

namespace SSGCore
{
	internal enum StartupMenuType
	{
		Offline,
		OfflineSynced,
		Setup,
		SetupClass,
		SetupTeachers
	}
	
	internal class StartupMenu : GameScreen
	{
		private DisplayObjectContainer view = new DisplayObjectContainer();
		private MovieClip container;
		
		public StartupMenu (StartupMenuType type)
		{
			var builder = new MenuBuilder();
			builder.Title = TitleForType(type);
			builder.Subtitle = SubtitleForType(type);
			
			if(type == StartupMenuType.SetupClass) {
				builder.AddButton("Setup Wizard", OnSetupClassClicked);
			}
			
			if(CanUseDemoMode(type)) {
				builder.AddButton("Continue in Demo Mode", OnDemoClicked);
			}
			
			if(type == StartupMenuType.OfflineSynced) {
				builder.AddButton("Continue", OnContinueClicked);
			}
			
			if(CanContactAdmin(type)) {
				builder.AddButton("Contact Administrator", OnContactAdminClicked);
			}
			
			if(type == StartupMenuType.Setup) {
				builder.AddButton("I Am the Administrator", "Go to Hatch RMS to set up the device", OnSetupClicked);
			}
			else if(type == StartupMenuType.SetupTeachers) {
				builder.AddButton("I Am the Administrator", "Go to Hatch RMS to assign teachers to a class", OnSetupTeachersClicked);
			}
			
			if( PlatformUtils.GetPlatform() != Platforms.IOS && PlatformUtils.GetPlatform() != Platforms.IOSRETINA )
				builder.AddRedButton("Exit Adventures", OnExitGameClicked);
			
			container = builder.Build();
			view.addChild(container);
		}
		
		private bool CanContactAdmin(StartupMenuType type)
		{
			switch (type) {
			case StartupMenuType.SetupTeachers:
			//case StartupMenuType.SetupClass:
				return true;
			default:
				return false;
			}
		}
		
		private bool CanUseDemoMode(StartupMenuType type)
		{
			return type != StartupMenuType.OfflineSynced && GlobalConfig.GetProperty<bool>("AllowDemoMode", true);
		}
		
		private string TitleForType(StartupMenuType type)
		{
			switch (type) {
			case StartupMenuType.OfflineSynced:
				return "No Internet";
			default:
				return "We're Sorry!";
			}
		}
		
		private string SubtitleForType(StartupMenuType type)
		{
			switch (type) {
			case StartupMenuType.Offline:
				return "This device cannot connect to the internet. Please check with your system administrator to gain Internet access. Would you like to:";
			case StartupMenuType.OfflineSynced:
				return "This device is not connected to the internet. Please check with your system administrator to gain internet access. " +
					"Connection to the internet is required to store information in RMS and to be shared to other devices in the class.";
			case StartupMenuType.Setup:
				return "This device is not yet set up within the Hatch RMS.";
			case StartupMenuType.SetupTeachers:
				return "This device has been associated with one or more classes, but there are no teachers assigned to the classes yet.";
			case StartupMenuType.SetupClass:
				return "This device has not been assigned to a class.\n\n" +
					"If you would like to set up this device now, please select the \"Setup Wizard\" option.\n\n" +
					"If you would like to play the games as pre-configured children without saving data, select \"Continue in Demo Mode.\"";
			default:
				return "";
			}
		}
		
		public Action DemoClicked;
		public Action ContinueClicked;
//		public Action SetupClicked;
//		public Action SetupSchoolClicked;
		public Action SetupTeachersClicked;
		public Action SetupClassClicked;
		public Action ContactAdminClicked;
		public Action ExitGameClicked;
		
		private void OnDemoClicked(CEvent e)
		{
			if(DemoClicked != null) {
				DemoClicked();
			}
		}
		
		private void OnSetupClicked(CEvent e)
		{
//			if(SetupClicked != null) {
//				SetupClicked();
//			}
		}
		
		private void OnSetupClassClicked(CEvent e)
		{
			if(SetupClassClicked != null) {
				SetupClassClicked();
			}
		}
		
		private void OnSetupTeachersClicked(CEvent e)
		{
			if(SetupTeachersClicked != null) {
				SetupTeachersClicked();
			}
		}
		
		private void OnContinueClicked(CEvent e)
		{
			if(ContinueClicked != null) {
				ContinueClicked();
			}
		}
		
		private void OnSetupSchoolClicked(CEvent e)
		{
			if(SetupTeachersClicked != null) {
				SetupTeachersClicked();
			}
		}
		
		private void OnContactAdminClicked(CEvent e)
		{
			if(ContactAdminClicked != null) {
				ContactAdminClicked();
			}
		}
		
		private void OnExitGameClicked(CEvent e)
		{
			if(ExitGameClicked != null) {
				ExitGameClicked();
			}
		}
		
		
		#region GameScreen implementation
		public DisplayObject View{ get { return view; } }

		public void Resize (int width, int height)
		{
			view.width = width;
			view.height = height;
			container.ScaleRelativeToHeight();
			container.AlignCentered();
		}

		public void Start ()
		{
		}

		public void Dispose ()
		{
		}
		#endregion
	}
}

