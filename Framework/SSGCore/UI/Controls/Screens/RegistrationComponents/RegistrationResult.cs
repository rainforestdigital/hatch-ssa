using System;
using System.Collections.Generic;

namespace SSGCore
{
	public class RegistrationResult
	{
		public string organization = "";
		public string school = "";
		public string classroom = "";
		public string deviceCode = "";
		public string serialNumber = "";
		public string orderNumber = "";
		
		public RegistrationResult ()
		{
		}
		
		public RegistrationResult(Dictionary<string, string> info) {
			info.TryGetValue("org_name", out organization);
			info.TryGetValue("school_name", out school);
			info.TryGetValue("class_name", out classroom); 
			info.TryGetValue("device_code", out deviceCode);
			info.TryGetValue("serial_number", out serialNumber);
		}
	}
}

