using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using pumpkin.display;
using pumpkin;
using pumpkin.events;
using pumpkin.text;
using HatchFramework;

namespace SSGCore
{
	public class RegistrationError
	{
		public MovieClip view;
		public MovieClip target;
		
		public bool IsShowing
		{
			get{ return view.visible; }
		}
		
		public RegistrationError (MovieClip _target)
		{
			view = new MovieClip();
			view.width = Screen.width;
			view.height = Screen.height;
			
			view.addChild(DisplayObjectUtils.CreateBlocker() );
			
			target = _target;
			if(target.parent != null) {
				target.parent.removeChild(target);
			}
			view.addChild(target);
			target.ScaleRelativeToHeight();
			target.AlignCentered();
			
			DebugConsole.Log("WHAT'S NULL - target: " + (target == null) + ", continueBtn: " + (target.getChildByName<MovieClip>("continuebtn") == null) );
			var continueBtn = new BasicButton(target.getChildByName<MovieClip>("continuebtn"));
			continueBtn.Root.addEventListener(MouseEvent.CLICK, HandleContinueBtn);
			
			Hide();
		}
		
		private void HandleContinueBtn(CEvent e)
		{
			Hide();
		}
		
		public void Show()
		{
			view.visible = true;
		}
		
		public void Hide()
		{
			view.visible = false;
		}
	}
}