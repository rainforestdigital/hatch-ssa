using System;
using pumpkin.display;
using pumpkin.events;
using HatchFramework;
using pumpkin.text;
using UnityEngine;

namespace SSGCore
{
	public class EulaScreen : GameScreen
	{
		
		private DisplayObjectContainer view = new DisplayObjectContainer();
		private DisplayObjectContainer container;
		
		private TextField text;
		private ScrollView scrollView;
		private DisplayObjectContainer content;
		private TextInput name_txt;
		
		private const float rowHeight = 24.5f;
		
		public Action Reject;
		public Action<string> Accept;
		
		public EulaScreen ()
		{
			container = new DisplayObjectContainer();
			container.width = Main.STAGE_WIDTH;
			container.height = Main.STAGE_HEIGHT;
			view.addChild(container);
			
			var clip = new MovieClip(Main.SWF, "EulaView");
			container.addChild(clip);
			clip.AlignCentered();
			
			var barPlaceholder = clip.getChildByName("bar_placeholder");
			var bar = new ScrollBar(Main.SWF, "Eula_Bar");
			bar.x = barPlaceholder.x;
			bar.y = barPlaceholder.y;
			clip.addChild(bar);
			
			var eulaContent = Resources.Load("Config/EULA") as TextAsset;
			
			text = clip.getChildByName<TextField>("eula_txt");
			var x = text.x;
			var y = text.y;
			text.text = eulaContent.text;
			
			content = new DisplayObjectContainer();
			content.width = text.width;
			content.height = text.height;
			text.x = 0;
			text.width -= 10;
			clip.removeChild(text);
			var textBG = new MovieClip();
			content.addChild(textBG);
			
			content.addChild(text);
			
			scrollView = new ScrollView(content);
			scrollView.UseMomentum = true;
			scrollView.x = x;
			scrollView.y = y;
			scrollView.width = text.width;
			scrollView.height = text.height;
			clip.addChild(scrollView);
			
			scrollView.addEventListener(CEvent.CHANGED, OnScroll);
			
			text.height = 1024*1024;
			var newScrollHeight = text.numLines * rowHeight;
			text.height = content.height + rowHeight;
			Debug.Log(text.maxScrollV);
			content.height = newScrollHeight;
			textBG.graphics.drawSolidRectangle(Color.white, 0, 0, content.width, content.height);
			
			bar.Init(scrollView);
			OnScroll(null);
			
			name_txt = new TextInput(clip.getChildByName<TextField>("name_txt"));
			name_txt.Text = "";
			
			var accept_btn = new BasicButton(clip.getChildByName<MovieClip>("accept_btn"));
			accept_btn.Root.addEventListener(MouseEvent.CLICK, OnAcceptClick);
			
			if( PlatformUtils.GetPlatform() != Platforms.IOS && PlatformUtils.GetPlatform() != Platforms.IOSRETINA )
			{
				var reject_btn = new BasicButton(clip.getChildByName<MovieClip>("reject_btn"));
				reject_btn.Root.addEventListener(MouseEvent.CLICK, OnRejectClick);
				
				var cancel = clip.getChildByName<MovieClip>("cancel_btn");
				if(cancel != null) {
					var cancel_btn = new BasicButton(cancel);
					cancel_btn.Root.addEventListener(MouseEvent.CLICK, OnRejectClick);
				}
				
				var close = clip.getChildByName<MovieClip>("close_btn");
				if(close != null) {
					var close_btn = new BasicButton(close);
					close_btn.Root.addEventListener(MouseEvent.CLICK, OnRejectClick);
				}
			}
			else
			{
				clip.getChildByName<MovieClip>("reject_btn").alpha = 0;
				clip.getChildByName<MovieClip>("cancel_btn").alpha = 0;
				clip.getChildByName<MovieClip>("close_btn").alpha = 0;
			}
		}
		
		void ShowErrorDialog (string title, string message)
		{
			var mc = new MovieClip(Main.SWF, "ConfirmationDialog");
			var dialog = new SimpleDialog(mc, title, message);
			view.addChild(dialog);
		}
		
		void OnScroll(CEvent e)
		{
			var percent = scrollView.ScrollValue;
			var range = content.height - scrollView.height;
			int row = (int)(range*percent/rowHeight);
			text.y = row * rowHeight;
			text.scrollV = row;
		}
		
		void OnRejectClick(CEvent e)
		{
			if(Reject != null) {
				Reject();
			}
		}
		
		void OnAcceptClick(CEvent e)
		{
			if(name_txt.Text.Length >= 5) {
				if(Accept != null) Accept(name_txt.Text);
			} else {
				ShowErrorDialog("Invalid User Name", "User name must be at least 5 characters long.");
			}
		}
		
		#region GameScreen implementation
		public DisplayObject View{ get { return view; } }

		public void Resize (int width, int height)
		{
			view.width = width;
			view.height = height;
			container.FillParent();
		}

		public void Start ()
		{
		}

		public void Dispose ()
		{
		}
		#endregion
	}
}

