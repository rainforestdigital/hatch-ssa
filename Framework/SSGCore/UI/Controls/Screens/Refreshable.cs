using System;

namespace SSGCore
{
	public interface Refreshable
	{
		void Refresh();
	}
}

