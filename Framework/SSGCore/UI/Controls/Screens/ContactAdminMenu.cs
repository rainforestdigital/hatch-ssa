using System;
using pumpkin.display;
using pumpkin.events;
using pumpkin.text;
using HatchFramework;
using UnityEngine;
using System.Text;
using System.Collections.Generic;

namespace SSGCore
{
	
	internal class ContactAdminMenu : GameScreen
	{
		private DisplayObjectContainer view = new DisplayObjectContainer();
		private MovieClip container;
		
		public ContactAdminMenu (IEnumerable<Admin> admins)
		{
			var builder = new MenuBuilder();
			builder.Title = "Admin Contact Info";
			
			var message = new StringBuilder();
			foreach(var admin in admins) {
				message.AppendLine(admin.Name);
				if(!String.IsNullOrEmpty(admin.Organization))
					message.AppendLine(admin.Organization);
				if(!String.IsNullOrEmpty(admin.PhoneNumber))
					message.AppendLine(admin.PhoneNumber);
				if(!String.IsNullOrEmpty(admin.Email))
					message.AppendLine(admin.Email);
				message.AppendLine();
			}
			builder.Subtitle = message.ToString();
			
			if( PlatformUtils.GetPlatform() != Platforms.IOS && PlatformUtils.GetPlatform() != Platforms.IOSRETINA )
				builder.AddRedButton("Exit iStartSmart", OnExitGameClicked);
			
			container = builder.Build();
			view.addChild(container);
		}
		
		public Action ExitGameClicked;
		
		private void OnExitGameClicked(CEvent e)
		{
			if(ExitGameClicked != null) {
				ExitGameClicked();
			}
		}
		
		
		#region GameScreen implementation
		public DisplayObject View{ get { return view; } }

		public void Resize (int width, int height)
		{
			view.width = width;
			view.height = height;
			container.ScaleRelativeToHeight();
			container.AlignCentered();
		}

		public void Start ()
		{
		}

		public void Dispose ()
		{
		}
		#endregion
	}
}

