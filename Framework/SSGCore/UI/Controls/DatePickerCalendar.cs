using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;

namespace SSGCore
{

	public class DatePickerCalendar : DisplayObjectContainer
	{
	
		private List<LabeledButton> buttons;
		
		private int currentYear;
		private int currentMonth;
		private int currentDay = 1;
		public int CurrentDay
		{
			get{ return currentDay; }
			set
			{
				for(int i = 0; i < buttons.Count; i++)
				{
					if(buttons[i].visible && buttons[i].Label == value.ToString())
					{
						buttons[i].gotoAndStop("active");
					}
					else 
						buttons[i].gotoAndStop("inactive");
				}
				
				currentDay = value;
				
			}
		}
		
		public DateTime SelectedDate{ 
			get{
				return new DateTime(currentYear, currentMonth, currentDay); 
			}
		}
		
		public DatePickerCalendar() : base()
		{
			
			buttons = new List<LabeledButton>();
			
			int row = 0;
			int column = 0;
			
			for(int i = 0; i < 42; i++)
			{
				if(column == 7)
				{
					row++;
					column = 0;
				}
				
				LabeledButton newDateBtn = MovieClipFactory.CreateLabeledButton( "DatePicker_Button" );
				
				newDateBtn.x = (column * newDateBtn.width);
				newDateBtn.y = (row * newDateBtn.height);
				newDateBtn.Label = (i + 1).ToString();
				addChild( newDateBtn );
				newDateBtn.visible = false;
				buttons.Add( newDateBtn );
				newDateBtn.addEventListener( MouseEvent.CLICK, onDateClick );
				
				column++;

			}
			
		}
		
		public void UpdateCalendar( int _currentYear, int _currentMonth, int _currentDay )
		{
			
			currentYear = _currentYear;
			currentMonth = _currentMonth;
			
			DateTime beginningOfMonth = new DateTime(_currentYear, _currentMonth, 1);
			int beginningIndex = (int)beginningOfMonth.DayOfWeek;
			int days = beginningIndex + DateTime.DaysInMonth(currentYear, currentMonth);
			int dayNumber = 1;
		
			for(int i = 0; i < buttons.Count; i++)
			{
				if(i >= beginningIndex && i < days)
				{
					buttons[i].visible = true;
					buttons[i].Label = dayNumber.ToString();
					dayNumber++;
				}
				else 
					buttons[i].visible = false;
			}
			
			CurrentDay = Mathf.Clamp(_currentDay, 1, DateTime.DaysInMonth(currentYear, currentMonth));
			
		}
		
		private void onDateClick( CEvent e )
		{
			LabeledButton button = e.currentTarget as LabeledButton;
			CurrentDay = int.Parse( button.Label );
			dispatchEvent( new CEvent(CEvent.CHANGED) );
		}
		
		public string[] DaysInMonth
		{
			get
			{
				List<string> numberStrings = new List<string>();
				for(int i = 1; i <= DateTime.DaysInMonth(currentYear, currentMonth); i++)
				{
					numberStrings.Add( i.ToString() );
				}
				
				return numberStrings.ToArray();
				
			}
		}
		
	}
	
}