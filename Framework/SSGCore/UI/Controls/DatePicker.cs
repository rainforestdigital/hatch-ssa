using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;

namespace SSGCore
{
	
	public enum Months
	{
		January,
		February,
		March,
		April,
		May,
		June,
		July,
		August,
		September,
		October,
		November,
		December
	}
	
	public class DatePicker : MovieClip
	{
			
		private DateTime currentDate;
		
		private MovieClip done_btn;
		
		private DatePickerCalendar calendar;
		
		private int currentMonth;
		public int CurrentMonth{ get{ return currentMonth; } }
		
		private int currentDay = 1;
		public int CurrentDay{ get{ return currentDay; } }
		
		private int currentYear;
		public int CurrentYear{ get{ return currentYear; } }
		
		private DateTime selectedDate;
		public DateTime SelectedDate{ get{ return selectedDate; } }
			
		private List<string> yearRange;
		
		private DateScroller monthScroller;
		private DateScroller dayScroller;
		private DateScroller yearScroller;
		
		public Action onDoneClick;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="HatchFramework.DatePicker"/> class.
		/// </summary>
		/// <param name='swf'>
		/// SWF used for the base DatePicker MovieClip.
		/// </param>
		/// <param name='symbolname'>
		/// Symbolname of the DatePicker MovieClip.
		/// </param>
		/// <param name='btnsymbolname'>
		/// The symbol name for the date button movie clips.
		/// </param>
		public DatePicker( string swf, string symbolname, string btnsymbolname) : base( swf, symbolname )
		{
			init();
		}
			
		//	Init the DatePicker. Creates all the date buttons, adds the drop down menus for month and year.
		private void init()
		{
			DisplayObject buttonRef = this.getChildByName("datePicker_btn");
			buttonRef.visible = false;

			done_btn = getChildByName<MovieClip>( "done_btn" );
			done_btn.addEventListener( MouseEvent.CLICK, onDoneButtonClick );
			
			var now = DateTime.UtcNow;
			var startDate = now.AddYears(-2);
			var endDate = now.AddYears(-35);
			
			currentDate = new DateTime(now.Year-4, 1, 1);
			
			currentMonth = currentDate.Month;
			currentYear = currentDate.Year;
			
			calendar = new DatePickerCalendar();
			calendar.addEventListener( CEvent.CHANGED, onDateClick );
			calendar.x = 58;//getChildByName<MovieClip>("buttonContainer_mc").x;
			calendar.y = -155;//getChildByName<MovieClip>("buttonContainer_mc").y;
			addChild( calendar );

			DisplayObject datePickerRef = getChildByName ("datePicker_dropdown_left");
			removeChild (datePickerRef);
			string[] months = new string[12]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug","Sep", "Oct", "Nov", "Dec"};		
			monthScroller = new DateScroller();
			monthScroller.x = datePickerRef.x;
			monthScroller.y = datePickerRef.y;
			addChild(monthScroller);
			monthScroller.Init(months);
			monthScroller.addEventListener( CEvent.CHANGED, onMonthChange );
			addChild (monthScroller);

			yearRange = new List<string>();
			for(int y = startDate.Year; y > endDate.Year; y--)
			{
				yearRange.Add( y.ToString() );
			}

			datePickerRef = getChildByName ("datePicker_dropdown_right");
			removeChild (datePickerRef);
			yearScroller = new DateScroller();
			yearScroller.x = datePickerRef.x;
			yearScroller.y = datePickerRef.y;
			addChild(yearScroller);
			yearScroller.Init(yearRange);
			yearScroller.addEventListener( CEvent.CHANGED, onYearChange );
			yearScroller.SetValueImmediate(yearRange.IndexOf(currentYear.ToString()));

			datePickerRef = getChildByName ("datePicker_dropdown_mid");
			removeChild (datePickerRef);
			dayScroller = new DateScroller();
			dayScroller.x = datePickerRef.x;
			dayScroller.y = datePickerRef.y;
			addChild(dayScroller);
			dayScroller.addEventListener( CEvent.CHANGED, onDayChange );
				
			onMonthChange( null );
			
		}
		
		//	Click handler for date buttons. Updates selectedDate.
		private void onDateClick( CEvent e )
		{
			selectedDate = calendar.SelectedDate;
			currentDay = calendar.CurrentDay;
			dayScroller.SetValue(currentDay - 1);

		}
		
		//	Change handler for monthSelect. Updates calendar.
		private void onMonthChange( CEvent e )
		{
			currentMonth = monthScroller.CurrentValueIndex;
			calendar.UpdateCalendar(currentYear, currentMonth + 1, currentDay);
			dayScroller.Init( calendar.DaysInMonth );
			selectedDate = calendar.SelectedDate;
		}
		
		//	Change handler for daySelect. Updates calendar.
		private void onDayChange( CEvent e )
		{
			currentDay = dayScroller.CurrentValueIndex + 1;
			calendar.CurrentDay = currentDay;
			selectedDate = calendar.SelectedDate;
		}
		
		//	Change handler for yearSelect. Updates calendar.
		private void onYearChange( CEvent e )
		{
			currentYear = int.Parse( yearRange[yearScroller.CurrentValueIndex] );
			calendar.UpdateCalendar(currentYear, currentMonth + 1, currentDay);
			dayScroller.Init( calendar.DaysInMonth );
			selectedDate = calendar.SelectedDate;
		}
			
		private void onDoneButtonClick( CEvent e )
		{
			if(onDoneClick != null)
			{
				onDoneClick();
			}
		}
		
	}

}