using System;
using pumpkin.display;
using System.Collections.Generic;
using pumpkin.tweener;

namespace SSGCore
{
	public static class AnimationUtils
	{
		public static void GrowIn(this Sprite obj)
		{
			var bounds = obj.getBounds(obj.parent);
			var origScale = obj.scaleX;
			obj.scaleX *= 0.5f;
			obj.scaleY *= 0.5f;
				
			var origX = obj.x;
			var origY = obj.y;
			
			var newBounds = obj.getBounds(obj.parent);
			obj.x += (bounds.width-newBounds.width) * 0.5f;
			obj.y += (bounds.height-newBounds.height) * 0.5f;
			
			obj.mouseEnabled = false;
			var tween = Tweener.addTween(obj, Tweener.Hash(
				"scaleX", origScale,
				"scaleY", origScale,
				"x", origX,
				"y", origY,
				"time", 0.5,
				"transition","easeOutQuad"));
			
			tween.OnComplete(() => obj.mouseEnabled = true);
		}
		
		public static void GrowIn<T>(IEnumerable<T>  objects)
			where T : Sprite
		{
			foreach(var obj in objects)
			{
				obj.GrowIn();
			}
		}
	}
}

