using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Callback = System.Action;

namespace SSGCore
{
	public class TimerUtils : MonoBehaviour
	{	
		public static TimerObject SetTimeout (float delay, Callback cb )
		{
			GameObject t = new GameObject("TimerObject: " + delay.ToString() + " seconds");
			TimerObject timer = t.AddComponent<TimerObject>();
			timer.SetTimeout(delay, cb);
			return timer;
		}
		
		public static TimerObject SetGUITimer(Callback cb)
		{
			GameObject t = new GameObject("TimerObject GUI Timer");
			TimerObject timer = t.AddComponent<TimerObject>();
			timer.SetGUITimer(cb);
			return timer;
		}
	}
}
