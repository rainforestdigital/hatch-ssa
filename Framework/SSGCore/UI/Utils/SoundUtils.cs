using System;
using UnityEngine;
using HatchFramework;

namespace SSGCore
{
	public static class SoundUtils
	{
		public static void PlaySimpleSound(string path)
		{
			var clip = LoadGlobalSound(path);
			if(clip != null)
			{
				var bundle = new SoundEngine.SoundBundle();
				bundle.AddClip(clip);
				SoundEngine.Instance.PlayBundle(bundle);
			}
		}
		
		public static void PlaySimpleEffect(string path)
		{
			var clip = LoadGlobalSound(path);
			if(clip != null)
			{
				SoundEngine.Instance.PlayEffect(clip);
			}
		}
		
		public static AudioClip LoadGlobalSound(string path)
		{
			// hack to load without asset bundles for now
			AudioClip clip = null;
			
			if(AssetLoader.Instance != null) {
				clip = AssetLoader.Instance.GetAudioClip(MovieClipFactory.GLOBAL, path);
			}
			
			if(clip == null) {
				clip = (AudioClip)Resources.Load("SSGAssets/UNIVERSAL/"+path, typeof(AudioClip));
			}
			
			return clip;
		}
		
		public static AudioClip LoadGlobalSound(string bundle, string path)
		{
			// hack to load without asset bundles for now
			AudioClip clip = null;
			
			if(AssetLoader.Instance != null) {
				clip = AssetLoader.Instance.GetAudioClip(bundle, path);
			}
			
			if(clip == null) {
				clip = (AudioClip)Resources.Load("SSGAssets/UNIVERSAL/"+path, typeof(AudioClip));
			}
			
			if(clip == null){
				DebugConsole.LogError("Unable to find audio asset with path: {0} in bundle: {1}", path, bundle);	
			}
			
			return clip;
		}
	}
}

