using System;
using pumpkin.display;
using System.Collections.Generic;
using UnityEngine;

namespace SSGCore
{
	public static class LayoutUtils
	{
		/// <summary>
		/// Converts local coordinates from one DisplayObject (MovieClip) to another's local coordinate system
		/// </summary>
		/// <returns>
		/// Vector2 representing the toContainer's coordinate system
		/// </returns>
		/// <param name='fromContainer'>
		/// From container.
		/// </param>
		/// <param name='toContainer'>
		/// To container.
		/// </param>
		/// <param name='point'>
		/// Vector2 point in the fromContainer's coordinate system.  If you just want the reg point of the fromContainer, don't pass the 3rd argument
		/// </param>
		
		public static Vector2 LocalToLocal(DisplayObject fromContainer, DisplayObject toContainer )
		{

			Vector2 p2 = fromContainer.getFullMatrix().transformPoint(new Vector2(0, 0));
			
			p2 = toContainer.getFullMatrix().invert().transformPoint(new Vector2(p2.x, p2.y));
					
			return p2;
		}
		
		public static void ResetScale<T>(IEnumerable<T> objects)
			where T : DisplayObject
		{
			foreach(var clip in objects) {
				clip.scaleX = clip.scaleY = 1;
			}
		}
		
		public static void LayoutTiled<T>(this DisplayObjectContainer container, IList<T> tiles) 
			where T : DisplayObject
		{
			container.LayoutTiled(tiles, 3);
		}
		
		public static void LayoutTiled<T>(this DisplayObjectContainer container, IList<T> tiles, float maxScale) 
			where T : DisplayObject
		{
			if(tiles.Count == 0) {
				return;
			}
			
			// assume tiles are uniform in size
			T firstTile = tiles[0];
			int tileHeight = (int)firstTile.height;
			int tileWidth = (int)firstTile.width;
			
			int containerHeight = (int)container.height;
			int containerWidth = (int)container.width;
			int padding = 20;
			
			int maxTilesPerRow = Math.Max(1, containerWidth / (tileWidth+padding));
			//int maxTilesPerColumn = containerHeight / (tileHeight+padding);
			
			bool fitsOnOneRow = tiles.Count <= maxTilesPerRow;
			int columns = Math.Min(tiles.Count, maxTilesPerRow);
			int rows = fitsOnOneRow ? 1 : ((tiles.Count + columns - 1) / columns);
			
			float scale = 1;
			if(tiles.Count < maxTilesPerRow) {
				float rowWidth = tileWidth*columns + padding*(columns-1);
				scale = (float)((containerWidth*0.75) / rowWidth);
				scale = Math.Min((containerHeight*0.75f)/(tileHeight), scale);
				scale = Math.Min(maxScale, scale);
				scale = Math.Max(1, scale);
			}else{
			
				scale = Math.Min((containerHeight*0.85f)/(tileHeight*rows), scale);
			}
			
			var startX = (containerWidth - tileWidth*scale*columns - padding*(columns-1))/2;
			var startY = (containerHeight - tileHeight*scale*rows - padding*(rows-1))/2;
			
			var offsetX = tileWidth*scale + padding;
			var offsetY = tileHeight*scale + padding;
			
			int r = 0;
			int c = 0;
			foreach(T tile in tiles) {
				tile.x = startX + c*offsetX;
				tile.y = startY + r*offsetY;
				float cS = tile.scaleX;
				cS *= scale;
				tile.scaleX = tile.scaleY = cS;
				container.addChild(tile);
				
				if(++c == columns) {
					r++;
					c = 0;
				}
			}
			
		}
		
		public static void LayoutTiled<T>(this DisplayObjectContainer container, IList<T> tiles, float maxScale, float minScale) 
			where T : DisplayObject
		{
			if(tiles.Count == 0) {
				return;
			}
			
			// assume tiles are uniform in size
			T firstTile = tiles[0];
			int tileHeight = (int)firstTile.height;
			int tileWidth = (int)firstTile.width;
			
			int containerHeight = (int)container.height;
			int containerWidth = (int)container.width;
			int padding = 20;
			
			int maxTilesPerRow = (int)Math.Max(1, containerWidth / ((tileWidth * minScale)+padding));
			//int maxTilesPerColumn = containerHeight / (tileHeight+padding);
			
			bool fitsOnOneRow = tiles.Count <= maxTilesPerRow;
			int columns = Math.Min(tiles.Count, maxTilesPerRow);
			int rows = fitsOnOneRow ? 1 : ((tiles.Count + columns - 1) / columns);
			
			
			float scale = 1;
			if(maxScale == minScale)
				scale = maxScale;
			else{
				if(tiles.Count < maxTilesPerRow) {
					float rowWidth = tileWidth*columns + padding*(columns-1);
					scale = (float)((containerWidth*0.75) / rowWidth);
					scale = Math.Min((containerHeight*0.75f)/(tileHeight), scale);
					scale = Math.Min(maxScale, scale);
					scale = Math.Max(minScale, scale);
				}
				else{
				
					scale = Math.Min((containerHeight*0.85f)/(tileHeight*rows), scale);
				}
			}
			
			var startX = (containerWidth - tileWidth*scale*columns - padding*(columns-1))/2;
			var startY = (containerHeight - tileHeight*scale*rows - padding*(rows-1))/2;
			
			var offsetX = tileWidth*scale + padding;
			var offsetY = tileHeight*scale + padding;
			
			int r = 0;
			int c = 0;
			foreach(T tile in tiles) {
				tile.x = startX + c*offsetX;
				tile.y = startY + r*offsetY;
				float cS = tile.scaleX;
				cS *= scale;
				tile.scaleX = tile.scaleY = cS;
				container.addChild(tile);
				
				if(++c == columns) {
					r++;
					c = 0;
				}
			}
			
		}
		
		public static void ShiftGameTiles<T>(this DisplayObjectContainer container, IList<T> tiles, Vector2 offset)
			where T : DisplayObject
		{
		
			foreach(T tile in tiles){
				tile.x += offset.x;
				tile.y += offset.y;
			}
		}
		
		//Layout col count method
		public static void LayoutGameTiles<T>(this DisplayObjectContainer container, IList<T> tiles, float maxScale, int maxCols) 
			where T : DisplayObject
		{
			if(tiles.Count == 0) {
				return;
			}
			
			
			int tileHeight = 0;
			int tileWidth = 0;
			
			foreach(T x in tiles){
			
				pumpkin.geom.Rectangle rec = x.getBounds(container);
				tileHeight = (int)Mathf.Max(rec.height, tileHeight);
				tileWidth = (int)Mathf.Max(rec.width, tileWidth);
			}
			
			
			
			
		//	int tileHeightMax = (int)Mathf.Min(tileHeight, tileHeight*maxScale);
			int tileWidthMax = (int)Mathf.Min (tileWidth, tileHeight*maxScale);
			DebugConsole.Log("tileWidth : {0}, containerWidth: {1}", tileWidthMax, container.width);
			
			
			int containerHeight = (int)container.height;
			int containerWidth = (int)container.width;
			int padding = 20;
			
			int maxTilesPerRow = Math.Max(1, containerWidth / (tileWidthMax+padding));
			maxTilesPerRow = Math.Min(maxTilesPerRow, maxCols);
			//int maxTilesPerColumn = containerHeight / (tileHeight+padding);
			
			bool fitsOnOneRow = tiles.Count <= maxTilesPerRow;
			int columns = Math.Min(tiles.Count, maxTilesPerRow);
			int rows = fitsOnOneRow ? 1 : ((tiles.Count + columns - 1) / columns);
			
			
			
			float scale = 1;
			if(tiles.Count < maxTilesPerRow) {
				float rowWidth = tileWidth*columns + padding*(columns-1);
				scale = (float)((containerWidth*0.75) / rowWidth);
				scale = Math.Min((containerHeight*0.75f)/tileHeight, scale);
				scale = Math.Min(maxScale, scale);
				
			}else{
				scale = Math.Min(scale, maxScale);	
			}
			
			var startX = (containerWidth - tileWidth*scale*columns - padding*(columns-1))/2;
			var startY = (containerHeight - tileHeight*scale*rows - padding*(rows-1))/2;
			
			var offsetX = tileWidth*scale + padding;
			var offsetY = tileHeight*scale + padding;
			
			DebugConsole.Log("Colums: {0}, {1}", columns, offsetX);
			
			int r = 0;
			int c = 0;
			foreach(T tile in tiles) {
				tile.x = startX + c*offsetX;
				tile.y = startY + r*offsetY;
				tile.scaleX = tile.scaleY = scale;
				container.addChild(tile);
				
				if(++c == columns) {
					r++;
					c = 0;
				}
			}
			
		}
		
		
	
		
		public static float ScaledParent(this DisplayObject obj){
			var parent = obj.parent;
			if(parent == null) {
				return 1;
			}
						
			var scaleX = parent.width / obj.width;
			var scaleY = parent.height / obj.height;
			var scale = Math.Min(scaleX, scaleY);
			return scale;
		}
		
		public static void FitToParent(this DisplayObject obj)
		{
			
			
			
			var parent = obj.parent;
			if(parent == null) {
				return;
			}
						
			var scale = ScaledParent(obj);
			
			obj.scaleX = scale;
			obj.scaleY = scale;
			obj.x = (parent.width - obj.width*scale)/2;
			obj.y = (parent.height - obj.height*scale)/2;
		}
		
		public static void ScaleAsIfFullScreen(this DisplayObject obj, float scaleCoefficient)
		{
			var parent = obj.parent;
			if(parent == null) {
				return;
			}
			
			var scaleX = parent.width / MainUI.STAGE_WIDTH;
			var scaleY = parent.height / MainUI.STAGE_HEIGHT;
			var scale = scaleCoefficient * Math.Min(scaleX, scaleY);
			
			obj.scaleX = scale;
			obj.scaleY = scale;
		}
		
		public static void ScaleRelativeToHeight(this DisplayObject obj)
		{
			var parent = obj.parent;
			if(parent == null) {
				return;
			}
			
			var scale = parent.height / MainUI.STAGE_HEIGHT;
			obj.scaleX = scale;
			obj.scaleY = scale;
		}
		
		public static void ScaleCentered(this DisplayObject obj, float scale)
		{
			var parent = obj.parent;
			if(parent == null) {
				return;
			}
			
			var rect = obj.getBounds(parent);
			
			obj.x += (rect.width - rect.width*(scale/obj.scaleX))/2;
			obj.y += (rect.height - rect.height*(scale/obj.scaleY))/2;
			
			obj.scaleX = scale;
			obj.scaleY = scale;
		}
		
		public static void CenterInScreen(this DisplayObject obj){
		
			DisplayObjectContainer parent = obj.parent;
			if(parent == null) {
				return;
			}
			
			pumpkin.geom.Rectangle rect = obj.getBounds(obj.stage);
			
			Vector2 pos = Vector2.zero;
			
			pos.x = (Screen.width - rect.width)/2;
			pos.y = (Screen.height - rect.height)/2;
			pos = parent.getFullMatrix().invert().transformPoint(new Vector2(pos.x, pos.y));
			obj.x = pos.x;
			obj.y = pos.y;
		}
		
		public static void AlignCentered(this DisplayObject obj)
		{
			var parent = obj.parent;
			if(parent == null) {
				return;
			}
			
			var rect = obj.getBounds(parent);
			
			obj.x = (parent.width - rect.width)/2;
			obj.y = (parent.height - rect.height)/2;
			
			
		}
		
		public static void AlignCenteredToScreen(this DisplayObject obj)
		{		
			//obj.x = (Screen.width - obj.width)*.5f;
			//obj.y = (Screen.height - obj.height)*.5f;
			
			obj.x = (Screen.width*.5f) - (obj.width*.5f);
			obj.y = (Screen.height*.5f) - (obj.height*.5f);
		}
		
		public static void FillParent(this DisplayObject obj)
		{
			var parent = obj.parent;
			if(parent == null) {
				return;
			}
			
			//var rect = obj.getBounds(parent);
			
			var scaleX = parent.width / obj.width;
			var scaleY = parent.height / obj.height;
			var scale = Math.Max(scaleX, scaleY);
			
			
			obj.x = (parent.width - obj.width*scale)/2;
			obj.y = (parent.height - obj.height*scale)/2;
			obj.scaleX *= scale;
			obj.scaleY *= scale;
		}
		
		public static void AlignBottomLeft(this DisplayObject obj)
		{
			AlignBottomLeft(obj, 0);
		}
		
		public static void AlignBottomLeft(this DisplayObject obj, int padding )
		{
			var parent = obj.parent;
			if(parent == null) {
				return;
			}
			
			var rect = obj.getBounds(parent);
			obj.x = padding;
			obj.y = parent.height - (rect.height+padding);
		}
		
		public static void AlignBottomRight(this DisplayObject obj )
		{
			var parent = obj.parent;
			if(parent == null) {
				return;
			}
			
			var padding = 0;
			var rect = obj.getBounds(parent);
			obj.x = parent.width - (rect.width+padding);
			obj.y = parent.height - (rect.height+padding);
		}
		
		public static void AlignTopRight(this DisplayObject obj )
		{
			var parent = obj.parent;
			if(parent == null) {
				return;
			}
			
			var padding = 0;
			var rect = obj.getBounds(parent);
			obj.x = parent.width - (rect.width+padding);
			obj.y = padding;
		}
		
		public static void StretchToWidth(this DisplayObject obj)
		{
			var parent = obj.parent;
			if(parent == null) {
				return;
			}
			
			var rect = obj.getBounds(parent);
			obj.scaleX *= parent.width / rect.width;
		}
		
		public static void RemoveAll<T>(this DisplayObjectContainer parent, IEnumerable<T> objects)
			where T : DisplayObject
		{
			foreach( var obj in objects) {
				parent.removeChild(obj);
			}
		}
		
		// doubles the scale if the in iOS retina
		// Use this only when necessary. Avoid when possible
		public static void DoubleIfRetina(this DisplayObject obj)
		{
			if(HatchFramework.PlatformUtils.GetPlatform() == HatchFramework.Platforms.IOSRETINA)
			{
				obj.scaleX = obj.scaleY = 2;
			}
		}
	}
}

