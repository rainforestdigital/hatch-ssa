using System;
using UnityEngine;
using pumpkin.display;

namespace SSGCore
{
	public class DisplayObjectUtils
	{	
		public static MovieClip CreateBlocker()
		{
			MovieClip blocker = new MovieClip();
			blocker.graphics.drawSolidRectangle(new Color(0, 0, 0, 0.5f), 0, 0, Screen.width, Screen.height);
			return blocker;
		}
	}
}

