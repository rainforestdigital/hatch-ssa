using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using HatchFramework;

using pumpkin.display;
using pumpkin;
using pumpkin.events;
using pumpkin.text;

public class DeviceWizardView : MovieClip
{
	private static DeviceWizardView instance;
	public static DeviceWizardView Instance
	{
		get
		{
			return instance;
		}

		set
		{
			instance = value;
		}
	}

	public TextField title;
	public TextField code;
	public TextField error;
	
	public LabeledButton submitBtn;
	public LabeledButton cancelBtn;
	
	public DeviceWizardView(string linkage):base(linkage)
	{		
		Initialize();
	}
		
	public DeviceWizardView(string swf, string symbol):base(swf, symbol)
	{
		Initialize();
	}
	
	private void Initialize()
	{
		instance = this;
		
		title = getChildByName<TextField>("title");
		code = getChildByName<TextField>("code");
		error = getChildByName<TextField>("error");
		
		/*cancelBtn = getChildByName<MovieClip>("cancelBtn");
		cancelBtn.getChildByName<TextField>("label").text = "CANCEL";
		submitBtn = getChildByName<MovieClip>("submitBtn");
		submitBtn.getChildByName<TextField>("label").text = "SUBMIT";*/
		
		cancelBtn = new LabeledButton(getChildByName<MovieClip>("cancelBtn"), "Cancel");
		submitBtn = new LabeledButton(getChildByName<MovieClip>("submitBtn"), "Submit");
		
		x = Screen.width*.5f - width*.5f;
		y = Screen.height*.5f - height*.5f;
	}
}
