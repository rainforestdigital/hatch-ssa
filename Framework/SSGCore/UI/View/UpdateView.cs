using System;
using UnityEngine;
using pumpkin.display;
using pumpkin.events;
using pumpkin.text;
using HatchFramework;

namespace SSGCore
{
	public class UpdateView
	{
		private DisplayObjectContainer container;
		private MovieClip clip;
		
		private TextField title_txt;
		private MovieClip bar_mc;
		
		private MovieClip bar_middle_mc;
		private MovieClip bar_end_mc;
		private DisplayObject wait_txt;
		private DisplayObject update_txt;
		private DisplayObject updateInfo_txt;
		private MovieClip spinner_mc;
		private MovieClip update_btn;
		private MovieClip updateLater_btn;
		private MovieClip fail_mc;
		
		public DisplayObject View
		{
			get { return container; }
		}
		
		public Action UpdateLaterClicked;
		public Action UpdateClicked;
		public Action UpdateBundlesClicked;
		public Action RetryClicked;
		public Action ExitClicked;
		
		
		
		public UpdateView ()
		{
			container = new DisplayObjectContainer();
			container.width = Screen.width;
			container.height = Screen.height;
			
			var background_mc = new MovieClip(MovieClipFactory.PATH_BACKGROUND, "Menu_Background");
			container.addChild(background_mc);
			background_mc.FillParent();

			var bottom_bar_mc = new MovieClip(MovieClipFactory.PATH_BACKGROUND, "Menu_Bar");
			container.addChild(bottom_bar_mc);
			bottom_bar_mc.ScaleAsIfFullScreen(2);
			bottom_bar_mc.StretchToWidth();
			bottom_bar_mc.y = container.height - bottom_bar_mc.height;
			
			clip = new MovieClip(MovieClipFactory.PATH_UPDATE, "UpdateWindow");
			var width = clip.width;
			var height = clip.height;
			clip.DoubleIfRetina();
			clip.x = (container.width - width*clip.scaleX) * 0.5f;
			clip.y = (container.height - height*clip.scaleY) * 0.5f;
			container.addChild(clip);
			
			title_txt = clip.getChildByName<TextField>("title_txt");
			bar_mc = clip.getChildByName<MovieClip>("bar_mc");
			wait_txt = clip.getChildByName<TextField>("wait_txt");
			update_txt = clip.getChildByName<TextField>("update_txt");
			updateInfo_txt = clip.getChildByName<TextField>("updateInfo_txt");
			spinner_mc = clip.getChildByName<MovieClip>("spinner_mc");
			update_btn = clip.getChildByName<MovieClip>("update_btn");
			updateLater_btn = clip.getChildByName<MovieClip>("updateLater_btn");
			fail_mc = clip.getChildByName<MovieClip>("fail_mc");
			bar_middle_mc = bar_mc.getChildByName<MovieClip>("middle_mc");
			bar_end_mc = bar_mc.getChildByName<MovieClip>("end_mc");
			
#if !UNITY_IPHONE
			update_btn.addEventListener(MouseEvent.CLICK, HandleUpdateClick);
			updateLater_btn.addEventListener(MouseEvent.CLICK, HandleUpdateLaterClick);
			
			new BasicButton(update_btn);
			new BasicButton(updateLater_btn);
#else
			update_btn.alpha = 0;
			updateLater_btn.alpha = 0;
#endif
			
			// workaround for padded UVs from UniSWF
			var child = bar_middle_mc.getChildAt<Sprite>(0);
			Debug.Log(bar_middle_mc.graphics.drawOPs);
			var op = child.graphics.drawOPs[0];
			var srcRect = op.srcRect;
			Debug.Log(op.srcRect);
			var offset = 0.5f  / 512f;
			child.graphics.clear();
			bar_middle_mc.graphics.clear();
			bar_middle_mc.graphics.drawRectUV(op.material, 
				new Rect(srcRect.x+offset, srcRect.y, 0, srcRect.height),
				new Rect(op.x, op.y, op.drawWidth, op.drawHeight));
			
			var exit_btn = fail_mc.getChildByName<MovieClip>("exit_btn");
			if( PlatformUtils.GetPlatform() != Platforms.IOS && PlatformUtils.GetPlatform() != Platforms.IOSRETINA )
			{
				exit_btn.addEventListener(MouseEvent.CLICK, HandleExitClick);
				new BasicButton(exit_btn);
			}
			else
			{
				exit_btn.mouseEnabled = false;
				fail_mc.removeChild( exit_btn );
			}
			
			var retry_btn = fail_mc.getChildByName<MovieClip>("retry_btn");
			retry_btn.addEventListener(MouseEvent.CLICK, HandleRetryClick);
			new BasicButton(retry_btn);
			
			ShowSpinner();
		}
		
		public void ShowSpinner() {
			title_txt.text = "Checking for updates to Shell Squad Adventures...";
			bar_mc.visible = false;
			wait_txt.visible = true;
			update_txt.visible = false;
			updateInfo_txt.visible = false;
			spinner_mc.visible = true;
			update_btn.visible = false;
			updateLater_btn.visible = false;
			clip.removeChild(updateLater_btn);
			fail_mc.visible = false;
			clip.removeAllEventListeners(CEvent.ENTER_FRAME);
			clip.addEventListener(CEvent.ENTER_FRAME, UpdateSpinner);
			clip.visible = true;
		}
		
		private void UpdateSpinner(CEvent e) {
			const int steps = 8;
			const int speed = 1;
			spinner_mc.rotation = 360 * Mathf.Floor(Time.time * steps * speed) / steps;
		}
		
		private void HandleUpdateClick(CEvent e) {
			if(UpdateClicked != null) {
				UpdateClicked();
			}
		}
		
		private void HandleUpdateLaterClick(CEvent e) {
			if(UpdateLaterClicked != null) {
				UpdateLaterClicked();
			}
		}
		
		private void HandleExitClick(CEvent e) {
			if(ExitClicked != null) {
				ExitClicked();
			}
		}
		
		private void HandleRetryClick(CEvent e) {
			if(RetryClicked != null) {
				RetryClicked();
			}
		}
		
		public void ShowProgress() {
			title_txt.text = "Installing updates to Shell Squad Adventures...";
			bar_mc.visible = true;
			wait_txt.visible = true;
			update_txt.visible = false;
			updateInfo_txt.visible = false;
			spinner_mc.visible = false;
			update_btn.visible = false;
			updateLater_btn.visible = false;
			clip.removeChild(updateLater_btn);
			fail_mc.visible = false;
			clip.removeAllEventListeners(CEvent.ENTER_FRAME);
			SetProgress(0);
			clip.visible = true;
		}
		
#if !UNITY_IPHONE
		public void ShowRequiredUpgrade() {
			title_txt.text = "Required update available to Shell Squad Adventures...";
			bar_mc.visible = false;
			wait_txt.visible = false;
			update_txt.visible = true;
			updateInfo_txt.visible = true;
			spinner_mc.visible = false;
			update_btn.visible = true;
			updateLater_btn.visible = false;
			clip.removeChild(updateLater_btn);
			fail_mc.visible = false;
			clip.removeAllEventListeners(CEvent.ENTER_FRAME);
			const int offset = 50;
			update_btn.x -= offset;
			updateInfo_txt.x -= offset;
			clip.visible = true;
		}
		
		public void ShowOptionalUpgrade() {
			title_txt.text = "Recommended update available to Shell Squad Adventures...";
			bar_mc.visible = false;
			wait_txt.visible = false;
			update_txt.visible = true;
			updateInfo_txt.visible = true;
			spinner_mc.visible = false;
			update_btn.visible = true;
			updateLater_btn.visible = true;
			clip.addChild(updateLater_btn);
			fail_mc.visible = false;
			clip.removeAllEventListeners(CEvent.ENTER_FRAME);
			clip.visible = true;
		}
#endif
		
		public void ShowFailure() {
			title_txt.text = "Failed to download updates to Shell Squad Adventures...";
			bar_mc.visible = false;
			wait_txt.visible = false;
			update_txt.visible = false;
			updateInfo_txt.visible = false;
			spinner_mc.visible = false;
			update_btn.visible = false;
			updateLater_btn.visible = false;
			clip.removeChild(updateLater_btn);
			fail_mc.visible = true;
			clip.removeAllEventListeners(CEvent.ENTER_FRAME);
			clip.visible = true;
		}
		
		public void SetProgress(float progress) {
			bar_middle_mc.scaleX = progress*575;
			bar_end_mc.x = bar_middle_mc.x + progress*575 - 1;
		}
		
		public void HideWindow() {
			clip.visible = false;
		}
		
		public void Unload() {
			MovieClipFactory.UnloadSwf(MovieClipFactory.PATH_UPDATE);
		}
	}
}

