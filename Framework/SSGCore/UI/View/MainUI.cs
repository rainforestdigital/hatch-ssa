using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using pumpkin;
using pumpkin.swf;
using pumpkin.display;
using pumpkin.events;
using UnityEngine;
using HatchFramework;


namespace SSGCore
{
	
	/*
	 * Flow
	 * ->PlayIntro
	 * -->Initialize
	 * --->Data Sync
	 * ---->LoadEntryPoint
	 */
	
	
	public class MainUI : MovieClip
	{
		public static int STAGE_WIDTH = 1920;
		public static int STAGE_HEIGHT = 1080;
		
		readonly UnityEngine.Color BLOCKER_COLOR = new UnityEngine.Color(0, 0, 0, 0.5f);
		
		
		private Classroom currentClassroom;
		private Child currentChild;
		
		private MovieClip background_mc;
		private MovieClip bar_mc;
		private MovieClip bar_text_mc;
		private MovieClip bar_logo_mc;
		private MovieClip blocker_mc;
		private DisplayObjectContainer screenContainer;
		private GameScreen screen;
		private MenuTrigger menuTrigger;
		private DisplayObjectContainer dialogContainer;
		private DisplayObject spinner;
		private VersionDisplay versionDisplay;
		private SessionInfo preTeacherModeSession;
		
		private Stack<GameScreen> dialogs = new Stack<GameScreen>();
		
		private Museum museumScreen;
		
		private DBObject databaseObject;
		IList<Classroom> classrooms;
		
		public SessionInfo currentSession;
		SessionController sessionController;
		
		private bool noChildrenOnce;
		
		public MainUI(DBObject db) {
			databaseObject = db;
			Initialize();
		}
		
		public void LoadEntryPoint(){
		
			string entry = GlobalConfig.GetProperty<string>("EntryPoint");
			
			string[] vals = entry.Split(' ');
			
			switch(vals[0]){
			case "Eula":
				GoToEula();
				break;
			case "Session":
				LoadSkill (vals[1], int.Parse(vals[2]));
				break;
			default:
				GoToInitialScreen();
				break;
			}
			
		}
		
		public void LoadSkill(string name, int level){
			
			LoadSkill(name, "Travel", level);
		}
		
		public void LoadSkill(string name, string theme, int level){
			
			SessionInfo config = currentSession;
			if(currentSession == null){
				config = new SessionInfo();
			}
			
			var skill = MockSkillBuilder.CreateSkillForName(name);
			config.teacherMode = false;
			config.skillPair = new SkillLevelPair(skill, (SkillLevel)level);
			config.family = skill.Family;
			config.theme = new Theme(theme);
			config.user = currentChild;
			LoadSession(config);
		}
		
		protected void SetupDatabase() {
			databaseObject.OnDatasetChanged += OnDatasetUpdated;
			ReloadDatabase();
		}
		
		protected void ReloadDatabase(){
			
			if(currentSession.demoMode){
				classrooms = StaticData.GenerateDemoDataSet();
			}else{
				databaseObject.currentUserID = -1;
				databaseObject.LoadClassrooms();
				databaseObject.LoadUsers();
				databaseObject.LoadRefocusActivities();
				databaseObject.LoadLocks();
				databaseObject.LoadSessions();
				
				classrooms = databaseObject.BuildDataset();
				
				int i = 0;
				while( currentClassroom != null && i < classrooms.Count)
				{
					if( classrooms[i].ID == currentClassroom.ID )
					{
						currentClassroom = classrooms[i];
						break;
					}
					i++;
				}
			}
		}
		
		
		/// <summary>
		/// Initialize this instance;
		/// </summary>
		protected void Initialize(){
			
			if(PlatformUtils.GetPlatform() == Platforms.WIN)
				Screen.SetResolution( 1920, 1080, true );
				
			currentSession = new SessionInfo();
			
			noChildrenOnce = false;
			
			background_mc = new MovieClip(MovieClipFactory.PATH_BACKGROUND, "Menu_Background");
			addChild(background_mc);

			bar_mc = new MovieClip(MovieClipFactory.PATH_BACKGROUND, "Menu_Bar");
			addChild(bar_mc);

			bar_logo_mc = new MovieClip(MovieClipFactory.PATH_BACKGROUND, "Bar_Logo");
			addChild(bar_logo_mc);

			bar_text_mc = new MovieClip(MovieClipFactory.PATH_BACKGROUND, "Bar_Text");
			addChild(bar_text_mc);

			screenContainer = new DisplayObjectContainer();
			addChild(screenContainer);
			
			MovieClip sv = MovieClipFactory.CreateEmptyMovieClip();
			addChild (sv);
			sessionController = new SessionController(sv, databaseObject);
			sessionController.OnStop += HandleStop;
			sessionController.Sync = NormalSync;
			sessionController.OnExitTeacherMode += onTeacherModeClicked;
			
			menuTrigger = new MenuTrigger();
			menuTrigger.OnTriggered = PushTeacherMenu;
			menuTrigger.OnComboMenu = () => { sessionController.Pause(); };
			menuTrigger.OnComboClear = () => { sessionController.Resume(); };
			addChild(menuTrigger);
			
			versionDisplay = SSGEngine.Instance.GetComponent<VersionDisplay>();
			
			/*teacherModeBar = new TeacherMode();
			addChild(teacherModeBar);
			teacherModeBar.OnExitClicked = ExitTeacherMode;
			teacherModeBar.visible = SSGEngine.Instance.TeacherMode;*/
			
			blocker_mc = new MovieClip();
			addChild(blocker_mc);
			blocker_mc.visible = false;
			
			dialogContainer = new DisplayObjectContainer();
			addChild(dialogContainer);
			
			// any texture is as good as any other at this point
			var stage = MovieClipOverlayCameraBehaviour.instance.stage;
			stage.addEventListener(MouseEvent.RESIZE, HandleResize);
			
			HandleResize(null);
			
			// spinner
			spinner = MovieClipFactory.CreateMainUISpinner();
			addChild(spinner); 
			DebugConsole.LogWarning("SPINNER NULL? " + (spinner.getChildByName<MovieClip>("spinner") == null));
			HideSyncClip();
			
			SetupDatabase();
			SSGEngine.Instance.StartCoroutine(DeviceStartupSync());
			
			
		}
				
		public IEnumerator DeviceStartupSync(){

			//load legacy UID into database IF we don't have a UID yet
			if(string.IsNullOrEmpty(databaseObject.GetDeviceUid())){
				string legacyID = LegacyUpgrade.GetLegacyUID ();
				if(LegacyUpgrade.www != null)
					yield return LegacyUpgrade.www;
				if(LegacyUpgrade.www != null && LegacyUpgrade.www.error == null)
					legacyID = LegacyUpgrade.www.text.Replace("\"", "");
				if(legacyID.IndexOf("Error") > -1 || legacyID.IndexOf("false") > -1)
					legacyID = "";
				if (!string.IsNullOrEmpty (legacyID)) {
					databaseObject.InsertSetting (legacyID);
				}
			}
			
			if( String.IsNullOrEmpty( databaseObject.GetDeviceUid() ) || 
				(( PlatformUtils.GetPlatform() == Platforms.IOSRETINA || PlatformUtils.GetPlatform() == Platforms.IOS ) && !string.IsNullOrEmpty( PlatformUtils.GetSerial() )) )
			{
				//special iOS licensing check
				if( HasInternet() )
				{
					ShowSyncClip();
					
					bool stat = false, fin = false;
					DateTime exDate = DateTime.Now;
					Dictionary<string, string> information = null;
					new RMS.Webservice.GetDeviceLicense().Fetch( PlatformUtils.GetSerial(), (status, expirationDate, info) => {
						fin = true;
						stat = status;
						exDate = expirationDate;
						information = info;
					} );
					
					while( !fin )
					{
						yield return new WaitForSeconds( 0.01f );
					}
					
					if( stat && String.IsNullOrEmpty( information["unique_id"] ) )
					{
						stat = false;
						DebugConsole.LogWarning( "RMS returned dummy data - Unregistered Device." );
					}
					
					if( stat )
					{
						if( String.IsNullOrEmpty( databaseObject.GetDeviceUid() ) )
						{
							databaseObject.InsertSetting( information["unique_id"] );
						}
						
						if( PlatformUtils.GetPlatform() == Platforms.IOSRETINA || PlatformUtils.GetPlatform() == Platforms.IOS )
						{
							PlayerPrefs.SetString( "LexpirationDate", exDate.ToString( "yyyy-MM-dd" ) );
							PlayerPrefs.SetString( "Lname", information["name"] );
							PlayerPrefs.SetString( "Llicense", information["license"] );
							PlayerPrefs.SetString( "Lorginaization", information["orginization"] );
							PlayerPrefs.SetString( "Lsale", information["sale"] );
							
							int daysLeft = exDate.Subtract( DateTime.Now ).Days;
							bool showWarn = false;
							bool showExp = false;
							
							DebugConsole.Log( "License will expire in " + daysLeft + " days." );
							
							if( daysLeft <= 15 ) {
								showWarn = true;
								if( exDate.Subtract( DateTime.Now ).TotalDays <= 0 )
									showExp = true;
								else if( exDate.Subtract( DateTime.Now ).TotalDays < 1 )
									daysLeft++;
							}
							else {
								switch( daysLeft ){
								case 90:
								case 60:
								case 45:
								case 30:
									showWarn = true;
									break;
								default:
									showWarn = false;
									break;
								}
							}
							if( showWarn )
							{
								HideSyncClip();
								
								//display screen
								LicenseReminder reminder = new LicenseReminder( daysLeft, information );
								addChild( reminder );
								//send LoadEntryPoint as 'callback' for close
								//but if expired, Application.Quit instead
								if( showExp )
									reminder.onClose += Application.Quit;
								else
									reminder.onClose += () => { SSGEngine.Instance.StartCoroutine( StartupSyncAndLoadEntry() ); };
								
								yield break;
							}
						}
					}
					else
						DebugConsole.LogWarning( "There was an error obtaining the license info." );
					
					HideSyncClip();
				}
				else if( !String.IsNullOrEmpty( databaseObject.GetDeviceUid() ) && PlayerPrefs.HasKey( "LexpirationDate" ) )
				{
					DateTime exDateS = DateTime.ParseExact( PlayerPrefs.GetString( "LexpirationDate" ), "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
					
					Dictionary<string,string> informationS = new Dictionary<string, string>();
					informationS["name"] = PlayerPrefs.GetString( "Lname" );
					informationS["license"] = PlayerPrefs.GetString( "Llicense" );
					informationS["orginization"] = PlayerPrefs.GetString( "Lorginaization" );
					informationS["sale"] = PlayerPrefs.GetString( "Lsale" );
					informationS["dateString"] = exDateS.ToShortDateString();
					
					int daysLeftS = exDateS.Subtract( DateTime.Now ).Days;
					bool showWarnS = false;
					bool showExpS = false;
					
					DebugConsole.Log( "License will expire in " + daysLeftS + " days." );
					
					if( daysLeftS <= 15 ) {
						showWarnS = true;
						if( exDateS.Subtract( DateTime.Now ).TotalDays <= 0 )
							showExpS = true;
						else if( exDateS.Subtract( DateTime.Now ).TotalDays < 1 )
							daysLeftS++;
					}
					else {
						switch( daysLeftS ){
						case 90:
						case 60:
						case 45:
						case 30:
							showWarnS = true;
							break;
						default:
							showWarnS = false;
							break;
						}
					}
					if( showWarnS )
					{
						HideSyncClip();
						
						//display screen
						LicenseReminder reminderS = new LicenseReminder( daysLeftS, informationS );
						addChild( reminderS );
						//send LoadEntryPoint as 'callback' for close
						//but if expired, Application.Quit instead
						if( showExpS )
							reminderS.onClose += Application.Quit;
						else
							reminderS.onClose += () => { SSGEngine.Instance.StartCoroutine( StartupSyncAndLoadEntry() ); };
						
						yield break;
					}
				}
			}
			
			ShowSyncClip();
			
			if(!string.IsNullOrEmpty(databaseObject.GetDeviceUid()))
			{
				//we have a UID - validate it if we have internet access
				if(HasInternet()){
					yield return SSGEngine.Instance.StartCoroutine(SSGCore.RMS.DataSync.Instance.SyncStartup(this.databaseObject));
				}
			}

			versionDisplay.UpdateText(databaseObject);
			
			//SYNC HERE - call LoadEntryPoint on success
			ReloadDatabase();

			yield return new WaitForSeconds(0.15f);
			
			HideSyncClip();
			
			LoadEntryPoint();
			
			yield break;
		}
		
		public IEnumerator StartupSyncAndLoadEntry(){
			ShowSyncClip();
			
			yield return SSGEngine.Instance.StartCoroutine(SSGCore.RMS.DataSync.Instance.SyncStartup(this.databaseObject));
			
			versionDisplay.UpdateText(databaseObject);
			
			ReloadDatabase();
			
			yield return new WaitForSeconds(0.15f);
			
			HideSyncClip();
			
			LoadEntryPoint();
		}
		
		public IEnumerator SyncAndUpdateSession(){
			yield return SSGEngine.Instance.StartCoroutine(NormalSync());
			
			if(SSGCore.RMS.DataSync.Instance.LastSyncSucceeded) {
				sessionController.RestartSessionIfNoSkillsAvailable();
			}
		}
		
		public IEnumerator NormalSync(){ yield return SSGEngine.Instance.StartCoroutine( NormalSync(true) ); }
		public IEnumerator NormalSync( bool showSync ){
			
			//terminate from sync!
			if(string.IsNullOrEmpty(databaseObject.GetDeviceUid()))
				yield break;
				
			if(HasInternet()){
				if( showSync ) ShowSyncClip();
				yield return SSGEngine.Instance.StartCoroutine(SSGCore.RMS.DataSync.Instance.SyncNormal(this.databaseObject));
				if( showSync ) HideSyncClip();
			}
			yield break;
		}
		
		public void ShowSyncClip()
		{
			spinner.visible = true;
		}
		
		public void HideSyncClip()
		{
			spinner.visible = false;
		}
		
		private void OnDatasetUpdated()
		{
			var refreshable = screen as Refreshable;
			if(refreshable != null) {
				refreshable.Refresh();
			}
		}
		
		private void HandleResize(CEvent e)
		{
			width = Screen.width;
			height = Screen.height;
			
			background_mc.FillParent();
			
			float sMod = 1f;
			if(Screen.width > 1280)
				sMod = 0.8f;
			
			bar_mc.ScaleAsIfFullScreen(2);
			bar_mc.StretchToWidth();
			bar_mc.scaleY *= sMod;
			bar_mc.y = height - bar_mc.height;

			bar_logo_mc.ScaleAsIfFullScreen(2);
			bar_logo_mc.scaleX = bar_logo_mc.scaleY *= sMod * 0.75f;
			bar_logo_mc.x = (Screen.width) - (bar_logo_mc.width) - (bar_logo_mc.width * 0.2f);
			bar_logo_mc.y = bar_mc.y + ((bar_mc.height * 0.5f) - (bar_logo_mc.height * 0.5f));

			bar_text_mc.ScaleAsIfFullScreen(2);
			bar_text_mc.scaleX = bar_text_mc.scaleY *= sMod;
			bar_text_mc.x = (Screen.width * 0.5f) - (bar_text_mc.width * 0.5f);
			bar_text_mc.y = bar_mc.y + ((bar_mc.height * 0.5f) - (bar_text_mc.height * 0.5f));

			//teacherModeBar.Resize(Screen.width, Screen.height);
			
			if(screen != null) {
				screen.Resize(Screen.width, Screen.height);
			}
			
			menuTrigger.Resize(Screen.width, Screen.height);
			
			foreach (var dialog in dialogs) {
				dialog.Resize(Screen.width, Screen.height);
			}
			
			blocker_mc.width = width;
			blocker_mc.height = height;
			blocker_mc.graphics.clear();
			blocker_mc.graphics.drawSolidRectangle(BLOCKER_COLOR, 0, 0, width, height);
		}
		
		private void GoToSelectClassroom()
		{
			if( SoundEngine.Instance != null )
			{
				SoundEngine.Instance.SendCancelToken();
				SoundEngine.Instance.PlayTheme(null);
			}
			
			//Debug.Log (classrooms.Count);
			currentSession.classroom = null;
			currentClassroom = null;
			var s = new SelectClassroomScreen(classrooms);
			s.NoClassrooms += GoToInitialScreen;
			s.ClassroomSelected += (Classroom c) => {
				currentSession.classroom = c;
				currentClassroom = c;
				GoToSelectChild();
			};

			SetScreen(s);
		}
		
		bool forceReRegister = false;
		
		private void GoToInitialScreen() 
		{
			bool registered = forceReRegister ? false : !String.IsNullOrEmpty(databaseObject.GetDeviceUid());
			forceReRegister = false;
			bool hasInternet = HasInternet();
			bool hasClasses = databaseObject.Classrooms.Count > 0;
			bool hasTeachers = databaseObject.Classrooms.Any(x => x.TeacherID >= 0 );
			bool hasStudents = classrooms.Any( x => x.HasChildren );
			bool canContinue = hasClasses && hasTeachers && hasStudents;
			currentClassroom = null;
			DebugConsole.Log ("Current Initial states: continue: {0}, hasInternet: {1}, hasClasses:{2}, hasTeachers:{3}, hasStudents:{4} registered:{5}", canContinue, hasInternet, hasClasses, hasTeachers, hasStudents, registered);
			if( !registered) 
			{
				GoToRegistration();
			} else if(Eula.ShouldDisplay)
			{
				GoToEula();
			} else if( !canContinue && !hasInternet) 
			{
				GoToStartupMenu(StartupMenuType.Offline);
			} else if( !hasClasses ) 
			{
				GoToStartupMenu(StartupMenuType.SetupClass);
			} else if( !hasTeachers ) 
			{
				GoToStartupMenu(StartupMenuType.SetupTeachers);
			} else if( !hasStudents ) 
			{
				GoToStartupTeacherMenu(false);
			} else if(!hasInternet) 
			{
				GoToStartupMenu(StartupMenuType.OfflineSynced);
			} else 
			{
				GoToSelectClassroom();
			}
		}
		
		private void GoToStartupMenu(StartupMenuType type)
		{
			var s = new StartupMenu(type);
			s.DemoClicked = StartDemoMode;
			s.ExitGameClicked = SSGEngine.Instance.Quit;
			s.SetupClassClicked = () => OpenStartupRMS("wizard");
			s.SetupTeachersClicked = () => OpenStartupRMS("/classroom/admin");
			s.ContactAdminClicked = GoToContactAdminScreen;
			s.ContinueClicked = GoToSelectClassroom;
			SetScreen(s);
		}
		
		private void StartDemoMode()
		{		
			ToggleDemoMode();	
		}
		
		private void GoToSelectChild()
		{
			AudioListener.pause = false;// reset to false just incase it wasn't after unloading the last theme/skill
			
			ShowSyncClip();
		
			if(currentClassroom == null || !currentClassroom.Valid) 
			{
				GoToInitialScreen();
				return;
			}
			
			MuseumDataObject.loadDB( databaseObject );
			
			var s = new SelectChildScreen(currentClassroom);
			s.Invalidated += GoToInitialScreen;
			s.BackPressed += GoToSelectClassroom;
			s.ChildSelected += OnChildSelected;
			SetScreen(s);
			HideSyncClip();
		}
		
		private void OnSessionExit()
		{
			GoToInitialScreen();
		}
		
		private void OnChildSelected(Child c)
		{
			currentSession.user = c;
			currentChild = c;
			
			MuseumDataObject.populate();
			
			SkillSessionContext context;
			if(currentSession.demoMode) {
				context = new DemoSkillContext(currentChild);
			} else if(currentSession.teacherMode) {
				context = new TeacherSkillContext(databaseObject, currentChild);
			} else {
				context = new DatabaseSkillContext(databaseObject, currentChild);
			}
			
			DebugConsole.LogWarning ("Checking Bookmark Conditions; HasInternet? " + HasInternet() + "; demoMode? " + currentSession.demoMode + "; bookmark? " + (!string.IsNullOrEmpty(c.BookmarkRaw)) );
			
			if( HasInternet() && !currentSession.demoMode && (!string.IsNullOrEmpty(c.BookmarkRaw))){
				DebugConsole.Log("BOOKMARK FOUND");
//				Debug.Log(c.BookmarkRaw);
				
				LitJson.JsonData bookmark = c.Bookmark;
				string[] sdata = ((string)bookmark["skillSer"]).Split('+');
				if(sdata.Length > 1)
				{
					var oldSession = currentSession;
					currentSession.ResetSessionStats();
					currentSession.Deserialize( (string)bookmark["themeSer"], sdata[1], sdata[0] );
					
					if( SkillImplementationUtil.getLangTagforSkill( currentSession.skillPair.Skill.linkage, LanguageProfileUtility.FromId( c.Profile ) ).Contains( "BAD" ) )
					{
						DebugConsole.LogError( "Bookmarked skill is incompatible with child's profile somehow - abandoning bookmark" );
						currentSession = new SessionInfo();
						currentSession.demoMode = oldSession.demoMode;
						
						currentSession.skillPair = (new SkillSession( context )).ChooseFirstSkill();
					}
					else
						sessionController.deserialized = false;
				}
				else
				{
					DebugConsole.LogError("Bookmark is incompatible, abandoning it");
					
					var oldSession = currentSession;
					currentSession = new SessionInfo();
					currentSession.demoMode = oldSession.demoMode;
				
					currentSession.skillPair = (new SkillSession( context )).ChooseFirstSkill();
				}
			}
			else{
				DebugConsole.LogWarning("Bookmark usage conditions not met, continuing without bookmark");
				
				var oldSession = currentSession;
				currentSession = new SessionInfo();
				currentSession.demoMode = oldSession.demoMode;
				
				currentSession.skillPair = (new SkillSession( context )).ChooseFirstSkill();
			}
			
			if( currentSession.skillPair == null )
			{
				SetScreen(null);
				sessionController.ShowNoAvailableSkillsView();
			}
			else
			{
				MuseumDataObject.nextSkill = currentSession.skillPair.Skill.linkage;
				
				museumScreen = new Museum();
				museumScreen.addEventListener( MuseumEvent.COMPLETED, MuseumReturn );
				museumScreen.addEventListener( MuseumEvent.STOPPED, MuseumStopped );
				
				SetScreen( museumScreen );
			}
		}
		
		private void MuseumReturn( CEvent e )
		{
			LoadSession( currentSession );
			
			museumScreen.Dispose();
			museumScreen = null;
		}
		
		private void MuseumStopped( CEvent e )
		{
			museumScreen.Dispose();
			museumScreen = null;
			
			GoToInitialScreen();
		}
		
		public void BackToMuseum( SkillLevelPair lastLevel )
		{
			MuseumDataObject.awardingSkill = lastLevel.Skill.linkage;
			MuseumDataObject.awardingLevel = lastLevel.Level;
			
			museumScreen = new Museum();
			museumScreen.addEventListener( MuseumEvent.COMPLETED, SessionReturn );
			museumScreen.addEventListener( MuseumEvent.STOPPED, MuseumStopped );
			
			SetScreen( museumScreen );
		}
		
		private void SessionReturn( CEvent e )
		{
			SetScreen(null);
			
			sessionController.ContinueSkillComplete();
			
			museumScreen.Dispose();
			museumScreen = null;
		}
		
		private void CheckForEula()
		{
			if(Eula.ShouldDisplay) 
			{
				GoToEula();
			} 
			else 
			{
				// since this is currently only called after registration,
				// always do a full sync even if EULA already agreed to
				SetScreen(null);
				SSGEngine.Instance.StartCoroutine(DeviceStartupSync());
			}
		}
		
		private void GoToEula()
		{
			var s = new EulaScreen();
			s.Reject = SSGEngine.Instance.Quit;
			s.Accept = (string username) => {
				Debug.Log ("SAVING EULA");
				databaseObject.InsertEulaAgreement(username);
				Eula.StoreEulaAcceptance(username);
				SetScreen(null);
				//Do full sync
				SSGEngine.Instance.StartCoroutine(DeviceStartupSync());
			};	
			SetScreen(s);
		}
		
		private void GoToRegistration()
		{
			var r = new RegistrationScreen();
			r.Reject = SSGEngine.Instance.Quit;
			r.Accept = GoToRegSuccess;
			r.ManualRegistration = GoToManualRegistration;
			SetScreen(r);
		}
		
		private void GoToRegSuccess(RegistrationResult result)
		{
			var r = new RegistrationSuccessScreen(result);
			
			//create temporary settings until sync
			databaseObject.ClearSettings();
			databaseObject.InsertSetting(result.deviceCode);
			r.Reject = SSGEngine.Instance.Quit;
			r.Accept = () => {
				CheckForEula();
			};
			SetScreen(r);
		}
		
		private void GoToManualRegistration()
		{
			var r = new ManualRegistrationScreen();
			r.Cancel = GoToRegistration;
			r.Registered = GoToManualRegSuccess;
			SetScreen(r);
		}
		
		private void GoToManualRegSuccess(RegistrationResult result)
		{
			databaseObject.ClearSettings();
			databaseObject.InsertSetting(result.deviceCode);
			
			var r = new ManualRegistrationSuccessScreen(result);
			r.Exit = SSGEngine.Instance.Quit;
			SetScreen(r);
		}
		
		private void SyncRMS()
		{
			
			SSGEngine.Instance.StartCoroutine(SyncAndUpdateSession());
		}
		
		private void CloseTeacherMenu()
		{
			
			PopDialog();
			sessionController.Resume();
			SyncRMS();
		}
		
		private bool HasInternet()
		{
			return Application.internetReachability != NetworkReachability.NotReachable
				&& InternetConnectivity.hasConnection;
		}
		
		private void PushTeacherMenu()
		{			
			var param = new TeacherMenu.Params() {
				inGame = sessionController.inGame,
				inTeacherMode = currentSession.teacherMode,
				inDemoMode = currentSession.demoMode,
				canClose = true,
				hasInternet = HasInternet()
			};
			var menu = new TeacherMenu(param);
			menu.CloseClicked = CloseTeacherMenu;
			menu.AddChildClicked = PushAddChild;
			menu.EditChildImageClicked = PushEditChild;
			menu.EditChildClicked = () => OpenRMS("/child/admin");
			menu.ClassOverviewClicked = () => OpenRMS("/report/classoverview");
			menu.UnlockSkillClicked = () => OpenRMS("/report/classoverview");
			menu.DualLanguageClicked = () => OpenRMS("child/manageclass");

			menu.WizardTrigger = () => OpenRMS("wizard");
			
			menu.ExitGameClicked = SSGEngine.Instance.Quit;
			menu.TeacherModeClicked = () => {
				PopDialog();
				onTeacherModeClicked();
			};
			menu.DemoModeClicked = () => {
				CloseTeacherMenu();
				ToggleDemoMode();
			};
			PushDialog(menu);
		}

		void ExitDemoMode ()
		{
			if(sessionController.inGame) {
				sessionController.CancelSession();
			}
			currentSession.demoMode = true;
			ReloadDatabase();
			GoToSelectClassroom();
		}
		
		private void ToggleDemoMode()
		{
			DebugConsole.Log("Toggling demo mode: inDemoMode {0} inGame {1}", currentSession.demoMode, sessionController.inGame);
			if(currentSession.demoMode) {
				// if in demo mode, cancel demo mode and go back to classroom select if applicable
				if(sessionController.inGame) {
					sessionController.CancelSession();
				}
				currentSession.demoMode = false;
				ReloadDatabase();
				GoToInitialScreen();
			} else if(sessionController.inGame) {
				// if in game, show confirmation for whether to cancel
				// if yes, go to demo class login, if no, return to teacher menu
				// assuming yes for now
				sessionController.Pause();
				var dialog = new ConfirmationDialog("Entering Demo Mode",
					"Are you sure that you wish to proceed?");
				dialog.OkayClicked = () => {
					PopDialog();
					sessionController.Resume();
					ExitDemoMode();
				};
				dialog.CancelClicked = () => {
					PopDialog();
					sessionController.Resume();
				};
				PushDialog(dialog);
			} else {
				ExitDemoMode();
			}
		}
		
		private bool HaveChildren()
		{
			return classrooms.Any( x => x.Children.Count > 0 );
		}
		
		private void GoToStartupTeacherMenu(bool canClose)
		{
			Action onDialogClose = () => {
				ReloadDatabase();
				if(HaveChildren()) {
					GoToStartupTeacherMenu(true);
				}
			};
			
			var param = new TeacherMenu.Params() {
				startupPrompt = !canClose,
				canClose = canClose,
				hasInternet = HasInternet()
			};

			DebugConsole.Log ("Showing startup teacher menu with internet: {0}", param.hasInternet);

			if(!noChildrenOnce)
			{
				noChildrenOnce = true;
				param.firstNoChildren = true;
			}
			
			var menu = new TeacherMenu(param);
			menu.CloseClicked = () => { SyncRMS(); GoToSelectClassroom(); };
			menu.AddChildClicked = () => PushAddChild(onDialogClose);
			menu.EditChildImageClicked = () => PushEditChild(onDialogClose);
			menu.ClassOverviewClicked = () => OpenRMS("/report/classoverview");
			menu.WizardTrigger = () => OpenRMS("wizard");
			menu.WhoAreYouTrigger = () => OpenRMS("whoAreYou");
			menu.DemoModeClicked = () => {
				CloseTeacherMenu();
				ToggleDemoMode();
			};
			menu.DualLanguageClicked = () => OpenRMS("child/manageclass");
			menu.ExitGameClicked = SSGEngine.Instance.Quit;
			SetScreen(menu);
		}
		
		private void FetchAdmins(Action<IEnumerable<Admin>> handler) {
			new RMS.Webservice.Admin().Fetch(databaseObject.GetDeviceUid(), (status) => {
				var admins = new List<Admin>();
				if(status) {
					foreach (var admin in RMS.Webservice.Admin.fetched) {
						admins.Add( new Admin(admin) );
					}
				}
				handler(admins);
			});
		}
		
		private void GoToContactAdminScreen()
		{
			SetScreen(null);
			FetchAdmins((IEnumerable<Admin> admins) => {
				var menu = new ContactAdminMenu(admins);
				menu.ExitGameClicked = SSGEngine.Instance.Quit;
				SetScreen(menu);
			});
		}
		
		public void ExitTeacherMode()
		{
		
			//reload current session without teachermode 
			if(preTeacherModeSession != null) {
				currentSession = preTeacherModeSession.Clone();
				preTeacherModeSession = null;
			}
			currentSession.teacherMode = false;
			LoadSession(currentSession);
		}
		
		private void onTeacherModeClicked()
		{
			SSGEngine.Instance.StartCoroutine(DoToggleTeacherMode());
		}
		 
		private IEnumerator DoToggleTeacherMode()
		{
			yield return SSGEngine.Instance.StartCoroutine(NormalSync());
			
			sessionController.Resume();
			SoundEngine.Instance.StopAll();
			
			if(currentSession.teacherMode) {
				ExitTeacherMode();
			} else {
				currentSession = sessionController.CurrentConfig.Clone();
				preTeacherModeSession = currentSession.Clone();
				currentSession.ResetSessionStats();
				currentSession.teacherMode = true;
				LoadSession(currentSession);
			}
		}
		
		private void PushEditChild(Action onClose)
		{
			var s = new EditChildScreen(classrooms);
			s.CloseClicked = () => { PopDialog(); if(onClose != null) onClose(); };
			s.PhotoChanged = (Child child, Texture2D texture) => {
				if(!currentSession.demoMode) {
					databaseObject.SetUserImage(child, texture);
				}
				PopDialog(); 
				if(onClose != null) onClose();
			};
			PushDialog(s);
		}
		
		private void PushAddChild(Action onClose)
		{
			var s = new AddChildScreen(classrooms);
			s.CloseClicked = () => { PopDialog(); if(onClose != null) onClose(); };
			s.ChildAdded = (Child child, Classroom classroom) => {
				if(!currentSession.demoMode) {
					string folder = Path.Combine(PlatformUtils.InternalDataPath, GlobalConfig.GetProperty<string>("LOCAL_DIR"));
					string fileName = "images/users/" + child.UID+ ".png";
					string imagePath = Path.Combine(folder, fileName);
					ImageUtility.TextureToFile(imagePath, (Texture2D)child.Texture);
					
					databaseObject.InsertUser(child, classroom);
					
					(new RMS.Webservice.Save()).CreateChild(
						databaseObject.GetDeviceUid(),
						child.UID,
						child.FirstName,
						child.LastName,
						child.CustomId,
						child.BirthDate,
						classroom.ID.ToString(),
						imagePath,
						LanguageProfileUtility.FromId( child.Profile ),
						null
 					);
				}
				PopDialog();
				if(onClose != null) {
					onClose();
				}
			};
			PushDialog(s);
		}
		
		private void PushEditChild()
		{
			PushEditChild(null);
		}
		
		private void PushAddChild()
		{
			PushAddChild(null);
		}
		
		private void GoToSelectTheme()
		{
			var s = new SelectThemeScreen(currentChild, currentClassroom.Themes);
			s.ThemeSelected += OnThemeSelected;
			s.BackPressed += GoToSelectChild;
			SetScreen(s);
		}
		
		protected void OnThemeSelected(Theme theme){
			
			var oldSession = currentSession;
			currentSession = new SessionInfo();
			currentSession.demoMode = oldSession.demoMode;
			Debug.Log ("Selected theme: "+theme.themeName);
			currentSession.theme = theme;
			
			LoadSession(currentSession);
		}
		
		private void UnimplementedScreen()
		{
			GoToSelectClassroom();
		}
		
		private void OpenStartupRMS(string redirect)
		{
			var rms = new SSGCore.RMS.Admin();
			rms.ExitedEvent = () => SSGEngine.Instance.StartCoroutine(DeviceStartupSync());
			if(redirect == "wizard")
				rms.OpenWizard(databaseObject.GetDeviceUid());
			else if(redirect == "whoAreYou")
				rms.OpenWhoAreYou(databaseObject.GetDeviceUid());
			else
				rms.Open(redirect); 
		}
		
		private void OpenRMS(string redirect)
		{
			var rms = new SSGCore.RMS.Admin();
			if(currentSession.demoMode) {
				rms.OpenLocal(redirect);
			} else {
				rms.ExitedEvent = () => SSGEngine.Instance.StartCoroutine(NormalSync());
				if(redirect == "wizard")
					rms.OpenWizard(databaseObject.GetDeviceUid());
				else if(redirect == "whoAreYou")
					rms.OpenWhoAreYou(databaseObject.GetDeviceUid());
				else
					rms.Open(redirect);
			}
		}
		
		public void HandleStop()
		{
			ShowSyncClip();
			
			TimerUtils.SetTimeout(.25f, StopAfterShowSync);
			
			int complete;
			if( sessionController.bookmark == null ){
				complete = sessionController.CurrentConfig.correct + sessionController.CurrentConfig.incorrect;
				DebugConsole.Log( "Pulling 'complete' from session: " + complete ); 
			}
			else{
				complete = (int)sessionController.bookmark["questions_answered"];
				DebugConsole.Log( "Pulling 'complete' from bookmark: " + complete ); 
			}
			
			if(sessionController.syncNeeded)
				SSGEngine.Instance.StartCoroutine(sessionController.SessionSync( false ));
			else if(complete > 0 && sessionController.bookmark != null)
			{
				databaseObject.pushBookmark( sessionController.bookmark.ToJson(), (int)sessionController.bookmark["user_id"] );
				sessionController.bookmark = null;
			}
		}
		
		
		public void HandleTimeout(){
			if (currentClassroom != null) {
				PopDialog ();
				
				if(screen != null)
				{
					screenContainer.removeChild(screen.View);
					screen.Dispose();
				}
				
				HandleStop();
				
				sessionController.Resume ();
				sessionController.CancelSession ();
			}
		}
		
		private void StopAfterShowSync()
		{
			sessionController.CancelSession();
			
			ReloadDatabase();
			
			if(currentClassroom != null) {
				TimerUtils.SetTimeout( 3, GoToSelectAndHide );
			} else {
				GoToInitialScreen();
				HideSyncClip();
			}
		}
		
		private void GoToSelectAndHide()
		{
			GoToSelectChild();
			HideSyncClip();
		}
		
		private void SetScreen(GameScreen newScreen)
		{
			if(screen != null)
			{
				screenContainer.removeChild(screen.View);
				screen.Dispose();
			}
			
			screen = newScreen;
			
			if(newScreen != null){
				screen.Start();
				screen.Resize(Screen.width, Screen.height);
				screenContainer.addChild(screen.View);
			
				DebugConsole.Log("New Screen is classroom select: {0}", (newScreen.GetType() == typeof(SelectClassroomScreen)));
			}
			versionDisplay.SetVisible(screen != null);

		}
		
		public void LoadSession(SessionInfo s)
		{
			if( s.user == null ) s.user = currentChild;
			currentSession = s.Clone();
			SetScreen(null);
			sessionController.LoadSession(s);
			
			currentSession.ClearSerialization();
		}
		
		public void LoadTeacherMode()
		{		
			// kill the theme and skill
			sessionController.CancelSession();
			
			currentSession.teacherMode = true;
			LoadSession(currentSession);
		}

		private void PushDialog(GameScreen dialog)
		{
			dialogs.Push(dialog);
			dialog.Start();
			
			dialog.Resize(Screen.width, Screen.height);
			dialogContainer.addChild(dialog.View);
			blocker_mc.visible = true;
			menuTrigger.SetEnabled(false);
		}
		
		private void PopDialog()
		{
			if(dialogs.Count > 0) {
				var dialog = dialogs.Pop();
				dialogContainer.removeChild(dialog.View);
				dialog.Dispose();
				blocker_mc.visible = dialogs.Count > 0;
				menuTrigger.SetEnabled(dialogs.Count == 0);
			}
		}
	}
}

