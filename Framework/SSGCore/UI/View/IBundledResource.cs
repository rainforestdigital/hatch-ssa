using System;
using System.Collections.Generic;
using pumpkin;
using pumpkin.swf;
using pumpkin.display;
using UnityEngine;

namespace SSGCore
{
	/// <summary>
	
	/// </summary>
	public interface IBundledResource
	{
		string ResourcePath{get;}
		AssetBundle ResourceBundle{get;}
	}
}

