using UnityEngine;
using System.Collections;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;

namespace SSGCore
{
	
	public delegate void OnIntroComplete();
	
	public class IntroAnimation : DisplayObjectContainer
	{
	
		OnVideoComplete OnVideoCompleteInvoker;
		public event OnVideoComplete OnVideoCompleteEvent{ add{OnVideoCompleteInvoker += value;} remove{OnVideoCompleteInvoker -= value;} }
		
		private MovieClip view;
		
		private MovieClip background;
		
		//	Characters
		private MovieClip cam;
		private MovieClip henry;
		private MovieClip plattie;
		
		//Audio
		private AudioClip introClip1;
		private AudioClip introClip2;
		
		public IntroAnimation()
		{
			
		
			
			this.width = Screen.width;
			this.height = Screen.height;
			
			background = new MovieClip();
			background.graphics.drawSolidRectangle(Color.white, 0, 0, Screen.width, Screen.height);
			addChild( background );
			
			view = MovieClipFactory.CreateIntroAnimation();
			
			addChild( view );
			
			Debug.Log("intro anim num children: " + view.numChildren);
			
			cam = view.getChildByName<MovieClip>( "mc_cam" );
			Debug.Log("cam == null: " + (cam == null));
			cam.gotoAndStop(1);
			
			henry = view.getChildByName<MovieClip>( "mc_henry" );
			henry.gotoAndStop(1);
			
			plattie = view.getChildByName<MovieClip>( "mc_platt" );
			plattie.gotoAndStop(1);
						
			scaleView();
			
			view.addEventListener( MouseEvent.CLICK, onViewClick );
			background.addEventListener( MouseEvent.CLICK, onViewClick );
			
			addHenryFrameScripts();
			addCamFrameScripts();
			addPlattieFrameScripts();
			view.addFrameScript(85, triggerAudio1);
			view.addFrameScript(255, triggerAudio2);
			view.addFrameScript(475, playOutro);
		}

		private void triggerAudio1( CEvent e ) {
			SoundEngine.SoundBundle introAudio = new SoundEngine.SoundBundle();
			introClip1 = (AudioClip)Resources.Load("Audio/splashScreen1", typeof(AudioClip));
			introAudio.AddClip( introClip1 );
			SoundEngine.Instance.PlayBundle( introAudio );
		}

		private void triggerAudio2( CEvent e ) {
			SoundEngine.SoundBundle introAudio = new SoundEngine.SoundBundle();
			introClip2 = (AudioClip)Resources.Load("Audio/splashScreen2", typeof(AudioClip));
			introAudio.AddClip( introClip2 );
			SoundEngine.Instance.PlayBundle( introAudio );
		}

		private void scaleView()
		{
			
			float heightScale = (Mathf.Round(Screen.height) / Mathf.Round(MainUI.STAGE_HEIGHT));
			
			view.scaleX =  heightScale;
			view.scaleY =  heightScale;
			
			view.ScaleCentered( heightScale );
			
			view.x = Screen.width * 0.5f;
			view.y = Screen.height * 0.5f;
			
		}
		
		private void addHenryFrameScripts()
		{
			DebugConsole.Log ("Henry Frame Scripts");
			view.addFrameScript(Mathf.FloorToInt(250), playHenryHop);
			view.addFrameScript(Mathf.FloorToInt(278), playHenryLookRight);
			view.addFrameScript(Mathf.FloorToInt(300), playHenryLookLeft);
			view.addFrameScript(Mathf.FloorToInt(330), playHenryJump);
			view.addFrameScript(Mathf.FloorToInt(405), playHenryLookLeft);
		}
		
		private void addCamFrameScripts()
		{
			DebugConsole.Log ("Cam Frame Scripts");
			view.addFrameScript(262, playCamHop);
			view.addFrameScript(273, playCamThumbsUp);
			view.addFrameScript(368, playCamLookLeft);
		}
		
		private void addPlattieFrameScripts()
		{
			DebugConsole.Log ("Plattie Frame Scripts");
			view.addFrameScript(258, playPlattieHop);
			view.addFrameScript(296, playPlattieRest);
			view.addFrameScript(396, playPlattieLookRight);
		}
		
		//Camie Anims
		private void playCamHop( CEvent e )
		{
			DebugConsole.Log ("Cam Jump");
			cam.gotoAndPlay("hop");
		}
		
		private void playCamThumbsUp( CEvent e )
		{
			DebugConsole.Log ("Cam Thumbs up");
			cam.gotoAndPlay("thumbs up");
		}
		
		private void playCamLookLeft( CEvent e )
		{
			DebugConsole.Log ("Cam Look Left");
			cam.gotoAndPlay("look left");
		}
		
		//Plattie Anims
		private void playPlattieHop( CEvent e )
		{
			DebugConsole.Log ("Plattie Hop");
			plattie.gotoAndPlay("jump");
		}
		
		private void playPlattieRest( CEvent e )
		{
			DebugConsole.Log ("Plattie Resting");
			plattie.gotoAndPlay("resting");
		}
		
		private void playPlattieLookRight( CEvent e )
		{
			DebugConsole.Log ("Plattie Look Right");
			plattie.gotoAndPlay("look right");
		}
		
		
		//Henry Anims
		private void playHenryHop( CEvent e )
		{
			DebugConsole.Log ("Henry Hop");
			henry.gotoAndPlay("jump");
			cam.gotoAndPlay("hop");
			plattie.gotoAndPlay("jump");
		}
		
		private void playHenryJump( CEvent e )
		{
			DebugConsole.Log ("Henry Jump");
			henry.gotoAndPlay("jump");
		}
		
		private void playHenryLookRight( CEvent e )
		{
			DebugConsole.Log ("Henry Look Right");
			henry.gotoAndPlay("head turn right");
		}
		
		private void playHenryLookLeft( CEvent e )
		{
			DebugConsole.Log ("Henry Look Left");
			henry.gotoAndPlay("head turn left");
		}
				
		private void playOutro( CEvent e )
		{
			view.stop();
			henry.gotoAndPlay("jump");
			cam.gotoAndPlay("jump");
			plattie.gotoAndPlay("jump");
			fadeOut(0.5f);
		}
		
		private void fadeOut(float delay)
		{
			Tweener.addTween(view, Tweener.Hash( "alpha",0, "time",0.5f, "delay",delay, "transition",Tweener.TransitionType.easeInOutQuad ) );
			Tweener.addTween(this, Tweener.Hash( "x",this.x, "time",1.25f, "delay",0.5f ) ).OnComplete( completeIntro ); //delay before moving to Menu
		}
		
		private void onViewClick( CEvent e )
		{
			fadeOut(0f);

		}
		
		private void completeIntro()
		{
			SoundEngine.Instance.StopAll();
			SoundEngine.Instance.SendCancelToken();
			view.stopAll();
			henry.stopAll();
			cam.stopAll();
			plattie.stopAll();
			if(OnVideoCompleteInvoker != null) OnVideoCompleteInvoker();
		}
		
	}
	
}