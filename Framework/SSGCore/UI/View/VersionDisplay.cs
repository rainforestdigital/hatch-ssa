using System;
using UnityEngine;
using HatchFramework;

namespace SSGCore
{
	public class VersionDisplay : MonoBehaviour
	{
		private string versionOutput = "";
		private bool visible = true;
		protected GUIStyle style;
		
		protected float timeout = 0;
		public void OnGUI(){
			if(!visible) {
				return;
			}
			if(style == null){
				style = new GUIStyle(GUI.skin.label);
				style.alignment = TextAnchor.MiddleRight;
			}
			Rect labelRect = new Rect(Screen.width-310, 0, 300, 30);
			GUI.Label(labelRect, versionOutput, style);
		}
		
		public void SetVisible(bool visible)
		{
			this.visible = visible;
		}
		
		public void UpdateText(DBObject db) {
			string bc = " " + GlobalConfig.GetProperty<string>("buildConfig");
			if(bc == " Release") bc = "";
			versionOutput = db.GetDeviceAlias() + " " + VersionControl.CurrentVersion + bc;
		}
	}
}



