using UnityEngine;
using System.Collections;

using pumpkin;
using pumpkin.display;

namespace SSGCore
{
	/// <summary>
	/// Editor camera overlay. -  Base class for displaying content inside any scene with assets locally (for local testing before assetloader is setup)
	/// </summary>
	public class EditorCameraOverlay : MonoBehaviour
	{
		
		protected DisplayObject movieClip;
		protected virtual void Start()
		{
			LoadMC();
			MovieClipOverlayCameraBehaviour.instance.stage.addChild( movieClip );
		}
		
		/// <summary>
		/// Load the movieclip that you want to be added to the stage
		/// </summary>
		protected virtual void LoadMC(){
		
			movieClip = new MovieClip();

		}

	}

}