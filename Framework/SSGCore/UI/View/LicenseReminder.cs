using System;
using UnityEngine;
using pumpkin.display;
using pumpkin.events;
using pumpkin.text;
using HatchFramework;
using System.Collections.Generic;

namespace SSGCore
{
	public class LicenseReminder : MovieClip
	{
		public Action onClose;
		
		public LicenseReminder( int daysLeft, Dictionary<string, string> information ) : base( "UI/licenseReminder.swf", "Sub_Reminder" )
		{
			//set frame
			if( daysLeft < 1 )
				gotoAndStop( 3 );
			else if( daysLeft == 1 )
				gotoAndStop( 2 );
			else
			{
				gotoAndStop( 1 );
				//more than one day remaining: show how many
				getChildByName<TextField>("days_txt").text = daysLeft.ToString();
			}
			
			//set universal info fields
			getChildByName<TextField>("device_txt").text = information["name"];
			getChildByName<TextField>("license_txt").text = information["license"];
			getChildByName<TextField>("orginization_txt").text = information["orginization"];
			getChildByName<TextField>("sales_txt").text = information["sale"];
			getChildByName<TextField>("expiration_txt").text = information["dateString"];
			
			//resize
			float s = (Screen.width * 0.75f) / this.width;
			this.y = (Screen.height - (this.height * s)) / 2f;
			this.x = (Screen.width - (this.width * s)) / 2f;
			this.scaleX = this.scaleY = s;
			
			//set close listener
			//hide the button if the license expired and it's iOS
			if( daysLeft > 0 || ( PlatformUtils.GetPlatform() != Platforms.IOS && PlatformUtils.GetPlatform() != Platforms.IOSRETINA ) )
				getChildByName<MovieClip>("close_mc").addEventListener( MouseEvent.CLICK, closeHit );
			else
				getChildByName<MovieClip>("close_mc").alpha = 0;
		}
		
		private void closeHit( CEvent e )
		{
			getChildByName<MovieClip>("close_mc").removeAllEventListeners( MouseEvent.CLICK );
			this.parent.removeChild( this );
			
			if( onClose != null )
				onClose();
		}
	}
}
