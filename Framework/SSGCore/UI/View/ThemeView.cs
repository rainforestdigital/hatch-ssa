using System;
using UnityEngine;
using pumpkin.display;
using pumpkin.tweener;

namespace SSGCore
{
	public class ThemeView : MovieClip
	{		
		protected MovieClip root;
		public MovieClip shadow_mc;
		public MovieClip tutorialBG_mc;
		public MovieClip mc_overlay;
		public MovieClip mc_overlay2;

		public string hostName;
					
		protected SessionInfo currentSession;
		
		protected bool deserialized;
		
		public ThemeView ():base()
		{
		}
		
		public ThemeView(string swf, string symbol):base(swf, symbol){
		}
		
		public virtual MovieClip GetHostAnchor(){
		
			return null;
		}
		
		public virtual void Init()
		{
			root = this;	
			shadow_mc = getChildByName<MovieClip>( "tutorialBG_mc" );
			shadow_mc.visible = false;
			
			tutorialBG_mc = getChildByName<MovieClip>( "tutorialBG_mc" );
			tutorialBG_mc.visible = false; 	
			
			mc_overlay = getChildByName<MovieClip>( "overlay_mc" );
			mc_overlay.visible = false;

			mc_overlay2 = getChildByName<MovieClip> ("overlay_mc2");
			if (mc_overlay2 != null) 
			{
				DebugConsole.Log ("MC OVERLAY 2 IS NOT NULL");
				mc_overlay2.visible = false;
			}

			deserialized = false;
		}
		
		public virtual void Resize( int width, int height )
		{
			
		}
		
		public virtual void CorrectOpportunity(bool correct)
		{
			DebugConsole.Log("Opportunity Correct: {0}", correct);
		}
		
		public void SetSession( SessionInfo  info )
		{
			currentSession = info;
			
			if( !String.IsNullOrEmpty( currentSession.themeData ) && !deserialized )
			{
				Deserialize( currentSession.themeData );
				deserialized = true;
			}
		}
		
		public void ShowOverlay(){
			
			
			if(mc_overlay == null)
			{
				DebugConsole.LogError("ShowOverlay || mc_overlay is NULL");
				return;
			}
			Debug.LogError ("SHOWING OVERLAY");
			mc_overlay.visible = true;
			mc_overlay.alpha = 0.0f;
			Tweener.addTween(mc_overlay, Tweener.Hash("time", 0.35, "alpha", 1.0, "transition", Tweener.TransitionType.easeOutQuad)).OnComplete(HandleFadeInComplete);
		}
		
		private void HandleFadeInComplete()
		{
//			Debug.LogError("OVERLAY FADE IN COMPLETE");
		}
		
		//hide overlay MC
		public void HideOverlay(){ 
			Debug.Log ("HIDING OVERLAY");
			
			if(mc_overlay == null)
			{
				DebugConsole.LogError("HideOverlay || mc_overlay is NULL");
			}
			else Tweener.addTween(mc_overlay, Tweener.Hash("time", 0.35, "alpha", 0, "transition", Tweener.TransitionType.easeOutQuad));
			
			if (mc_overlay2 != null && mc_overlay2.alpha > 0) 
			{
				mc_overlay2.alpha = 0;
				mc_overlay2.visible = false;
			}

		}
		
		
		public void ShowTutorialBG(){
			Debug.Log ("SHOWING TUT BG");
			
			if(shadow_mc == null)
			{
				DebugConsole.LogError("ShowTutorialBG || shadow_mc is NULL");
				return;
			}
			
			shadow_mc.visible = true;
			shadow_mc.alpha = 0.0f;
			Tweener.addTween(shadow_mc, Tweener.Hash("time", 0.35, "alpha", 1.0, "transition", Tweener.TransitionType.easeOutQuad));
		}
		
		public void HideTutorialBG(){
			Debug.Log("HIDING TUT BG");
			
			if(shadow_mc == null)
			{
				DebugConsole.LogError("HideTutorialBG || shadow_mc is NULL");
				return;
			}
			
			Tweener.addTween(shadow_mc, Tweener.Hash("time", 0.35, "alpha", 0, "transition", Tweener.TransitionType.easeOutQuad));
		}
		
		public virtual void CompleteSession()
		{
			
			if(shadow_mc == null)
			{
				DebugConsole.LogError("CompleteSession || shadow_mc is NULL");
				return;
			}
			
			shadow_mc.visible = false;
		}
		
		public virtual void ResetTheme()
		{
		}
		
		public virtual void Unload()
		{
		}
		
		public virtual string Serialize()
		{
			return "Serialization Method has not been overridden";
		}
		
		public virtual string PullBadges()
		{
			return "PullBadges Method has not been overridden";
		}
		
		public virtual void Deserialize( string data )
		{
		}
	}
}

