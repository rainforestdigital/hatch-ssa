using System;
using UnityEngine;
using pumpkin;
using pumpkin.display;

namespace SSGCore
{
	public class ViewController
	{
		public DisplayObjectContainer view;
		
		public ViewController ()
		{
		}
		
		public ViewController(DisplayObjectContainer view){
			this.view = view;	
		}
	}
}

