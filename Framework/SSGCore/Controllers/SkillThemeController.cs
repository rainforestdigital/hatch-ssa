using System;
using UnityEngine;
using System.Collections.Generic;

using HatchFramework;

using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;

using LitJson;

namespace SSGCore
{
	public class SkillThemeController : DisplayObjectContainer
	{
		private BaseSkill skillParent;
		public int CorrectOpps;
		public int FailedOpps;
		public int animsSeen;
		public int currentBG;
		private Base3dCharacter characterAnim;
		private Base3dCharacter lunaAnim;
		private MovieClip background;
		private MovieClip animBG;
		private List<GameObject> loadedAnimations;
		private TimerObject delayTime;
		private List<int> compUsed;
		private List<int> retryUsed;
		private Sprite flatAnim;
		private MovieClip compAnim;
		private Rect charRect = new Rect( -1f/2f, 2.5f/16f, 1f, 1f );
		public MovieClip Background {
			get {
				return background;
			}
		}
		public SkillThemeController ( BaseSkill parentSkill )
		{
			skillParent = parentSkill;
			
			CorrectOpps = FailedOpps = animsSeen = 0;
			
			skillParent.addEventListener( SkillEvent.COMPLIMENT_REQUEST, PlayCompliment );
			skillParent.addEventListener( SkillEvent.CRITICISM_REQUEST, PlayCriticism );
			skillParent.addEventListener( SkillEvent.END_THEME_REQUEST, CompleteSession );
			skillParent.addEventListener( SkillEvent.ANIMATION_REQUEST, PlayAnimation );
			skillParent.addEventListener( SkillEvent.TIMER_COMPLETE, PlayWaitingAnimation );
			
			string hostString = skillParent.Host + "/" + skillParent.Host;
			
			loadedAnimations = new List<GameObject>();
			loadedAnimations.Add( (GameObject)Resources.Load( "Characters/" + hostString + "01" ) );
			loadedAnimations.Add( (GameObject)Resources.Load( "Characters/" + hostString + "02" ) );
			loadedAnimations.Add( (GameObject)Resources.Load( "Characters/" + hostString + "03" ) );
			loadedAnimations.Add( (GameObject)Resources.Load( "Characters/" + hostString + "04" ) );
			loadedAnimations.Add( (GameObject)Resources.Load( "Characters/" + hostString + "05" ) );
			loadedAnimations.Add( (GameObject)Resources.Load( "Characters/" + hostString + "06" ) );
			loadedAnimations.Add( (GameObject)Resources.Load( "Characters/" + hostString + "06a" ) );
			loadedAnimations.Add( (GameObject)Resources.Load( "Characters/" + hostString + "07" ) );
			loadedAnimations.Add( (GameObject)Resources.Load( "Characters/Luna/Luna01a" ) );
			loadedAnimations.Add( (GameObject)Resources.Load( "Characters/Luna/Luna02" ) );
			loadedAnimations.Add( (GameObject)Resources.Load( "Characters/Luna/Luna01c" ) );
			
			switch( skillParent.Host )
			{
			case "Cami":
				compliment_names = CAMI_COMPLIMENT;
				retry_names = CAMI_RETRY;
				break;
			
			case "Platty":
				compliment_names = PLATTY_COMPLIMENT;
				retry_names = PLATTY_RETRY;
				break;
			
			case "Henry":
			default:
				compliment_names = HENRY_COMPLIMENT;
				retry_names = HENRY_RETRY;
				charRect.x = 0;
				break;
			}
			
			compUsed = new List<int>();
			retryUsed = new List<int>();
			
			currentBG = 1;
		}

		public void Dispose()
		{
			SoundEngine.Instance.OnAudioWordEvent -= AudioWordIn;
			
			skillParent.removeEventListener( SkillEvent.COMPLIMENT_REQUEST, PlayCompliment );
			skillParent.removeEventListener( SkillEvent.CRITICISM_REQUEST, PlayCriticism );
			skillParent.removeEventListener( SkillEvent.END_THEME_REQUEST, CompleteSession );
			skillParent.removeEventListener( SkillEvent.ANIMATION_REQUEST, PlayAnimation );	
			skillParent.removeEventListener( SkillEvent.TIMER_COMPLETE, PlayWaitingAnimation );
			
			skillParent = null;

			FXController.instance.Dispose ();
			
			if( background != null )
				Tweener.removeTweens( background );
			if( animBG != null )
				Tweener.removeTweens( animBG );
			if( flatAnim != null && flatAnim.parent != null )
				flatAnim.parent.removeChild( flatAnim );
			if( compAnim != null )
				Tweener.removeTweens( compAnim );
			
			if( characterAnim != null )
			{
				Tweener.removeTweens( characterAnim );
				characterAnim.Dispose();
			}
			
			if( lunaAnim != null )
				lunaAnim.Dispose();
			
			if( loadedAnimations != null )				
				loadedAnimations.Clear();

			stopTimer();
			
			SoundEngine.Instance.StopAll();
		}
		
		public void Init()
		{
			FXController.instance.Start (this);

			background = MovieClipFactory.GetSkillMC( skillParent.skillName, skillParent.BACKGROUND );
			background.gotoAndStop( BGFrame );
//			background = cleanEdge( background );

			DebugConsole.LogError("background object is null? {0}", (background == null));

			currentBG = background.currentFrame;
			
			addChild( background );
			Resize( Screen.width, Screen.height );
			
			if(!GlobalConfig.GetProperty<bool>("MuteThemeSong")){
				AudioClip clip = SoundUtils.LoadGlobalSound( skillParent.skillName, "Background-Music" );
				SoundEngine.Instance.PlayTheme(clip);
			}

			if( !skillParent.CurrentSession.skipIntro)
			{
				characterAnim = new Base3dCharacter( loadedAnimations[6] );
				characterAnim.camSize = 17.5f;
				characterAnim.camTarg = charRect;
				
				lunaAnim = new Base3dCharacter( loadedAnimations[8], new Vector3(0, 250, 0), 10 );
				lunaAnim.camSize = 25f;
				lunaAnim.addEventListener( CEvent.COMPLETE, lunaIntroHover );
				FXController.instance.EnableLunaGlow (lunaAnim);

				addChildOverSkill( new DisplayObject[] { characterAnim, lunaAnim } );
			}
		}

		public void showTutorialBack () {}
		
		public void hideTutorialBack () {}
		
		public void showOverlay () {}
		
		public void hideOverlay () {}

		private void lunaIntroHover( CEvent e )
		{
			FXController.instance.Dispose();
			lunaAnim.Dispose();
			lunaAnim = new Base3dCharacter( loadedAnimations[9], new Vector3(0, 250, 0), 10 );
			lunaAnim.loops = true;
			lunaAnim.camSize = 25f;

			FXController.instance.EnableLunaGlow (lunaAnim);
			FXController.instance.EnableLunaParticles (lunaAnim);
			MovieClip mc = getArtifactMC (false);
			FXController.instance.EnableLunaArtifactOrbit(
				//MovieClipFactory.GetSkillMC( skillParent.skillName, skillParent.ARTIFACT ), 
				mc,
				lunaAnim, 
				skillParent.Level);
			FXController.instance.Start(this);
			
			addChildOverSkill( lunaAnim );
			
			SoundEngine.Instance.OnAudioWordEvent += AudioWordIn;
		}
		
		private int BGFrame {
			get{
				int total = CorrectOpps + FailedOpps;
				int animNum = 0;
				if( skillParent.CurrentSession.totalOpportunities != 0 )
					animNum = Mathf.FloorToInt( ( (float)total / (float)skillParent.CurrentSession.totalOpportunities ) * 5 );
				
				while( animsSeen + 1 < animNum )
					animsSeen++;
				while( animsSeen > animNum )
					animsSeen--;
				
				int val = total + animsSeen + 1;
				
				DebugConsole.Log( "Getting Background Frame; total: " + total + ", animNum: " + animNum + ", animsSeen: " + animsSeen + ", val: " + val + ", currentBG: " + currentBG );
				
				if( total > 0 && total == skillParent.CurrentSession.totalOpportunities && animNum == animsSeen )
				{
					val = artifactFrame;
					DebugConsole.Log( "returning artifact screen - " + val );
				}
				else if( animNum == animsSeen )
				{
					while( total >= oppFrames.Length ) total -= oppFrames.Length;
					val = oppFrames[ total ];
					DebugConsole.Log( "returning opp screen - " + val );
				}
				else
				{
					val = transFrames[ animsSeen ];
					DebugConsole.Log( "returning transition screen - " + val );
				}
				
				return val;
			}
		}
		
		private void BuildNextBG()
		{

			Debug.LogWarning("skillParent.BACKGROUND: " + skillParent.BACKGROUND);

			animBG = MovieClipFactory.GetSkillMC( skillParent.skillName, skillParent.BACKGROUND );
			
			animBG.scaleX = animBG.scaleY = background.scaleX;
			animBG.y = background.y;
			animBG.x = Screen.width + animBG.x;
			
			animBG.gotoAndStop( BGFrame );
//			animBG = cleanEdge( animBG );
			
			currentBG = animBG.currentFrame;
			
			addChild( animBG );
		}
		
		private void SwapBG()
		{
			removeChild( background );
			background = animBG;
			animBG = null;
		}
		
		public void Pause()
		{
			if( delayTime != null )
			{
				delayTime.PauseTimer();
				timerPaused = true;
			}
			
			if( characterAnim != null )
				characterAnim.Pause();
			
			if( lunaAnim != null )
				lunaAnim.Pause();
		}
		private bool timerPaused = false;
		public void Resume()
		{
			if( timerPaused )
			{
				delayTime.ResumeTimer();
				timerPaused = false;
			}
			
			if( characterAnim != null )
				characterAnim.Resume();
			
			if( lunaAnim != null )
				lunaAnim.Resume();
		}
		
		private void MoveBGs( Action targ )
		{
			float xTarg = background.x;
			Tweener.addTween( background, Tweener.Hash( "x", Screen.width * -1, "time", 0.5f ) );
			Tweener.addTween( animBG, Tweener.Hash( "x", xTarg, "time", 0.5f ) );
			delayTime = TimerUtils.SetTimeout( 1.5f, targ );
		}
		
		public void correctOpportunity ( bool correct )
		{
			if( correct )
				CorrectOpps++;
			else
				FailedOpps++;
			
			BuildNextBG();
			
			Action targ;
			
			float progress = ( (float)( CorrectOpps + FailedOpps ) / (float)skillParent.CurrentSession.totalOpportunities );
			
			if( progress * 5 >= ( animsSeen ) + 1 )
			{
				targ = AnimStart;
				animsSeen++;
			}
			else targ = BackToSkill;
			
			MoveBGs( targ );
		}
		
		private void AnimStart()
		{
			stopTimer();
			
			SwapBG();
			
			delayTime = TimerUtils.SetTimeout( 0.5f, AnimTrigger );
		}
			
		private void AnimTrigger()
		{
			characterAnim = new Base3dCharacter( loadedAnimations[ animsSeen - 1 ] );
			characterAnim.addEventListener( CEvent.COMPLETE, AnimEnd );
			characterAnim.camSize = 17.5f;
			characterAnim.camTarg = charRect;
			addChildOverSkill( characterAnim );
		}
		
		private void AnimEnd( CEvent e )
		{
			FXController.instance.Dispose();
	
			characterAnim.Dispose();
			characterAnim = null;
			
			BuildNextBG();
			MoveBGs( BackToSkill );
		}
			
		private void BackToSkill()
		{
			stopTimer();
			
			SwapBG();
			dispatchEvent(new ThemeEvent(ThemeEvent.COMPLETE, true, false));
		}
		
		public void playIntro ()
		{
			TimerUtils.SetTimeout( 0.5f, delayedIntro );
		}
		
		private void delayedIntro()
		{
			skillParent.OnAudioWordComplete("THEME_INTRO");
		}
		
		public void reset ()
		{
			//unnessesary?
		}
		
		private void buildFlatAnim()
		{
			ClearFlatAnim();
			
			flatAnim = new Sprite();
			
			flatAnim.scaleX = flatAnim.scaleY = Screen.width / 2048f;
			flatAnim.y = (Screen.height - (1534f * flatAnim.scaleY)) / 2f;
			
			skillParent.addChild( flatAnim );
			
			
			compAnim = MovieClipFactory.CreateCharacterPopIn( skillParent.Host );
			
			compAnim.x = compAnim.width * -1f;
			compAnim.y = 800f;
			
			flatAnim.addChild(compAnim);
		}
		
		public void PlayCompliment ( CEvent e )
		{
			buildFlatAnim();
			Tweener.addTween( compAnim, Tweener.Hash( "time", 0.25f, "x", 0, "y", 554 ) ).OnComplete( ComplimentAudio );
		}
		
		private void ComplimentAudio()
		{
			if( compUsed.Count >= compliment_names.Length )
				compUsed = new List<int>();
			int i;
			do{
				i = UnityEngine.Random.Range( 0, compliment_names.Length );
				if( i == compliment_names.Length ) i--;
			}while( compUsed.Contains( i ) );
			
			compUsed.Add( i );
			
			//send audio clip with word
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( (skillParent.CurrentSession.SPN_FLAG ? "SPN-" : "") + "Narration/Character Complement Audio/" + skillParent.Host + "-Complements/" + compliment_names[i] );
			
			//if clip is broken:
			if( clip == null )
				skillParent.OnAudioWordComplete("THEME_COMPLIMENT");
			else
			{
				bundle.AddClip( clip, "THEME_COMPLIMENT", clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
				SoundEngine.Instance.OnAudioWordEvent += AudioWordIn;
			}
			
			AddAudioSessionData("comp", i);
		}
		
		public void PlayCriticism ( CEvent e )
		{
			buildFlatAnim();
			Tweener.addTween( compAnim, Tweener.Hash( "time", 0.25f, "x", 0, "y", 554 ) ).OnComplete( CriticismAudio );
		}
		
		private void CriticismAudio()
		{
			if( retryUsed.Count >= retry_names.Length )
				retryUsed = new List<int>();
			int i;
			do{
				i = UnityEngine.Random.Range( 0, retry_names.Length );
				if( i == retry_names.Length ) i--;
			}while( retryUsed.Contains( i ) );
			
			retryUsed.Add( i );
			
			//send audio clip with word
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( (skillParent.CurrentSession.SPN_FLAG ? "SPN-" : "") + "Narration/Character Retry Audio/" + skillParent.Host + "-Retry/" + retry_names[i] );
			
			//if clip is broken:
			if( clip == null )
				skillParent.OnAudioWordComplete("THEME_CRITICISM");
			else
			{
				bundle.AddClip( clip, "THEME_CRITICISM", clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
				SoundEngine.Instance.OnAudioWordEvent += AudioWordIn;
			}
			
			AddAudioSessionData("rtry", i);
		}
		
		public void AddAudioSessionData( String type, int id )
		{
			double time = skillParent.CurrentSession.intervalTime;
			List<SerialOppMap> refNode = skillParent.CurrentSession.currentSerializedSession.opps;
			SerialOppMap oppNode = refNode[ refNode.Count - 1 ];
			time = time - oppNode.start;
			
			SerialActionMap insert = new SerialActionMap();
			insert.time = (double)(Mathf.FloorToInt(((float)time)*100)/100f);
			insert.type = type;
			insert.audio = id;
			
			skillParent.CurrentSession.currentSerializedSession.opps[refNode.Count - 1].actions.Add( insert );
		}
		
		private void ClearFlatAnim()
		{
			if( flatAnim != null )
			{
				flatAnim.parent.removeChild( flatAnim );
				flatAnim.removeChild( compAnim );
			}
			if( compAnim != null )
				Tweener.removeTweens( compAnim );
			
			flatAnim = null;
			compAnim = null;
		}
		
		public void CompleteSession ( CEvent e )
		{
			float percent = CorrectOpps / (float)(CorrectOpps + FailedOpps);
			Rect endRect = new Rect( charRect );
			endRect.x -= 1f/6f;

			bool enableBeam = false;

			dispatchEvent(new ThemeEvent(ThemeEvent.SESSION_COMPLETE, true, false));

			DebugConsole.Log( "Correct Count: " + CorrectOpps + ", Wrong Count: " + FailedOpps + ", is " + (percent * 100) + "%" );
			if( percent >= 0.8f )
			{
				background.addChild ( getArtifactMC(true) );
				
				characterAnim = new Base3dCharacter( loadedAnimations[5] );
				characterAnim.camSize = 17.5f;
				characterAnim.camTarg = endRect;
				
				lunaAnim = new Base3dCharacter( loadedAnimations[8], new Vector3(0, 250, 0), 10 );
				lunaAnim.camSize = 25f;
				lunaAnim.addEventListener( CEvent.COMPLETE, lunaFinaleStart );
			}
			else
			{
				characterAnim = new Base3dCharacter( loadedAnimations[7] );
				characterAnim.camSize = 17.5f;
				characterAnim.camTarg = endRect;
				
				lunaAnim = new Base3dCharacter( loadedAnimations[9], new Vector3(0, 250, 0), 10 );
				lunaAnim.loops = true;
				lunaAnim.camSize = 25f;

				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				AudioClip clip = SoundUtils.LoadGlobalSound( (skillParent.CurrentSession.SPN_FLAG ? "SPN-" : "") + "Narration/Luna Universal/User-Misses-Artifact" );
				bundle.AddClip( clip, "Artifact-Missed", clip.length );
			
				SoundEngine.Instance.OnAudioWordEvent += AudioWordIn;
				SoundEngine.Instance.PlayBundle( bundle );

				enableBeam = true;
			}
			
			FXController.instance.Dispose();
			FXController.instance.EnableLunaGlow (lunaAnim);
			if (enableBeam) {
				FXController.instance.EnableLunaArtifactBeamAndOrbit (
					//MovieClipFactory.GetSkillMC (skillParent.skillName, skillParent.ARTIFACT), 
					getArtifactMC (false),
					new Vector2 (Screen.width * 0.5f, Screen.height * 0.5f), 
					lunaAnim, 
					2f,
					skillParent.Level);
			}
			FXController.instance.Start(this);
			addChildOverSkill( new DisplayObject[] { characterAnim, lunaAnim } );
		}

		private void lunaFinaleStart ( CEvent e )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;
			
			if( skillParent.Level != SkillLevel.Completed )
				clip = SoundUtils.LoadGlobalSound( skillParent.skillName, "User-Earns-Artifact" );
			else
				clip = SoundUtils.LoadGlobalSound( (skillParent.CurrentSession.SPN_FLAG ? "SPN-" : "") + "Narration/Luna Universal/User-Gets-CD-Star" );
			
			bundle.AddClip( clip, "Artifact-Rewarded", clip.length );
			
			SoundEngine.Instance.OnAudioWordEvent += AudioWordIn;
			SoundEngine.Instance.PlayBundle( bundle );

			FXController.instance.Dispose();
			lunaAnim.Dispose();
			lunaAnim = new Base3dCharacter( loadedAnimations[9], new Vector3(0, 250, 0), 10 );
			lunaAnim.loops = true;
			lunaAnim.camSize = 25f;
			FXController.instance.EnableLunaGlow (lunaAnim);
			FXController.instance.EnableLunaParticles (lunaAnim);
			FXController.instance.Start(this);
			addChildOverSkill( lunaAnim );
		}
		
		private void AudioWordIn ( string word )
		{
			switch( word )
			{
			case "THEME_COMPLIMENT":
			case "THEME_CRITICISM":
				Tweener.addTween( flatAnim, Tweener.Hash( "time", 0.25f, "x", (flatAnim.width) * -1f, "y", 800 ) ).OnComplete( ClearFlatAnim );
				SoundEngine.Instance.OnAudioWordEvent -= AudioWordIn;
				break;
				
			case "INTRO":
			case "INITIAL_INSTRUCTION":
				FXController.instance.DisconnectArtifactOrbit();
				
				Tweener.addTween( characterAnim, Tweener.Hash( "time", 0.25f, "alpha", 0f ) ).OnComplete( () => { characterAnim.Dispose(); characterAnim = null; } );

				lunaAnim.Dispose();
				lunaAnim = new Base3dCharacter( loadedAnimations[10], new Vector3(0, 250, 0), 10 );
				lunaAnim.camSize = 25f;
				lunaAnim.addEventListener( CEvent.COMPLETE, ( CEvent e ) => { lunaAnim.Dispose(); lunaAnim = null; } );

				FXController.instance.Dispose();

				FXController.instance.MoveArtifactOrbitToNewLuna(lunaAnim);
				FXController.instance.DoParticleOutAnim();

				FXController.instance.EnableLunaGlow (lunaAnim);
				FXController.instance.Start(this);

				addChildOverSkill( lunaAnim );
				SoundEngine.Instance.OnAudioWordEvent -= AudioWordIn;
				break;
				
			case "Artifact-Rewarded":
			case "Artifact-Missed":
				lunaAnim.Dispose();
				lunaAnim = new Base3dCharacter( loadedAnimations[10], new Vector3(0, 250, 0), 10 );
				lunaAnim.camSize = 25f;
				lunaAnim.addEventListener( CEvent.COMPLETE, FinishSession );

				FXController.instance.Dispose();

				FXController.instance.MoveArtifactOrbitToNewLuna(lunaAnim);
				FXController.instance.DoParticleOutAnim();

				FXController.instance.EnableLunaGlow (lunaAnim);
				FXController.instance.Start(this);
				addChildOverSkill( lunaAnim );
			
				SoundEngine.Instance.OnAudioWordEvent -= AudioWordIn;
				break;
				
			default:
				break;
			}
		}

		private MovieClip getArtifactMC(bool positionOnBackground)
		{
			MovieClip mc = MovieClipFactory.GetSkillMC( skillParent.skillName, skillParent.ARTIFACT );
			int frame;
			switch( skillParent.Level ){
			case SkillLevel.Developing:
				frame = 2;
				break;
			case SkillLevel.Developed:
				frame = 3;
				break;
			case SkillLevel.Completed:
				frame = 4;
				break;
			default:
				frame = 1;
				break;
			}
			mc.gotoAndStop( frame );
			
			Rect offset = mc.getBounds( mc ).rect;
			float targWidth = ( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA ) ? 200f : 75f;
			float targScale = targWidth / ((mc.width + mc.height) / 2f);
			float targHeight = targScale * offset.height;
			if (positionOnBackground) {
				mc.x = (((Screen.width / background.scaleX) - (targScale * offset.width)) * 0.5f) - (offset.x * targScale);
				mc.y = (((((Screen.width * 3) / 4) / background.scaleY) - targHeight) * 0.5f) - (offset.y * targScale);
				mc.scaleX = mc.scaleY = targScale;
			} else {
				mc.scaleX = mc.scaleY = targScale * background.scaleX;
				mc.x -= offset.x * mc.scaleX;
				mc.y -= offset.y * mc.scaleY;
			}
			return mc;
		}

		private void AnimFinish ( CEvent e )
		{
			FinishSession( e );
		}
		
		private void FinishSession ( CEvent e )
		{
			FXController.instance.Dispose ();

			if( characterAnim != null )
				characterAnim.DisposeImmediately ();
			if (lunaAnim != null)
				lunaAnim.DisposeImmediately ();
			
			dispatchEvent( new SkillEvent( SkillEvent.SKILL_COMPLETE, true, false ) );
		}
		
		public void PlayAnimation ( CEvent e ){}
		
		public void PlayWaitingAnimation ( CEvent e ){}
		
		private void stopTimer()
		{
			if( delayTime != null )
			{
				delayTime.StopTimer();
				delayTime = null;
			}
		}
		
		public void Resize ( float width, float height )
		{
			Rect clipping;
			if( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA )
				clipping = new Rect( -2f, -2f, 2048f, 1536f );
			else
				clipping = new Rect( -1f, -1f, 1024f, 768f );
			
			float scale = (width + 4f) / (clipping.width);
			background.x = -2f;
			background.y = ( (height + 4f) - ( (clipping.height) * scale ) ) / 2f;
			background.scaleX = background.scaleY = scale;
			PlatformUtils.BackgroundScale = scale;
		}
		
		private MovieClip cleanEdge( MovieClip mc )
		{
			MovieClip c;
			for( int i = 0; i < mc.numChildren; i++ )
			{
				c = mc.getChildAt<MovieClip>( i );
				if( c == null )
				{
					continue;
				}
				if( c.getBounds( mc ).x < 0 )
				{
					c.x -= c.getBounds(mc).x;
				}
			}
			
			return mc;
		}
		
		public string GetSessionData ()
		{
			string data = "";
			
			data += CorrectOpps + ";" + FailedOpps + ";" + animsSeen + ";" + currentBG;
			
			return data;
		}
		
		public void parseSessionData ( string raw )
		{
			string[] data = raw.Split( ';' );
			
			CorrectOpps = int.Parse( data[0] );
			FailedOpps = int.Parse( data[1] );
			animsSeen = int.Parse( data[2] );
			currentBG = int.Parse( data[3] );
		}

		private void addChildOverSkill( DisplayObject obj ) {
			addChildOverSkill (new DisplayObject[] { obj });
		}

		private void addChildOverSkill( DisplayObject[] objs )
		{
			if (skillParent.parent != null) {
				for (int i = 0; i < objs.Length; i++) {
					skillParent.parent.addChildAt (objs[i], skillParent.parent.getChildIndex (skillParent) + i);
				}
			}
			else
				delayTime = TimerUtils.SetTimeout( 0.05f, () => { deferedAddChildOverSkill(objs); } );
		}
		
		private void deferedAddChildOverSkill( DisplayObject[] objs )
		{
			delayTime.Unload();
			if(objs == null)
			{
				DebugConsole.LogError( "Failed to add obj to stage" );
				return;
			}
			
			if (skillParent.parent != null) {
				for (int i = 0; i < objs.Length; i++) {
					skillParent.parent.addChildAt (objs[i], skillParent.parent.getChildIndex (skillParent) + 1);
				}
			} else {
				delayTime = TimerUtils.SetTimeout (0.05f, () => {
					for (int i = 0; i < objs.Length; i++) {
						deferedAddChildOverSkill (objs);
					}
				});
			}
		}
		
		private void removeChildOverSkill( DisplayObject obj )
		{
			if( obj.parent != null )
				obj.parent.removeChild( obj );
		}
		
		public int[] oppFrames = new int[]{ 1,1,1,1,1,1,1,1,1,1 };//,2,4,5,7,8,10,11,13,14 };
		public int[] transFrames = new int[]{ 3,6,9,12,15 };
		public int artifactFrame = 16;
		
		private string[] compliment_names;
		private string[] retry_names;
		
		private string[] HENRY_COMPLIMENT = new string[]{
				"COMP-a-round-of-applause",
				"COMP-clap-your-hands-stomp-your-feet",
				"COMP-excellent",
				"COMP-fab-you-you-are-fabulous",
				"COMP-fabulous-effort",
				"COMP-fabulous-fabulous",
				"COMP-fantastic-you-are-at-the-top",
				"COMP-fantastic",
				"COMP-give-a-silent-cheer",
				"COMP-give-yourself-a-big-hug",
				"COMP-good-job",
				"COMP-great-job-will-you-be-my-friend",
				"COMP-hooray-hooray-you-made-my-day",
				"COMP-jump-high-for-joy",
				"COMP-kiss-your-brain",
				"COMP-knee-slappin",
				"COMP-look-at-you-you-are-the-best",
				"COMP-outstanding",
				"COMP-pat-yourself-on-the-back",
				"COMP-shake-shake-you-take-the-cake",
				"COMP-spirit-fingers",
				"COMP-super-job",
				"COMP-super-super",
				"COMP-superior",
				"COMP-three-cheers-for-you",
				"COMP-we-are-having-fun-you-are-my-partner",
				"COMP-wow-did-you-see-what-you-did-wow",
				"COMP-you-are-the-top-please-dont-stop",
				"COMP-you-did-it",
				"COMP-you-make-me-smile"
		};
		private string[] HENRY_RETRY = new string[]{
				"RTRY-give-it-another-try",
				"RTRY-i-want-to-help-you-try-again",
				"RTRY-lets-keep-going-one-more-try",
				"RTRY-lets-look-again",
				"RTRY-lets-try-again",
				"RTRY-think-one-more-time",
				"RTRY-try-try-again",
				"RTRY-you-were-close-lets-try-again"
		};
		
		private string[] CAMI_COMPLIMENT = new string[]{
				"COMP-a-round-of-applause",
				"COMP-clap-your-hands",
				"COMP-excellent",
				"COMP-fab-you-you-are-fabulous",
				"COMP-fabulous-effort",
				"COMP-fabulous-fabulous",
				"COMP-fantastic-you-are-at-the-top",
				"COMP-fantastic",
				"COMP-give-a-silent-cheer",
				"COMP-give-yourself-a-big-hug",
				"COMP-good-job",
				"COMP-great-job-will-you-be-my-friend",
				"COMP-hooray-hooray-you-made-my-day",
				"COMP-jump-high-for-joy",
				"COMP-kiss-your-brain",
				"COMP-knee-slappin-good-job",
				"COMP-look-at-you-you-are-the-best",
				"COMP-outstanding",
				"COMP-pat-your-self-on-the-back",
				"COMP-shake-shake-you-take-the-cake",
				"COMP-spirit-fingers",
				"COMP-super-job",
				"COMP-super-super",
				"COMP-superior",
				"COMP-three-cheers-for-you",
				"COMP-we-are-having-fun-you-are-my-partner",
				"COMP-wow-did-you-see-what-you-did-wow",
				"COMP-you-are-tops",
				"COMP-you-did-it",
				"COMP-you-make-me-smile"
		};
		private string[] CAMI_RETRY = new string[]{
				"RTRY-give-it-another-try",
				"RTRY-i-want-to-help-you-try-again",
				"RTRY-lets-keep-going-one-more-try",
				"RTRY-lets-look-again",
				"RTRY-lets-try-again",
				"RTRY-think-one-more-time",
				"RTRY-try-try-again",
				"RTRY-you-were-close-lets-try-again"
		};
		
		private string[] PLATTY_COMPLIMENT = new string[]{
				"COMP-a-round-of-applause",
				"COMP-big-hug",
				"COMP-clap-your-hands-stomp-your-feet",
				"COMP-excellent",
				"COMP-fab-you-you-are-fabulous",
				"COMP-fabulous-effort",
				"COMP-fabulous-fabulous",
				"COMP-fantastic-you-are-at-the-top",
				"COMP-fantastic",
				"COMP-give-a-silent-cheer",
				"COMP-good-job",
				"COMP-great-job-will-you-be-my-friend",
				"COMP-hooray-you-made-my-day",
				"COMP-jump-high-for-joy",
				"COMP-kiss-your-brain",
				"COMP-knee-slappin",
				"COMP-look-at-you-you-are-the-best",
				"COMP-outstanding",
				"COMP-pat-yourself-on-the-back",
				"COMP-shake-shake-you-take-the-cake",
				"COMP-spirit-fingers",
				"COMP-super-job",
				"COMP-super-super",
				"COMP-superior",
				"COMP-three-cheers-for-you",
				"COMP-we-are-having-fun-you-are-my-partner",
				"COMP-wow-did-you-see-what-you-did-wow",
				"COMP-you-are-tops-please-dont-stop",
				"COMP-you-did-it",
				"COMP-you-make-me-smile"
		};
		private string[] PLATTY_RETRY = new string[]{
				"RTRY-give-it-another-try",
				"RTRY-i-want-to-help-you-try-again",
				"RTRY-lets-keep-going-one-more-try",
				"RTRY-lets-look-again",
				"RTRY-lets-try-again",
				"RTRY-think-one-more-time",
				"RTRY-try-try-again",
				"RTRY-you-were-close-lets-try-again"
		};
	}
}
