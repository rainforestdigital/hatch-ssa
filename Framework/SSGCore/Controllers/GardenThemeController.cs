using System;
using pumpkin.display;
using UnityEngine;
using System.Collections.Generic;
using HatchFramework;
using pumpkin.events;

using System.Collections;
using pumpkin.swf;
using System.Linq;
using pumpkin.tweener;
using pumpkin.text;

namespace SSGCore
{
	public class GardenThemeController : ThemeController
	{
		public const string BASE_PATH = "TH-GN-";
		public const string PAYOFF50_PATH = "TH-GN-50Percent-Payoff-Audio";
		public const string PAYOFF100_PATH = "TH-GN-100Percent-Payoff-Audio";
		public const string END_PATH = "TH-GN-End-of-Game-Audio";
		public const string INTRO_PATH = "TH-GN-Introduction-Audio";
		public const string COMP_PATH = "-Complements";
		public const string RTRY_PATH = "-Retry";
		public const string THEME_SONG = "TH-GN-garden-theme-song";
		
		public const string BIRD = "TH-GN-bird-chirp";
		public const string CLONES = "TH-GN-clones";
		public const string FULL_GROWN = "TH-GN-full-grown";
		public const string HALF_GROWN = "TH-GN-half-grown";
		public const string SPARKLES = "TH-sparkles";
		public const string HOORAY = "TH-hip_hooray";
		
		public string INTRO;// = "TH-GN-Platty-Garden";
		
		
		//THERE ARE 3 OF EACH OF THESE
		public const string PAYOFF50 = "TH-GN-50-Payoff";
		public const string PAYOFF100 = "TH-GN-";
		public const string PAYOFF_BROCCOLI = "TH-GN-broccoli";
		public const string PAYOFF_CARROT = "TH-GN-carrot";
		public const string PAYOFF_CELERY = "TH-GN-celery";
		public const string PAYOFF_CORN = "TH-GN-corn";
		public const string PAYOFF_LETTUCE = "TH-GN-lettuce";
		public const string PAYOFF_ONION = "TH-GN-onion";
		public const string PAYOFF_PUMPKIN = "TH-GN-pumpkin";
		public const string PAYOFF_RADISH = "TH-GN-radish";
		public const string PAYOFF_SQUASH = "TH-GN-squash";
		public const string PAYOFF_TOMATO = "TH-GN-tomato";
		public const string END_BEGINNER = "TH-GN-beginner";
		public const string END_INTERMEDIATE = "TH-GN-intermediate";
		public const string END_MASTER = "TH-GN-master";
		
		public string[] RETRY;
		/*= {"RTRY-give-it-another-try",
								 "RTRY-i-want-to-help-you-try-again",
							 	 "RTRY-lets-keep-going-one-more-try",
							 	 "RTRY-lets-look-again",
								 "RTRY-lets-think-harder",
								 "RTRY-lets-try-again",
								 "RTRY-put-your-thinking-cap-on",
								 "RTRY-think-one-more-time",
								 "RTRY-try-try-again",
								 "RTRY-you-were-close-lets-try-again"};*/
		
		public string[] COMP;
			/* = {"COMP-a-round-of-applause",
								"COMP-big-hug",
								"COMP-clap-your-hands-stomp-your-feet",
								"COMP-excellent",
								"COMP-fab-you-you-are-fabulous",
								"COMP-fabulous-effort",
								"COMP-fabulous-fabulous",
								"COMP-fantastic",
								"COMP-fantastic-you-are-at-the-top",
								"COMP-give-a-silent-cheer",
								"COMP-good-job",
								"COMP-great-job-will-you-be-my-friend",
								"COMP-hooray-you-made-my-day",
								"COMP-jump-high-for-joy",
								"COMP-kiss-your-brain",
								"COMP-knee-slappin",
								"COMP-look-at-you-you-are-the-best",
								"COMP-outstanding",
								"COMP-pat-yourself-on-the-back",
								"COMP-shake-shake-you-take-the-cake",
								"COMP-spirit-fingers",
								"COMP-super-job",
								"COMP-super-super",
								"COMP-superior",
								"COMP-three-cheers-for-you",
								"COMP-we-are-having-fun-you-are-my-partner",
								"COMP-wow-did-you-see-what-you-did-wow",
								"COMP-you-are-tops-please-dont-stop",
								"COMP-you-did-it",
								"COMP-you-make-me-smile"};*/
		
		public GardenThemeController(Theme theme, GardenThemeView view):base(theme, view)
		{						
			//Debug.Log("host comp length: " + host.COMP.Length);
		}
		
		private void OnAudioWordComplete( string word )
		{
			switch(word)
			{
				case "ROAR":
				case "PEEK":
				case "THUMP":
				case "BIRD":
					host.PlayAnimation(BaseCharacter.RIGHT_TALKING);
				break;
				case "THEME_INTRO":
					host.PlayAnimation(BaseCharacter.RIGHT_TALKING_END);
					//themeView.ShowOverlay();
				break;
				case "PAYOFF_50":
				case "PAYOFF_100":
					host.PlayAnimation(BaseCharacter.RIGHT_TALKING_END);
				break;
				case "THEME_END":
					host.PlayAnimation(BaseCharacter.PAYOFF_END);
				break;
			}
		}
	
		public override void LoadHost ()
		{
			base.LoadHost();
			switch(currentSkill.Host)
			{
				case "Platty":
					BasePlatty plat = CharacterFactory.GetGardenPlattyCharacter();
					INTRO = "TH-GN-Platty-Garden";
					this.SetHost(plat);
				break;
				case "Henry":
					BaseHenry henry = CharacterFactory.GetGardenHenryCharacter();
					INTRO = "TH-GN-Henry-Garden";
					this.SetHost(henry);
				break;
				case "Cami":
					BaseCam cam = CharacterFactory.GetGardenCamCharacter();
					INTRO = "TH-GN-Cami-Garden";
					this.SetHost(cam);
				break;
			}
			
			COMP = host.COMP;
			RETRY = host.RETRY;
			
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;	
			
			themeView.hostName = host.GetCharName();
		}
		
		
		public override void SetHost (BaseCharacter _host)
		{
			if( host is BaseCam ) hostScale = 1.5f;
			base.SetHost (_host);
		
			if(hostAnchor == null) hostAnchor = themeView.GetHostAnchor();
			PositionHost(hostAnchor);
			
			int index = themeView.getChildIndex(themeView.getChildByName<MovieClip>("tutorialBG_mc"));
			themeView.setChildIndex(hostAnchor, index);
			
			// remove after setting position since the visual shadow is now with the character asset
			hostAnchor.visible = false;
		}
		
		public override void Unload ()
		{
			base.Unload ();
				view = null;
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
		}
		
		public override void PlayIntro()
		{
			base.PlayIntro();
			
			TimerUtils.SetTimeout( 1f, delayedIntro );
		}
		
		private void delayedIntro()
		{
			//DebugConsole.Log("PlayIntro HelpMode: " + themeView.sessionController.HelpMode);
			if(currentSkill.CurrentSession.helpMode)
			{
				currentSkill.OnAudioWordComplete("THEME_INTRO");
				return;
			}
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			//lion roar
			Debug.Log("audio: " + BIRD);
			AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.GARDEN_THEME, "Sound Effects/" + BIRD);
			bundle.AddClip( clip, "BIRD", clip.length );
			//intro speak
			
			string audioPath = BASE_PATH + host.GetCharName() + "/" + INTRO_PATH + "/" + INTRO;
			Debug.Log("audio path: " + audioPath);
			
			clip = SoundUtils.LoadGlobalSound(MovieClipFactory.GARDEN_THEME, "Narration/" + audioPath);
			
			if(clip == null)
			{
				DebugConsole.LogError("Intro audio is null: " + audioPath);
				currentSkill.OnAudioWordComplete("THEME_INTRO");
				return;
			}
			
			bundle.AddClip( clip, "THEME_INTRO", clip.length );
			SoundEngine.Instance.PlayBundle( bundle );
			//Send message to the main game to play game's intro	
		}
		
		public override void PlayCompliment(CEvent e ){
			//Need Audio
			base.PlayCompliment(e);
			 
			/*if(currentSkill.CurrentSession.helpMode)
			{
				currentSkill.OnAudioWordComplete("THEME_COMPLIMENT");
				return;
			}*/
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			
			string audioPath = BASE_PATH + host.GetCharName() + "/" + host.GetCharName() + COMP_PATH + "/" + COMP[UnityEngine.Random.Range(0,COMP.Length)];
			Debug.Log("audio path: " + audioPath);
			
			AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.GARDEN_THEME, "Narration/" + audioPath);
			
			if(clip == null)
			{
				DebugConsole.LogError("Compliment audio is null: " + audioPath);
				currentSkill.OnAudioWordComplete("THEME_COMPLIMENT");
				return;
			}
			
			bundle.AddClip( clip, "THEME_COMPLIMENT", clip.length );
			SoundEngine.Instance.PlayBundle( bundle );
			//Send message to the main game to play game's message
		}
		
		public override void PlayCriticism(CEvent e){
			//Need Audio
			base.PlayCriticism(e);
			
			/*if(currentSkill.CurrentSession.helpMode)
			{
				currentSkill.OnAudioWordComplete("THEME_CRITICISM");
				return;
			}*/
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			
			string audioPath = BASE_PATH + host.GetCharName() + "/" + host.GetCharName() + RTRY_PATH + "/" + RETRY[UnityEngine.Random.Range(0,RETRY.Length)];
			Debug.Log("audio path: " + audioPath);
			
			AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.GARDEN_THEME,  "Narration/" + audioPath);
			
			if(clip == null)
			{
				DebugConsole.LogError("Criticism audio is null: " + audioPath );
				currentSkill.OnAudioWordComplete("THEME_CRITICISM");
				return;
			}
			
			bundle.AddClip( clip, "THEME_CRITICISM", clip.length );
			SoundEngine.Instance.PlayBundle( bundle );
			//Send message to the main game to play game's message
		}		
	}
}


