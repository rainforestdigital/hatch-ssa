using System;
using pumpkin.display;
using UnityEngine;
using System.Collections.Generic;
using HatchFramework;
using pumpkin.events;

using System.Collections;
using pumpkin.swf;
using System.Linq;
using pumpkin.tweener;
using pumpkin.text;

namespace SSGCore
{
	public class TravelThemeController : ThemeController
	{
		public const string BASE_PATH = "Narration/TH-TR-";
		public const string PAYOFF50_PATH = "TH-TR-50Percent-Payoff-Audio";
		public const string PAYOFF100_PATH = "TH-TR-100Percent-Payoff-Audio";
		public const string END_PATH = "TH-TR-End-of-Game-Audio";
		public const string INTRO_PATH = "TH-TR-Introduction-Audio";
		public const string COMP_PATH = "-Complements";
		public const string RTRY_PATH = "-Retry";
		public const string THEME_SONG = "Sound Effects/TH-TR-theme-song";
		
		public const string CAMERA = "Sound Effects/TH-TR-camera-snap";
		public const string DRIVE = "Sound Effects/TH-TR-drive-by";
		public const string POP = "Sound Effects/TH-TR-pop";
		
		public string INTRO;// = "TH-TR-Platty-Travel";
		
		//THERE ARE 3 OF EACH OF THESE
		public const string PAYOFF50 = "TH-TR-50-Payoff";
		public const string PAYOFF100 = "TH-TR-";
		public const string PAYOFF_BUS_TOUR = "TH-TR-bus-tour";
		public const string PAYOFF_CAPITOL_BUILDING = "TH-TR-capitol-building";
		public const string PAYOFF_JEFFERSON_MEM = "TH-TR-jefferson-memorial";
		public const string PAYOFF_LOC = "TH-TR-library-of-congress";
		public const string PAYOFF_LINC_MEM = "TH-TR-lincoln-memorial";
		public const string PAYOFF_METRO = "TH-TR-metro";
		public const string PAYOFF_PENT = "TH-TR-pentagon";
		public const string PAYOFF_SMITHSONIAN = "TH-TR-smithsonian";
		public const string PAYOFF_WASH_MON = "TH-TR-washington-monument";
		public const string PAYOFF_WHITE_HOUSE = "TH-TR-white-house";
		public const string END_BEGINNER = "TH-TR-beginner";
		public const string END_INTERMEDIATE = "TH-TR-intermediate";
		public const string END_MASTER = "TH-TR-master";
		
		public string[] RETRY;
		/*= {"RTRY-give-it-another-try",
								 "RTRY-i-want-to-help-you-try-again",
							 	 "RTRY-lets-keep-going-one-more-try",
							 	 "RTRY-lets-look-again",
								 "RTRY-lets-think-harder",
								 "RTRY-lets-try-again",
								 "RTRY-put-your-thinking-cap-on",
								 "RTRY-think-one-more-time",
								 "RTRY-try-try-again",
								 "RTRY-you-were-close-lets-try-again"};*/
		
		public string[] COMP;
			/* = {"COMP-a-round-of-applause",
								"COMP-big-hug",
								"COMP-clap-your-hands-stomp-your-feet",
								"COMP-excellent",
								"COMP-fab-you-you-are-fabulous",
								"COMP-fabulous-effort",
								"COMP-fabulous-fabulous",
								"COMP-fantastic",
								"COMP-fantastic-you-are-at-the-top",
								"COMP-give-a-silent-cheer",
								"COMP-good-job",
								"COMP-great-job-will-you-be-my-friend",
								"COMP-hooray-you-made-my-day",
								"COMP-jump-high-for-joy",
								"COMP-kiss-your-brain",
								"COMP-knee-slappin",
								"COMP-look-at-you-you-are-the-best",
								"COMP-outstanding",
								"COMP-pat-yourself-on-the-back",
								"COMP-shake-shake-you-take-the-cake",
								"COMP-spirit-fingers",
								"COMP-super-job",
								"COMP-super-super",
								"COMP-superior",
								"COMP-three-cheers-for-you",
								"COMP-we-are-having-fun-you-are-my-partner",
								"COMP-wow-did-you-see-what-you-did-wow",
								"COMP-you-are-tops-please-dont-stop",
								"COMP-you-did-it",
								"COMP-you-make-me-smile"};*/
		
		public TravelThemeController(Theme theme, TravelThemeView view):base(theme, view)
		{						
			//Debug.Log("host comp length: " + host.COMP.Length);
		}
		
		private void OnAudioWordComplete( string word )
		{
			if(host == null) return;
			DebugConsole.Log("TRAVEL THEME CONTROLLER: WORD: " + word + ", host null? " + (host == null));
			switch(word)
			{
				case "CAMERA":
				case "DRIVE":
				case "POP":
					host.PlayAnimation(BaseCharacter.RIGHT_TALKING);
				break;
				case "THEME_INTRO":
					host.PlayAnimation(BaseCharacter.RIGHT_TALKING_END);
					//themeView.ShowOverlay();
				break;
				case "PAYOFF_50":
				case "PAYOFF_100":
					host.PlayAnimation(BaseCharacter.RIGHT_TALKING_END);
				break;
				case "THEME_END":
					host.PlayAnimation(BaseCharacter.PAYOFF_END);
				break;
			}
		}
	
		public override void LoadHost ()
		{
			base.LoadHost();
			switch(currentSkill.Host)
			{
				case "Platty":
					BasePlatty plat = CharacterFactory.GetTravelPlattyCharacter();
					INTRO = "TH-TR-Platty-Travel";
					this.SetHost(plat);
				break;
				case "Henry":
					BaseHenry henry = CharacterFactory.GetTravelHenryCharacter();
					INTRO = "TH-TR-Henry-Travel";
					this.SetHost(henry);
				break;
				case "Cami":
					BaseCam cam = CharacterFactory.GetTravelCamCharacter();
					INTRO = "TH-TR-Cami-Travel";
					this.SetHost(cam);
				break;
			}
			
			COMP = host.COMP;
			RETRY = host.RETRY;
			
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;	
			
			themeView.hostName = host.GetCharName();
		}
		
		public override void Unload ()
		{
			base.Unload ();
			view = null;
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
		}
		
		
		public override void SetHost (BaseCharacter _host)
		{
			
			base.SetHost (_host);
		
			if(hostAnchor == null)
				hostAnchor = themeView.getChildByName<MovieClip>("mc_hostAnchor");
			PositionHost(hostAnchor); 
			
			int index = themeView.getChildIndex(themeView.getChildByName<MovieClip>("tutorialBG_mc"));
			themeView.setChildIndex(hostAnchor, index);	
			
			// remove after setting position since the visual shadow is now with the character asset
			hostAnchor.visible = false;
		}
		
		public override void PlayIntro()
		{
			base.PlayIntro();
			
			TimerUtils.SetTimeout( 1f, delayedIntro );
		}
		
		private void delayedIntro()
		{
			if(currentSkill.CurrentSession.helpMode)
			{
				currentSkill.OnAudioWordComplete("THEME_INTRO");
				return;
			}
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			//lion roar
			Debug.Log("audio: " + DRIVE);
			AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.TRAVEL_THEME, DRIVE);
			bundle.AddClip( clip, "DRIVE", clip.length );
			//intro speak
			
			string audioPath = BASE_PATH + host.GetCharName() + "/" + INTRO_PATH + "/" + INTRO;
			Debug.Log("audio path: " + audioPath);
			
			clip = SoundUtils.LoadGlobalSound(MovieClipFactory.TRAVEL_THEME, audioPath);
			
			if(clip == null)
			{
				DebugConsole.LogError("Intro audio is null: " + audioPath);
				currentSkill.OnAudioWordComplete("THEME_INTRO");
				return;
			}
			
			bundle.AddClip( clip, "THEME_INTRO", clip.length );
			SoundEngine.Instance.PlayBundle( bundle );
			//Send message to the main game to play game's intro
		}
		
		public override void PlayCompliment(CEvent e ){
			//Need Audio
			base.PlayCompliment(e);
			
			/*if(currentSkill.CurrentSession.helpMode)
			{
				currentSkill.OnAudioWordComplete("THEME_COMPLIMENT");
				return;
			}*/
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			
			string audioPath = BASE_PATH + host.GetCharName() + "/" + host.GetCharName() + COMP_PATH + "/" + COMP[UnityEngine.Random.Range(0,COMP.Length)];
			Debug.Log("audio path: " + audioPath);
			
			AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.TRAVEL_THEME, audioPath);
			
			if(clip == null)
			{
				DebugConsole.LogError("Compliment audio is null: " + audioPath);
				currentSkill.OnAudioWordComplete("THEME_COMPLIMENT");
				return;
			}
			
			bundle.AddClip( clip, "THEME_COMPLIMENT", clip.length );
			SoundEngine.Instance.PlayBundle( bundle );
			//Send message to the main game to play game's message
		}
		
		public override void PlayCriticism(CEvent e){
			//Need Audio
			base.PlayCriticism(e);
			
			/*if(currentSkill.CurrentSession.helpMode)
			{
				currentSkill.OnAudioWordComplete("THEME_CRITICISM");
				return;
			}*/
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			
			string audioPath = BASE_PATH + host.GetCharName() + "/" + host.GetCharName() + RTRY_PATH + "/" + RETRY[UnityEngine.Random.Range(0,RETRY.Length)];
			Debug.Log("audio path: " + audioPath);
			
			AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.TRAVEL_THEME,  audioPath);
			
			if(clip == null)
			{
				DebugConsole.LogError("Criticism audio is null: " + audioPath );
				currentSkill.OnAudioWordComplete("THEME_CRITICISM");
				return;
			}
			
			bundle.AddClip( clip, "THEME_CRITICISM", clip.length );
			SoundEngine.Instance.PlayBundle( bundle );
			//Send message to the main game to play game's message
		}		
	}
}


