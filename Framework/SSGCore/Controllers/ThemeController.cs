using System;
using UnityEngine;
using HatchFramework;

using pumpkin.display;
using pumpkin;
using pumpkin.events;
using pumpkin.text;
using pumpkin.swf;
using pumpkin.tweener;

namespace SSGCore
{
	public class ThemeController : ViewController
	{
		public Theme theme;
		
		public BaseCharacter host;
		public ThemeView themeView;
		public bool sessionCompleted;
		protected BaseSkill currentSkill;
		
		private int hostXOffset = 0;
		private int hostYOffset = 35;
		
		protected MovieClip hostAnchor;
		private bool shifted = false;
		
		public string hostName;
		
		OnFrame OnHappyEvent;
		public event OnFrame OnFrameEventHappy{ add{OnHappyEvent += value;} remove{OnHappyEvent -= value;} }
		OnFrame OnSadEvent;
		public event OnFrame OnFrameEventSad{ add{OnSadEvent += value;} remove{OnSadEvent -= value;} }
		
		public ThemeController(){ Init (); }
		
		public ThemeController (Theme theme):base()
		{
			Init ();
			this.theme = theme;
		}
		
		public ThemeController(Theme theme, ThemeView view):base(view){
			Init ();
			this.theme = theme;
			this.themeView = view;
		}
		
		public virtual void Init()
		{
		}	
		
		public virtual void Unload()
		{
			
			themeView.Unload();
			UnloadHost();
			theme = null;
			themeView = null;
			view = null;
			//remove sound engine references to everything... just to be safe?
			SoundEngine.Instance.ClearListeners();

		}

		
		//Audio
		public virtual void PlayIntro(){
			//Play audio here	
			
			/*if(curre.HelpMode)
			{
				currentSkill.OnAudioWordComplete("THEME_INTRO");
				return;
			}*/
		}
		
		//Same for all themes
		//private static var _broadCompliments:Array = ["A ROUND","CLAP YOUR","FAB YOU","FANTASTIC YOU","GIVE A","GIVE YOURSELF","GREAT JOB","HOORAY","JUMP HIGH","KNEE","LOOK AT","PAT YOUR","SHAKE","SPIRIT","THREE CHEERS","WE ARE","WOW","YOU ARE","YOU DID","YOU MAKE","KISS BRAIN","FABULOUS EFFORT","OUTSTANDING","SUPERIOR","USING BRAIN"];
		//private static var _broadCriticisms:Array = ["GIVE IT","I WANT","LETS KEEP","LETS LOOK","LETS TRY","TRY","YOU WERE"];
		public virtual void PlayCompliment(CEvent e ){
			//Need Audio
			
			host.PlayAnimation("HAPPY");
			
			/*if(themeView.sessionController.HelpMode)
			{
				currentSkill.OnAudioWordComplete("THEME_COMPLIMENT");
				return;
			}*/
			
		}
		
		public virtual void PlayCriticism(CEvent e){
			//Need Audio
			
			host.PlayAnimation("SAD");
			
			/*if(themeView.sessionController.HelpMode)
			{
				currentSkill.OnAudioWordComplete("THEME_CRITICISM");
				return;
			}*/
			
		}
		 
		// Similar to PlayCompliment() but no audio is used
		// the shell does not reqire an event to be dispatched
		public virtual void HandleCorrect(){
			//Need Audio
			host.PlayAnimation("HAPPY");
		}
		
		// Similar to PlayCriticism() but no audio is used
		// the shell does not reqire an event to be dispatched
		public virtual void HandleIncorrectCorrect(){
			//Need Audio
			host.PlayAnimation("SAD");
		}
		
		public virtual void PlayWaitingAnimation(CEvent e)
		{
			host.PlayAnimation(BaseCharacter.RESTING);
		}
		
		public virtual void CorrectOpportunity(bool correct){
			themeView.CorrectOpportunity(correct);
		}
		
		// The main shell calls this method when a user has completed a skill game entirely
		// any payoffs that are not complete should be removed and any objects on the stage should be reset to their original positions
		// $percent is a number 0 - 100 which represents the percent of correct opportunities the user got in the skill game.
		// The audio for the reset animation is determined by what range $percent is in.
		// when the reset animation is completed dispatch the ThemeEvent.SESSION_COMPLETE event.
		public virtual void CompleteSession(CEvent e){
			themeView.CompleteSession();
			HandleSessionComplete();
		}
		
		public virtual void LoadHost(){
			themeView.SetSession(currentSkill.CurrentSession);
		}
		
		public float hostScale = 1.3f;
		public virtual void SetHost(BaseCharacter _host)
		{			
			UnloadHost();
			host = _host;
			
			host.scaleX = host.scaleY *= hostScale;
			
			//if( (float)Screen.height / (float)Screen.width == .75f ) host.scaleX *= 1.25f;
			view.addChild(host);		
		}
		
		public virtual void PositionHost(MovieClip anchor)
		{				
						
			if( host is BasePlatty )
			{
				hostXOffset = PlatformUtils.GetClosestRatio() != ScreenRatio.FOURxTHREE ? -45 : -5; //-45
				hostYOffset = 80; //60
			}
			else if( host is BaseCam )
			{
				hostXOffset = PlatformUtils.GetClosestRatio() != ScreenRatio.FOURxTHREE ? -105 : -75; //-25
				hostYOffset = 70;
			}
			else if( host is BaseHenry )
			{
				hostXOffset = PlatformUtils.GetClosestRatio() != ScreenRatio.FOURxTHREE ? -80 : -60; //-80
				hostYOffset = 70;
			}
			
			// bug 1432 - reported as android issue with resolution like 1024x600. My assumption is that it's not android specific, but a resolution issue, hence the check for 4x3			
			if( currentSkill.CurrentSession.theme.themeName == "Safari" && !shifted)
			{
				anchor.x -= PlatformUtils.GetClosestRatio() != ScreenRatio.FOURxTHREE ? 80 : 20;
				shifted = true;
			}

			host.x = anchor.getBounds(host.parent).x + Mathf.Abs((anchor.getBounds(host.parent).width*.5f) - (host.getBounds(host.parent).width*.5f)) - hostXOffset;
			host.y = anchor.getBounds(host.parent).y - (host.getBounds(host.parent).height-(anchor.getBounds(host.parent).height*.5f)) + hostYOffset;
			
			if( SWFViewer.Instance != null )
			{
				SWFViewer.Instance.AddClip("view", view);
			}
		}
		
		//Show overlay MC
		public void ShowOverlay()
		{
			DebugConsole.LogError("SHOW OVERLAY CALLED");
			themeView.mc_overlay.alpha = 0.0f;
			Tweener.addTween(themeView.mc_overlay, Tweener.Hash("time", 0.35, "alpha", 1.0, "visible", true, "transition", Tweener.TransitionType.easeOutQuad));
		}
		
		//hide overlay MC
		public void HideOverlay(){ 
			Tweener.addTween(themeView.mc_overlay, Tweener.Hash("time", 0.35, "alpha", 0, "visible", false, "transition", Tweener.TransitionType.easeOutQuad));
		}
		
		//show Tutorial bg
		public void ShowTutorial()
		{
			DebugConsole.LogError("SHOW TUTORIAL");
			themeView.mc_overlay.visible = false;
			themeView.mc_overlay2.visible = false;
			themeView.shadow_mc.visible = true;
			themeView.shadow_mc.alpha = 0.0f;
			Tweener.addTween(themeView.shadow_mc, Tweener.Hash("time", 0.35, "alpha", 1.0, "visible", true, "transition", Tweener.TransitionType.easeOutQuad));
			
		}
		
		//hide Tutorial MC
		public void HideTutorial()
		{
			themeView.mc_overlay.visible = true;
			themeView.mc_overlay2.visible = true;
			Tweener.addTween(themeView.shadow_mc, Tweener.Hash("time", 0.35, "alpha", 0, "visible", false, "transition", Tweener.TransitionType.easeOutQuad));
		}
		
		public void PlayAnimation(CEvent e)
		{
			SkillEvent evt = e as SkillEvent;
			if( !string.IsNullOrEmpty((evt.Value as string)) )
			{
				host.PlayAnimation(evt.Value as string);
			}
		}
		
		public void RegisterSkill(BaseSkill skill)
		{		
			currentSkill = skill;
			skill.addEventListener(SkillEvent.COMPLIMENT_REQUEST, PlayCompliment);
			skill.addEventListener(SkillEvent.CRITICISM_REQUEST, PlayCriticism);
			skill.addEventListener(SkillEvent.END_THEME_REQUEST, CompleteSession);
			skill.addEventListener(SkillEvent.ANIMATION_REQUEST, PlayAnimation);
			skill.addEventListener(SkillEvent.TIMER_COMPLETE, PlayWaitingAnimation);
			
			UnloadHost();
			
			LoadHost();
			
		}
		
		public void UnregisterSkill(){
		
			if(currentSkill != null)
			{
				currentSkill.removeEventListener(SkillEvent.COMPLIMENT_REQUEST, PlayCompliment);
				currentSkill.removeEventListener(SkillEvent.CRITICISM_REQUEST, PlayCriticism);
				currentSkill.removeEventListener(SkillEvent.END_THEME_REQUEST, CompleteSession);
				currentSkill.removeEventListener(SkillEvent.ANIMATION_REQUEST, PlayAnimation);	
				currentSkill.removeEventListener(SkillEvent.TIMER_COMPLETE, PlayWaitingAnimation);
				
				currentSkill = null;
			}
			
		}
		
		public void UnloadHost(){
		
			if(host != null){
				host.parent.removeChild(host);
				host = null;
			}
		}
				
		//called when session has been completed - should be called internally
		public void HandleSessionComplete()
		{			
			MoveHostToCenter();
			
			AudioClip clip = null;
			
			switch( currentSkill.CurrentSession.theme.themeName )
			{
				case "Safari":
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SAFARI_THEME, "TH-sparkles" );
					break;
					
				case "Garden":
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.GARDEN_THEME, "Sound Effects/TH-sparkles" );
					break;
					
				case "Travel":
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.TRAVEL_THEME, "Sound Effects/TH-sparkles" );
					break;
			}
			
			SoundEngine.Instance.PlayEffect(clip);
		}
		
		private void MoveHostToCenter()
		{
			DebugConsole.LogError("**************** CENTER HOST ****************");
			float hostCenterPositionX = ((Screen.width * 0.5f) - (host.width*.5f)) - hostXOffset;//(MainUI.STAGE_WIDTH * .5f) - ((host.width) * 0.5f) - hostXOffset;//
			float hostCenterPositionY = (MainUI.STAGE_HEIGHT * .5f) + (host.height *.5f);//((Screen.height * 0.5f) - (host.height*.5f)) + hostYOffset;
			
			host.PlayAnimation("PAYOFF");
			
			//Tweener.addTween( host, Tweener.Hash( "x",hostCenterPositionX, "y",hostCenterPositionY, "scaleX",host.scaleX * 3f , "scaleY",host.scaleY * 3f , "time",0.35f, "transition",Tweener.TransitionType.easeOutQuint ) );
			Tweener.addTween( host, Tweener.Hash( "x",hostCenterPositionX, "y",hostCenterPositionY , "time",0.35f, "transition",Tweener.TransitionType.easeOutQuint ) );
			
		}
		
		public virtual void forceShadowUp()
		{
			themeView.removeChild(hostAnchor);
			int index = themeView.getChildIndex(host);
			themeView.addChildAt(hostAnchor, index - 1);
		}
	}
}

