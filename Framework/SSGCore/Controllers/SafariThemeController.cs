using System;
using pumpkin.display;
using UnityEngine;
using System.Collections.Generic;
using HatchFramework;
using pumpkin.events;

using System.Collections;
using pumpkin.swf;
using System.Linq;
using pumpkin.tweener;
using pumpkin.text;

namespace SSGCore
{
	public class SafariThemeController : ThemeController
	{
		public const string BASE_PATH = "TH-SF-";
		public const string PAYOFF50_PATH = "TH-SF-50Percent-Payoff-Audio";
		public const string PAYOFF100_PATH = "TH-SF-100Percent-Payoff-Audio";
		public const string END_PATH = "TH-SF-End-of-Game-Audio";
		public const string INTRO_PATH = "TH-SF-Introduction-Audio";
		public const string COMP_PATH = "-Complements";
		public const string RTRY_PATH = "-Retry";
		public const string THEME_SONG = "TH-SF-theme-song";
		
		public const string ROAR = "TH-SF-lion";
		public const string PEEK = "TH-SF-peeking";
		public const string THUMP = "TH-SF-thump";
		public const string SPARKLES = "TH-sparkles";
		public const string HOORAY = "TH-hip_hooray";
		
		public string INTRO;// = "TH-SF-Platty-Safari";
		
		//THERE ARE 3 OF EACH OF THESE
		public const string PAYOFF50 = "TH-SF-50-Payoff";
		public const string PAYOFF100 = "TH-SF-";
		public const string PAYOFF_CHEETAH = "TH-SF-cheetah";
		public const string PAYOFF_GAZELLE = "TH-SF-gazelle";
		public const string PAYOFF_GIRAFFE = "TH-SF-giraffe";
		public const string PAYOFF_LION = "TH-SF-lion";
		public const string PAYOFF_ELEPHANT = "TH-SF-elephant";
		public const string PAYOFF_HIPPO = "TH-SF-hippo";
		public const string PAYOFF_HYENA = "TH-SF-hyena";
		public const string PAYOFF_MEERKAT = "TH-SF-meerkat";
		public const string PAYOFF_ZEBRA = "TH-SF-zebra";
		public const string PAYOFF_RHINO = "TH-SF-rhino";
		public const string END_BEGINNER = "TH-SF-beginner";
		public const string END_INTERMEDIATE = "TH-SF-intermediate";
		public const string END_MASTER = "TH-SF-master";
		
		public string[] RETRY;
		/*= {"RTRY-give-it-another-try",
								 "RTRY-i-want-to-help-you-try-again",
							 	 "RTRY-lets-keep-going-one-more-try",
							 	 "RTRY-lets-look-again",
								 "RTRY-lets-think-harder",
								 "RTRY-lets-try-again",
								 "RTRY-put-your-thinking-cap-on",
								 "RTRY-think-one-more-time",
								 "RTRY-try-try-again",
								 "RTRY-you-were-close-lets-try-again"};*/
		
		public string[] COMP;
			/* = {"COMP-a-round-of-applause",
								"COMP-big-hug",
								"COMP-clap-your-hands-stomp-your-feet",
								"COMP-excellent",
								"COMP-fab-you-you-are-fabulous",
								"COMP-fabulous-effort",
								"COMP-fabulous-fabulous",
								"COMP-fantastic",
								"COMP-fantastic-you-are-at-the-top",
								"COMP-give-a-silent-cheer",
								"COMP-good-job",
								"COMP-great-job-will-you-be-my-friend",
								"COMP-hooray-you-made-my-day",
								"COMP-jump-high-for-joy",
								"COMP-kiss-your-brain",
								"COMP-knee-slappin",
								"COMP-look-at-you-you-are-the-best",
								"COMP-outstanding",
								"COMP-pat-yourself-on-the-back",
								"COMP-shake-shake-you-take-the-cake",
								"COMP-spirit-fingers",
								"COMP-super-job",
								"COMP-super-super",
								"COMP-superior",
								"COMP-three-cheers-for-you",
								"COMP-we-are-having-fun-you-are-my-partner",
								"COMP-wow-did-you-see-what-you-did-wow",
								"COMP-you-are-tops-please-dont-stop",
								"COMP-you-did-it",
								"COMP-you-make-me-smile"};*/
		
		protected new SafariThemeView view;
		public SafariThemeController(Theme theme, SafariThemeView view):base(theme, view)
		{						
			//Debug.Log("host comp length: " + host.COMP.Length);
			this.view = view;
		}
		
		private void OnAudioWordComplete( string word )
		{			
			/*if( host == null )
			{
				DebugConsole.LogError("SAFARI THEME CONTROLLER - HOST IS NULL");
				return;
			}*/
			
			DebugConsole.Log("Host is null: {0}", (host == null) );
			
			switch(word)
			{
				case "ROAR":
				case "PEEK":
				case "THUMP":
					host.PlayAnimation(BaseCharacter.RIGHT_TALKING);
				break;
				case "THEME_INTRO":
					host.PlayAnimation(BaseCharacter.RIGHT_TALKING_END);
					//themeView.ShowOverlay();
				break;
				case "PAYOFF_50":
				case "PAYOFF_100":
					host.PlayAnimation(BaseCharacter.RIGHT_TALKING_END);
				break;
				case "THEME_END":
					host.PlayAnimation(BaseCharacter.PAYOFF_END);
				break;
			}
		}
	
		public override void LoadHost ()
		{
			base.LoadHost();
			switch(currentSkill.Host)
			{
				case "Platty":
					BasePlatty plat = CharacterFactory.GetSafariPlattyCharacter();
					INTRO = "TH-SF-Platty-Safari";
					this.SetHost(plat);
				break;
				case "Henry":
					BaseHenry henry = CharacterFactory.GetSafariHenryCharacter();
					INTRO = "TH-SF-Henry-Safari";
					this.SetHost(henry);
				break;
				case "Cami":
					BaseCam cam = CharacterFactory.GetSafariCamCharacter();
					INTRO = "TH-SF-Cami-Safari";
					this.SetHost(cam);
				break;
			}
			
			COMP = host.COMP;
			RETRY = host.RETRY;
			
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;	
			
			themeView.hostName = host.GetCharName();
		}
		
		public override void Unload ()
		{
			base.Unload ();
			view = null;
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
		}
		
		
		public override void SetHost (BaseCharacter _host)
		{			
			base.SetHost (_host);
		
			if(hostAnchor == null) hostAnchor = view.safariRoot.getChildByName<MovieClip>("mc_hostAnchor");
			
			PositionHost(hostAnchor);
			
			int index = view.safariRoot.getChildIndex( view.safariRoot.getChildByName<MovieClip>("tutorialBG_mc"));
			view.safariRoot.setChildIndex(hostAnchor, index);
			
			// remove after setting position since the visual shadow is now with the character asset
			hostAnchor.visible = false;
		}
		
		public override void PlayIntro()
		{
			base.PlayIntro();
			
			TimerUtils.SetTimeout( 1f, delayedIntro );
		}
		
		private void delayedIntro()
		{
			//DebugConsole.Log("PlayIntro HelpMode: " + themeView.sessionController.HelpMode);
			if(currentSkill.CurrentSession.helpMode)
			{
				currentSkill.OnAudioWordComplete("THEME_INTRO");
				return;
			}
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			//lion roar
			//Debug.Log("audio: " + ROAR);
			AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SAFARI_THEME, ROAR);
			bundle.AddClip( clip, "ROAR", clip.length );
			//intro speak
			
			string audioPath = BASE_PATH + host.GetCharName() + "/" + INTRO_PATH + "/" + INTRO;
			//Debug.Log("audio path: " + audioPath);
			
			clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SAFARI_THEME, audioPath);
			
			if(clip == null)
			{
				DebugConsole.LogError("Intro audio is null: " + audioPath);
				currentSkill.OnAudioWordComplete("THEME_INTRO");
				return;
			}
			
			bundle.AddClip( clip, "THEME_INTRO", clip.length );
			SoundEngine.Instance.PlayBundle( bundle );
			//Send message to the main game to play game's intro	
		}
		
		
		
		public override void PlayCompliment(CEvent e ){
			//Need Audio
			base.PlayCompliment(e);
			
			/*if(currentSkill.CurrentSession.helpMode)
			{
				currentSkill.OnAudioWordComplete("THEME_COMPLIMENT");
				return;
			}*/
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			
			string audioPath = BASE_PATH + host.GetCharName() + "/" + host.GetCharName() + COMP_PATH + "/" + COMP[UnityEngine.Random.Range(0,COMP.Length)];
			//Debug.Log("audio path: " + audioPath);
			
			AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SAFARI_THEME, audioPath);
			
			if(clip == null)
			{
				DebugConsole.LogError("Compliment audio is null: " + audioPath);
				currentSkill.OnAudioWordComplete("THEME_COMPLIMENT");
				return;
			}
			
			bundle.AddClip( clip, "THEME_COMPLIMENT", clip.length );
			SoundEngine.Instance.PlayBundle( bundle );
			//Send message to the main game to play game's message
		}
		
		public override void PlayCriticism(CEvent e){
			//Need Audio
			base.PlayCriticism(e);
			
			/*if(currentSkill.CurrentSession.helpMode)
			{
				currentSkill.OnAudioWordComplete("THEME_CRITICISM");
				return;
			}*/
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			
			string audioPath = BASE_PATH + host.GetCharName() + "/" + host.GetCharName() + RTRY_PATH + "/" + RETRY[UnityEngine.Random.Range(0,RETRY.Length)];
			Debug.Log("audio path: " + audioPath);
			
			AudioClip clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SAFARI_THEME,  audioPath);
			
			if(clip == null)
			{
				DebugConsole.LogError("Criticism audio is null: " + audioPath );
				currentSkill.OnAudioWordComplete("THEME_CRITICISM");
				return;
			}
			
			bundle.AddClip( clip, "THEME_CRITICISM", clip.length );
			SoundEngine.Instance.PlayBundle( bundle );
			//Send message to the main game to play game's message
		}
		
		public override void forceShadowUp ()
		{
			view.safariRoot.removeChild(hostAnchor);
			//int index = view.safariRoot.getChildIndex(host);
			view.safariRoot.addChild(hostAnchor);
		}
	}
}


