using System;
using UnityEngine;
using HatchFramework;
using pumpkin.display;
using pumpkin.events;
using System.Collections;
using System.Collections.Generic;
using pumpkin;
using pumpkin.swf;
using LitJson;

namespace SSGCore
{
	public class SessionController : ViewController
	{

		protected DBObject databaseObject;
		public SessionInfo CurrentConfig{ get{ return currentConfig; }}
		protected SessionInfo currentConfig = new SessionInfo();
		protected SessionInfo lastSession = new SessionInfo();
		public BaseSkill CurrentSkill{ get{ return skill; }}
		protected BaseSkill skill;
		protected TeacherMode teacherModeBar;
		protected MenuOverlay menu;
		private DisplayObject noSkillsView;
		private TryAgainMenu tryOrNew;
		
		protected DisplayObjectContainer teacherModeContainer;
		
		public bool inGame = false;
				
		protected SkillLevelPair selectedSkill;
		protected SkillSession skillSession;
		protected SkillSessionContext skillSessionContext;
		
		private Action OnStopInvoker;
		public event Action OnStop
		{
			add { OnStopInvoker += value; }
			remove { OnStopInvoker -= value; }
		}
		
		private Action OnExitTeacherModeInvoker;
		public event Action OnExitTeacherMode
		{
			add { OnExitTeacherModeInvoker += value; }
			remove { OnExitTeacherModeInvoker -= value; }
		}
		
		public delegate IEnumerator SyncMethod( bool showSync );
		public SyncMethod Sync;
		
		public JsonData bookmark;
		public bool deserialized = false;
		
		private bool skillCompleted = false;
		private bool unpauseOnResume = false;
		
		public bool syncNeeded = false;
		
		private bool holdBookmark = false;
		private bool helpMode = false;
		public bool HelpMode
		{
			get{  return helpMode; }
			set
			{
				menu.HelpButtonEnabled(!value);
				menu.HelpContinueButtonEnabled(value);
				
				if(helpMode == value) return;
				helpMode = value;
				
				holdBookmark = true;
				
				DebugConsole.Log("Help Mode changed to " + helpMode);
				var initialOpportunity = 0;
				if(currentConfig.skillPair.Level != SkillLevel.Tutorial) {
					initialOpportunity = currentConfig.initialOpportunity;
				}
				
				string sessionData = currentConfig.sessionData;
				string themeData = currentConfig.themeData;
				
				UnloadSkill();
				
				SoundEngine.Instance.SendCancelToken();
				SoundEngine.Instance.StopAll();
				
				SkillLevelPair helpPair;
				
				currentConfig.skipIntro = true;
				
				if(value)
				{
					currentConfig.sessionData = sessionData;
					currentConfig.themeData = themeData;
					helpPair = new SkillLevelPair( currentConfig.skillPair.Skill, SkillLevel.Tutorial );
					currentConfig.initialOpportunity = 0;
					lastSession = currentConfig.Clone();
					lastSession.initialOpportunity = initialOpportunity;
					lastSession.currentSerializedSession.help = true;
				}
				else
				{
					helpPair = new SkillLevelPair( lastSession.skillPair.Skill, lastSession.skillPair.Level );
					currentConfig.CopyFrom(lastSession);
				}
				
				LoadSkill( helpPair );
			}
		}
					
		private void clearHelp(CEvent e)
		{
			HelpMode = false;
		}
		
		[DebugConsoleMethod("completeskill","completes skill with given result")]
		public void CompleteSkill(int correct, int total){
			if(inGame) {
				currentConfig.correct = correct;
				currentConfig.incorrect = total - correct;
				currentConfig.totalOpportunities = total;
				this.OnSkillComplete(null);
			}
		}

		
		public SessionController(MovieClip view, DBObject databaseObject):base(view){
		
			this.databaseObject = databaseObject;
			this.view = view;
			DebugConsole.Instance.RegisterBehaviour(this);
		}
		
		[DebugConsoleMethod("forcespanish","forces the next loaded skill to play in spanish")]
		public void forceSpanish(){
			SessionInfo.FORCE_SPN = true;
		}
		[DebugConsoleMethod("releasespanish","returns language selection to normal profile behavior")]
		public void releaseSpanish(){
			SessionInfo.FORCE_SPN = false;
		}
		
		public void LoadSession(SessionInfo sessionConfig)
		{
			helpMode = false;
			if( !sessionConfig.sessionResume || deserialized )
			{
				sessionConfig.ResetSessionStats();
				deserialized = false;
			}
			else deserialized = true;
			sessionConfig.skipHelpFlag = true;
			
			SSGEngine.Instance.StartCoroutine(LoadSessionAsync(sessionConfig));
		}
		
		protected IEnumerator LoadSessionAsync(SessionInfo sessionConfig)
		{
			UnloadSkill();
			
			view.visible = true;
			
			if(sessionConfig.user == null){
				sessionConfig.user= new Child(0, "Test", "test", null, "1/1/2009","", "");
			}
			
			if(sessionConfig.demoMode) {
				skillSessionContext = new DemoSkillContext(sessionConfig.user);
			} else if(sessionConfig.teacherMode) {
				skillSessionContext = new TeacherSkillContext(databaseObject, sessionConfig.user);
			} else {
				skillSessionContext = new DatabaseSkillContext(databaseObject, sessionConfig.user);
			}
			
			skillSession = new SkillSession(skillSessionContext);
			
			if(sessionConfig.skillPair == null){
				sessionConfig.skillPair = skillSession.ChooseFirstSkill();
			}
			
			if( currentConfig == null )
			{
				lastSession = new SessionInfo();
				lastSession.ResetSessionStats();
			}
			else
				lastSession = currentConfig.Clone();
			currentConfig = sessionConfig;
			
			//add skill overlay
			LoadSkillOverlay();
			
			// if in non-demo teacher mode, unlock skill
			if(sessionConfig.teacherMode && !sessionConfig.demoMode) {
				if( SkillImplementationUtil.getLangTagforSkill( sessionConfig.skillPair.Skill.linkage, LanguageProfileUtility.FromId(sessionConfig.user.Profile) ) == SkillImplementationUtil.ENG_LANG_TAG )
					databaseObject.UnlockSkill(sessionConfig.user.ID, sessionConfig.skillPair.Skill.ID);
				else
					databaseObject.UnlockSkill(sessionConfig.user.ID, sessionConfig.skillPair.Skill.SPN_ID);
			}
			
			if(sessionConfig.skillPair == null) 
			{
				DebugConsole.LogError("NO SKILL PAIR - UNLOADING MEMORY");
				ShowNoAvailableSkillsView();
				yield break;
			}
			
			if(sessionConfig.teacherMode || GlobalConfig.GetProperty<bool>("ForceTeacherMode")){
				teacherModeBar = new TeacherMode();
				menu.addTeacherMode(teacherModeBar);
				teacherModeBar.OnExitClicked = HandleExitTeacherMode;
				teacherModeBar.sessionConfig = sessionConfig;
				teacherModeBar.Resize( Screen.width, Screen.height );
				
				currentConfig.currentSerializedSession.teacher = true;
				Debug.Log ("SET TEACHER MODE");
			}
			else
			{
				if(teacherModeBar != null)
				{
					menu.removeTeacherMode();
					teacherModeBar = null;
				}
			}
			
			// INTRO FLAGS
			if( !(currentConfig.teacherMode || lastSession.teacherMode)
				&& (lastSession.skillPair == null || lastSession.skillPair.Level != SkillLevel.Tutorial)
				&& currentConfig.skillPair.Level == SkillLevel.Emerging  )
			{
				helpMode = true;
				HelpMode = true;
				
				lastSession = currentConfig.Clone();
				lastSession.skipIntro = true;
				
				currentConfig.initialOpportunity = 0;

				currentConfig.skipIntro = false;
				yield return LoadSkill( new SkillLevelPair( currentConfig.skillPair.Skill, SkillLevel.Tutorial ) );
			}
			else
				yield return LoadSkill(sessionConfig.skillPair);
			
			yield return null;
		}
		
		protected void LoadSkillOverlay()
		{
			UnloadSkillOverlay();
			menu = MovieClipFactory.CreateMenuOverlay(this);
			if( menu != null ) view.addChild(menu);
			else DebugConsole.LogError("MENU WAS NOT CREATED FROM MOVIE CLIP FACTORY");
		}
		
		private void UnloadSkillOverlay()
		{
			if(menu != null) 
			{
				view.removeChild(menu);
				
				bool didMenuOverlayUnload = MovieClipFactory.UnloadSwf(menu.swfUri.swf);
				DebugConsole.Log("****** DID MENUOVERLAY SWF UNLOAD (" + menu.swfUri.swf + ")? " + didMenuOverlayUnload);
				
				menu.CancelMonitor();
				menu.Unload();
				
				//menu = null;
			}
		}
		
		protected Coroutine LoadSkill(SkillLevelPair pair){
			//This is to fix a VERY edge case when the tryOrNew screen times out it will appear in the next skill at every transition
			if(tryOrNew != null)
			{
				view.removeChild( tryOrNew );
				tryOrNew.Again = null;
				tryOrNew.Another = null;
				tryOrNew = null;
			}
			return SSGEngine.Instance.StartCoroutine(DoLoadSkill(pair));
		}
		
		// a simple hack so that un-implemented skills are not selected
		// keep updated, and remove when all skills are present
		public static bool HACK_IsSkillImplemented(Skill skill)
		{
			switch(skill.linkage)
			{
			case "LetterRecognition":
			case "NumeralRecognition":
			case "InitialSounds":
			case "CommonShapes":
			case "CountingFoundations":
			case "Sorting":
			case "SpacialSkills":
			case "SpatialSkills":
			case "SentenceSegmenting":
			case "LanguageVocabulary":
			case "Addition":
			case "AdditionUpToTen":
			case "Subtraction":
			case "SubtractionFromTen":
			case "OnsetRime":
			case "DeletingOnsetAndRime":
			case "ObjectsInASet":
			case "SequenceCounting":
			case "SegmentingCompoundWords":
			case "BlendingCompoundWords":
			case "Patterning":
			case "Measurement":
			case "BlendingSoundsInWords":
				return true;
			default:
				return false;
			}
		}
	
		protected IEnumerator DoLoadSkill(SkillLevelPair pair)
		{			
			menu.SetSkillLevel(pair.Level);
			menu.StopButtonEnabled(true);
			
			DebugConsole.Log("Loading {0}", pair.Skill);

			bool restartingSkill = currentConfig.initialOpportunity > 0;
			if(!restartingSkill) currentConfig.ResetSessionStats();
		
			if(skill != null) 
			{
				DebugConsole.LogError("SKILL NOT UNLOADED WHEN TRYING TO LOAD NEW SKILL - UNLOADING: " + skill.name);
				UnloadSkill();
				// give it a moment to actually clear the old skill before continuing 
				yield return new WaitForSeconds(1f);
			}
			
			
			if( SessionInfo.FORCE_SPN ){
				AssetLoader.LoadLanguageFlag = SkillImplementationUtil.IsSkillSpanish( pair.Skill.linkage ) ? SkillImplementationUtil.SPN_LANG_TAG : SkillImplementationUtil.ENG_LANG_TAG ;
				currentConfig.SPN_FLAG = true;
			}
			else if( CurrentConfig.user.Profile != LanguageProfile.English ){
				string langFlag = SkillImplementationUtil.getLangTagforSkill( pair.Skill.linkage, LanguageProfileUtility.FromId( CurrentConfig.user.Profile ) );
				currentConfig.SPN_FLAG = langFlag == SkillImplementationUtil.SPN_LANG_TAG;
				AssetLoader.LoadLanguageFlag = langFlag;
			}
			else
			{
				currentConfig.SPN_FLAG = false;
				AssetLoader.LoadLanguageFlag = SkillImplementationUtil.ENG_LANG_TAG;
			}
			
			if( currentConfig.SPN_FLAG )
				menu.SetMenuSpn();
			else
				menu.SetMenuEng();
			
			yield return SSGEngine.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle(pair.Skill.linkage, null));
			
			MovieClipFactory.reloadTextureSpecificSkill( pair.Skill.linkage );
			yield return new WaitForSeconds(1f);
			
			SoundEngine.Instance.StopAll();
			
			string skillName = AssetLoader.LoadLanguageFlag + pair.Skill.linkage;
			
			switch(skillName)
			{
				case "LetterRecognition":
				case "SPN-LetterRecognition":
					skill = new LetterRecognition(pair.Level, currentConfig);
					menu.SetSkillName("Letter Recognition");
				break;
				
				case "NumeralRecognition":
				case "SPN-NumeralRecognition":
					skill = new NumeralRecognition(pair.Level, currentConfig);	
					menu.SetSkillName("Numeral Recognition");
				break;
				
				case "InitialSounds":
				case "SPN-InitialSounds":
					skill = new InitialSounds(pair.Level, currentConfig);
					menu.SetSkillName("Initial Sounds");
				break;
				
				case "CommonShapes":
				case "SPN-CommonShapes":
					skill = new CommonShapes(pair.Level, currentConfig);
					menu.SetSkillName("Common Shapes");
				break;
				
				case "CountingFoundations":
				case "SPN-CountingFoundations":
					skill = new CountingFoundations(pair.Level, currentConfig);
					menu.SetSkillName("Counting Foundations");
//					if( SWFViewer.Instance != null ) SWFViewer.Instance.AddClip(menu.GetSkillName(), (skill as CountingFoundations).container);
				break;
				
				case "Sorting":
				case "SPN-Sorting":
					skill = new Sorting(pair.Level, currentConfig);
					menu.SetSkillName("Sorting");
				break;
				
				case "SpacialSkills":
				case "SpatialSkills":
				case "SPN-SpacialSkills":
				case "SPN-SpatialSkills":
					skill = new SpacialSkills( pair.Level, currentConfig );
					menu.SetSkillName("Spatial Skills");
				break;
				
				case "SentenceSegmenting":
				case "SPN-SentenceSegmenting":
					skill = new SentenceSegmenting( pair.Level, currentConfig );
					menu.SetSkillName("Sentence Segmenting");
				break;
				
				case "LanguageVocabulary":
				case "SPN-LanguageVocabulary":
					skill = new LanguageVocabulary( pair.Level, currentConfig );
					menu.SetSkillName("Language Vocabulary");
				break;
				
				case "Addition":
				case "SPN-Addition":
					skill = new Addition( pair.Level, currentConfig );
					menu.SetSkillName("Addition");
				break;
				
				case "AdditionUpToTen":
					skill = new AdditionUpToTen( pair.Level, currentConfig );
					menu.SetSkillName("AdditionUpToTen");
				break;
				
				case "Subtraction":
				case "SPN-Subtraction":
					skill = new Subtraction( pair.Level, currentConfig );
					menu.SetSkillName("Subtraction");
				break;
				
				case "SubtractionFromTen":
					skill = new SubtractionFromTen( pair.Level, currentConfig );
					menu.SetSkillName("SubtractionFromTen");
				break;
				
				case "SegmentingCompoundWords":
				case "SPN-SegmentingCompoundWords":
					skill = new SegmentingCompoundWords( pair.Level, currentConfig );
					menu.SetSkillName("Segmenting Compound Words");
				break;
				
				case "BlendingCompoundWords":
				case "SPN-BlendingCompoundWords":
					skill = new BlendingCompoundWords( pair.Level, currentConfig );
					menu.SetSkillName("Blending Compound Words");
				break;

				case "OnsetRime":
				case "SPN-OnsetRime":
					skill = new OnsetRime( pair.Level, currentConfig );
					menu.SetSkillName("Onset Rime");
				break;

				case "DeletingOnsetAndRime":
					skill = new DeletingOnsetAndRime( pair.Level, currentConfig );
					menu.SetSkillName("Deleting Onset And Rime");
				break;

				
				case "ObjectsInASet":
				case "SPN-ObjectsInASet":
					skill = new ObjectsInASet( pair.Level, currentConfig );
					menu.SetSkillName("Objects in a Set");
				break;
				
				case "SequenceCounting":
				case "SPN-SequenceCounting":
					skill = new SequenceCounting( pair.Level, currentConfig );
					menu.SetSkillName("Sequence Counting");
				break;
				
				case "Patterning":
				case "SPN-Patterning":
					skill = new Patterning( pair.Level, currentConfig );
					menu.SetSkillName("Patterning");
				break;
				
				case "Measurement":
				case "SPN-Measurement":
					skill = new Measurement( pair.Level, currentConfig );
					menu.SetSkillName("Measurement");
				break;

				case "BlendingSoundsInWords":
					skill = new BlendingSoundsInWords( pair.Level, currentConfig );
					menu.SetSkillName("Blending Sounds In Words");
				break;
				
				default:
					DebugConsole.LogError("Unsupported Skill: {0}", pair.Skill.linkage);
				break;
				
			}
			
			MuseumDataObject.nextSkill = pair.Skill.linkage;
			
			menu.show();
			menu.StartMonitorProgress();
			
			view.addChildAt(skill.baseTheme, view.getChildIndex(menu));
			view.addChildAt(skill, view.getChildIndex(menu));
			
			inGame = true;
			//skill.addEventListener(SkillEvent.SKILL_COMPLETE, OnSkillComplete);
			skill.addEventListener(SkillEvent.INSTRUCTION_START, OnInstructionStart);
			skill.addEventListener(SkillEvent.INSTRUCTION_END, OnInstructionEnd);
			skill.addEventListener(SkillEvent.OPPORTUNITY_COMPLETE, OnOpportunityComplete);
			skill.addEventListener(TutorialEvent.START, OnTutorialStart);
			skill.addEventListener(SkillEvent.HELP_END, clearHelp);
			skill.addEventListener(SkillEvent.BOOKMARK_NOW, BookmarkCurrent);
			skill.addEventListener(SkillEvent.END_THEME_REQUEST, TransmitSession);
			skill.addEventListener(SkillEvent.TIMEOUT_END, Timeout);
			
			if(	helpMode && pair.Level != SkillLevel.Tutorial )
				helpMode = false;
			
			currentConfig.helpMode = helpMode;
			menu.HelpButtonEnabled(!helpMode);
			skill.Start();
			
			currentConfig.StartTiming();
			
			LoadTheme();
			
			Debug.Log ("WE ARE RESTARTING THE SKILL WHAT?" + restartingSkill + " " + currentConfig.initialOpportunity);
			skill.baseTheme.playIntro();
			
			SerialSessionMap subSession = new SerialSessionMap();
			subSession.time = DateTime.Now.ToString("HH:mm:ss MM/dd/yyyy");
			subSession.help = currentConfig.skipHelpFlag ? false : helpMode;
			subSession.teacher = currentConfig.teacherMode;
			subSession.timed = false;
			List<int> screenArray = new List<int>();
			screenArray.Add(Screen.width);
			screenArray.Add(Screen.height);
			subSession.screen = screenArray;
			subSession.opps = new List<SerialOppMap>();
			
			DebugConsole.LogWarning( "Detected Screen Resolution is: " + Screen.width + "x" + Screen.height );
			
			currentConfig.skipHelpFlag = false;
			
			if( currentConfig.SerializedData != null )
			{
				if( currentConfig.SerializedData.student != currentConfig.user.ID ||
					currentConfig.SerializedData.skill != currentConfig.skillPair.Skill.ID ||
					currentConfig.SerializedData.level != (int)currentConfig.skillPair.Level )
				{
					currentConfig.SerializedData = null;
				}
				else
				{
					currentConfig.SerializedData.sessions.Add( subSession );
					
//					Debug.Log( "Continued serialization: " + currentConfig.SerializedData.toString() );
				}
			}
			if( currentConfig.SerializedData == null )
			{
				subSession.help = false;
				SerialMap sessionData = new SerialMap();
				sessionData.student = currentConfig.user.ID;
				sessionData.skill = currentConfig.skillPair.Skill.ID;
				sessionData.level = (int)currentConfig.skillPair.Level;
				
				sessionData.sessions = new List<SerialSessionMap>();
				sessionData.sessions.Add( subSession );
				
				currentConfig.SerializedData = sessionData;
				
//				Debug.Log( "New serialization: " + currentConfig.SerializedData.toString() );
			}

			/*
			 * This appears to do nothing
			if(restartingSkill && currentConfig.skillPair.Level != SkillLevel.Tutorial) {
				skill.baseTheme.showOverlay();
			}
			*/
		}
		
		public void LoadTheme()
		{
			//add theme to view
			
			skill.baseTheme.addEventListener( ThemeEvent.COMPLETE, OnThemeContinue );
			skill.baseTheme.addEventListener( SkillEvent.SKILL_COMPLETE, OnSkillComplete );
			skill.baseTheme.addEventListener( ThemeEvent.SESSION_COMPLETE, OnSessionComplete );
		}
		
		public void UnloadTheme()
		{
			if(skill != null && skill.baseTheme != null)
			{
				//remove theme from view
				skill.baseTheme.removeEventListener( ThemeEvent.COMPLETE, OnThemeContinue );
				skill.baseTheme.removeEventListener( SkillEvent.SKILL_COMPLETE, OnSkillComplete );
			}
		}
		
		public void UnloadSkill()
		{
			UnloadTheme();
			
			if( !holdBookmark )
				bookmark = null;
			else
				holdBookmark = false;
			
			UnloadNoAvailableSkillsView();
			if(skill != null) 
			{	
				if( skill.parent != null ){
					skill.Dispose();
					view.removeChild(skill);
					view.removeChild(skill.baseTheme);
				}
				
				if(lastSession != null && lastSession.skillPair != null) 
				{
					DebugConsole.LogError("LAST SESSION EXISTS, UNLOADING SWF");
					BuiltinResourceLoader loader = pumpkin.display.MovieClip.rootResourceLoader as BuiltinResourceLoader;
					loader.unloadSWF(lastSession.skillPair.Skill.linkage +".swf");
				}
				
				skill.removeEventListener(SkillEvent.INSTRUCTION_START, OnInstructionStart);
				skill.removeEventListener(SkillEvent.INSTRUCTION_END, OnInstructionEnd);
				skill.removeEventListener(SkillEvent.OPPORTUNITY_COMPLETE, OnOpportunityComplete);
				skill.removeEventListener(SkillEvent.BOOKMARK_NOW, BookmarkCurrent);
				skill.removeEventListener(SkillEvent.END_THEME_REQUEST, TransmitSession);
				skill.removeEventListener(SkillEvent.TIMEOUT_END, Timeout);
				
//				if(lastSession != null && lastSession.skillPair != null /*&& CurrentConfig.skillPair.Skill != lastSession.skillPair.Skill*/)
//				{
//					DebugConsole.LogError("LAST SESSION EXISTS, SKILL PAIR DOESN'T MATCH NEW ONE, UNLOADING BUNDLE");
//					AssetLoader.Instance.UnloadBundle(lastSession.skillPair.Skill.linkage);
//				}
				
				skill = null;
			}
		}
		
		protected void OnOpportunityComplete(CEvent e){
			Debug.Log ("LOG OPPORTUNITY HERE");

			if(tryOrNew != null)
			{
				view.removeChild( tryOrNew );
				tryOrNew.Again = null;
				tryOrNew.Another = null;
				tryOrNew = null;
			}

			if(helpMode)
			{
				skill.nextOpportunity();
				return;
			}
			
			SkillEvent skillEvent = (SkillEvent)e;
			bool correct = skillEvent.Value as string == "1";
			DebugConsole.LogError("******Opportunity was correct: {0}", correct);
			
			if(skill.timeoutEnd)
			{
				Timeout();
				return;
			}
			
			if(correct) {
				currentConfig.correct++;
			} else {
				currentConfig.incorrect++;
			}
			
			if(currentConfig.skillPair.Level == SkillLevel.Tutorial){
			
				if( DateUtils.ConvertToUnixTimestamp( DateTime.UtcNow ) - skill.timeOutPoint < 1 && skillEvent.Value as string == "0" )
					currentConfig.currentSerializedSession.timed = true;
				
				OnSkillComplete(null);
				return;
			}
			
			skill.baseTheme.correctOpportunity( correct );
		}
		
		protected void OnTutorialStart( CEvent e )
		{
			skill.baseTheme.hideOverlay();
		}
		
		protected void OnInstructionStart( CEvent e )
		{
			menu.HelpButtonEnabled(false);
		}
		
		protected void OnInstructionEnd( CEvent e )
		{
			menu.HelpButtonEnabled(!helpMode);
		}

		protected void OnSessionComplete(CEvent e) {
			menu.StopButtonEnabled(false);
		}

		protected void OnSkillComplete(CEvent e) {
			SSGEngine.Instance.StartCoroutine(DoOnSkillComplete());
		}
		
		private IEnumerator DoOnSkillComplete(){
		
			Debug.LogError("SKILL IS COMPLETE");
			
			inGame = false;
			
			if(currentConfig.helpMode) {
				HelpMode = false;
				yield break;
			}
			
			skillCompleted = true;
			
			//menu.hide();
			menu.StopButtonEnabled(true);

			var previous = currentConfig.skillPair;
			float result = currentConfig.Result;

			// INTRO FLAGS
			if( previous.Level == SkillLevel.Tutorial && result >= SkillProgress.ProficiencyThreshold && !currentConfig.sessionResume )
				currentConfig.skipIntro = true;
			else
			{
				currentConfig.skipIntro = false;
				
				skill.Dispose();
				view.removeChild(skill);
				view.removeChild(skill.baseTheme);
				
				yield return new WaitForSeconds(0.25f);
			}
			
			if( currentConfig.teacherMode )
			{
				currentConfig.ResetSessionStats();
				currentConfig.teacherMode = false;
				LoadSession( currentConfig );
				yield break;
			}
			
			yield return SSGEngine.Instance.StartCoroutine( SessionSync( true ) );
			syncNeeded = false;
			
			menu.HelpSelectedThisSession = false;
			
			UnloadSkill();
			// need to give time for GC here
			yield return new WaitForSeconds(0.25f);
			
			Debug.Log ("GOT RESULT FOR SKILL: " + previous.Skill.linkage + ", " + SkillLevelUtil.GetCode(previous.Level) + " - " + (result*100) + "% " + + currentConfig.correct + "/" + currentConfig.totalOpportunities);
			selectedSkill = skillSession.ChooseSkillForResult(previous.Skill, previous.Level, result);
			
			if( previous.Level != SkillLevel.Tutorial && result >= SkillProgress.MasteryThreshold )
			{
				if(tryOrNew != null)
				{
					view.removeChild( tryOrNew );
					tryOrNew.Again = null;
					tryOrNew.Another = null;
					tryOrNew = null;
				}
				
				MovieClipFactory.clearTextureSpecificSkill( currentConfig.skillPair.Skill.linkage );
				AssetLoader.Instance.UnloadBundle(currentConfig.skillPair.Skill.linkage);
				
				yield return Resources.UnloadUnusedAssets();
				
				menu.visible = false;
				
				SSGEngine.Instance.SessionEndMuseum( previous );
			}
			else if( previous.Level == SkillLevel.Tutorial )
			{
				if( result < SkillProgress.ProficiencyThreshold )
				{
					MovieClipFactory.clearTextureSpecificSkill( currentConfig.skillPair.Skill.linkage );
					AssetLoader.Instance.UnloadBundle(currentConfig.skillPair.Skill.linkage);
					
					yield return Resources.UnloadUnusedAssets();
				}
				ContinueSkillComplete();
			}
			else
			{
				if( skillSessionContext.IsLocked( currentConfig.skillPair.Skill ) )
				{
					NewGame();
				}
				else
				{
					tryOrNew = new TryAgainMenu(currentConfig.SPN_FLAG ? "sp" : "en");
					view.addChildAt( tryOrNew, view.getChildIndex( menu ) );
					tryOrNew.Another = NewGame;
					tryOrNew.Again = ReplayGame;
				}
			}
		}
		
		private void TransmitSession( CEvent e )
		{
			syncNeeded = true;
		}
		
		public IEnumerator SessionSync( bool showSync )
		{
			SessionInfo syncConfig = currentConfig.Clone();
			
			syncNeeded = false;
			
			syncConfig.StopTiming();
			bookmark = null;
			syncConfig.ClearSerialization();
			
			DebugConsole.LogError("***** Completed Skill {0} Correct: {1} Incorrect: {2} Percent: " + "{3}", syncConfig.skillPair, syncConfig.correct, syncConfig.incorrect, syncConfig.Result);
			
			// workaround for when opportunites not properly logged
			var previous = syncConfig.skillPair;
			if(syncConfig.correct == 0 && syncConfig.incorrect == 0) {
				DebugConsole.LogError("No opportunities logged for Skill ({0}, {1}). Fix this or the results will be wrong!", previous.Skill, previous.Level);
				syncConfig.incorrect = 1;
			}
			
			bool useDatabase = !syncConfig.demoMode && !syncConfig.teacherMode && !syncConfig.helpMode;
			
			if(useDatabase) {
				databaseObject.InsertSession(syncConfig);
			}
			
			// update progress before sync so that locks are synced immediately
			float result = syncConfig.Result;
			if(!syncConfig.helpMode) {
				skillSession.UpdateProgress(previous.Skill, previous.Level, result);
				
				MuseumDataObject.updateSkill( previous.Skill );
			}
			
			if(useDatabase && Sync != null) {
				yield return SSGEngine.Instance.StartCoroutine( Sync( showSync ) );
			}
		}
		
		private void NewGame() {
			if(tryOrNew != null) view.removeChild( tryOrNew );
			
			MovieClipFactory.clearTextureSpecificSkill( currentConfig.skillPair.Skill.linkage );
			AssetLoader.Instance.UnloadBundle(currentConfig.skillPair.Skill.linkage);
			
			SSGEngine.Instance.StartCoroutine( CleanAndContinue() );
		}
		
		private IEnumerator CleanAndContinue()
		{
			yield return Resources.UnloadUnusedAssets();
			
			ContinueSkillComplete();
		}
		
		private void ReplayGame() {

			view.removeChild( tryOrNew );
			//SSGEngine.Instance.StartCoroutine( SessionSync( true ) );
			skillCompleted = false;
			SkillLevel sl = skillSessionContext.GetLevel( currentConfig.skillPair.Skill );
			currentConfig.skillPair = new SkillLevelPair(currentConfig.skillPair.Skill, sl );
			
			currentConfig.ClearSerialization();
			
			LoadSkill(currentConfig.skillPair);
		}
		
		public void ContinueSkillComplete() {
			if(!skillCompleted){
				DebugConsole.LogError( "ContinueSkillComplete called before OnSkillComplete" );
				return;
			}
			
			menu.visible = true;
			
			skillCompleted = false;
			currentConfig.skillPair = selectedSkill;
			
			currentConfig.ClearSerialization();
			
			if(currentConfig.skillPair != null) {
				LoadSkill(currentConfig.skillPair);
			} else {
				ShowNoAvailableSkillsView();
			}
		}
		
		private void UnloadNoAvailableSkillsView() {
			if(noSkillsView != null) {
				view.removeChild(noSkillsView);
				noSkillsView = null;
			}
		}
		
		public void ShowNoAvailableSkillsView() 
		{
			DoMemoryCleanup();
			
			// TODO: better way of achieving desired behavior
			inGame = true;
			
			UnloadNoAvailableSkillsView();
			if(noSkillsView == null) {
				noSkillsView = MovieClipFactory.CreateNoSkillsAvailable();
				view.addChild(noSkillsView);
			}
		}
		
		protected void OnThemeContinue(CEvent e)
		{
			Debug.Log("SKILL GOING TO NEXT OPPORTUNITY");
			skill.nextOpportunity();
			
			//Capturing bookmark data
			BookmarkCurrent(null);
		}
		
		protected void BookmarkCurrent(CEvent e)
		{
			if(currentConfig.skillPair.Level == SkillLevel.Tutorial)
				return;
			
			currentConfig.initialOpportunity = skill.currentOpportunity;
			currentConfig.sessionData = skill.GetSessionData();
			currentConfig.themeData = skill.GetThemeData();
			
			if(bookmark == null)
				bookmark = new JsonData();
			
			bookmark["user_id"] = currentConfig.user.ID;
			bookmark["skill_id"] = currentConfig.SPN_FLAG ? currentConfig.skillPair.Skill.SPN_ID : currentConfig.skillPair.Skill.ID;
			bookmark["skill_level_id"] = SkillLevelUtil.FromId(currentConfig.skillPair.Level);
			bookmark["theme"] =  "Garden";
			bookmark["questions_answered"] = (currentConfig.correct + currentConfig.incorrect);
			bookmark["questions_correct"] = currentConfig.correct;
			bookmark["questions_incorrect"] = currentConfig.incorrect;
			bookmark["badges"] = "[]";
			bookmark["session_duration"] = (int)currentConfig.currentTime;
			bookmark["themeSer"] = currentConfig.Serialize();
			bookmark["skillSer"] = currentConfig.sessionData + "+" + currentConfig.themeData;
		}
		
		protected void HandleExitTeacherMode()
		{
			if(OnExitTeacherModeInvoker != null) {
				OnExitTeacherModeInvoker();
			}
		}
		
		public void RestartSessionIfNoSkillsAvailable()
		{
			if(inGame && noSkillsView != null && currentConfig != null) {
				LoadSession(currentConfig);
			}
		}
		
		private ConfirmationMenu cm;
		public void Stop()
		{
			if(cm != null || tm != null) return;
			
			cm = MovieClipFactory.CreateConfirmationMenu(currentConfig.SPN_FLAG ? "sp" : "en");
			
			view.addChild(cm);
			cm.CenterInScreen();
			bool helpIsOn = menu.helpBtn.mouseEnabled;
			Pause();
			Time.timeScale = 0;
			
			cm.Reject = () => 
			{
				Resume();
				menu.HelpButtonEnabled( helpIsOn );
			};
			
			cm.Accept = () =>
			{
				SoundEngine.Instance.StopAll();
				Resume();	
				menu.HelpButtonEnabled( false );

				TimerUtils.SetTimeout(0.1f, ContinueWithCancellationStop);
			};		
		}
		
		private TimeoutMenu tm;
		public void Timeout(CEvent e){ Timeout() ;}
		public void Timeout()
		{
			skill.timeoutEnd = false;
			
			if(tm != null || cm != null) return;
			
			tm = MovieClipFactory.CreateTimeoutMenu(currentConfig.user, currentConfig.SPN_FLAG ? "sp" : "en");
			
			view.addChild(tm);
			tm.CenterInScreen();
			Pause();
			Time.timeScale = 0;
			
			tm.Accept = () => 
			{
				SoundEngine.Instance.StopAll();
				Resume();
				menu.HelpButtonEnabled( false );
				currentConfig.incorrect++;
				
				if(currentConfig.skillPair.Level == SkillLevel.Tutorial){
					OnSkillComplete(null);
					return;
				}
				
				if(skill != null && skill.quietContinue)
				{
					skill.nextOpportunity();
					skill.quietContinue = false;
				}
				else
				{
					skill.baseTheme.correctOpportunity(false);
				}
			};
			
			tm.Reject = () =>
			{
				currentConfig.currentSerializedSession.timed = true;
				
				string themeSer = (string)bookmark["themeSer"];// = currentConfig.Serialize();
				themeSer = themeSer.Substring( 0, themeSer.LastIndexOf(";") );
				themeSer += ";" + currentConfig.SerializedJsonToString();
				bookmark["themeSer"] = themeSer;
				
				SoundEngine.Instance.StopAll();
				Resume();	
				menu.HelpButtonEnabled( false );

				TimerUtils.SetTimeout(0.1f, ContinueWithCancellationStop);
			};
		}
		
		private void ContinueWithCancellationStop()
		{
			SoundEngine.Instance.PlayTheme(null);
			if(OnStopInvoker != null) OnStopInvoker();
		}
				
		public void CancelSession()
		{
			
			SoundEngine.Instance.StopAll();

			currentConfig.ResetSessionStats();	
			
			DoMemoryCleanup();
			
			inGame = false;
			view.visible = false;			
						
			
			if(teacherModeBar != null)
			{
				teacherModeBar.sessionConfig.teacherMode = false;
				menu.removeTeacherMode();
				teacherModeBar = null;
			}

			
		}
		
		private void DoMemoryCleanup()
		{
			UnloadSkill();
			UnloadSkillOverlay();
			
			if( currentConfig.skillPair != null && currentConfig.skillPair.Skill != null ){
				MovieClipFactory.clearTextureSpecificSkill( currentConfig.skillPair.Skill.linkage );
				AssetLoader.Instance.UnloadBundle(currentConfig.skillPair.Skill.linkage);
			
				Resources.UnloadUnusedAssets();
			}
			
//			FinalizeTotalMemoryCleanup();
		}
		
		private void FinalizeTotalMemoryCleanup()
		{
			//UniswfTextureMonitor.clearTextureCache();
			
			if(lastSession != null && lastSession.skillPair != null && lastSession.skillPair.Skill != null && lastSession.skillPair.Skill.linkage != null)
			{
				Debug.Log ("UNLOADING BUNDLES FROM LAST SESSION: skill2 : " + lastSession.skillPair.Skill.linkage);
				
				AssetLoader.Instance.UnloadBundle(lastSession.skillPair.Skill.linkage);
			
				Resources.UnloadUnusedAssets();
			}
		}
		
		public void EndSession()
		{
			menu.CancelMonitor();
		}
		
		public void Pause()
		{
			if( menu != null )
				menu.clickOff();
			
			if(SoundEngine.Instance.audio.isPlaying) {
				unpauseOnResume = true;
			} else {
				unpauseOnResume = false;
			}

			SoundEngine.Instance.SendPauseToken ();

			if(currentConfig != null) {
				currentConfig.StopTiming();
			}
			if(skill != null) {
				skill.Pause();
			}
		}
		
		public void Resume()
		{
			if( menu != null )
				menu.clickOn();
			
			if (cm != null) {
				view.removeChild(cm);
				cm.Unload();
				cm = null;
			}
			if (tm != null) {
				view.removeChild(tm);
				tm.Unload();
				tm = null;
			}
			Time.timeScale = 1;
			
			SoundEngine.Instance.SendUnPauseToken ();
			if (!unpauseOnResume) {
				SoundEngine.Instance.StopAll();
			}

			if(currentConfig != null) {
				currentConfig.StartTiming();
			}
			if(skill != null) {
				skill.Resume();
			}
		}
		
	}
}
