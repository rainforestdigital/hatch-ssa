using System;
using UnityEngine;
using HatchFramework;

namespace SSGCore
{
	public class FiltersFactory
	{
		public static GlowMovieClipFilter GetYellowGlowFilter()
		{		
			return new GlowMovieClipFilter(50, 2, 0.05f, Color.yellow, 1);
		}
		
		public static GlowMovieClipFilter GetScaledYellowGlowFilter()
		{		
			return new GlowMovieClipFilter(50, 2, 0.05f, Color.yellow, 1.05f);
		}
	}
}

