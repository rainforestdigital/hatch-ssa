using System;
using HatchFramework;
using UnityEngine;
using System.Collections;
using System.IO;

namespace SSGCore
{
	public static class ApplicationUpdater
	{
		public static void OpenApplicationUpdater(UpdatesDataObject updates, UpdateView updateView)
		{

#if UNITY_ANDROID
			//Application.OpenURL ("http://play.google.com/store/apps/details?id=com.hatch.ShellSquadGames");
			SSGEngine.Instance.StartCoroutine (DownloadAndroidUpdate(updates, updateView));
#elif UNITY_IPHONE
//			Application.OpenURL ("https://itunes.apple.com/us/app/shell-squad-games-by-hatch/id703522013?ls=1&mt=8");
//			SSGEngine.Instance.Quit();
#else
			Screen.fullScreen = false; // This is required to address a crash on windows ("cannot change resolution to 410x120")
        
			string patchSettingsFile = "Patcher_Data/patchsettings.txt";
			string patcherUrl = GlobalConfig.GetProperty<string>("WindowsUpdaterUrl");
			if(File.Exists(patchSettingsFile) && patcherUrl != null) {
				string[] lines = File.ReadAllLines(patchSettingsFile);
				for(int i = 0; i < lines.Length; ++i) {
					if(lines[i].StartsWith("universalPatchInfo=")) {
						lines[i] = "universalPatchInfo=" + patcherUrl;
					}
				}
				File.WriteAllLines(patchSettingsFile, lines);
			}
			
	        //Start patcher (same directory)
			
			//Except form M2HPatcher_Runtime
	        try
	        {
	            System.Diagnostics.Process Proc = new System.Diagnostics.Process();
	            Proc.StartInfo.FileName = "Patcher.exe";
				Proc.StartInfo.UseShellExecute = false;
	            Proc.Start();
	        }
	        catch (System.Exception e)
	        {
	            DebugConsole.LogError("Could not QuitAndStartPatcher. The patch client could not be found or has no execution permission.\n\n" +e);
	        }
			
	        SSGEngine.Instance.Quit();
#endif
		}

#if UNITY_ANDROID

	

		static IEnumerator DownloadAndroidUpdate(UpdatesDataObject updates, UpdateView updateView){
			string file = "HatchSSG-"+updates.applicationVersion.ToString()+".apk";
			string path = GlobalConfig.GetProperty ("RMS_URL")+"update/"+file;
			Debug.Log ("FILE NAME IS: " + path);

			updateView.ShowProgress ();
			WWW www = new WWW (path);


			while (!www.isDone) {
				updateView.SetProgress (www.progress);
				yield return new WaitForSeconds (0.15f);
			}

			Debug.Log ("Got the www: " + www.error);
			if(!string.IsNullOrEmpty(www.error)) {
				updateView.ShowFailure();
				yield break;
			}


			string localPath = Path.Combine ("/mnt/sdcard/", GlobalConfig.GetProperty ("LOCAL_DIR"));
			string filePath = localPath + "/SSG.apk";

			if (Application.isEditor) {
				File.WriteAllBytes (Application.persistentDataPath + file, www.bytes);
			} else {


				Debug.Log ("Setting local path: " + localPath);
				if (!Directory.Exists (localPath)) {
					Directory.CreateDirectory (localPath);
				}
				File.WriteAllBytes (filePath, www.bytes);
				
				
				AndroidJavaClass classPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

				AndroidJavaObject objActivity = classPlayer.GetStatic<AndroidJavaObject>("currentActivity");
				
				AndroidJavaObject secondIntent = null;
				
				
				//looks for the KidSpaceApp - if it's running, it gets killed, then an intent is written up to re-open it
				//if that intent isn't still empty, it's sent to the intent queue behind the data manipulator
				string KSPackage = "com.st2i.android.hatch.kidspace";
				AndroidJavaObject activityManager = objActivity.Call<AndroidJavaObject>( "getSystemService", "activity" );
				AndroidJavaObject processList = activityManager.Call<AndroidJavaObject>( "getRunningAppProcesses" );
				
				int loopMax = processList.Call<int>( "size" );
				for( int i = 0; i < loopMax; i++ )
				{
					AndroidJavaObject processInfo = processList.Call<AndroidJavaObject>( "get", i );
					string pName = processInfo.Get<string>( "processName" );
					
					if( pName.ToLower() == KSPackage.ToLower() )
					{
						activityManager.Call( "killBackgroundProcesses", KSPackage );
						secondIntent = objActivity.Call<AndroidJavaObject>( "getPackageManager" ).Call<AndroidJavaObject>( "getLaunchIntentForPackage", KSPackage );
						break;
					}
				}
				

				Debug.Log("**create Uri class");

				AndroidJavaClass classUri = new AndroidJavaClass("android.net.Uri");

				Debug.Log("**create Intent object");

				AndroidJavaObject objIntent = new AndroidJavaObject("android.content.Intent", "android.intent.action.VIEW");
				objIntent.Call<AndroidJavaObject>("setDataAndType", classUri.CallStatic<AndroidJavaObject>("parse", "file://" + filePath), "application/vnd.android.package-archive");
				objIntent.Call <AndroidJavaObject>("addFlags", 268435456); //Intent.FLAG_ACTIVITY_NEW_TASK

				objActivity.Call ("startActivity", objIntent);
				
				
				if( secondIntent != null )
				{
					objActivity.Call( "startActivity", secondIntent );
				}
			}
		}
#endif
	}
}

