using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using pumpkin.display;
using pumpkin;
using pumpkin.events;
using pumpkin.text;

public class SWFViewer : MonoBehaviour
{
	private static SWFViewer instance;
	public static SWFViewer Instance
	{
		get
		{
			return instance;
		}

		set
		{
			instance = value;
		}
	}
	
	public Dictionary<string, DisplayObjectContainer> clips;
	public List<DisplayObjectContainer> childList;
	
	public void Awake()
	{
		instance = this;
	}

	public void Start ()
	{
		clips = new Dictionary<string, DisplayObjectContainer>();
	}
	
	public void AddClip( string name, DisplayObjectContainer mc)
	{
		if( !clips.ContainsValue(mc) && !clips.ContainsKey(name)) clips.Add(name, mc);
	}
	
	private void SetChildList(DisplayObjectContainer mc)
	{

		currentClip = mc;
		
		childList = new List<DisplayObjectContainer>();
		for(int i = 0; i < mc.numChildren; i++)
		{
			MovieClip m = mc.getChildAt<MovieClip>(i);
			if( m != null )	childList.Add(m);
		}
	}
	
	public void OnGUI()
	{
		windowRect = GUILayout.Window(0, windowRect, DoWindow, "SWFViewer",  GUILayout.Width(500), GUILayout.Height(windowHeight));
	}
	
	private int windowHeight = 500;
	private DisplayObjectContainer currentClip;

	private Vector2 scrollPos = Vector2.zero;
	private Rect windowRect = new Rect(5, 200, 500, 500);
	private bool isVisible = true;
	private bool isPaused = false;
	public void DoWindow(int windowID)
	{		
		GUILayout.BeginHorizontal();
		if( GUILayout.Button("Hide/Show") )
		{
			isVisible = !isVisible;
		}
		if( GUILayout.Button("Pause") )
		{
			isPaused = !isPaused;
			if( isPaused ) Time.timeScale = 0;
			else Time.timeScale = 1;
		}
		GUILayout.EndHorizontal();
		
		windowHeight = 500;
		
		if( !isVisible ) 
		{
			windowHeight = 75;
			return;
		}
		
		// clear out buttons that have gone away
		clips = (from kv in clips
  	  	where kv.Value.parent != null && kv.Value.parent != null
    	select kv).ToDictionary(kv => kv.Key, kv => kv.Value);
		
		GUILayout.BeginHorizontal();		
		foreach(KeyValuePair<string, DisplayObjectContainer> pair in clips )
		{
			if( GUILayout.Button(pair.Key) )
			{
				SetChildList(pair.Value);
			}
		}
		GUILayout.EndHorizontal();
		
		if( currentClip == null ) return;
		
		childList.RemoveAll(t => t == null);
		
		scrollPos = GUILayout.BeginScrollView(scrollPos);
		if( currentClip.parent != null ) 
		{
			if( GUILayout.Button("...") )
			{
				SetChildList(currentClip.parent);
			}
		}
		
		string name = string.IsNullOrEmpty(currentClip.name) ? currentClip.ToString() : currentClip.name;
		GUILayout.Label("SELF: (x:" + currentClip.x + ", y:" + currentClip.y + ", w: " + currentClip.width + ", h: " + currentClip.height +", a:" + currentClip.alpha +", scaleX:" + currentClip.scaleX + ", scaleY:" + currentClip.scaleY + ")", GUILayout.Width(300));
		
		foreach(MovieClip mc in childList )
		{				
			GUILayout.BeginHorizontal();
			name = string.IsNullOrEmpty(mc.name) ? mc.ToString() : mc.name;
			GUILayout.Label(name + ": (x:" + mc.x + ", y:" + mc.y + ", w: " + mc.width + ", h: " + mc.height +", a:" + mc.alpha +", scaleX:" + mc.scaleX + ", scaleY:" + mc.scaleY + ")", GUILayout.Width(300));
			
			GUILayout.Space(3);
			mc.visible = GUILayout.Toggle(mc.visible, "visible");
			if( GUILayout.Button("...") )
			{
				SetChildList(mc);
			}
			GUILayout.EndHorizontal();
		}
		GUILayout.EndScrollView();
	}
}
