using System;
using UnityEngine;

using HatchFramework;

using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;

namespace SSGCore
{
	public class Base3dCharacter : DisplayObjectContainer
	{
		private const int FRAMES_UNTIL_DISPOSE = 1;

		public Camera cam;
		public CharacterAnimation animController;
		public bool loops;
		private Sprite target;
		private RenderTexture camTarget;
		private Material camMaterial;
		private int frameCount = 0;

		public float camSize {
			set{
				if( PlatformUtils.GetClosestRatio() == ScreenRatio.FOURxTHREE )
					value *= 5f/4f;
				cam.orthographicSize = value;
			}
		}
		public Rect camTarg {
			set{	
				cam.transform.position += Vector3.left * ( value.x * cam.orthographicSize );
				cam.transform.position += Vector3.up * ( value.y * cam.orthographicSize );
				target.scaleX = value.width;
				target.scaleY = value.height;
			}
		}
		
		public Base3dCharacter ()
		{
			Init( (GameObject)Resources.Load( "Characters/Henry/Henry01" ), new Vector3(0, 500, 0), 0 );
		}
		
		public Base3dCharacter ( GameObject animationSRC )
		{
			Init( animationSRC, new Vector3(0, 500, 0), 0 );
		}
		
		public Base3dCharacter ( GameObject animationSRC, float zPos )
		{
			Init( animationSRC, new Vector3(0, 500, 0), zPos );
		}
		
		public Base3dCharacter ( GameObject animationSRC, Vector3 pos )
		{
			Init( animationSRC, pos, 0 );
		}
		
		public Base3dCharacter ( GameObject animationSRC, Vector3 pos, float zPos )
		{
			Init( animationSRC, pos, zPos );
		}
		
		private void Init( GameObject animationSRC, Vector3 pos, float zPos )
		{
			GameObject obj = new GameObject( "animCamera" );
			cam = obj.AddComponent<Camera>();
			
			cam.orthographic = true;

			if( PlatformUtils.GetClosestRatio() == ScreenRatio.FOURxTHREE )
				cam.orthographicSize = 17.5f;
			else
				cam.orthographicSize = 15f;

			cam.transform.position = pos;
			cam.clearFlags = CameraClearFlags.Color;
			cam.backgroundColor = Color.clear;
			cam.renderingPath = RenderingPath.VertexLit;
			cam.useOcclusionCulling = false;
			
			animController = obj.AddComponent<CharacterAnimation>();
			animController.pos = new Vector3( pos.x, pos.y, zPos );
			animController.SetAnimation = animationSRC;
			animController.AnimFinished += animationFinished;
			loops = false;
			
			camTarget = new RenderTexture( Screen.width, Screen.height, 1 );
			cam.targetTexture = camTarget;
			
			camMaterial = new Material( Shader.Find("Sprites/Default") );
			camMaterial.mainTexture = camTarget;
			
			target = new Sprite();
			target.mouseEnabled = false;
			target.graphics.drawRectUV( camMaterial, new Rect(0,0,1,1), new Rect(0,0,Screen.width, Screen.height) );
			addChild( target );
		}
		
		public void Pause()
		{
			foreach( AnimationState state in animController.animActive.animation )
			{
				state.speed = 0f;
			}
		}
		
		public void Resume()
		{
			foreach( AnimationState state in animController.animActive.animation )
			{
				state.speed = 1f;
			}
		}

		public void DisposeImmediately()
		{
			doDispose ();
		}

		public void Dispose()
		{
			frameCount = 0;
			this.removeAllEventListeners( CEvent.COMPLETE );
			addEventListener (CEvent.ENTER_FRAME, frameHandler);
			TimerUtils.SetTimeout( 0.05f, () => { if( frameCount == 0 ){ frameCount = FRAMES_UNTIL_DISPOSE; frameHandler(null); } } );
		}

		private void frameHandler(CEvent e) {		
			frameCount++;		
			if (frameCount >= FRAMES_UNTIL_DISPOSE) {
				removeEventListener(CEvent.ENTER_FRAME, frameHandler);
				doDispose();
			}
		}

		private void doDispose() {
			if (target != null) {
				target.graphics.clear ();
				removeChild (target);
				target = null;
			}
			
			if(camMaterial != null) {
				UnityEngine.Object.Destroy(camMaterial);
			}
			
			if(camTarget != null) {
				UnityEngine.Object.Destroy(camTarget);
			}
			
			if (cam != null)
				UnityEngine.Object.Destroy (cam.gameObject);
			
			if (parent != null)
				parent.removeChild (this);
			
			if( animController != null )
			{
				animController.OutofTheWay();
				TimerUtils.SetTimeout (0.1f, () => { animController.Dispose (); });
			}
		}

		private void animationFinished()
		{
			if( loops )
				animController.Replay();
			
			dispatchEvent( new CEvent( CEvent.COMPLETE, true, false ) );
		}
	}
}
