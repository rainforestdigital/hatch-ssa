using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using pumpkin.display;
using pumpkin;
using pumpkin.events;
using pumpkin.text;

namespace SSGCore
{
	public delegate void OnFrame(string eventName);
	
	public class BaseCharacter:MovieClip
	{
		public const string IDLE = "IDLE";
		public const string LOOK_RIGHT = "LOOK RIGHT";
		public const string LOOK_LEFT = "LOOK LEFT";
		public const string HAPPY = "HAPPY";
		public const string SAD = "SAD";
		public const string RIGHT_TALKING = "RIGHT TALKING";
		public const string RIGHT_TALKING_LOOP = "RIGHT TALKING LOOP";
		public const string RIGHT_TALKING_END = "RIGHT TALKING END";
		public const string LEFT_TALKING = "LEFT TALKING";	
		public const string LEFT_TALKING_LOOP = "LEFT TALKING LOOP";
		public const string LEFT_TALKING_END = "LEFT TALKING END";	
		public const string PAYOFF = "PAYOFF";	
		public const string PAYOFF_END = "PAYOFF END";	
		public const string RESTING = "RESTING";
		public const string TALKING = "TALKING";
		
		public string[] COMP;
		public string[] RETRY;
		
		OnFrame OnFrameInvoker;
		public event OnFrame OnFrameEvent{ add{OnFrameInvoker += value;} remove{OnFrameInvoker -= value;} }
		
		public Dictionary<string, int> animationEndFrames;
		
		public bool isAnimationSetToLoop = false;
		public int animationEndFrame = 1;
		
		protected string lastAnimation = "";
		protected string currentAnimation = "";
		public string CurrentAnimation
		{
			get
			{
				return currentAnimation;
			}
			
			set
			{
				lastAnimation = currentAnimation;
				currentAnimation = value;
			}
		}
		
		public BaseCharacter(string linkage):base(linkage)
		{		
			Initialize();
		}
			
		public BaseCharacter(string swf, string symbol):base(swf, symbol)
		{
			Initialize();
		}
		
		virtual public void Initialize()
		{			
			addEventListener("OnLookRightComplete", HandleFrameEvent);
			addEventListener("OnLookLeftComplete", HandleFrameEvent);
			addEventListener("OnHappyComplete", HandleFrameEvent);
			addEventListener("OnSadComplete", HandleFrameEvent);
			addEventListener("OnTalkComplete", HandleFrameEvent);
			addEventListener("OnRightTalkingLoopComplete", HandleFrameEvent);
			addEventListener("OnRightTalkingEndComplete", HandleFrameEvent);
			addEventListener("OnLeftTalkingLoopComplete", HandleFrameEvent);
			addEventListener("OnLeftTalkingEndComplete", HandleFrameEvent);
			addEventListener("OnPayoffComplete", HandleFrameEvent);
			addEventListener("OnPayoffEndComplete", HandleFrameEvent);
			addEventListener("OnRestingComplete", HandleFrameEvent);
			
			GoToAndStopAtAnimation(IDLE);
		}
		
		virtual public void PlayAnimation(string animation)
		{
			PlayAnimation(animation, false);
		}
		
		virtual public void PlayAnimation(string animation, bool loop)
		{
			CurrentAnimation = animation;
			isAnimationSetToLoop = loop;
			animationEndFrame = animationEndFrames[animation];
			gotoAndPlay(animation);
		}
		
		virtual protected void HandleFrameEvent(CEvent e)
		{
			Debug.Log("RECEIVED FRAME EVENT: " + e.type);
			if( OnFrameInvoker != null ) OnFrameInvoker(e.type);
		}
		
		virtual public void GoToAndStopAtAnimation(string animation)
		{
			gotoAndStop(animation);
		}
		
		virtual public string GetCharName()
		{
			return "";
		}
	}
}