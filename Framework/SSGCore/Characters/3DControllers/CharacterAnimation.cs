using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace SSGCore
{
	public class CharacterAnimation : MonoBehaviour
	{
		public GameObject SetAnimation { set{ animStored = value; } }
		private GameObject animStored;
		public GameObject animActive;
		private Dictionary<Transform, Renderer> visibilityPairs;
		private bool isPlayCalled;
		public Vector3 pos;
		public ICharacterEffect Effect = null;
		
		Action AnimStartInvoker;
		public event Action AnimStart{ add{ AnimStartInvoker += value; } remove{ AnimStartInvoker -= value; } }
		Action AnimFinishedInvoker;
		public event Action AnimFinished{ add{AnimFinishedInvoker += value;} remove{AnimFinishedInvoker -= value;}}
		
		// Update is called once per frame
		public void Update ()
		{
			if( animStored != null && animActive == null )
			{
				Init ();
			}
			
			//deal with visibility
			setVisibility();
			
			if( isPlayCalled && AnimStartInvoker != null && animActive.animation.isPlaying )
			{
				AnimStartInvoker();
				
				AnimStartInvoker = null;
			}
			
			if( isPlayCalled && !animActive.animation.isPlaying )
			{
				isPlayCalled = false;
				
				if( AnimFinishedInvoker != null )
				{
					AnimFinishedInvoker();
				}
			}
		}
		
		public void Replay()
		{
			animActive.animation.Play();
			isPlayCalled = true;
		}
		public void OutofTheWay()
		{
			pos.x += 20000;
			pos.y += 20000;
			pos.z += 20000;
			
			animActive.transform.position = pos;
		}
		
		public void Dispose()
		{
			if( visibilityPairs != null )
				visibilityPairs.Clear();
			visibilityPairs = null;
			
			if( animActive != null )
				Destroy( animActive );
			
			animStored = null;
			
			AnimFinishedInvoker = null;
		}
		
		private void Init()
		{			
			animActive = (GameObject)Instantiate(animStored);
			
			animActive.transform.position = pos;
			animActive.transform.Rotate( 0f, 180f, 0f );
			animActive.animation.cullingType = AnimationCullingType.AlwaysAnimate;
			
			Transform geoBase = animActive.transform.FindChild( "geo" );
			
			//make array of children
			int i = 0;
			List<Transform> geoChildren = GetAllChildren( geoBase );
			
			visibilityPairs = new Dictionary<Transform, Renderer>();
			List<Renderer> unpairedRend = new List<Renderer>();
			List<Transform> unpairedVis = new List<Transform>();
			
			//loop until the array is clear
			Transform trans = null;
			Renderer rend = null;
			string nameMatch;			
			while( geoChildren.Count > 0 )
			{
				//pull out a transform
				//determine if it's a vis controller or rendered object
				//store name
				trans = geoChildren[0];
				geoChildren.RemoveAt(0);
				nameMatch = trans.name;
				if( !isVis( nameMatch ) )
				{
					rend = trans.renderer;
					trans = null;
				}
				
				//loop through array
				for( i = 0; i < geoChildren.Count; i++ )
				{
					//pull out item whose name matches stored name
					if( rend == null && nameMatches( nameMatch, geoChildren[i].name ) )
					{
						rend = geoChildren[i].renderer;
					}
					else if( trans == null && nameMatches( nameMatch, geoChildren[i].name ) )
					{
						trans = geoChildren[i];
					}
					
					//add to dictionary as key/value pair
					if( rend != null && trans != null )
					{
						visibilityPairs[trans] = rend;
						geoChildren.RemoveAt(i);
						break;
					}
					
					if( i == geoChildren.Count - 1 )
					{
						if( rend == null ) unpairedVis.Add(trans); 
						else unpairedRend.Add(rend);
					}
				}
				trans = null;
				rend = null;
			}
			while( unpairedRend.Count > 0 && unpairedVis.Count > 0 )
			{
				rend = unpairedRend[0];
				unpairedRend.RemoveAt(0);
				for( i = 0; i < unpairedVis.Count; i++ )
				{
					if( nameMatches( unpairedVis[i].name, rend.name, true ) )
					{
						visibilityPairs[ unpairedVis[i] ] = rend;
						unpairedVis.RemoveAt(i);
						break;
					}
				}
			}
			
			setVisibility();
			
			isPlayCalled = true;
		}
	
		private List<Transform> GetAllChildren( Transform targ )
		{
			List<Transform> retList = new List<Transform>();
			
			if( targ.childCount > 0 )
			{
				for( int i = 0; i < targ.childCount; i++ )
				{
					retList.AddRange( GetAllChildren(targ.GetChild( i )) );
				}
			}
			else
				retList.Add( targ );
			
			return retList;
		}
		
		private bool nameMatches( string orig, string comp )
		{
			return nameMatches( orig, comp, false );
		}
		private bool nameMatches( string orig, string comp, bool brute )
		{
			if( isVis(orig) && isVis(comp) )
				return false;
		
			if( orig.Contains( "_geo" ) ) orig = orig.Replace( "_geo", "");
			else if( orig.Contains( "geo_" ) ) orig = orig.Replace( "geo_", "");
			if( comp.Contains( "_geo" ) ) comp = comp.Replace( "_geo", "");
			else if( comp.Contains( "geo_" ) ) comp = comp.Replace( "geo_", "");
			
			if( orig.IndexOf( "_visibility" ) > -1 ) orig = orig.Split( new string[]{"_visibility"}, System.StringSplitOptions.RemoveEmptyEntries )[0];
			else if( comp.IndexOf( "_visibility" ) > -1 ) comp = comp.Split( new string[]{"_visibility"}, System.StringSplitOptions.RemoveEmptyEntries )[0];
			
			if( orig == comp ) return true;
			
			if( brute && ( orig.Contains( comp ) || comp.Contains( orig ) ) )
				return true;
			
			return false;
		}
		
		private bool isVis( string s )
		{
			string visMark = "visibility";
			if( s.Contains( visMark ) )
				return true;
			else
				return false;
		}
		
		private void setVisibility()
		{
			if( animActive == null || visibilityPairs == null )
				return;
				
			foreach( KeyValuePair< Transform, Renderer > item in visibilityPairs )
			{
				if( item.Key.localPosition.y > 0 )
				{
					item.Value.enabled = true;
				}
				else
				{
					item.Value.enabled = false;
				}
			}
		}

		public void OnRenderImage(RenderTexture sourceTexture, RenderTexture destTexture) {
			if (Effect != null) {
				Effect.Update(Time.deltaTime);
				Effect.Render (sourceTexture, destTexture);
			} else {
				UnityEngine.Graphics.Blit (sourceTexture, destTexture);
			}
		}
	}
}

