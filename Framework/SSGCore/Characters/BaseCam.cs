using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SSGCore
{
	public class BaseCam : BaseCharacter
	{
				
		public BaseCam(string linkage):base(linkage)
		{		
			Initialize();
		}
			
		public BaseCam(string swf, string symbol):base(swf, symbol)
		{
			Initialize();
		}
		
		public override void Initialize ()
		{
			animationEndFrames = new Dictionary<string, int>();
			animationEndFrames.Add(LOOK_RIGHT, 59);
			animationEndFrames.Add(LOOK_LEFT, 144);
			animationEndFrames.Add(HAPPY, 230);
			animationEndFrames.Add(SAD, 289);
			animationEndFrames.Add(RIGHT_TALKING, 805); // MAKING IT THE SAME AS THE LOOP ASSUMING THEY WANT TO ANIMATE INTO THE TALKING
			animationEndFrames.Add(RIGHT_TALKING_LOOP, 805);
			animationEndFrames.Add(RIGHT_TALKING_END, 859);
			animationEndFrames.Add(LEFT_TALKING, 977);
			animationEndFrames.Add(LEFT_TALKING_LOOP, 977);
			animationEndFrames.Add(LEFT_TALKING_END, 1000);
			animationEndFrames.Add(PAYOFF, 459);
			animationEndFrames.Add(PAYOFF_END, 579);
			animationEndFrames.Add(RESTING, 1160);
			animationEndFrames.Add(TALKING, 680);
			
			COMP = new string[]{"COMP-a-round-of-applause",
								"COMP-give-yourself-a-big-hug",
								"COMP-clap-your-hands",
								"COMP-excellent",
								"COMP-fab-you-you-are-fabulous",
								"COMP-fabulous-effort",
								"COMP-fabulous-fabulous",
								"COMP-fantastic",
								"COMP-fantastic-you-are-at-the-top",
								"COMP-give-a-silent-cheer",
								"COMP-good-job",
								"COMP-great-job-will-you-be-my-friend",
								"COMP-hooray-hooray-you-made-my-day",
								"COMP-jump-high-for-joy",
								"COMP-kiss-your-brain",
								"COMP-knee-slappin-good-job",
								"COMP-look-at-you-you-are-the-best",
								"COMP-outstanding",
								"COMP-pat-your-self-on-the-back",
								"COMP-shake-shake-you-take-the-cake",
								"COMP-spirit-fingers",
								"COMP-super-job",
								"COMP-super-super",
								"COMP-superior",
								"COMP-three-cheers-for-you",
								"COMP-we-are-having-fun-you-are-my-partner",
								"COMP-wow-did-you-see-what-you-did-wow",
								"COMP-you-are-tops",
								"COMP-you-did-it",
								"COMP-you-make-me-smile"};
			
			RETRY = new string[]{"RTRY-give-it-another-try",
								 "RTRY-i-want-to-help-you-try-again",
							 	 "RTRY-lets-keep-going-one-more-try",
							 	 "RTRY-lets-look-again",
								 "RTRY-lets-think-harder",
								 "RTRY-lets-try-again",
								 "RTRY-put-your-thinking-cap-on",
								 "RTRY-think-one-more-time",
								 "RTRY-try-try-again",
								 "RTRY-you-were-close-lets-try-again"};
			
			base.Initialize ();
		}
		
		public override string GetCharName()
		{
			return "Cami";
		}
	}
}
