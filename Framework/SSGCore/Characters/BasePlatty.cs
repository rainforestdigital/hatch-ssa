using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SSGCore
{
	public class BasePlatty : BaseCharacter
	{
		
		public BasePlatty(string linkage):base(linkage)
		{		
			Initialize();
		}
			
		public BasePlatty(string swf, string symbol):base(swf, symbol)
		{
			Initialize();
		}
		
		public override void Initialize ()
		{
			animationEndFrames = new Dictionary<string, int>();
			animationEndFrames.Add(LOOK_RIGHT, 51);
			animationEndFrames.Add(LOOK_LEFT, 120);
			animationEndFrames.Add(HAPPY, 225);
			animationEndFrames.Add(SAD, 289);
			animationEndFrames.Add(RIGHT_TALKING, 504); // MAKING IT THE SAME AS THE LOOP ASSUMING THEY WANT TO ANIMATE INTO THE TALKING
			animationEndFrames.Add(RIGHT_TALKING_LOOP, 504);
			animationEndFrames.Add(RIGHT_TALKING_END, 519);
			animationEndFrames.Add(LEFT_TALKING, 609);
			animationEndFrames.Add(LEFT_TALKING_LOOP, 609);
			animationEndFrames.Add(LEFT_TALKING_END, 639);
			animationEndFrames.Add(PAYOFF, 795);
			animationEndFrames.Add(PAYOFF_END, 895);
			animationEndFrames.Add(RESTING, 1060);
			animationEndFrames.Add(TALKING, 362);
			
			COMP = new string[]{"COMP-a-round-of-applause",
								"COMP-big-hug",
								"COMP-clap-your-hands-stomp-your-feet",
								"COMP-excellent",
								"COMP-fab-you-you-are-fabulous",
								"COMP-fabulous-effort",
								"COMP-fabulous-fabulous",
								"COMP-fantastic",
								"COMP-fantastic-you-are-at-the-top",
								"COMP-give-a-silent-cheer",
								"COMP-good-job",
								"COMP-great-job-will-you-be-my-friend",
								"COMP-hooray-you-made-my-day",
								"COMP-jump-high-for-joy",
								"COMP-kiss-your-brain",
								"COMP-knee-slappin",
								"COMP-look-at-you-you-are-the-best",
								"COMP-outstanding",
								"COMP-pat-yourself-on-the-back",
								"COMP-shake-shake-you-take-the-cake",
								"COMP-spirit-fingers",
								"COMP-super-job",
								"COMP-super-super",
								"COMP-superior",
								"COMP-three-cheers-for-you",
								"COMP-we-are-having-fun-you-are-my-partner",
								"COMP-wow-did-you-see-what-you-did-wow",
								"COMP-you-are-tops-please-dont-stop",
								"COMP-you-did-it",
								"COMP-you-make-me-smile"};
			
			RETRY = new string[]{"RTRY-give-it-another-try",
								 "RTRY-i-want-to-help-you-try-again",
							 	 "RTRY-lets-keep-going-one-more-try",
							 	 "RTRY-lets-look-again",
								 "RTRY-lets-think-harder",
								 "RTRY-lets-try-again",
								 "RTRY-put-your-thinking-cap-on",
								 "RTRY-think-one-more-time",
								 "RTRY-try-try-again",
								 "RTRY-you-were-close-lets-try-again"};
			
			base.Initialize ();
		}
		
		public override string GetCharName()
		{
			return "Platty";
		}
	}
}
