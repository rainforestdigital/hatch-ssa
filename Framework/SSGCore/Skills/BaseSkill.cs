using System;
using System.Collections.Generic;
using UnityEngine;
using pumpkin.display;
using pumpkin.events;
using HatchFramework;

using LitJson;

namespace SSGCore
{
	
	
	public class BaseSkill : DisplayObjectContainer
	{
		
		protected const string INTRO = "INTRO";
		protected const string PLEASE_FIND = "PLEASE_FIND";
		protected const string NOW_ITS = "NOW_ITS";
		protected const string NOW_LETS = "NOW_LETS";
		protected const string YOU_FOUND = "YOU_FOUND";
		protected const string YOU_FOUND_INCORRECT = "YOU_FOUND_INCORRECT";
		protected const string THIS_IS = "THIS_IS";
		protected const string THEME_COMPLIMENT = "THEME_COMPLIMENT";
		protected const string THEME_CRITICISM = "THEME_CRITICISM";
		protected const string THEME_INTRO = "THEME_INTRO";
		protected const string CLICK_CORRECT = "CLICK_CORRECT";
		protected const string CLICK_INCORRECT = "CLICK_INCORRECT";
		protected const string TRY_AGAIN = "TRY_AGAIN";
		
		protected SkillLevel _level;
		public SkillLevel Level
		{
			get
			{
				return _level;
			}
		}
		
		//Shared Properties
		//protected int _totalOpportunities = 10;
		protected int _currentOpportunity = 0;
		private bool _complete = false;
		protected bool opportunityComplete = false;
		protected bool opportunityCorrect = false;
		protected bool opportunityAnswered = false;
		protected bool resumingSkill = false;
		
		//protected float _percent;
		protected Tutorial tutorial_mc;
		protected int totalTime = 5;
		protected float timer;
		protected int timeoutCount = 0;
		public bool timeoutEnd = false;
		public bool quietContinue = false;
		//private bool shouldRunTimer;
		
		public SkillThemeController baseTheme;
		public string BACKGROUND = "BACKGROUND";
		public string ARTIFACT = "ARTIFACT";
		
		private bool resumeTimer = false;
		public double timeOutPoint = 0;

		protected float Percent{
			get{
				return  Mathf.Round(((float)NumCorrect / ((float)NumCorrect + (float)NumIncorrect)) * 100);
			}
		}
		
		protected int totalOpportunities{
			get{ return currentSession.totalOpportunities;}
			set{ currentSession.totalOpportunities = value;}
		}
		
		protected int NumCorrect{
			get{return currentSession.correct;}
			//set{ currentSession.correct = value;}
		}
		protected int NumIncorrect{
			get{return currentSession.incorrect;}
			//set{ currentSession.incorrect = value;}
		}
	
		
		protected SessionInfo currentSession;
		public SessionInfo CurrentSession{ get{ return currentSession; } }
		
		public string skillName{ get{ return currentSession.skillPair.Skill.linkage; } }
		
		protected string host = "Platty";
		public string Host
		{
			get {return host;}
		}
		
		public bool Resuming
		{
			get { return resumingSkill; }
		}
		
		//GETTER/SETTERS
		public int currentOpportunity
		{
			get{ return _currentOpportunity; }
			protected set
			{
				_currentOpportunity = value;
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_CHANGE, true, false) );
			}
		}
		
		protected MovieClip parentClip;
		public BaseSkill (SkillLevel level, SessionInfo currentSession) : base()
		{
			_level = level;
			this.currentSession = currentSession;
			host = hostForSkill( currentSession.skillPair.Skill.linkage );
			if(currentSession.initialOpportunity > 0) {
				resumingSkill = true;
				if(_level != SkillLevel.Tutorial)
					_currentOpportunity = currentSession.initialOpportunity - 1;
				else
					currentOpportunity = 0;
				DebugConsole.Log("Resuming skill at opportunity {0}", _currentOpportunity);
			}
			baseTheme = new SkillThemeController( this );
		}
		
		public BaseSkill(string linkage):base(){
			
		}
		
		public virtual void Init( MovieClip parent ){
			
			this.parentClip = parent;
			
			if( !String.IsNullOrEmpty( currentSession.themeData ) )
				baseTheme.parseSessionData( currentSession.themeData );

			baseTheme.Init();
		}
		
		public virtual void Start()
		{
			MarkInstructionStart();
		}
		
		public virtual void Resize( int width, int height )
		{
			
		}
		
		/// <summary>
		/// Gets the session data for the current session
		/// </summary>
		/// <returns>
		/// The session data as a stringified json object
		/// </returns>
		public virtual string GetSessionData(){
			return "";
		}
		
		public string GetThemeData(){
			return baseTheme.GetSessionData();	
		}
		
		public virtual void Dispose()
		{		
			this.parentClip = null;
			stopTimer();
			
			baseTheme.Dispose();
		}
		
		public virtual void nextOpportunity()
		{
			
		}
		
		public virtual void OpportunityStart()
		{
			
		}
				
		public virtual void OnAudioWordComplete(string word)
		{
			
		}
		
		private TimerObject timerObject;
		
		public void startTimer()
		{
			stopTimer();
			
			Debug.Log ("Timer Started!");
			
			timer = totalTime;
			timerObject = TimerUtils.SetTimeout(timer, onTimerComplete);
			if(currentSession.paused) {
				timerObject.PauseTimer();
			}
		}
		
		public void stopTimer()
		{
			Debug.Log ("Timer Stopped");
			
			if( timerObject != null )  {
				timerObject.Unload();
				timerObject = null;
			}
		}

		public void Pause()
		{
			currentSession.paused = true;
			if( timerObject != null ) {
				stopTimer();
				resumeTimer = true;
			}
			if( baseTheme != null )
				baseTheme.Pause();
			allChildrenMouseToggle(false);
		}
		
		public void Resume()
		{
			currentSession.paused = false;
			allChildrenMouseToggle(true);
			if( resumeTimer ) {
				startTimer();
				resumeTimer = false;
			}
			if( baseTheme != null )
				baseTheme.Resume();
		}
		
		private void allChildrenMouseToggle( bool enable ){ allChildrenMouseToggle(enable, this); }
		private void allChildrenMouseToggle( bool enable, DisplayObjectContainer target )
		{
			MovieClip tempMC;
			DisplayObjectContainer tempDOC;
			for( int i = 0; i < target.numChildren; i++ )
			{
				tempMC = null;
				try{
					tempMC = target.getChildAt<MovieClip>(i);
				}catch{}
				
				if(tempMC == null)
				{
					tempDOC = target.getChildAt<DisplayObjectContainer>(i);
					allChildrenMouseToggle( enable, tempDOC );
				}
				else
				{
					tempMC.mouseEnabled = enable;
					tempMC.mouseChildrenEnabled = enable;
				}
			}
		}
		
		//	TIMER
		/*public void startTimer()
		{
			if( shouldRunTimer ) stopTimer();
			shouldRunTimer = true;
			timer = totalTime;
			if(!currentSession.paused) 
			{
				addEventListener( CEvent.ENTER_FRAME, onTimer );
			}
		}
		
		public void stopTimer()
		{
			removeEventListener( CEvent.ENTER_FRAME, onTimer );
			shouldRunTimer = false;
		}
		
		public void Pause()
		{
			currentSession.paused = true;
			removeEventListener( CEvent.ENTER_FRAME, onTimer );
		}
		
		public void Resume()
		{
			currentSession.paused = false;
			if(shouldRunTimer) {
				addEventListener( CEvent.ENTER_FRAME, onTimer );
			}
		}
		
		protected void onTimer( CEvent e )
		{
			timer -= Time.deltaTime;
			
			if(timer <= 0)
			{
				onTimerComplete();
			}
			
		}*/
		
		public virtual void onTimerComplete()
		{
			dispatchEvent( new SkillEvent(SkillEvent.TIMER_COMPLETE, true, false) );
			
			timeOutPoint = DateUtils.ConvertToUnixTimestamp( DateTime.UtcNow );
		}
		
		protected virtual void resetTutorial()
		{
		}
		
		public virtual void onTutorialStart( CEvent e)
		{
			DebugConsole.Log("Tutorial Start");
			dispatchEvent( new TutorialEvent(TutorialEvent.START) );
		}
		
		public float GetPercentComplete()
		{
			if(currentOpportunity < 1 ) return 0f;
			if(_complete) return 1f;
			return ((float)currentOpportunity - 1f) / (float)totalOpportunities;
		}
		public void resumeTimeout()
		{
			dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
			timeoutEnd = false;
		}
		
		protected void tallyAnswers()
		{
			//_percent = Mathf.Round(((float)NumCorrect / ((float)NumCorrect + (float)NumIncorrect)) * 100);
			stopTimer();
			if(_complete == false)
			{
				_complete = true;
				if(currentSession.helpMode)
					dispatchEvent( new SkillEvent(SkillEvent.HELP_END, true, false) );
				else
					dispatchEvent( new SkillEvent(SkillEvent.END_THEME_REQUEST, true, false) );
			}
		}
		
		protected void MarkInstructionStart()
		{
			dispatchEvent( new SkillEvent(SkillEvent.INSTRUCTION_START));
		}
		
		protected void MarkInstructionEnd()
		{
			dispatchEvent( new SkillEvent(SkillEvent.INSTRUCTION_END));
		}
		
		public SerialObjectMap formatObjectData( String id, Vector2 position )
		{
			SerialObjectMap retObj = new SerialObjectMap();
			retObj.id = id;
			retObj.pos = formatPoint(position);
			retObj.correct = null;
			
			return retObj;
		}
		public SerialObjectMap formatObjectData( String id, Vector2 position, bool correct )
		{
			SerialObjectMap retObj = formatObjectData( id, position );
			
			retObj.correct = correct;
			
			return retObj;
		}
		
		public void AddNewObjectData( SerialObjectMap insert )
		{
			List<SerialOppMap> refNode = currentSession.currentSerializedSession.opps;
			
			if( refNode == null )
			{
				currentSession.currentSerializedSession.opps = new List<SerialOppMap>();
				refNode = currentSession.currentSerializedSession.opps;
			}
			
			currentSession.currentSerializedSession.opps[refNode.Count - 1].objects.Add( insert );
		}
		
		protected double formatDouble( double inVal )
		{
			return (double)(Mathf.FloorToInt((float)inVal * 100) / 100f);
		}
		
		protected List<double> formatPoint( Vector2 position )
		{
			if( position == Vector2.zero )
				return null;
			
			List<double> point = new List<double>();
			point.Add( formatDouble( (double)(position.x / (float)Screen.width) * 100f) );
			point.Add( formatDouble( (double)(position.y / (float)Screen.height) * 100f) );
			
			return point;
		}
		
		public SerialActionMap formatActionData( String type, int objID, Vector2 position )
		{
			double time = currentSession.intervalTime;
			List<SerialOppMap> refNode = currentSession.currentSerializedSession.opps;
			SerialOppMap oppNode = refNode[ refNode.Count - 1 ];
			time = time - oppNode.start;
			
			if(time < 0){
				time -= CorrectNegativeTime( time );
			}
			
			SerialActionMap retAction = new SerialActionMap();
			retAction.time = formatDouble(time);
			retAction.type = type;
			retAction.obj = objID;
			retAction.pos = formatPoint(position);
			retAction.correct = null;
			
			return retAction;
		}
		public SerialActionMap formatActionData( String type, int objID, Vector2 position, bool wasCorrect )
		{
			SerialActionMap retAction = formatActionData( type, objID, position );
			
			retAction.correct = wasCorrect;
			
			return retAction;
		}
		
		public double CorrectNegativeTime( double negativeTime )
		{
			List<SerialOppMap> refNode = currentSession.currentSerializedSession.opps;
			SerialOppMap oppNode = refNode[ refNode.Count - 1 ];
			
			double correctionTime = negativeTime + 5;
			
			oppNode.start += correctionTime;
			
			return correctionTime;
		}
		
		public void AddNewActionData( SerialActionMap insert )
		{
			List<SerialOppMap> refNode = currentSession.currentSerializedSession.opps;
			currentSession.currentSerializedSession.opps[refNode.Count - 1].actions.Add( insert );
		}
		
		public void AddNewOppData( int audioID )
		{
			SerialOppMap newOpp = new SerialOppMap();
			newOpp.count = currentOpportunity;
			newOpp.start = currentSession.intervalTime;
			
			Debug.LogWarning("Starting Opp at: " + newOpp.start);
			
			newOpp.audio = audioID;
			newOpp.objects = new List<SSGCore.SerialObjectMap>();
			newOpp.actions = new List<SerialActionMap>();
			
			currentSession.SerializedData.sessions[currentSession.lastSessionCount].opps.Add( newOpp );
		}
		
		protected void PlayClickSound()
		{
			SoundUtils.PlaySimpleEffect("click");
		}
		
		protected void PlayCorrectSound()
		{
			var clip = SoundUtils.LoadGlobalSound("ding");
			var bundle = new SoundEngine.SoundBundle();
			bundle.AddClip(clip, CLICK_CORRECT, clip.length, false);
			SoundEngine.Instance.PlayBundle(bundle);
		}
		
		protected void PlayIncorrectSound()
		{
			var clip = SoundUtils.LoadGlobalSound("pop");
			var bundle = new SoundEngine.SoundBundle();
			bundle.AddClip(clip, CLICK_INCORRECT, clip.length, false);
			SoundEngine.Instance.PlayBundle(bundle);
		}
		
		public static string hostForSkill( string skillName )
		{
			switch( skillName )
			{
			case "SegmentingCompoundWords":
			case "OnsetRime":
			case "ObjectsInASet":
			case "LanguageVocabulary":
			case "SpatialSkills":
			case "LetterRecognition":
			case "AdditionUpToTen":
				return "Cami";
				
			case "SentenceSegmenting":
			case "InitialSounds":
			case "SequenceCounting":
			case "Subtraction":
			case "CommonShapes":
			case "Patterning":
			case "DeletingOnsetAndRime":
			case "BlendingSoundsInWords":
				return "Henry";
				
			case "BlendingCopoundWords":
			case "CountingFoundations":
			case "NumeralRecognition":
			case "Addition":
			case "Measurement":
			case "Sorting":
			case "SubtractionFromTen":
			default:
				return "Platty";
			}
		}
	}
}
