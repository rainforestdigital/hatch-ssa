using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.geom;

namespace SSGCore
{

	public class SentenceSegmenting : BaseSkill
	{
		private enum SIZE_OPTIONS { THREE_BY_FOUR_1024, THREE_BY_FOUR_SUB_1024, NOT_THREE_BY_FOUR };

		private const string BASE_PATH = "Narration/";
		private const string HELP_PLAY = "HELP_PLAY";
		private const string PLATTY_WANTS = "PLATTY_WANTS";
		private const string SENTENCE_COUNT = "SENTENCE_COUNT";
		
		//Book Cale
		private const float BOOK_SCALE = 0.75f;
		
		//Book Placement
		private const float BOOK_POSITION_MODIFIER_Y = .5f; //How far (percent) down the book is on the screen
		private const float  BOOK_POSITION_MODIFIER_X = .51f; // was .45f //How far (percent) across the screen the books is

		//Pictures
		private const float  PICTURE_POSITION_MODIFIER_X = .275f; //How far over (percent) the picture is on the page
		private const float PICTURE_POSITION_MODIFIER_Y = .5f; //How far down (percent) the book is on the screen
		private Vector2 pictureBase;
		
		//Counter Functionality Constants
		private const float COUNTER_ANSWER_LINE_BASE_X = .555f; //The base is based off of the book's local position. Position found in the FLA.
		private const float COUNTER_ANSWER_LINE_BASE_Y = .43f;

		private const float COUNTER_VERTICAL_OFFSET = .02f;
		
		private const float COUNTER_CONTAINER_LINE_BASE_X = .5f; //The base for the container as a modifer of the screen size
		private const float COUNTER_CONTAINER_LINE_BASE_Y_HIGH = .87f; //The base for the container as a modifer of the screen size
		private const float COUNTER_CONTAINER_LINE_BASE_Y_LOW = .85f; //The base for the container as a modifer of the screen size
		
		public const float COUNTER_TRANSITION_TIME = 0.15f;
		public static int COUNTER_SPACING = 15;

		public const float SOLVE_FOR_PLAYER_BUFFER = 1.0f; //When solving for the player, allows for buffer between steps
		
		public Vector2 adjustedAnswerLineBase;
		public Vector2 adjustedContainerLineBase;
		public Vector2 sourceCounterSize;
		
		//General Constants
		private const float FADE_TIME = 0.35f;

		private SIZE_OPTIONS size;
		private float sourceLineWidth;
		private float lastCounterClick;
		private float bookOffset;

		MovieClip pictures_mc;
		MovieClip book_mc;

		MovieClip counter_mc;
		MovieClip lineCover_mc;
		MovieClip cleanLineCover;
		public PageFlip pageFlip;
		
		int attempt = 0;
		int numIncorrect = 0;
		int numCorrect = 0;
		int glowCount = 0;
		
		int stepCounter = 0;
		float stepTime;
		
		public bool canPlayerInteract = false;
		public bool restartedTutorial = false;
		
		private List<FilterMovieClip> _counters = new List<FilterMovieClip>();
		private List<FilterMovieClip> clickedCounters = new List<FilterMovieClip>();
		private List<TimerObject> _trackedTimers = new List<TimerObject>();
		private TimerObject _restartTutorialTimer;

		private static string[] tutorialSentences = new string[]{"FISH SWIM"};
		
		private static string[] emergingSentences = new string[]{
													  "PLANTS GROW",
													  "GOATS LEAP",
													  "KITTENS SLEEP",
													  "MONKEYS CLIMB",
													  "BIRDS FLY",
													  "SOUP COOKS",
													  "BOATS FLOAT",
													  "WATER DRIPS",
													  "CLOCKS TICK",
													  "TURTLES CRAWL"
		};
		private static string[] developingSentences = new string[]{
														"THE TEACHER READS",
														"MOM EATS FOOD",
														"COMB MY HAIR",
														"I DRINK MILK",
														"HE BRUSHES TEETH",
														"HE IS TALL",
														"SHE JUMPS HIGH",
														"ANN DRINKS WATER"
		};
		private static string[] developedSentences = new string[]{
													   "MY SHOES ARE DIRTY",
													   "HIS HOUSE IS OLD",
													   "OUR CAR IS NEW",
													   "RED APPLES TASTE GOOD"
		};
		private List<string> availableSentences = new List<string>();
		
		private string _currentSentence;
		private List<FilterMovieClip> _answerCounters;

		private FilterMovieClip currentCounter;
		
		/*
		private var _answerCounters:Array = [];
		private var offsetX:Number;
		private var offsetY:Number;
		private var currentCounter:MovieClip;
		*/
		public DisplayObjectContainer bookContainer;
		public DisplayObjectContainer counterContainer;
		
		private new SentenceSegmentingTutorials tutorial_mc;
		private Stack<float> _glowDelays;

		public SentenceSegmenting( SkillLevel level, SessionInfo  currentSession) : base(level, currentSession)
		{
			BACKGROUND = "Background";
			ARTIFACT = "Artifact";
			
			Init (null);
		}
		
		public override void Dispose ()
		{
			base.Dispose();
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			
			clickedCounters = null;

			KillTutorialRestartTimer ();
			foreach( TimerObject timerObj in _trackedTimers)
			{
				if(timerObj != null){
					timerObj.StopTimer();
					timerObj.Unload();
				}
			}
			if(tutorial_mc != null)
			{
				if(tutorial_mc.obj_1 != null) tutorial_mc.obj_1.Destroy();
				if(tutorial_mc.obj_2 != null) tutorial_mc.obj_2.Destroy();
				if(tutorial_mc.gtime != null) tutorial_mc.gtime.Unload();
				tutorial_mc.Dispose();
			}
			if(_counters != null)
			{
				foreach(FilterMovieClip fmc in _counters)
					if(fmc != null) fmc.Destroy();
			}
			if(_answerCounters != null)
			{
				foreach(FilterMovieClip f in _answerCounters)
					if(f!= null) f.Destroy();
			}
			if(currentCounter != null)
				currentCounter.Destroy();
		}

		public SentenceSegmenting( string swf ) : base( swf + ":Skill" )
		{
			PreCacheAssets();
		}

		public void KillTutorialRestartTimer() {
			if (_restartTutorialTimer != null) {
				_restartTutorialTimer.StopTimer ();
				_restartTutorialTimer.Unload ();
				_restartTutorialTimer = null;
			}
		}

		private void PreCacheAssets()
		{
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle("SentenceSegmenting", OnAssetsLoaded));
		}
		
		void OnAssetsLoaded(AssetBundle bundle)
		{
			if( bundle != null )
			{
				Init( null );
			}
		}
		
		private PageAnimation pageAnimation;
		
		public override void Init (MovieClip parent)
		{
			base.Init (parent);
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			this.visible = false;
			
			clickedCounters = new List<FilterMovieClip>();

			bookContainer = new DisplayObjectContainer();
			bookContainer.width = Screen.width;//MainUI.STAGE_WIDTH;
			bookContainer.height = Screen.height;//MainUI.STAGE_HEIGHT;
			bookContainer.x = bookContainer.y = 0;
			
			addChild( bookContainer );
			
			// FIX for bug 993
			float scale = 1;
			if( PlatformUtils.GetClosestRatio() == ScreenRatio.FOURxTHREE )
			{
				
				if(Screen.width > 1024) {
					size = SIZE_OPTIONS.THREE_BY_FOUR_1024;
					scale = 1.25f;
				}
				else
				{
					size = SIZE_OPTIONS.THREE_BY_FOUR_SUB_1024;
				//	COUNTER_SPACING = 7;
					scale = .85f;
				}
				bookContainer.scaleX = bookContainer.scaleY = scale;
				bookContainer.x = (Screen.width*.5f)-((Screen.width*scale) *.5f);
				bookContainer.y = (Screen.height*.5f)-((Screen.height*scale) *.5f);
			}
			else
			{
				size = SIZE_OPTIONS.NOT_THREE_BY_FOUR;
				float expWidth = Screen.width *1.1f;
				float expHeight = Screen.height *1.1f;
				bookContainer.scaleX = expWidth / bookContainer.width;
				bookContainer.scaleY = expHeight / bookContainer.height;
				bookContainer.x = (Screen.width*.5f)-((Screen.width*bookContainer.scaleX) *.5f);
				bookContainer.y = (Screen.height*.5f)-((Screen.height*bookContainer.scaleY) *.5f);
			}			
			
			
			
			bookContainer.alpha = 0;
			
			pictures_mc = MovieClipFactory.CreateSentenceSegmentingPicture();
			
			book_mc = MovieClipFactory.CreateSentenceSegmentingBook();
			bookContainer.addChild(book_mc);
			
			
			book_mc.x = ((bookContainer.width * BOOK_POSITION_MODIFIER_X)-((book_mc.width) * .5f));//-(184*book_mc.scaleX);
			book_mc.y = ((bookContainer.height* BOOK_POSITION_MODIFIER_Y)-((book_mc.height) * .5f));//-(146*book_mc.scaleY);;
			
			//Get the page flip, register it as a page flip for glow effects, and add the submit event listener to it			
			pageFlip = new PageFlip(book_mc.getChildByName<MovieClip>("pageFlip"));
			
			pageAnimation = new PageAnimation(book_mc.getChildByName<MovieClip>("pageAnimation"));
			pageAnimation.skill = this;
			pageAnimation.Root.alpha = 0;
			pageAnimation.Root.visible = false;
			pageAnimation.Root.mouseEnabled = false;
			
			// FIX for bug 1070 - black line bug
			// I had to move the white blank to it's own FLA so that I could set the filter mode on the texture to "point"
			lineCover_mc = book_mc.getChildByName<MovieClip>("lineCover");
			
			cleanLineCover = MovieClipFactory.GetLineBlank();
			book_mc.addChild(cleanLineCover);
			cleanLineCover.x = lineCover_mc.x;
			cleanLineCover.y = lineCover_mc.y;
			Rectangle r = lineCover_mc.getBounds(book_mc);
			
			cleanLineCover.scaleX = r.width / cleanLineCover.width;
			cleanLineCover.scaleY = r.height / cleanLineCover.height;

			book_mc.removeChild(lineCover_mc);
			lineCover_mc = cleanLineCover;

			switch (size) {
			case SIZE_OPTIONS.THREE_BY_FOUR_1024:
				sourceLineWidth = lineCover_mc.width * 1.17f;
				break;

			case SIZE_OPTIONS.THREE_BY_FOUR_SUB_1024:
				sourceLineWidth = lineCover_mc.width * 0.815f;
				break;

			default:
				sourceLineWidth = lineCover_mc.width * 1.03f;
				break;
			}

			int index = book_mc.getChildIndex( cleanLineCover );
			book_mc.setChildIndex(book_mc.getChildByName<MovieClip>("pageAnimation"), index);
			
			if(_level != SkillLevel.Tutorial) lineCover_mc.alpha = 1;
			else lineCover_mc.alpha = 0;
			
			//Find adjusted line base after getting book for the counters
			adjustedAnswerLineBase = book_mc.localToGlobal(new Vector2(book_mc.width * COUNTER_ANSWER_LINE_BASE_X, book_mc.height * COUNTER_ANSWER_LINE_BASE_Y));

//			bookContainer.addChild(pictures_mc);
//			pictures_mc.scaleX = pictures_mc.scaleY = .75f;// (float)Screen.height/(float)Screen.width == .75f ? .75f : .75f;
			//bookContainer.addChild(pageFlip);
			
			book_mc.addChild(pictures_mc);
			pictureBase.x = book_mc.width * PICTURE_POSITION_MODIFIER_X;
			pictureBase.y = book_mc.height * PICTURE_POSITION_MODIFIER_Y;
			
			pictures_mc.gotoAndStop(1);
			
			//book_mc.addChild(bookAnim_mc);
						
			counterContainer = new DisplayObjectContainer();
			counterContainer.width = Screen.width;//MainUI.STAGE_WIDTH;
			counterContainer.height = Screen.height;//MainUI.STAGE_HEIGHT;
			counterContainer.x = counterContainer.y = 0;
			
			//calculated the base location for the counters' line
			float baseY = (PlatformUtils.GetPlatform() == Platforms.ANDROID || PlatformUtils.GetPlatform() == Platforms.IOS) ? COUNTER_CONTAINER_LINE_BASE_Y_LOW : COUNTER_CONTAINER_LINE_BASE_Y_HIGH;
			Vector2 tempVector2 = counterContainer.localToGlobal(new Vector2(counterContainer.width * COUNTER_CONTAINER_LINE_BASE_X,
			                                                                 counterContainer.height * baseY));							
			adjustedContainerLineBase.x = tempVector2.x;
			adjustedContainerLineBase.y = tempVector2.y;
			
			//Set the position of the counter
			addChild(counterContainer);
//			counterContainer.x = (bookContainer.width * COUNTER_CONTAINER_LINE_BASE_X);
//			counterContainer.y = (bookContainer.height * COUNTER_CONTAINER_LINE_BASE_X);
//			counterContainer.x = 300;
//			counterContainer.y = 300;
			
			
//			addChild( counterContainer );
			counterContainer.alpha = 0;
			
			//Set a reference to the counter
			counter_mc = MovieClipFactory.CreateSentenceSegmentingCounter().Target;
			sourceCounterSize = new Vector2 (counter_mc.width, counter_mc.height);

//			Debug.LogError("counter_mc NULL? " + (counter_mc == null));

			bookOffset = Mathf.Min(Screen.width, bookContainer.localToGlobal(new Vector2(book_mc.x + book_mc.width, 0)).x);

			switch(_level)
			{
				case SkillLevel.Tutorial:
					opportunityComplete = false;
					//Sound bundle platty wants
					totalOpportunities = 1;
					tutorial_mc = new SentenceSegmentingTutorials(this);
					addChild( tutorial_mc.View );
					tutorial_mc.View.addEventListener( TutorialEvent.START, onTutorialStart );
					tutorial_mc.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
					tutorial_mc.View.addEventListener( TutorialEvent.YOUR_TURN, HandleTutorialYourTurn );
					tutorial_mc.View.addEventListener( TutorialEvent.STOP_TIMER, HandleTutorialStopTimer );
					tutorial_mc.Resize( Screen.width, Screen.height );	
					SetCurrentSentence(getNextSentence());
					fadeInContainers(); //Fade in Containers to reveal book from opening as requested by client - Jordan
					if(resumingSkill) {
						tutorial_mc.OnStart();
					}
				break;
				case SkillLevel.Emerging:	
				case SkillLevel.Developing:
				case SkillLevel.Developed:
				case SkillLevel.Completed:
					totalOpportunities = 10;
					if(resumingSkill)
					{
						parseSessionData(currentSession.sessionData);
					//	nextOpportunity();
					}
				break;
				default:
				
				break;
			}	
			
			Resize( Screen.width, Screen.height );
			
		}
		
		// called when the tutorial platty touched the first counter and the "now it's your turn audio has played"
		private void HandleTutorialYourTurn(CEvent e)
		{
			nextOpportunity();
			//No timer should be started on the tutorial, Final Instruction should not play until after the counter is touched
			startTimer();
		}
		
		private void HandleTutorialStopTimer(CEvent e)
		{
			stopTimer();
		}
		
		public void SetCurrentSentence(string sentence)
		{
			_currentSentence = sentence;
			pictures_mc.gotoAndStop(sentence);
			
//			pictures_mc.x = 0;
//			pictures_mc.y = 0;
			
			pictures_mc.x = pictureBase.x - .5f * pictures_mc.width;
			pictures_mc.y = pictureBase.y - .5f * pictures_mc.height;
			
//			pictures_mc.x = book_mc.x + (book_mc.width * BOOK_SCALE * PICTURE_POSITION_MODIFIER_X)- (pictures_mc.width * 0.5f);// + ((book_mc.width * 0.5f));// - (pictures_mc.width * 0.5f));
//			pictures_mc.y = book_mc.y + ( book_mc.height * BOOK_SCALE * PICTURE_POSITION_MODIFIER_Y )- (pictures_mc.height * 0.5f);// + ((book_mc.height * 0.25f) - (pictures_mc.height * 0.5f));
			
//			DebugConsole.Log("pictures_mc width: " + pictures_mc.width);
		}
	
		public override void nextOpportunity()
		{
			//Debug.LogWarning("NEXT OPPORTUNITY: " + currentOpportunity);

			stopTimer();
			pageFlip.HideGlow();
			pictures_mc.alpha = 0;			
			attempt = 0;
			
			switch(_level)
			{
				case SkillLevel.Tutorial:
				
					if(currentOpportunity > 0)
					{
						tallyAnswers();
					}
					else
					{
						currentOpportunity++;
						SetCurrentSentence(tutorialSentences[0]);
						pictures_mc.alpha = 1;
						
					// old way ---
						//numCounters = 2;
						//updateCounters(false);
						//OpportunityStart();
					}
				break;
				case SkillLevel.Emerging:
					if(currentOpportunity > 0)
					{
						if(opportunityComplete)
						{
							if(opportunityCorrect)
							{
								numCorrect++;
							}
							else
							{
								numIncorrect++;
							}
						}
						else
						{
							numIncorrect++;
						}
					}
					if(currentOpportunity < totalOpportunities)
					{
						opportunityComplete = false;
						opportunityCorrect = false;
						opportunityAnswered = false;
						
						currentOpportunity++;
						SetCurrentSentence(getNextSentence());
						numCounters = 3;

						pictures_mc.alpha = 0.0f;
//						fadeInContainers().OnComplete(OpportunityStart);
						StartPageAnimation();
						
					}
					else
					{
						numCounters = 0;
					
						tallyAnswers();
					}
				break;
				case SkillLevel.Developing:
					if(currentOpportunity > 0)
					{
						if(opportunityComplete)
						{
							if(opportunityCorrect)
							{
								numCorrect++;
							}
							else
							{
								numIncorrect++;
							}
						}
						else
						{
							numIncorrect++;
						}
					}
					if(currentOpportunity < totalOpportunities)
					{
						opportunityComplete = false;
						opportunityCorrect = false;
						opportunityAnswered = false;
						
						currentOpportunity++;
						SetCurrentSentence(getNextSentence());
						numCounters = 4;

						pictures_mc.alpha = 0.0f;
//						fadeInContainers().OnComplete(OpportunityStart);
						StartPageAnimation();
						
					}
					else
					{
						numCounters = 0;
					
						tallyAnswers();
					}
				break;
				case SkillLevel.Developed:
					if(currentOpportunity > 0)
					{
						if(opportunityComplete)
						{
							if(opportunityCorrect)
							{
								numCorrect++;
							}
							else
							{
								numIncorrect++;
							}
						}
						else
						{
							numIncorrect++;
						}
					}
					if(currentOpportunity < totalOpportunities)
					{
						opportunityComplete = false;
						opportunityCorrect = false;
						opportunityAnswered = false;
						
						currentOpportunity++;
						SetCurrentSentence(getNextSentence());
						numCounters = 5;

						pictures_mc.alpha = 0.0f;
//						fadeInContainers().OnComplete(OpportunityStart);
						StartPageAnimation();
						
					}
					else
					{
						numCounters = 0;
						
						tallyAnswers();
					}
				break;
				case SkillLevel.Completed:
					if(currentOpportunity > 0)
					{
						if(opportunityComplete)
						{
							if(opportunityCorrect)
							{
								numCorrect++;
							}
							else
							{
								numIncorrect++;
							}
						}
						else
						{
							numIncorrect++;
						}
					}
					if(currentOpportunity < totalOpportunities)
					{
						opportunityComplete = false;
						opportunityCorrect = false;
						opportunityAnswered = false;
						
						currentOpportunity++;
						SetCurrentSentence(getNextSentence());
						numCounters = 5;
	
						pictures_mc.alpha = 0.0f;
//						fadeInContainers().OnComplete(OpportunityStart);
						StartPageAnimation();
						
					}
					else
					{
						numCounters = 0;
						
						tallyAnswers();
					}
				break;
				default:
				
				break;
			}
		}
		
		private TweenerObj fadeInContainers()
		{
			Tweener.addTween(pictures_mc, Tweener.Hash("time", FADE_TIME, "alpha", 1.0f, "transition", Tweener.TransitionType.easeInOutQuint));
			Tweener.addTween(counterContainer, Tweener.Hash("time", FADE_TIME, "alpha", 1.0f, "transition", Tweener.TransitionType.easeInOutQuint));
//			foreach(FilterMovieClip fmc in _counters) Tweener.addTween(fmc.Target, Tweener.Hash("time", FADE_TIME, "alpha", 1.0f, "transition", Tweener.TransitionType.easeInOutQuint));
			if(_level != SkillLevel.Tutorial) lineCover_mc.alpha = 1;
			else lineCover_mc.alpha = 0;
			return Tweener.addTween(bookContainer, Tweener.Hash("time", FADE_TIME, "alpha", 1.0f, "transition", Tweener.TransitionType.easeInOutQuint));
		}
		
		private TweenerObj fadeOutContainers()
		{
			Tweener.addTween(pictures_mc, Tweener.Hash("time", FADE_TIME, "alpha", 0f, "transition", Tweener.TransitionType.easeInOutQuint));
			Tweener.addTween(counterContainer, Tweener.Hash("time", FADE_TIME, "alpha", 0f, "transition", Tweener.TransitionType.easeInOutQuint));
//			foreach(FilterMovieClip fmc in _counters) Tweener.addTween(fmc.Target, Tweener.Hash("time", FADE_TIME, "alpha", 0f, "transition", Tweener.TransitionType.easeInOutQuint));
			return Tweener.addTween(bookContainer, Tweener.Hash("time", FADE_TIME, "alpha", 0f, "transition", Tweener.TransitionType.easeInOutQuint));
		}
		
		public override void OpportunityStart()
		{	
			//Debug.Log("Opportunity Start");
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;
			
			totalTime = 5;
			
			clickedCounters = new List<FilterMovieClip>();
			
			switch(_level)
			{
				case SkillLevel.Tutorial:
					/*clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, "Narration/1A-Skill Instruction Audio/1A-now-its-your-turn" );
					bundle.AddClip(clip, NOW_ITS, clip.length);
					SoundEngine.Instance.PlayBundle(bundle);
					TimerUtils.SetTimeout(clip.length, StartTimerAfterSound);*/
					
				break;
				default:
					float totalClipTime = 0;
					//DebugConsole.Log("OPPORTUNITY START, DEFAULT HANDLED: " + _currentSentence + " :: Current Opportunity " + currentOpportunity);
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_16 + _currentSentence.ToLower().Replace(" ","-"));
					totalClipTime += clip.length;
					bundle.AddClip(clip, "", clip.length);
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_38 + _currentSentence.ToLower().Replace(" ","-"));
					totalClipTime += clip.length;
					bundle.AddClip(clip, "TOUCH", clip.length);
					SoundEngine.Instance.PlayBundle(bundle);

					_trackedTimers.Add(TimerUtils.SetTimeout(totalClipTime, StartTimerAfterSound));
					canPlayerInteract = true;
				
					if( !visible ) 
					{
						bookContainer.alpha = 0;
						visible = true;
						Tweener.addTween(bookContainer, Tweener.Hash("time", FADE_TIME, "alpha", 1.0f, "transition", Tweener.TransitionType.easeInOutQuint));
					}
				break;
			}
				
//			foreach(FilterMovieClip item in _counters)
//			{
//				item.addEventListener( MouseEvent.CLICK, onCounterClick );
//			}
			
			//bookAnim_mc.enabled = true;
			//pictureBtn_mc.enabled = true;
			
			timeoutCount = 0;
			
			dispatchEvent(new SkillEvent(SkillEvent.OPPORTUNITY_START, true, false));
			
			AddNewOppData( audioNames.IndexOf( SentenceSegmentingSounds.SOUND_38 + _currentSentence.ToLower().Replace(" ","-") ) );
			
			AddNewObjectData( formatObjectData( "PageFlipSubmit", pageFlip.target.parent.localToGlobal( new Vector2(pageFlip.target.x, pageFlip.target.y) ) ) );
			foreach( FilterMovieClip fmc in _counters )
			{
				string label = fmc.Target.currentLabel.Replace("BEAR_", "");
				string realLabel = "COUNTER_" + label;
				switch(label){
				case "PURPLE":
					realLabel = "COUNTER_YELLOW";
					break;
				case "BLUE":
					realLabel = "COUNTER_PURPLE";
					break;
				default:
					break;
				}
				AddNewObjectData( formatObjectData( realLabel, fmc.parent.localToGlobal(new Vector2( fmc.x, fmc.y )) ) );
			}
		}
		
		public string getNextSentence()
		{	
			List<string> tempList = new List<string>(); //temp list to help randomly select words from lists
			int tempIndex;
			
			if(availableSentences.Count == 0)
			{
				switch(_level)
				{
					case SkillLevel.Tutorial:
						availableSentences.Add(tutorialSentences[0]);
					break;
					
					case SkillLevel.Emerging:
					
						foreach(string str in emergingSentences) tempList.Add(str); //Set available strings to temp list
					
						for(int i = 0;i< emergingSentences.Length;i++)
						{
							tempIndex = UnityEngine.Random.Range(0, tempList.Count); //Choose random item from list
							if( tempIndex == tempList.Count ) tempIndex--;
							availableSentences.Add(tempList[tempIndex]);
							tempList.RemoveAt (tempIndex);
						}
					break;
					case SkillLevel.Developing:
					
						//emergingSentences.sort(sortRandom);
						foreach(string str in emergingSentences) tempList.Add(str);
						for(int i = 0; i < 5; i++)
						{
							tempIndex = UnityEngine.Random.Range(0, tempList.Count);
							if( tempIndex == tempList.Count ) tempIndex--;
							availableSentences.Add(tempList[tempIndex]);
							tempList.RemoveAt (tempIndex);
						}
					
						tempList.Clear();
					
						//developingSentences.sort(sortRandom);
						foreach(string str in developingSentences) tempList.Add(str);
						for(int i = 0; i < 5; i++)
						{
							tempIndex = UnityEngine.Random.Range(0, tempList.Count);
							if( tempIndex == tempList.Count ) tempIndex--;
							availableSentences.Add(tempList[tempIndex]);
							tempList.RemoveAt (tempIndex);
						}
					break;
					case SkillLevel.Developed:
						//emergingSentences.sort(sortRandom);
						foreach(string str in emergingSentences) tempList.Add(str);
						for(int i = 0; i < 2; i++)
						{
							tempIndex = UnityEngine.Random.Range(0, tempList.Count);
							if( tempIndex == tempList.Count ) tempIndex--;
							availableSentences.Add(tempList[tempIndex]);
							tempList.RemoveAt (tempIndex);
						}
					
						tempList.Clear();
					
						//developingSentences.sort(sortRandom);
						foreach(string str in developingSentences) tempList.Add(str);
						for(int i = 0; i < 4; i++)
						{
							tempIndex = UnityEngine.Random.Range(0, tempList.Count);
							if( tempIndex == tempList.Count ) tempIndex--;
							availableSentences.Add(tempList[tempIndex]);
							tempList.RemoveAt (tempIndex);
						}
					
											
						//developedSentences.sort(sortRandom);
						//Add all developed sentences
						for(int i = 0; i < 4; i++)
						{
							availableSentences.Add(developedSentences[i]);
						}
					break;
					case SkillLevel.Completed:
					
						//emergingSentences.sort(sortRandom);
						foreach(string str in emergingSentences) tempList.Add(str);
						for(int i = 0; i < 2; i++)
						{
							tempIndex = UnityEngine.Random.Range(0, tempList.Count);
							if( tempIndex == tempList.Count ) tempIndex--;
							availableSentences.Add(tempList[tempIndex]);
							tempList.RemoveAt (tempIndex);
						}
					
						tempList.Clear();
					
						//developingSentences.sort(sortRandom);
						foreach(string str in developingSentences) tempList.Add(str);
						for(int i = 0; i < 4; i++)
						{
							tempIndex = UnityEngine.Random.Range(0, tempList.Count);
							if( tempIndex == tempList.Count ) tempIndex--;
							availableSentences.Add(tempList[tempIndex]);
							tempList.RemoveAt (tempIndex);
						}

					
						//developedSentences.sort(sortRandom);
						//Add all developed sentences
						for(int i = 0; i < 4; i++)
						{
							availableSentences.Add(developedSentences[i]);
						}
					break;
					default:
					
					break;
				}
			}
			string returnString;
			if(resumingSkill)
			{
				returnString = availableSentences[0];
				resumingSkill = false;
			}
			else
			{		
				int randomSentence = Mathf.FloorToInt(UnityEngine.Random.Range(0, availableSentences.Count));
				if( randomSentence == availableSentences.Count ) randomSentence--;
				returnString = availableSentences[randomSentence];
			}
			availableSentences.Remove(returnString);
			//Debug.Log("#@##@##@#@# RETURNED SENTENCE: " + returnString);
			return returnString;
		}
		
		public override void OnAudioWordComplete( string word )
		{	
			if( word.Length == 0 ) return;
			
			AudioClip clip = null;
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			
			if(word == "START_TIMER")
			{	
				totalTime = 5;
				startTimer();
				removeCounterGlow();
				return;
			}
			
			if(word == THEME_INTRO)
			{
				//this.visible = true;	
			}
			
			if(opportunityComplete)
			{
				switch( word )
				{			
					case THEME_COMPLIMENT:
						if(_level == SkillLevel.Tutorial)
						{
							clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_10 );
							bundle.AddClip(clip, "NOW_LETS", clip.length);
							SoundEngine.Instance.PlayBundle(bundle);
						
							Tweener.addTween( tutorial_mc.tutorial_mc, Tweener.Hash("time", clip.length, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuint) ).OnComplete(HandleTutorialFadeOutComplete);
							tutorial_mc.tutorialComplete(null);
						}
						else
						{
							string[] words = _currentSentence.Split(' ');
							
							string number = "";
							if(words.GetLength(0) == 2)
								number = "Words-In";	
							if(words.GetLength(0) == 3)
								number = "Three";
							if(words.GetLength(0) == 4)
								number = "Four";
							clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_60+number + "-" + _currentSentence.ToLower().Replace(" ","-"));
							bundle.AddClip(clip, SENTENCE_COUNT, clip.length);
							SoundEngine.Instance.PlayBundle(bundle);
						
							float waitTime = clip.length;
							for( int i = 0 ; i < words.GetLength(0) ; i++)
							{
								clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_82 + words[i].ToLower() );
								waitTime -= clip.length *.6f;
							}
							glowCount = 0;
							_trackedTimers.Add(TimerUtils.SetTimeout(waitTime, glowInTime));
						}
					break;
					case THEME_CRITICISM:
						_answerCounters = new List<FilterMovieClip>();
						foreach(FilterMovieClip item in _counters)
						{
							item.addEventListener( MouseEvent.CLICK, onCounterClick );
						}
					break;
					case SENTENCE_COUNT:
						//DebugConsole.Log("SENTENCE_COUNT");
					
						if(_level != SkillLevel.Tutorial)// && (opportunityCorrect == true || currentOpportunity == totalOpportunities))
						{
							fadeOutContainers();
						}
					
						if(opportunityCorrect)
						{
							//DebugConsole.LogError("*******CORRECT ANSWER");
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
						}
						else
						{
							//DebugConsole.LogError("*******INCORRECT ANSWER");
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
						}
						
						foreach(FilterMovieClip item in _counters)
						{
							item.removeEventListener(MouseEvent.CLICK, onCounterClick);
							counterContainer.removeChild(item);
						}
					break;
					case HELP_PLAY :
						//startTimer();
						//enableSortItems();
					break;
					
					case "TOUCH":
					//Instructions are over, add buttons
					enableInteractions();
					
					break;

					default:
						
					break;
					/*case "PAYOFF_50":
					case "PAYOFF_100":
						DebugConsole.Log("PAYOFF CALLED: " + word);	
						if(_level != SkillLevel.Tutorial)
						{
							nextOpportunity();
						}
					break;*/
					case NOW_LETS:
						
					break;
				}
				
			}
			else
			{
				switch( word )
				{			
					case THEME_INTRO:
						if( !currentSession.skipIntro )
						{
							clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_1 );
							bundle.AddClip(clip, INTRO, clip.length);
							SoundEngine.Instance.PlayBundle(bundle);
						}
						else
							OnAudioWordComplete( INTRO );
					break;
					case INTRO:
						if(_level == SkillLevel.Tutorial)
						{
							//Debug.Log("START TUTORIAL");
							tutorial_mc.OnStart();
							if( !visible ) 
							{
								bookContainer.alpha = 0;
								visible = true;
								Tweener.addTween(bookContainer, Tweener.Hash("time", FADE_TIME, "alpha", 1.0f, "transition", Tweener.TransitionType.easeInOutQuint));
							}
						}
						else
						{
							nextOpportunity();
						}
					break;
					/*case "PAYOFF_50":
					case "PAYOFF_100":
						if(_level != SkillLevel.Tutorial)
						{
							nextOpportunity();
						}
					break;*/
					case THEME_CRITICISM:
						_answerCounters = new List<FilterMovieClip>();
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_38 + _currentSentence.ToLower().Replace(" ","-"));
						bundle.AddClip(clip, "TOUCH", clip.length);
						SoundEngine.Instance.PlayBundle(bundle);
					break;
					case HELP_PLAY :
						//startTimer();
						//enableSortItems();
					break;
					
					/*case "PLATTY_WANTS":
					 	this.visible = true;
					break;*/
					case "TOUCH":
					//Instructions are over, add buttons
					enableInteractions();
					
					break;
					
					case "SWIM":
					timeoutCount = 0;
					break;
					
					default:
						
					break;
				}
			}
		}
		
				
		public override void Resize( int _width, int _height )
		{
			this.width = _width;
			this.height = _height;
			
			float targWidth = width;
			if( PlatformUtils.GetPlatform() == Platforms.WIN ) targWidth = 1920f;
			if( PlatformUtils.GetPlatform() == Platforms.ANDROID ) targWidth = 1280f;
			
			scaleX = scaleY = _width/targWidth + ((1f - (_width/targWidth))/2f);

			y = (_height - (height*scaleY))/2f;
			x = (_width - (width*scaleX))/2f;
		}
		
		
		private int numCounters
		{
			get{ return _counters.Count; }
			set
			{
				//Debug.Log ("SETTING COUNTERS TO: " + value + ", counters: " + _counters.Count);
				
				_answerCounters = new List<FilterMovieClip>();
				clickedCounters = new List<FilterMovieClip>();
				
				foreach(FilterMovieClip item in _counters)
				{
					item.Destroy();
					item.removeEventListener(MouseEvent.CLICK, onCounterClick);
					counterContainer.removeChild(item);
				}
				
				_counters = new List<FilterMovieClip>();
				
				for(int i = 0; i < value; i++)
				{
					FilterMovieClip tempCounter = MovieClipFactory.CreateSentenceSegmentingCounter();
					
					//Resizing the Coutners
//					tempCounter.Target.scaleX = tempCounter.Target.scaleY = COUNTER_SCALE;
					
					//tempCounter.addEventListener(MouseEvent.MOUSE_DOWN, onCounterDown);
//					tempCounter.addEventListener(MouseEvent.CLICK, onCounterClick);
					if(i == 0)
					{
						int r = Mathf.FloorToInt(UnityEngine.Random.value*5);
						if(r == 5) r--;
						tempCounter.Target.gotoAndStop(r);
					}
					else
					{
						tempCounter.Target.gotoAndStop(_counters[0].Target.currentFrame);
					}
					
//					tempCounter.Target.alpha = 0;
					_counters.Add(tempCounter);
					counterContainer.addChild(tempCounter);
				}
				
//				float scale;
//				
//				if( PlatformUtils.GetClosestRatio() == ScreenRatio.FOURxTHREE)
//				{
//					if(Screen.width > 1024)
//						scale = 1.4f;
//					else
//						scale = .95f;
//				}
//				else
//					scale = 1.0f;
				
				/*if( PlatformUtils.GetClosestRatio() == ScreenRatio.FOURxTHREE)
				{
					if(Screen.width > 1024)
						counterContainer.LayoutGameTiles(_counters, 1.25f, value);
					else
						counterContainer.LayoutGameTiles(_counters, .85f, value);
				}
				else
					counterContainer.LayoutGameTiles(_counters, 1.0f, value);*/
				
				if(_level == SkillLevel.Tutorial)
				{
					_answerCounters.Add(_counters[0]);
					_counters[0].Target.gotoAndStop("BEAR_BLUE");
					//_counters[0].enabled = false;
					_counters[1].Target.gotoAndStop("BEAR_BLUE");
				}
				/*
				counter_mc.scaleX = counter_mc.scaleY = scale;
				float counterWidth = counter_mc.width;
				for(int j=0; j< _counters.Count; j++)
				{
					_counters[j].x = adjustedContainerLineBase.x + (counterWidth + COUNTER_SPACING) * j;
					_counters[j].y = adjustedContainerLineBase.y;
					_counters[j].scaleX = _counters[j].scaleY = scale;
				}
				*/
				//counterContainer.ShiftGameTiles(_counters, new Vector2(0, Screen.height * 0.5f));
//				counterContainer.alpha = 1;
				updateCounters(false);
			}
		}
		
		private void onTutorialComplete( CEvent e )
		{
			//Debug.Log("TUTORIAL COMPLETE");
			stopTimer();
			tutorial_mc.View.removeEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.View.removeEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			
//			TimerUtils.SetTimeout(4f, PlayFinalInstructions);
			
			//removeChild( tutorial_mc.View );
			
			//nextOpportunity();
			
		}
		
		private void HandleTutorialFadeOutComplete()
		{
			//container.removeChild( tutorialBG );
			dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
			removeChild( tutorial_mc.View );
		}

		private void onCounterClick( CEvent e )
		{
			// To avoid accidental double clicks
			if( Time.time - lastCounterClick < 1f ) 
			{
				return;
			}
			lastCounterClick = Time.time;

			Tweener.removeTweens(this);
			stopTimer();
			
			foreach(FilterMovieClip  item in _counters)
			{
				if(item.filters.Count>0) item.filters[0].Visible = false;
			}
			
			timeoutCount = 0;
			

			currentCounter = (FilterMovieClip)e.currentTarget;
//			currentCounter.removeEventListener( MouseEvent.CLICK, onCounterClick );
			
			counterContainer.setChildIndex(currentCounter, counterContainer.numChildren - 1);
			
			Tweener.removeTweens(currentCounter);
			
			//if(_answerCounters.IndexOf(currentCounter) == -1)
			if( !_answerCounters.Contains(currentCounter) )
			{
				_answerCounters.Add(currentCounter);
				string[] answerArray = _currentSentence.Split(' ');
				if((_answerCounters.Count) <= answerArray.GetLength(0) && _answerCounters.Count > 0)
				{
					//DebugConsole.LogWarning("COUNTER CLICKED AND WAS CORRECT - LEVEL: " + _level + ", _answerCounters.Count: " + _answerCounters.Count + ", answerArray.GetLength(0): " + answerArray.GetLength(0));
					SoundEngine.Instance.SendCancelToken();
					//SoundEngine.Instance.StopAll();
					
					if( _answerCounters.Count == answerArray.GetLength(0) ) 
					{
						SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
						AudioClip clip;
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_82 + answerArray[_answerCounters.Count-1].ToLower());
						bundle.AddClip(clip);
						SoundEngine.Instance.PlayBundle(bundle);
					}
					else PlaySound(SentenceSegmentingSounds.SOUND_82 +answerArray[_answerCounters.Count-1].ToLower());
				}	
			}
			else
			{
				_answerCounters.Remove(currentCounter);
			}
			
			clickedCounters.Add(currentCounter);
			
			updateCounters();
			
			startTimer(); //start the timer again for inactivity prompts

			if(_answerCounters.Count > 0)
				AddListenerPageFlipSubmit();
			else
				RemoveListenerPageFlipSubmit();

			PlayClickSound();
			
			AddNewActionData( formatActionData( "tap", _counters.IndexOf(currentCounter) + 1, Vector2.zero ) );
		}
		
		public void PlayCounterPrompt()
		{
			PlayCounterPrompt("");
		}
			
		public void PlayCounterPrompt(string msg)
		{

			if(!canPlayerInteract) 
			{
//				Debug.Log("Player Can't interract so why bother prompting?");
				return;
			}
//			Debug.Log("*******PLAYING FINAL INSTRUCTIONS**********");
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;
			clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, "Narration/1A-Skill Instruction Audio/1A-when-you-are-finished");
			bundle.AddClip(clip, msg, clip.length);
			SoundEngine.Instance.PlayBundle(bundle);
			
			pageFlip.ShowGlow();
			_trackedTimers.Add(TimerUtils.SetTimeout(clip.length, StartTimerAfterSound));
		}
		
		public void PlayFinalInstructions()
		{
			PlayFinalInstructions("");
		}
		
		public void PlayFinalInstructions(string msg)
		{
			if(!canPlayerInteract) 
			{
//				Debug.Log("Player Can't interract so why bother prompting?");
				return;
			}
//			Debug.Log("*******PLAYING FINAL INSTRUCTIONS**********");
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;
			clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_6);
			bundle.AddClip(clip, msg, clip.length);
			SoundEngine.Instance.PlayBundle(bundle);
			
			pageFlip.ShowGlow();
			_trackedTimers.Add(TimerUtils.SetTimeout(clip.length, StartTimerAfterSound));
		}
	
		
		private void onPictureDown( CEvent e )
		{
			SoundEngine.Instance.SendCancelToken();
			//SoundEngine.Instance.StopAll();
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;
			clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_16 +_currentSentence.ToLower().Replace(" ","-"));
			bundle.AddClip(clip);
			SoundEngine.Instance.PlayBundle(bundle);

		}
		
		void updateCounters()
		{
			updateCounters(true);
		}
		
		void updateCounters(bool animate)
		{
			//Update counters moves each counter towards its target location based off of screen resolution
			float targetX; //In Pixels
			float targetY; //In Pixels

			Vector2 counterSize = new Vector2 (sourceCounterSize.x, sourceCounterSize.y);
			float scaledCounterWidth = _counters.Count < 5 ? sourceCounterSize.x : sourceLineWidth / _counters.Count; // Only scale if we have 5 or more counters
			float scaleValue = Mathf.Min(1f, scaledCounterWidth / sourceCounterSize.x);
			Vector2 scaledCounterSize = new Vector2 (scaledCounterWidth, sourceCounterSize.y * scaleValue);

			for(int i=0;i< _counters.Count;i++)
			{
				//Choose target position based off of whether the counter is an answer or not.

				bool isOnLine = false;

				if(_answerCounters.Contains(_counters[i]))
				{
					isOnLine = true;
					//Set the position of the counter
					targetX = (adjustedAnswerLineBase.x) + scaledCounterSize.x * _answerCounters.IndexOf(_counters[i]);
					targetY = adjustedAnswerLineBase.y - scaledCounterSize.y - 2;
					targetY += (COUNTER_VERTICAL_OFFSET * book_mc.height);
				}
				else
				{
					targetX = (bookOffset * scaleX) - (counterSize.x + COUNTER_SPACING) * _counters.Count + (counterSize.x + COUNTER_SPACING) * i;
					targetY = adjustedContainerLineBase.y;
				}
					
				if(animate)
				{
					/*
					Hashtable hash = new Hashtable();
					hash.Add("time",COUNTER_TRANSITION_TIME);
					hash.Add("x",targetX);
					hash.Add("y", targetY);
					hash.Add("scaleX", isOnLine ? scaleValue : 1f);
					hash.Add("scaleY", isOnLine ? scaleValue : 1f);
					hash.Add("transition", Tweener.TransitionType.easeInOutQuint);
					Tweener.addTween(_counters[i], hash);
					*/
					Tweener.addTween(_counters[i], Tweener.Hash("time",COUNTER_TRANSITION_TIME, "x",targetX, "y", targetY, "scaleX", isOnLine ? scaleValue : 1f, "scaleY", isOnLine ? scaleValue : 1f, "transition", Tweener.TransitionType.easeInOutQuint) );
				}
				else
				{
					_counters[i].x = targetX;
					_counters[i].y = targetY;
					_counters[i].scaleX = _counters[i].scaleY = isOnLine ? scaleValue : 1f;
				}
			}
		}
		
		public override void onTimerComplete()
		{			
			base.onTimerComplete();
			
			stopTimer();
			
			timeoutCount++;
			
			//Debug.Log ("CURRENT SKILL: " + _level.ToString() + " ::: TIMEOUTCOUNT = " + timeoutCount);
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;
			string clipName = "";
			
			switch(timeoutCount)
			{
				case 1:
					if(_level == SkillLevel.Tutorial)
					{					
						// check counter - if they are at the 2nd word, then tell them to touch the corner again
						// else, “Now it’s your turn. Touch a counter for the word runs.”
						if((tutorial_mc as SentenceSegmentingTutorials).HasTouched) 
						{
							PlaySoundAndStartTimer(SentenceSegmentingSounds.SOUND_5);
							clipName = SentenceSegmentingSounds.SOUND_5;
						
							/*clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, "Narration/1A-Skill Instruction Audio/1A-Sentence/1A-"+_currentSentence.ToLower().Replace(" ","-"));
							bundle.AddClip(clip, "START_TIMER", clip.length);
							SoundEngine.Instance.PlayBundle(bundle);*/

						}
						else if(tutorial_mc.repeatFinalInstruction )
						{
							PlayFinalInstructions();
							clipName = SentenceSegmentingSounds.SOUND_6;
						}
					}
					else
					{

						if(_answerCounters.Count == 0) //No counter has been clicked
						{
							clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_7);
							bundle.AddClip(clip, "START_TIMER", clip.length);
							SoundEngine.Instance.PlayBundle(bundle);
													
							clipName = SentenceSegmentingSounds.SOUND_7;
						}
						else
						{
							clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_9);
							bundle.AddClip(clip, "START_TIMER", clip.length);
							SoundEngine.Instance.PlayBundle(bundle);
						
							clipName = SentenceSegmentingSounds.SOUND_9;
						
							pageFlip.ShowGlow();
						}
					}
				break;
				
				case 2:
					// Please touch the help button to see Henry count
					if(_level == SkillLevel.Tutorial)
					{
						if(!restartedTutorial)
						{
							currentOpportunity = 0;
							tutorial_mc.disableCounters();
							PlaySound(SentenceSegmentingSounds.SOUND_11);
							//onTutorialComplete(new CEvent(CEvent.COMPLETE));
							timeoutCount = 0;
							//_answersCorrect = 0;
							_restartTutorialTimer = TimerUtils.SetTimeout (3f, RestartTutorialAfterPause);
							//_trackedTimers.Add(restartTutorialTimer); // restart tutorial
						
							clipName = SentenceSegmentingSounds.SOUND_11;
						}
						else
						{
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") ); 
						}
						
					}
					else
					{
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_8);
						bundle.AddClip(clip, "START_TIMER", clip.length);
						SoundEngine.Instance.PlayBundle(bundle);
					
						clipName = SentenceSegmentingSounds.SOUND_8;
					}
				break;
				
				case 3:
					if(_level == SkillLevel.Tutorial)
					{
						onTutorialComplete(new CEvent(CEvent.COMPLETE));
						timeoutCount = 0;
					}
					else
					{
						if(_answerCounters.Count == 0) //No counter has been clicked
						{
							opportunityCorrect = false;
							opportunityAnswered = true;
						
							clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_38 + _currentSentence.ToLower().Replace(" ","-"));
							bundle.AddClip(clip, "START_TIMER", clip.length);
							SoundEngine.Instance.PlayBundle(bundle);
						
							clipName = SentenceSegmentingSounds.SOUND_38 + _currentSentence.ToLower().Replace(" ","-");
						
							//Highlight counters
							float waitTime = clip.length;
							string[] answerArray = _currentSentence.Split(' ');
							_glowDelays = new Stack<float> (); 
							for( int i = 0 ; i < answerArray.GetLength(0) ; i++)
							{
								clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_82 + answerArray[i].ToLower() );
								//waitTime -= clip.length *.6f;
								waitTime -= clip.length;
								_glowDelays.Push(clip.length); 
							}
							glowCount = 0;
							//_trackedTimers.Add(TimerUtils.SetTimeout(waitTime, glowInTime));
							_trackedTimers.Add(TimerUtils.SetTimeout(waitTime, glowUsingTimerArray));
							//glowWithoutDelay();
						}
						else
						{
							opportunityCorrect = false;
							opportunityAnswered = true;
						
							clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_9);
							bundle.AddClip(clip, "START_TIMER", clip.length);
							SoundEngine.Instance.PlayBundle(bundle);
						
							clipName = SentenceSegmentingSounds.SOUND_9;
						
							pageFlip.ShowGlow();
						}
					}
				break;
				
				case 4:

					//Debug.Log ("SOLVE FOR PLAYER");
					timeoutEnd = true;
					disableInteractions();
					solveForPlayer();
				
					clipName = SentenceSegmentingSounds.SOUND_60 + "Counters-For-" + "-" + _currentSentence.ToLower().Replace(" ","-");

				break;
			}
			
			if( !string.IsNullOrEmpty( clipName ) )
			{
				baseTheme.AddAudioSessionData("time", audioNames.IndexOf( clipName ) );
			}
		}

		private void glowUsingTimerArray() {
			string[] answerArray = _currentSentence.Split(' ');
			if(_glowDelays.Count > 0 && glowCount < answerArray.Length) {
				float delay = _glowDelays.Pop ();
				_trackedTimers.Add(TimerUtils.SetTimeout(delay, glowUsingTimerArray));
				if(_answerCounters.Count <= glowCount)
					addCounterGlow(getNonAnswers()[glowCount - _answerCounters.Count]);
				else
					addCounterGlow( _answerCounters[glowCount] );

				glowCount++;
			}
		}

		private void glowWithoutDelay() {
			string[] answerArray = _currentSentence.Split(' ');
			if(glowCount >= answerArray.Length)
				return;

			if(_answerCounters.Count <= glowCount)
				addCounterGlow(getNonAnswers()[glowCount - _answerCounters.Count]);
			else
				addCounterGlow( _answerCounters[glowCount] );
			
			glowCount++;
			glowWithoutDelay ();
		}

		private void glowInTime ()
		{
			string[] answerArray = _currentSentence.Split(' ');
			if(glowCount >= answerArray.Length)
				return;
			
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_82 + answerArray[glowCount].ToLower() );
			_trackedTimers.Add(TimerUtils.SetTimeout(clip.length *.6f, glowInTime));
			if(_answerCounters.Count <= glowCount)
				addCounterGlow(getNonAnswers()[glowCount - _answerCounters.Count]);
			else
				addCounterGlow( _answerCounters[glowCount] );
			//addCounterGlow( clickedCounters[glowCount] );
			
			glowCount++;
		}
		
		private List<FilterMovieClip> getNonAnswers()
		{
			if(_answerCounters.Count < 1)
				return _counters;
			
			List<FilterMovieClip> nonAnswers = new List<FilterMovieClip>();
			
			for(int i = 0; i < _counters.Count; i++)
			{
				if(_answerCounters.IndexOf(_counters[i]) < 0)
					nonAnswers.Add(_counters[i]);
			}
			
			return nonAnswers;
		}
		
		private void PlaySoundAndStartTimer(string snd)
		{
			PlaySound(snd);
			_trackedTimers.Add(TimerUtils.SetTimeout(1.5f, StartTimerAfterSound));
		}

		private void StartTimerAfterSound()
		{
			totalTime = 5;
			startTimer();
		}
		
		public void PlaySound(string snd)
		{
			PlaySound (snd, "");
		}
		
		public void PlaySound(string snd, string evnt)
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, snd );
			if( evnt.Length == 0 ) bundle.AddClip(clip);
			else bundle.AddClip(clip, evnt, clip.length, false);
			SoundEngine.Instance.PlayBundle(bundle);
		}
		
		private void RestartTutorialAfterPause()
		{
			RestartTutorialAfterPause("");
		}
		
		private void RestartTutorialAfterPause(string msg)
		{
			restartedTutorial = true;
			pageFlip.HideGlow();
			tutorial_mc.Restart();
		}
		
		private void DelayComplimentCallAfterDing()
		{
			dispatchEvent(new SkillEvent(SkillEvent.COMPLIMENT_REQUEST,true,false));
		}
		
		private void DelayCriticismCallAfterDing()
		{
			dispatchEvent(new SkillEvent(SkillEvent.CRITICISM_REQUEST,true,false));
		}
		
		private float submitTime = 0;
		private void onSubmit( CEvent e )
		{
			if( Time.time - submitTime < 1 )
			{
				DebugConsole.LogError("DOUBLE TAP ON SUBMIT CAUGHT");
				return;
			}
			submitTime = Time.time;
			
//			Debug.Log("//////////////////SUBMITED TIMER STOPPED\\\\\\\\\\\\\\\\\\\\\\\\\\");
			
			Debug.Log ("NUMBER OF COUNTERS: " + _counters.Count);

			KillTutorialRestartTimer ();

			disableInteractions();
			//Hide the corner glow
			pageFlip.HideGlow();
			
			canPlayerInteract = false;
			stopTimer();
			timeoutCount = 0;
			
			SoundEngine.Instance.SendCancelToken();
			//SoundEngine.Instance.StopAll();
			
			if( _level == SkillLevel.Tutorial )
			{
				opportunityComplete = true;
				if(!opportunityAnswered)
					opportunityCorrect = true;
				//Tutorial is done, deactivate submit button
				TimerUtils.SetTimeout(1, DelayComplimentCallAfterDing);
				PlayCorrectSound();
				RemoveListenerPageFlipSubmit();
				//pageAnimation.Show();
				
				AddNewActionData( formatActionData( "tap", 0, Vector2.zero, true ) );
				return;
			}
			
			Tweener.removeTweens(this);

			string[] answerArray = _currentSentence.Split(' ');
			
			//Answer is correct
			if(_answerCounters.Count == answerArray.GetLength(0))
			{
				if(opportunityAnswered == false)
				{
					//Debug.Log("Opportunity Correct");
					opportunityAnswered = true;
					opportunityCorrect = true;
					//_answerCounters = new List<FilterMovieClip>();

					//debug("Opportunity " + currentOpportunity + ": Opportunity will be marked correct.");
				}
				opportunityComplete = true;				
				
				
				TimerUtils.SetTimeout(1, DelayComplimentCallAfterDing);
				PlayCorrectSound();
				
				AddNewActionData( formatActionData( "tap", 0, Vector2.zero, true ) );
			}
			//Opportunity Incorrect
			else
			{
				//dispatchEvent(new SkillEvent(SkillEvent.INCORRECT,true,false));
				
				_answerCounters = new List<FilterMovieClip>();
				
				numCounters = answerArray.GetLength(0);
				
				disableCounters();
				
				//Debug.Log("Opportunity incorrect");
				opportunityAnswered = true;
				opportunityCorrect = false;
				//debug("Opportunity " + currentOpportunity + ": Opportunity will be marked incorrect.");
				
				AddNewActionData( formatActionData( "tap", 0, Vector2.zero, false ) );
				
				attempt++;
				//debug("Opportunity " + currentOpportunity + ": Failed attempt " + attempt.toString());
				if(attempt >= 3)
				{
					//debug("Opportunity " + currentOpportunity + ": Continuing to next opportunity.");
					opportunityComplete = true;
					
					for(int i = 0; i< numCounters; i++)
					{
						_answerCounters.Add(_counters[i]);
					}
						
					updateCounters();
					
					if(_level == SkillLevel.Tutorial)
					{
						timeoutCount = 1;
						onTimerComplete();
					}
					else
					{
						SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
						AudioClip clip;
						string number = "";
						if(numCounters == 2)
							number = "Words-In";	
						if(numCounters == 3)
							number = "Three";
						if(numCounters == 4)
							number = "Four";
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_60 +number + "-" + _currentSentence.ToLower().Replace(" ","-"));
						bundle.AddClip(clip, "SENTENCE_COUNT", clip.length);
						SoundEngine.Instance.PlayBundle(bundle);
						
						float waitTime = clip.length;
						for( int i = 0 ; i < answerArray.GetLength(0) ; i++)
						{
							clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_82 + answerArray[i].ToLower() );
							waitTime -= clip.length *.6f;
						}
						glowCount = 0;
						_trackedTimers.Add(TimerUtils.SetTimeout(waitTime, glowInTime));
					}
					//Tweener.addTween(this, {time:0.35, onComplete:showAnswerCounter});
				}
				else
				{
					PlayIncorrectSound();
					TimerUtils.SetTimeout(1, DelayCriticismCallAfterDing);
					//dispatchEvent(new SkillEvent(SkillEvent.CRITICISM_REQUEST,true,false));
				}
			}
			
		}
		
		public void CriticismContinue()
		{
			if(opportunityComplete == false)
			{
				enableCounters();
			}
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;
			clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_10);
			bundle.AddClip(clip, "NOW_LETS", clip.length);
			SoundEngine.Instance.PlayBundle(bundle);
		}
		
		public void StartPageAnimation()
		{
			//Debug.Log ("StartingPageAnimation");
			
			//If book is faded out, fade in
			if(bookContainer.alpha == 0) 
				Tweener.addTween(bookContainer, Tweener.Hash("time", FADE_TIME, "alpha", 1.0f, "transition", Tweener.TransitionType.easeInOutQuint));
			
			//show page turn
			pageAnimation.Root.visible = true;
			
			//Fade out unwated visuals
			Tweener.addTween(pictures_mc, Tweener.Hash("time", FADE_TIME, "alpha", 0f, "transition", Tweener.TransitionType.easeInOutQuint));
			Tweener.addTween(counterContainer, Tweener.Hash("time", FADE_TIME, "alpha", 0f, "transition", Tweener.TransitionType.easeInOutQuint));
			
			//Fade in line cover
			TweenerObj tempTweener =  Tweener.addTween(lineCover_mc, Tweener.Hash("time", FADE_TIME, "alpha", 1f, "transition", Tweener.TransitionType.easeInOutQuint));
			
			//Play Animation
			tempTweener.OnComplete(pageAnimation.Show);

		}
		
		public void EndPageAnimation()
		{
			//Page has been turned, fade in counters and picture
			Tweener.addTween(pictures_mc, Tweener.Hash("time", FADE_TIME, "alpha", 1f, "transition", Tweener.TransitionType.easeInOutQuint));
//			Tweener.addTween(counterContainer, Tweener.Hash("time", FADE_TIME, "alpha", 1f, "transition", Tweener.TransitionType.easeInOutQuint));
			counterContainer.alpha = 1.0f;
			
			foreach(FilterMovieClip fmc in _counters)
			{
				if( fmc != null)
				{
					counterContainer.removeChild(fmc);
					counterContainer.addChild(fmc);
				}
			}

			//Fade out line cover
			TweenerObj tempTweener = Tweener.addTween(lineCover_mc, Tweener.Hash("time", FADE_TIME, "alpha", 0f, "transition", Tweener.TransitionType.easeInOutQuint));
			
//			foreach(FilterMovieClip fmc in _counters) fmc.alpha = 1.0f;
			
			//hide page turn
			pageAnimation.Root.visible = false;
			
			//Call the next opportunity when fading is complete
			tempTweener.OnComplete(OpportunityStart);
		}
		
		/// <summary>
		/// Disables the card buttons.
		/// </summary>
		private void disableCounters()
		{
			for(int i = 0; i < _counters.Count; i++)
			{
				if( _counters[i] != null ) _counters[i].removeEventListener( MouseEvent.CLICK, onCounterClick );
			}
		}
		
		/// <summary>
		/// Enables the card buttons.
		/// </summary>
		private void enableCounters()
		{
			for(int i = 0; i < _counters.Count; i++)
			{
				if( _counters[i] != null ) _counters[i].addEventListener( MouseEvent.CLICK, onCounterClick );
			}
			startTimer();
		}
		
		//Adding and removing event listeners to the pageflip as functions so that the tutorial can access them at very specific points
		public void AddListenerPageFlipSubmit()
		{
			pageFlip.target.addEventListener(MouseEvent.MOUSE_DOWN, onSubmit);
			if(_level == SkillLevel.Tutorial)
			{
				canPlayerInteract = true;
			}
			
		}
		
		public void RemoveListenerPageFlipSubmit()
		{
			pageFlip.target.removeEventListener(MouseEvent.MOUSE_DOWN, onSubmit);
		}
		
		private void enableInteractions()
		{
			enableCounters();
			pictures_mc.addEventListener(MouseEvent.MOUSE_DOWN, onPictureDown);
			if(_level != SkillLevel.Tutorial)
				MarkInstructionEnd();
		}
		
		private void disableInteractions()
		{
			disableCounters();
			RemoveListenerPageFlipSubmit();
			pictures_mc.removeEventListener(MouseEvent.MOUSE_DOWN, onPictureDown);
			MarkInstructionStart();
			
		}
		
		private void addCounterGlow(FilterMovieClip filterMC)
		{
			filterMC.AddFilter( FiltersFactory.GetScaledYellowGlowFilter());
			filterMC.filters[0].Visible = true;
			
			counterContainer.removeChild(filterMC);
			counterContainer.addChild(filterMC);
		}
		
		private void removeCounterGlow()
		{
			foreach(FilterMovieClip fmc in _counters)
			{
				if(fmc.filters.Count > 0) fmc.filters[0].Visible = false;	
			}
		}
		
		private void solveForPlayer() //increments through steps based off of a timer 
		{
			string[] answerArray = _currentSentence.Split(' ');
			
			opportunityComplete = true;
			
			if(stepCounter == 0)
			{
				//Play Audio
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				AudioClip clip;
				string number = "";
				if(answerArray.GetLength(0) == 2)
					number = "Words-In";	
				if(answerArray.GetLength(0) == 3)
					number = "Three";
				if(answerArray.GetLength(0) == 4)
					number = "Four";
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_60 +number + "-" + _currentSentence.ToLower().Replace(" ","-"));
				bundle.AddClip(clip);
				SoundEngine.Instance.PlayBundle(bundle);
				
				stepTime = Time.time + clip.length;

				_glowDelays = new Stack<float> ();
				float waitTime = clip.length;
				for( int i = 0 ; i < answerArray.GetLength(0) ; i++)
				{
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_82  + answerArray[i].ToLower() );
					waitTime -= clip.length;
					_glowDelays.Push(clip.length); 
				}
				glowCount = 0;
				_trackedTimers.Add(TimerUtils.SetTimeout(waitTime, glowUsingTimerArray));
			}
			
			
			if(stepCounter < _counters.Count)
			{
				int answerIndex = _answerCounters.Count;
				
				//If there are more answer counters than there should be, fade out extra
				if(_answerCounters.Count >= answerArray.Length)
				{
					for(int i = 0 ; i < _answerCounters.Count ; i++)
					{
						if(i < answerArray.Length)
						{
							//addCounterGlow(_answerCounters[i]);	
						}
						else	
						{
							Tweener.addTween(_answerCounters[i], Tweener.Hash("time", FADE_TIME, "alpha", 0f, "transition", Tweener.TransitionType.easeInOutQuint));
						}
						
					}

					stepCounter = _counters.Count;
					_trackedTimers.Add(TimerUtils.SetTimeout(FADE_TIME, solveForPlayer));
					return;	
				}
				else
				{
					foreach(FilterMovieClip answerFilterMC in _answerCounters)
					{
						//addCounterGlow(answerFilterMC);	
					}
				}
				
				if(answerIndex < answerArray.Length && !_answerCounters.Contains(_counters[stepCounter]))
				{
					//Set the position of the counter
					//targetX = (adjustedAnswerLineBase.x) + (counter_mc.width + COUNTER_SPACING) * answerIndex; //First position = target position + half width of counter adjustment
					//targetY = adjustedAnswerLineBase.y - counter_mc.height * .5f;
					_answerCounters.Add(_counters[stepCounter]);
					//Tweener.addTween(_counters[stepCounter], Tweener.Hash("time",COUNTER_TRANSITION_TIME + SOLVE_FOR_PLAYER_BUFFER, "x",targetX, "y", targetY, "transition", Tweener.TransitionType.easeInOutQuint));
					//addCounterGlow(_counters[stepCounter]);
				}
				
				updateCounters(true);
				
				_trackedTimers.Add(TimerUtils.SetTimeout(COUNTER_TRANSITION_TIME, solveForPlayer));
				stepCounter++;
				return;
			}
			
			if(stepCounter == _counters.Count)
			{
				stepCounter++;
				if(Time.time < stepTime)
				{
					_trackedTimers.Add(TimerUtils.SetTimeout(stepTime - Time.time, solveForPlayer));
					return;
				}
			}
			
			if(stepCounter == _counters.Count + 1)
			{
				//Debug.Log ("OPPORTUNITY DONE");
				opportunityCorrect = false;
				opportunityComplete = true;
//				dispatchEvent(new SkillEvent(SkillEvent.CRITICISM_REQUEST,true,false));
				stepCounter = 0;
//				nextOpportunity();
				
				fadeOutContainers();
				
				
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, 0) );
			}
		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = _currentSentence;
			
			foreach(string s in availableSentences)
			{
				data = data + "-" + s;
			}
			
			return data;
		}
		
		private void parseSessionData (string data)
		{
			availableSentences = new List<string>();
			
			string[] sData = data.Split('-');
			
			foreach( string s in sData )
			{
				availableSentences.Add(s);
			}
		}
		
		public static List<string> audioNames = new List<string>
		{	"",
			"Narration/1A-Story Introduction Audio/1A-StoryIntroduction",
			"Narration/1A-Skill Instruction Audio/1A-TT-Instruction-Henry",
			"",
			"",
			"Narration/1A-Skill Instruction Audio/1A-Now-its-your-turn",
			"Narration/1A-Skill Instruction Audio/1A-when-your-finished",
			"Narration/1A-Skill Instruction Audio/1A-touch-the-picture",
			"Narration/1A-Skill Instruction Audio/1A-touch-green-button",
			"Narration/1A-Skill Instruction Audio/1A-when-you-have-touched",
			"Narration/1A-Skill Instruction Audio/1A-let-the-listening-game-begin",
			"Narration/1A-Skill Instruction Audio/1A-try-again",
			"Narration/1A-Skill Instruction Audio/1A-we-need-two-counters-fish-swim",
			"Narration/1A-Skill Instruction Audio/1A-fish-swim",
			"Narration/1A-Skill Instruction Audio/1A-fish",
			"Narration/1A-Skill Instruction Audio/1A-swim",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-goats-leap",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-boats-float",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-plants-grow",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-kittens-sleep",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-water-drips",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-clocks-tick",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-birds-fly",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-monkeys-climb",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-turtles-crawl",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-soup-cooks",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-ann-drinks-water",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-comb-my-hair",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-he-brushes-teeth",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-he-is-tall",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-i-drink-milk",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-mom-eats-food",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-she-jumps-high",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-the-teacher-reads",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-his-house-is-old",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-my-shoes-are-dirty",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-our-car-is-new",
			"Narration/1A-Skill Instruction Audio/1A-Sentence/1A-red-apples-taste-good",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-goats-leap",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-ann-drinks-water",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-boats-float",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-plants-grow",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-kittens-sleep",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-water-drips",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-clocks-tick",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-comb-my-hair",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-monkeys-climb",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-turtles-crawl",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-soup-cooks",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-birds-fly",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-he-brushes-teeth",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-he-is-tall",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-i-drink-milk",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-she-jumps-high",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-the-teacher-reads",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-mom-eats-food",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-his-house-is-old",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-my-shoes-are-dirty",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-our-car-is-new",
			"Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-red-apples-taste-good",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-goats-leap",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-boats-float",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-plants-grow",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-kittens-sleep",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-water-drips",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-clocks-tick",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-birds-fly",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-monkeys-climb",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-turtles-crawl",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-soup-cooks",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-ann-drinks-water",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-comb-my-hair",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-he-brushes-teeth",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-he-is-tall",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-i-drink-milk",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-mom-eats-food",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-she-jumps-high",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-the-teacher-reads",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-his-house-is-old",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-my-shoes-are-dirty",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-our-car-is-new",
			"Narration/1A-Reinforcement Audio/1A-Counters-For-red-apples-taste-good",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-soup",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-tick",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-fly",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-goats",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-boats",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-plants",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-kittens",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-water",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-clocks",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-leap",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-float",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-birds",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-monkeys",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-turtles",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-crawl",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-drips",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-grow",
			"",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-climb",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-cooks",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-sleep",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-ann",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-apples",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-are",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-brushes",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-car",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-comb",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-dirty",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-drink",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-drinks",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-eats",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-food",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-good",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-hair",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-he",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-high",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-his",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-house",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-I",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-is",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-jumps",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-milk",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-mom",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-my",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-new",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-old",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-our",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-red",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-she",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-shoes",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-tall",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-taste",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-teacher",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-teeth",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-the",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-water",
			"/User-Earns-Artifact",
			"Narration/1A-Reinforcement Audio/1A-Single Words/1A-reads"
		};
	}

	public class SentenceSegmentingSounds
	{
		public static string SOUND_1 = "Narration/1A-Story Introduction Audio/1A-Story-Introduction";
		public static string SOUND_2 = "Narration/1A-Skill Instruction Audio/1A-TT-Instruction-Henry";
		public static string SOUND_3 = "Narration/1A-Skill Instruction Audio/1A-Now-its-your-turn";
		public static string SOUND_5 = "Narration/1A-Skill Instruction Audio/1A-Now-its-your-turn";
		public static string SOUND_6 = "Narration/1A-Skill Instruction Audio/1A-when-your-finished";
		public static string SOUND_7 = "Narration/1A-Skill Instruction Audio/1A-touch-the-picture";
		public static string SOUND_8 = "Narration/1A-Skill Instruction Audio/1A-touch-green-button";
		public static string SOUND_9 = "Narration/1A-Skill Instruction Audio/1A-when-you-have-touched";
		public static string SOUND_10 = "Narration/1A-Skill Instruction Audio/1A-let-the-listening-game-begin";
		public static string SOUND_11 = "Narration/1A-Skill Instruction Audio/1A-try-again";
		public static string SOUND_14 = "Narration/1A-Skill Instruction Audio/1A-fish";
		public static string SOUND_15 = "Narration/1A-Skill Instruction Audio/1A-swim";
		public static string SOUND_16 = "Narration/1A-Skill Instruction Audio/1A-Sentence/1A-";
		public static string SOUND_38 = "Narration/1A-Skill Instruction Audio/1A-Touch Counters/1A-Touch-";
		public static string SOUND_60 = "Narration/1A-Reinforcement Audio/1A-";
		public static string SOUND_82 = "Narration/1A-Reinforcement Audio/1A-Single Words/1A-";
	}

}