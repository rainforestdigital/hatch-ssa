using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.geom;

namespace SSGCore
{
	public class DeletingOnsetAndRime : BaseSkill
	{

		
		private DisplayObjectContainer container;

		private FilterMovieClip sign;
		private FilterMovieClip rabbitTarget;
//		private FilterMovieClip rabbit_HitBox;
		private DeletingOnsetAndRimeTutorial tutorial;

		private FilterMovieClip currentVegetable;

		private float baseScale = 1;
		private float[] offset = new float[]{0,0};

		private Platforms selectedPlatform;

		private DOnsetAndRimeOpp[] TTOpps = new DOnsetAndRimeOpp[]
		{
			new DOnsetAndRimeOpp("Cat", "C", "at", missingType.onset)
		};

		private DOnsetAndRimeOpp[] EGOpps = new DOnsetAndRimeOpp[] 
		{
			new DOnsetAndRimeOpp("Bag", "B", "ag", missingType.onset),
			new DOnsetAndRimeOpp("Bed", "B", "ed", missingType.onset),
			new DOnsetAndRimeOpp("Cat", "C", "at", missingType.onset),
			new DOnsetAndRimeOpp("Fan", "F", "an", missingType.onset),
			new DOnsetAndRimeOpp("Hat", "H", "at", missingType.onset),
			new DOnsetAndRimeOpp("Jet", "J", "et", missingType.onset),
			new DOnsetAndRimeOpp("Leg", "L", "eg", missingType.onset),
			new DOnsetAndRimeOpp("Man", "M", "an", missingType.onset),
			new DOnsetAndRimeOpp("Nap", "N", "ap", missingType.onset),
			new DOnsetAndRimeOpp("Pan", "P", "an", missingType.onset)
		};

		private DOnsetAndRimeOpp[] DGOpps = new DOnsetAndRimeOpp[] 
		{
			new DOnsetAndRimeOpp("Ball", "B", "all", missingType.onset),
			new DOnsetAndRimeOpp("Bat", "B", "at", missingType.onset),
			new DOnsetAndRimeOpp("Dog", "D", "og", missingType.onset),
			new DOnsetAndRimeOpp("Cup", "C", "up", missingType.onset),
			new DOnsetAndRimeOpp("Fig", "F", "ig", missingType.onset),
			new DOnsetAndRimeOpp("Log", "L", "og", missingType.onset),
			new DOnsetAndRimeOpp("Mat", "M", "at", missingType.onset),
			new DOnsetAndRimeOpp("Pen", "P", "en", missingType.rime),
			new DOnsetAndRimeOpp("Pot", "P", "ot", missingType.rime),
			new DOnsetAndRimeOpp("Sad", "S", "ad", missingType.rime)
		};

		private DOnsetAndRimeOpp[] DDOpps = new DOnsetAndRimeOpp[] 
		{
			new DOnsetAndRimeOpp("Bug", "B", "ug", missingType.onset),
			new DOnsetAndRimeOpp("Bun", "B", "un", missingType.onset),
			new DOnsetAndRimeOpp("Hen", "H", "en", missingType.onset),
			new DOnsetAndRimeOpp("Mud", "M", "ud", missingType.onset),
			new DOnsetAndRimeOpp("Mug", "M", "ug", missingType.onset),
			new DOnsetAndRimeOpp("Nut", "N", "ut", missingType.rime),
			new DOnsetAndRimeOpp("Pig", "P", "ig", missingType.rime),
			new DOnsetAndRimeOpp("Red", "R", "ed", missingType.rime),
			new DOnsetAndRimeOpp("Sun", "S", "un", missingType.rime),
			new DOnsetAndRimeOpp("Web", "W", "eb", missingType.rime)
		};

		private DOnsetAndRimeOpp[] availableOptions;
		private List<int> usedList;
		private DOnsetAndRimeOpp currentOpp;

		private List<string> onsetDecoys = new List<string> (){"b", "c", "d", "f", "h", "j", "l", "m", "n", "p", "r", "s", "w"};
		private List<string> rimeDecoys = new List<string> (){"ag", "all", "at", "ed", "ug", "un", "up", "og", "an", "ig", "en", "et", "ud", "ap", "ut", "ot", "ad", "eb"};

		private List<string> vegetableOptions;
		private int correctVegetableIndex = 0;

		private List<string> vegetableNames = new List<string>() {"Asparagus", "Beet", "Bellpepper", "Broccoli", "Cabbage", "Cantaloupe", "Carrot", "Cauliflower", "Eggplant", "Lettuce", "Onion", "Potato", "Pumpkin", "Radish", "Squash", "Strawberry", "Tomato", "Turnip", "Watermelon", "Yam"};
		private List<FilterMovieClip> vegetables;

		private string audioPrecursor = "1F-";
		private string audioFolder_Reinforcement 		= "Narration/1F-Reinforcement Audio/";
		private string audioFolder_SkillInstruction 	= "Narration/1F-Skill Instructions Audio/";
		private string audioFolder_InitialInstructions 	= "Narration/1F-Skill Instructions Audio/1F-Initial Instructions/";
		private string audioFolder_Onset 				= "Narration/1F-Skill Instructions Audio/1F-Onset Veggie Sounds/";
		private string audioFolder_Rime 				= "Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/";

		private const string VEGETABLES_ENABLED = "Enable Vegetables";
		private const string INCORRECT_SKILL_AUDIO_PLAYED = "Incorrect";
		private const string SIGN_HIGHLIGHT_OFF = "Highlight sign off";
		private const string INTRODUCE_VEGETABLES = "Introduce Vegetables";
		private const string VEG_SOUND_COMPLETED = "Veg Sound Completed";
		private const string REPEAT_VEGETABLE = "Repeat Instruction";

		private const string HIGHLIGHT_CORRECT_ANSWER = "Highlight Correct Answer";

		private const string REPEAT_TUTORIAL_TEXT = "Repeat tutorial text";
		private const string TUTORIAL_LEFT_VEG_HIGHLIGHT = "Highlight tutorial left veg";

		private const string SIGN_GLOW_OFF = "Turn glow sign off";
		private const string CLOSE_VEGETABLE_MOUTH = "Close Vegetable Mouth";

		private const string RESTART_TIMER = "Restart Timer";

		private const string RESTART = "Restart";

		private bool pictureFadeOut = false;
		private bool pictureFadedIn = false;

		private int currentIntroVegIndex = 0;

//		private TimerObject inactivityTimer;
//		private int inactivityCounter = 0;
		public bool restarted;

		private int playerSolveStep = 0;

		private string currentVegetableType = "";

		private Vector2 targetLoc;

		public DeletingOnsetAndRime (SkillLevel level, SessionInfo  currentSession) : base(level, currentSession)
		{

			BACKGROUND = "Background";
			ARTIFACT = "Artifact";

			selectedPlatform = PlatformUtils.GetPlatform ();
			//selectedPlatform = Platforms.ANDROID;
			PreCacheAssets();
		}
		
		private void PreCacheAssets()
		{
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle("DeletingOnsetAndRime", OnAssetsLoaded));
		}
		
		void OnAssetsLoaded(AssetBundle bundle)
		{
			if( bundle != null )
			{
				Init( null );
			}
		}
		
		public override void Dispose ()
		{
			base.Dispose();
			
			if( stage != null )
			{
				stage.removeEventListener(MouseEvent.MOUSE_MOVE, vegetableMove);
				stage.removeEventListener(MouseEvent.MOUSE_UP, vegetableUp);
			}

			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			
			if( sign != null )
			{
				sign.removeEventListener(MouseEvent.MOUSE_DOWN, signClicked);
				sign.Destroy ();
			}
			if( rabbitTarget != null )
				rabbitTarget.Destroy ();

			destroyVegetables ();

			if (tutorial != null) {
				tutorial.Dispose();
			}
		}
		
		public override void Init (MovieClip parent)
		{
			base.Init (parent);

			float _width = Screen.width;
			float _height = Screen.height;

			container = new DisplayObjectContainer();
			container.width = _width;
			container.height = _height;

			container.alpha = 0;
			container.visible = false;
			addChild(container);
			
			Resize();
			setWordList (_level);

			//generateTutorial ();
			generateSign ("Bag");
			generateTarget ();

			targetLoc = new Vector2 (rabbitTarget.x, rabbitTarget.y);
			setVegetableTarget_Platform ();

			container.visible = true;

			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;

			usedList = new List<int>();
			MarkInstructionStart ();
			//setVegetables ();
			
			switch(_level)
			{
			case SkillLevel.Tutorial:
				totalOpportunities = 1;
				if(resumingSkill)
					nextOpportunity();
				else{

					tutorial = new DeletingOnsetAndRimeTutorial();
					tutorial.View.addEventListener( TutorialEvent.START, onTutorialStart );
					tutorial.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
					tutorial.parentRef = this;
					tutorial.Resize(Screen.width, Screen.height);

				}
				break;
			default:
				totalOpportunities = 10;
				if(resumingSkill)
				{
					parseSessionData( currentSession.sessionData );
					//	nextOpportunity();
				}

				break;
			}

			if ((_level == SkillLevel.Developed) || (_level == SkillLevel.Completed)) {
				pictureFadeOut = true;
			}

			if(resumingSkill && _level != SkillLevel.Tutorial)
			{
				parseSessionData( currentSession.sessionData );
			}
		}

		private void restart () {
			restarted = true;
//			inactivityCounter = 0;
			stopTimer ();

			hideContainer ();
			currentOpportunity = 0;

			tutorial.View.addEventListener( TutorialEvent.START, onTutorialStart );
			tutorial.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );

			tutorial.OnStart ();

			MarkInstructionStart ();
		}

		private void setVegetableTarget_Platform() {

			targetLoc.y += 70f;

			switch (selectedPlatform) {
			case Platforms.WIN:
				targetLoc.y += 70f;
				break;
			}
		}

		public override void OnAudioWordComplete ( string word)
		{
			switch (word) {
			case THEME_INTRO:
				if (!currentSession.skipIntro)
					playSound ("story-introduction", INTRO);
				else
					OnAudioWordComplete (INTRO);
				break;
				
			case INTRO:
				if (_level == SkillLevel.Tutorial) {
					tutorial.OnStart ();
				} else {
					nextOpportunity ();
				}
				break;

			case INTRODUCE_VEGETABLES:
				introduceNextVeg();

				break;

			case VEG_SOUND_COMPLETED:
				// Shut mouth
				currentVegetable.Target.gotoAndStop(currentVegetableType.ToUpper() + "1");
				moveVegBack();
				break;

			case VEGETABLES_ENABLED:
				// Turn off highlight
				glowOff(sign);
				// Enable vegetables
				enableVegetables();
				enableSign();

				startTimer ();


				break;

			case CLICK_CORRECT:
				dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
				break;
				
			case CLICK_INCORRECT:
				dispatchEvent( new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
				break;

			case THEME_COMPLIMENT:
				playSound(getReinforcement(currentOpp), YOU_FOUND);
				startVeggieTimer();
				break;
				
			case THEME_CRITICISM:

				if (pictureFadeOut) {
					if (!pictureFadedIn) {
						fadeOutPictorial();
					}
				}
				
				glowOn(currentVegetable);
				currentVegetable.Target.gotoAndStop (currentVegetableType.ToUpper() + "2");
				int vegetableIndex = vegetables.IndexOf(currentVegetable);
				if (currentOpp.missing == missingType.onset) {
					playSound (getOnsetSound (vegetableOptions [vegetableIndex]), YOU_FOUND_INCORRECT);
				} else {
					playSound (getRimeSound (vegetableOptions [vegetableIndex]), YOU_FOUND_INCORRECT);
				}

				break;
				
			case YOU_FOUND:

				currentVegetable.Target.gotoAndStop (currentVegetableType.ToUpper() + "1");
				glowOff(currentVegetable);

				if(_level == SkillLevel.Tutorial)
				{
					playSound(audioFolder_SkillInstruction + audioPrecursor + "lets-keep-havesting-sounds", NOW_LETS);
					break;
				} 
				goto case NOW_LETS;

				
			case NOW_LETS:
				//clearAnswer();
				fadeOutContainer();
				if(opportunityCorrect)
				{
					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
				}
				else
				{
					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
				}
				break;
				
			case YOU_FOUND_INCORRECT:

				fadeOutVegetable(currentVegetable);
				glowOn(sign);
				playSound(getInitialInstruction(currentOpp), SIGN_HIGHLIGHT_OFF);

				break;

			case INCORRECT_SKILL_AUDIO_PLAYED:


				break;

			case SIGN_HIGHLIGHT_OFF:
				toggleEnable(true);
				startTimer();
				glowOff(sign);
				break;

			case REPEAT_TUTORIAL_TEXT:
				glowOff(sign);
				glowOff(vegetables[0]);
				playSound(audioFolder_SkillInstruction + audioPrecursor + "touch-the-vegetables", TUTORIAL_LEFT_VEG_HIGHLIGHT);
				break;

			case TUTORIAL_LEFT_VEG_HIGHLIGHT:
				startTimer();

				break;

			case RESTART:
				if (_level == SkillLevel.Tutorial) {
					restart ();
				} else {
					playInstruction(false);
				}
				break;

			case PLEASE_FIND:
				playInstruction(false);
				break;

			case REPEAT_VEGETABLE:
				repeatSkillInstruction();
				break;

			case RESTART_TIMER:
				restartTimer();
				break;

			case HIGHLIGHT_CORRECT_ANSWER:
				highlightCorrectAnswer();
				break;

			case SIGN_GLOW_OFF:
				startTimer();
				glowOff(sign);
				break;

			case CLOSE_VEGETABLE_MOUTH:
				currentVegetable.Target.gotoAndStop (currentVegetableType.ToUpper() + "1");
				break;
			}
		}


		
		public override void nextOpportunity()
		{
			opportunityAnswered = false;
			opportunityCorrect = false;
			opportunityComplete = false;
			timeoutCount = 0;
			pictureFadedIn = false;
			rabbitTarget.Target.gotoAndStop (1);
			currentIntroVegIndex = 0;

			currentVegetable = null;

			if(currentOpportunity == totalOpportunities)
			{
				tallyAnswers();
				return;
			}
			
			if(_level == SkillLevel.Tutorial)
			{
				//currentWord = "BALL";
				currentOpp = availableOptions[0];
				changeSignImage(currentOpp.targetWord);
				setVegetables ();
				deleteVeg(1);
				vegetableOptions[0] = "C";
				correctVegetableIndex = 0;
			}
			else
			{

				int randomIndex = 0;
				if(resumingSkill && usedList.Count > 0)
				{
					randomIndex = usedList[ usedList.Count - 1 ];
					resumingSkill = false;
				}
				else
				{
					bool indexFound = false;
	
					while (!indexFound) {
						randomIndex = Random.Range(0, availableOptions.Length);
						indexFound = checkIfAvailable(randomIndex);
					}
	
					usedList.Add(randomIndex);
				}

				currentOpp = availableOptions[randomIndex];
				// Set the sign
				string displayWord = currentOpp.targetWord;
				
				if (pictureFadeOut) {
					displayWord += "2";
				}
				changeSignImage(displayWord);
				setVegetables ();
			}

			currentOpportunity++;
			
			if(_level == SkillLevel.Tutorial)
			{
				//playSound( "Instruction/now-its-your-turn", NOW_ITS );
			}
			else if(_level == SkillLevel.Emerging)
			{
				introduce();
			}

			fadeInContainer();
			dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_START, true, false) );
			
			AddNewOppData( audioNames.IndexOf( getInitialInstruction (currentOpp) ) );
			
			AddNewObjectData( formatObjectData( "basket", rabbitTarget.parent.localToGlobal(new Vector2( rabbitTarget.x, rabbitTarget.y )) ) );
			
			foreach( FilterMovieClip fmc in vegetables )
			{
				Vector2 point = fmc.parent.localToGlobal(new Vector2( fmc.x, fmc.y ));
				AddNewObjectData( formatObjectData( fmc.Target.currentLabel, point, vegetables.IndexOf(fmc) == correctVegetableIndex ) );
			}
			
			Debug.LogWarning( "Session Data: " + currentSession.SerializedData.toString() );
		}

		private bool checkIfAvailable(int index) {
			if (usedList.Contains(index)) {
				return false;
			}

			return true;
		}

		public void Resize()
		{

			resize (Screen.width, Screen.height);

		}

		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = "";
			
			foreach(int i in usedList)
			{
				if(data != "")
					data = data + "-";
				data = data + i.ToString();
			}
			
			return data;
		}
		
		private void parseSessionData (string data)
		{
			usedList = new List<int>();
			
			string[] sData = data.Split('-');
			
			foreach( string s in sData )
			{
				usedList.Add( int.Parse(s) );
			}
		}

		private void setWordList(SkillLevel _suggestedLevel) {
			switch (_suggestedLevel) {
			case SkillLevel.Tutorial:
				availableOptions = TTOpps;
				break;

			case SkillLevel.Emerging:
				availableOptions = EGOpps;
				break;

			case SkillLevel.Developing:
				availableOptions = DGOpps;
				break;

			case SkillLevel.Developed:
			case SkillLevel.Completed:
				availableOptions = DDOpps;
				break;
			}
		}

		private void generateSign(string displayImage) {
			sign = new FilterMovieClip (MovieClipFactory.CreateDeletingOnsetAndRimeSign ());
			sign.scaleX = sign.scaleY = baseScale;
			sign.x = (container.width * baseScale) * 0.5f;
			sign.y = (container.height * baseScale) * 0.4f;

			if (selectedPlatform == Platforms.WIN) {
				sign.x += 16f;
			}

			container.addChild (sign);
			changeSignImage (displayImage);
		}

		private void changeSignImage (string displayImage) {
			sign.Target.gotoAndStop (displayImage.ToUpper());
		}

		private void fadeOutPictorial() {
			pictureFadedIn = true;
			Tweener.addTween (sign.Target.getChildAt (1), Tweener.Hash ("time", 0.25f, "alpha", 0)).OnComplete(switchImage);
		}

		private void fadeInPictorial() {
			Tweener.addTween (sign.Target.getChildAt (1), Tweener.Hash ("time", 0.25f, "alpha", 1));
		}

		private void switchImage() {
			changeSignImage (currentOpp.targetWord);
		}

		private void generateTarget() {
			rabbitTarget = new FilterMovieClip (MovieClipFactory.CreateDeletingOnsetAndRimeTarget ());
			rabbitTarget.scaleX = rabbitTarget.scaleY = baseScale;

			float xLoc = (container.width * 0.83f) * baseScale;
			float yLoc = 0f;
			switch (selectedPlatform) {
			case Platforms.ANDROID:
				xLoc -= 85f;
				break;

			case Platforms.WIN:
				xLoc += 56f;
				//yLoc -= 8f;
				break;
			}

			rabbitTarget.x = xLoc;
			rabbitTarget.y = ((container.height * 0.45f) * baseScale) + yLoc;
			rabbitTarget.y -= (rabbitTarget.height * baseScale) * 0.11f;
			rabbitTarget.Target.stop ();
			container.addChild (rabbitTarget);

		}

		/*
		private void generateTutorial() {
			tutorial = new FilterMovieClip (MovieClipFactory.CreateDeletingOnsetAndRimeTutorial ());
			tutorial.scaleX = tutorial.scaleY = baseScale;
			tutorial.x = 0f;
			tutorial.y = 0f;
			//rabbitTarget.Target.gotoAndPlay ();
			container.addChild (tutorial);
		}*/

		private void setVegetables() {

			destroyVegetables ();

			vegetables = new List<FilterMovieClip> ();

			currentVegetableType = vegetableNames [Random.Range (0, vegetableNames.Count)];

			int numberOfVegetables = 0;
			switch(currentOpp.missing)
			{
			case missingType.onset:
				numberOfVegetables = 3;

				if (_level == SkillLevel.Emerging) {
					numberOfVegetables = 2;
				}

				break;
			
			default:
				numberOfVegetables = 2;
				break;
			} 

			if (_level == SkillLevel.Tutorial) {
				numberOfVegetables = 2;
				currentVegetableType = "Radish";
			}

			for (int vegetableCounter = 0; vegetableCounter < numberOfVegetables; vegetableCounter++) {
				createVegetable(currentVegetableType.ToUpper() + "1");
			}

			arrangeVegetables ();


			// Set the decoys

			correctVegetableIndex = Random.Range (0, numberOfVegetables);
			vegetableOptions = new List<string> ();
			for (int counter = 0; counter < numberOfVegetables; counter++) {

				switch(currentOpp.missing) {
				case missingType.onset:
					if (counter == correctVegetableIndex) {
						vegetableOptions.Add(currentOpp.onset);
					} else {
						// Find a random that isn't the onset and hasn't been used
						string randomOnset = currentOpp.onset;

						while (randomOnset.ToUpper() == currentOpp.onset.ToUpper()) {
							randomOnset = getRandomString(vegetableOptions, onsetDecoys, currentOpp.onset);
						}

						vegetableOptions.Add ( randomOnset );
					}
					break;

				case missingType.rime:
					if (counter == correctVegetableIndex) {
						vegetableOptions.Add(currentOpp.rime);
					} else {
						// Find a random that isn't the rime and hasn't been used

						string randomRime = currentOpp.rime;

						while (randomRime.ToUpper() == currentOpp.rime.ToUpper()) {
							randomRime = getRandomString(vegetableOptions, rimeDecoys, currentOpp.rime);
						}

						vegetableOptions.Add ( randomRime );
					}
					break;
				}


			}

		}

		private void fadeOutVegetable(FilterMovieClip vegClip) {
			vegClip.removeEventListener(MouseEvent.MOUSE_DOWN, vegetableDown);
			Tweener.addTween (vegClip, Tweener.Hash ("time", 0.25f, "alpha", 0));
		}

		private string getRandomString (List<string> previousChoices, List<string> availableChoices, string correctVersion) {

			bool stringFound = false;
			string currentChoice = "";

			while (!stringFound) {
				currentChoice = availableChoices[Random.Range(0, availableChoices.Count)];

				if (currentChoice != correctVersion) {
					// Check if the choice has been used previous

					bool previousFound = false;
					foreach(string previousOption in previousChoices) {
						if (previousOption == "") {
							stringFound = true;
							break;
						} else if (previousOption == currentChoice) {
							// Find a new one
							previousFound = true;
							break;
						}
					}

					if (!previousFound) {
						stringFound = true;
					}
				}
			}

			return currentChoice;

		}

		private void createVegetable(string vegetableName) {
			FilterMovieClip fmc = new FilterMovieClip(MovieClipFactory.CreateDeletingOnsetAndRimeVegetable());
			fmc.Target.gotoAndStop (vegetableName);
			fmc.scaleX = fmc.scaleY = baseScale;

			if (selectedPlatform == Platforms.WIN) {
				fmc.scaleX = fmc.scaleY = 0.905f;
			}

			fmc.alpha = 0f;
			//fmc.ScaleCentered(baseScale);
			vegetables.Add(fmc);
			container.addChild (fmc);
		}

		private void arrangeVegetables() {

			for (int counter = 0; counter < vegetables.Count; counter++) {
				resetVegetablePosition(counter, false);
			}

		}

		private void resetVegetablePosition (int index, bool animate) {
			float pad = container.width * 0.1f;
			float vegetableWidth = (vegetables[0].Target.width * baseScale) + pad;
			float startX = (container.width/2f) - ((vegetableWidth * vegetables.Count)/2f);
			float vPos = container.height * 0.71f;

			if (selectedPlatform == Platforms.WIN) {
				if (_level == SkillLevel.Tutorial) {
					startX -= 20f;
				}

				vPos += 103f;
			}

			startX += pad;

			int counter = 0;
			while (counter < index) {
				counter++;
				startX += vegetableWidth;
			}

			// Tween it back into position

			if (animate) {
				float vegScale = baseScale;

				if (selectedPlatform == Platforms.WIN) {
					vegScale = 0.9f;
				}

				Tweener.addTween (currentVegetable, Tweener.Hash ("time", 0.25f, "x", startX, "y", vPos, "scaleX", vegScale, "scaleY", vegScale));
			} else {
				FilterMovieClip fmc = vegetables[index];
				fmc.x = startX;
				fmc.y = vPos;
			}
		}

		private void startVeggieTimer() {
			Tweener.addTween(currentVegetable, Tweener.Hash("delay", 2.5f)).OnComplete(glowVeggie);

		}

		private void glowVeggie() {
			currentVegetable.Target.gotoAndStop (currentVegetableType.ToUpper() + "2");
			glowOn (currentVegetable);
		}

		private void startVegetableIntro(int vegetableIndex) {
			currentVegetable = vegetables [vegetableIndex];
			currentVegetable.x = container.width / 2f;
			currentVegetable.y = container.height / 2f;

			Tweener.addTween (currentVegetable, Tweener.Hash ("time", 0.25f, "alpha", 1f)).OnComplete(magnify);


		}

		private void magnify() {
			Tweener.addTween (currentVegetable, Tweener.Hash ("time", 0.25f, "scaleX", currentVegetable.scaleX * 1.25f, "scaleY", currentVegetable.scaleX * 1.25f)).OnComplete(introVegSound);

		}

		private void introVegSound() {

			int vegetableIndex = vegetables.IndexOf(currentVegetable);
			string soundFile = "";
			if (currentOpp.missing == missingType.onset) {
				soundFile = getOnsetSound (vegetableOptions [vegetableIndex]);
			} else {
				soundFile = getRimeSound (vegetableOptions [vegetableIndex]);
			}

			glowOn (currentVegetable);
			currentVegetable.Target.gotoAndStop (currentVegetableType.ToUpper() + "2");
			playSound (soundFile, VEG_SOUND_COMPLETED);

		}

		private void moveVegBack() {
			glowOff (currentVegetable);
			currentIntroVegIndex++;
			resetVegetablePosition (vegetables.IndexOf (currentVegetable), true);
			Tweener.addTween (currentVegetable, Tweener.Hash ("delay", 0.4f)).OnComplete(introduceNextVeg);

		}

		private void solveForPlayer() {
			currentVegetable = vegetables [correctVegetableIndex];
			playerSolveStep++;

			switch (playerSolveStep) {
			case 1:
				glowOff(currentVegetable);
				Tweener.addTween (currentVegetable, Tweener.Hash ("time", 0.4f, "x", targetLoc.x, "y", targetLoc.y )).OnComplete(solveForPlayer);

				if ((_level == SkillLevel.Developed) || (_level == SkillLevel.Completed)) {
					if (pictureFadeOut) {
						if (!pictureFadedIn) {
							fadeOutPictorial();
						}
					}
				}

				break;

			case 2:

				// Fades out incorrect veggies
				foreach(FilterMovieClip fmc in vegetables) {
					if (fmc != currentVegetable) {
						if (fmc.alpha != 0f ) {
							fadeOutVegetable(fmc);
						}
					}
				}

				playSound(getReinforcement(currentOpp), NOW_LETS);

				Tweener.addTween(currentVegetable, Tweener.Hash("delay", 2.5f)).OnComplete(glowVeggie);


				break;

			case 3:

				break;
			}


		}



		private void introduceNextVeg() {
			// Iterating through vegetables
			if (currentIntroVegIndex < vegetables.Count) {
				startVegetableIntro(currentIntroVegIndex);
			} else {
				// No more vegetables to introduce
				currentVegetable = null;
				playInstruction(false);
			}
		}

		private void playInstruction() {
			playInstruction (true);
		}

		private void playInstruction(bool fadeInVeg) {
			// Highlight the sign

			// Play audio
			if (_level == SkillLevel.Tutorial) {
				playSound (audioFolder_SkillInstruction + "1F-drag-cat-without-at", VEGETABLES_ENABLED);
				// Do a delayed glow
				Tweener.addTween(sign, Tweener.Hash("time", 3f)).OnComplete(glowSign);
			} else {
				glowOn (sign);
				playSound (getInitialInstruction (currentOpp), VEGETABLES_ENABLED);
			}
			if (fadeInVeg) {
				fadeInVegetables ();
			}
		}

		private void glowSign() {
			glowOn (sign);
		}

		private void fadeInVegetables() {
			foreach (FilterMovieClip veg in vegetables) {
				Tweener.addTween (veg, Tweener.Hash ("time", 0.25f, "alpha", 1));
			}
		}

		private void hideVegetable(FilterMovieClip veg) {
			veg.alpha = 0f;
		}

		private void hideVegetables() {
			foreach (FilterMovieClip veg in vegetables) {
				veg.alpha = 0f;
			}
		}

		private void deleteVeg(int index) {
			FilterMovieClip fmc = vegetables [index];
			fmc.Destroy();
			container.removeChild(fmc);
		}

		private void destroyVegetables() {
			if(vegetables != null)
			{
				foreach(FilterMovieClip fmc in vegetables) {
					fmc.Destroy();
					container.removeChild(fmc);
				}
			}

		}

		private void enableVegetables() {

			foreach(FilterMovieClip fmc in vegetables)
			{
				fmc.addEventListener(MouseEvent.MOUSE_DOWN, vegetableDown);
			}

			if (currentVegetable == null) {
			}

			if (_level != SkillLevel.Tutorial) {
				MarkInstructionEnd();
			}
		}

		private void disableVegetables() {
			foreach(FilterMovieClip fmc in vegetables)
			{
				fmc.removeEventListener(MouseEvent.MOUSE_DOWN, vegetableDown);
			}
		}
		
		private bool isDrag;
		private Vector2 cardOrig;
		
		private void vegetableDown ( CEvent e )
		{

//			inactivityCounter = 0;
			playClick ();

			stopTimer ();
			MarkInstructionStart ();

			if (currentVegetable != null) {
				currentVegetable.Target.gotoAndStop (currentVegetableType.ToUpper() + "1");
			}

			timeoutCount = 0;

			currentVegetable = e.currentTarget as FilterMovieClip;
			currentVegetable.ScaleCentered(currentVegetable.scaleX * 1.25f);
			offset[0] = container.mouseX - currentVegetable.x;
			offset[1] = container.mouseY - currentVegetable.y;
			
			container.removeChild(currentVegetable);
			container.addChild(currentVegetable);
			
			stage.addEventListener(MouseEvent.MOUSE_MOVE, vegetableMove);
			stage.addEventListener(MouseEvent.MOUSE_UP, vegetableUp);
			//cardsOff();

			glowOn (currentVegetable);
			glowOff(sign);
			
			isDrag = false;
			cardOrig = new Vector2( currentVegetable.x, currentVegetable.y );
		}

		private void vegetableMove (CEvent e) {
			currentVegetable.x = container.mouseX - offset[0];
			currentVegetable.y = container.mouseY - offset[1];
			
			if( Mathf.Abs(cardOrig.x - currentVegetable.x + cardOrig.y - currentVegetable.y) > 5 ){
				isDrag = true;
			}

			if (isOnTarget()) {
				glowOn (rabbitTarget);
				//cardIsInEar = true;
			} else {
				//cardIsInEar = false;
				glowOff (rabbitTarget);
			}


		}

		private void vegetableUp (CEvent e) {

			glowOff (currentVegetable);

			stage.removeEventListener (MouseEvent.MOUSE_MOVE, vegetableMove);
			stage.removeEventListener (MouseEvent.MOUSE_UP, vegetableUp);

			submitAnswer ();

		}

		private void submitAnswer() {

			int vegetableIndex = vegetables.IndexOf (currentVegetable);

			// Logic
			if (isOnTarget ()) {
				toggleEnable(false);
				
				Vector2 dragPoint = currentVegetable.parent.localToGlobal(new Vector2(currentVegetable.x, currentVegetable.y));
				
				Tweener.addTween (currentVegetable, Tweener.Hash ("time", 0.25f, "scaleX", baseScale, "scaleY", baseScale, "x", targetLoc.x , "y", targetLoc.y));
				glowOff(rabbitTarget);

				// Determine if correct answer or not
				if (vegetableIndex == correctVegetableIndex) {
					// Correct answer!
					if(!opportunityAnswered)
					{
						opportunityAnswered = true;
						opportunityCorrect = true;
					}
					opportunityComplete = true;

					if (pictureFadeOut) {
						if (!pictureFadedIn) {
							fadeOutPictorial();
						}
					}
					PlayCorrectSound();
					rabbitTarget.Target.gotoAndStop (2);
					
					AddNewActionData( formatActionData( "move", vegetableIndex + 1, dragPoint, true ) );

				} else {
					// :( Wrong answer
					if(!opportunityAnswered)
					{
						opportunityAnswered = true;
						opportunityCorrect = false;
					}

					PlayIncorrectSound();
					
					AddNewActionData( formatActionData( "move", vegetableIndex + 1, dragPoint, false ) );

				}
			} else {

				// Play associated audio
				if (currentOpp.missing == missingType.onset) {
					playSound (getOnsetSound (vegetableOptions [vegetableIndex]), CLOSE_VEGETABLE_MOUTH);
				} else {
					playSound (getRimeSound (vegetableOptions [vegetableIndex]), CLOSE_VEGETABLE_MOUTH);
				}

				currentVegetable.Target.gotoAndStop (currentVegetableType.ToUpper() + "2");

				resetVegetablePosition (vegetableIndex, true);
				startTimer();
				MarkInstructionEnd();
				
				if( isDrag )
				{
					AddNewActionData( formatActionData( "snap", vegetableIndex + 1, Vector2.zero ) );
				}
				else
				{
					AddNewActionData( formatActionData( "tap", vegetableIndex + 1, Vector2.zero ) );
				}
			}
		}

		private bool isOnTarget() {
			float xThreshold = 60f;
			float yThreshold = 120f;

			float xDistance = Mathf.Abs (rabbitTarget.x - currentVegetable.x);
			float yDistance = Mathf.Abs (rabbitTarget.y - currentVegetable.y);
			//DebugConsole.Log ("Distance X: " + xDistance + " YDist: " + yDistance);

			switch (selectedPlatform) {
			case Platforms.IOSRETINA:
			case Platforms.WIN:
				xThreshold *= 2f;
				yThreshold *= 2f;
				break;
			}

			if ((xDistance < xThreshold) && (yDistance < yThreshold)) {
				return true;
			}

			/*
			Rectangle rTarget = rabbitTarget.getBounds (container);
			Rectangle vTarget = currentVegetable.getBounds (container);
			float intersectionAmount = rabbitTarget.getBounds (container).intersection (currentVegetable.getBounds (container)).width;

			DebugConsole.Log ("Amount: " + intersectionAmount);

			if (intersectionAmount > boundThreshold) {
				return true;
			}*/

			return false;
		}

		private void enableSign() {
			sign.addEventListener(MouseEvent.MOUSE_DOWN, signClicked);
		}

		private void disableSign() {
			sign.removeEventListener(MouseEvent.MOUSE_DOWN, signClicked);
		}

		private void signClicked (CEvent e) {
//			inactivityCounter = 0;
			timeoutCount = 0;
			stopTimer ();
			startTimer ();
			glowOn (sign);
			playSound(getInitialInstruction(currentOpp), SIGN_GLOW_OFF);

			// Close all the mouths
			foreach (FilterMovieClip veg in vegetables) {
				veg.Target.gotoAndStop (currentVegetableType.ToUpper() + "1");
			}
			
			AddNewActionData( formatActionData( "tap", 0, Vector2.zero ) );
		}

		#region Containers

		//size in should be the stage size from the flash file
		private void resize( int width, int height ) { resize( (float)width, (float)height ); }
		private void resize( float width, float height )
		{
			if( container == null )
				return;
			
			float scale = (float)Screen.width / width;
			baseScale = scale;

			if (selectedPlatform == Platforms.WIN) {
				baseScale = 0.97f;
			}

			container.scaleX = container.scaleY = baseScale;
			container.y = Screen.height - ( baseScale * height );
			container.x = 0;

			if (selectedPlatform == Platforms.WIN) {

				container.x = 27f;
				container.y = 8f;
			}

		}

		public DisplayObjectContainer getContainer() {
			return container;
		}

		private void fadeInContainer()
		{
			container.visible = true;

			if (_level == SkillLevel.Emerging) {
				Tweener.addTween (container, Tweener.Hash ("time", 0.25f, "alpha", 1));
			} else {
				Tweener.addTween (container, Tweener.Hash ("time", 0.25f, "alpha", 1)).OnComplete (playInstruction);
			}
		}
		
		private void fadeOutContainer()
		{
			Tweener.addTween(container, Tweener.Hash("time", 0.25f, "alpha", 0)).OnComplete(hideContainer);
		}

		private void hideContainer()
		{ 
			container.visible = false; 
			destroyVegetables();
		}
		#endregion

		#region Audio

		private string playSound ( string clipName ){ return playSound(clipName, ""); }
		private string playSound ( string clipName, string word )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();;
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.DELETING_ONSET_AND_RIME, clipName );
			
			if(word != "")
				bundle.AddClip(clip, word, clip.length);
			else
				bundle.AddClip(clip);
			
			SoundEngine.Instance.PlayBundle(bundle);
			
			return clipName;
		}

		private void playClick() {
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			var clip = SoundUtils.LoadGlobalSound("click");
			bundle.AddClip (clip);
			SoundEngine.Instance.PlayBundle(bundle);
		}

		private string getInitialInstruction (DOnsetAndRimeOpp oppDetails) {
			string filename = audioFolder_InitialInstructions + audioPrecursor + oppDetails.targetWord.ToLower() + "-without-";

			if (oppDetails.missing == missingType.onset) {
				filename += oppDetails.rime;
			} else {
				filename += oppDetails.onset.ToLower();
			}

			return filename;

		}

		private string getReinforcement (DOnsetAndRimeOpp oppDetails) {
			string filename = audioFolder_Reinforcement + audioPrecursor + oppDetails.targetWord.ToLower() + "-without-";
			
			if (oppDetails.missing == missingType.onset) {
				filename += oppDetails.rime + "-is-" + oppDetails.onset.ToLower();
			} else {
				filename += oppDetails.onset.ToLower() + "-is-" + oppDetails.rime;
			}

			return filename;
		}

		private string getOnset (DOnsetAndRimeOpp oppDetails) {
			string filename = audioFolder_Onset + audioPrecursor + oppDetails.onset;
			return filename;
		}

		private string getRime (DOnsetAndRimeOpp oppDetails) {
			string filename = audioFolder_Rime + audioPrecursor + oppDetails.rime;
			return filename;
		}


		private string getOnset(string word) {
			return word.Substring(0, 1);
		}

		private string getRime(string word) {
			return word.Substring (1, word.Length - 1);
		}

		private string getOnsetSoundFileName(string word) {
			string fileName = audioPrecursor + word [0];
			return fileName;
		}

		private string getRimeSoundFileName(string word) {
			string fileName = audioPrecursor + word.Substring(1, word.Length-1);
			return fileName;
		}

		private string getOnsetSound(string word) {
			string fileName = audioFolder_Onset + audioPrecursor + getOnset (word);
			return fileName;
		}

		private string getRimeSound(string word) {
			string fileName = audioFolder_Rime + audioPrecursor + word;
			return fileName;
		}

		private string getOnsetVersion (string word) {
			string onsetInstruction = audioPrecursor + word + "-without-" + getRime(word);
			return onsetInstruction;
		}

		private string getRimeVersion (string word) {
			string rimeInstruction = audioPrecursor + word + "-without-" + getOnset(word);
			return rimeInstruction;
		}

		#endregion

		private void glowOn(FilterMovieClip fmc)
		{
			while(fmc.filters.Count > 1)
			{
				fmc.filters[1].Destroy();
				fmc.filters[1].Visible = false;
				fmc.filters.RemoveAt(1);
			}
			if(fmc.filters.Count == 0)
				fmc.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
			fmc.filters[0].Visible = true;
		}

		private void glowOff(FilterMovieClip fmc)
		{
			while(fmc.filters.Count > 1)
			{
				fmc.filters[1].Destroy();
				fmc.filters[1].Visible = false;
				fmc.filters.RemoveAt(1);
			}
			if(fmc.filters.Count == 1)
				fmc.filters[0].Visible = false;
		}

		private void introduce() {
			// Fade all the vegetables out
			hideVegetables ();
			playSound (audioFolder_SkillInstruction + audioPrecursor + "listen-to-the-sounds-these-vegetables", INTRODUCE_VEGETABLES);
		}

		public override void onTutorialStart( CEvent e )
		{
			base.onTutorialStart(e);
			tutorial.View.removeEventListener( TutorialEvent.START, onTutorialStart );
			addChild(tutorial.View);
		}
		
		private void onTutorialComplete( CEvent e )
		{
			tutorial.View.removeEventListener(TutorialEvent.COMPLETE, onTutorialComplete);
			removeChild(tutorial.View);
			
			container.alpha = 1;
			nextOpportunity();
		}

		public float getBaseScale() {
			return baseScale;
		}


		public override void onTimerComplete()
		{
			base.onTimerComplete();

			stopTimer();
			//toggleEnable (false);
			timeoutCount++;
			
			string clipName = "";
			if (_level == SkillLevel.Tutorial) {
				switch(timeoutCount) {
				case 1:
					glowOn(sign);
					//glowOn(vegetables[0]);
					clipName = playSound(getInitialInstruction(currentOpp), REPEAT_TUTORIAL_TEXT);	
					break;

				case 2:
					if(restarted)
					{
						//skillRef.timeoutEnd = true;
						//correct = false;
						//dispatchEvent(new SkillEvent(SkillEvent.SKILL_COMPLETE, false, true));
						dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
						tallyAnswers();
						return;
					} 
					
					clipName = playSound(audioFolder_SkillInstruction + audioPrecursor + "listen-again", RESTART);
					break;
				}
			} else {

				switch(timeoutCount)
				{
				case 1:
					playerSolveStep = 0;
					clipName = playInstruction_Timeout();

					break;
				case 2:
					clipName = playSound(audioFolder_SkillInstruction + audioPrecursor + "touch-the-green-button", RESTART_TIMER);
					break;
				case 3:
					opportunityAnswered = true;
					opportunityCorrect = false;
					clipName = repeatSkillInstruction();
					break;
				default:
					timeoutEnd = true;
					opportunityAnswered = true;
					opportunityCorrect = false;

					toggleEnable(false);
					solveForPlayer();
					clipName = getReinforcement(currentOpp);
					break;
				}	
			}
			
			if( !string.IsNullOrEmpty( clipName ) )
			{
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf( clipName ) );
			}
		}

		private string playInstruction_Timeout() {
			
			// Play audio
			return playSound (audioFolder_SkillInstruction + audioPrecursor + "touch-the-vegetables", REPEAT_VEGETABLE);
			//


		}

		private string repeatSkillInstruction() {
			glowOn (sign);
			if (timeoutCount == 1) {
				return playSound(getInitialInstruction(currentOpp), RESTART_TIMER);
			} else {
				return playSound(getInitialInstruction(currentOpp), HIGHLIGHT_CORRECT_ANSWER);
			}
		}

		private void restartTimer() {

			startTimer ();
			toggleEnable (true);
		}

		private void highlightCorrectAnswer() {
			toggleEnable (true);
			glowOn (vegetables [correctVegetableIndex]);
			startTimer ();
		}

		private void toggleEnable(bool enabled) {
			if (enabled) {
				glowOff (sign);
				enableSign ();
				enableVegetables ();
			} else {
				disableSign();
				disableVegetables();
			}
		}
		
		private static List<string> audioNames = new List<string>
		{	"",
			"Story-Introduction",
			"Narration/1F-Skill Instructions Audio/1F-listen-which-vegetable",
			"Narration/1F-Skill Instructions Audio/1F-this-vegetable-says-b",
			"Narration/1F-Skill Instructions Audio/1F-now-you-try",
			"Narration/1F-Skill Instructions Audio/1F-drag-cat-without-at",
			"Narration/1F-Skill Instructions Audio/1F-listen-again",
			"Narration/1F-Skill Instructions Audio/1F-touch-the-vegetables",
			"Narration/1F-Skill Instructions Audio/1F-touch-the-green-button",
			"Narration/1F-Skill Instructions Audio/1F-lets-keep-havesting-sounds",
			"Narration/1F-Skill Instructions Audio/1F-listen-to-the-sounds-these-vegetables",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-bag-without-ag",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-bed-without-ed",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-cat-without-at",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-fan-without-an",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-hat-without-at",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-jet-without-et",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-leg-without-eg",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-man-without-an",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-nap-without-ap",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-pan-without-an",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-ball-without-all",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-bat-without-at",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-dog-without-og",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-cup-without-up",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-fig-without-ig",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-log-without-og",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-mat-without-at",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-pen-without-p",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-pot-without-p",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-sad-without-s",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-bug-without-ug",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-bun-without-un",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-hen-without-en",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-mud-without-ud",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-mug-without-ug",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-nut-without-n",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-pig-without-p",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-red-without-r",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-sun-without-s",
			"Narration/1F-Skill Instructions Audio/1F-Initial Instructions/1F-web-without-w",
			"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
			"Narration/1F-Skill Instructions Audio/1F-Onset Veggie Sounds/1F-b.wav",
			"",
			"Narration/1F-Skill Instructions Audio/1F-Onset Veggie Sounds/1F-c",
			"Narration/1F-Skill Instructions Audio/1F-Onset Veggie Sounds/1F-f",
			"Narration/1F-Skill Instructions Audio/1F-Onset Veggie Sounds/1F-h",
			"Narration/1F-Skill Instructions Audio/1F-Onset Veggie Sounds/1F-j",
			"Narration/1F-Skill Instructions Audio/1F-Onset Veggie Sounds/1F-l",
			"Narration/1F-Skill Instructions Audio/1F-Onset Veggie Sounds/1F-m",
			"Narration/1F-Skill Instructions Audio/1F-Onset Veggie Sounds/1F-n",
			"Narration/1F-Skill Instructions Audio/1F-Onset Veggie Sounds/1F-p",
			"", "",
			"Narration/1F-Skill Instructions Audio/1F-Onset Veggie Sounds/1F-d.wav",
			"", "", "", "", 
			"Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/1F-en",
			"Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/1F-ot",
			"Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/1F-ad.wav",
			"", "", "", "", "",
			"Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/1F-ut",
			"Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/1F-ig",
			"Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/1F-ed",
			"Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/1F-un",
			"Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/1F-eb",
			"Narration/1F-Skill Instructions Audio/1F-Onset Veggie Sounds/1F-r",
			"Narration/1F-Skill Instructions Audio/1F-Onset Veggie Sounds/1F-s",
			"Narration/1F-Skill Instructions Audio/1F-Onset Veggie Sounds/1F-w",
			"",
			"Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/1F-at",
			"Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/1F-ag",
			"Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/1F-all",
			"Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/1F-ug",
			"Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/1F-up",
			"Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/1F-og",
			"Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/1F-an",
			"",
			"Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/1F-et",
			"Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/1F-eg",
			"Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/1F-ud",
			"Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/1F-ap",
			"",
			"Narration/1F-Reinforcement Audio/1F-bag-without-ag-is-b",
			"Narration/1F-Reinforcement Audio/1F-bed-without-ed-is-b",
			"Narration/1F-Reinforcement Audio/1F-cat-without-at-is-c",
			"Narration/1F-Reinforcement Audio/1F-fan-without-an-is-f",
			"Narration/1F-Reinforcement Audio/1F-hat-without-at-is-h",
			"Narration/1F-Reinforcement Audio/1F-jet-without-et-is-j",
			"Narration/1F-Reinforcement Audio/1F-leg-without-eg-is-l",
			"Narration/1F-Reinforcement Audio/1F-man-without-an-is-m",
			"Narration/1F-Reinforcement Audio/1F-nap-without-ap-is-n",
			"Narration/1F-Reinforcement Audio/1F-pan-without-an-is-p",
			"Narration/1F-Reinforcement Audio/1F-ball-without-all-is-b",
			"Narration/1F-Reinforcement Audio/1F-bat-without-at-is-b",
			"Narration/1F-Reinforcement Audio/1F-dog-without-og-is-d",
			"Narration/1F-Reinforcement Audio/1F-cup-without-up-is-c",
			"Narration/1F-Reinforcement Audio/1F-fig-without-ig-is-f",
			"Narration/1F-Reinforcement Audio/1F-log-without-og-is-l",
			"Narration/1F-Reinforcement Audio/1F-mat-without-at-is-m",
			"Narration/1F-Reinforcement Audio/1F-pen-without-p-is-en",
			"Narration/1F-Reinforcement Audio/1F-pot-without-p-is-ot",
			"Narration/1F-Reinforcement Audio/1F-sad-without-s-is-ad",
			"Narration/1F-Reinforcement Audio/1F-bug-without-ug-is-b",
			"Narration/1F-Reinforcement Audio/1F-bun-without-un-is-b",
			"Narration/1F-Reinforcement Audio/1F-hen-without-en-is-h",
			"Narration/1F-Reinforcement Audio/1F-mud-without-ud-is-m",
			"Narration/1F-Reinforcement Audio/1F-mug-without-ug-is-m",
			"Narration/1F-Reinforcement Audio/1F-nut-without-n-is-ut",
			"Narration/1F-Reinforcement Audio/1F-pig-without-p-is-ig",
			"Narration/1F-Reinforcement Audio/1F-red-without-r-is-ed",
			"Narration/1F-Reinforcement Audio/1F-sun-without-s-is-un",
			"Narration/1F-Reinforcement Audio/1F-web-without-w-is eb",
			"User-Earns-Artifact"
		};
	}

	public enum missingType {
		onset,
		rime
	}

	public class DOnsetAndRimeOpp {
		public string targetWord;
		public string onset;
		public string rime;
		public missingType missing;

		public DOnsetAndRimeOpp (string _targetWord, string _onset, string _rime, missingType _missing) {
			targetWord = _targetWord;
			onset = _onset;
			rime = _rime;
			missing = _missing;
		}

	}
}