using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;

namespace SSGCore
{
	public class PatterningBead : DisplayObjectContainer
	{
		private string _type;
		public string Type
		{
			get{ return _type; }
		}
		private FilterMovieClip disp;
		public int placeIn;
		public int dataIndex;
		
		public PatterningBead (string type, SkillLevel level)
		{
			placeIn = -1;
			dataIndex = -1;
			_type = type;
			MovieClip temp_mc;
			switch(level)
			{
			case SkillLevel.Emerging:
				temp_mc = MovieClipFactory.CreatePatterningShapes();
				break;
			case SkillLevel.Tutorial:
			case SkillLevel.Developing:
				temp_mc = MovieClipFactory.CreatePatterningFruits();
				break;
			case SkillLevel.Developed:
			case SkillLevel.Completed:
				temp_mc = MovieClipFactory.CreatePatterningBalls();
				break;
			default:
				goto case SkillLevel.Emerging;
			}
			temp_mc.gotoAndStop(type);
			disp = new FilterMovieClip(temp_mc);
			addChild(disp);
		}
		
		public void glowOn()
		{
			while(disp.filters.Count > 1)
			{
				disp.filters[1].Destroy();
				disp.filters[1].Visible = false;
				disp.filters.RemoveAt(1);
			}
			if(disp.filters.Count == 0)
				disp.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
			disp.filters[0].Visible = true;
		}
		
		public void glowOff()
		{
			while(disp.filters.Count > 0)
			{
				disp.filters[0].Destroy();
				disp.filters[0].Visible = false;
				disp.filters.RemoveAt(0);
			}
		}
		public void Destroy()
		{
			disp.Destroy();
		}
	}
}

