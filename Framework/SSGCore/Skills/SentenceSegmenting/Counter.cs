using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;
using pumpkin.text;

/// <summary>
/// Counting object. Used for all game pieces in CountingFoundations games
/// </summary>
namespace SSGCore
{
	public class Counter : FilterMovieClip
	{
		public int id = -1;
		public string type;
		public int num = 0;
		
		public Counter(MovieClip _target, string labelToGoTo):base(_target)
		{
			type = labelToGoTo;
			
			//DebugConsole.Log("COUNTING OBJECT CREATED: " + labelToGoTo + ", VISIBLE? " + Target.visible +", target label: " + Target.currentLabel);
		}
		
		private void SetFilter()
		{
			if( filters.Count > 0 )
			{
				Destroy();
				filters = new List<MovieClipFilter>();
			}
			MovieClipFilter filter = FiltersFactory.GetYellowGlowFilter();
			if( filter == null ) DebugConsole.LogError("FILTER IS NULL FROM FACTORY");
			else AddFilter(filter);
		}
		
		public override void AddFilter(MovieClipFilter filter){
		
			filters.Add(filter);
			
			filter.SetTexture(target.getChildAt<Sprite>(0));
			addChildAt(filter.GetSprite(), 0);
			filter.ApplyEffect();
			destroyed = false;
		}
		
		public void ShowFilter()
		{
			if( filters.Count > 0 ) filters[0].Visible = true;
			else AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
		}
		
		public void HideFilter()
		{
			if( filters.Count > 0 ) filters[0].Visible = false;
		}
		
		public void UpdateImage()
		{
			Target.gotoAndStop(type + "_" + (num+1));
			Target.visible = true;
			SetFilter();
			HideFilter();
			//DebugConsole.Log("FRAME TO GO TO FOR COUNTING OBJECT: " + type+ ", label at: " + Target.currentLabel + ", visible: " + Target.visible);
		}
	}
}

