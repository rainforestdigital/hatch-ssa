using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using pumpkin.display;
using pumpkin;
using pumpkin.events;
using pumpkin.text;

namespace SSGCore
{
	public class PageFlip
	{
		public MovieClip target;
		
		public PageFlip (MovieClip _target)
		{
			target = _target;
			
			HideGlow();
		}
		
		public void ShowGlow(){ target.gotoAndStop("on"); }
		
		public void HideGlow(){ target.gotoAndStop("off"); }
	}
}