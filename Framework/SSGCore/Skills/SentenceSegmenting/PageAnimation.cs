using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using pumpkin.display;
using pumpkin;
using pumpkin.events;
using pumpkin.text;
using pumpkin.tweener;
using pumpkin.geom;

namespace SSGCore
{
	public class PageAnimation : MovieClip
	{
		public SentenceSegmenting skill;
		
		private MovieClip root;
		public MovieClip Root
		{
			get{ return root; } 
		}
		
		public PageAnimation( string linkage ) : base( linkage )
		{
			root = this;
			Init();
		}
		
		public PageAnimation( string swf, string symbolName ) : base( swf, symbolName )
		{
			root = this;
			Init();
		}
		
		public PageAnimation( MovieClip mc ) : base()
		{
			root = mc;
			Init();
		}
		
		public void Init()
		{
			if(root != null)
			{
				root.addFrameScript(41, HandleFadeOut);
				root.addFrameScript(root.totalFrames, HandleEndOfAnimation);
			}
			HandleEndOfAnimation(new CEvent(CEvent.COMPLETE));
			
		}
		
		private void HandleEndOfAnimation(CEvent e)
		{
			visible = false;
			if(root!= null) root.gotoAndStop(1);
		}
		
		public void Show()
		{
			Debug.Log("SHOWING PAGEFLIP");
			if(root != null) 
			{
				root.alpha = 1;
				root.gotoAndPlay(1);
			}
			visible = true;

		}
		
		private void HandleFadeOut(CEvent e)
		{
			Debug.Log ("FADING OUT PAGEFLIP");
			Tweener.addTween(root, Tweener.Hash("time", 0.35, "alpha", 0, "visible", false, "transition", Tweener.TransitionType.easeOutQuad));
			if(skill != null) skill.EndPageAnimation();
		}
		
		public void Unload()
		{
			
		}
	}
}
