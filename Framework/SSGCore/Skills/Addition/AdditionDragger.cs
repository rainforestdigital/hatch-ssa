using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;

namespace SSGCore
{
	public class AdditionDragger : FilterMovieClip
	{

		private const string EG = "2E-EG";
		private const string DG = "2E-DG";
		private const string DDCD = "2E-DDCD";

		public bool inHouse = true;
		public int variant;
		public float snapInX = 0;
		public float snapInY = 0;
		public float snapOutX = 0;
		public float snapOutY = 0;
		
		public AdditionDragger (string species, float randomNum, float scale) : base (MovieClipFactory.CreateAdditionDragger())
		{
			MovieClip clip;
			switch(species)
			{
				case EG:
					variant = Mathf.CeilToInt(randomNum * 6f);
					if(variant == 0) variant++;
					target.gotoAndStop(1);
					clip = target.getChildByName<MovieClip>("bean_mc");
					break;
				case DG:
					variant = Mathf.CeilToInt(randomNum * 6f);
					if(variant == 0) variant++;
					variant += 6 * Mathf.FloorToInt(Random.value * 6f);
					if(variant > 36) variant -= 6;
					target.gotoAndStop(2);
					clip = target.getChildByName<MovieClip>("vial_mc");
					break;
				case DDCD:
					variant = Mathf.CeilToInt(randomNum * 10f);
					if(variant == 0) variant++;
					target.gotoAndStop(3);
					clip = target.getChildByName<MovieClip>("orange_mc");
					break;
				default:
					Debug.Log("AdditionDragger || Invalid type. Defaulting to bear");
					goto case EG;
			}
			clip.gotoAndStop(variant);
			clip.scaleX = clip.scaleY = scale;
		}
		
		public void glowOn()
		{
			while(filters.Count > 0)
			{
				filters[0].Destroy();
				filters[0].Visible = false;
				RemoveFilter(filters[0]);
			}
			AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
			filters[0].Visible = true;
		}
		
		public void glowOff()
		{
			Destroy();
			while(filters.Count > 0)
			{
				filters[0].Visible = false;
				RemoveFilter(filters[0]);
			}
		}
		
		public TweenerObj snapIn()
		{
			inHouse = true;
			return Tweener.addTween(this, Tweener.Hash("time", 0.25f, "x", snapInX, "y", snapInY));
		}
		
		public TweenerObj snapOut()
		{
			inHouse = false;
			return Tweener.addTween(this, Tweener.Hash("time", 0.25f, "x", snapOutX, "y", snapOutY));
		}
	}
}

