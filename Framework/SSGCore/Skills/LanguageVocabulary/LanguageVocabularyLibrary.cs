using UnityEngine;
using System.Collections;

public class LanguageVocabularyLibrary : MonoBehaviour
{
	public static string AIRPLANE_FLIES_IN_THE_AIR = "airplane-flying-in-the-air";
	public static string ALLIGATOR_BY_WATER = "alligator-beside-water";
	public static string APPLES_IN_A_BOWL = "apples-in-a-bowl";
	public static string ALLIGATOR_SUNNING_ON_A_SHORE = "alligator-sunning-on-the-shore";
	public static string AIRPLANE_LANDS_AT_THE_AIRPORT = "airplane-landing-at-the-airport";
	
	public static string BABY_CRYING = "baby-crying";
	public static string BABY_SLEEPING = "baby-sleeping";
	public static string BALLOONS_IN_THE_SKY = "balloons-in-the-sky";
	public static string BIRD_IN_BIRDCAGE = "bird-in-a-birdcage";
	public static string BIRDS_FLYING_IN_THE_SKY = "birds-flying-in-the-sky";
	public static string BOAT_IN_THE_WATER = "boat-in-water";
	public static string BOY_RUNNING = "boy-running";
	public static string BOY_WALKING = "boy-walking";
	public static string BRUSHING_TEETH = "girl-brushing-teeth";
	public static string BUTTERFLY_AROUND_SUNFLOWER = "butterfly-near-a-sunflower";
	public static string BOOK_OPEN_ON_A_TABLE = "book-open-on-a-table";
	public static string BIRD_SITTING_IN_A_NEST = "bird-sitting-in-a-nest";
	public static string BUTTERFLY_SITTING_ON_A_LEAF = "butterfly-sitting-on-a-leaf";
	public static string BOY_DRAWING_ON_THE_SIDEWALK = "boy-drawing-on-the-sidewalk";
	
	public static string CAT_ON_A_HOUSETOP = "the-cat-on-rooftop";
	public static string CHEF_COOKING = "chef-cooking";
	public static string CHILD_READING_BOOK = "child-reading-a-book";
	public static string CHILDREN_EATING = "children-eating";
	public static string CHILDREN_WRITING = "children-writing";
	public static string CHEESE_SANDWICH_ON_A_PLATE = "cheese-sandwich-on-a-plate";
	public static string CHILDREN_TALKING = "children-talking";
	public static string CHILD_USING_A_COMPUTER = "child-using-a-computer";
	public static string CHILDREN_PLAYING_IN_THE_SNOW = "children-playing-in-the-snow";
	
	public static string DOCTOR_TALKING_TO_CHILDREN = "doctor-talking-children";
	public static string DRAWING_ON_AN_EASEL = "girl-drawing-on-easel";
	public static string DOCTOR_LOOKING_AT_AN_XRAY = "doctor-looking-at-an-xray";
	
	public static string FIREMAN_RIDES_FIRE_TRUCK = "fireman-riding-firetruck";
	public static string FIREMAN_RIDES_FIRE_TRUCK_WITH_DOG = "fireman-riding-on-firetruck-with-dog";
	public static string FISH_IN_A_FISHBOWL = "fish-in-fishbowl";
	public static string FLOWERS_IN_THE_VASE = "flowers-in-vase";
	public static string FOOTBALL_PLAYER_THROWING_FOOTBALL = "football-player-throwing-a-football";
	public static string FIREMAN_STANDS_IN_FRONT_OF_THE_FIRE_STATION = "fireman-standing-in-front-of-a-fire-station";
	public static string FOOTBALL_PLAYER_RUNS_WITH_FOOTBALL = "football-player-running-with-a-football";
	
	public static string GIRL_JUMPING = "girl-jumping";
	public static string GIRL_RIDING_BIKE = "girl-riding-bike";
	public static string GIRL_RIDING_BIKE_WITH_A_FRIEND = "girl-riding-bike-with-a-friend";
	public static string GIRL_RIDING_A_SCOOTER = "girl-riding-a-scooter";
	public static string GIRL_BRUSHING_HER_HAIR = "girl-brushing-her-hair";
	public static string GLASSES_ON_A_CHILD = "glasses-on-a-child";
	
	public static string HELICOPTER_IN_THE_SKY = "helicopter-in-the-sky";
	public static string HORSE_EATING_GRASS = "horses-eating-grass";
	public static string HORSE_GALLOPING = "horse-galloping";
	public static string HORSES_RUNNING_THROUGH_WATER = "horses-running-through-water";
	public static string HORSE_IN_A_STABLE = "horse-in-a-stable";
	
	public static string ICEBERG_FLOATING_IN_THE_OCEAN = "iceberg-floating-in-the-ocean";
	
	public static string LADY_USES_COMPUTER = "lady-using-computer";
	public static string LADY_WEARING_EARRING = "lady-wearing-earring";
	
	public static string MAN_EXERCISING = "man-exercising";
	public static string MAN_IN_WHEELCHAIR = "man-in-a-wheelchair";
	public static string MAN_PAINTS = "man-painting";
	public static string MAN_PAINTS_HOUSE = "man-painting-house";
	public static string MAN_WEARS_A_TUXEDO_WITH_A_BOWTIE = "man-wearing-tuxedo-and-bowtie";
	public static string MOM_SHOPPING = "mom-shopping";
	public static string MONKEYS_IN_THE_ZOO = "monkeys-in-zoo";
	public static string MAN_WEARS_SHORTS_AND_SHIRT = "man-wearing-shorts-and-a-shirt";
	public static string MAN_BUILDING_A_HOUSE = "man-building-a-house";
	public static string MAN_DRIVES_A_CAR = "man-driving-a-car";
	
	public static string PEANUT_BUTTER_SANDWICH_ON_A_PLATE_WITH_MILK = "peanut-butter-sandwich-on-a-plate-with-milk";
	public static string PEANUT_BUTTER_SANDWICH_ON_A_PLATE = "peanut-butter-sandwich";
	public static string PEOPLE_ON_THE_BEACH = "people-on-beach";
	public static string PLAYING_BASKETBALL = "boy-playing-basketball";
	public static string PEOPLE_TALKING_ON_CELL_PHONES = "people-talking-on-cell-phones";
	
	public static string RAINBOW_IN_THE_SKY = "rainbow-in-sky";
	public static string RAINBOW_ON_THE_COVER_OF_A_BOOK = "rainbow-on-the-cover-of-a-book";
	
	public static string SNOWMAN_IN_FRONT_OF_HOUSE = "snowman-in-front-house";
	public static string SNOWMAN_IN_FRONT_OF_HOUSE_WITH_CHILDREN = "snowman-in-front-of-house-with-children";
	public static string SUNGLASSES_ON_A_DOG = "sunglasses-on-dog";
	public static string SUNGLASSES_ON_A_MAN = "sunglasses-on-man";
	public static string SUNGLASSES_ON_A_WOMAN = "sunglasses-on-woman";
	
	public static string TRAINS_ON_THE_RAILROAD_TRACK = "train-on-track";
	
	public static string WASHING_FACE = "boy-washing-face";

}

