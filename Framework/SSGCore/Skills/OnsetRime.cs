using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.geom;

namespace SSGCore
{
	public class OnsetRime : BaseSkill
	{
		private const string LISTEN = "Listen_Here_You_Little";
		private const string LETS_TRY = "Let's_Try_Again";
		
		private DisplayObjectContainer container;
		private OnsetRimeTutorial tutorial;
		
		private List<FilterMovieClip> cards;
		private FilterMovieClip ear;
		private TimerObject glowTimer;
		
		private FilterMovieClip currentCard;
		private string currentWord;
		private int introduced = -1;
		private float[] offset = new float[]{0,0};
		private float baseScale = 1;
		
		private List<string> allWords = new List<string>(){ "BAG", "BALL", "BAT", "BED", "BOX", "BOY", "BUN", "CAT", "COW", "CUP", "DOG",
															"FAN", "FOX", "HAT", "HEN", "JET", "LEG", "LOG", "MAN", "MAT", "MUD", "MUG",
															"NAP", "NUT", "PAN", "PEN", "PIG", "POT", "RED", "SAD", "SUN", "WEB"};
		private List<string> wordsLeft;
		
		private Rectangle hornRect;
		private Rectangle cardRect;
		private bool rectsDefined = false;

		public OnsetRime (SkillLevel level, SessionInfo  currentSession) : base(level, currentSession)
		{
			BACKGROUND = "Background";
			ARTIFACT = "Artifact";
			
			PreCacheAssets();
		}
		
		private void PreCacheAssets()
		{
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle("OnsetRime", OnAssetsLoaded));
		}
		
		void OnAssetsLoaded(AssetBundle bundle)
		{
			if( bundle != null )
			{
				Init( null );
			}
		}
		
		public override void Dispose ()
		{
			base.Dispose();
			if(glowTimer != null)
				glowTimer.Unload();
			stage.removeEventListener(MouseEvent.MOUSE_UP, earUp);
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, cardMove);
			stage.removeEventListener(MouseEvent.MOUSE_UP, cardUp);
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			ear.Destroy();
			if(currentCard != null)
				currentCard.Destroy();
			if(cards != null)
			{
				foreach(FilterMovieClip fmc in cards)
					fmc.Destroy();
			}
			if(tutorial != null)
			{
				if( tutorial.backCard != null )
					tutorial.backCard.Destroy();
				if( tutorial.ear != null )
					tutorial.ear.Destroy();
			}
		}
		
		public override void Init (MovieClip parent)
		{
			base.Init (parent);
			
			container = new DisplayObjectContainer();
			container.width = MainUI.STAGE_WIDTH;
			container.height = MainUI.STAGE_HEIGHT;
			container.alpha = 0;
			container.visible = false;
			addChild(container);
			
			Resize(Screen.width, Screen.height);
			
			ear = new FilterMovieClip(MovieClipFactory.CreateOnsetRimeEar());
			//ear.scaleX = ear.scaleY = baseScale = 250f/ear.width;
			ear.x = 858f;
			ear.y = 369f;
			baseScale = 629f / ear.width;
			DebugConsole.LogWarning("baseScale: {0}", baseScale);
			ear.scaleX = ear.scaleY = baseScale;
			/*ear.x = (Screen.width * 0.5f) - (ear.width * 0.5f);//(container.width - 250f)/2;
			ear.y = 100f;*/
			container.addChild(ear);
			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			switch(_level)
			{
			case SkillLevel.Tutorial:
				totalOpportunities = 1;
				if(resumingSkill)
					nextOpportunity();
				else{
					tutorial = new OnsetRimeTutorial();
					tutorial.View.addEventListener( TutorialEvent.START, onTutorialStart );
					tutorial.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
					tutorial.Resize(Screen.width, Screen.height);
				}
				break;
			default:
				totalOpportunities = 10;
				if(resumingSkill)
				{
					parseSessionData( currentSession.sessionData );
				//	nextOpportunity();
				}
				else
				{
					wordsLeft = new List<string>(allWords);
				}
				break;
			}
		}
		
		public override void onTutorialStart( CEvent e )
		{
			base.onTutorialStart(e);
			tutorial.View.removeEventListener( TutorialEvent.START, onTutorialStart );
			addChild(tutorial.View);
		}
		
		private void onTutorialComplete( CEvent e )
		{
			tutorial.View.removeEventListener(TutorialEvent.COMPLETE, onTutorialComplete);
			removeChild(tutorial.View);
			
			container.alpha = 1;
			nextOpportunity();
		}
		
		public override void OnAudioWordComplete ( string word)
		{
			switch(word)
			{
			case THEME_INTRO:
				if( !currentSession.skipIntro )
					playSound("story-introduction", INTRO);
				else
					OnAudioWordComplete( INTRO );
				break;
			
			case INTRO:
				if(_level == SkillLevel.Tutorial)
				{
					tutorial.OnStart();
				}
				else
				{
					nextOpportunity();
				}
				break;
				
			case NOW_ITS:
				playSound( "Initial Instruction/drag-" + currentWord.ToLower(), PLEASE_FIND );
				startEarGlow(1f);
				break;
				
			case THIS_IS:
				introduce();
				break;
				
			case PLEASE_FIND:
				cardsOn();
				break;
				
			case LISTEN:
				if(currentCard == null)
					startTimer();
				glowOff();
				break;
				
			case CLICK_CORRECT:
				dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
				break;
				
			case CLICK_INCORRECT:
				dispatchEvent( new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
				break;
				
			case THEME_COMPLIMENT:
				playSound("Reinforcement/" + currentCard.Target.currentLabel.ToLower(), YOU_FOUND);
				break;
				
			case THEME_CRITICISM:
				playSound("Reinforcement/" + currentCard.Target.currentLabel.ToLower(), YOU_FOUND_INCORRECT);
				break;
				
			case YOU_FOUND:
				if(_level == SkillLevel.Tutorial)
				{
					playSound("Instruction/lets-keep-listening-with-the-creatures", NOW_LETS);
					break;
				}
				goto case NOW_LETS;
			
			case NOW_LETS:
				clearAnswer();
				fadeOutContainer();
				if(opportunityCorrect)
				{
					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
				}
				else
				{
					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
				}
				break;
				
			case YOU_FOUND_INCORRECT:
				clearAnswer();
				playSound( "Initial Instruction/drag-" + currentWord.ToLower(), PLEASE_FIND );
				startEarGlow(1f);
				break;
				
			case LETS_TRY:
				resetTutorial();
				break;
			}
		}
		
		public override void nextOpportunity()
		{
			opportunityComplete = false;
			timeoutCount = 0;
			
			if(currentOpportunity == totalOpportunities)
			{
				tallyAnswers();
				return;
			}
			
			if(_level == SkillLevel.Tutorial)
			{
				currentWord = "BALL";
			}
			else
			{
				opportunityAnswered = false;
				
				int r;
				if(resumingSkill)
				{
					r = 0;
					resumingSkill = false;
				}
				else
				{
					r = Mathf.FloorToInt(Random.Range(0, wordsLeft.Count));
					if(r == wordsLeft.Count)
						r--;
				}
				currentWord = wordsLeft[r];
				wordsLeft.RemoveAt(r);
			}
			//currentCard = null;
			if(cards != null)
			{
				cardsOff();
				glowOff();
				foreach(FilterMovieClip fmc in cards)
				{
					fmc.Destroy();
					container.removeChild(fmc);
				}
			}
			cards = new List<FilterMovieClip>();
			setCards();
			
			currentOpportunity++;
			
			if(_level == SkillLevel.Tutorial)
			{
				playSound( "Instruction/now-its-your-turn", NOW_ITS );
				
				AddNewOppData( audioNames.IndexOf( "Instruction/now-its-your-turn" ) );
			}
			else if(_level == SkillLevel.Emerging)
			{
				introduce();
				
				AddNewOppData( audioNames.IndexOf( "Initial Instruction/drag-" + currentWord.ToLower() ) );
			}
			else
			{
				playSound( "Initial Instruction/drag-" + currentWord.ToLower(), PLEASE_FIND );
				startEarGlow(1f);
				
				AddNewOppData( audioNames.IndexOf( "Initial Instruction/drag-" + currentWord.ToLower() ) );
			}
			fadeInContainer();
			dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_START, true, false) );
			
			AddNewObjectData( formatObjectData( "horn", ear.parent.localToGlobal(new Vector2( ear.x, ear.y )) ) );
			foreach( FilterMovieClip fmc in cards )
			{
				if( fmc.parent == null )
				{
					AddNewObjectData( formatObjectData( "BAT", container.localToGlobal(new Vector2( fmc.x, fmc.y )), false ) );
					continue;
				}
				AddNewObjectData( formatObjectData( fmc.Target.currentLabel, fmc.parent.localToGlobal(new Vector2( fmc.x, fmc.y )), fmc.Target.currentLabel == currentWord ) );
			}
		}
		
		private void fadeInContainer()
		{
			container.visible = true;
			Tweener.addTween(container, Tweener.Hash("time", 0.25f, "alpha", 1));
		}
		
		private void fadeOutContainer()
		{
			Tweener.addTween(container, Tweener.Hash("time", 0.25f, "alpha", 0)).OnComplete(hideAway);
		}
		private void hideAway(){ container.visible = false; }
		
		private void setCards()
		{
			int num = 0;
			switch(_level)
			{
			case SkillLevel.Tutorial:
				num = 2;
				break;
			case SkillLevel.Emerging:
			case SkillLevel.Developing:
				num = 3;
				break;
			case SkillLevel.Developed:
			case SkillLevel.Completed:
				num = 4;
				break;
			}
			
			FilterMovieClip fmc = new FilterMovieClip(MovieClipFactory.CreateOnsetRimeCard());
			fmc.Target.gotoAndStop(currentWord);
			fmc.scaleX = fmc.scaleY = baseScale;
			//fmc.ScaleCentered(baseScale);
			cards.Add(fmc);
			container.addChild(fmc);
			if( _level == SkillLevel.Emerging )
				fmc.alpha = 0f;
			
			List<string> words = new List<string>{currentWord};
			int r;
			
			int i;
			for(i = 1; i < num; i++)
			{
				do{
					r = Mathf.FloorToInt(Random.Range(0, allWords.Count));
				}while(r == allWords.Count || words.IndexOf(allWords[r]) > -1);
				words.Add(allWords[r]);
				fmc = new FilterMovieClip(MovieClipFactory.CreateOnsetRimeCard());
				fmc.Target.gotoAndStop(words[i]);
				fmc.scaleX = fmc.scaleY = baseScale;
				cards.Add(fmc);
				container.addChild(fmc);
				if( _level == SkillLevel.Emerging )
					fmc.alpha = 0f;
			}
			if(_level == SkillLevel.Tutorial)
			{
				fmc = cards[0];
				cards.Remove(fmc);
				cards.Add(fmc);
				fmc.Destroy();
				container.removeChild(cards[0]);
			}
			else
			{
				cards.Shuffle();
			}
			updateCards();
		}
		
		private void updateCards(){updateCards(-1);}
		private void updateCards(int i)
		{
			float pad = 20f;
			float cWide = (cards[0].Target.width * baseScale) + pad;
			float startX = (2048 - ((cWide * cards.Count) - pad))/2f;
			float vPos = 922f;
			
			if(i < 0)
			{
				foreach(FilterMovieClip fmc in cards)
				{
					fmc.x = startX;
					fmc.y = vPos;
					
					startX += cWide;
				}
			}
			else
			{
				if(i >= cards.Count)
				{
					DebugConsole.LogWarning("OnsetRime || updateCards || Input " + i + " is out of range.");
					updateCards();
					return;
				}
				float newX = startX + (cWide * i);
				Tweener.addTween(cards[i], Tweener.Hash("time", 0.25f, "x", newX, "y", vPos, "scaleX", baseScale, "scaleY", baseScale));
			}
		}
		
		private void introduce()
		{
			if(introduced > 0)
			{
				updateCards(introduced - 1);
				currentCard = null;
			}
			
			if(introduced < 0)
			{
				playSound( "Instruction/listen-carefully-to-the-words", THIS_IS );
				introduced = 0;
			}
			else if(introduced == cards.Count)
			{
				playSound( "Initial Instruction/drag-" + currentWord.ToLower(), PLEASE_FIND );
				startEarGlow(1f);
				introduced = -1;
			}
			else
			{
				currentCard = cards[introduced];
				container.removeChild(currentCard);
				container.addChild(currentCard);
				float newScale = baseScale * 1.25f;
				currentCard.x = (MainUI.STAGE_WIDTH - (currentCard.Target.width * newScale)) / 2;
				currentCard.y = (MainUI.STAGE_HEIGHT - (currentCard.Target.height * newScale)) / 2;
				
				Tweener.addTween(currentCard, Tweener.Hash("time", 0.25f, "alpha", 1f, "scaleX", newScale, "scaleY", newScale));
				
				playSound( "Reinforcement/" + currentCard.Target.currentLabel.ToLower(), THIS_IS );
				introduced++;
			}
		}
		
		private void cardsOn ()
		{
			if(!cards[0].hasEventListener(MouseEvent.MOUSE_DOWN))
			{
				foreach(FilterMovieClip fmc in cards)
				{
					fmc.addEventListener(MouseEvent.MOUSE_DOWN, cardDown);
				}
				ear.addEventListener(MouseEvent.MOUSE_DOWN, earDown);
			}
			if(currentCard == null)
				startTimer();
			if(_level != SkillLevel.Tutorial)
				MarkInstructionEnd();
		}
		
		private void cardsOff ()
		{
			if(cards == null)
				return;
			foreach(FilterMovieClip fmc in cards)
			{
				fmc.removeEventListener(MouseEvent.MOUSE_DOWN, cardDown);
			}
			ear.removeEventListener(MouseEvent.MOUSE_DOWN, earDown);
			stopTimer();
			MarkInstructionStart();
		}
		
		private void earDown ( CEvent e )
		{
			timeoutCount = 0;
			PlayClickSound();
			cardsOff();
			stage.addEventListener(MouseEvent.MOUSE_UP, earUp);
		}
		
		private void earUp ( CEvent e )
		{
			stage.removeEventListener(MouseEvent.MOUSE_UP, earUp);
			cardsOn();
			stopTimer();
			glowOff();
			glowOn(ear);
			playSound("Ear/" + currentWord.ToLower(), LISTEN);
			
			AddNewActionData( formatActionData( "tap", 0, Vector2.zero ) );
		}
		
		private bool isDrag;
		private Vector2 cardOrig;
		private void cardDown ( CEvent e )
		{
			timeoutCount = 0;
			currentCard = e.currentTarget as FilterMovieClip;
			currentCard.ScaleCentered(currentCard.scaleX * 1.25f);
			offset[0] = container.mouseX - currentCard.x;
			offset[1] = container.mouseY - currentCard.y;
			
			container.removeChild(currentCard);
			container.addChild(currentCard);
			
			stage.addEventListener(MouseEvent.MOUSE_MOVE, cardMove);
			stage.addEventListener(MouseEvent.MOUSE_UP, cardUp);
			cardsOff();
			PlayClickSound();
			
			isDrag = false;
			cardOrig = new Vector2( currentCard.x, currentCard.y );
		}
		
		private void cardMove ( CEvent e )
		{
			Vector2 newPoint = new Vector2( container.mouseX -offset[0], container.mouseY -offset[1] );
			
			if( Mathf.Abs(cardOrig.x - newPoint.x + cardOrig.y - newPoint.y) > 5 ){
				isDrag = true;
			}
			
			currentCard.x = newPoint.x;
			currentCard.y = newPoint.y;
			
			if(checkEarHit())
				glowOn(ear);
			else
				glowOff(ear);
		}
		
		private void cardUp ( CEvent e )
		{
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, cardMove);
			stage.removeEventListener(MouseEvent.MOUSE_UP, cardUp);
			
			glowOff();
			glowOn(currentCard);
			
			if(checkEarHit())
			{
				Vector2 point = currentCard.parent.localToGlobal( new Vector2( currentCard.x, currentCard.y ) );
				if(currentCard.Target.currentLabel == currentWord)
				{
					if(!opportunityAnswered)
					{
						opportunityAnswered = true;
						opportunityCorrect = true;
					}
					opportunityComplete = true;
					
					PlayCorrectSound();
					
					AddNewActionData( formatActionData( "move", cards.IndexOf( currentCard ) + 1, point, true ) );
				}
				else
				{
					if(!opportunityAnswered)
					{
						opportunityAnswered = true;
						opportunityCorrect = false;
					}
					
					PlayIncorrectSound();
					
					AddNewActionData( formatActionData( "move", cards.IndexOf( currentCard ) + 1, point, false ) );
				}
			}
			else
			{
				updateCards(cards.IndexOf(currentCard));
				playSound("Reinforcement/" + currentCard.Target.currentLabel, LISTEN);
				currentCard = null;
				cardsOn();
				stopTimer();
					
				if( isDrag )
					AddNewActionData( formatActionData( "snap", cards.IndexOf( currentCard ) + 1, Vector2.zero ) );
				else
					AddNewActionData( formatActionData( "tap", cards.IndexOf( currentCard ) + 1, Vector2.zero ) );
			}
		}

		private bool checkEarHit()
		{
			if (!rectsDefined) {
				cardRect = currentCard.getBounds(container);
				hornRect = ear.getBounds(container);
				hornRect.width = hornRect.width * 0.44f; // we only want to check hits against the horn part of image
				rectsDefined = true;
			}

			cardRect.x = currentCard.x;
			cardRect.y = currentCard.y;

			float[] cCent = new float[]{cardRect.x + (cardRect.width / 2), cardRect.y + (cardRect.height / 2)};
			float[] eCent = new float[]{hornRect.x + (hornRect.width / 2), hornRect.y + (hornRect.height / 2)};
			
			if(Mathf.Abs(cCent[0] - eCent[0]) < (cardRect.width + hornRect.width)/2.35f && Mathf.Abs(cCent[1] - eCent[1]) < (cardRect.height + hornRect.height)/2.35f)
				return true;
			
			return false;
		}
		
		private void glowOn(FilterMovieClip fmc)
		{
			while(fmc.filters.Count > 1)
			{
				fmc.filters[1].Destroy();
				fmc.filters[1].Visible = false;
				fmc.filters.RemoveAt(1);
			}
			if(fmc.filters.Count == 0)
				fmc.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
			fmc.filters[0].Visible = true;
		}
		
		private void glowOff()
		{
			foreach(FilterMovieClip fmc in cards)
			{
				if(fmc != currentCard)
					glowOff(fmc);
			}
			if(currentCard == null || !checkEarHit())
				glowOff(ear);
		}
		private void glowOff(FilterMovieClip fmc)
		{
			while(fmc.filters.Count > 1)
			{
				fmc.filters[1].Destroy();
				fmc.filters[1].Visible = false;
				fmc.filters.RemoveAt(1);
			}
			if(fmc.filters.Count == 1)
				fmc.filters[0].Visible = false;
		}
		
		private void startEarGlow(float timerTime)
		{
			if(glowTimer != null) glowTimer.StopTimer();
			glowTimer = TimerUtils.SetTimeout(timerTime, eTimer1);
			if(timeoutCount == 3)
			{
				foreach(FilterMovieClip fmc in cards)
				{
					if(fmc.Target.currentLabel == currentWord)
						glowOn(fmc);
				}
			}	
		}
		
		private void eTimer1()
		{
			glowOn(ear);
			if(glowTimer != null) glowTimer.StopTimer();
			glowTimer = TimerUtils.SetTimeout(2.25f, eTimer2);
		}
		
		private void eTimer2()
		{
			glowOff();
			if(glowTimer != null) glowTimer.StopTimer();
		}
		
		private void clearAnswer ()
		{
			glowOff(ear);
			glowOff(currentCard);
			currentCard.Destroy();
			//cards.Remove(currentCard);
			Tweener.addTween(currentCard, Tweener.Hash("time", 0.25f, "alpha", 0)).OnComplete(nukeAnswer);
		}
		
		private void nukeAnswer ()
		{
			container.removeChild(currentCard);
			currentCard.Destroy();
			currentCard = null;
		}
		
		private string playSound ( string clipName ){ return playSound(clipName, ""); }
		private string playSound ( string clipName, string word )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();;
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.ONSET_RIME, clipName );
			
			if(word != "")
				bundle.AddClip(clip, word, clip.length);
			else
				bundle.AddClip(clip);
			
			SoundEngine.Instance.PlayBundle(bundle);
			
			return clipName;
		}
		
		public override void onTimerComplete()
		{
			base.onTimerComplete();
			
			timeoutCount++;
			string clipName = "";
			if(_level == SkillLevel.Tutorial)
			{
				if(timeoutCount == 1)
				{
					clipName = playSound( "Instruction/now-its-your-turn", NOW_ITS );
				}
				else
				{
					if(opportunityAnswered)
					{
						opportunityComplete = true;
						opportunityCorrect = false;
						currentCard = cards[cards.Count - 1];
						cardsOff();
						OnAudioWordComplete(NOW_LETS);
					}
					else
					{
						opportunityAnswered = true;
						opportunityCorrect = true;
						cardsOff();
						clipName = playSound( "Instruction/lets-try-again", LETS_TRY );
					}
				}
			}
			else
			{
				switch(timeoutCount)
				{
				case 1:
					clipName = playSound("Instruction/touch-the-pictures-to-hear-their-names", PLEASE_FIND);
					startEarGlow(3.5f);
					break;
				case 2:
					clipName = playSound("Instruction/touch-the-green-button", PLEASE_FIND);
					break;
				case 3:
					opportunityAnswered = true;
					opportunityCorrect = false;
					
					clipName = playSound("Initial Instruction/drag-" + currentWord.ToLower(), PLEASE_FIND);
					startEarGlow(0f);
					break;
				case 4:
					timeoutEnd = true;
					opportunityComplete = true;
					cardsOff();
					for(int i = cards.Count - 1; i >= 0; i--)
					{
						FilterMovieClip fmc = cards[i];
						if(fmc.Target.currentLabel == currentWord)
						{
							glowOn(fmc);
							Tweener.addTween(fmc, Tweener.Hash("time", 0.25, "x", (container.width - fmc.width)/2, "y", 200f, "scaleX", (fmc.scaleX * 1.25f), "scaleY", (fmc.scaleY * 1.25f)));
							currentCard = fmc;
						}
						else
						{
							container.removeChild(fmc);
							fmc.Destroy();
							cards.Remove(fmc);
						}
					}
					clipName = playSound("Reinforcement/" + currentWord.ToLower(), YOU_FOUND);
					break;
				}
			}
			
			if( !string.IsNullOrEmpty( clipName ) )
			{
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf( clipName ) );
			}
		}
		
		protected override void resetTutorial()
		{
			tutorial.backCard.Destroy();
			tutorial.ear.Destroy();
			tutorial = null;
			tutorial = new OnsetRimeTutorial();
			tutorial.Resize(Screen.width, Screen.height );
			
			addChild(tutorial.View);
			tutorial.View.addEventListener( TutorialEvent.START, onTutorialStart );
			tutorial.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			_currentOpportunity--;
			
			container.visible = false;
			container.alpha = 0;
			
			tutorial.OnStart();
		}
		
		public override void Resize( int wide, int high )
		{
			this.width = wide;
			this.height = high;
			container.scaleX = container.scaleY = wide/2048f;
			
			container.y = (high - (1536f * container.scaleY))/2;
			container.x = 0;
		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = currentWord;
			
			foreach(string s in wordsLeft)
			{
				data = data + "-" + s;
			}
			return data;
		}
		
		private void parseSessionData (string data)
		{
			wordsLeft = new List<string>();
			
			string[] sData = data.Split('-');
			
			foreach( string s in sData )
			{
				wordsLeft.Add(s);
			}
		}
		
		private static List<string> audioNames = new List<string>
		{	"",
			"story-introduction",
			"Instruction/using-their-ear-horns",
			"Instruction/Cami-which-picture",
			"Instruction/the-bat-picture-goes-in-the-horn",
			"Instruction/now-its-your-turn",
			"Instruction/lets-keep-listening-with-the-creatures",
			"Instruction/try-again",
			"Instruction/listen-carefully-to-the-words",
			"Instruction/touch-the-pictures-to-hear-their-names",
			"Instruction/touch-the-green-button",
			"Initial Instruction/drag-bag",
			"Initial Instruction/drag-ball",
			"Initial Instruction/drag-bat",
			"Initial Instruction/drag-bed",
			"Initial Instruction/drag-box",
			"Initial Instruction/drag-boy",
			"Initial Instruction/drag-bun",
			"Initial Instruction/drag-cat",
			"Initial Instruction/drag-cow",
			"Initial Instruction/drag-cup",
			"Initial Instruction/drag-dog",
			"Initial Instruction/drag-fan",
			"Initial Instruction/drag-fox",
			"Initial Instruction/drag-hat",
			"Initial Instruction/drag-hen",
			"Initial Instruction/drag-jet",
			"Initial Instruction/drag-leg",
			"Initial Instruction/drag-log",
			"Initial Instruction/drag-man",
			"Initial Instruction/drag-mat",
			"Initial Instruction/drag-mud",
			"Initial Instruction/drag-mug",
			"Initial Instruction/drag-nap",
			"Initial Instruction/drag-nut",
			"Initial Instruction/drag-pan",
			"Initial Instruction/drag-pen",
			"Initial Instruction/drag-pig",
			"Initial Instruction/drag-pot",
			"Initial Instruction/drag-red",
			"Initial Instruction/drag-sad",
			"Initial Instruction/drag-sun",
			"Initial Instruction/drag-web",
			"Ear/bag",
			"Ear/ball",
			"Ear/bat",
			"Ear/bed",
			"Ear/box",
			"Ear/boy",
			"Ear/bun",
			"Ear/cat",
			"Ear/cow",
			"Ear/cup",
			"Ear/dog",
			"Ear/fan",
			"Ear/fox",
			"Ear/hat",
			"Ear/hen",
			"Ear/jet",
			"Ear/leg",
			"Ear/log",
			"Ear/man",
			"Ear/mat",
			"Ear/mud",
			"Ear/mug",
			"Ear/nap",
			"Ear/nut",
			"Ear/pan",
			"Ear/pen",
			"Ear/pig",
			"Ear/pot",
			"Ear/red",
			"Ear/sad",
			"Ear/sun",
			"Ear/web",
			"Reinforcement/bag",
			"Reinforcement/ball",
			"Reinforcement/bat",
			"Reinforcement/bed",
			"Reinforcement/box",
			"Reinforcement/boy",
			"Reinforcement/bun",
			"Reinforcement/cat",
			"Reinforcement/cow",
			"Reinforcement/cup",
			"Reinforcement/dog",
			"Reinforcement/fan",
			"Reinforcement/fox",
			"Reinforcement/hat",
			"Reinforcement/hen",
			"Reinforcement/jet",
			"Reinforcement/leg",
			"Reinforcement/log",
			"Reinforcement/man",
			"Reinforcement/mat",
			"Reinforcement/mud",
			"Reinforcement/mug",
			"Reinforcement/nap",
			"Reinforcement/nut",
			"Reinforcement/pan",
			"Reinforcement/pen",
			"Reinforcement/pig",
			"Reinforcement/pot",
			"Reinforcement/red",
			"Reinforcement/sad",
			"Reinforcement/sun",
			"Reinforcement/web",
			"User-Earns-Artifact"
		};
	}
}