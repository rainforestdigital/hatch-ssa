using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;

namespace SSGCore
{

	public class LanguageVocabulary : BaseSkill
	{
		
		public const float PICTURE_SPACING = 10;
	
		private List<List<string>> egWords = new List<List<string>>(){	new List<string>(){LanguageVocabularyLibrary.BOY_RUNNING},
													new List<string>(){LanguageVocabularyLibrary.BABY_SLEEPING},
													new List<string>(){LanguageVocabularyLibrary.CHILD_READING_BOOK},
													new List<string>(){LanguageVocabularyLibrary.CHILDREN_EATING},
													new List<string>(){LanguageVocabularyLibrary.BABY_CRYING},
													new List<string>(){LanguageVocabularyLibrary.BRUSHING_TEETH},
													new List<string>(){LanguageVocabularyLibrary.FLOWERS_IN_THE_VASE},
													new List<string>(){LanguageVocabularyLibrary.HELICOPTER_IN_THE_SKY},
													new List<string>(){LanguageVocabularyLibrary.BIRDS_FLYING_IN_THE_SKY},
													new List<string>(){LanguageVocabularyLibrary.APPLES_IN_A_BOWL}};
		
		private List<List<string>> dgWords = new List<List<string>>(){	new List<string>(){LanguageVocabularyLibrary.RAINBOW_IN_THE_SKY, LanguageVocabularyLibrary.AIRPLANE_FLIES_IN_THE_AIR, LanguageVocabularyLibrary.RAINBOW_ON_THE_COVER_OF_A_BOOK}, 
													new List<string>(){LanguageVocabularyLibrary.GIRL_RIDING_BIKE_WITH_A_FRIEND, LanguageVocabularyLibrary.GIRL_RIDING_BIKE, LanguageVocabularyLibrary.GIRL_RIDING_A_SCOOTER},
													new List<string>(){LanguageVocabularyLibrary.BIRD_IN_BIRDCAGE, LanguageVocabularyLibrary.BIRDS_FLYING_IN_THE_SKY, LanguageVocabularyLibrary.BIRD_SITTING_IN_A_NEST},
													new List<string>(){LanguageVocabularyLibrary.PEANUT_BUTTER_SANDWICH_ON_A_PLATE_WITH_MILK, LanguageVocabularyLibrary.PEANUT_BUTTER_SANDWICH_ON_A_PLATE, LanguageVocabularyLibrary.CHEESE_SANDWICH_ON_A_PLATE},
													new List<string>(){LanguageVocabularyLibrary.BUTTERFLY_AROUND_SUNFLOWER, LanguageVocabularyLibrary.FLOWERS_IN_THE_VASE, LanguageVocabularyLibrary.BUTTERFLY_SITTING_ON_A_LEAF},
													new List<string>(){LanguageVocabularyLibrary.CHILDREN_WRITING, LanguageVocabularyLibrary.CHILD_READING_BOOK, LanguageVocabularyLibrary.CHILDREN_TALKING}, 
													new List<string>(){LanguageVocabularyLibrary.DOCTOR_TALKING_TO_CHILDREN, LanguageVocabularyLibrary.MAN_EXERCISING, LanguageVocabularyLibrary.DOCTOR_LOOKING_AT_AN_XRAY},
													new List<string>(){LanguageVocabularyLibrary.MAN_WEARS_A_TUXEDO_WITH_A_BOWTIE, LanguageVocabularyLibrary.LADY_WEARING_EARRING, LanguageVocabularyLibrary.MAN_WEARS_SHORTS_AND_SHIRT},
													new List<string>(){LanguageVocabularyLibrary.FIREMAN_RIDES_FIRE_TRUCK_WITH_DOG, LanguageVocabularyLibrary.FIREMAN_RIDES_FIRE_TRUCK, LanguageVocabularyLibrary.FIREMAN_STANDS_IN_FRONT_OF_THE_FIRE_STATION},
													new List<string>(){LanguageVocabularyLibrary.HORSE_GALLOPING, LanguageVocabularyLibrary.HORSE_EATING_GRASS, LanguageVocabularyLibrary.BOY_RUNNING}};
		
		private List<List<string>> ddWords = new List<List<string>>(){	new List<string>(){LanguageVocabularyLibrary.FOOTBALL_PLAYER_THROWING_FOOTBALL, LanguageVocabularyLibrary.BOY_WALKING, LanguageVocabularyLibrary.PLAYING_BASKETBALL, LanguageVocabularyLibrary.CHEF_COOKING, LanguageVocabularyLibrary.FOOTBALL_PLAYER_RUNS_WITH_FOOTBALL}, 
													new List<string>(){LanguageVocabularyLibrary.BRUSHING_TEETH, LanguageVocabularyLibrary.WASHING_FACE, LanguageVocabularyLibrary.GIRL_JUMPING, LanguageVocabularyLibrary.CHILDREN_EATING, LanguageVocabularyLibrary.GIRL_BRUSHING_HER_HAIR},
													new List<string>(){LanguageVocabularyLibrary.MAN_PAINTS_HOUSE, LanguageVocabularyLibrary.MAN_PAINTS, LanguageVocabularyLibrary.MAN_EXERCISING, LanguageVocabularyLibrary.MAN_IN_WHEELCHAIR, LanguageVocabularyLibrary.MAN_BUILDING_A_HOUSE},
													new List<string>(){LanguageVocabularyLibrary.ALLIGATOR_BY_WATER, LanguageVocabularyLibrary.MONKEYS_IN_THE_ZOO, LanguageVocabularyLibrary.BIRDS_FLYING_IN_THE_SKY, LanguageVocabularyLibrary.BUTTERFLY_AROUND_SUNFLOWER, LanguageVocabularyLibrary.ALLIGATOR_SUNNING_ON_A_SHORE},
													new List<string>(){LanguageVocabularyLibrary.HORSE_GALLOPING, LanguageVocabularyLibrary.HORSE_EATING_GRASS, LanguageVocabularyLibrary.MONKEYS_IN_THE_ZOO, LanguageVocabularyLibrary.BOY_WALKING, LanguageVocabularyLibrary.HORSE_IN_A_STABLE},
													new List<string>(){LanguageVocabularyLibrary.SNOWMAN_IN_FRONT_OF_HOUSE_WITH_CHILDREN, LanguageVocabularyLibrary.SNOWMAN_IN_FRONT_OF_HOUSE, LanguageVocabularyLibrary.PEOPLE_ON_THE_BEACH, LanguageVocabularyLibrary.RAINBOW_IN_THE_SKY, LanguageVocabularyLibrary.CHILDREN_PLAYING_IN_THE_SNOW}, 
													new List<string>(){LanguageVocabularyLibrary.MAN_EXERCISING, LanguageVocabularyLibrary.GIRL_JUMPING, LanguageVocabularyLibrary.MOM_SHOPPING, LanguageVocabularyLibrary.LADY_USES_COMPUTER, LanguageVocabularyLibrary.MAN_DRIVES_A_CAR},
													new List<string>(){LanguageVocabularyLibrary.DRAWING_ON_AN_EASEL, LanguageVocabularyLibrary.CHILDREN_WRITING, LanguageVocabularyLibrary.GIRL_JUMPING, LanguageVocabularyLibrary.BOY_RUNNING, LanguageVocabularyLibrary.BOY_DRAWING_ON_THE_SIDEWALK},
													new List<string>(){LanguageVocabularyLibrary.AIRPLANE_FLIES_IN_THE_AIR, LanguageVocabularyLibrary.BOAT_IN_THE_WATER, LanguageVocabularyLibrary.BIRDS_FLYING_IN_THE_SKY, LanguageVocabularyLibrary.BALLOONS_IN_THE_SKY, LanguageVocabularyLibrary.AIRPLANE_LANDS_AT_THE_AIRPORT},
													new List<string>(){LanguageVocabularyLibrary.SUNGLASSES_ON_A_MAN, LanguageVocabularyLibrary.SUNGLASSES_ON_A_WOMAN, LanguageVocabularyLibrary.SUNGLASSES_ON_A_DOG, LanguageVocabularyLibrary.MAN_WEARS_A_TUXEDO_WITH_A_BOWTIE, LanguageVocabularyLibrary.GLASSES_ON_A_CHILD}};
		
		private List<string> randomWord = new List<string>(){	LanguageVocabularyLibrary.BOY_RUNNING,
													LanguageVocabularyLibrary.CHEF_COOKING,
													LanguageVocabularyLibrary.BABY_SLEEPING,
													LanguageVocabularyLibrary.GIRL_JUMPING,
													LanguageVocabularyLibrary.CHILD_READING_BOOK,
													LanguageVocabularyLibrary.PLAYING_BASKETBALL,
													LanguageVocabularyLibrary.CHILDREN_EATING,
													LanguageVocabularyLibrary.MAN_EXERCISING,
													LanguageVocabularyLibrary.BABY_CRYING,
													LanguageVocabularyLibrary.BRUSHING_TEETH,
													LanguageVocabularyLibrary.WASHING_FACE,
													LanguageVocabularyLibrary.FLOWERS_IN_THE_VASE,
													LanguageVocabularyLibrary.BALLOONS_IN_THE_SKY,
													LanguageVocabularyLibrary.HELICOPTER_IN_THE_SKY,
													LanguageVocabularyLibrary.TRAINS_ON_THE_RAILROAD_TRACK,
													LanguageVocabularyLibrary.BIRDS_FLYING_IN_THE_SKY,
													LanguageVocabularyLibrary.BIRD_IN_BIRDCAGE,
													LanguageVocabularyLibrary.APPLES_IN_A_BOWL,
													LanguageVocabularyLibrary.PEANUT_BUTTER_SANDWICH_ON_A_PLATE,
													LanguageVocabularyLibrary.BOOK_OPEN_ON_A_TABLE,
													LanguageVocabularyLibrary.CHILD_USING_A_COMPUTER,
													LanguageVocabularyLibrary.PEOPLE_TALKING_ON_CELL_PHONES,
													LanguageVocabularyLibrary.HORSES_RUNNING_THROUGH_WATER,
													LanguageVocabularyLibrary.ICEBERG_FLOATING_IN_THE_OCEAN};
		
		private List<string> currentWords;
		
		private DisplayObjectContainer cardContainer_mc;
		
		private FilterMovieClip _incorrectAnswerCard;
		
		private List<FilterMovieClip> _cards;
		private new LanguageVocabularyTutorial tutorial_mc;

		private string currentClickedSound;
		FilterMovieClip cardClicked; //tracks the last clicked card
		
		private int numCards
		{
			get{ return _cards.Count; }
			set
			{
				int i;
				
				foreach(FilterMovieClip fmc in _cards)
				{
					fmc.Destroy();
					Tweener.removeTweens(fmc);
					cardContainer_mc.removeChild(fmc);
				}
				
				_cards.Clear();
												
				for(i = 0; i < currentWords.Count; i++)
				{
					MovieClip card_mc = new MovieClip( "LanguageVocabulary.swf", "Card" );
					FilterMovieClip card_filter = new FilterMovieClip(card_mc);
					card_filter.name = currentWords[i];
					card_filter.Target.name = currentWords[i];
					card_filter.Target.gotoAndStop(card_filter.Target.getFrameLabel( currentWords[i] ) );
					Debug.Log ("Previously Set word Word #" + i + ": " + currentWords[i] + " :: Card getting set to frame #" + card_filter.Target.currentFrame + " Current Word: " + currentWords[i]);
					card_filter.Target.mouseEnabled = false;
					card_filter.AddFilter( FiltersFactory.GetYellowGlowFilter() );
					card_filter.filters[0].GetSprite().ScaleCentered(1.05f);
					card_filter.filters[0].Visible = false;
//					card_filter.addEventListener( MouseEvent.CLICK, onCardClick );
					_cards.Add( card_filter );
					cardContainer_mc.addChild( card_filter );
				}
				
				int randomCount =  value - currentWords.Count;
				List<string> randomSample = randomWord.Filter(word => !currentWords.Contains(word)).RandomSample(randomCount);
				if(currentWords[0] == LanguageVocabularyLibrary.HORSE_GALLOPING)
					randomSample.Remove(LanguageVocabularyLibrary.HORSES_RUNNING_THROUGH_WATER);
				for(i = 0; i < randomSample.Count; i++)
				{
					MovieClip tempCard_mc = new MovieClip( "LanguageVocabulary.swf", "Card" );
					
					tempCard_mc.name = randomSample[ i ];
					FilterMovieClip tempCard_filter = new FilterMovieClip(tempCard_mc);
					tempCard_filter.name = randomSample[i];
					tempCard_filter.Target.name = randomSample[i];
					tempCard_filter.Target.gotoAndStop( randomSample[i] );
					Debug.Log ("Random Word #" + i + ": " + randomSample[i]);
//					tempCard_filter.addEventListener( MouseEvent.CLICK, onCardClick );
					tempCard_filter.AddFilter( FiltersFactory.GetYellowGlowFilter() );
					tempCard_filter.filters[0].GetSprite().ScaleCentered(1.05f);	
					tempCard_filter.filters[0].Visible = false;
					_cards.Add( tempCard_filter );
					cardContainer_mc.addChild( tempCard_filter );
				}
				
				if(_level == SkillLevel.Tutorial)
				{
					_cards[1].visible = false;
				}
				else
				{
					_cards.Shuffle();
					for(i = 0; i < _cards.Count; i++)
					{
						DebugConsole.Log( "Card {0} label: {1}", i, _cards[i].Target.currentLabel );
					}
				}
				
				//cardContainer_mc.LayoutTiled(_cards, 0.5f);
				
				positionCards();
				
			}
		}
		
		private void positionCards()
		{
			
			int i;
			
			float largestWidth = 0;
			float largestHeight = 0;
			
			for(i = 0; i < _cards.Count; i++)
			{
				if(_cards[i].Target.width > largestWidth)
				{
					largestWidth = _cards[i].Target.width;
				}
				if(_cards[i].Target.height > largestHeight) 
				{
					largestHeight = _cards[i].Target.height;
				}
			}
			
//			float scale = (float)Screen.height/(float)Screen.width == .75f ? .5f : .75f;
			float scale; //the scale of the cards, set to a percent based off of the number of cards

			float scaleOffset = 1;//0.65f; //Used to make the cards fit into the background section
			float cardOffset = 200;
			if(PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA)
			{
				scale = 1f * scaleOffset;
			}
			else
			{
				cardOffset = 120;
				scale = 1 * scaleOffset;
			}
								
			//scale = Mathf.Max(0.5f, scale);
			//scale = Mathf.Min(1, scale);
			
			//var startX = (cardContainer_mc.width - largestWidth * scale * _cards.Count - padding * (_cards.Count - 1)) / 2;
			//var startY = (cardContainer_mc.height - largestHeight * scale - padding) / 2;
			//var offsetX = padding + (largestWidth * scale);
			//var offsetY = largestHeight * scale + padding;
			
			float interval;
			float cS;
			var startY = (((Screen.height - cardOffset) - (_cards[0].Target.height * scale)) / 2f) ;
			
			if(_level == SkillLevel.Tutorial)
			{
				_cards[0].x = 677.65f;
				_cards[0].y = startY;
				cS = _cards[0].scaleX;
				cS *= scale;
				_cards[0].ScaleCentered(cS);
			}
			else if( cardContainer_mc.width > (((_cards[0].Target.width * scale) + 10f )* _cards.Count) + 10f)
			{
				interval = (_cards[0].Target.width * scale) + 10f;
				float startX = (cardContainer_mc.width - ((((_cards[0].Target.width * scale) + 10f )* _cards.Count) - 10f)) / 2f;
				for(i = 0; i < _cards.Count; i++)
				{
					cS = _cards[i].scaleX;
					cS *= scale;
					_cards[i].ScaleCentered(cS);
					_cards[i].x = startX + (i * interval) + (interval / 2f) - ((_cards[i].Target.width * _cards[i].scaleX) / 2f);
					_cards[i].y = startY;
				}
			}
			else
			{
				interval = ((cardContainer_mc.width - 20f) / ( _cards.Count));
				for(i = 0; i < _cards.Count; i++)
				{
					cS = (interval - 10) / _cards[i].Target.width;
//					cS *= scale;
					_cards[i].ScaleCentered(cS);
					_cards[i].x = (i * interval) + (interval / 2f) - ((_cards[i].Target.width * _cards[i].scaleX) / 2f) + 10f;
					_cards[i].y = startY;
				}
			}
			
		}
		
		public LanguageVocabulary( SkillLevel level, SessionInfo  currentSession ) : base( level, currentSession )
		{

			BACKGROUND = "Background";
			ARTIFACT = "Artifact";

			MarkInstructionStart();
			
			_level = level;
			
			Init (null);
		}
		
		public override void Dispose ()
		{
			base.Dispose();
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			if(tutorial_mc != null)
			{
				SoundEngine.Instance.OnAudioWordEvent -= (tutorial_mc as LanguageVocabularyTutorial).OnAudioWordComplete;
				tutorial_mc.card_1.Destroy();
				tutorial_mc.card_2.Destroy();
			}
			if(_incorrectAnswerCard != null)
				_incorrectAnswerCard.Destroy();
			if(cardClicked != null)
				cardClicked.Destroy();
			if(_cards != null)
			{
				foreach(FilterMovieClip fmc in _cards)
				{
					fmc.Destroy();
					Tweener.removeTweens(fmc);
				}
			}
		}
	
		private void PreCacheAssets()
		{
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle("SentenceSegmenting", OnAssetsLoaded));
		}
		
		void OnAssetsLoaded(AssetBundle bundle)
		{
			if( bundle != null )
			{
				Init( null );
			}
		}
		
		public override void Init (MovieClip parent)
		{
			Debug.Log("INIT CALLED");
			
			base.Init (parent);
			
			tutorial_mc = new LanguageVocabularyTutorial();
			(tutorial_mc as LanguageVocabularyTutorial).skillRef = this;

			cardContainer_mc = new DisplayObjectContainer();
			cardContainer_mc.width = Screen.width;
			cardContainer_mc.height = Screen.height;
			addChild( cardContainer_mc );
			cardContainer_mc.visible = false;

			_cards = new List<FilterMovieClip>();
			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			

			
			switch(_level)
			{
				case SkillLevel.Tutorial:
					totalOpportunities = 1;
					opportunityComplete = false;
					if( !resumingSkill) {
//						tutorial_mc = new LanguageVocabularyTutorial();
						addChild( tutorial_mc.View );
						tutorial_mc.View.addEventListener( TutorialEvent.START, onTutorialStart );
						tutorial_mc.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
						tutorial_mc.Resize( Screen.width, Screen.height );
						tutorial_mc.View.visible = false;
					} else {
						nextOpportunity();
					}
				break;
				default:
				totalOpportunities = 10;
					opportunityComplete = false;
				
					if(resumingSkill )
					{
						parseSessionData(currentSession.sessionData);
					//	nextOpportunity();
					}
				
				break;
			}
			
			Resize( Screen.width, Screen.height );
		}

		
		private void RemoveIncorrectCardAfterTween()
		{
			_incorrectAnswerCard.Destroy();
			_incorrectAnswerCard.Target.mouseEnabled = false;
			_incorrectAnswerCard.removeAllEventListeners(MouseEvent.CLICK);
			_incorrectAnswerCard.Target.x = _incorrectAnswerCard.Target.y = -1000;
			_incorrectAnswerCard.x = _incorrectAnswerCard.y = -1000;
			_incorrectAnswerCard = null;
		}
		
		public override void OnAudioWordComplete( string word )
		{
			if(_incorrectAnswerCard != null && !(word == CLICK_INCORRECT || word == THEME_CRITICISM))
			{
				Tweener.addTween( _incorrectAnswerCard, Tweener.Hash("time", 0.35f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuad) ).OnComplete(RemoveIncorrectCardAfterTween);
			}
			
			if(opportunityComplete)
			{
			
				switch( word )
				{
										
					case YOU_FOUND:
					DebugConsole.Log("you found complete");
						Tweener.addTween( cardContainer_mc, Tweener.Hash("time",0.35f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuint) );
						
						if(opportunityCorrect)
						{
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
						}
						else
						{
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
						}
					break;
					
					case THEME_COMPLIMENT:
						if(_level == SkillLevel.Tutorial)
						{
//							playInstructionAudio("3A-Skill Instruction Audio/3A-now-lets-find-henrys-cards", NOW_LETS);
						}
						else
						{
							playFoundCurrentAudio();
						}
					break;
					
					case INTRO:
					case PLEASE_FIND:
					case NOW_ITS:
					case NOW_LETS:
					case YOU_FOUND_INCORRECT:
					case THIS_IS :
						Tweener.addTween( cardContainer_mc, Tweener.Hash("time",0.35f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuint) );
						if(opportunityCorrect)
						{
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
						}
						else
						{
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
						}
					break;
					case THEME_INTRO:
						playIntro();
					break;
					case CLICK_CORRECT:
					case CLICK_INCORRECT:
					requestClickThemeFeedback();
					break;
				}
				
			}
			else
			{
				
				switch( word )
				{
					
					case THEME_CRITICISM:
						playFoundCurrentAudio();
					break;
					
					case YOU_FOUND:
					case YOU_FOUND_INCORRECT:
						playFindCurrentAudio();
					break;
										
					case INTRO:
						cardContainer_mc.visible = true;
						if(_level == SkillLevel.Tutorial)
						{
							tutorial_mc.View.visible = true;
							tutorial_mc.OnStart();
						}
						else
						{
							nextOpportunity();
						}
					break;
					
					case NOW_LETS:
						resetTutorial();
					break;
					
					case PLEASE_FIND:
					case NOW_ITS:					
					case THIS_IS :
						startTimer();
						enableCardButtons();
						if(_level != SkillLevel.Tutorial)
							MarkInstructionEnd();
						
					break;
					case THEME_INTRO:
						if( !currentSession.skipIntro )
						{
							playIntro();
						} else {
							OnAudioWordComplete( INTRO );
						}
					break;
					case "RESTART_TUTORIAL":
						tutorial_mc = new LanguageVocabularyTutorial();
						(tutorial_mc as LanguageVocabularyTutorial).skillRef = this;
					break;
				
					case CLICK_CORRECT:
				case CLICK_INCORRECT:
					requestClickThemeFeedback();
					break;
				}
				
			}
			
		}
		
		public override void nextOpportunity()
		{
			
			Debug.Log("NEXT OPPORTUNITy - Opportunity # = " + currentOpportunity + "OF " + totalOpportunities);
			if(currentOpportunity == totalOpportunities)
			{	
//				dispatchEvent( new SkillEvent(SkillEvent.END_THEME_REQUEST, true, false) ); //All opportunities are complete
				tallyAnswers();
				return;
			}
			
			switch(_level)
			{
				case SkillLevel.Tutorial:
					if(currentOpportunity > 0)
					{
						tallyAnswers();
					}
					else
					{
						currentOpportunity++;
						currentWords = new List<string>(){LanguageVocabularyLibrary.CAT_ON_A_HOUSETOP, LanguageVocabularyLibrary.FISH_IN_A_FISHBOWL};
						numCards = 2;
						
						fadeInCardContainer().OnComplete( OpportunityStart );
					
					}
				break;
				
				case SkillLevel.Emerging:
					setNextOpportunity(3, egWords);
				break;
				
				case SkillLevel.Developing:
					setNextOpportunity(4, dgWords);
				break;
				
				case SkillLevel.Developed:
					setNextOpportunity(5, ddWords);
				break;
				
				case SkillLevel.Completed:
					setNextOpportunity(5, ddWords);
				break;
				
			}
			
		}
		
		private void setNextOpportunity(int _cardCount, List<List<string>> _words)
		{
			int randAnswer;	
			
			if(currentOpportunity > 0)
			{
//				if(opportunityComplete)
//				{
//					if(opportunityCorrect)
//					{
//						NumCorrect++;
//					}
//					else
//					{
//						NumIncorrect++;
//					}
//				}
//				else
//				{
//					NumIncorrect++;
//				}
			}
			if(currentOpportunity < totalOpportunities)
			{
				opportunityComplete = false;
				opportunityCorrect = false;
				opportunityAnswered = false;
			
				currentOpportunity++;
				
				if(resumingSkill)
				{
					randAnswer = 0;
					resumingSkill = false;
				}
				else
				{
					do{
						randAnswer = Random.Range(0, _words.Count);
						if(randAnswer == _words.Count) randAnswer--;
					}while(_words[ randAnswer ].Equals(currentWords));
				}
				currentWords = _words[ randAnswer ];
				_words.RemoveAt( randAnswer );
			
				numCards = _cardCount;
			
				fadeInCardContainer().OnComplete( OpportunityStart );
			
			}
			else
			{
				Debug.Log("TALLYING ANSWERS");
				tallyAnswers();
				numCards = 0;
			}
		}
		
		public override void OpportunityStart()
		{
			string clipName = "";
			switch( _level )
			{
				case SkillLevel.Tutorial:
//					playInstructionAudio("3A-Skill Instruction Audio/3A-now-its-your-turn", NOW_ITS);
				break;
				default:
					clipName = playFindCurrentAudio();
				break;
			}
			
			timeoutCount = 0;
			
			dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_START, true, false) );
			
			AddNewOppData( audioNames.IndexOf( clipName ) );
			
			foreach( FilterMovieClip fmc in _cards )
			{
				Vector2 point = fmc.parent.localToGlobal( new Vector2( fmc.x, fmc.y ) );
				AddNewObjectData( formatObjectData( fmc.Target.currentLabel, point, fmc.Target.currentLabel == currentWords[0]  ) );
			}
		}
		
		public override void onTimerComplete()
		{
			base.onTimerComplete();
			
			stopTimer();

			timeoutCount++;
			
			string clipName = "";
			
			switch(timeoutCount)
			{
				case 1:
					if(_level == SkillLevel.Tutorial)
					{	
						playInstructionAudio("3A-Skill Instruction Audio/3A-3-TT-Now-your-turn", NOW_ITS);
						clipName = "3A-Skill Instruction Audio/3A-3-TT-Now-your-turn";
					}
					else
					{
						clipName = playFindCurrentAudio();
					}
				break;
				case 2:
					if(_level == SkillLevel.Tutorial)
					{	
						disableCardButtons();
						if(opportunityAnswered)
						{
							opportunityComplete = true;
							opportunityCorrect = false;
							
							OnAudioWordComplete(NOW_LETS);
							return;
						}
					
						opportunityAnswered = true;
						opportunityCorrect = true;
					
						playInstructionAudio("3A-Skill Instruction Audio/3A-6-try-again", NOW_LETS);
						clipName = "3A-Skill Instruction Audio/3A-6-try-again";
					}
					else
					{
						playInstructionAudio("3A-Skill Instruction Audio/3A-5-touch-green-button", NOW_ITS);
						clipName = "3A-Skill Instruction Audio/3A-5-touch-green-button";
					}
				break;
				case 3:
					if(opportunityAnswered == false)
					{
						opportunityAnswered = true;
						opportunityCorrect = false;
					}
					
					foreach(FilterMovieClip lb in _cards)
					{
						if(lb.Target.currentLabel == currentWords[0])
						{
							if(lb.filters.Count > 0)
								lb.filters[0].Visible = true;
							else
								lb.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
							break;
						}
					}
					
					clipName = playFindCurrentAudio();
				break;
				case 4:
					timeoutEnd = true;
					disableCardButtons();
					MarkInstructionStart();
					opportunityCorrect = false;
					opportunityComplete = true;
					
					foreach(FilterMovieClip lb in _cards)
					{
						lb.removeEventListener( MouseEvent.CLICK, onCardClick );
						if(lb.Target.currentLabel != currentWords[0])
						{
							Tweener.addTween(lb, Tweener.Hash("time",0.5f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuad) );
						}
						else
						{
							lb.Target.ScaleCentered( lb.Target.scaleX * 1.25f);
							lb.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
							lb.filters[0].Visible = true;
						}
					}
					
					clipName = playThisCurrentAudio();
				break;
			}
			
			if( !string.IsNullOrEmpty( clipName ) )
			{
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf( clipName ) );
			}
		}
		
		private void onCardClick( CEvent e )
		{
			if( (e.currentTarget as FilterMovieClip).name == "CLICKED") return;
			
//			DebugConsole.Log("card clicked: " + ((e.currentTarget as FilterMovieClip) == null) + " ::: " + (e.currentTarget as FilterMovieClip).name);
			
			cardClicked = e.currentTarget as FilterMovieClip;
			if( string.IsNullOrEmpty((e.currentTarget as FilterMovieClip).name) ) 
			{
				(e.currentTarget as FilterMovieClip).name = "CLICKED";
			}
			
			Debug.Log ("CARD CLICKED LABEL: " + cardClicked.Target.currentLabel);
			
			disableCardButtons();
			MarkInstructionStart();
					
			cardClicked.Target.ScaleCentered( cardClicked.Target.scaleX * 1.25f);
		
			SoundEngine.Instance.SendCancelToken();
			//SoundEngine.Instance.StopAll();	
			
			//Play Card Clicked
			PlayClickSound();
						
			cardContainer_mc.removeChild( cardClicked );
			cardContainer_mc.addChild( cardClicked );
			
			stopTimer();
			timeoutCount = 0;
			
			currentClickedSound = (e.currentTarget as FilterMovieClip).name;
			
			cardClicked.Destroy();
			
			cardClicked.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
			cardClicked.filters[0].Visible = true;
			
			//If Right Answer
			if(cardClicked.Target.currentLabel == currentWords[0])
			{
				SoundUtils.PlaySimpleEffect("ding");
				if(opportunityAnswered == false)
				{
					opportunityAnswered = true;
					opportunityCorrect = true;
				}
				opportunityComplete = true;
				
				disableCardButtons();
				MarkInstructionStart();
				
				PlayCorrectSound();
				
				AddNewActionData( formatActionData( "tap", _cards.IndexOf( cardClicked ), Vector2.zero, true ) );
			}
			//If Wrong Answer
			else
			{
				SoundUtils.PlaySimpleEffect("pop");
				disableCardButtons();
				MarkInstructionStart();
				
				if(opportunityAnswered == false)
				{
					opportunityAnswered = true;
					opportunityCorrect = false;
				}
				
				_incorrectAnswerCard = cardClicked;
				
				PlayIncorrectSound();
				
				AddNewActionData( formatActionData( "tap", _cards.IndexOf( cardClicked ), Vector2.zero, false ) );
			}
		}
		
		private void requestClickThemeFeedback() //The correct/incorrect sound has been played and now we need theme feedback
		{
			if(cardClicked.Target.currentLabel == currentWords[0]) dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
			else dispatchEvent( new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
		}
		
		private void playInstructionAudio( string type, string eventID )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LANGUAGE_VOCABULARY, type );
			if(clip != null)
			{
				bundle.AddClip(clip, eventID, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else
			{
				DebugConsole.LogError("Instruction Audio NULL");
				OnAudioWordComplete(eventID);
			}
		}
		
		private string playFindCurrentAudio()
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LANGUAGE_VOCABULARY,  "3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-" + currentWords[0] );
			if(clip != null)
			{
				bundle.AddClip(clip, PLEASE_FIND, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else
			{
				DebugConsole.LogError("PLEASE_FIND Audio NULL");
				OnAudioWordComplete(PLEASE_FIND);
			}
			
			return "3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-" + currentWords[0];
		}
		
		private void playFoundCurrentAudio()	
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			
		
			
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LANGUAGE_VOCABULARY,  "3A-Reinforcement Audio/3A-Found-" + currentClickedSound );
			if(clip != null)
			{
				bundle.AddClip(clip, YOU_FOUND, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else
			{
				DebugConsole.LogError("YOU_FOUND Audio NULL -- currentClickedSound: " + currentClickedSound);
				OnAudioWordComplete(YOU_FOUND);
			}
		}
		
		private string playThisCurrentAudio()
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			
		
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LANGUAGE_VOCABULARY,  "3A-Inactivity Reinforcement Audio/3A-This-" + currentWords[0] );
			if(clip != null)
			{
				bundle.AddClip(clip, THIS_IS, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else
			{
				DebugConsole.LogError("YOU_FOUND Audio NULL");
				OnAudioWordComplete(THIS_IS);
			}
			
			return "3A-Inactivity Reinforcement Audio/3A-This-" + currentWords[0];
		}
		
		private void playIntro()
		{
			DebugConsole.Log("playing intro");
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LANGUAGE_VOCABULARY, "3A-Story Introduction Audio/3A-1-StoryIntroduction" );
			if(clip != null && !currentSession.skipIntro)
			{
				bundle.AddClip(clip, INTRO, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else
			{
				DebugConsole.LogError("Intro Audio NULL");
				OnAudioWordComplete(INTRO);
			}
		}
		
		/// <summary>
		/// Disables the card buttons.
		/// </summary>
		private void disableCardButtons()
		{
			for(int i = 0; i < _cards.Count; i++)
			{
				_cards[i].Target.mouseEnabled = false;
				_cards[i].removeEventListener( MouseEvent.CLICK, onCardClick );
			}
		}
		
		/// <summary>
		/// Enables the card buttons.
		/// </summary>
		private void enableCardButtons()
		{
			for(int i = 0; i < _cards.Count; i++)
			{
				_cards[i].Target.mouseEnabled = true;
				_cards[i].addEventListener( MouseEvent.CLICK, onCardClick );
			}
		}
		
		
		private void onTutorialComplete( CEvent e )
		{
			tutorial_mc.View.removeEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.View.removeEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			
//			currentOpportunity++;
			
			removeChild( tutorial_mc.View );
			currentOpportunity++;
			opportunityAnswered = true;
			opportunityCorrect = true;
//			opportunityComplete = true;
			
			if(tutorial_mc.correct)
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
			else
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
			
			//nextOpportunity();
		}
		
		/// <summary>
		/// Fades in the card container.
		/// </summary>
		/// <returns>
		/// The TweenerObj used to fade the container.
		/// </returns>
		private TweenerObj fadeInCardContainer()
		{
			cardContainer_mc.visible = true;
			cardContainer_mc.alpha = 0;
			return Tweener.addTween(cardContainer_mc, Tweener.Hash("time", 0.35f, "alpha", 1, "transition", Tweener.TransitionType.easeInOutQuint) );
		}
		
		/// <summary>
		/// Resize the specified width and height.
		/// </summary>
		/// <param name='width'>
		/// Width.
		/// </param>
		/// <param name='height'>
		/// Height.
		/// </param>
		public override void Resize( int width, int height )
		{
			this.width = width;
			this.height = height;
			
			cardContainer_mc.width = width;
			cardContainer_mc.height = height;
			
			float targWidth = cardContainer_mc.width;
			if( PlatformUtils.GetPlatform() == Platforms.WIN ) targWidth = 1920f;
			if( PlatformUtils.GetPlatform() == Platforms.ANDROID ) targWidth = 1280f;
			
			cardContainer_mc.scaleX = cardContainer_mc.scaleY = width/targWidth;

			cardContainer_mc.y = (height - (cardContainer_mc.height*cardContainer_mc.scaleY))/2f;
			cardContainer_mc.x = (width - (cardContainer_mc.width*cardContainer_mc.scaleX))/2f;

//			cardContainer_mc.FitToParent();
		}

		protected override void resetTutorial()
		{
			
			cardContainer_mc.alpha = 0;
			currentOpportunity = 0;
			
			timeoutCount = 0;
			
			DebugConsole.LogWarning("resetting tutorial");
			
			SoundEngine.Instance.OnAudioWordEvent -= tutorial_mc.OnAudioWordComplete;
			tutorial_mc.card_1.Destroy();
			tutorial_mc.card_2.Destroy();
			tutorial_mc = null;
			tutorial_mc = new LanguageVocabularyTutorial();
			
			addChild( tutorial_mc.View );
			tutorial_mc.View.visible = true;
			tutorial_mc.skillRef = this;
			tutorial_mc.View.addEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			tutorial_mc.Resize( Screen.width, Screen.height );
			tutorial_mc.OnStart();
		}
		
		public void TutorialRequest()
		{
			
		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			List<List<string>> _words;
			switch(_level)
			{
			case SkillLevel.Emerging:
				_words = egWords;
				break;
			case SkillLevel.Developing:
				_words = dgWords;
				break;
			case SkillLevel.Developed:
			case SkillLevel.Completed:
				_words = ddWords;
				break;
			default:
				goto case SkillLevel.Emerging;
			}
			
			string data = "";
			foreach(string s in currentWords)
			{
				if(data != "")
					data = data + ":";
				data = data + s;
			}
			
			foreach(List<string> ls in _words)
			{
				data = data + ";";
				foreach(string s in ls)
				{
					if(data.LastIndexOf(';') != data.Length - 1)
						data = data + ":";
					data = data + s;
				}
			}
			
			return data;
		}
		
		private void parseSessionData (string data)
		{
			List<List<string>> _words = new List<List<string>>();
			
			string[] sData = data.Split(';');
			string[] bData;
			List<string> sWords;
			
			foreach( string s in sData )
			{
				sWords = new List<string>();
				bData = s.Split(':');
				foreach( string b in bData )
				{
					sWords.Add(b);
				}
				_words.Add(sWords);
			}
			
			switch(_level)
			{
			case SkillLevel.Emerging:
				egWords = _words;
				break;
			case SkillLevel.Developing:
				dgWords = _words;
				break;
			case SkillLevel.Developed:
			case SkillLevel.Completed:
				ddWords = _words;
				break;
			}
		}
		
		public static List<string> audioNames = new List<string>
		{	"",
			"3A-Story Introduction Audio/3A-StoryIntroduction",
			"3A-Skill Instruction Audio/3A-2-TT-Instruction",
			"3A-Skill Instruction Audio/3A-3-TT-Now-your-turn",
			"3A-Skill Instruction Audio/3A-4-TT-now-lets-show",
			"3A-Skill Instruction Audio/3A-5-touch-green-button",
			"3A-Skill Instruction Audio/3A-6-try-again",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-airplane-flying-in-the-air",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-alligator-beside-water",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-apples-in-a-bowl",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-baby-crying",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-baby-sleeping",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-balloons-in-the-sky",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-bird-in-a-birdcage",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-birds-flying-in-the-sky",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-boy-running",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-butterfly-near-a-sunflower",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-child-reading-a-book",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-children-eating",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-children-writing",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-doctor-talking-children",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-fireman-riding-on-firetruck-with-dog",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-flowers-in-vase",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-football-player-throwing-a-football",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-girl-brushing-teeth",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-girl-drawing-on-easel",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-girl-riding-bike-with-a-friend",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-helicopter-in-the-sky",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-horse-galloping",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-man-exercising",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-man-painting-house",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-man-wearing-tuxedo-and-bowtie",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-peanut-butter-sandwich-on-a-plate-with-milk",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-rainbow-in-sky",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-snowman-in-front-of-house-with-children",
			"3A-Skill Instruction Audio/3A-Initial Instructions/3A-show-sunglasses-on-man",
			"3A-Reinforcement Audio/3A-TT-that-is-fish-in-fishbowl",
			"3A-Reinforcement Audio/3A-TT-you-found-the-cat-on-rooftop",
			"3A-Reinforcement Audio/3A-Found-airplane-flying-in-the-air",
			"3A-Reinforcement Audio/3A-Found-airplane-landing-at-the-airport",
			"3A-Reinforcement Audio/3A-Found-alligator-beside-water",
			"3A-Reinforcement Audio/3A-Found-alligator-sunning-on-the-shore",
			"3A-Reinforcement Audio/3A-Found-apples-in-a-bowl",
			"3A-Reinforcement Audio/3A-Found-baby-crying",
			"3A-Reinforcement Audio/3A-Found-baby-sleeping",
			"3A-Reinforcement Audio/3A-Found-balloons-in-the-sky",
			"3A-Reinforcement Audio/3A-Found-bird-in-a-birdcage",
			"3A-Reinforcement Audio/3A-Found-birds-flying-in-the-sky",
			"3A-Reinforcement Audio/3A-Found-bird-sitting-in-a-nest",
			"3A-Reinforcement Audio/3A-Found-boat-in-water",
			"3A-Reinforcement Audio/3A-Found-book-open-on-a-table",
			"3A-Reinforcement Audio/3A-Found-boy-drawing-on-the-sidewalk",
			"3A-Reinforcement Audio/3A-Found-boy-playing-basketball",
			"3A-Reinforcement Audio/3A-Found-boy-running",
			"3A-Reinforcement Audio/3A-Found-boy-walking",
			"3A-Reinforcement Audio/3A-Found-boy-washing-face",
			"3A-Reinforcement Audio/3A-Found-butterfly-near-a-sunflower",
			"3A-Reinforcement Audio/3A-Found-butterfly-sitting-on-a-leaf",
			"3A-Reinforcement Audio/3A-Found-cheese-sandwich-on-a-plate",
			"3A-Reinforcement Audio/3A-Found-chef-cooking",
			"3A-Reinforcement Audio/3A-Found-child-reading-a-book",
			"3A-Reinforcement Audio/3A-Found-children-eating",
			"3A-Reinforcement Audio/3A-Found-children-playing-in-the-snow",
			"3A-Reinforcement Audio/3A-Found-children-talking",
			"3A-Reinforcement Audio/3A-Found-children-writing",
			"3A-Reinforcement Audio/3A-Found-child-using-a-computer",
			"3A-Reinforcement Audio/3A-Found-doctor-looking-at-an-xray",
			"3A-Reinforcement Audio/3A-Found-doctor-talking-children",
			"3A-Reinforcement Audio/3A-Found-fireman-riding-firetruck",
			"3A-Reinforcement Audio/3A-Found-fireman-riding-on-firetruck-with-dog",
			"3A-Reinforcement Audio/3A-Found-fireman-standing-in-front-of-a-fire-station",
			"3A-Reinforcement Audio/3A-Found-flowers-in-vase",
			"3A-Reinforcement Audio/3A-Found-football-player-running-with-a-football",
			"3A-Reinforcement Audio/3A-Found-football-player-throwing-a-football",
			"3A-Reinforcement Audio/3A-Found-girl-brushing-her-hair",
			"3A-Reinforcement Audio/3A-Found-girl-brushing-teeth",
			"3A-Reinforcement Audio/3A-Found-girl-drawing-on-easel",
			"3A-Reinforcement Audio/3A-Found-girl-jumping",
			"3A-Reinforcement Audio/3A-Found-girl-riding-a-scooter",
			"3A-Reinforcement Audio/3A-Found-girl-riding-bike",
			"3A-Reinforcement Audio/3A-Found-girl-riding-bike-with-a-friend",
			"3A-Reinforcement Audio/3A-Found-glasses-on-a-child",
			"3A-Reinforcement Audio/3A-Found-helicopter-in-the-sky",
			"3A-Reinforcement Audio/3A-Found-horse-galloping",
			"3A-Reinforcement Audio/3A-Found-horse-in-a-stable",
			"3A-Reinforcement Audio/3A-Found-horses-eating-grass",
			"3A-Reinforcement Audio/3A-Found-horses-running-through-water",
			"3A-Reinforcement Audio/3A-Found-iceberg-floating-in-the-ocean",
			"3A-Reinforcement Audio/3A-Found-lady-using-computer",
			"3A-Reinforcement Audio/3A-Found-lady-wearing-earring",
			"3A-Reinforcement Audio/3A-Found-man-building-a-house",
			"3A-Reinforcement Audio/3A-Found-man-driving-a-car",
			"3A-Reinforcement Audio/3A-Found-man-exercising",
			"3A-Reinforcement Audio/3A-Found-man-in-a-wheelchair",
			"3A-Reinforcement Audio/3A-Found-man-painting",
			"3A-Reinforcement Audio/3A-Found-man-painting-house",
			"3A-Reinforcement Audio/3A-Found-man-wearing-shorts-and-a-shirt",
			"3A-Reinforcement Audio/3A-Found-man-wearing-tuxedo-and-bowtie",
			"3A-Reinforcement Audio/3A-Found-mom-shopping",
			"3A-Reinforcement Audio/3A-Found-monkeys-in-zoo",
			"3A-Reinforcement Audio/3A-Found-peanut-butter-sandwich",
			"3A-Reinforcement Audio/3A-Found-peanut-butter-sandwich-on-a-plate-with-milk",
			"3A-Reinforcement Audio/3A-Found-people-on-beach",
			"3A-Reinforcement Audio/3A-Found-people-talking-on-cell-phones",
			"3A-Reinforcement Audio/3A-Found-rainbow-in-sky",
			"3A-Reinforcement Audio/3A-Found-rainbow-on-the-cover-of-a-book",
			"3A-Reinforcement Audio/3A-Found-snowman-in-front-house",
			"3A-Reinforcement Audio/3A-Found-snowman-in-front-of-house-with-children",
			"3A-Reinforcement Audio/3A-Found-sunglasses-on-dog",
			"3A-Reinforcement Audio/3A-Found-sunglasses-on-man",
			"3A-Reinforcement Audio/3A-Found-sunglasses-on-woman",
			"3A-Reinforcement Audio/3A-Found-train-on-track",
			"3A-Inactivity Reinforcement Audio/3A-This-airplane-flying-in-the-air",
			"3A-Inactivity Reinforcement Audio/3A-This-alligator-beside-water",
			"3A-Inactivity Reinforcement Audio/3A-This-apples-in-a-bowl",
			"3A-Inactivity Reinforcement Audio/3A-This-baby-crying",
			"3A-Inactivity Reinforcement Audio/3A-This-baby-sleeping",
			"3A-Inactivity Reinforcement Audio/3A-This-balloons-in-the-sky",
			"3A-Inactivity Reinforcement Audio/3A-This-bird-in-a-birdcage",
			"3A-Inactivity Reinforcement Audio/3A-This-birds-flying-in-the-sky",
			"3A-Inactivity Reinforcement Audio/3A-This-boy-running",
			"3A-Inactivity Reinforcement Audio/3A-This-butterfly-near-a-sunflower",
			"3A-Inactivity Reinforcement Audio/3A-This-child-reading-a-book",
			"3A-Inactivity Reinforcement Audio/3A-This-children-eating",
			"3A-Inactivity Reinforcement Audio/3A-This-children-writing",
			"3A-Inactivity Reinforcement Audio/3A-This-doctor-talking-children",
			"3A-Inactivity Reinforcement Audio/3A-This-fireman-riding-on-firetruck-with-dog",
			"3A-Inactivity Reinforcement Audio/3A-This-flowers-in-vase",
			"3A-Inactivity Reinforcement Audio/3A-This-football-player-throwing-a-football",
			"3A-Inactivity Reinforcement Audio/3A-This-girl-brushing-teeth",
			"3A-Inactivity Reinforcement Audio/3A-This-girl-drawing-on-easel",
			"3A-Inactivity Reinforcement Audio/3A-This-girl-riding-bike-with-a-friend",
			"3A-Inactivity Reinforcement Audio/3A-This-helicopter-in-the-sky",
			"3A-Inactivity Reinforcement Audio/3A-This-horse-galloping",
			"3A-Inactivity Reinforcement Audio/3A-This-man-exercising",
			"3A-Inactivity Reinforcement Audio/3A-This-man-painting-house",
			"3A-Inactivity Reinforcement Audio/3A-This-man-wearing-tuxedo-and-bowtie",
			"3A-Inactivity Reinforcement Audio/3A-This-peanut-butter-sandwich-on-a-plate-with-milk",
			"3A-Inactivity Reinforcement Audio/3A-This-rainbow-in-sky",
			"3A-Inactivity Reinforcement Audio/3A-This-snowman-in-front-of-house-with-children",
			"3A-Inactivity Reinforcement Audio/3A-This-sunglasses-on-man",
			"User-Earns-Artifact"
		};
	}
}