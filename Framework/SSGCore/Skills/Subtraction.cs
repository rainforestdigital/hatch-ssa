using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.geom;

namespace SSGCore
{
	public class Subtraction : BaseSkill
	{		
		private const string TOUCH_SWITCH = "Touch_Switch";
		private const string LETS_TRY = "Let's Try Again";
		
		private DisplayObjectContainer view;
		private int[,] egSets = new int[,]{{5, 1}, {4, 1}, {3, 1}, {2, 1}, {3, 2}, {5, 1}, {4, 1}, {3, 1}, {2, 1}, {3, 2}};
		private int[,] dgSets = new int[,]{{5, 2}, {5, 3}, {5, 4}, {4, 2}, {4, 3}, {5, 2}, {5, 3}, {5, 4}, {4, 2}, {4, 3}};
		private int[,] ddSets = new int[,]{{5, 1}, {4, 1}, {3, 1}, {2, 1}, {3, 2}, {5, 2}, {5, 3}, {5, 4}, {4, 2}, {4, 3}};
		private int[,] levelSets;
		private int[] levelUsed;
		private int levelIn;
		private int levelOut;
		private int currentOut;
		private int glowDelayCount;
		private int failCount;
		private float[] glowDelays = new float[]{.8f, 1.6f, 2f, 1.6f, 2f, 1.8f};
		private float[] dGlowDelays = new float[]{.245f, 1.2f};
		private float bGlowMod = 0.3f;
		private float levelRandom;
		private float leeway = 20f;
		private float baseScale;
		private string levelShort;
		private string seaAnimalName;
		private bool glow2 = false;
		private bool glow3 = false;
		private bool doorIsOn = false;
		private TimerObject glowTimer;
		private SubtractionDragger currentObject;
		private SubtractionDragger[] draggers;
		private SubtractionHouse house;
		private SubtractionTutorial tutorial;
		//private Rect egBounds = new Rect(20, 140, 545, 360);
		//private Rect dgBounds = new Rect(20, 140, 545, 360);
		//private Rect ddBounds = new Rect(415, 325, 500, 285);
		private Rect levelBounds = new Rect(170, 270, 545, 360);
//		private Rect levelBounds_IOS = new Rect (-20, 180, 472, 310);
//		private Rect levelBounds_IOSRETINA = new Rect (60, 470, 708, 400);
//		private Rect levelBounds_ANDROID = new Rect (20, 180, 472, 310);
		private Vector2 dragOffset;

		private const string basePath = "Narration/";
		private const string inactivityReinforcementPath = "2F-Inactivity Reinforcement Audio/";
		private const string reinforcementPath = "2F-Reinforcement Audio/";
		private const string skillInstructionPath = "2F-Skill Instruction Audio/";
		private const string skillInitialInstructionPath = "2F-Initial Instruction/2F-";
		private const string storyIntroductionPath = "2F-Story Introduction Audio/";

		Platforms selectedPlatform;
		float retinaScaler = 2f;
		private Vector2 starfishCenter;
		private float starfishDistance = 0f;

		public Subtraction (SkillLevel level, SessionInfo  currentSession) : base(level, currentSession)
		{

			BACKGROUND = "Background";
			ARTIFACT = "Artifact";

			PreCacheAssets();
		}
		
		private void PreCacheAssets()
		{
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle("Subtraction", OnAssetsLoaded));
		}
		
		void OnAssetsLoaded(AssetBundle bundle)
		{
			if( bundle != null )
			{
				Init( null );
			}
		}
		
		/// <summary>
		/// Releases all resource used by the <see cref="SSGCore.Subtraction"/> object.
		/// </summary>
		/// <remarks>
		/// Call <see cref="Dispose"/> when you are finished using the <see cref="SSGCore.Subtraction"/>. The
		/// <see cref="Dispose"/> method leaves the <see cref="SSGCore.Subtraction"/> in an unusable state. After
		/// calling <see cref="Dispose"/>, you must release all references to the <see cref="SSGCore.Subtraction"/> so
		/// the garbage collector can reclaim the memory that the <see cref="SSGCore.Subtraction"/> was occupying.
		/// </remarks>
		public override void Dispose ()
		{
			base.Dispose();
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			clearDraggerTweens();
			draggersOff();
			doorOff();
			if(currentObject != null)
			{
				stage.removeEventListener(MouseEvent.MOUSE_MOVE, onObjectMove);
				stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
				currentObject.Destroy();
			}
			if(tutorial != null)
			{
				tutorial.View.removeEventListener( TutorialEvent.START, onTutorialStart );
				tutorial.View.removeEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
				if( tutorial.bears != null )
				{
					foreach(SubtractionDragger d in tutorial.bears)
					{
						d.Destroy();
					}
				}
				if( tutorial.house != null )
					tutorial.house.door.Destroy();
			}
			if(glowTimer != null)
				glowTimer.Unload();
			house.door.Destroy();
			if(draggers != null)
			{
				foreach(SubtractionDragger dd in draggers)
				{
					dd.Destroy();
				}
			}
		}
		
		/// <summary>
		/// Init the specified parent.
		/// </summary>
		/// <param name='parent'>
		/// Parent movieclip. Not currently used.
		/// </param>
		public override void Init (MovieClip parent)
		{
			base.Init (parent);
			
			view = new DisplayObjectContainer();
			view.width = MainUI.STAGE_WIDTH;
			view.height = MainUI.STAGE_HEIGHT;
			addChild(view);
			
			totalOpportunities = 10;
			do{
				levelRandom = Random.value;
			}while(levelRandom == 0f);
			levelUsed = new int[10];
			
			//baseScale = view.scaleY;
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			house = new SubtractionHouse("EG");

//			float baseNumber = 990f;
//			float xPos = 10f;
//			float yPos = 150f;

			/*
			selectedPlatform = PlatformUtils.GetPlatform ();
			//selectedPlatform = Platforms.ANDROID;

			if (selectedPlatform == Platforms.IOS || selectedPlatform == Platforms.IOSRETINA) {
				baseNumber = 550f;
				xPos = 30f;
				yPos = 148f;
				levelBounds = levelBounds_IOS;

				if (selectedPlatform == Platforms.IOSRETINA) {
					baseNumber *= retinaScaler;
					xPos *= retinaScaler;
					yPos *= retinaScaler;
					levelBounds = levelBounds_IOSRETINA;
				}

			} else if (selectedPlatform == Platforms.ANDROID) {
				baseNumber = 650f;
				xPos = 45f;
				yPos = 108f;
				levelBounds = levelBounds_ANDROID;
			} else if (selectedPlatform == Platforms.WIN) {
				xPos = 93f;
				yPos = 125f;
			}
			*/
			
			selectedPlatform = PlatformUtils.GetPlatform ();
			baseScale = (selectedPlatform == Platforms.ANDROID || selectedPlatform == Platforms.IOS) ? Screen.width / 1025f : Screen.width / 2048f;


			house = new SubtractionHouse (levelShort);
			house.scaleX = house.scaleY = baseScale;
			house.x = Screen.width * 0.5f;//xPos;//Mathf.CeilToInt(0.0385f * thisScreenWidth);//74;
			house.y = Screen.height * 0.5f;//yPos; // Win only?

			starfishCenter = (selectedPlatform == Platforms.ANDROID || selectedPlatform == Platforms.IOS) ? 
								new Vector2 (Screen.width * 0.5f - 300f * baseScale, Screen.height * 0.5f - 60f * baseScale) :
								new Vector2 (Screen.width * 0.5f - 600f * baseScale, Screen.height * 0.5f - 100f * baseScale);
			
			//setBaseScale (Screen.width, Screen.height);
			//baseScale = Screen.width/Screen.height; //850f/house.width;
			house.door.Destroy();
			
			switch(_level)
			{
			case SkillLevel.Tutorial:
				totalOpportunities = 1;
				levelShort = "EG";
				levelRandom = 9f/12f;
				levelSets = new int[,] {{5, 1}};
				if(resumingSkill)
					nextOpportunity();
				else
				{
					tutorial = new SubtractionTutorial(baseScale);
					tutorial.setHouse(house);
					addChild( tutorial.View );
					tutorial.View.addEventListener( TutorialEvent.START, onTutorialStart );
					tutorial.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
					tutorial.Resize( Screen.width, Screen.height );
					//float sMod = 150f;
					//float dist = (levelBounds.height - (leeway * 5f)) / 2.5f;

					/*
					float tutorialScale = ((dist / sMod) * baseScale);

					if (selectedPlatform == Platforms.IOS ) {
						tutorialScale += 0.2f;

					} else if (selectedPlatform == Platforms.IOSRETINA) {
						//tutorialScale = 0.05f;

					} else if (selectedPlatform == Platforms.ANDROID) {
						tutorialScale += 0.1f;
					}
					*/

					//tutorial.setScale(tutorialScale);

				}
				break;
			case SkillLevel.Emerging:
				levelSets = egSets;
				levelShort = "EG";
				break;
			case SkillLevel.Developing:
				levelSets = dgSets;
				levelShort = "DG";
				break;
			case SkillLevel.Developed:
			case SkillLevel.Completed:
				levelSets = ddSets;
				levelShort = "DDCD";
				break;
			}

			seaAnimalName = getSeaAnimal(levelShort);


			
			if(resumingSkill && _level != SkillLevel.Tutorial)
			{
				parseSessionData( currentSession.sessionData );
			//	nextOpportunity();
			}
			
			opportunityAnswered = false;
			opportunityComplete = false;
			opportunityCorrect = true;
		}

		void setBaseScale( int width, int height )
		{
			Rect clipping;
			if( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA )
				clipping = new Rect( 1f, 1f, 1980f, 1080f );
			else
				clipping = new Rect( 0.5f, 0.5f, 1023f, 767f );
			
			baseScale = (width + 4f) / (clipping.width);
			baseScale += .22f;
		}
		
		public override void OnAudioWordComplete ( string word)
		{
			switch(word)
			{
			case THEME_INTRO:
				house.alpha = 0;
				if(_level != SkillLevel.Tutorial)
					view.addChild(house);
				Tweener.addTween(house, Tweener.Hash("time", 0.25f, "alpha", 1f));
				
				if( !currentSession.skipIntro )
					//OnAudioWordComplete( INTRO );
					playSound( basePath + storyIntroductionPath + "2F-story-introduction", INTRO);
				else
					OnAudioWordComplete( INTRO );
				
				break;
			case INTRO:
				if(_level == SkillLevel.Tutorial)
				{
					tutorial.OnStart();
				}
				else
				{
					nextOpportunity();
				}
				break;
			case NOW_ITS:
				playSound( basePath + skillInstructionPath + skillInitialInstructionPath + levelShort + "/2F-" + levelIn + "-minus-" + levelOut + "-" + seaAnimalName, PLEASE_FIND);
				beginDraggerGlow(1);
				break;
			case TOUCH_SWITCH:
				startTimer();
				goto case PLEASE_FIND;
			case PLEASE_FIND:
				if(_level != SkillLevel.Tutorial || !doorIsOn)
					draggersOn();
				break;
			case THEME_COMPLIMENT:
				playSound( basePath + reinforcementPath + "2F-" + levelShort + "/2F-" + levelIn + "-minus-" + levelOut + "-is-" + (levelIn-levelOut).ToString() + "-" + seaAnimalName , YOU_FOUND);
				beginDraggerGlow(3);
				break;
			case THEME_CRITICISM:
				currentOut = 0;//levelIn;//Could be mixed up
				playSound( basePath + skillInstructionPath + skillInitialInstructionPath + levelShort + "/2F-" + levelIn + "-minus-" + levelOut + "-" + seaAnimalName, PLEASE_FIND);
				beginDraggerGlow(2);
				break;
			case YOU_FOUND:
			case YOU_FOUND_INCORRECT:
				if(_level == SkillLevel.Tutorial)
				{
					playSound( basePath + skillInstructionPath + "2F-lets-keep-unsticking-the-animals", NOW_LETS);
				}
				else
				{
					goto case NOW_LETS;
				}
				break;
			case NOW_LETS:
				hideAll();
				if(opportunityCorrect)
				{
					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
					//NumCorrect++;
				}
				else
				{
					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
					//NumIncorrect++;
				}
				break;
			case CLICK_CORRECT:
				dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
				break;
			case CLICK_INCORRECT:
				if(opportunityComplete)
					autoClear();
				else
					dispatchEvent( new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
				break;
			case LETS_TRY:
				resetTutorial();
				break;
			}
		}
		
		public override void onTutorialStart( CEvent e )
		{
			tutorial.View.removeEventListener( TutorialEvent.START, onTutorialStart );
		}
		
		private void onTutorialComplete( CEvent e )
		{
			view.addChild(house);
			
			removeChild(tutorial.View);
			
			tutorial.View.removeEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			
			nextOpportunity();
		}
		
		protected override void resetTutorial()
		{
			foreach(SubtractionDragger d in tutorial.bears)
			{
				d.Destroy();
			}
			tutorial.house.door.Destroy();
			tutorial = null;
			tutorial = new SubtractionTutorial(baseScale);
			tutorial.Resize(Screen.width, Screen.height );
			
			addChild(tutorial.View);
			tutorial.View.addEventListener( TutorialEvent.START, onTutorialStart );
			tutorial.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			currentOpportunity--;
			hideAll();
			tutorial.setHouse (house);
			tutorial.OnStart();
		}
		
		public override void nextOpportunity()
		{
			if(draggers != null)
			{
				foreach(SubtractionDragger dd in draggers)
				{
					dd.Destroy();
				}
			}
			if(currentOpportunity == totalOpportunities)
			{
				tallyAnswers();
				return;
			}
			
			view.addChild(house);
			doorOff();
			
			int r;
			if(resumingSkill)
			{
				r = levelUsed[currentOpportunity];
				resumingSkill = false;
			}
			else
			{
				do{
					r = Mathf.FloorToInt(Random.value * totalOpportunities);
				}while(r == totalOpportunities || checkLevelUsed(r));
				levelUsed[currentOpportunity] = r;
			}
			
			levelIn = levelSets[r, 0];
			levelOut = levelSets[r, 1];
			currentOut = 0;
			timeoutCount = 0;
			failCount = 0;
			
			if(_level == SkillLevel.Tutorial)
			{
				levelRandom = 9f/12f;
			}
			else
			{
				do{
					levelRandom = Random.value;
				}while(levelRandom == 0f);
			}
			
			draggers = new SubtractionDragger[levelIn];
			SubtractionDragger d;
			int i;
			for(i = 0; i < levelIn; i++)
			{
				d  = new SubtractionDragger(levelShort, levelRandom);
				d.alpha = 0;
				view.addChild(d);
				draggers[i] = d;
			}
			setDraggerPos();
			house.openDoor();
			for(i = 0; i < levelIn; i++)
			{
				d = draggers[i];
				Tweener.addTween(d, Tweener.Hash("time", 0.5f, "alpha", 1));
			}
			dispatchEvent(new SkillEvent(SkillEvent.OPPORTUNITY_START, true, false));
			
			currentOpportunity++;
			
			if(_level == SkillLevel.Tutorial)
			{
				playSound( basePath + skillInstructionPath + "2F-now-its-your-turn", NOW_ITS);
				
				AddNewOppData( audioNames.IndexOf(basePath + skillInstructionPath + "2F-now-its-your-turn") );
			}
			else
			{
				OnAudioWordComplete(NOW_ITS);
				opportunityAnswered = false;
				opportunityComplete = false;
				opportunityCorrect = true;
				
				string clipName = basePath + skillInstructionPath + skillInitialInstructionPath + levelShort + "/2F-" + levelIn + "-minus-" + levelOut + "-" + seaAnimalName;
				AddNewOppData( audioNames.IndexOf( clipName ) );
			}
			
			Vector2 point = house.door.parent.localToGlobal(new Vector2(house.door.x, house.door.y));
			AddNewObjectData( formatObjectData( "switch", point ) );
			
			foreach( SubtractionDragger dr in draggers )
			{
				point = dr.parent.localToGlobal( new Vector2( dr.x, dr.y ) );
				AddNewObjectData( formatObjectData( seaAnimalName + dr.variant, point ) );
			}
		}
		
		private bool checkLevelUsed (int val)
		{
			if(currentOpportunity == 0)
				return false;
			for(int i = 0; i < currentOpportunity - 1; i++)
			{
				if(val == levelUsed[i])
					return true;
			}
			int last = levelUsed[currentOpportunity - 1];
			if(val == last || (levelSets[val, 0] == levelSets[last, 0] && levelSets[val, 1] == levelSets[last, 1]))
				return true;
			
			if(currentOpportunity == totalOpportunities - 3)
			{
				int[,] sublist = new int[2,2];
				int k = 0;
				bool used;
				for(int j = 0; j < 10; j++)
				{
					used = false;
					if(j == val)
						continue;
					for(int l = 0; l < currentOpportunity; l++)
					{
						if(j == levelUsed[l])
						{
							used = true;
							break;
						}
					}
					if(!used)
					{
						sublist[k,0] = levelSets[j,0];
						sublist[k,1] = levelSets[j,1];
						k++;
					}
				}
				if(sublist[0,0] == sublist[1,0] && sublist[0,1] == sublist[1,1])
					return true;
			}
			return false;
		}
		
		private void setDraggerPos()
		{
			currentOut = 0;
			
			float xScale = levelBounds.width / levelBounds.height;
			float dist;
			float sMod;

			sMod = 150f;
			dist = (selectedPlatform == Platforms.ANDROID || selectedPlatform == Platforms.IOS) ? 60f : 120f;// (levelBounds.height - (leeway * 5f)) / 2.5f;

			float angStep = (2f * Mathf.PI) / levelIn;
			//Vector2 center = new Vector2 (0f, 0f);//levelBounds.x + (levelBounds.width / 2f),
										//levelBounds.y + (levelBounds.height / 2f));
			SubtractionDragger d;
			float ang;

			float scaleDist = (levelBounds.height - (leeway * 5f)) / 2.5f;
			
			float draggerScale = (scaleDist / sMod) * baseScale;
			if( ( _level != SkillLevel.Developed || _level != SkillLevel.Completed ) && selectedPlatform == Platforms.IOS )
				draggerScale *= .8f;

			if (selectedPlatform == Platforms.IOS) { 
				draggerScale += 0.2f;
			} else if ( selectedPlatform == Platforms.IOSRETINA) {
				//draggerScale += 0.1f;
			} else if (selectedPlatform == Platforms.ANDROID) {
				draggerScale += 0.1f;
			}

			float[] distanceValues = new float[levelIn];

			for(int i = 0; i < levelIn; i++)
			{
				ang = (Mathf.PI / 2f) + angStep;
				ang += angStep * i;
				d = draggers[i];
				d.x = ((Mathf.Cos(ang) * dist * baseScale) * xScale) + starfishCenter.x;
				d.y = (Mathf.Sin(ang) * dist * baseScale) + starfishCenter.y;
				d.scaleX = d.scaleY = draggerScale;
				d.inHouse = true;
				d.snapInX = d.x;
				d.snapInY = d.y;
				distanceValues[i] = Vector2.Distance(starfishCenter, new Vector2(d.x, d.y));
			}

			starfishDistance = Mathf.Max (distanceValues);
			starfishDistance = starfishDistance * 1.5f;
			
			float goalRight = levelBounds.x + levelBounds.width;
			dist = (MainUI.STAGE_WIDTH - goalRight)/6;
//			float vertMod = PlatformUtils.GetClosestRatio() == ScreenRatio.FOURxTHREE ? 3f : 2f;
		//	center = new Vector2( goalRight + ((MainUI.STAGE_WIDTH - goalRight)/ 2f),
		//								 MainUI.STAGE_HEIGHT/ vertMod);

			float xIncrement = 1000f * (Screen.width/1920f);// Win?
			float yIncrement = -50f* (Screen.width/1920f);

			//selectedPlatform = PlatformUtils.GetPlatform ();

			if (selectedPlatform == Platforms.IOS || selectedPlatform == Platforms.IOSRETINA) {
				xIncrement = 550f;

				if (selectedPlatform == Platforms.IOSRETINA) {
					xIncrement *= retinaScaler;

				}
			} else if (selectedPlatform == Platforms.ANDROID) {
                xIncrement = 700f * (Screen.width/1280f);
                yIncrement = -70f * (Screen.width/1280f);
			}


			for(int i = 0; i < levelIn; i++)
			{
				ang = (Mathf.PI / 2f) + angStep;
				ang += angStep * i;
				d = draggers[i];
				//d.snapOutX = (Mathf.Cos(ang) * dist) + center.x;
				//d.snapOutY = (Mathf.Sin(ang) * dist) + center.y;

				d.snapOutX = d.x + xIncrement;
				d.snapOutY = d.y + yIncrement;

			}
		}
		
		private void draggersOn()
		{
			if(!draggers[0].hasEventListener(MouseEvent.MOUSE_DOWN))
			{
				for(int i = 0; i < levelIn; i++)
				{
					draggers[i].addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
				}
			}
			startTimer();
			if(_level != SkillLevel.Tutorial)
				MarkInstructionEnd();
		}
		
		private void draggersOff()
		{
			if(draggers == null)
				return;
			if(draggers[0].hasEventListener(MouseEvent.MOUSE_DOWN))
			{
				for(int i = 0; i < levelIn; i++)
				{
					draggers[i].removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
				}
			}
			stopTimer();
			MarkInstructionStart();
		}
		
		private void doorOn()
		{

			DebugConsole.Log("Subtraction || doorOn: {0}", doorIsOn);

			if(!doorIsOn)
			{
				float clipLength = playSound(basePath + skillInstructionPath + "2F-when-youre-done-touch-the-switch", TOUCH_SWITCH);
				
				dGlowDelays = new float[]{ clipLength / 3f, clipLength * 0.75f };
				
				beginDoorGlow();
				
				stopTimer();
			}
			if(!house.door.hasEventListener(MouseEvent.MOUSE_DOWN))
				house.door.addEventListener(MouseEvent.MOUSE_DOWN, doorClick);
			doorIsOn = true;
		}
		
		private void doorOff()
		{
			if(house == null)
				return;
			if(house.door.hasEventListener(MouseEvent.MOUSE_DOWN))
				house.door.removeEventListener(MouseEvent.MOUSE_DOWN, doorClick);
			doorIsOn = false;
		}
		
		private void onMouseDown( CEvent e )
		{
			PlayClickSound();
			
			draggersOff();
			timeoutCount = 0;
			currentObject = e.currentTarget as SubtractionDragger;
			dragOffset = new Vector2(currentObject.x - currentObject.parent.mouseX, currentObject.y - currentObject.parent.mouseY);
			
			view.removeChild(currentObject);
			view.addChild(currentObject);
			
			currentObject.scaleX *= 1.5f;
			currentObject.scaleY *= 1.5f;
			
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onObjectMove);
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			
			onObjectMove(null);
		}
		
		private void onObjectMove( CEvent e )
		{
			currentObject.x = currentObject.parent.mouseX+dragOffset.x;
			currentObject.y = currentObject.parent.mouseY+dragOffset.y;
		}
		
		private void onMouseUp( CEvent e )
		{
			draggersOn();
			currentObject.scaleX *= 2f/3f;
			currentObject.scaleY *= 2f/3f;
			checkIn(currentObject);
			currentObject = null;
			
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, onObjectMove);
			stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}
		
		private void checkIn(SubtractionDragger d)
		{

			Rectangle dbox = d.getBounds(view);
			float _distance = Vector2.Distance(starfishCenter, new Vector2(dbox.x, dbox.y));

			Debug.LogError ("Drop distance: " + _distance + " Starfish distance: " + starfishDistance);
			if (_distance < starfishDistance) {
				if(!d.inHouse)
				{
					currentOut--;
					AddNewActionData( formatActionData( "move", System.Array.IndexOf( draggers, d ) + 1, d.parent.localToGlobal(new Vector2( d.snapInX, d.snapInY )) ) );
				}
				else
				{
					AddNewActionData( formatActionData( "snap", System.Array.IndexOf( draggers, d ) + 1, Vector2.zero ) );
				}
				d.snapIn ();
			} else {
				if(d.inHouse)
				{
					currentOut++;
					if(_level == SkillLevel.Tutorial)
						draggersOff();
					if(!doorIsOn)
						doorOn();
					AddNewActionData( formatActionData( "move", System.Array.IndexOf( draggers, d ) + 1, d.parent.localToGlobal(new Vector2( d.snapOutX, d.snapOutY )) ) );
				}
				else
				{
					AddNewActionData( formatActionData( "snap", System.Array.IndexOf( draggers, d ) + 1, Vector2.zero ) );
				}
				d.snapOut();
			}

		}
		
		private void doorClick(CEvent e)
		{
			draggersOff();
			timeoutCount = 0;
			doorOff();

			DebugConsole.LogWarning("currentOut: {0}, levelOut: {1}, levelIn: {2}", currentOut, levelIn, levelOut);

			if(levelOut == currentOut)
			{
				house.closeDoor();
				
				PlayCorrectSound();
				
				AddNewActionData( formatActionData( "tap", 0, Vector2.zero, true ) );
			}
			else
			{
				foreach(SubtractionDragger d in draggers)
					d.snapIn();
				opportunityCorrect = false;
				opportunityAnswered = true;
				failCount++;
				if(failCount == 3)
					opportunityComplete = true;
				PlayIncorrectSound();
				
				AddNewActionData( formatActionData( "tap", 0, Vector2.zero, false ) );
			}
			
			PlayClickSound();
		}
		
		private void beginDraggerGlow(int glows)
		{
			if(glows > 1)
			{
				glow2 = true;
				if(glows > 2)
					glow3 = true;
			}
			glowDelayCount = 0;
			
			if(glowTimer != null) glowTimer.StopTimer();
			glowTimer = TimerUtils.SetTimeout(glowDelays[glowDelayCount], glowDelayed);
		}
		
		private void glowDelayed()
		{
			glowDelayCount++;
			
			SubtractionDragger d;
			float delay = 0;
			if(glowDelayCount < 6)
				delay = glowDelays[glowDelayCount];
			if(levelShort == "EG")
				delay -= bGlowMod;
			switch(glowDelayCount)
			{
			case 1:
				for(int i = 0; i < levelIn; i++)
				{
					d = draggers[i];
					d.glowOn();
				}
				if(glowTimer != null) glowTimer.StopTimer();
				glowTimer = TimerUtils.SetTimeout(delay, glowDelayed);
				break;
			case 2:
				for(int i = 0; i < levelIn; i++)
				{
					d = draggers[i];
					d.glowOff();
				}
				if(glow2)
				{
					if(glowTimer != null) glowTimer.StopTimer();
					glowTimer = TimerUtils.SetTimeout(delay, glowDelayed);
					
					glow2 = false;
				}
				break;
			case 3:
				int shouldBeOut = levelOut - currentOut;
				for(int i = 0; i < levelIn; i++)
				{
					d = draggers[i];
					if(!d.inHouse)
					{
						if(shouldBeOut < 0)
						{
							shouldBeOut++;
						}
						else
							d.glowOn();
					}
					else if(shouldBeOut > 0)
					{
						d.glowOn();
						shouldBeOut--;
					}
				}
				if(glowTimer != null) glowTimer.StopTimer();
				glowTimer = TimerUtils.SetTimeout(delay, glowDelayed);
				break;
			case 4:
				for(int i = 0; i < levelIn; i++)
				{
					d = draggers[i];
					d.glowOff();
				}
				if(glow3)
				{
					if(glowTimer != null) glowTimer.StopTimer();
					glowTimer = TimerUtils.SetTimeout(delay, glowDelayed);
					
					glow3 = false;
				}
				break;
			case 5:
				for(int i = 0; i < levelIn; i++)
				{
					d = draggers[i];
					if(d.inHouse)
						d.glowOn();
				}
				if(glowTimer != null) glowTimer.StopTimer();
				glowTimer = TimerUtils.SetTimeout(delay, glowDelayed);
				break;
			case 6:
				for(int i = 0; i < levelIn; i++)
				{
					d = draggers[i];
					d.glowOff();
				}
				break;
			}
		}
		
		private void beginDoorGlow()
		{
			glowDelayCount = 0;
			
			if(glowTimer != null) glowTimer.StopTimer();
			glowTimer = TimerUtils.SetTimeout(dGlowDelays[glowDelayCount], dGlowDelayed);
		}
		
		private void dGlowDelayed()
		{
			glowDelayCount++;
			switch(glowDelayCount)
			{
			case 1:
				house.glowOn();
				if(glowTimer != null) glowTimer.StopTimer();
				glowTimer = TimerUtils.SetTimeout(dGlowDelays[glowDelayCount], dGlowDelayed);
				break;
			case 2:
				house.glowOff();
				break;
			}
		}
		
		private void autoClear()
		{
			currentOut = 0;
			
			SubtractionDragger d;
			int i;
			for(i = 0; i < levelOut; i++)
			{
				d = draggers[i];
				d.snapOut();
				currentOut++;
			}
			for(i = levelOut; i < levelIn; i++)
			{
				d = draggers[i];
				d.snapIn();
			}
			
			playSound(basePath + inactivityReinforcementPath + "2F-" + levelShort + "/2F-I-moved-" + levelIn + "-minus-" + levelOut + "-is-" + (levelIn - levelOut).ToString() + "-" + seaAnimalName, YOU_FOUND_INCORRECT);
			beginDraggerGlow(3);
		}
		
		private void hideAll()
		{
			foreach(SubtractionDragger d in draggers)
			{
				view.removeChild(d);
			}
			view.removeChild(house);
		}
		
		public override void onTimerComplete()
		{
			base.onTimerComplete();
			
			stopTimer();
			timeoutCount++;
			
			string clipName = "";
			
			if(_level == SkillLevel.Tutorial)
			{
				if(timeoutCount == 1)
				{
					if(doorIsOn)
					{
						playSound(basePath + skillInstructionPath + "2F-when-youre-done-touch-the-switch", TOUCH_SWITCH);
						clipName = basePath + skillInstructionPath + "2F-when-youre-done-touch-the-switch";
						beginDoorGlow();
					}
					else
					{
						playSound(basePath + skillInstructionPath + "2F-now-its-your-turn", NOW_ITS);
						clipName = basePath + skillInstructionPath + "2F-now-its-your-turn";
					}
				}
				else
				{
					draggersOff();
					doorOff();
					if(!opportunityAnswered)
					{
						opportunityAnswered = true;
						opportunityCorrect = true;
						
						draggersOff();
						doorOff();
						
						playSound(basePath + skillInstructionPath + "2F-try-again", LETS_TRY);
						clipName = basePath + skillInstructionPath + "2F-try-again";
					}
					else
					{
						opportunityComplete = true;
						opportunityCorrect = false;
						dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0"));
					}
				}
			}
			else
			{
				switch(timeoutCount)
				{
					case 1:
						if(doorIsOn)
						{
							playSound(basePath + skillInstructionPath + "2F-when-youre-done-touch-the-switch", TOUCH_SWITCH);
							clipName = basePath + skillInstructionPath + "2F-when-youre-done-touch-the-switch";
							beginDoorGlow();
						}
						else
						{
							playSound(basePath + skillInstructionPath + "2F-keep-your-finger-on-the-" + seaAnimalName, PLEASE_FIND);
							clipName = basePath + skillInstructionPath + "2F-keep-your-finger-on-the-" + seaAnimalName;
							//playSound("Instruction/keep-your-finger-on-" + levelShort, PLEASE_FIND);
						}
						break;
					case 2:
						playSound(basePath + skillInstructionPath + "2F-touch-the-green-button", PLEASE_FIND);
						clipName = basePath + skillInstructionPath + "2F-touch-the-green-button";
						break;
					case 3:
						opportunityCorrect = false;
							
						playSound(basePath + skillInstructionPath + "2F-try-again", NOW_ITS);
						clipName = basePath + skillInstructionPath + "2F-try-again";
						glow2 = true;
						
						draggersOff();
						doorOff();
						foreach(SubtractionDragger d in draggers)
							d.snapIn();
						break;
					default:
						timeoutEnd = true;
						autoClear();
						clipName = basePath + inactivityReinforcementPath + "2F-" + levelShort + "/2F-I-moved-" + levelIn + "-minus-";
						clipName+= levelOut + "-is-" + (levelIn - levelOut).ToString() + "-" + seaAnimalName;
						break;
				}	
			}
			
			if( !string.IsNullOrEmpty( clipName ) )
			{
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf( clipName ) );
			}
		}
		
		private void clearDraggerTweens()
		{
			if(draggers == null)
				return;
			foreach(SubtractionDragger d in draggers)
			{
				Tweener.removeTweens(d);
			}
		}
		
		private float playSound ( string clipName ) { return playSound(clipName, ""); }
		private float playSound ( string clipName, string word )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SUBTRACTION, clipName );
			
			if(word != "")
				bundle.AddClip(clip, word, clip.length);
			else
				bundle.AddClip(clip);
			
			SoundEngine.Instance.PlayBundle(bundle);
			
			return clip.length;
		}
		
		public override void Resize( int width, int height )
		{
			view.scaleX = view.scaleY = ((float)Screen.width)/((float)width);
			view.y = (Screen.height - (height*view.scaleY))/2;
			view.x = 0;
		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = "";
			
			foreach(int i in levelUsed)
			{
				if(data != "")
					data = data + "-";
				data = data + i.ToString();
			}
			return data;
		}
		
		private void parseSessionData (string data)
		{
			levelUsed = new int[10];
			
			string[] sData = data.Split('-');
			
			int i = 0;
			foreach( string s in sData )
			{
				levelUsed[i] = int.Parse(s);
				i++;
			}
		}

		private string getSeaAnimal(string shortNameRef) {

			string animalName = "starfish";

			switch (shortNameRef) {
			case "DG":
				animalName = "sea-urchins";
				break;

			case "DDCD":
				animalName = "snails";
				break;

			}

			return animalName;
		}
		
		private static List<string> audioNames = new List<string>
		{	"",
			"Narration/2F-Story Introduction Audio/2F-story-introduction",
			"Narration/2F-Skill Instruction Audio/2F-Henry-please-move-one-starfish",
			"Narration/2F-Skill Instruction Audio/2F-when-youre-done-touch-the-switch",
			"Narration/2F-Skill Instruction Audio/2F-there-were-5-starfish-on-the-submarine",
			"Narration/2F-Skill Instruction Audio/2F-now-its-your-turn",
			"Narration/2F-Skill Instruction Audio/2F-lets-keep-unsticking-the-animals",
			"",
			"Narration/2F-Skill Instruction Audio/2F-touch-the-green-button",
			"Narration/2F-Skill Instruction Audio/2F-try-again",
			"Narration/2F-Skill Instruction Audio/2F-keep-your-finger-on-the-starfish",
			"Narration/2F-Skill Instruction Audio/2F-keep-your-finger-on-the-sea-urchins",
			"Narration/2F-Skill Instruction Audio/2F-keep-your-finger-on-the-snails",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-EG/2F-2-minus-1-starfish",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-EG/2F-3-minus-1-starfish",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-EG/2F-3-minus-2-starfish",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-EG/2F-4-minus-1-starfish",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-EG/2F-5-minus-1-starfish",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-DG/2F-4-minus-2-sea-urchins",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-DG/2F-4-minus-3-sea-urchins",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-DG/2F-5-minus-2-sea-urchins",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-DG/2F-5-minus-3-sea-urchins",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-DG/2F-5-minus-4-sea-urchins",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-DDCD/2F-2-minus-1-snails",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-DDCD/2F-3-minus-1-snails",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-DDCD/2F-3-minus-2-snails",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-DDCD/2F-4-minus-1-snails",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-DDCD/2F-4-minus-2-snails",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-DDCD/2F-4-minus-3-snails",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-DDCD/2F-5-minus-1-snails",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-DDCD/2F-5-minus-2-snails",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-DDCD/2F-5-minus-3-snails",
			"Narration/2F-Skill Instruction Audio/2F-Initial Instruction/2F-DDCD/2F-5-minus-4-snails",
			"Narration/2F-Reinforcement Audio/2F-EG/2F-2-minus-1-is-1-starfish",
			"Narration/2F-Reinforcement Audio/2F-EG/2F-3-minus-1-is-2-starfish",
			"Narration/2F-Reinforcement Audio/2F-EG/2F-3-minus-2-is-1-starfish",
			"Narration/2F-Reinforcement Audio/2F-EG/2F-4-minus-1-is-3-starfish",
			"Narration/2F-Reinforcement Audio/2F-EG/2F-5-minus-1-is-4-starfish",
			"Narration/2F-Reinforcement Audio/2F-DG/2F-4-minus-2-is-2-sea-urchins",
			"Narration/2F-Reinforcement Audio/2F-DG/2F-4-minus-3-is-1-sea-urchins",
			"Narration/2F-Reinforcement Audio/2F-DG/2F-5-minus-2-is-3-sea-urchins",
			"Narration/2F-Reinforcement Audio/2F-DG/2F-5-minus-3-is-2-sea-urchins",
			"Narration/2F-Reinforcement Audio/2F-DG/2F-5-minus-4-is-1-sea-urchins",
			"Narration/2F-Reinforcement Audio/2F-DDCD/2F-2-minus-1-is-1-snails",
			"Narration/2F-Reinforcement Audio/2F-DDCD/2F-3-minus-1-is-2-snails",
			"Narration/2F-Reinforcement Audio/2F-DDCD/2F-3-minus-2-is-1-snails",
			"Narration/2F-Reinforcement Audio/2F-DDCD/2F-4-minus-1-is-3-snails",
			"Narration/2F-Reinforcement Audio/2F-DDCD/2F-4-minus-2-is-2-snails",
			"Narration/2F-Reinforcement Audio/2F-DDCD/2F-4-minus-3-is-1-snails",
			"Narration/2F-Reinforcement Audio/2F-DDCD/2F-5-minus-1-is-4-snails",
			"Narration/2F-Reinforcement Audio/2F-DDCD/2F-5-minus-2-is-3-snails",
			"Narration/2F-Reinforcement Audio/2F-DDCD/2F-5-minus-3-is-2-snails",
			"Narration/2F-Reinforcement Audio/2F-DDCD/2F-5-minus-4-is-1-snails",
			"Narration/2F-Inactivity Reinforcement Audio/2F-EG/2F-I-moved-2-minus-1-is-1-starfish",
			"Narration/2F-Inactivity Reinforcement Audio/2F-EG/2F-I-moved-3-minus-1-is-2-starfish",
			"Narration/2F-Inactivity Reinforcement Audio/2F-EG/2F-I-moved-3-minus-2-is-1-starfish",
			"Narration/2F-Inactivity Reinforcement Audio/2F-EG/2F-I-moved-4-minus-1-is-3-starfish",
			"Narration/2F-Inactivity Reinforcement Audio/2F-EG/2F-I-moved-5-minus-1-is-4-starfish",
			"Narration/2F-Inactivity Reinforcement Audio/2F-DG/2F-I-moved-4-minus-2-is-2-sea-urchins",
			"Narration/2F-Inactivity Reinforcement Audio/2F-DG/2F-I-moved-4-minus-3-is-1-sea-urchins",
			"Narration/2F-Inactivity Reinforcement Audio/2F-DG/2F-I-moved-5-minus-2-is-3-sea-urchins",
			"Narration/2F-Inactivity Reinforcement Audio/2F-DG/2F-I-moved-5-minus-3-is-2-sea-urchins",
			"Narration/2F-Inactivity Reinforcement Audio/2F-DG/2F-I-moved-5-minus-4-is-1-sea-urchins",
			"Narration/2F-Inactivity Reinforcement Audio/2F-DDCD/2F-I-moved-2-minus-1-is-1-snails",
			"Narration/2F-Inactivity Reinforcement Audio/2F-DDCD/2F-I-moved-3-minus-1-is-2-snails",
			"Narration/2F-Inactivity Reinforcement Audio/2F-DDCD/2F-I-moved-3-minus-2-is-1-snails",
			"Narration/2F-Inactivity Reinforcement Audio/2F-DDCD/2F-I-moved-4-minus-1-is-3-snails",
			"Narration/2F-Inactivity Reinforcement Audio/2F-DDCD/2F-I-moved-4-minus-2-is-2-snails",
			"Narration/2F-Inactivity Reinforcement Audio/2F-DDCD/2F-I-moved-4-minus-3-is-1-snails",
			"Narration/2F-Inactivity Reinforcement Audio/2F-DDCD/2F-I-moved-5-minus-1-is-4-snails",
			"Narration/2F-Inactivity Reinforcement Audio/2F-DDCD/2F-I-moved-5-minus-2-is-3-snails",
			"Narration/2F-Inactivity Reinforcement Audio/2F-DDCD/2F-I-moved-5-minus-3-is-2-snails",
			"Narration/2F-Inactivity Reinforcement Audio/2F-DDCD/2F-I-moved-5-minus-4-is-1-snails",
			"User-Earns-Artifact"
		};
	}
}