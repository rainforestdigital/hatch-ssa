using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.geom;

namespace SSGCore
{
	public class Addition : BaseSkill
	{		
		private const string TOUCH_DOOR = "Touch_Door";
		private const string LETS_TRY = "Let's Try Again";

		private const string EG = "2E-EG";
		private const string DG = "2E-DG";
		private const string DDCD = "2E-DDCD";

		private DisplayObjectContainer view;
		private int[,] egSets = new int[,]{{1, 1}, {2, 1}, {3, 1}, {4, 1}, {1, 2}, {1, 1}, {2, 1}, {3, 1}, {4, 1}, {1, 2}};
		private int[,] dgSets = new int[,]{{2, 2}, {3, 2}, {1, 3}, {2, 3}, {1, 4}, {2, 2}, {3, 2}, {1, 3}, {2, 3}, {1, 4}};
		private int[,] ddSets = new int[,]{{2, 2}, {3, 2}, {1, 3}, {2, 3}, {1, 4}, {1, 1}, {2, 1}, {3, 1}, {4, 1}, {1, 2}};
		private int[,] levelSets;
		private int[] levelUsed;
		private int levelIn;
		private int levelOut;
		private string levelObject;
		private int currentIn;
		private int glowDelayCount;
		private int failCount;
		private float[] glowDelays = new float[]{.8f, 1.6f, 2f, 1.6f, 1.6f, 1.8f};
		private float[] dGlowDelays = new float[]{.245f, 1.2f};
		private float bGlowMod = 0.3f;
		private float levelRandom;
		private float leeway = 20f;
		private float baseScale;
		private string levelShort;
		private bool glow2 = false;
		private bool glow3 = false;
		private bool doorIsOn = false;
		private TimerObject glowTimer;
		private AdditionDragger currentObject;
		private AdditionDragger[] statics;
		private AdditionDragger[] draggers;
		private AdditionHouse house;
		private AdditionTutorial tutorial;
		private Rect egBounds = new Rect(100, 600, 340, 385);
		private Rect dgBounds = new Rect(100, 600, 545, 360);
		private Rect ddBounds = new Rect(100, 600, 500, 285);
		private Rect levelBounds;
		private Rect goalBounds;
		private Vector2 dragOffset;

		private float snapInStaticScale;
		private float snapOutScaleModifier = 0.75f;
		
		public Addition (SkillLevel level, SessionInfo  currentSession) : base(level, currentSession)
		{

			BACKGROUND = "Background";
			ARTIFACT = "Artifact";

			PreCacheAssets();
		}
		
		private void PreCacheAssets()
		{
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle("Addition", OnAssetsLoaded));
		}
		
		void OnAssetsLoaded(AssetBundle bundle)
		{
			if( bundle != null )
			{
				Init( null );
			}
		}
		
		public override void Dispose ()
		{
			base.Dispose();
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			clearDraggerTweens();
			draggersOff();
			doorOff();
			house.door.Destroy();
			if(statics != null)
			{
				foreach(AdditionDragger sd in statics)
				{
					sd.Destroy();
				}
			}
			if(draggers != null)
			{
				foreach(AdditionDragger dd in draggers)
				{
					dd.Destroy();
				}
			}
			if(currentObject != null)
			{
				stage.removeEventListener(MouseEvent.MOUSE_MOVE, onObjectMove);
				stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
				currentObject.Destroy();
			}
			if(tutorial != null)
			{
				tutorial.View.removeEventListener( TutorialEvent.START, onTutorialStart );
				tutorial.View.removeEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
				foreach(AdditionDragger d in tutorial.bears)
				{
					d.Destroy();
				}
				if( tutorial.house != null )
					tutorial.house.door.Destroy();
			}
			if(glowTimer != null)
				glowTimer.Unload();
		}
		
		/// <summary>
		/// Init the specified parent.
		/// </summary>
		/// <param name='parent'>
		/// Parent movieclip. Not currently used.
		/// </param>
		public override void Init (MovieClip parent)
		{
			base.Init (parent);
			view = new DisplayObjectContainer();
			view.width = MainUI.STAGE_WIDTH;
			view.height = MainUI.STAGE_HEIGHT;
			addChild(view);
			
			totalOpportunities = 10;
			
			levelUsed = new int[10];
			
			Resize(Screen.width, Screen.height);
			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			house = new AdditionHouse(EG);
			baseScale = 1024f/house.width;
			house.door.Destroy();

			switch(_level)
			{
			case SkillLevel.Tutorial:
				totalOpportunities = 1;
				levelShort = EG;
				levelRandom = 9f/12f;
				levelBounds = egBounds;
				levelSets = new int[,] {{2, 1}};
				if(resumingSkill)
					nextOpportunity();
				else
				{
					tutorial = new AdditionTutorial(baseScale);
					view.addChild( tutorial.View );
					tutorial.View.addEventListener( TutorialEvent.START, onTutorialStart );
					tutorial.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
					tutorial.Resize( Screen.width, Screen.height );
				}
				break;
			case SkillLevel.Emerging:
				levelSets = egSets;
				levelShort = EG;
				levelBounds = egBounds;
				break;
			case SkillLevel.Developing:
				levelSets = dgSets;
				levelShort = DG;
				levelBounds = dgBounds;
				break;
			case SkillLevel.Developed:
			case SkillLevel.Completed:
				levelSets = ddSets;
				levelShort = DDCD;
				levelBounds = ddBounds;
				break;
			}

			float houseX;

			if(PlatformUtils.GetPlatform() == Platforms.WIN)
			{
				houseX = 58;
			}
			else
			{
				houseX = 250;
			}

			house = new AdditionHouse(levelShort);
			house.scaleX = house.scaleY = baseScale;
			house.x = houseX;//(PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA) ? 58 : 250;
			house.y = 200;

			if(resumingSkill && _level != SkillLevel.Tutorial)
			{
				parseSessionData( currentSession.sessionData );
			//	nextOpportunity();
			}
			
			opportunityAnswered = false;
			opportunityComplete = false;
			opportunityCorrect = true;
		}
		
		public override void OnAudioWordComplete ( string word)
		{
			switch(word)
			{
			case THEME_INTRO:
				house.alpha = 0;
				if(_level != SkillLevel.Tutorial)
					view.addChild(house);
				//Tweener.addTween(house, Tweener.Hash("time", 0.25f, "alpha", 1f));


				if( !currentSession.skipIntro )
				{
					house.alpha = 0;
					playSound( "2E-Story Introduction Audio/2E-story-introduction", INTRO);
				}
				else
				{
					OnAudioWordComplete( INTRO );
				}

				break;
			case INTRO:
				Tweener.addTween(house, Tweener.Hash("time", 0.5f, "alpha", 1));
				if(_level == SkillLevel.Tutorial)
				{
					tutorial.OnStart();
				}
				else
				{
					nextOpportunity();

				}
				break;
			case NOW_ITS:
				playSound( "2E-Skill Instruction Audio/2E-Initial Instruction/" + levelShort + "/2E-" + levelIn + "-plus-" + levelOut + levelObject, PLEASE_FIND);
				beginDraggerGlow(1);
				break;
			case TOUCH_DOOR:
				startTimer();
				goto case PLEASE_FIND;
			case PLEASE_FIND:
				if(_level != SkillLevel.Tutorial || !doorIsOn)
					draggersOn();
				break;
			case THEME_COMPLIMENT:
				playSound( "2E-Reinforcement Audio/" + levelShort + "/2E-" + levelIn + "-plus-" + levelOut + "-is-" + (levelIn + levelOut) + levelObject, YOU_FOUND);
				beginDraggerGlow(3);
				break;
			case THEME_CRITICISM:
				playSound( "2E-Skill Instruction Audio/2E-Initial Instruction/" + levelShort + "/2E-" + levelIn + "-plus-" + levelOut + levelObject, PLEASE_FIND);
				beginDraggerGlow(2);
				break;
			case YOU_FOUND:
			case YOU_FOUND_INCORRECT:
				if(_level == SkillLevel.Tutorial)
				{
					playSound( "2E-Skill Instruction Audio/2E-let-the-experiment-begin", NOW_LETS);
				}
				else
				{
					goto case NOW_LETS;
				}
				break;
			case NOW_LETS:
				hideAll();
				if(opportunityCorrect)
				{
					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
					//NumCorrect++;
				}
				else
				{
					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
					//NumIncorrect++;
				}
				break;
			case CLICK_CORRECT:
				dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
				break;
			case CLICK_INCORRECT:
				currentIn = levelIn;
				if(opportunityComplete)
					autoClear();
				else
					dispatchEvent( new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
				break;
			case LETS_TRY:
				resetTutorial();
				break;
			}
		}
		
		public override void onTutorialStart( CEvent e )
		{
			tutorial.View.removeEventListener( TutorialEvent.START, onTutorialStart );
		}
		
		private void onTutorialComplete( CEvent e )
		{
			view.addChild(house);
			
			removeChild(tutorial.View);
			
			tutorial.View.removeEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			
			nextOpportunity();
		}
		
		protected override void resetTutorial()
		{

			foreach(AdditionDragger d in tutorial.bears)
			{
				d.Destroy();
			}
			tutorial.house.door.Destroy();
			view.removeChild(tutorial.View);
			tutorial = null;
			tutorial = new AdditionTutorial(baseScale);
			tutorial.Resize(Screen.width, Screen.height );
			
			view.addChild(tutorial.View);
			tutorial.View.addEventListener( TutorialEvent.START, onTutorialStart );
			tutorial.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			currentOpportunity--;
			hideAll();
			tutorial.OnStart();
		}
		
		public override void nextOpportunity()
		{
			if(statics != null)
			{
				foreach(AdditionDragger sd in statics)
				{
					sd.Destroy();
				}
			}
			if(draggers != null)
			{
				foreach(AdditionDragger dd in draggers)
				{
					dd.Destroy();
				}
			}
			
			if(currentOpportunity == totalOpportunities)
			{
				tallyAnswers();
				return;
			}
			
			view.addChild(house);
			doorOff();
			
			int r;
			if(resumingSkill)
			{
				r = levelUsed[currentOpportunity];
				resumingSkill = false;
			}
			else
			{
				do{
					r = Mathf.FloorToInt(Random.value * totalOpportunities);
				}while(r == totalOpportunities || checkLevelUsed(r));
				levelUsed[currentOpportunity] = r;
			}
			
			levelIn = levelSets[r, 0];
			if(_level == SkillLevel.Completed || _level == SkillLevel.Developed)
			{
				levelObject = "-fruit";
				levelOut = Mathf.FloorToInt(Random.Range(levelSets[r, 1] + 1, 6));
				if(levelOut == 6) levelOut--;
			}
			else if(_level == SkillLevel.Developing)
			{
				levelObject = "-vials";
				levelOut = levelSets[r, 1];
			}
			else
			{
				levelObject = "-beans";
				levelOut = levelSets[r, 1];
			}
			currentIn = 0;
			timeoutCount = 0;
			failCount = 0;
			
			if(_level == SkillLevel.Tutorial)
			{
				levelRandom = 3f/12f;
			}
			else
			{
				do{
					levelRandom = Random.value;
				}while(levelRandom == 0f);
			}
			
			int i;
			AdditionDragger d;
			statics = new AdditionDragger[levelIn];
			for( i = 0; i < levelIn; i++)
			{
				d  = new AdditionDragger(levelShort, levelRandom, PlatformUtils.GetPlatform() == Platforms.IOSRETINA ? 0.8f : 1f);
				d.alpha = 0;
				view.addChild(d);
				statics[i] = d;
			}
			
			int avoidNum = Mathf.CeilToInt(levelRandom * 6f);
			
			if(_level == SkillLevel.Tutorial)
			{
				levelRandom = 1f/12f;
			}
			else
			{
				do{
					levelRandom = Random.value;
				}while(levelRandom == 0f || Mathf.CeilToInt(levelRandom*6f) == avoidNum);
			}
			
			draggers = new AdditionDragger[levelOut];
			for( i = 0; i < levelOut; i++)
			{
				d  = new AdditionDragger(levelShort, levelRandom, PlatformUtils.GetPlatform() == Platforms.IOSRETINA ? 0.8f : 1f);
				d.alpha = 0;
				view.addChild(d);
				draggers[i] = d;
			}
			setStaticPos();
			//setDraggerPos();
			
			levelOut = levelSets[r, 1];
			
			house.openDoor();
			foreach(AdditionDragger sd in statics)
			{
				Tweener.addTween(sd, Tweener.Hash("time", 0.5f, "alpha", 1));
			}
			foreach(AdditionDragger dd in draggers)
			{
				Tweener.addTween(dd, Tweener.Hash("time", 0.5f, "alpha", 1));
			}
			dispatchEvent(new SkillEvent(SkillEvent.OPPORTUNITY_START, true, false));
			
			currentOpportunity++;
			
			if(_level == SkillLevel.Tutorial)
			{
				playSound( "2E-Skill Instruction Audio/2E-now-its-your-turn", NOW_ITS);
				
				AddNewOppData( audioNames.IndexOf( "2E-Skill Instruction Audio/2E-now-its-your-turn" ) );
			}
			else
			{
				OnAudioWordComplete(NOW_ITS);
				opportunityAnswered = false;
				opportunityComplete = false;
				opportunityCorrect = true;
				
				AddNewOppData( audioNames.IndexOf( "2E-Skill Instruction Audio/2E-Initial Instruction/" + levelShort + "/2E-" + levelIn + "-plus-" + levelOut + levelObject ) );
			}
			
			AddNewObjectData( formatObjectData( "switch", house.door.parent.localToGlobal( new Vector2( house.door.x, house.door.y ) ) ) );
			foreach( AdditionDragger dr in draggers )
			{
				AddNewObjectData( formatObjectData( levelObject.Substring(1) + dr.variant, dr.parent.localToGlobal(new Vector2( dr.x, dr.y)) ) );
			}
			foreach( AdditionDragger st in statics )
			{
				AddNewObjectData( formatObjectData( levelObject.Substring(1) + st.variant, st.parent.localToGlobal(new Vector2( st.x, st.y)) ) );
			}
		}
		
		private bool checkLevelUsed (int val)
		{
			if(currentOpportunity == 0)
				return false;
			for(int i = 0; i < currentOpportunity - 1; i++)
			{
				if(val == levelUsed[i])
					return true;
			}
			int last = levelUsed[currentOpportunity - 1];
			if(val == last || (levelSets[val, 0] == levelSets[last, 0] && levelSets[val, 1] == levelSets[last, 1]))
				return true;
			
			if(currentOpportunity == totalOpportunities - 3)
			{				
				int[,] sublist = new int[2,2];
				int k = 0;
				bool used;
				for(int j = 0; j < 10; j++)
				{
					used = false;
					if(j == val)
						continue;
					for(int l = 0; l < currentOpportunity; l++)
					{
						if(j == levelUsed[l])
						{
							used = true;
							break;
						}
					}
					if(!used)
					{
						sublist[k,0] = levelSets[j,0];
						sublist[k,1] = levelSets[j,1];
						k++;
					}
				}
				if(sublist[0,0] == sublist[1,0] && sublist[0,1] == sublist[1,1])
					return true;
			}
			return false;
		}
		
		private void setStaticPos()
		{
			currentIn = 0;

			DebugConsole.Log("setStaticPos || {0}", currentIn);

			float dist;
			float sMod;
			if(levelShort == EG)
			{
				dist = (((levelBounds.height + levelBounds.width)/2) - (leeway * 5f)) / 2.5f;
				sMod = 256;
			}
			else
			{
				dist = (levelBounds.height - (leeway * 5f)) / 2.5f;
				sMod = 125;//125;
			}

			float xScale = levelBounds.width / levelBounds.height;

			switch( levelShort )
			{
				case EG:
					setStaticEGPos(xScale, dist, sMod);
				break;
				case DG:
					setStaticDGPos(xScale, dist, sMod);
				break;
				case DDCD:
					setStaticDDCDPos(xScale, dist, sMod);
				break;
			}

			setDraggerPos();

		}

		private void setStaticEGPos( float xScale, float dist, float sMod )
		{

			float angStep = (2f * Mathf.PI) / (levelIn + draggers.Length);
//			Vector2 center = new Vector2(levelBounds.x + (levelBounds.width / 2f),
//			                             levelBounds.y + (levelBounds.height / 2f));

			if( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA)
			{
				goalBounds = new Rect( house.x, house.y, 682.45f * baseScale, 824.35f * baseScale );
			}
			else
			{
				goalBounds = new Rect( house.x, house.y, 350.65f * baseScale, 423.55f * baseScale );
			}

			AdditionDragger d;
			float ang;
			int i;
			for(i = 0; i < levelIn; i++)
			{
				ang = (Mathf.PI / 2f) + angStep;
				ang += angStep * i;
				d = statics[i];
				d.inHouse = true;
				d.snapInX = d.snapOutX = d.x = house.x + 250 + ((Mathf.Cos(ang) * dist) * xScale);//((Mathf.Cos(ang) * dist) * xScale) + center.x;
				d.snapInY = d.snapOutY = d.y = house.y + 400 + (Mathf.Sin(ang) * dist);//(Mathf.Sin(ang) * dist) + center.y;
				d.scaleX = d.scaleY = snapInStaticScale = (dist / sMod) * baseScale * ( levelIn + draggers.Length > 5 ? 1f - (.075f * (levelIn + draggers.Length - 5)) : 1f);
				currentIn++;
			}
			for(i = levelIn; i < levelIn + draggers.Length; i++)
			{
				d = draggers[i - levelIn];
				ang = (Mathf.PI / 2f) + angStep;
				ang += angStep * i;
				d.snapInX = house.x + 250 + ((Mathf.Cos(ang) * dist) * xScale);//((Mathf.Cos(ang) * dist) * xScale) + center.x;
				d.snapInY = house.y + 400 + (Mathf.Sin(ang) * dist);//(Mathf.Sin(ang) * dist) + center.y;
			}
		}

		private void setStaticDGPos( float xScale, float dist, float sMod )
		{

			AdditionDragger d;
			int i;
			int y = 0;
			int x = 0;
			int padding = 20;
			int yshift = 0;

			if( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA)
			{
				goalBounds = new Rect( house.x, house.y, 790f * baseScale, 755f * baseScale );
			}
			else
			{
				goalBounds = new Rect( house.x, house.y, 395f * baseScale, 377.5f * baseScale );
			}

			for(i = 0; i < levelIn; i++)
			{

				if(PlatformUtils.GetPlatform() == Platforms.ANDROID) padding = 70;
				if(PlatformUtils.GetPlatform() == Platforms.IOS){ padding = 70; yshift = -5; }
				if(PlatformUtils.GetPlatform() == Platforms.IOSRETINA) yshift = 20;

				//ang = (Mathf.PI / 2f) + angStep;
				//ang += angStep * i;
				d = statics[i];
				d.inHouse = true;
				d.snapInX = d.snapOutX = d.x = house.x + 100 + ( (i-x) * (d.width + padding) );
				d.snapInY = d.snapOutY = d.y = house.y + 145 + yshift + (y * 275);
				d.scaleX = d.scaleY = snapInStaticScale = (dist / sMod) * baseScale;//(dist / sMod) * baseScale * ( levelIn + draggers.Length > 5 ? 1f - (.075f * (levelIn + draggers.Length - 5)) : 1f);
				currentIn++;
				if( i > 1 && y == 0 ){
					x = 3;
					y = 1;
				}
			}
			for(i = levelIn; i < levelIn + draggers.Length; i++)
			{
				d = draggers[i - levelIn];
				//ang = (Mathf.PI / 2f) + angStep;
				//ang += angStep * i;
				d.snapInX = house.x + 100 + ( (i-x) * (d.width + padding) );
				d.snapInY = house.y + 145 + yshift + (y * 275);
				if( i > 1 && y == 0 ){
					x = 3;
					y = 1;
				}
			}
		}

		private void setStaticDDCDPos( float xScale, float dist, float sMod )
		{

			Debug.Log ("Setting Static fruit pos");
			float angStep = (2f * Mathf.PI) / (levelIn + draggers.Length);
			//Vector2 center = new Vector2(levelBounds.x + (levelBounds.width / 2f),
			//                             levelBounds.y + (levelBounds.height / 2f));

//			Vector2 center = new Vector2(goalBounds.x + (goalBounds.width / 2f) - 20,
//			                             goalBounds.y + (goalBounds.height / 2f) - 20);

			AdditionDragger d;
			float ang;
			int i;

			if( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA)
			{
				goalBounds = new Rect( house.x, house.y, 716.1f * baseScale, 933.1f * baseScale );
			}
			else
			{
				goalBounds = new Rect( house.x, house.y, 352.4f * baseScale, 459.2f * baseScale );
			}

			float distMod = 1.4f;
			float distMod_Y = 1.62f;
			if (PlatformUtils.GetPlatform () == Platforms.WIN || PlatformUtils.GetPlatform () == Platforms.IOSRETINA) { 
				distMod = 1.5f;
				distMod_Y = 1.72f;
			}

			float fruitXOffset = 300;
			float fruitYOffset = 460;

			if (PlatformUtils.GetPlatform () == Platforms.WIN || PlatformUtils.GetPlatform () == Platforms.IOSRETINA) { 
				fruitYOffset = 490;
			}

			for(i = 0; i < levelIn; i++)
			{
				ang = (Mathf.PI / 2f) + angStep;
				ang += angStep * i;
				d = statics[i];
				d.inHouse = true;
				d.snapInX = d.snapOutX = d.x = house.x + fruitXOffset + ((Mathf.Cos(ang) * (dist * distMod)) * xScale);//((Mathf.Cos(ang) * dist) * xScale) + center.x;
				d.snapInY = d.snapOutY = d.y = house.y + fruitYOffset + (Mathf.Sin(ang) * (dist * distMod_Y));//(Mathf.Sin(ang) * dist) + center.y;
				d.scaleX = d.scaleY = snapInStaticScale = (dist / sMod) * baseScale;// * ( levelIn + draggers.Length > 5 ? 1f - (.075f * (levelIn + draggers.Length - 5)) : 1f);
				currentIn++;
			}
			for(i = levelIn; i < levelIn + draggers.Length; i++)
			{
				d = draggers[i - levelIn];
				ang = (Mathf.PI / 2f) + angStep;
				ang += angStep * i;
				d.snapInX = house.x + fruitXOffset + ((Mathf.Cos(ang) * (dist * distMod)) * xScale);//((Mathf.Cos(ang) * dist) * xScale) + center.x;
				d.snapInY = house.y + fruitYOffset + (Mathf.Sin(ang) * (dist * distMod_Y));//(Mathf.Sin(ang) * dist) + center.y;
			}
		}


		private void setDraggerPos()
		{
			currentIn = levelIn;

			DebugConsole.Log("setDraggerPos || {0}", currentIn);

			float angStep = (2f * Mathf.PI) / levelOut;
			float goalRight = levelBounds.x + levelBounds.width;
//			float dist = (MainUI.STAGE_WIDTH - goalRight)/6;
			float vertMod = PlatformUtils.GetClosestRatio() == ScreenRatio.FOURxTHREE ? 3f : 2f;
//			Vector2 center = new Vector2( goalRight + ((MainUI.STAGE_WIDTH - goalRight)/ 2f),
//										 MainUI.STAGE_HEIGHT/ vertMod);

			float draggerPadding = 35;
			float xStartPos = 980;
			float yStartPos = 691;
			if (PlatformUtils.GetPlatform () == Platforms.WIN || PlatformUtils.GetPlatform () == Platforms.IOSRETINA)
			{
				draggerPadding = -65; //For some reason hi-res spacing for draggers is massive
				xStartPos = 1020;
			}
			if( _level == SkillLevel.Developing && PlatformUtils.GetPlatform() == Platforms.IOSRETINA )
			{
				draggerPadding = 0;
			}

			AdditionDragger d;
			float ang;
			for(int i = 0; i < draggers.Length; i++)
			{
				ang = (Mathf.PI / 2f) + angStep;
				ang += angStep * i;
				d = draggers[i];
				d.x = (xStartPos - (draggers[i].width / 2)) + (i * ((draggers[i].width * draggers[i].scaleX) + draggerPadding ));//(Mathf.Cos(ang) * dist) + center.x;
				d.y = yStartPos;//(Mathf.Sin(ang) * dist) + center.y;
				d.scaleX = d.scaleY = baseScale * snapOutScaleModifier;//;//statics[0].scaleX;
				d.inHouse = false;
				d.snapOutX = d.x;
				d.snapOutY = d.y;
			}
		}
		
		private void draggersOn()
		{
			if(!draggers[0].hasEventListener(MouseEvent.MOUSE_DOWN))
			{
				foreach(AdditionDragger d in draggers)
				{
					d.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
				}
			}
			startTimer();
			if(_level != SkillLevel.Tutorial)
				MarkInstructionEnd();
		}
		
		private void draggersOff()
		{
			if(draggers == null)
				return;
			if(draggers[0].hasEventListener(MouseEvent.MOUSE_DOWN))
			{
				foreach(AdditionDragger d in draggers)
				{
					d.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
				}
			}
			stopTimer();
			MarkInstructionStart();
		}
		
		private void doorOn()
		{
			if(!doorIsOn)
			{
				playSound("2E-Skill Instruction Audio/2E-when-youre-done-touch-the-switch", TOUCH_DOOR);
				beginDoorGlow();
				stopTimer();
			}
			if(!house.door.hasEventListener(MouseEvent.MOUSE_DOWN))
				house.door.addEventListener(MouseEvent.MOUSE_DOWN, doorClick);
			doorIsOn = true;
		}
		
		private void doorOff()
		{
			if(house == null)
				return;
			if(house.door.hasEventListener(MouseEvent.MOUSE_DOWN))
				house.door.removeEventListener(MouseEvent.MOUSE_DOWN, doorClick);
			doorIsOn = false;
		}
		
		private void onMouseDown( CEvent e )
		{
			PlayClickSound();
			
			draggersOff();
			timeoutCount = 0;
			currentObject = e.currentTarget as AdditionDragger;
			dragOffset = new Vector2(currentObject.x - currentObject.parent.mouseX, currentObject.y - currentObject.parent.mouseY);
			
			view.removeChild(currentObject);
			view.addChild(currentObject);
			
			currentObject.scaleX = baseScale * 1.5f;
			currentObject.scaleY = baseScale * 1.5f;
			
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onObjectMove);
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			
			onObjectMove(null);
		}
		
		private void onObjectMove( CEvent e )
		{
			currentObject.x = currentObject.parent.mouseX+dragOffset.x;
			currentObject.y = currentObject.parent.mouseY+dragOffset.y;
		}
		
		private void onMouseUp( CEvent e )
		{
			draggersOn();
			//currentObject.scaleX *= 2f/3f;
			//currentObject.scaleY *= 2f/3f;
			checkIn(currentObject);
			//currentObject.scaleX = baseScale;
			//currentObject.scaleY = baseScale;

			currentObject = null;
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, onObjectMove);
			stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}
		
		private void checkIn(AdditionDragger d)
		{
			Rectangle dbox = d.getBounds(view);
			
			//if(dbox.x > levelBounds.x + levelBounds.width - leeway || levelBounds.x > dbox.x + dbox.width - leeway ||
			//	dbox.y > levelBounds.y + levelBounds.height - leeway || levelBounds.y > dbox.y + dbox.height - leeway)
			if(dbox.x > goalBounds.x + goalBounds.width - leeway || goalBounds.x > dbox.x + dbox.width - leeway ||
			   dbox.y > goalBounds.y + goalBounds.height - leeway || goalBounds.y > dbox.y + dbox.height - leeway)

			{
				if(d.inHouse){
					currentIn--;
					AddNewActionData( formatActionData( "move", System.Array.IndexOf(draggers, d), d.parent.localToGlobal(new Vector2(d.snapOutX, d.snapOutY)) ) );
				}
				else
					AddNewActionData( formatActionData( "snap", System.Array.IndexOf(draggers, d) + 1, Vector2.zero ) );
				d.snapOut();
				d.scaleX = d.scaleY = baseScale * snapOutScaleModifier;//snapInStaticScale;
			}
			else
			{
				if(!d.inHouse)
				{
					currentIn++;
					if(_level == SkillLevel.Tutorial)
						draggersOff();
					if(!doorIsOn)
						doorOn();
					AddNewActionData( formatActionData( "move", System.Array.IndexOf(draggers, d), d.parent.localToGlobal(new Vector2(d.snapInX, d.snapInY)) ) );
				}
				else
					AddNewActionData( formatActionData( "snap", System.Array.IndexOf(draggers, d) + 1, Vector2.zero ) );
				d.snapIn();
				d.scaleX = snapInStaticScale;//snapInStaticScale;
				d.scaleY = snapInStaticScale;//snapInStaticScale;
			}
		}
		
		private void doorClick(CEvent e)
		{
			draggersOff();
			timeoutCount = 0;
			doorOff();

			DebugConsole.LogWarning("levelIn: {0}, levelOut: {1}, currentIn: {2}", levelIn, levelOut, currentIn);

			if(levelIn + levelOut == currentIn)
			{
				house.closeDoor();
				
				PlayCorrectSound();
			
				AddNewActionData( formatActionData( "tap", 0, Vector2.zero, true ) );
			}
			else
			{
				foreach(AdditionDragger d in draggers) {
					d.snapOut();
					d.scaleX = d.scaleY = baseScale * snapOutScaleModifier;
				}
				opportunityCorrect = false;
				opportunityAnswered = true;
				failCount++;
				if(failCount == 3)
					opportunityComplete = true;
				PlayIncorrectSound();
			
				AddNewActionData( formatActionData( "tap", 0, Vector2.zero, false ) );
			}
			PlayClickSound();
		}
		
		private void beginDraggerGlow(int glows)
		{
			if(glows > 1)
			{
				glow2 = true;
				if(glows > 2)
					glow3 = true;
			}
			glowDelayCount = 0;
			
			if(glowTimer != null) glowTimer.StopTimer();
			glowTimer = TimerUtils.SetTimeout(glowDelays[glowDelayCount], glowDelayed);
		}
		
		private void glowDelayed()
		{
			glowDelayCount++;
			
			float delay = 0;
			if(glowDelayCount < 6)
				delay = glowDelays[glowDelayCount];
			if(levelShort == EG)
				delay -= bGlowMod;
			switch(glowDelayCount)
			{
			case 1:
				foreach(AdditionDragger d in statics)
				{
					d.glowOn();
				}
				if(glowTimer != null) glowTimer.StopTimer();
				glowTimer = TimerUtils.SetTimeout(delay, glowDelayed);
				break;
			case 2:
				foreach(AdditionDragger d in statics)
				{
					d.glowOff();
				}
				if(glow2)
				{
					if(glowTimer != null) glowTimer.StopTimer();
					glowTimer = TimerUtils.SetTimeout(delay, glowDelayed);
					
					glow2 = false;
				}
				break;
			case 3:
				int shouldBeIn = levelOut - (currentIn - levelIn);
				foreach(AdditionDragger d in draggers)
				{
					if(d.inHouse)
					{
						if(shouldBeIn < 0)
						{
							shouldBeIn++;
						}
						else
							d.glowOn();
					}
					else if(shouldBeIn > 0)
					{
						d.glowOn();
						shouldBeIn--;
					}
				}
				if(glowTimer != null) glowTimer.StopTimer();
				glowTimer = TimerUtils.SetTimeout(delay, glowDelayed);
				break;
			case 4:
				foreach(AdditionDragger d in draggers)
				{
					d.glowOff();
				}
				if(glow3)
				{
					if(glowTimer != null) glowTimer.StopTimer();
					glowTimer = TimerUtils.SetTimeout(delay, glowDelayed);
					
					glow3 = false;
				}
				break;
			case 5:
				foreach(AdditionDragger d in statics)
				{
					if(d.inHouse)
						d.glowOn();
				}
				foreach(AdditionDragger d in draggers)
				{
					if(d.inHouse)
						d.glowOn();
				}
				if(glowTimer != null) glowTimer.StopTimer();
				glowTimer = TimerUtils.SetTimeout(delay, glowDelayed);
				break;
			case 6:
				foreach(AdditionDragger d in statics)
				{
					d.glowOff();
				}
				foreach(AdditionDragger d in draggers)
				{
					d.glowOff();
				}
				break;
			}
		}
		
		private void beginDoorGlow()
		{
			glowDelayCount = 0;
			
			if(glowTimer != null) glowTimer.StopTimer();
			glowTimer = TimerUtils.SetTimeout(dGlowDelays[glowDelayCount], dGlowDelayed);
		}
		
		private void dGlowDelayed()
		{
			glowDelayCount++;
			switch(glowDelayCount)
			{
			case 1:
				house.glowOn();
				if(glowTimer != null) glowTimer.StopTimer();
				glowTimer = TimerUtils.SetTimeout(dGlowDelays[glowDelayCount], dGlowDelayed);
				break;
			case 2:
				house.glowOff();
				break;
			}
		}
		
		private string autoClear()
		{
			currentIn = 0;

			DebugConsole.Log("autoClear || {0}", currentIn);

			AdditionDragger d;
			int i;
			for(i = 0; i < levelOut; i++)
			{
				d = draggers[i];
				d.snapIn();
				d.scaleX = d.scaleY = snapInStaticScale;
				currentIn++;
			}
			for(i = levelOut; i < draggers.Length; i++)
			{
				d = draggers[i];
				d.snapOut();
			}
			
			beginDraggerGlow(3);
			return playSound("2E-Inactivity Reinforcement Audio/" + levelShort + "/2E-I-added-" + levelIn + "-plus-" + levelOut + "-is-" + (levelIn + levelOut) + levelObject, YOU_FOUND_INCORRECT);
		}
		
		private void hideAll()
		{
			foreach(AdditionDragger d in draggers)
			{
				view.removeChild(d);
			}
			foreach(AdditionDragger ad in statics)
			{
				view.removeChild(ad);
			}
			view.removeChild(house);
		}
		
		public override void onTimerComplete()
		{
			base.onTimerComplete();
			
			stopTimer();
			timeoutCount++;
			
			string clipName = "";
			
			if(_level == SkillLevel.Tutorial)
			{
				if(timeoutCount == 1)
				{
					if(doorIsOn)
					{
						clipName = playSound("2E-Skill Instruction Audio/2E-when-youre-done-touch-the-switch", TOUCH_DOOR);
						beginDoorGlow();
					}
					else
					{
						clipName = playSound("2E-Skill Instruction Audio/2E-now-its-your-turn", NOW_ITS);
					}
				}
				else
				{
					draggersOff();
					doorOff();
					if(!opportunityAnswered)
					{
						opportunityAnswered = true;
						opportunityCorrect = true;
						
						draggersOff();
						doorOff();
						
						clipName = playSound("2E-Skill Instruction Audio/2E-try-again", LETS_TRY);
					}
					else
					{
						opportunityCorrect = true;
						opportunityComplete = true;
						dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0"));
					}
				}
			}
			else
			{
				switch(timeoutCount)
				{
					case 1:
						if(doorIsOn)
						{
							clipName = playSound("2E-Skill Instruction Audio/2E-when-youre-done-touch-the-switch", TOUCH_DOOR);
							beginDoorGlow();
						}
						else
						{
							clipName = playSound("2E-Skill Instruction Audio/2E-keep-your-finger-on-the" + levelObject, PLEASE_FIND);
						}
						break;
					case 2:
						clipName = playSound("2E-Skill Instruction Audio/2E-touch-the-green-button", PLEASE_FIND);
						break;
					case 3:
						opportunityCorrect = false;
							
						clipName = playSound("2E-Skill Instruction Audio/2E-try-again", NOW_ITS);
						glow2 = true;
						
						draggersOff();
						doorOff();
						foreach(AdditionDragger d in draggers)
						{
							d.snapOut();
							d.scaleX = d.scaleY = baseScale * snapOutScaleModifier;
						}
						break;
					default:
						timeoutEnd = true;
						clipName = autoClear();
						break;
				}	
			}
			
			if( !string.IsNullOrEmpty( clipName ) )
			{
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf( clipName ) );
			}
		}
		
		private void clearDraggerTweens()
		{
			if(draggers == null)
				return;
			foreach(AdditionDragger d in draggers)
			{
				Tweener.removeTweens(d);
			}
		}
		
		private string playSound ( string clipName ) { return playSound(clipName, ""); }
		private string playSound ( string clipName, string word )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.ADDITION, clipName );
			
			if(word != "")
				bundle.AddClip(clip, word, clip.length);
			else
				bundle.AddClip(clip);
			
			SoundEngine.Instance.PlayBundle(bundle);
			
			return clipName;
		}
		
		public override void Resize( int width, int height )
		{
			//this.width = width;
			//this.height = height;
			//view.FitToParent();
			//this.x = 0;
			//this.y = 0;
			view.scaleY = height/view.height;
			view.scaleX = view.scaleY;
			view.y = 0;
			view.x = (width - (view.width*view.scaleX))/2;
		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = "";
			
			foreach(int i in levelUsed)
			{
				if(data != "")
					data = data + "-";
				data = data + i.ToString();
			}
			return data;
		}
		
		private void parseSessionData (string data)
		{
			levelUsed = new int[10];
			
			string[] sData = data.Split('-');
			
			int i = 0;
			foreach( string s in sData )
			{
				levelUsed[i] = int.Parse(s);
				i++;
			}
		}
		
		private static List<string> audioNames = new List<string>
		{	"",
			"2E-Story Introduction Audio/2E-story-introduction",
			"2E-Skill Instruction Audio/2E-these-plants-need-to-be-planted",
			"2E-Skill Instruction Audio/2E-when-youre-done-touch-the-switch",
			"2E-Skill Instruction Audio/2E-there-were-two-beans",
			"2E-Skill Instruction Audio/2E-now-its-your-turn",
			"",
			"2E-Skill Instruction Audio/2E-try-again",
			"2E-Skill Instruction Audio/2E-keep-your-finger-on-the-beans",
			"2E-Skill Instruction Audio/2E-keep-your-finger-on-the-vials",
			"2E-Skill Instruction Audio/2E-keep-your-finger-on-the-fruit",
			"2E-Skill Instruction Audio/2E-touch-the-green-button",
			"2E-Skill Instruction Audio/2E-let-the-experiment-begin",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-EG/2E-2-plus-1-beans",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-EG/2E-3-plus-1-beans",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-EG/2E-4-plus-1-beans",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-EG/2E-1-plus-1-beans",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-EG/2E-1-plus-2-beans",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-DG/2E-2-plus-2-vials",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-DG/2E-2-plus-3-vials",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-DG/2E-3-plus-2-vials",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-DG/2E-1-plus-3-vials",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-DG/2E-1-plus-4-vials",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-DDCD/2E-2-plus-1-fruit",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-DDCD/2E-2-plus-2-fruit",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-DDCD/2E-2-plus-3-fruit",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-DDCD/2E-3-plus-1-fruit",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-DDCD/2E-3-plus-2-fruit",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-DDCD/2E-4-plus-1-fruit",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-DDCD/2E-1-plus-1-fruit",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-DDCD/2E-1-plus-2-fruit",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-DDCD/2E-1-plus-3-fruit",
			"2E-Skill Instruction Audio/2E-Initial Instruction/2E-DDCD/2E-1-plus-4-fruit",
			"2E-Reinforcement Audio/2E-EG/2E-1-plus-1-is-2-beans",
			"2E-Reinforcement Audio/2E-EG/2E-1-plus-2-is-3-beans",
			"2E-Reinforcement Audio/2E-EG/2E-2-plus-1-is-3-beans",
			"2E-Reinforcement Audio/2E-EG/2E-3-plus-1-is-4-beans",
			"2E-Reinforcement Audio/2E-EG/2E-4-plus-1-is-5-beans",
			"2E-Reinforcement Audio/2E-DG/2E-1-plus-3-is-4-vials",
			"2E-Reinforcement Audio/2E-DG/2E-1-plus-4-is-5-vials",
			"2E-Reinforcement Audio/2E-DG/2E-2-plus-2-is-4-vials",
			"2E-Reinforcement Audio/2E-DG/2E-2-plus-3-is-5-vials",
			"2E-Reinforcement Audio/2E-DG/2E-3-plus-2-is-5-vials",
			"2E-Reinforcement Audio/2E-DDCD/2E-1-plus-1-is-2-fruit",
			"2E-Reinforcement Audio/2E-DDCD/2E-1-plus-2-is-3-fruit",
			"2E-Reinforcement Audio/2E-DDCD/2E-1-plus-3-is-4-fruit",
			"2E-Reinforcement Audio/2E-DDCD/2E-1-plus-4-is-5-fruit",
			"2E-Reinforcement Audio/2E-DDCD/2E-2-plus-1-is-3-fruit",
			"2E-Reinforcement Audio/2E-DDCD/2E-2-plus-2-is-4-fruit",
			"2E-Reinforcement Audio/2E-DDCD/2E-2-plus-3-is-5-fruit",
			"2E-Reinforcement Audio/2E-DDCD/2E-3-plus-1-is-4-fruit",
			"2E-Reinforcement Audio/2E-DDCD/2E-3-plus-2-is-5-fruit",
			"2E-Reinforcement Audio/2E-DDCD/2E-4-plus-1-is-5-fruit",
			"2E-Inactivity Reinforcement Audio/2E-EG/2E-I-added-1-plus-1-is-2-beans",
			"2E-Inactivity Reinforcement Audio/2E-EG/2E-I-added-1-plus-2-is-3-beans",
			"2E-Inactivity Reinforcement Audio/2E-EG/2E-I-added-2-plus-1-is-3-beans",
			"2E-Inactivity Reinforcement Audio/2E-EG/2E-I-added-3-plus-1-is-4-beans",
			"2E-Inactivity Reinforcement Audio/2E-EG/2E-I-added-4-plus-1-is-5-beans",
			"2E-Inactivity Reinforcement Audio/2E-DG/2E-I-added-1-plus-3-is-4-vials",
			"2E-Inactivity Reinforcement Audio/2E-DG/2E-I-added-1-plus-4-is-5-vials",
			"2E-Inactivity Reinforcement Audio/2E-DG/2E-I-added-2-plus-2-is-4-vials",
			"2E-Inactivity Reinforcement Audio/2E-DG/2E-I-added-2-plus-3-is-5-vials",
			"2E-Inactivity Reinforcement Audio/2E-DG/2E-I-added-3-plus-2-is-5-vials",
			"2E-Inactivity Reinforcement Audio/2E-DDCD/2E-I-added-1-plus-1-is-2-fruit",
			"2E-Inactivity Reinforcement Audio/2E-DDCD/2E-I-added-1-plus-2-is-3-fruit",
			"2E-Inactivity Reinforcement Audio/2E-DDCD/2E-I-added-1-plus-3-is-4-fruit",
			"2E-Inactivity Reinforcement Audio/2E-DDCD/2E-I-added-1-plus-4-is-5-fruit",
			"2E-Inactivity Reinforcement Audio/2E-DDCD/2E-I-added-2-plus-1-is-3-fruit",
			"2E-Inactivity Reinforcement Audio/2E-DDCD/2E-I-added-2-plus-2-is-4-fruit",
			"2E-Inactivity Reinforcement Audio/2E-DDCD/2E-I-added-2-plus-3-is-5-fruit",
			"2E-Inactivity Reinforcement Audio/2E-DDCD/2E-I-added-3-plus-1-is-4-fruit",
			"2E-Inactivity Reinforcement Audio/2E-DDCD/2E-I-added-3-plus-2-is-5-fruit",
			"2E-Inactivity Reinforcement Audio/2E-DDCD/2E-I-added-4-plus-1-is-5-fruit",
			"User-Earns-Artifact"
		};
	}
}