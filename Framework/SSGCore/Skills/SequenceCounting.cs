using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.geom;

namespace SSGCore
{
	public class SequenceCounting : BaseSkill
	{
		private const string COUNTING = "Counting";
		private const string LETS_TRY = "Let's_Try_Again";
		
		private DisplayObjectContainer container;
		private SequenceCountingTutorial tutorial;
		
		private string currentType;
		private int nextNum;
		private SequenceCounter currentCounter;
		private int failCount;
		private int countAt;
		
		private string[] egOpps = new string[]{"TURBINE", "TURBINE", "TURBINE", "TURBINE", "GEAR", "GEAR", "GEAR", "BATTERY", "BATTERY", "BATTERY"};
		private string[] dgOpps = new string[]{"TURBINE", "TURBINE", "GEAR", "GEAR", "GEAR", "GEAR", "GEAR", "GEAR", "BATTERY", "BATTERY", "BATTERY", "BATTERY"};
		private string[] ddOpps = new string[]{"BATTERY", "BATTERY", "BATTERY", "BATTERY", "BATTERY", "BATTERY", "BATTERY", "BATTERY", "BATTERY", "BATTERY",
														"GEAR", "GEAR", "GEAR", "TURBINE", "TURBINE", "TURBINE"};
		private List<string> oppsLeft;
		
		private List<SequenceCounter> counters;
		private SequenceTarget target;
		
		private int rightPart = 0;
		private int wrongPart = 0;
		private int right = 0;
		private int wrong = 0;
		
		private float baseScale = 1;
		private bool henryTutorial = false;

		public SequenceCounting (SkillLevel level, SessionInfo  currentSession) : base(level, currentSession)
		{
			BACKGROUND = "background";
			ARTIFACT = "artifact";
			
			PreCacheAssets();
		}
		
		private void PreCacheAssets()
		{
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle("SequenceCounting", OnAssetsLoaded));
		}
		
		void OnAssetsLoaded(AssetBundle bundle)
		{
			if( bundle != null )
			{
				Init( null );
			}
		}
		
		public override void Dispose ()
		{
			base.Dispose();
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			
			if(tutorial != null)
				tutorial.Destroy();
			if(counters != null)
			{
				foreach(SequenceCounter c in counters)
					c.Destroy();
			}
		}
		
		public override void Init (MovieClip parent)
		{
			base.Init(parent);
			
			container = new DisplayObjectContainer();
			container.width = 2048f;
			container.height = 1536f;
			container.alpha = 0;
			container.visible = false;
			addChild(container);
			
			target = MovieClipFactory.CreateSequenceCountingTarget();
			baseScale = 2035f/target.width;
			container.addChild(target);
			target.scaleX = target.scaleY = baseScale;
			target.x = 15f;
			target.y = 185f;
			
			Resize(Screen.width, Screen.height);
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			switch(_level)
			{
			case SkillLevel.Tutorial:
				totalOpportunities = 1;
				if(!resumingSkill)
				{
					tutorial = new SequenceCountingTutorial();
					tutorial.baseScale = baseScale;
					tutorial.target = target;
					tutorial.container = container;
					tutorial.View.addEventListener( TutorialEvent.START, onTutorialStart );
					tutorial.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
					tutorial.Resize(Screen.width, Screen.height);
				}
				else
				{
					nextOpportunity();
					return;
				}
				break;
			case SkillLevel.Emerging:
				totalOpportunities = 10;
				oppsLeft = new List<string>(egOpps);
				break;
			case SkillLevel.Developing:
				totalOpportunities = 12;
				oppsLeft = new List<string>(dgOpps);
				break;
			case SkillLevel.Developed:
			case SkillLevel.Completed:
				totalOpportunities = 16;
				oppsLeft = new List<string>(ddOpps);
				break;
			}
			if(resumingSkill)
			{
				parseSessionData(currentSession.sessionData);
				//nextOpportunity();
			}
		}
		
		public override void onTutorialStart( CEvent e )
		{
			base.onTutorialStart(e);
			tutorial.View.removeEventListener( TutorialEvent.START, onTutorialStart );
			addChild(tutorial.View);
		}
		
		private void onTutorialComplete( CEvent e )
		{
			tutorial.View.removeEventListener(TutorialEvent.COMPLETE, onTutorialComplete);
			removeChild(tutorial.View);
			
			container.alpha = 1;
			nextOpportunity();
		}
		
		public override void OnAudioWordComplete ( string word)
		{
			switch(word)
			{
			case THEME_INTRO:
				if( !currentSession.skipIntro )
					playSound("StoryIntroduction", INTRO);
				else
					OnAudioWordComplete( INTRO );
				break;
				
			case INTRO:
				if(_level == SkillLevel.Tutorial)
				{
					nextOpportunity (true);
					tutorial.counters = counters;
					tutorial.OnStart();
					fadeInContainer (1f);
				}
				else
				{
					nextOpportunity();
				}
				break;
				
			case NOW_ITS:
				string path = "";
				if(nextNum == 1)
					path = "number-1-";
				else
					path = "next-";
				playSound("Initial Instruction/" + path + currentType.ToLower(), PLEASE_FIND);
				break;
				
			case PLEASE_FIND:
				countersOn();
				break;
				
			case CLICK_CORRECT:
				if(opportunityComplete)
					playSound( "Numerals/" + (nextNum), THIS_IS );
				else
					playSound( "Numerals/" + (nextNum - 1), YOU_FOUND );
				break;
				
			case THIS_IS:
				if(opportunityComplete)
				{
					dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
				}
				else
				{
					countOn();
				}
				break;
				
			case CLICK_INCORRECT:
				if(opportunityComplete)
					doItForThem();
				else
					dispatchEvent( new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
				break;
				
			case THEME_COMPLIMENT:
				playSound("Reinforcement/count-with-me", COUNTING);
				break;
				
			case THEME_CRITICISM:
				playSound("Incorrect/that-" + currentCounter.Num + "-" + currentType.ToLower(), YOU_FOUND);
				break;
				
			case COUNTING:
				countOn();
				break;
				
			case YOU_FOUND:
				if(_level == SkillLevel.Tutorial)
				{
					playSound("Instructions/keep-counting-to-turn-on-the-lights", NOW_LETS);
					break;
				}
				else
					goto case NOW_LETS;
			case NOW_LETS:
				glowOff();
				if(opportunityComplete)
				{
					fadeOutContainer();
					endOpportunity();
					break;
				}
				else
					goto case NOW_ITS;
				
			case LETS_TRY:
				resetTutorial();
				break;
			}
		}

		public override void nextOpportunity() { nextOpportunity (false); }
		public void nextOpportunity(bool henryTutorial)
		{
			opportunityComplete = false;
			timeoutCount = 0;
			failCount = 0;
			this.henryTutorial = henryTutorial;

			if (!henryTutorial) {
				if (currentOpportunity == totalOpportunities) {
					tallyAnswers ();
					return;
				}
				
				currentOpportunity++;

				if (_level == SkillLevel.Tutorial) {
					currentType = "TURBINE";
					//setCounters (8, 3, 2);
					playSound ("Instructions/now-its-your-turn", NOW_ITS);
					
					AddNewOppData( audionNames.IndexOf("Instructions/now-its-your-turn") );
				} else {
					opportunityAnswered = false;
					int inGoal = 0;
					switch (_level) {
					case SkillLevel.Emerging:
						inGoal = 0;
						break;
					case SkillLevel.Developing:
						inGoal = 5;
						break;
					case SkillLevel.Developed:
					case SkillLevel.Completed:
						inGoal = 10;
						break;
					}
					int r;
					if (resumingSkill) {
						r = 0;
						resumingSkill = false;
					} else {
						r = Mathf.FloorToInt (Random.Range (0, oppsLeft.Count));
						if (r == oppsLeft.Count)
							r--;
					}
					currentType = oppsLeft [r];
					oppsLeft.Remove (currentType);
					
					int varients = 0;
					switch (currentType) {
					case "TURBINE":
						inGoal = 0;
						varients = 8;
						break;
					case "GEAR":
						if (inGoal > 5)
							inGoal = 5;
						varients = 20;
						break;
					case "BATTERY":
						if (inGoal > 10)
							inGoal = 10;
						varients = 15;
						break;
					}
					setCounters (varients, inGoal);
					playSound ("Instructions/count-the-" + currentType.ToLower (), NOW_ITS);
					
					AddNewOppData( audionNames.IndexOf("Instructions/count-the-" + currentType.ToLower ()) );
				}

				fadeInContainer ();
				dispatchEvent (new SkillEvent (SkillEvent.OPPORTUNITY_START));
				if (totalOpportunities > 10 && currentOpportunity > 1)
					dispatchEvent (new SkillEvent (SkillEvent.BOOKMARK_NOW));
				
				foreach( SequenceCounter s in counters )
				{
					AddNewObjectData( formatObjectData( s.Num.ToString(), s.parent.localToGlobal(new Vector2( s.x, s.y )) ) );
				}
			} else {
				currentType = "TURBINE";
				setCounters (8, 3, 2);
				// Swap these two counter positions around
			}
		}
		
		private void setCounters( int varients, int inGoal ){setCounters(varients, inGoal, 5);}
		private void setCounters( int varients, int inGoal, int leftOut )
		{
			nextNum = 1;
			target.type = currentType;
			if(counters != null)
			{
				foreach(SequenceCounter c in counters)
				{
					c.Destroy();
					if(c.parent != null)
						c.parent.removeChild(c);
				}
			}
			counters = new List<SequenceCounter>();
			
			SequenceCounter temp;
			int i = 0;
			List<int> unusedVarients = new List<int>();
			if(_level == SkillLevel.Tutorial)
			{
				unusedVarients = new List<int>(){2,7,5,4,1};
				for(i = 1; i <= inGoal + leftOut; i++)
				{
					temp = new SequenceCounter(currentType, i, unusedVarients[i-1]);
					temp.scaleX = temp.scaleY = baseScale;
					counters.Add(temp);
				}
			}
			else
			{
				unusedVarients = new List<int>();
				for(i = 1; i <= varients; i++)
				{
					unusedVarients.Add(i);
				}
				int r;
				for(i = 1; i <= inGoal + leftOut; i++)
				{
					r = Mathf.FloorToInt(Random.Range(0, unusedVarients.Count));
					if(r == unusedVarients.Count) r--;
					temp = new SequenceCounter(currentType, i, unusedVarients[r]);
					temp.scaleX = temp.scaleY = baseScale;
					unusedVarients.RemoveAt(r);
					counters.Add(temp);
				}
			}
			List<SequenceCounter> fielded = new List<SequenceCounter>();
			for(i = 0; i < counters.Count; i++)
			{
				temp = counters[i];
				if(i < inGoal)
				{
					target.fillDummy(temp);
					nextNum++;
				}
				else
				{
					fielded.Add(temp);
				}
			}
			if (_level != SkillLevel.Tutorial) 
			{
				fielded.Shuffle ();
			} else {
				//fielded.Reverse();
				nextNum = 5;
			}
				
			arrangeCounters(fielded);
		}
		
		private void arrangeCounters( List<SequenceCounter> fielded )
		{
			float cWidth = 0, padding = 10, yPos = 0f, xmod = 0f;
			switch(currentType)
			{
			case "TURBINE":
				cWidth = 390;// * baseScale;
				if( _level == SkillLevel.Tutorial ){
					yPos = 1100;//1265f;
					xmod = -30f;
				}
				else
					yPos = 1100f;
				break;
			case "GEAR":
				cWidth = 220;// * baseScale;
				yPos = 1200f;
				break;
			case "BATTERY":
			default:
				cWidth = 180;// * baseScale;
				yPos = 1250f;
				break;
			}
			float totalWidth = ((cWidth + padding) * fielded.Count) - padding;
			float startX = ((2048f - totalWidth) / 2f) + xmod;
			foreach(SequenceCounter sc in fielded)
			{
				sc.scaleX = sc.scaleY = baseScale;
				sc.x = startX;
				sc.y = yPos - (cWidth / 2f);
				startX += cWidth + padding;
				container.addChild(sc);
			}
			if(_level == SkillLevel.Tutorial && !henryTutorial)
			{
				target.fillDummy(counters[3]);
				nextNum++;
			}
		}
		
		private void countersOn()
		{
			foreach(SequenceCounter sc in counters)
			{
				if(sc.Num < nextNum)
					continue;
				sc.addEventListener(MouseEvent.MOUSE_DOWN, onCounterDown);
			}
			if(_level != SkillLevel.Tutorial)
				MarkInstructionEnd();
			startTimer();
			glowOff();
		}
		
		private void countersOff()
		{
			foreach(SequenceCounter sc in counters)
			{
				sc.removeAllEventListeners(MouseEvent.MOUSE_DOWN);
			}
			MarkInstructionStart();
			stopTimer();
		}
		
		private void onCounterDown(CEvent e)
		{
			currentCounter = e.currentTarget as SequenceCounter;
			timeoutCount = 0;
			stopTimer();
			PlayClickSound();
			SoundEngine.Instance.SendCancelToken();
			countAt = 1;
			glowOff();
			stage.addEventListener(MouseEvent.MOUSE_UP, onCounterUp);
		}
		
		private void onCounterUp(CEvent e)
		{
			stage.removeEventListener(MouseEvent.MOUSE_UP, onCounterUp);

			Debug.LogError("Counter num: " + currentCounter.Num + " Next: " + nextNum);

			if(currentCounter.Num == nextNum)
			{
				failCount = 0;
				currentCounter.removeEventListener(MouseEvent.MOUSE_DOWN, onCounterDown);
				target.fillDummy(currentCounter);
				if(nextNum == counters.Count)
				{
					countersOff();
					opportunityComplete = true;
					if(!opportunityAnswered)
					{
						opportunityAnswered = true;
						opportunityCorrect = true;
					}
				}
				else
				{
					nextNum++;
				}
				PlayCorrectSound();
				
				AddNewActionData( formatActionData( "tap", counters.IndexOf(currentCounter), Vector2.zero, true ) );
			}
			else
			{
				countersOff();
				if(!opportunityAnswered)
				{
					opportunityAnswered = true;
					opportunityCorrect = false;
				}
				failCount++;
				if(failCount == 3)
				{
					opportunityComplete = true;
				}
				PlayIncorrectSound();
				
				AddNewActionData( formatActionData( "tap", counters.IndexOf(currentCounter), Vector2.zero, false ) );
			}
		}
		
		public override void onTimerComplete()
		{
			base.onTimerComplete();
			
			timeoutCount++;
			
			string path;
			if(nextNum == 1)
				path = "number-1-";
			else
				path = "next-";
			
			string clipName = "";
			if(_level == SkillLevel.Tutorial)
			{
				if(timeoutCount == 1)
				{
					clipName = playSound("Instructions/now-its-your-turn", NOW_ITS);
				}
				else
				{
					if(opportunityAnswered)
					{
						opportunityComplete = true;
						opportunityCorrect = false;
						countersOff();
						OnAudioWordComplete(NOW_LETS);
					}
					else
					{
						opportunityAnswered = true;
						opportunityCorrect = true;
						clipName = playSound("Instructions/lets-try-again", LETS_TRY);
					}
				}
			}
			else
			{
				switch(timeoutCount)
				{
				case 1:
					OnAudioWordComplete(NOW_ITS);
					clipName = "Initial Instruction/" + path + currentType.ToLower();
					break;
				case 2:
					clipName = playSound("Instructions/touch-the-green-button", PLEASE_FIND);
					break;
				case 3:
					opportunityAnswered = true;
					opportunityCorrect = false;
					glowOn(counters[nextNum - 1]);
					OnAudioWordComplete(NOW_ITS);
					clipName = "Initial Instruction/" + path + currentType.ToLower();
					break;
				case 4:
					timeoutEnd = true;
					opportunityComplete = true;
					countersOff();
					doItForThem();
					clipName = "Reinforcement/Now-count-with-me-" + counters.Count + currentType.ToLower();
					break;
				}
			}
			
			if( !string.IsNullOrEmpty( clipName ) )
			{
				baseTheme.AddAudioSessionData( "time", audionNames.IndexOf( clipName ) );
			}
		}
		
		protected override void resetTutorial()
		{
			tutorial.Destroy();
			tutorial = null;
			_currentOpportunity--;
			tutorial = new SequenceCountingTutorial();
			tutorial.baseScale = baseScale;
			tutorial.target = target;
			tutorial.container = container;
			tutorial.View.addEventListener( TutorialEvent.START, onTutorialStart );
			tutorial.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			tutorial.Resize(Screen.width, Screen.height);
			nextOpportunity (true);
			tutorial.counters = counters;
			tutorial.OnStart();
		}
		
		private void doItForThem()
		{
			glowOff();
			/*for(int i = nextNum - 1; i < counters.Count; i++)
			{
				target.fillDummy(counters[i]);
				nextNum++;
			}*/
			countAt = 1;
			playSound("Reinforcement/count-with-me", COUNTING);
		}
		
		private void countOn()
		{
			if(countAt > 1)
				glowOff(counters[countAt - 2]);
			if((countAt == nextNum - 1 && !opportunityComplete) || (countAt == counters.Count && opportunityComplete))
			{
				playSound("Numerals/" + countAt, COUNTING);
				glowOn(counters[countAt - 1]);
				if(countAt == nextNum && counters[countAt - 1].inTarget == false)
					target.fillDummy(counters[countAt - 1]);
			}
			else if((countAt == nextNum && !opportunityComplete) || (countAt > counters.Count && opportunityComplete))
			{
				string path = "";
				switch(currentType)
				{
				case "TURBINE":
					path = "turbines-are-spinning";
					break;
				case "GEAR":
					path = "gears-are-moving";
					break;
				case "BATTERY":
					path = "batteries-are-giving";
					break;
				}
				playSound("Reinforcement/" + path, YOU_FOUND);
			}
			else
			{
				playSound("Counting Numerals/" + countAt, COUNTING);
				glowOn(counters[countAt - 1]);
				if(countAt == nextNum)
				{
					target.fillDummy(counters[countAt - 1]);
					nextNum++;
				}
			}
			countAt++;
		}

		private void fadeInContainer() { fadeInContainer (0); }
		private void fadeInContainer(float delay)
		{
			container.visible = true;
			Tweener.addTween(container, Tweener.Hash("time", 0.25f, "alpha", 1, "delay", delay));
		}
		
		private void fadeOutContainer()
		{
			Tweener.addTween(container, Tweener.Hash("time", 0.25f, "alpha", 0)).OnComplete(hideAway);
		}
		private void hideAway(){ container.visible = false; }
		
		private void glowOn(FilterMovieClip fmc)
		{
			while(fmc.filters.Count > 1)
			{
				fmc.filters[1].Destroy();
				fmc.filters[1].Visible = false;
				fmc.filters.RemoveAt(1);
			}
			if(fmc.filters.Count == 0)
				fmc.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
			fmc.filters[0].Visible = true;
		}
		
		private void glowOff()
		{
			foreach(FilterMovieClip fmc in counters)
			{
				glowOff(fmc);
			}
		}
		private void glowOff(FilterMovieClip fmc)
		{
			while(fmc.filters.Count > 1)
			{
				fmc.filters[1].Destroy();
				fmc.filters[1].Visible = false;
				fmc.filters.RemoveAt(1);
			}
			if(fmc.filters.Count == 1)
				fmc.filters[0].Visible = false;
		}
		
		private string playSound ( string clipName ){ return playSound(clipName, ""); }
		private string playSound ( string clipName, string word )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();;
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEQUENCE_COUNTING, clipName );
			
			if(word != "")
				bundle.AddClip(clip, word, clip.length);
			else
				bundle.AddClip(clip);
			
			SoundEngine.Instance.PlayBundle(bundle);
			
			return clipName;
		}
		
		private void endOpportunity()
		{
			if(opportunityCorrect)
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
			else
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
			
//			if((rightPart/(float)totalOpportunities)*10 >= right + 1)
//			{
//				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
//				right++;
//			}
//			else if((wrongPart/(float)totalOpportunities)*10 >= wrong + 1)
//			{
//				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
//				wrong++;
//			}
//			else if(currentOpportunity == totalOpportunities)
//			{
//				if(opportunityCorrect)
//					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
//				else
//					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
//			}
//			else
//			{
//				if(timeoutEnd)
//				{
//					quietContinue = true;
//					dispatchEvent( new SkillEvent(SkillEvent.TIMEOUT_END, true, false) );
//				}
//				else
//					nextOpportunity();
//				timeoutEnd = false;
//			}
		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = currentType;
			
			foreach(string s in oppsLeft)
			{
				data = data + "-" + s;
			}
			
			data = data + ";" + rightPart.ToString() + "-" + right.ToString() + "-" + wrongPart.ToString() + "-" + wrong.ToString();
			
			return data;
		}
		
		private void parseSessionData (string data)
		{
			oppsLeft = new List<string>();
			
			string partData = data.Split(';')[1];
			data = data.Split(';')[0];
			
			string[] sData = data.Split('-');
			
			foreach( string s in sData )
			{
				oppsLeft.Add(s);
			}
			
			sData = partData.Split('-');
			if(sData.Length > 2)
			{
				rightPart = int.Parse(sData[0]);
				right = int.Parse(sData[1]);
				wrongPart = int.Parse(sData[2]);
				wrong = int.Parse(sData[3]);
			}
			else
			{
				rightPart = Mathf.RoundToInt(float.Parse(sData[0]) * totalOpportunities);
				right = currentSession.correct;
				wrongPart = Mathf.RoundToInt(float.Parse(sData[1]) * totalOpportunities);
				wrong = currentSession.incorrect;
			}
		}
		
		public override void Resize( int wide, int high )
		{
			this.width = wide;
			this.height = high;
			container.scaleY = container.scaleX = width/2048f;
			container.x = 0;
			container.y = (height - (1536f * container.scaleY))/2;
		}
		
		private static List<string> audionNames = new List<string>
		{	"",
			"StoryIntroduction",
			"Instructions/batteries-and-wind-turbines-give",
			"Instructions/2C-Henry-sees-1-2-3/2C-1-Henry-sees" +
			"Instructions/2C-Henry-sees-1-2-3/2C-2-one + Instructions/2C-Henry-sees-1-2-3/2C-3-two" +
			"Instructions/2C-Henry-sees-1-2-3/2C-4-three"+
			"Instructions/2C-Henry-sees-1-2-3/2C-5-turbines" +
			"Instructions/2C-Henry-sees-1-2-3/2C-6-WatchClosely",
			"Instructions/now-its-your-turn",
			"Instructions/help-us-turn-on-the-power",
			"Instructions/keep-counting-to-turn-on-the-lights",
			"Instructions/touch-the-green-button",
			"Instructions/try-again",
			"Instructions/count-the-turbine",
			"Instructions/count-the-gear",
			"Instructions/count-the-battery",
			"Initial Instruction/next-gear",
			"Initial Instruction/next-turbine",
			"Initial Instruction/next-battery",
			"Initial Instruction/number-1-gear",
			"Initial Instruction/number-1-turbine",
			"Initial Instruction/number-1-battery",
			"Reinforcement/Now-count-with-me-5-turbine",
			"Reinforcement/Now-count-with-me-10-gear",
			"Reinforcement/Now-count-with-me-15-battery",
			"Reinforcement/Now-count-with-me-5-gear",
			"Reinforcement/Now-count-with-me-5-battery",
			"Reinforcement/Now-count-with-me-10-battery",
			"Incorrect/that-2-gear",
			"Incorrect/that-2-turbine",
			"Incorrect/that-2-battery",
			"Incorrect/that-3-gear",
			"Incorrect/that-3-turbine",
			"Incorrect/that-3-battery",
			"Incorrect/that-4-gear",
			"Incorrect/that-4-turbine",
			"Incorrect/that-4-battery",
			"Incorrect/that-5-gear",
			"Incorrect/that-5-turbine",
			"Incorrect/that-5-battery",
			"Incorrect/that-6-gear",
			"Incorrect/that-6-battery",
			"Incorrect/that-7-gear",
			"Incorrect/that-7-battery",
			"Incorrect/that-8-gear",
			"Incorrect/that-8-battery",
			"Incorrect/that-9-gear",
			"Incorrect/that-9-battery",
			"Incorrect/that-10-gear",
			"Incorrect/that-10-battery",
			"Incorrect/that-11-battery",
			"Incorrect/that-12-battery",
			"Incorrect/that-13-battery",
			"Incorrect/that-14-battery",
			"Incorrect/that-15-battery",
			"Numerals/1",
			"Numerals/2",
			"Numerals/3",
			"Numerals/4",
			"Numerals/5",
			"Numerals/6",
			"Numerals/7",
			"Numerals/8",
			"Numerals/9",
			"Numerals/10",
			"Numerals/11",
			"Numerals/12",
			"Numerals/13",
			"Numerals/14",
			"Numerals/15",
			"User-Earns-Artifact"
		};
	}
}