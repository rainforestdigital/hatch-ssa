using System;
using UnityEngine;
using System.Collections;
using Callback = System.Action;

namespace SSGCore
{
	public class TimerObject : MonoBehaviour
	{
		private bool useGUI = false;
		private bool canRun = true;
		private bool isPaused = false;
		
		public void StopTimer()
		{
			canRun = false;
		}
		
		public void Unload()
		{
			canRun = false;
			useGUI = false;
			Destroy(gameObject);
		}
		
		private float pauseTime;
		public void PauseTimer()
		{
			isPaused = true;
			pauseTime = Time.time;
		}
		
		public void ResumeTimer()
		{
			runtime += Time.time - pauseTime;
			isPaused = false;
		}
	
		public void SetGUITimer(Callback _guiCB)
		{
			guiCB = _guiCB;
			useGUI = true;
		}
		
		public void SetTimeout(float delay, Callback cb)
		{
			StartCoroutine( DoTimeout(delay, cb) );
		}
		
		private float runtime;
		private IEnumerator DoTimeout(float delay, Callback cb)
		{
			runtime = Time.time;
			while( canRun )
			{
				if( !isPaused )
				{					
					if( Time.time - runtime >= delay ) break;
					yield return new WaitForSeconds(.005f);
				}
				else if( canRun && isPaused ) 
				{
					yield return new WaitForSeconds(.005f);
				}
				else if( !canRun && isPaused ) break;
			}			
			
			if( canRun ) cb();
			Destroy(gameObject);
		}
		
		private Callback guiCB;
		public void OnGUI()
		{
			if( !useGUI || !canRun ) return;
			
			if( guiCB != null ) guiCB();
		}
	}
}

