using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using pumpkin.display;
using pumpkin;
using pumpkin.events;
using pumpkin.text;

namespace SSGCore
{
	public enum COFrameLabels:int
	{
		BLANK,
		BALL,
		CAT,
		DRAGON,
		POND,
		HEDGEHOG,
		HORSESHOES,
		SKY,
		FISHBOWLS,
		SPACE, 
		HAT,
		BEAR,
		SEALBALL,
		OCTOPUS,
	}
	
	public class CountingFoundationsBGContainer : MovieClip
	{
		private static CountingFoundationsBGContainer instance;
		public static CountingFoundationsBGContainer Instance
		{
			get
			{
				return instance;
			}
	
			set
			{
				instance = value;
			}
		}
		
		public MovieClip currentContainer;
		public COFrameLabels currentCOFrameLabel;
		
		private List<CountingObject> objectList;
		
		public int objectCount
		{
			get{ return objectList.Count; }
		}
		
		public CountingFoundationsBGContainer(string swf, string id):base(swf, id)
		{
			instance = this;
			
			Init();
		}
		
		private void Init()
		{
			gotoAndStop(COFrameLabels.BLANK);
		}		
		
		private void GetAllObjects()
		{
			objectList = new List<CountingObject>();
			int counter = 0;
			for(int i = 0; i < 20; i++ )
			{
				MovieClip temp = currentContainer.getChildByName<MovieClip>("btn_" + i.ToString());
				if( temp != null ) 
				{
					DebugConsole.Log("FOUND COUNTING OBJECT IN GAME: " + ((counter % 5)+1).ToString());
					CountingObject co = new CountingObject(temp, currentLabel + ((counter % 5)+1).ToString());
					currentContainer.addChild(co);
					objectList.Add(co);
					counter++;
				}
			}
			
			ResetAllObjects();
			DebugConsole.Log("FOUND " + counter + " OBJECTS IN " + currentLabel);
		}
		
		public void ResetAllObjects()
		{
			foreach(CountingObject co in objectList)
			{
				co.visible = false;
			}
		}
		
		public void SetObjectVisible(int num, string imgLabel)
		{
			DebugConsole.Log("SET OBJECT VISIBLE: " + num.ToString() + ", picToShow: " + imgLabel.ToString() + ", currentLabel: " + currentLabel);
			objectList[num-1].GoToImage(imgLabel);
			objectList[num-1].ShowNumber((num).ToString());
			objectList[num-1].visible = true;
			objectList[num-1].HideFilter();
		}
		
		public void SetObjectInvisible(int num)
		{			
			objectList[num-1].visible = false;
		}

		public void GoToGameLabel(COFrameLabels label)
		{
			DebugConsole.Log("GOTO FRAME LABEL: " + label.ToString());
			Destroy();
			gotoAndStop(label.ToString());
			currentCOFrameLabel = label;
			currentContainer = getChildByName<MovieClip>("container");
			//currentContainer.getChildByName<MovieClip>("BG").visible = false;
			GetAllObjects();
		}
		
		public void Destroy()
		{
			if(objectList != null)
			{
				foreach(CountingObject co in objectList)
					co.Destroy();
			}
		}
	}
}