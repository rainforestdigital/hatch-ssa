using System;
using UnityEngine;

namespace SSGCore
{
	public class AbsoluteTimeoutObject
	{
		private int timeOut;
		private Action returnFunc;
		private TimerObject timer;
		
		public AbsoluteTimeoutObject (int timeToSet, Action callBack)
		{
			timeOut = (int)Time.realtimeSinceStartup + timeToSet;
			returnFunc = callBack;
			timer = TimerUtils.SetGUITimer(CheckTime);
		}
		
		public void resetTimeout(int timeToSet, Action callBack)
		{
			timeOut = (int)Time.realtimeSinceStartup + timeToSet;
			returnFunc = callBack;
		}
		
		public void CheckTime()
		{
			if(returnFunc != null && Time.realtimeSinceStartup >= timeOut)
			{
				Action ret = returnFunc;
				returnFunc = null;
				ret();
			}
		}
		
		public void Cancel()
		{
			returnFunc = null;
			timer.Unload();
		}
	}
}

