using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using HatchFramework;

using pumpkin.display;
using pumpkin;
using pumpkin.geom;
using pumpkin.events;
using pumpkin.text;
using pumpkin.tweener;

namespace SSGCore
{
	public class CountingFoundations : BaseSkill
	{
		private TimerObject subTimer;
		private TimerObject subTimer2;
		private const string ITS_OVER = "ITS_OVER";
		private CountingObject currentObject;
		private new CountingFoundationsTutorial tutorial_mc;
		
		private static CountingFoundations instance;
		public static CountingFoundations Instance
		{
			get
			{
				return instance;
			}
			
			set
			{
				instance = value;
			}
		}
		
		public const string INACTIVITY_REINFORCMENT = "Narration Clips/2A-Inactivity Reinforcement Audio/";

		private int bgX;
		private int bgY;
		private float bg_scale;

		public CountingFoundationsBGContainer bgContainer;	
		
		public CountingFoundations( SkillLevel level, SessionInfo  currentSession) : base(level, currentSession)
		{

			BACKGROUND = "Background";
			ARTIFACT = "Artifact";

			DebugConsole.Log("COUNTING FOUNDATIONS CONSTRUCTOR: " + level);
			instance = this;
			Init(null);
		}
		
		public override void Dispose ()
		{
			base.Dispose();
			if(bgContainer != null)
			{
				bgContainer.Destroy();
			}
			bgContainer = null;
			if(subTimer != null) subTimer.Unload();
			if(subTimer2 != null) subTimer2.Unload();
			removeChild(container);
			container = null;
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			instance = null;
			if(currentObject != null)
				currentObject.Destroy();
			if(buttonsList != null)
			{
				foreach(CountingObject co in buttonsList)
				{
					co.Destroy();
				}
			}
			if(tutorial_mc != null)
			{
				if(tutorial_mc.frog1_mc != null)
					tutorial_mc.frog1_mc.Destroy();
				if(tutorial_mc.frog2_mc != null)
					tutorial_mc.frog2_mc.Destroy();
				if(tutorial_mc.View.parent != null)
					tutorial_mc.View.stage.removeEventListener(MouseEvent.MOUSE_UP, tutorial_mc.HandleFrogClicked);
			}
		}
		
		public DisplayObjectContainer container;
		public override void Resize( int width, int height )
		{
			container.scaleX = container.scaleY = ((float)Screen.width)/((float)width);
			container.y = (Screen.height - (height*container.scaleY))/2;
			container.x = 0;
		}
		
		private List<COFrameLabels> gameList;
		
		//private MovieClip tutorialBG;
		//private MovieClip gameBG;
		public override void Init (MovieClip parent)
		{			
			DebugConsole.Log("COUNTING FOUNDATIONS INIT");
			base.Init (parent);
			
			container = new DisplayObjectContainer();
			container.alpha = 0;
			addChild( container );
						
			bgContainer = MovieClipFactory.GetCountingFoundationsBGContainer();
			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;

			bg_scale = 1;

			switch( PlatformUtils.GetPlatform() )
			{
			case Platforms.WIN:
				bgX = 170;
				bgY = 60;
				bg_scale = 0.94f;
				Resize(1920, 1080);
				break;
			case Platforms.IOS:
				bgX = 90;
				bgY = 130;
				bg_scale = 1;
				break;
			case Platforms.IOSRETINA:
				bgX = Mathf.RoundToInt((container.width * 0.5f) - (1675f * 0.5f)) + 60;
				bgY = 260;
				bg_scale = 1;
				break;
			case Platforms.ANDROID:
				bgX = 115;
				bgY = 40;
				bg_scale = 1.25f;
                baseTheme.Background.y -= 20f;
				Resize(1280, 800);
				break;
			}

			bgContainer.scaleX = bgContainer.scaleY = bg_scale;//0.94f : 1;
			container.addChild(bgContainer);

			if( _level ==  SkillLevel.Tutorial ) 
			{
				totalTime = 5;
				totalOpportunities = 1;
				totalAnswers = 2;
				opportunityComplete = false;
				answersCorrect = 0;
			
				bgContainer.GoToGameLabel(COFrameLabels.POND);
				bgContainer.x = bgX;
				bgContainer.y = bgY;
				
				bgContainer.ScaleCentered( bg_scale * 0.85f );
				
				bgContainer.alpha = 0;
			
				tutorial_mc = new CountingFoundationsTutorial();
				tutorial_mc.View.addEventListener( TutorialEvent.START, OnTutorialStart );
				tutorial_mc.View.addEventListener( TutorialEvent.COMPLETE, OnTutorialComplete );
				tutorial_mc.Resize( Screen.width, Screen.height );
				
				if(resumingSkill) {
					FadeInContainer().OnComplete(() => { tutorial_mc.OnStart(); } );
				}
			}
			else
			{
				totalOpportunities = 10;
				
				if(resumingSkill) {
					parseSessionData( currentSession.sessionData );
				//	FadeInContainer().OnComplete(nextOpportunity);
				}
				else{
					CreateGameList();
					container.visible = false;
				}
			}
		}
		
		public override void Start()
		{
			MarkInstructionStart();
		}
		
		public BaseHenry henry;
		public void SetHost ()
		{
			henry = CharacterFactory.GetSafariHenryCharacter();
			container.addChild(henry);
		
			henry.x = Screen.width - (henry.width + 50);
			henry.y = Screen.height - (henry.height + 75);
		}
		
		private List<CountingObject> buttonsList;
		private void CreatePickPile(COFrameLabels game, int count)
		{
			if( buttonsList != null && buttonsList.Count > 0 )
			{
				foreach(CountingObject co in buttonsList )
				{
					co.Destroy();
					container.removeChild(co);
				}
			}
			
			buttonsList = new List<CountingObject>();
			
			for(int i = 0; i < count; i++ )
			{
				int ranNum;
				do{
					ranNum = Mathf.FloorToInt(UnityEngine.Random.Range(1, 6));
					if(ranNum == 6)
						ranNum--;
				} while( ranNum == 3 && ( bgContainer.currentLabel == "SKY" || bgContainer.currentLabel == "BLOCK" ));
				CountingObject co = new CountingObject(new MovieClip("CountingFoundations.swf", "CountingObject"), bgContainer.currentLabel + ranNum.ToString());
				buttonsList.Add(co);
				co.HideNumber();
				co.id = i;
				container.addChild(co);
				//co.x = (bgContainer.currentLabel == "TRAIN" ? (container.width*.325f) : (container.width*.25f));
				//co.x -= 50;
				//co.y = ((container.height - co.height) * .90f) - 175;//bgContainer.y + 265;
				//co.ScaleCentered(1.5f);

				int counterStartX = 0;
				int counterStartY = 0;
//				int coPadding = 0;

				int xRange = 2;

				if( PlatformUtils.GetPlatform() == Platforms.WIN)
				{
					counterStartX = 304;
					counterStartY = 675;
//					coPadding = 60;
				}
				else if( PlatformUtils.GetPlatform() == Platforms.IOSRETINA)
				{
					counterStartX = 304;
					counterStartY = 930;
//					coPadding = 60;
				}
				else if( PlatformUtils.GetPlatform() == Platforms.ANDROID)
				{
					counterStartX = 125;
					counterStartY = 470;
//					coPadding = 30;
					co.scaleX = co.scaleY = 0.75f;
				}
				else
				{
					counterStartX = 100;
					counterStartY = 475;
//					coPadding = 30;
					co.scaleX = co.scaleY = 0.75f;
				}
				
				Vector2 pt;
				
				do {
					pt = new Vector2(UnityEngine.Random.Range(0, xRange), UnityEngine.Random.Range(0, xRange));//UnityEngine.Random.insideUnitCircle * 75;
				} while( Mathf.Abs( Vector2.Distance( new Vector2( xRange / 2f, xRange / 2f ), pt ) ) > xRange / 2f );
				
				co.x = counterStartX - co.Target.getChildByName( "counterRef" ).x;
				co.y = counterStartY - co.Target.getChildByName( "counterRef" ).y;
				
				co.x += (pt.x * i);//(i * coPadding);
				co.y -= (pt.y * i);
				
				//co.scaleX *=.5f;
				//co.scaleY *=.5f;
			}
		}
		
		private CountingObject GetCountingObjectForMC(CountingObject mc)
		{
			foreach( CountingObject co in buttonsList )
			{
				if( co == mc ) return co;
			}
			
			return null;
		}
		
		private void soundClick(CEvent e)
		{
			currentObject = e.currentTarget as CountingObject;
			addEventListener(MouseEvent.MOUSE_UP, HandleCOClick);
		}
		
		private void HandleCOClick(CEvent e)
		{
			removeEventListener(MouseEvent.MOUSE_UP, HandleCOClick);
			
			SoundEngine.Instance.SendCancelToken();
			//SoundEngine.Instance.StopAll();
			if(subTimer != null) subTimer.StopTimer();
			stopTimer();
			
			//if( tutorial_mc != null && !(tutorial_mc as CountingFoundationsTutorial).interactivePortionComplete ) return;
			
			CountingObject co = currentObject;
			if( co == null )
			{
				DebugConsole.LogError("NO COUNTING OBJECT RETURNED FOR CLICKED MOVIECLIP IN COUNTING FOUNDATIONS");
				return;
			}			
			
			
			SatisfyAnswer();
			
			DebugConsole.Log ("OBJECT CLICKED: " + (answersCorrect).ToString() + ", imgLabel: " + co.imgLabel);
			CountingFoundations.Instance.bgContainer.SetObjectVisible(answersCorrect, co.imgLabel);
			
			AddNewActionData( formatActionData( "tap", co.id, Vector2.zero) );
			
			container.removeChild(co);
			co.removeEventListener(MouseEvent.MOUSE_DOWN, soundClick);
			co.Destroy();
			buttonsList.Remove(co);
			
			PlayClickSound();
		}
		
		private void OnTutorialStart( CEvent e )
		{
			container.visible = true;
			Tweener.addTween( container, Tweener.Hash("time",1, "alpha",1, "transition",Tweener.TransitionType.easeInOutQuint) );
			currentOpportunity = 1;
			onTutorialStart(e);
		}
		
		private TweenerObj FadeInContainer()
		{
			container.visible = true;
			return Tweener.addTween( container, Tweener.Hash("time",0.35f, "alpha",1, "transition",Tweener.TransitionType.easeInOutQuint) );
		}
		
		private void OnTutorialComplete( CEvent e )
		{
			tutorial_mc.View.removeEventListener( TutorialEvent.START, OnTutorialStart );
			tutorial_mc.View.removeEventListener( TutorialEvent.COMPLETE, OnTutorialComplete );
			
			Tweener.addTween( (tutorial_mc as CountingFoundationsTutorial).tutorial_mc, Tweener.Hash("time",1f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuint) ).OnComplete(HandleTutorialFadeOutComplete);
			
			if(opportunityCorrect)
			{
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
			}
			else
			{
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
			}
		}
		
		private void HandleTutorialFadeOutComplete()
		{
			//container.removeChild( tutorialBG );
			tutorial_mc.View.stage.removeEventListener(MouseEvent.MOUSE_UP, tutorial_mc.HandleFrogClicked);
			container.removeChild( tutorial_mc.View );
		}
		
		public void SatisfyAnswer()
		{
			SatisfyAnswer(true);
		}
		
		public void SatisfyAnswer(bool playSound)
		{
			NextAnswer();
			stopTimer();
			if( _level != SkillLevel.Tutorial ) timeoutCount = 0;
			if(playSound)
			{
				if(opportunityComplete || _level == SkillLevel.Tutorial)
					PlaySound("Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-" + (answersCorrect).ToString(), "Ding_Now");
				else
					PlaySound("Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-" + (answersCorrect).ToString());
			}
		}

		public void stopAllTimers() {
			if(subTimer != null) subTimer.StopTimer();
			stopTimer();
		}
		
		public int AnswersCorrect
		{
			get
			{
				return answersCorrect;
			}
		}
		int totalAnswers = 0;
		int answersCorrect = 0;
		public void NextAnswer()
		{		
			if(currentOpportunity > 0) 
			{
				answersCorrect++;
			}
			
			if( answersCorrect == totalAnswers) // game is over
			{
				if(opportunityCorrect)
				{
					MarkInstructionStart();
					opportunityComplete = true;
					if( _level != SkillLevel.Tutorial ) 
					{
						// Play hurray sounds
						if(subTimer != null) subTimer.StopTimer();
						stopTimer();
					}
					else
					{
						if(subTimer != null) subTimer.StopTimer();
						stopTimer();
					}
				}
			}
			else if( _level != SkillLevel.Tutorial && !opportunityComplete)
			{
				if(subTimer != null) subTimer.StopTimer();
				subTimer = TimerUtils.SetTimeout(.5f, StartTimerAfterSound);
			}
		}
		
		private void PlayCompSound()
		{
			PlayCompSound("");
		}
		
		private void PlayCompSound(string msg)
		{
			dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
			/*
			henry.PlayAnimation(BaseCharacter.HAPPY, false);
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			string clipName = COMP[UnityEngine.Random.Range(0,COMP.Length)];
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SAFARI_THEME, "TH-SF-Henry/Henry-Complements/"+ clipName );
			
			if( clip != null )
			{
				bundle.AddClip(clip);
				SoundEngine.Instance.PlayBundle(bundle);
				TimerUtils.Instance.SetTimeout(clip.length, HandlePauseForPlayConclusion); 
			}
			else
			{
				DebugConsole.LogError("AUDIO CLIP FOR PLAYCOMPSOUND RETURNED NULL: " + clipName);
				TimerUtils.Instance.SetTimeout(2f, HandlePauseForPlayConclusion); 
			}*/
		}
		
		public override void nextOpportunity()
		{
			MarkInstructionStart();
			
			if(currentOpportunity > 0) 
			{
				//NumCorrect++;
			}
			if( currentOpportunity == totalOpportunities && currentOpportunity > 0 ) // game is over
			{
				tallyAnswers();
			}
			else
			{
				currentOpportunity++;
				SetNextOpportunity();
			}	
		}
		
		private void HandlePauseForPlayConclusion()
		{
			Debug.Log("HandlePauseForPlayConclusion 1");
			HandlePauseForPlayConclusion("");
		}
		
		private void HandlePauseForPlayConclusion(string msg)
		{
			Debug.Log("HandlePauseForPlayConclusion 2");
			PlayConclusion();
			if(subTimer != null) subTimer.StopTimer();
			subTimer = TimerUtils.SetTimeout(4f, HandlePauseAfterOpportunityCompleted);
			Tweener.addTween( container, Tweener.Hash("time", 1f, "alpha",0, "delay", 2.5f, "transition",Tweener.TransitionType.easeInOutQuint) );
			
		}
		
		private void HandlePauseAfterOpportunityCompleted()
		{
			HandlePauseAfterOpportunityCompleted("");
		}
		
		private void HandlePauseAfterOpportunityCompleted(string msg)
		{
			if(subTimer != null) subTimer.StopTimer();
			stopTimer();
			opportunityComplete = true;
			if(opportunityCorrect)
			{
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
			}
			else
			{
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
			}
		}
		
		public float PlaySound(string snd)
		{
			return PlaySound (snd, "");
		}		
		
		public float PlaySound(string snd, string evnt)
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.COUNTING_FOUNDATIONS, snd );
			if( evnt.Length == 0 ) bundle.AddClip(clip);
			else bundle.AddClip(clip, evnt, clip.length, false);
			SoundEngine.Instance.PlayBundle(bundle);
			
			return clip.length;
		}
		
		private void SetNextOpportunity( )
		{
			opportunityComplete = false;
			opportunityCorrect = true;
			timeoutCount = 0;
			
//			Rectangle bound;
//			Rect offset;
			int xOffset = bgX;
			int yOffset = bgY;
			
			bgContainer.scaleX = bgContainer.scaleY = bg_scale;
			
			switch (_level)
			{
				case SkillLevel.Emerging:
					totalTime = 5;
					totalOpportunities = 10;
					answersCorrect = 0;
					
					bgContainer.GoToGameLabel(gameList[currentOpportunity]);

					//For Testing scale of BG object
					//bgContainer.GoToGameLabel(COFrameLabels.OCTOPUS);
//					bound = bgContainer.getBounds(container);
//					offset = new Rect(bgContainer.x - bound.x, bgContainer.y - bound.y - 115, 0,0);
					bgContainer.x = xOffset;//(container.width*.5f) - (bound.width*.5f) + offset.x;
					bgContainer.y = yOffset;//(container.height*.5f) - (bound.height*.6f) + offset.y;
					
					totalAnswers = 5;//bgContainer.objectCount;
					CreatePickPile(gameList[currentOpportunity], totalAnswers);
				break;
				
				case SkillLevel.Developing:
					totalTime = 5;
					totalOpportunities = 10;
					answersCorrect = 0;
				
					bgContainer.GoToGameLabel(gameList[currentOpportunity]);
//					bound = bgContainer.getBounds(container);
					bgContainer.x = xOffset;//container.width*.5f - bound.width*.5f;
					bgContainer.y = yOffset;//container.height*.5f - bound.height*.6f - 70;
					
					totalAnswers = 15;//bgContainer.objectCount;
					CreatePickPile(gameList[currentOpportunity], totalAnswers);
				break;
				
				case SkillLevel.Completed:
				case SkillLevel.Developed:
					totalTime = 5;
					totalOpportunities = 10;
					answersCorrect = 0;
				
					bgContainer.GoToGameLabel(gameList[currentOpportunity]);
//					bound = bgContainer.getBounds(container);
					bgContainer.x = xOffset;//container.width*.5f - bound.width*.5f;
					bgContainer.y = yOffset;//container.height*.5f - bound.height*.6f - 70;
					
					totalAnswers = 20;//bgContainer.objectCount;
					CreatePickPile(gameList[currentOpportunity], totalAnswers);
				break;
			}
			
			float bgYStart = bgContainer.y;
			bgContainer.ScaleCentered(bg_scale*0.8f);
			
			bgContainer.y += bgContainer.y - bgYStart;

			if( container.alpha == 0 ) 
			{
				container.visible = true;
				Tweener.addTween( container, Tweener.Hash("time",0.35f, "alpha",1, "transition",Tweener.TransitionType.easeInOutQuint) );
			}
			
			dispatchEvent(new SkillEvent(SkillEvent.ANIMATION_REQUEST, true, false, BaseCharacter.RESTING));
			dispatchEvent(new SkillEvent(SkillEvent.OPPORTUNITY_START));
			//henry.PlayAnimation(BaseCharacter.RESTING, false);
			string clipName = PlaySuggestion();
			
			AddNewOppData( audioNames.IndexOf( clipName ) );
			
			foreach( CountingObject co in buttonsList )
			{
				Vector2 point = co.parent.localToGlobal( new Vector2( co.x, co.y ) );
				AddNewObjectData( formatObjectData( co.imgLabel, point ) );
			}
		}
		
		public static void Shuffle<COFrameLabels>(List<COFrameLabels> list) 
		{
	        int n = list.Count;
	        System.Random rnd = new System.Random();
	        while (n > 1) 
			{
	            int k = (rnd.Next(0, n) % n);
	            n--;
	            COFrameLabels value = list[k];
	            list[k] = list[n];
	            list[n] = value;
	        }
	    }
		
		private void PlaySoundAndStartTimer(string snd)
		{
			PlaySound(snd);
			if(subTimer != null) subTimer.StopTimer();
			subTimer = TimerUtils.SetTimeout(1.5f, StartTimerAfterSound);
		}
		
		private void StartTimerAfterSound()
		{
			StartTimerAfterSound("");	
		}
		
		private void StartTimerAfterSound(string msg)
		{
			startTimer();
			
			if(_level != SkillLevel.Tutorial)
			{
				MarkInstructionEnd();
				
				foreach(CountingObject co in buttonsList)
				{
					if(!co.hasEventListener(MouseEvent.MOUSE_DOWN))
						co.addEventListener(MouseEvent.MOUSE_DOWN, soundClick);
				}
			}
		}
		
		private void countersOff()
		{
			foreach(CountingObject co in buttonsList)
				{
					if(co.hasEventListener(MouseEvent.MOUSE_DOWN))
						co.removeEventListener(MouseEvent.MOUSE_DOWN, soundClick);
				}
		}
		
		private void RestartTutorialAfterPause()
		{
			RestartTutorialAfterPause("");
		}
		
		private void RestartTutorialAfterPause(string msg)
		{
			(tutorial_mc as CountingFoundationsTutorial).Restart();
		}
		
		public override void onTimerComplete()
		{			
			base.onTimerComplete();
			
			stopTimer();
			
			timeoutCount++;
			
			string clipName = "";
			
			switch(timeoutCount)
			{
				case 1:
					// Please touch a wheel in the pile
					if(_level == SkillLevel.Tutorial)
					{
						PlaySoundAndStartTimer("Narration Clips/2A-Skill Instruction Audio/2A-now-its-your-turn");
						clipName = "Narration Clips/2A-Skill Instruction Audio/2A-now-its-your-turn";
					}
					else
					{
						clipName = PlaySuggestion();
					}
				break;
				
				case 2:
					// Please touch the help button to see Henry count
					if(_level == SkillLevel.Tutorial)
					{
						tutorial_mc.TimeoutDeactivate();
						if(opportunityAnswered)
						{
							opportunityCorrect = false;
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
							return;
						}
						opportunityAnswered = true;
						opportunityCorrect = true;
						tutorial_mc.disableFrog2();
						PlaySound("Narration Clips/2A-Skill Instruction Audio/2A-try-again");
						answersCorrect = 0;
						if(subTimer != null) subTimer.StopTimer();
						subTimer = TimerUtils.SetTimeout(1f, RestartTutorialAfterPause);
						timeoutCount = 0;
					}
					else
					{
						PlaySoundAndStartTimer("Narration Clips/2A-Skill Instruction Audio/2A-touch-the-green-button");
						clipName = "Narration Clips/2A-Skill Instruction Audio/2A-touch-the-green-button";
					}
				break;
				
				case 3:
					if( _level == SkillLevel.Tutorial )
					{
						opportunityCorrect = false;
						OnTutorialComplete(new CEvent(CEvent.COMPLETE));
						timeoutCount = 0;
					}
					else
					{
						clipName = PlaySuggestion();
					}
				break;
				
				case 4:
					timeoutEnd = true;
					// move remaining items to goal, go to next opportunity
					opportunityCorrect = false;
					opportunityComplete = true;
					MarkInstructionStart();
					countersOff();
					doItForThem();
					
					clipName = GetTimeOver();
				break;
			}
			
			if( !string.IsNullOrEmpty( clipName ) )
			{
				baseTheme.AddAudioSessionData("time", audioNames.IndexOf( clipName ));
			}
		}
		
		private void hitTheGlow()
		{
			foreach(CountingObject co in buttonsList)
			{
				co.ShowFilter();
			}
			subTimer2 = TimerUtils.SetTimeout(1.5f, killTheGlow);
		}
		
		private void killTheGlow()
		{
			foreach(CountingObject co in buttonsList)
			{
				co.HideFilter();
			}
		}
		
		private void doItForThem()
		{
			if(subTimer != null)
				subTimer.StopTimer();
			
			if(buttonsList.Count == 0)
				PlayTimeOver();
			else
			{
				answersCorrect++;
				
				CountingObject co = buttonsList[0];
				CountingFoundations.Instance.bgContainer.SetObjectVisible(answersCorrect, co.imgLabel);
				
				co.Destroy();
				container.removeChild(co);
				buttonsList.Remove(co);
				
				float clipTime = PlaySound("Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-" + (answersCorrect).ToString());
				
				if( clipTime < 0.25f )
					clipTime = 1.1f;
				
				subTimer = TimerUtils.SetTimeout( clipTime, doItForThem );
			}
		}
		
		public override void OnAudioWordComplete( string word )
		{
			DebugConsole.Log("ON AUDIO WORD COMPLETE: " + word + ", opportunityComplete: " + opportunityComplete);
			AudioClip clip = null;
			SoundEngine.SoundBundle bundle = null;
			
			if(opportunityComplete)
			{
			
				switch( word )
				{
										
					case YOU_FOUND:
					case THEME_COMPLIMENT:
						DebugConsole.Log("THEME COMPLIMENT COMPLETED");
						if( _level == SkillLevel.Tutorial )
							(tutorial_mc as CountingFoundationsTutorial).Handle2ClickFinishDelay();
						else
							HandlePauseForPlayConclusion();
					break;
					
					case INTRO:
					case PLEASE_FIND:
					case NOW_ITS:
					case NOW_LETS:
					case YOU_FOUND_INCORRECT:
					case THIS_IS :
						DebugConsole.Log("OPPORTUNITY COMPLETE WORD: " + word );
						if(opportunityCorrect)
						{
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
						}
						else
						{
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
						}
					break;
					case THEME_INTRO:
						/*bundle = new SoundEngine.SoundBundle();
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LETTER_RECOGNITION, "4A-platty-wants-to-play-letter-game" );
						
						bundle.AddClip( clip, INTRO, clip.length );
						SoundEngine.Instance.PlayBundle( bundle );*/
					break;
					case ITS_OVER:
						Tweener.addTween(container, Tweener.Hash("time", 1f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuint)).OnComplete(HandlePauseAfterOpportunityCompleted);
					break;
					case "Ding_Now":
						PlayCorrectSound();
						MarkInstructionStart();
					break;
					case CLICK_CORRECT:
						PlayCompSound();
					break;
				}
				
			}
			else
			{
				
				switch( word )
				{
					case YOU_FOUND:
					case YOU_FOUND_INCORRECT:
					case THEME_CRITICISM:
						//PLEASE FIND current number - audio
						/*clip = SoundUtils.LoadGlobalSound( MovieClipFactory.COUNTING_FOUNDATIONS, "4A-Find-Letter-" + currentLetter.ToUpper());
						bundle = new SoundEngine.SoundBundle();
						bundle.AddClip( clip, PLEASE_FIND, clip.length );
						SoundEngine.Instance.PlayBundle( bundle );*/
					break;
					
					case INTRO:
						Tweener.addTween( container, Tweener.Hash("time",0.35f, "alpha",1, "transition",Tweener.TransitionType.easeInOutQuint) );
						if(_level == SkillLevel.Tutorial)
						{
							tutorial_mc.OnStart();
							addChild(tutorial_mc.View);
							opportunityCorrect = true;
							Tweener.addTween(bgContainer, Tweener.Hash("time",0.5f, "alpha",1, "transition",Tweener.TransitionType.easeInOutQuad) );
						}
						else
						{
							nextOpportunity();
						}
					break;
					case PLEASE_FIND:
					case NOW_ITS:
					case NOW_LETS:
					case THIS_IS :
						
					break;
					
					case THEME_INTRO:
						if( !currentSession.skipIntro )
						{
							bundle = new SoundEngine.SoundBundle();
							clip = SoundUtils.LoadGlobalSound( MovieClipFactory.COUNTING_FOUNDATIONS, "Narration Clips/2A-Story Introduction Audio/2A-1-StoryIntroduction" );
								
							bundle.AddClip( clip, INTRO, clip.length );
							SoundEngine.Instance.PlayBundle( bundle );
						}
						else
							OnAudioWordComplete( INTRO );
					
						container.visible = true;
					break;
					case ITS_OVER:
						Tweener.addTween(container, Tweener.Hash("time", 1f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuint)).OnComplete(HandlePauseAfterOpportunityCompleted);
					break;
					case THEME_COMPLIMENT:
						DebugConsole.Log("THEME COMPLIMENT COMPLETED");
						if( _level == SkillLevel.Tutorial )
							(tutorial_mc as CountingFoundationsTutorial).Handle2ClickFinishDelay();
						else
							HandlePauseForPlayConclusion();
					break;
					case "Ding_Now":
						PlayCorrectSound();
					break;
				}
				
			}
			
		}
		
		private void PlayConclusion()
		{
			Debug.Log("PlayConclusion");
			string subFolder = "";
			switch (_level)
			{
				case SkillLevel.Emerging:
					subFolder = "2A-EG/2A-EG-" + totalAnswers.ToString();
				break;
				
				case SkillLevel.Developing:
					subFolder = "2A-DG/2A-DG-" + totalAnswers.ToString();
				break;
				
				case SkillLevel.Developed:
				case SkillLevel.Completed:
					subFolder = "2A-DDCD/2A-DDCD-" + totalAnswers.ToString();
				break;
				
			}
			
			switch(bgContainer.currentCOFrameLabel)
			{
				case COFrameLabels.BALL:
					if(_level != SkillLevel.Emerging)
						PlaySound("Narration Clips/2A-Reinforcement Audio/" + subFolder + "-balls" );
					else
						PlaySound("Narration Clips/2A-Reinforcement Audio/" + subFolder + "-balls-to-nets" );
				break;
				
				case COFrameLabels.CAT:
					PlaySound("Narration Clips/2A-Reinforcement Audio/" + subFolder + "-cats" );
				break;
				
				case COFrameLabels.DRAGON:
					PlaySound("Narration Clips/2A-Reinforcement Audio/" + subFolder + "-dragons" );
				break;
				
				case COFrameLabels.POND:
					PlaySound("Narration Clips/2A-Reinforcement Audio/" + subFolder + "-frogs" );
				break;
				
				case COFrameLabels.HEDGEHOG:
					PlaySound("Narration Clips/2A-Reinforcement Audio/" + subFolder + "-hedgehogs" );
				break;
				
				case COFrameLabels.HORSESHOES:
					PlaySound("Narration Clips/2A-Reinforcement Audio/" + subFolder + "-horseshoes" );
				break;
				
				case COFrameLabels.SKY:
					PlaySound("Narration Clips/2A-Reinforcement Audio/" + subFolder + "-paper-airplanes" );
				break;
				
				case COFrameLabels.FISHBOWLS:
					PlaySound("Narration Clips/2A-Reinforcement Audio/" + subFolder + "-rubber-ducks" );
				break;
				
				case COFrameLabels.SPACE:
					PlaySound("Narration Clips/2A-Reinforcement Audio/" + subFolder + "-space-creatures" );
				break;
				
				case COFrameLabels.BEAR:
					PlaySound("Narration Clips/2A-Reinforcement Audio/" + subFolder + "-teddy-bears" );
				break;

				case COFrameLabels.HAT:
					PlaySound("Narration Clips/2A-Reinforcement Audio/" + subFolder + "-silly-hats" );
				break;

				case COFrameLabels.SEALBALL:
					PlaySound("Narration Clips/2A-Reinforcement Audio/" + subFolder + "-balls-to-seals" );
				break;

				case COFrameLabels.OCTOPUS:
					PlaySound("Narration Clips/2A-Reinforcement Audio/" + subFolder + "-octopi" );
				break;
				
			}
			
		}
		
		private string PlaySuggestion()
		{
			if(subTimer != null) subTimer.StopTimer();
			subTimer = TimerUtils.SetTimeout(2.5f, StartTimerAfterSound);
			
			if(timeoutCount != 1)
				hitTheGlow();
			
			switch(bgContainer.currentCOFrameLabel)
			{
				default:
				case COFrameLabels.BALL:
					PlaySound("Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-ball" );
				return "Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-ball";
				
				case COFrameLabels.CAT:
					PlaySound("Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-cat" );
				return "Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-cat";
				
				case COFrameLabels.DRAGON:
					PlaySound("Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-dragon" );
				return "Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-dragon";
				
				case COFrameLabels.POND:
					PlaySound("Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-frog" );
				return "Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-frog";
				
				case COFrameLabels.HEDGEHOG:
					PlaySound("Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-hedgehog" );
				return "Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-hedgehog";
				
				case COFrameLabels.HORSESHOES:
					PlaySound("Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-horseshoe" );
				return "Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-horseshoe";

				case COFrameLabels.OCTOPUS:
					PlaySound("Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-octopus" );
				return "Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-octopus";
				
				case COFrameLabels.SKY:
					PlaySound("Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-paper-airplane" );
				return "Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-paper-airplane";
				
				case COFrameLabels.FISHBOWLS:
					PlaySound("Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-rubber-duck" );
				return "Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-rubber-duck";
				
				case COFrameLabels.SPACE:
					PlaySound("Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-space-creature" );
				return "Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-space-creature";
				
				case COFrameLabels.BEAR:
					PlaySound("Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-teddy-bear" );
				return "Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-teddy-bear";

				case COFrameLabels.HAT:
					PlaySound("Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-silly-hat" );
				return "Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-silly-hat";

				case COFrameLabels.SEALBALL:
					PlaySound("Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-ball" );
				return "Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-ball";
			}
			
		}
		
		private string GetTimeOver()
		{
			string subFolder = "";
			switch (_level)
			{
				case SkillLevel.Emerging:
					subFolder = "2A-EG/2A-EG-5";
				break;
				
				case SkillLevel.Developing:
					subFolder = "2A-DG/2A-DG-15";
				break;
				
				case SkillLevel.Developed:
				case SkillLevel.Completed:
					subFolder = "2A-DDCD/2A-DDCD-20";
				break;
				
			}
			
			string clipName = "";
			
			switch(bgContainer.currentCOFrameLabel)
			{
				case COFrameLabels.BALL:
					if(_level == SkillLevel.Emerging)
						clipName = INACTIVITY_REINFORCMENT + subFolder + "-balls-nets";
					else
						clipName = INACTIVITY_REINFORCMENT + subFolder + "-balls";
				break;
				
				case COFrameLabels.CAT:
					clipName = INACTIVITY_REINFORCMENT + subFolder + "-cats";
				break;
				
				case COFrameLabels.DRAGON:
					clipName = INACTIVITY_REINFORCMENT + subFolder + "-dragons";
				break;
				
				case COFrameLabels.POND:
					clipName = INACTIVITY_REINFORCMENT + subFolder + "-frogs";
				break;
				
				case COFrameLabels.HEDGEHOG:
					clipName = INACTIVITY_REINFORCMENT + subFolder + "-hedgehogs";
				break;
				
				case COFrameLabels.HORSESHOES:
					clipName = INACTIVITY_REINFORCMENT + subFolder + "-horseshoes";
				break;
				
				case COFrameLabels.OCTOPUS:
					clipName = INACTIVITY_REINFORCMENT + subFolder + "-octopi";
				break;
				
				case COFrameLabels.SKY:
					clipName = INACTIVITY_REINFORCMENT + subFolder + "-airplanes";
				break;
				
				case COFrameLabels.FISHBOWLS:
					clipName = INACTIVITY_REINFORCMENT + subFolder + "-ducks";
				break;
				
				case COFrameLabels.HAT:
					clipName = INACTIVITY_REINFORCMENT + subFolder + "-dogs";
				break;
				
				case COFrameLabels.SPACE:
					clipName = INACTIVITY_REINFORCMENT + subFolder + "-creatures";
				break;
				
				case COFrameLabels.BEAR:
					clipName = INACTIVITY_REINFORCMENT + subFolder + "-bears";
				break;

				case COFrameLabels.SEALBALL:
					if(_level == SkillLevel.Emerging)
						clipName = INACTIVITY_REINFORCMENT + subFolder + "-balls-seal";
					else
						clipName = INACTIVITY_REINFORCMENT + subFolder + "-balls";
				break;
				
			}
			
			return clipName;
			
		}
		
		private void PlayTimeOver(){
			PlaySound( GetTimeOver(), ITS_OVER );	
		}
		
		private void CreateGameList()
		{
			
			List<COFrameLabels> tempList = new List<COFrameLabels>();	
					
			List<COFrameLabels> temptempList = new List<COFrameLabels>(){ 	COFrameLabels.BALL,
																			COFrameLabels.CAT,
																			COFrameLabels.DRAGON,
																			COFrameLabels.POND,
																			COFrameLabels.HEDGEHOG,
																			COFrameLabels.HORSESHOES,
																			COFrameLabels.SKY,
																			COFrameLabels.FISHBOWLS,
																			COFrameLabels.SPACE,
																			COFrameLabels.BEAR };
			switch (_level)
			{
				case SkillLevel.Emerging:
					
					tempList.Add(COFrameLabels.OCTOPUS);
					tempList.Add(COFrameLabels.SEALBALL);
					tempList.Add(COFrameLabels.HAT);
				
					tempList.AddRange( temptempList.RandomSample(7) );
					
				break;
				
				case SkillLevel.Developing:
				case SkillLevel.Developed:
				case SkillLevel.Completed:
					tempList = temptempList;
				break;
			}
			tempList.Shuffle();
			
			gameList = new List<COFrameLabels>();
			gameList.Add(COFrameLabels.BLANK);
			for(int i = 0; i < tempList.Count; i++)
			{
				gameList.Add(tempList[i]);
			}
		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = ((int)gameList[0]).ToString();
			
			for(int i = 1; i< gameList.Count; i++)
			{
				data = data + "-" + (int)gameList[i];
			}
			return data;
		}
		
		private void parseSessionData (string data)
		{
			gameList = new List<COFrameLabels>();
			
			string[] sData = data.Split('-');
			
			foreach( string s in sData )
			{
				gameList.Add((COFrameLabels)int.Parse(s));
			}
		}
		
		public static List<string> audioNames = new List<string>()
		{	"",
			"Narration Clips/2A-Story Introduction Audio/2A-StoryIntroduction",
			"Narration Clips/2A-Skill Instruction Audio/2A-Platty-will-try-first",
			"Narration Clips/2A-Skill Instruction Audio/2A-touch-the-green-button",
			"Narration Clips/2A-Skill Instruction Audio/2A-now-its-your-turn",
			"Narration Clips/2A-Skill Instruction Audio/2A-lets-play-tossncount",
			"Narration Clips/2A-Skill Instruction Audio/2A-try-again",
			"Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-silly-hat",
			"Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-ball",
			"Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-octopus",
			"Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-dragon",
			"Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-frog",
			"Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-teddy-bear",
			"Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-paper-airplane",
			"Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-space-creature",
			"Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-hedgehog",
			"Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-horseshoe",
			"Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-ball",
			"Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-cat",
			"Narration Clips/2A-Skill Instruction Audio/2A-Initial-Instructions/2A-rubber-duck",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-1",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-2",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-3",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-4",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-5",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-6",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-7",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-8",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-9",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-10",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-11",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-12",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-13",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-14",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-15",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-16",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-17",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-18",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-19",
			"Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-20",
			"Narration Clips/2A-Reinforcement Audio/2A-EG/2A-EG-5-silly-hats",
			"Narration Clips/2A-Reinforcement Audio/2A-EG/2A-EG-5-balls-to-seals",
			"Narration Clips/2A-Reinforcement Audio/2A-EG/2A-EG-5-octopi",
			"Narration Clips/2A-Reinforcement Audio/2A-EG/2A-EG-5-dragons",
			"Narration Clips/2A-Reinforcement Audio/2A-EG/2A-EG-5-frogs",
			"Narration Clips/2A-Reinforcement Audio/2A-EG/2A-EG-5-teddy-bears",
			"Narration Clips/2A-Reinforcement Audio/2A-EG/2A-EG-5-paper-airplanes",
			"Narration Clips/2A-Reinforcement Audio/2A-EG/2A-EG-5-space-creatures",
			"Narration Clips/2A-Reinforcement Audio/2A-EG/2A-EG-5-hedgehogs",
			"Narration Clips/2A-Reinforcement Audio/2A-EG/2A-EG-5-horseshoes",
			"Narration Clips/2A-Reinforcement Audio/2A-EG/2A-EG-5-balls-to-nets",
			"Narration Clips/2A-Reinforcement Audio/2A-EG/2A-EG-5-cats",
			"Narration Clips/2A-Reinforcement Audio/2A-EG/2A-EG-5-rubber-ducks",
			"Narration Clips/2A-Reinforcement Audio/2A-DG/2A-DG-15-dragons",
			"Narration Clips/2A-Reinforcement Audio/2A-DG/2A-DG-15-frogs",
			"Narration Clips/2A-Reinforcement Audio/2A-DG/2A-DG-15-teddy-bears",
			"Narration Clips/2A-Reinforcement Audio/2A-DG/2A-DG-15-paper-airplanes",
			"Narration Clips/2A-Reinforcement Audio/2A-DG/2A-DG-15-space-creatures",
			"Narration Clips/2A-Reinforcement Audio/2A-DG/2A-DG-15-hedgehogs",
			"Narration Clips/2A-Reinforcement Audio/2A-DG/2A-DG-15-horseshoes",
			"Narration Clips/2A-Reinforcement Audio/2A-DG/2A-DG-15-balls",
			"Narration Clips/2A-Reinforcement Audio/2A-DG/2A-DG-15-cats",
			"Narration Clips/2A-Reinforcement Audio/2A-DG/2A-DG-15-rubber-ducks",
			"Narration Clips/2A-Reinforcement Audio/2A-DDCD/2A-DDCD-20-dragons",
			"Narration Clips/2A-Reinforcement Audio/2A-DDCD/2A-DDCD-20-frogs",
			"Narration Clips/2A-Reinforcement Audio/2A-DDCD/2A-DDCD-20-teddy-bears",
			"Narration Clips/2A-Reinforcement Audio/2A-DDCD/2A-DDCD-20-paper-airplanes",
			"Narration Clips/2A-Reinforcement Audio/2A-DDCD/2A-DDCD-20-space-creatures",
			"Narration Clips/2A-Reinforcement Audio/2A-DDCD/2A-DDCD-20-hedgehogs",
			"Narration Clips/2A-Reinforcement Audio/2A-DDCD/2A-DDCD-20-horseshoes",
			"Narration Clips/2A-Reinforcement Audio/2A-DDCD/2A-DDCD-20-balls",
			"Narration Clips/2A-Reinforcement Audio/2A-DDCD/2A-DDCD-20-cats",
			"Narration Clips/2A-Reinforcement Audio/2A-DDCD/2A-DDCD-20-rubber-ducks",
			"Narration Clips/2A-Reinforcement Audio/2A-TT/2A-TT-2-frogs",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-EG/2A-EG-5-dogs",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-EG/2A-EG-5-balls-seal",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-EG/2A-EG-5-octopi",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-EG/2A-EG-5-dragons",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-EG/2A-EG-5-frogs",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-EG/2A-EG-5-bears",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-EG/2A-EG-5-airplanes",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-EG/2A-EG-5-creatures",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-EG/2A-EG-5-hedgehogs",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-EG/2A-EG-5-horseshoes",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-EG/2A-EG-5-balls-nets",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-EG/2A-EG-5-cats",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-EG/2A-EG-5-ducks",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DG/2A-DG-15-dragons",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DG/2A-DG-15-frogs",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DG/2A-DG-15-bears",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DG/2A-DG-15-airplanes",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DG/2A-DG-15-creatures",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DG/2A-DG-15-hedgehogs",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DG/2A-DG-15-horseshoes",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DG/2A-DG-15-balls",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DG/2A-DG-15-cats",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DG/2A-DG-15-ducks",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DDCD/2A-DDCD-20-dragons",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DDCD/2A-DDCD-20-frogs",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DDCD/2A-DDCD-20-bears",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DDCD/2A-DDCD-20-airplanes",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DDCD/2A-DDCD-20-creatures",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DDCD/2A-DDCD-20-hedgehogs",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DDCD/2A-DDCD-20-horseshoes",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DDCD/2A-DDCD-20-balls",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DDCD/2A-DDCD-20-cats",
			"Narration Clips/2A-Inactivity Reinforcement Audio/2A-DDCD/2A-DDCD-20-rubberducks",
			"User-Earns-Artifact"
		};
	}
}

