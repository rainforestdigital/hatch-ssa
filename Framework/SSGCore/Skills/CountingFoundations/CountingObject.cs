using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;
using pumpkin.text;
using pumpkin.geom;

/// <summary>
/// Counting object. Used for all game pieces in CountingFoundations games
/// </summary>
namespace SSGCore
{
	public class CountingObject : FilterMovieClip
	{
		public MovieClip number;
		public int id = -1;
		public string imgLabel;
		
		public CountingObject(MovieClip _target, string labelToGoTo) : base(_target)
		{	
			number = _target.getChildByName<MovieClip>("number_mc");
			
			GoToImage(labelToGoTo);
			imgLabel = labelToGoTo;
			
			HideFilter();
			
			//DebugConsole.Log("COUNTING OBJECT CREATED: " + labelToGoTo + ", VISIBLE? " + Target.visible +", target label: " + Target.currentLabel);
		}
		
		private void SetFilter()
		{
			if( filters.Count > 0 )
			{
				Destroy();
				filters = new List<MovieClipFilter>();
			}
			MovieClipFilter filter = FiltersFactory.GetScaledYellowGlowFilter();
			if( filter == null ) DebugConsole.LogError("FILTER IS NULL FROM FACTORY");
			else AddFilter(filter);
		}
		
		public void ShowFilter()
		{
			if( filters.Count > 0 ) filters[0].Visible = true;
			else AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
		}
		
		public void HideFilter()
		{
			if( filters.Count > 0 ) filters[0].Visible = false;
		}
		
		public void GoToImage(string _imgLabel)
		{
			imgLabel = _imgLabel;
			Target.gotoAndStop(imgLabel);
			Target.visible = true;
			SetFilter();
			//DebugConsole.Log("FRAME TO GO TO FOR COUNTING OBJECT: " + imgLabel+ ", label at: " + Target.currentLabel + ", visible: " + Target.visible);
		}
		
		public void ShowNumber(string num)
		{
			number.getChildByName<TextField>("label").text = num;
			number.visible = true;
		}
		
		public void HideNumber()
		{
			number.visible = false;
		}
	}
}

