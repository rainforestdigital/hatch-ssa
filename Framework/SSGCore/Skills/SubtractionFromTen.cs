using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.geom;

namespace SSGCore
{
	public class SubtractionFromTen : BaseSkill
	{
		private const string TOUCH_KEEPER = "Touch_Caretaker";
		private const string LETS_TRY = "Let's Try Again";
		
		private DisplayObjectContainer view;
		
		private SubFromTenOpp TTOpp = new SubFromTenOpp(7,1,"DOG");
		private SubFromTenOpp[] EGOpps = new SubFromTenOpp[]
		{
			new SubFromTenOpp(7,1,"DOG"),
			new SubFromTenOpp(7,2,"CAT"),
			new SubFromTenOpp(7,3,"TORTOISE"),
			new SubFromTenOpp(8,1,"IGUANA"),
			new SubFromTenOpp(8,2,"SUGARGLIDER"),
			new SubFromTenOpp(8,3,"FERRET"),
			new SubFromTenOpp(9,1,"GERBIL"),
			new SubFromTenOpp(9,2,"HEDGEHOG"),
			new SubFromTenOpp(10,1,"CHICK"),
			new SubFromTenOpp(10,2,"CHINCHILLA")
		};
		private SubFromTenOpp[] DGOpps = new SubFromTenOpp[]
		{
			new SubFromTenOpp(7,4,"DOG"),
			new SubFromTenOpp(7,5,"CAT"),
			new SubFromTenOpp(8,4,"RABBIT"),
			new SubFromTenOpp(8,5,"HAMSTER"),
			new SubFromTenOpp(9,3,"PARROT"),
			new SubFromTenOpp(9,4,"FROG"),
			new SubFromTenOpp(9,5,"GUINEAPIG"),
			new SubFromTenOpp(10,3,"HERMITCRAB"),
			new SubFromTenOpp(10,4,"POTBELLIEDPIG"),
			new SubFromTenOpp(10,5,"TURTLE")
		};
		private SubFromTenOpp[] DDOpps = new SubFromTenOpp[]
		{
			new SubFromTenOpp(7,6, 1),
			new SubFromTenOpp(8,6, 2),
			new SubFromTenOpp(8,7, 3),
			new SubFromTenOpp(9,6, 4),
			new SubFromTenOpp(9,7, 5),
			new SubFromTenOpp(9,8, 6),
			new SubFromTenOpp(10,6, 7),
			new SubFromTenOpp(10,7, 8),
			new SubFromTenOpp(10,8, 9),
			new SubFromTenOpp(10,9, 10),
		};
		
		private Vector2[] outPoints = { new Vector2(456.10f, 490.8f),
										new Vector2(477.30f, 418.3f),
										new Vector2(534.95f, 490.8f),
										new Vector2(556.15f, 418.3f),
										new Vector2(613.80f, 490.8f),
										new Vector2(635.00f, 418.3f),
										new Vector2(692.65f, 490.8f),
										new Vector2(713.85f, 418.3f),
										new Vector2(771.55f, 490.8f),
										new Vector2(792.70f, 418.3f)
		};
		
		private SubFromTenOpp[] oppList;
		private List<int> usedList;
		private SubFromTenOpp currentOpp;
		private int failCount;
		
		private List<int> inPlace;
		private MovieClip draggingObject;
		private Vector2 dragOffset;
		private Rectangle wagonBounds;
		private TimerObject glowTimer;
		
		private MovieClip wagon;
		private List<MovieClip> wagonObjects;
		private SubtractionFromTenTarg target;
		private List<FilterMovieClip> glowList;
		
		protected new SubtractionFromTenTutorial tutorial_mc;
		
		public SubtractionFromTen (SkillLevel level, SessionInfo  currentSession) : base(level, currentSession)
		{
			BACKGROUND = "Background";
			ARTIFACT = "Artifact";
			
			Init(null);
		}
		
		public override void Dispose ()
		{
			base.Dispose();
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			
			if( tutorial_mc != null )
				tutorial_mc.Dispose();
			
			if( wagonObjects != null && wagonObjects.Count > 0 )
			{
				foreach( MovieClip mc in wagonObjects )
				{
					mc.removeAllEventListeners( MouseEvent.MOUSE_DOWN );
					Tweener.removeTweens( mc );
				}
			}
			
			if( glowTimer != null )
				glowTimer.StopTimer();
			
			clearFilters();
			
			if( stage.hasEventListener( MouseEvent.MOUSE_UP ) )
			{
				stage.removeEventListener( MouseEvent.MOUSE_MOVE, onDragMove );
				stage.removeEventListener( MouseEvent.MOUSE_UP, onDragUp );
			}
		}
		
		public override void Init (MovieClip parent)
		{
			base.Init (parent);
			
			view = new DisplayObjectContainer();
			addChild(view);
			view.alpha = 0;
			Resize();
			
			totalOpportunities = 10;
			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			wagon = MovieClipFactory.CreateSubtractionFromTenWagon();
			target = new SubtractionFromTenTarg();
			
			if( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA )
			{
				wagon.x		= 433.65f;	wagon.y		= 735.75f;
				target.x	= 1843.2f;	target.y	= 821.20f;
				
				for( int i = 0; i < outPoints.Length; i++ )
				{
					outPoints[i].x *= 2;
					outPoints[i].y *= 2;
				}
			}
			else
			{
				wagon.x		= 216.8f;	wagon.y		= 367.85f;
				target.x	= 921.6f;	target.y	= 410.60f;
			}
			view.addChild(target);
			view.addChild(wagon);
			
			wagonBounds = wagon.getBounds(wagon);
			
			switch(_level)
			{
			case SkillLevel.Tutorial:
				totalOpportunities = 1;
				oppList = new SubFromTenOpp[]{ TTOpp };
				
				if(resumingSkill)
					nextOpportunity();
				else
				{
					tutorial_mc = new SubtractionFromTenTutorial();
					addChild( tutorial_mc.view );
					tutorial_mc.view.addEventListener( TutorialEvent.START, onTutorialStart );
					tutorial_mc.view.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
					tutorial_mc.view.scaleX = tutorial_mc.view.scaleY = view.scaleX;
					tutorial_mc.view.y = view.y;
				}
				break;
			case SkillLevel.Emerging:
				oppList = EGOpps;
				
				break;
			case SkillLevel.Developing:
				oppList = DGOpps;
				
				break;
			case SkillLevel.Developed:
			case SkillLevel.Completed:
				oppList = DDOpps;
				
				break;
			}
			usedList = new List<int>();
			
			if(resumingSkill && _level != SkillLevel.Tutorial)
			{
				parseSessionData( currentSession.sessionData );
			}
			
			opportunityAnswered = false;
		}
		
		public override void OnAudioWordComplete( string word )
		{
			switch( word )
			{
			case THEME_INTRO:
				if( !currentSession.skipIntro )
					playSound( "2H-Story Introduction Audio/2H-Story-Introduction", INTRO);
				else
					OnAudioWordComplete( INTRO );
				break;
				
			case INTRO:
				if(_level == SkillLevel.Tutorial){
					tutorial_mc.OnStart();
				}
				else
					nextOpportunity();
				break;
				
			case NOW_ITS:
				playInstruction();
				break;
				
			case TOUCH_KEEPER:
				clearFilters();
				startTimer();
				goto case PLEASE_FIND;
				
			case PLEASE_FIND:
				InteractOn();
				break;
				
			case CLICK_CORRECT:
				//play reinforcement
				dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
				break;
				
			case THEME_COMPLIMENT:
				playReinforcement();
				break;
				
			case YOU_FOUND:
				if( _level == SkillLevel.Tutorial ){
					playSound( "2H-Skill Instructions Audio/2H-lets-help-take-more-animals" , NOW_LETS );
					break;
				}
				goto case NOW_LETS;
				
			case NOW_LETS:
			case YOU_FOUND_INCORRECT:
				Tweener.addTween( view, Tweener.Hash( "time", 0.25f, "alpha", 0f ) ).OnComplete( opportunityEnd );
				break;
				
			case CLICK_INCORRECT:
				//play correction
				if(opportunityComplete)
					autoClear();
				else
					dispatchEvent( new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
				break;
				
			case THEME_CRITICISM:
				playInstruction();
				break;
				
			case LETS_TRY:
				if( _level == SkillLevel.Tutorial )
					resetTutorial();
				else
					playInstruction(1);
				break;
			}
		}
		
		private void opportunityEnd()
		{
			if(opportunityCorrect)
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
			else
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
		}
		
		private void OnTutorialStart( CEvent e )
		{
		}
		
		private void onTutorialComplete( CEvent e )
		{
			tutorial_mc.view.removeEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.view.removeEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			
			view.alpha = 1;
			removeChild( tutorial_mc.view );
			tutorial_mc.Dispose();
			
			nextOpportunity();
		}
		
		protected override void resetTutorial()
		{
			tutorial_mc = new SubtractionFromTenTutorial();
			addChild( tutorial_mc.view );
			tutorial_mc.view.addEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.view.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			tutorial_mc.view.scaleX = tutorial_mc.view.scaleY = view.scaleX;
			tutorial_mc.view.y = view.y;
			
			currentOpportunity--;
			usedList = new List<int>();
			
			Tweener.addTween( view, Tweener.Hash( "time", 0.2f, "alpha", 0 ) ).OnComplete( tutorial_mc.OnStart );
		}
		
		public override void nextOpportunity()
		{
			if(currentOpportunity == totalOpportunities)
			{
				tallyAnswers();
				return;
			}
			
			opportunityComplete = false;
			opportunityCorrect = true;
			timeoutCount = 0;
			
			int rand;
			if( resumingSkill & usedList.Count > 0 )
			{
				rand = usedList[ usedList.Count - 1 ];
				resumingSkill = false;
			}
			else
			{
				do{
					rand = Random.Range( 0, oppList.Length );
				} while ( rand == oppList.Length || usedList.Contains( rand ) );
				usedList.Add( rand );
			}
			
			currentOpp = oppList[rand];
			
			setWagon();
			target.reset();
			
			failCount = 0;
			
			Tweener.addTween( view, Tweener.Hash( "time", 0.25f, "alpha", 1.0f ) ).OnComplete( playInstruction );
			
			currentOpportunity++;
			
			AddNewOppData( audioNames.IndexOf( getInstruction() ) );
			
			AddNewObjectData( formatObjectData( "sign", target.parent.localToGlobal(new Vector2( target.x, target.y )) ) );
			
			int i = 0;
			foreach( MovieClip mc in wagonObjects )
			{
				string typeName = _level == SkillLevel.Developed || _level == SkillLevel.Completed ? "ANIMAL" + currentOpp.varient : currentOpp.wagonType;
				AddNewObjectData( formatObjectData( typeName, mc.parent.localToGlobal(new Vector2( mc.x, mc.y )) ) );
			}
		}
		
		private void setWagon()
		{
			string wagonFrame;
			if( _level == SkillLevel.Developed || _level == SkillLevel.Completed )
			{
				wagonFrame = "DDANIMAL" + currentOpp.varient;
			}
			else if( _level == SkillLevel.Developing )
				wagonFrame = "DG" + currentOpp.wagonType;
			else
				wagonFrame = "EG" + currentOpp.wagonType;
			
			//clear altered positions of loaded objects
			wagon.gotoAndStop(1);wagon.gotoAndStop(2);
			//set opp frame
			wagon.gotoAndStop( wagonFrame );
			
			wagonObjects = new List<MovieClip>();
			inPlace = null;
			
			MovieClip child;
			int i;
			for( i = 1; i < wagon.numChildren; i++ )
			{
				child = wagon.getChildAt<MovieClip>( i );
				child.alpha = 1;
				child.mouseEnabled = true;
				child.name = child.x.ToString() + ";" + child.y.ToString();
				wagonObjects.Add(child);
			}
			
			wagonObjects.Sort( (a, b) => {
				//if x is lower, it's further to the left, it should go first
				if( a.x < b.x ) return -1;
				if( b.x < a.x ) return 1;
				//they're in the same place? failsafe to prevent errors
				return 0;
			} );
			
			for ( i = wagonObjects.Count - 1; i >= currentOpp.wagonNum; i-- )
			{
				child = wagonObjects[i];
				child.alpha = 0;
				child.mouseEnabled = false;
				wagonObjects.Remove(child);
			}
		}
		
		private void InteractOn()
		{
			foreach( MovieClip child in wagonObjects )
			{
				child.addEventListener( MouseEvent.MOUSE_DOWN, onDragDown );
			}
			if( inPlace != null && !target.hasEventListener( MouseEvent.CLICK ) && inPlace.Count > 0 )
				target.addEventListener( MouseEvent.CLICK, AnswerSubmit );
			
			startTimer();
				
			if(_level != SkillLevel.Tutorial)
				MarkInstructionEnd();
		}
		
		private void InteractOff()
		{
			foreach( MovieClip child in wagonObjects )
			{
				child.removeAllEventListeners( MouseEvent.MOUSE_DOWN );
			}
			if( target.hasEventListener( MouseEvent.CLICK ) )
				target.removeEventListener( MouseEvent.CLICK, AnswerSubmit );
			
			stopTimer();
			
			MarkInstructionStart();
		}
		
		public void onDragDown( CEvent e )
		{
			PlayClickSound();
			
			InteractOff();
			timeoutCount = 0;
			
			draggingObject = e.currentTarget as MovieClip;
			dragOffset = new Vector2(draggingObject.x - draggingObject.parent.mouseX, draggingObject.y - draggingObject.parent.mouseY);
			draggingObject.scaleX = draggingObject.scaleY = draggingObject.scaleX * 1.5f;
			
			stage.addEventListener( MouseEvent.MOUSE_MOVE, onDragMove );
			stage.addEventListener( MouseEvent.MOUSE_UP, onDragUp );
			
			wagon.removeChild( draggingObject );
			wagon.addChild( draggingObject );
			
			string[] splitName = draggingObject.name.Split( ';' );
			if( inPlace != null && inPlace.Count > 0 && splitName.Length > 2 )
				inPlace.Remove( int.Parse( splitName[2] ) );
			
			if( glowList != null || glowList.Count > 0 )
			{
				FilterMovieClip toRemove = null;
				foreach( FilterMovieClip fmc in glowList )
				{
					if( fmc.Target != draggingObject )
						continue;
					
					toRemove = fmc;
					break;
				}
				if( toRemove != null )
				{
					toRemove.Destroy();
					glowList.Remove( toRemove );
					addFilter( draggingObject );
				}
			}
		}
		
		public void onDragMove( CEvent e )
		{
			Vector2 movement = new Vector2( draggingObject.x, draggingObject.y );
			
			draggingObject.x = draggingObject.parent.mouseX+dragOffset.x;
			draggingObject.y = draggingObject.parent.mouseY+dragOffset.y;
			
			movement.x -= draggingObject.x;
			movement.y -= draggingObject.y;
			
			if( glowList != null || glowList.Count > 0 )
			{
				foreach( FilterMovieClip fmc in glowList )
				{
					if( fmc.Target != draggingObject )
						continue;
					
					fmc.x -= movement.x;
					fmc.y -= movement.y;
					break;
				}
			}
		}
		
		public void onDragUp( CEvent e )
		{
			stage.removeEventListener( MouseEvent.MOUSE_MOVE, onDragMove );
			stage.removeEventListener( MouseEvent.MOUSE_UP, onDragUp );
			
			InteractOn();
			
			draggingObject.scaleX = draggingObject.scaleY = draggingObject.scaleX / 1.5f;
			
			string[] splitName = draggingObject.name.Split( ';' );
			//check where to snap to
			if( !wagonBounds.contains( draggingObject.x, draggingObject.y ) )
			{
				if( inPlace == null || inPlace.Count < 1 )
					target.addEventListener( MouseEvent.CLICK, AnswerSubmit );
				
				if( inPlace == null )
				{
					inPlace = new List<int>();
					stopTimer();
					playSound( "2H-Skill Instructions Audio/2H-when-youre-done-touch-caretaker", TOUCH_KEEPER );
					
					clearFilters();
					glowShopkeeper();
				}
				
				if( _level == SkillLevel.Tutorial )
				{
					foreach( MovieClip mc in wagonObjects )
					{
						mc.mouseEnabled = false;
						mc.mouseChildrenEnabled = false;
					}
				}
				
				//is not in the wagon, snap in
				for( int i = 0; i < wagonObjects.Count; i++ )
				{
					if( !inPlace.Contains( i ) )
					{
						draggingObject.name = splitName[0] + ";" + splitName[1] + ";" + i.ToString();
						//shift co-ords from view space to wagon space
						Vector2 targPoint = new Vector2( outPoints[i].x - (wagon.x/wagon.scaleX), outPoints[i].y - (wagon.y/wagon.scaleY) );
						Tweener.addTween( draggingObject, Tweener.Hash( "time", 0.35f, "x", targPoint.x, "y", targPoint.y ) );
						inPlace.Add( i );
						
						if( splitName.Length > 2 )
						{
							AddNewActionData( formatActionData( "snap", wagonObjects.IndexOf(draggingObject) + 1, Vector2.zero ) );
						}
						else
						{
							AddNewActionData( formatActionData( "move", wagonObjects.IndexOf(draggingObject) + 1, draggingObject.parent.localToGlobal( targPoint ) ) );
						}
						
						if( glowList != null || glowList.Count > 0 )
						{
							FilterMovieClip toRemove = null;
							foreach( FilterMovieClip fmc0 in glowList )
							{
								if( fmc0.Target != draggingObject )
									continue;
								
								toRemove = fmc0;
								break;
							}
							
							if( toRemove != null )
							{
								toRemove.Destroy();
								glowList.Remove( toRemove );
								addFilter( draggingObject );
								
								FilterMovieClip fmc1 = glowList[ glowList.Count - 1 ];
								Vector2 glowTarg = new Vector2( fmc1.x + ( targPoint.x - draggingObject.x ), fmc1.y + (targPoint.y - draggingObject.y ) );
								Tweener.addTween( fmc1, Tweener.Hash( "time", 0.35f, "x", glowTarg.x, "y", glowTarg.y ) );
							}
						}
						break; 
					}
				}
			}
			else
			{
				//is in the wagon, snap back in place
				draggingObject.name = splitName[0] + ";" + splitName[1];
				Vector2 targetPoint = new Vector2( float.Parse( splitName[0] ), float.Parse( splitName[1] ) );
				Tweener.addTween( draggingObject, Tweener.Hash( "time", 0.35f, "x", targetPoint.x, "y", targetPoint.y ) );
				
				if( splitName.Length > 2 )
				{
					AddNewActionData( formatActionData( "move", wagonObjects.IndexOf(draggingObject) + 1, draggingObject.parent.localToGlobal( targetPoint ) ) );
				}
				else
				{
					AddNewActionData( formatActionData( "snap", wagonObjects.IndexOf(draggingObject) + 1, Vector2.zero ) );
				}
				
				if( glowList != null || glowList.Count > 0 )
				{
					FilterMovieClip toRemove0 = null;
					foreach( FilterMovieClip fmc2 in glowList )
					{
						if( fmc2.Target != draggingObject )
							continue;
						
						toRemove0 = fmc2;
						break;
					}
					if( toRemove0 != null )
					{
						toRemove0.Destroy();
						glowList.Remove( toRemove0 );
						addFilter( draggingObject );
						
						FilterMovieClip fmc3 = glowList[ glowList.Count - 1 ];
						Vector2 glowPoint = new Vector2( fmc3.x + ( float.Parse( splitName[0] ) - draggingObject.x ), fmc3.y + ( float.Parse( splitName[1] ) - draggingObject.y ) );
						Tweener.addTween( fmc3, Tweener.Hash( "time", 0.35f, "x", glowPoint.x, "y", glowPoint.y ) );
					}
				}
			}
			draggingObject = null;
		}
		
		public void AnswerSubmit( CEvent e )
		{
			InteractOff();
			clearFilters();
			
			timeoutCount = 0;
			
			if( inPlace.Count == currentOpp.outNum )
			{
				//correct
				target.setNumber( currentOpp.wagonNum - currentOpp.outNum );
				
				PlayCorrectSound();
				
				AddNewActionData( formatActionData( "tap", 0, Vector2.zero, true ) );
			}
			else
			{
				resetPlacement();
				opportunityCorrect = false;
				//incorrect
				failCount++;
				if( failCount == 3 )
					opportunityComplete = true;
				PlayIncorrectSound();
				
				AddNewActionData( formatActionData( "tap", 0, Vector2.zero, false ) );
			}
		}
		
		private void resetPlacement(){ resetPlacement( false ); }
		private void resetPlacement( bool interact )
		{
			foreach( MovieClip mc in wagonObjects )
			{
				string[] splitName = mc.name.Split( ';' );
				mc.name = splitName[0] + ";" + splitName[1];
				TweenerObj tween = Tweener.addTween( mc, Tweener.Hash( "time", 0.25f, "x", float.Parse( splitName[0] ), "y", float.Parse( splitName[1] ) ) );
				if( interact )
					tween.OnComplete( () => { InteractOn(); stopTimer(); } );
			}
			if( inPlace != null )
			{
				inPlace.Clear();
				inPlace = null;
			}
		}
		
		private string autoClear()
		{
			//run the reenforcement audio
			string filename = folderLevelCode() + "/2H-"+ currentOpp.wagonNum + "-" + objectType() + "-i-move-" + currentOpp.outNum +
				"-now-" + (currentOpp.wagonNum - currentOpp.outNum);
			
			//and move objects
			if(inPlace == null)
				inPlace = new List<int>();
			
			string[] splitName;
			for( int i = 0; i < currentOpp.outNum; i++ )
			{
				draggingObject = wagonObjects[i];
				splitName = draggingObject.name.Split(';');
				
				draggingObject.name = splitName[0] + ";" + splitName[1] + ";" + i.ToString();
				//shift co-ords from view space to wagon space
				Vector2 targPoint = new Vector2( outPoints[i].x - (wagon.x/wagon.scaleX), outPoints[i].y - (wagon.y/wagon.scaleY) );
				Tweener.addTween( draggingObject, Tweener.Hash( "time", 0.25f, "x", targPoint.x, "y", targPoint.y ) );
				inPlace.Add( i );
				
				draggingObject = null;
			}
			
			target.setNumber( currentOpp.wagonNum - currentOpp.outNum );
			
			glowSequence(2);
			
			return playSound( "2H-Inactivity Reinforcement Audio/" + filename, YOU_FOUND_INCORRECT );
		}
		
		public override void onTimerComplete()
		{
			base.onTimerComplete();
			
			stopTimer();
			timeoutCount++;
			
			string clipName = "";
			
			if(_level == SkillLevel.Tutorial)
			{
				if(timeoutCount == 1)
				{
					if( target.hasEventListener( MouseEvent.CLICK ) )
					{
						clipName = playSound("2H-Skill Instructions Audio/2H-when-youre-done-touch-caretaker", TOUCH_KEEPER);
						glowShopkeeper();
					}
					else
					{
						clipName = playSound("2H-Skill Instructions Audio/2H-now-its-your-turn", NOW_ITS);
					}
				}
				else
				{
					InteractOff();
					if(!opportunityAnswered)
					{
						opportunityAnswered = true;
						opportunityCorrect = true;
						
						clipName = playSound("2H-Skill Instructions Audio/2H-try-again", LETS_TRY);
					}
					else
					{
						opportunityCorrect = true;
						opportunityComplete = true;
						dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0"));
					}
				}
			}
			else
			{
				switch(timeoutCount)
				{
				case 1:
					if( inPlace != null )
					{
						clipName = playSound("2H-Skill Instructions Audio/2H-when-youre-done-touch-caretaker", TOUCH_KEEPER);
						glowShopkeeper();
					}
					else
					{
						playInstruction();
						clipName = getInstruction();
					}
					break;
				case 2:
					playSound("2H-Skill Instructions Audio/2H-touch-the-green-button", PLEASE_FIND);
					break;
				case 3:
					opportunityCorrect = false;
					if( inPlace != null )
					{
						InteractOff();
						resetPlacement( true );
						clipName = playSound("2H-Skill Instructions Audio/2H-try-again", LETS_TRY);
					}
					else
					{
						playInstruction(1);
						clipName = getInstruction();
					}
					break;
				default:
					timeoutEnd = true;
					InteractOff();
					clipName = autoClear();
					break;
				}	
			}
			
			if( !string.IsNullOrEmpty( clipName ) )
			{
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf(clipName) );
			}
		}
		
		private void playInstruction(){ playInstruction(0); }
		private void playInstruction( int tier )
		{
			string fileName = folderLevelCode() + "/2H-"+ currentOpp.wagonNum + "-" + objectType() + "-move-"+ currentOpp.outNum;
			
			glowSequence( tier );
			
			playSound( "2H-Skill Instructions Audio/2H-Initial Instructions/" + fileName, PLEASE_FIND );
		}
			
		private string getInstruction()
		{
			return "2H-Skill Instructions Audio/2H-Initial Instructions/" + folderLevelCode() + "/2H-"+ currentOpp.wagonNum + "-" + objectType() + "-move-"+ currentOpp.outNum;
		}
		
		private string playReinforcement()
		{
			string fileName = folderLevelCode() + "/2H-" + currentOpp.wagonNum + "-" + objectType() + "-you-move-" + currentOpp.outNum +
				"-now-" + (currentOpp.wagonNum - currentOpp.outNum);
			
			glowSequence(2);
			
			return playSound( "2H-Reinforcement Audio/" + fileName, YOU_FOUND );
		}
		
		private string objectType()
		{
			return currentOpp.wagonType.ToLower();
		}
		
		private string folderLevelCode()
		{
			string levelCode = "";
			switch( _level )
			{
			case SkillLevel.Tutorial:
			case SkillLevel.Emerging:
				levelCode = "TTEG";
				break;
			case SkillLevel.Developing:
				levelCode = "DG";
				break;
			case SkillLevel.Developed:
			case SkillLevel.Completed:
				levelCode = "DDCD";
				break;
			}
			return levelCode;
		}
		
		private string playSound ( string clipName ) { return playSound(clipName, "" ); }
		private string playSound ( string clipName, string word )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SUBTRACTIONFROMTEN, clipName );
			
			if(word != "")
				bundle.AddClip(clip, word, clip.length);
			else
				bundle.AddClip(clip);
			
			SoundEngine.Instance.PlayBundle(bundle);
			
			return clipName;
		}
		
		private void glowSequence( int tier )
		{
			//each timer replaces itself in sequence
			//.8 - 3 - glow all objects
			glowTimer = TimerUtils.SetTimeout( 0.8f, () => {
				glowBaseFromWagon();
				clearGlowTimer();
				glowTimer = TimerUtils.SetTimeout( 2.2f, () => { //+ 0.8 = 3
					clearFilters();
					clearGlowTimer();
					if( tier > 0 ){
						//4.2 - 5.5 - glow the objects moved out
						glowTimer = TimerUtils.SetTimeout( 1.2f, () => { //+ 3 = 4.2
							glowPulledFromWagon();
							clearGlowTimer();
							glowTimer = TimerUtils.SetTimeout( 1.3f, () => { //+ 4.2 = 5.5
								clearFilters();
								clearGlowTimer();
								if( tier > 1 ){
									//6.5 - 9 - glow the objects that weren't moved out
									glowTimer = TimerUtils.SetTimeout( 1.0f, () => { //+ 5.5 = 6.5
										glowLeftInWagon();
										glowShopkeeper();
										clearGlowTimer();
										glowTimer = TimerUtils.SetTimeout( 2.5f, () => { //+ 6.5 = 9
											clearFilters();
											clearGlowTimer();
										});
									});
								}
							});
						});
					}
				});
			});
		}
		
		private void clearGlowTimer()
		{
			if( glowTimer != null )
			{
				glowTimer.StopTimer();
				glowTimer = null;
			}
		}
		
		private void glowBaseFromWagon()
		{
			for( int i = 0; i < currentOpp.wagonNum; i++ )
			{
				addFilter( wagonObjects[i] );
			}
		}
		
		private void glowLeftInWagon()
		{
			string[] splitName;
			foreach( MovieClip mc in wagonObjects )
			{
				splitName = mc.name.Split(';');
				if( splitName.Length < 3 )
					addFilter( mc );
			}
		}
		
		private void glowPulledFromWagon()
		{
			string[] splitName;
			int i = 0;
			foreach( MovieClip mc in wagonObjects )
			{
				splitName = mc.name.Split(';');
				if( splitName.Length > 2 )
				{
					addFilter( mc );
					i++;
				}
			}
			
			if( i < currentOpp.outNum )
			{
				foreach( MovieClip mc0 in wagonObjects )
				{
					if( i == currentOpp.outNum )
						break;
					
					splitName = mc0.name.Split(';');
					if( splitName.Length < 3 )
					{
						addFilter( mc0 );
						i++;
					}
				}
			}
		}
		
		private void glowShopkeeper()
		{
			if( glowList == null )
				glowList = new List<FilterMovieClip>();
			
			DisplayObjectContainer _parent = target.parent;
			FilterGroupMovieClip fmc = new FilterGroupMovieClip( target );
			fmc.AddFilter( FiltersFactory.GetScaledYellowGlowFilter );
			fmc.removeChild( target );
			_parent.addChild( fmc );
			_parent.addChild( target );
			glowList.Add( fmc );
		}
		
		private void addFilter( MovieClip mc )
		{
			if( glowList == null )
				glowList = new List<FilterMovieClip>();
			
			DisplayObjectContainer _parent = mc.parent;
			FilterMovieClip fmc = new FilterMovieClip( mc );
			fmc.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
			fmc.removeChild( mc );
			_parent.addChild( fmc );
			_parent.addChild( mc );
			glowList.Add( fmc );
		}
		
		private void clearFilters()
		{
			if( glowList == null )
				return;
			
			foreach( FilterMovieClip fmc in glowList )
			{
				if( fmc.parent != null )
					fmc.parent.removeChild( fmc );
				fmc.Destroy();
			}
			glowList.Clear();
		}
		
		public void Resize()
		{
			if( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA )
			{
				resize( 2048f, 1536f );
			}
			else
			{
				resize( 1024f, 768f );
			}
		}
		//size in should be the stage size from the flash file
		private void resize( int width, int height ) { resize( (float)width, (float)height ); }
		private void resize( float width, float height )
		{
			if( view == null )
				return;
			
			float scale = (float)Screen.width / width;
			
			view.scaleX = view.scaleY = scale;
			view.y = ((float)Screen.height - ( scale * height ))/2f;
			view.x = 0;
		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = "";
			
			foreach(int i in usedList)
			{
				if(data != "")
					data = data + "-";
				data = data + i.ToString();
			}
			
			return data;
		}
		
		private void parseSessionData (string data)
		{
			usedList = new List<int>();
			
			string[] sData = data.Split('-');
			
			foreach( string s in sData )
			{
				usedList.Add( int.Parse(s) );
			}
		}
		
		private static List<string> audioNames = new List<string>
		{	"",
			"2H-Story Introduction Audio/2H-Story-Introduction",
			"2H-Skill Instructions Audio/2H-some-of-these-animals-have-found homes",
			"2H-Skill Instructions Audio/2H-when-youre-done-touch-caretaker",
			"2H-Skill Instructions Audio/2H-now-its-your-turn",
			"2H-Skill Instructions Audio/2H-Initial Instructions/TTEG/2H-7-dog-move-1",
			"2H-Reinforcement Audio/TTEG/2H-7-dog-you-move-1-now-6",
			"2H-Skill Instructions Audio/2H-try-again",
			"2H-Skill Instructions Audio/2H-touch-the-green-button",
			"2H-Skill Instructions Audio/2H-lets-help-take-more-animals    ",
			"",   
			"2H-Skill Instructions Audio/2H-Initial Instructions/TTEG/2H-7-cat-move-2",
			"2H-Skill Instructions Audio/2H-Initial Instructions/TTEG/2H-7-tortoise-move-3",
			"2H-Skill Instructions Audio/2H-Initial Instructions/TTEG/2H-8-iguana-move-1",
			"2H-Skill Instructions Audio/2H-Initial Instructions/TTEG/2H-8-sugarglider-move-2",
			"2H-Skill Instructions Audio/2H-Initial Instructions/TTEG/2H-8-ferret-move-3",
			"2H-Skill Instructions Audio/2H-Initial Instructions/TTEG/2H-9-gerbil-move-1",
			"2H-Skill Instructions Audio/2H-Initial Instructions/TTEG/2H-9-hedgehog-move-2",
			"2H-Skill Instructions Audio/2H-Initial Instructions/TTEG/2H-10-chick-move-1",
			"2H-Skill Instructions Audio/2H-Initial Instructions/TTEG/2H-10-chinchilla-move-2",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DG/2H-7-dog-move-4",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DG/2H-7-cat-move-5",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DG/2H-8-rabbit-move-4",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DG/2H-8-hamster-move-5",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DG/2H-9-parrot-move-3",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DG/2H-9-frog-move-4",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DG/2H-9-guineapig-move-5",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DG/2H-10-hermitcrab-move-3",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DG/2H-10-potbelliedpig-move-4",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DG/2H-10-turtle-move-5",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DDCD/2H-7-animal-move-6",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DDCD/2H-8-animal-move-6",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DDCD/2H-8-animal-move-7",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DDCD/2H-9-animal-move-6",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DDCD/2H-9-animal-move-7",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DDCD/2H-9-animal-move-8",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DDCD/2H-10-animal-move-6",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DDCD/2H-10-animal-move-7",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DDCD/2H-10-animal-move-8",
			"2H-Skill Instructions Audio/2H-Initial Instructions/DDCD/2H-10-animal-move-9",
			"2H-Reinforcement Audio/TTEG/2H-7-dog-you-move-1-now-6",
			"2H-Reinforcement Audio/TTEG/2H-7-cat-you-move-2-now-5",
			"2H-Reinforcement Audio/TTEG/2H-7-tortoise-you-move-3-now-4",
			"2H-Reinforcement Audio/TTEG/2H-8-iguana-you-move-1-now-7",
			"2H-Reinforcement Audio/TTEG/2H-8-sugarglider-you-move-2-now-6",
			"2H-Reinforcement Audio/TTEG/2H-8-ferret-you-move-3-now-5",
			"2H-Reinforcement Audio/TTEG/2H-9-gerbil-you-move-1-now-8",
			"2H-Reinforcement Audio/TTEG/2H-9-hedgehog-you-move-2-now-7",
			"2H-Reinforcement Audio/TTEG/2H-10-chick-you-move-1-now-9",
			"2H-Reinforcement Audio/TTEG/2H-10-chinchilla-you-move-2-now-8",
			"2H-Reinforcement Audio/DG/2H-7-dog-you-move-4-now-3",
			"2H-Reinforcement Audio/DG/2H-7-cat-you-move-5-now-2",
			"2H-Reinforcement Audio/DG/2H-8-rabbit-you-move-4-now-4",
			"2H-Reinforcement Audio/DG/2H-8-hamster-you-move-5-now-3",
			"2H-Reinforcement Audio/DG/2H-9-parrot-you-move-3-now-6",
			"2H-Reinforcement Audio/DG/2H-9-frog-you-move-4-now-5",
			"2H-Reinforcement Audio/DG/2H-9-guineapig-you-move-5-now-4",
			"2H-Reinforcement Audio/DG/2H-10-hermitcrab-you-move-3-now-7",
			"2H-Reinforcement Audio/DG/2H-10-potbelliedpig-you-move-4-now-6",
			"2H-Reinforcement Audio/DG/2H-10-turtle-you-move-5-now-5",
			"2H-Reinforcement Audio/DDCD/2H-7-animal-you-move-6-now-1",
			"2H-Reinforcement Audio/DDCD/2H-8-animal-you-move-6-now-2",
			"2H-Reinforcement Audio/DDCD/2H-8-animal-you-move-7-now-1",
			"2H-Reinforcement Audio/DDCD/2H-9-animal-you-move-6-now-3",
			"2H-Reinforcement Audio/DDCD/2H-9-animal-you-move-7-now-2",
			"2H-Reinforcement Audio/DDCD/2H-9-animal-you-move-8-now-1",
			"2H-Reinforcement Audio/DDCD/2H-10-animal-you-move-6-now-4",
			"2H-Reinforcement Audio/DDCD/2H-10-animal-you-move-7-now-3",
			"2H-Reinforcement Audio/DDCD/2H-10-animal-you-move-8-now-2",
			"2H-Reinforcement Audio/DDCD/2H-10-animal-you-move-9-now-1",
			"2H-Inactivity Reinforcement Audio/TTEG/2H-7-dog-i-move-1-now-6",
			"2H-Inactivity Reinforcement Audio/TTEG/2H-7-cat-i-move-2-now-5",
			"2H-Inactivity Reinforcement Audio/TTEG/2H-7-tortoise-i-move-3-now-4",
			"2H-Inactivity Reinforcement Audio/TTEG/2H-8-iguana-i-move-1-now-7",
			"2H-Inactivity Reinforcement Audio/TTEG/2H-8-sugarglider-i-move-2-now-6",
			"2H-Inactivity Reinforcement Audio/TTEG/2H-8-ferret-i-move-3-now-5",
			"2H-Inactivity Reinforcement Audio/TTEG/2H-9-gerbil-i-move-1-now-8",
			"2H-Inactivity Reinforcement Audio/TTEG/2H-9-hedgehog-i-move-2-now-7",
			"2H-Inactivity Reinforcement Audio/TTEG/2H-10-chick-i-move-1-now-9",
			"2H-Inactivity Reinforcement Audio/TTEG/2H-10-chinchilla-i-move-2-now-8",
			"2H-Inactivity Reinforcement Audio/DG/2H-7-dog-i-move-4-now-3",
			"2H-Inactivity Reinforcement Audio/DG/2H-7-cat-i-move-5-now-2",
			"2H-Inactivity Reinforcement Audio/DG/2H-8-rabbit-i-move-4-now-4",
			"2H-Inactivity Reinforcement Audio/DG/2H-8-hamster-i-move-5-now-3",
			"2H-Inactivity Reinforcement Audio/DG/2H-9-parrot-i-move-3-now-6",
			"2H-Inactivity Reinforcement Audio/DG/2H-9-frog-i-move-4-now-5",
			"2H-Inactivity Reinforcement Audio/DG/2H-9-guineapig-i-move-5-now-4",
			"2H-Inactivity Reinforcement Audio/DG/2H-10-hermitcrab-i-move-3-now-7",
			"2H-Inactivity Reinforcement Audio/DG/2H-10-potbelliedpig-i-move-4-now-6",
			"2H-Inactivity Reinforcement Audio/DG/2H-10-turtle-i-move-5-now-5",
			"2H-Inactivity Reinforcement Audio/DDCD/2H-7-animal-i-move-6-now-1",
			"2H-Inactivity Reinforcement Audio/DDCD/2H-8-animal-i-move-6-now-2",
			"2H-Inactivity Reinforcement Audio/DDCD/2H-8-animal-i-move-7-now-1",
			"2H-Inactivity Reinforcement Audio/DDCD/2H-9-animal-i-move-6-now-3",
			"2H-Inactivity Reinforcement Audio/DDCD/2H-9-animal-i-move-7-now-2",
			"2H-Inactivity Reinforcement Audio/DDCD/2H-9-animal-i-move-8-now-1",
			"2H-Inactivity Reinforcement Audio/DDCD/2H-10-animal-i-move-6-now-4",
			"2H-Inactivity Reinforcement Audio/DDCD/2H-10-animal-i-move-7-now-3",
			"2H-Inactivity Reinforcement Audio/DDCD/2H-10-animal-i-move-8-now-2",
			"2H-Inactivity Reinforcement Audio/DDCD/2H-10-animal-i-move-9-now-1",
			"User-Earns-Artifact"
		};
	}
	
	public class SubtractionFromTenTarg : MovieClip
	{
		private MovieClip target;
		
		public SubtractionFromTenTarg()
		{
			target = MovieClipFactory.CreateSubtractionFromTenTarget();
			reset ();
			addChild(target);
		}
		
		public void reset()
		{
			target.gotoAndStop(1);
		}
		
		public void setNumber( int answer )
		{
			target.gotoAndStop(2);
			MovieClip targ = target.getChildAt<MovieClip>( target.numChildren - 1 );
			targ = targ.getChildAt<MovieClip>( targ.numChildren - 2 );
			targ.gotoAndStop( answer );
		}
	}
	
	public class SubFromTenOpp
	{
		public int wagonNum;
		public int outNum;
		public int varient = 0;
		public string wagonType;
		
		public SubFromTenOpp( int _wagonNum, int _outNum, int _varient )
		{ wagonNum = _wagonNum; outNum = _outNum; wagonType = "ANIMAL"; varient = _varient; }
		public SubFromTenOpp( int _wagonNum, int _outNum, string _wagonName )
		{ wagonNum = _wagonNum; outNum = _outNum; wagonType = _wagonName;  }
	}
}
