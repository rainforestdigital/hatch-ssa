using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.geom;

namespace SSGCore
{
	public class AdditionUpToTen : BaseSkill
	{
		private const string TOUCH_KEEPER = "Touch_Shopkeeper";
		private const string LETS_TRY = "Let's Try Again";
		
		private DisplayObjectContainer view;
		
		private AddtoTenOpp TTOpp = new AddtoTenOpp(5,1,"CHILIPEPPER", true);
		private AddtoTenOpp[] EGOpps = new AddtoTenOpp[]
		{
			new AddtoTenOpp(4,2,"CORN"),
			new AddtoTenOpp(5,1,"CHILIPEPPER", true),
			new AddtoTenOpp(5,2,"TOMATO"),
			new AddtoTenOpp(6,1,"ONION"),
			new AddtoTenOpp(6,2,"POMEGRANATE"),
			new AddtoTenOpp(7,1,"APPLE"),
			new AddtoTenOpp(7,2,"PINEAPPLE"),
			new AddtoTenOpp(8,1,"CHILIPEPPER", true),
			new AddtoTenOpp(8,2,"TOMATO"),
			new AddtoTenOpp(9,1,"ONION")
		};
		private AddtoTenOpp[] DGOpps = new AddtoTenOpp[]
		{
			new AddtoTenOpp(2,4,"AVOCADO"),
			new AddtoTenOpp(3,3,"CHAYOTE"),
			new AddtoTenOpp(3,4,"JICAMA"),
			new AddtoTenOpp(4,3,"ZUCCHINI"),
			new AddtoTenOpp(4,4,"SQUASH"),
			new AddtoTenOpp(5,3,"PLANTAIN", true),
			new AddtoTenOpp(5,4,"DRAGONFRUIT", true),
			new AddtoTenOpp(6,3,"AVOCADO", true),
			new AddtoTenOpp(6,4,"ZUCCHINI", true),
			new AddtoTenOpp(7,3,"SQUASH")
		};
		private AddtoTenOpp[] DDOpps = new AddtoTenOpp[]
		{
			new AddtoTenOpp(1,5,"MARACA","SOMBRERO"),
			new AddtoTenOpp(1,6,"FAN","LANTERN"),
			new AddtoTenOpp(1,7,"PINATA","SOMBRERO"),
			new AddtoTenOpp(1,8,"SOMBRERO","BALLOON"),
			new AddtoTenOpp(1,9,"LANTERN","TOP"),
			new AddtoTenOpp(2,5,"TOP","PINATA"),
			new AddtoTenOpp(2,6,"BALLOON","MARACA"),
			new AddtoTenOpp(2,7,"MARACA","LANTERN"),
			new AddtoTenOpp(2,8,"FAN","PINATA"),
			new AddtoTenOpp(3,5,"PINATA","TOP"),
			new AddtoTenOpp(3,6,"SOMBRERO","LANTERN"),
			new AddtoTenOpp(3,7,"LANTERN","BALLOON"),
			new AddtoTenOpp(4,5,"TOP","PINATA"),
			new AddtoTenOpp(4,6,"BALLOON","SOMBRERO"),
			new AddtoTenOpp(5,5,"MARACA","PINATA")
		};
		
		private AddtoTenOpp[] oppList;
		private List<int> usedList;
		private AddtoTenOpp currentOpp;
		private int failCount;
		
		private List<int> inPlace;
		private MovieClip draggingObject;
		private Vector2 dragOffset;
		private Rectangle basketBounds;
		private TimerObject glowTimer;
		
		private MovieClip basket;
		private List<MovieClip> basketObjects;
		private MovieClip pile;
		private List<MovieClip> pileObjects;
		private AdditionToTenTarg target;
		private List<FilterMovieClip> glowList;
		
		private float pileScale;
		private float inScale;
		
		protected new AdditionUpToTenTutorial tutorial_mc;
		
		public AdditionUpToTen (SkillLevel level, SessionInfo  currentSession) : base(level, currentSession)
		{
			BACKGROUND = "Background";
			ARTIFACT = "Artifact";
			
			Init(null);
		}
		
		public override void Dispose ()
		{
			base.Dispose();
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			
			if( tutorial_mc != null )
				tutorial_mc.Dispose();
			
			if( pileObjects != null && pileObjects.Count > 0 )
			{
				foreach( MovieClip mc in pileObjects )
				{
					mc.removeAllEventListeners( MouseEvent.MOUSE_DOWN );
					Tweener.removeTweens( mc );
				}
			}
			
			if( glowTimer != null )
				glowTimer.StopTimer();
			
			clearFilters();
			
			if( stage.hasEventListener( MouseEvent.MOUSE_UP ) )
			{
				stage.removeEventListener( MouseEvent.MOUSE_MOVE, onDragMove );
				stage.removeEventListener( MouseEvent.MOUSE_UP, onDragUp );
			}
		}
		
		public override void Init (MovieClip parent)
		{
			base.Init (parent);
			
			view = new DisplayObjectContainer();
			addChild(view);
			view.alpha = 0;
			Resize();
			
			totalOpportunities = 10;
			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			basket = MovieClipFactory.CreateAdditionUpToTenBasket();
			pile = MovieClipFactory.CreateAdditionUpToTenPile();
			target = new AdditionToTenTarg();
			
			if( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA )
			{
				basket.x	= 1022.4f;	basket.y	= 987.85f;	basket.scaleX = basket.scaleY = 770.95f/540f;
				pile.x		= 363.95f;	pile.y		= 711.6f;
				target.x	= 1659.45f;	target.y	= 656.5f;
			}
			else
			{
				basket.x	= 513.6f;	basket.y	= 493.9f;	basket.scaleX = basket.scaleY = 393.25f/286.1f;
				pile.x		= 181.95f;	pile.y		= 355.8f;
				target.x	= 830.0f;	target.y	= 328.25f;
			}
			view.addChild(target);
			view.addChild(basket);
			view.addChild(pile);
			
			basketBounds = basket.getBounds(view);
			
			switch(_level)
			{
			case SkillLevel.Tutorial:
				totalOpportunities = 1;
				oppList = new AddtoTenOpp[]{ TTOpp };
				
				if(resumingSkill)
					nextOpportunity();
				else
				{
					tutorial_mc = new AdditionUpToTenTutorial();
					addChild( tutorial_mc.view );
					tutorial_mc.view.addEventListener( TutorialEvent.START, onTutorialStart );
					tutorial_mc.view.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
					tutorial_mc.view.scaleX = tutorial_mc.view.scaleY = view.scaleX;
					tutorial_mc.view.y = view.y;
				}
				break;
			case SkillLevel.Emerging:
				oppList = EGOpps;
				
				break;
			case SkillLevel.Developing:
				oppList = DGOpps;
				
				break;
			case SkillLevel.Developed:
			case SkillLevel.Completed:
				totalOpportunities = 15;
				oppList = DDOpps;
				
				break;
			}
			usedList = new List<int>();
			
			if(resumingSkill && _level != SkillLevel.Tutorial)
			{
				parseSessionData( currentSession.sessionData );
			}
			
			opportunityAnswered = false;
		}
		
		public override void OnAudioWordComplete( string word )
		{
			switch( word )
			{
			case THEME_INTRO:
				if( !currentSession.skipIntro )
					playSound( "2G-Story Introduction Audio/2G-Story-Introduction", INTRO);
				else
					OnAudioWordComplete( INTRO );
				break;
				
			case INTRO:
				if(_level == SkillLevel.Tutorial){
					tutorial_mc.OnStart();
				}
				else
					nextOpportunity();
				break;
				
			case NOW_ITS:
				playInstruction();
				break;
				
			case TOUCH_KEEPER:
				clearFilters();
				startTimer();
				goto case PLEASE_FIND;
				
			case PLEASE_FIND:
				InteractOn();
				break;
				
			case CLICK_CORRECT:
				//play reinforcement
				dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
				break;
				
			case THEME_COMPLIMENT:
				playReinforcement();
				break;
				
			case YOU_FOUND:
				if( _level == SkillLevel.Tutorial ){
					playSound( "2G-Skill Instructions Audio/2G-lets-find-more-supplies-for-the-fiesta" , NOW_LETS );
					break;
				}
				goto case NOW_LETS;
				
			case NOW_LETS:
			case YOU_FOUND_INCORRECT:
				Tweener.addTween( view, Tweener.Hash( "time", 0.25f, "alpha", 0f ) ).OnComplete( opportunityEnd );
				break;
				
			case CLICK_INCORRECT:
				//play correction
				if(opportunityComplete)
					autoClear();
				else
					dispatchEvent( new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
				break;
				
			case THEME_CRITICISM:
				playInstruction();
				break;
				
			case LETS_TRY:
				if( _level == SkillLevel.Tutorial )
					resetTutorial();
				else
					playInstruction(1);
				break;
			}
		}
		
		private void opportunityEnd()
		{
			if(opportunityCorrect)
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
			else
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
		}
		
		private void OnTutorialStart( CEvent e )
		{
		}
		
		private void onTutorialComplete( CEvent e )
		{
			tutorial_mc.view.removeEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.view.removeEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			
			view.alpha = 1;
			removeChild( tutorial_mc.view );
			tutorial_mc.Dispose();
			
			nextOpportunity();
		}
		
		protected override void resetTutorial()
		{
			tutorial_mc = new AdditionUpToTenTutorial();
			addChild( tutorial_mc.view );
			tutorial_mc.view.addEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.view.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			tutorial_mc.view.scaleX = tutorial_mc.view.scaleY = view.scaleX;
			tutorial_mc.view.y = view.y;
			
			currentOpportunity--;
			usedList = new List<int>();
			
			Tweener.addTween( view, Tweener.Hash( "time", 0.2f, "alpha", 0 ) ).OnComplete( tutorial_mc.OnStart );
		}
		
		public override void nextOpportunity()
		{
			if(currentOpportunity == totalOpportunities)
			{
				tallyAnswers();
				return;
			}
			
			opportunityComplete = false;
			opportunityCorrect = true;
			timeoutCount = 0;
			
			pileScale = 0;
			inScale = 0;
			
			int rand;
			if( resumingSkill & usedList.Count > 0 )
			{
				rand = usedList[ usedList.Count - 1 ];
				resumingSkill = false;
			}
			else
			{
				do{
					rand = Random.Range( 0, oppList.Length );
				} while ( rand == oppList.Length || usedList.Contains( rand ) );
				usedList.Add( rand );
			}
			
			currentOpp = oppList[rand];
			
			setBasket();
			setPile();
			target.reset();
			
			if( _level == SkillLevel.Emerging || _level == SkillLevel.Tutorial )
				SetSingleObjectType();
			
			failCount = 0;
			
			Tweener.addTween( view, Tweener.Hash( "time", 0.25f, "alpha", 1.0f ) ).OnComplete( playInstruction );
			
			currentOpportunity++;
			
			AddNewOppData( audioNames.IndexOf( getInstructionFile() ) );
			
			AddNewObjectData( formatObjectData( "sign", target.parent.localToGlobal(new Vector2(target.x, target.y)) ) );
			
			foreach( MovieClip mc in pileObjects )
			{
				AddNewObjectData( formatObjectData( "out-" + currentOpp.pileType, mc.parent.localToGlobal(new Vector2(mc.x, mc.y)) ) );
			}
			foreach( MovieClip mc in basketObjects )
			{
				if(mc.alpha < 0.5f)
					continue;
				
				AddNewObjectData( formatObjectData( "in-" + currentOpp.basketType, mc.parent.localToGlobal(new Vector2(mc.x, mc.y)) ) );
			}
		}
		
		private void setBasket()
		{
			basket.gotoAndStop( currentOpp.basketType );
			
			basketObjects = new List<MovieClip>();
			inPlace = null;
			
			MovieClip child;
			int i;
			for( i = 0; i < basket.numChildren; i++ )
			{
				child = basket.getChildByName<MovieClip>( "mc" + i );
				if(child == null) break;
				
				child.alpha = 1;
				child.mouseEnabled = true;
				child.scaleX = child.scaleY = child.scaleX * 0.95f;
				basketObjects.Add(child);
			}
			
			for ( i = currentOpp.basketNum; i < basketObjects.Count; i++ )
			{
				child = basketObjects[i];
				child.alpha = 0;
				child.mouseEnabled = false;
			}
		}
		
		private void setPile()
		{
			//clear altered positions of loaded objects
			pile.gotoAndStop(1);pile.gotoAndStop(2);
			//set opp frame
			pile.gotoAndStop( "PILE" + ( currentOpp.pileType == "ONION" ? "ONIONS": (currentOpp.pileType == "MARACA" ? "MARACAS" :currentOpp.pileType) ) );
			
			pileObjects = new List<MovieClip>();
			
			float pileScaleMod = currentOpp.upsizePile ? 1.1f : 0.9f;
			
			MovieClip child;
			int i;
			for( i = 0; i < pile.numChildren; i++ )
			{
				child = pile.getChildAt<MovieClip>( i );
				child.alpha = 1;
				child.mouseEnabled = true;
				child.name = child.x.ToString() + ";" + child.y.ToString();
				child.scaleX = child.scaleY = child.scaleX * pileScaleMod;
				pileObjects.Add(child);
			}
			
			pileScale = pileObjects[0].scaleX;
			
			int totalPile;
			if( _level == SkillLevel.Developed || _level == SkillLevel.Completed )
				totalPile = 10 - currentOpp.basketNum;
			else
				totalPile = currentOpp.pileNum;
			
			while( pileObjects.Count > totalPile )
			{
				i = Random.Range( 0, pileObjects.Count );
				if( i < pileObjects.Count )
				{
					child = pileObjects[i];
					pileObjects.RemoveAt(i);
					child.alpha = 0;
					child.mouseEnabled = false;
				}
			}
		}
		
		private void SetSingleObjectType()
		{
			int variation = 1;
			if( _level == SkillLevel.Emerging )
				variation = Random.Range( 1, 5 );
			
			MovieClip mc;
			foreach( MovieClip mc1 in pileObjects )
			{
				mc = MovieClipFactory.CreateAdditionUpToTenObject();
				mc.gotoAndStop( currentOpp.pileType + variation );
				mc = mc.getChildAt<MovieClip>(0);
				
				mc.scaleX = mc.scaleY = mc1.getChildAt<DisplayObjectContainer>(0).scaleX;
				mc.rotation = 180;
				
				mc1.removeChildAt( 0 );
				mc1.addChild( mc );
			}
			foreach( MovieClip mc2 in basketObjects )
			{
				mc = MovieClipFactory.CreateAdditionUpToTenObject();
				mc.gotoAndStop( currentOpp.pileType + variation );
				mc = mc.getChildAt<MovieClip>(0);
				
				mc.scaleX = mc.scaleY = mc2.getChildAt<DisplayObjectContainer>(0).scaleX;
				mc.rotation = 180;
				
				mc2.removeChildAt( 0 );
				mc2.addChild( mc );
			}
		}
		
		private void InteractOn()
		{
			foreach( MovieClip child in pileObjects )
			{
				child.addEventListener( MouseEvent.MOUSE_DOWN, onDragDown );
			}
			if( inPlace != null && !target.hasEventListener( MouseEvent.CLICK ) && inPlace.Count > 0 )
				target.addEventListener( MouseEvent.CLICK, AnswerSubmit );
			
			startTimer();
				
			if(_level != SkillLevel.Tutorial)
				MarkInstructionEnd();
		}
		
		private void InteractOff()
		{
			foreach( MovieClip child in pileObjects )
			{
				child.removeAllEventListeners( MouseEvent.MOUSE_DOWN );
			}
			if( target.hasEventListener( MouseEvent.CLICK ) )
				target.removeEventListener( MouseEvent.CLICK, AnswerSubmit );
			
			stopTimer();
			
			MarkInstructionStart();
		}
		
		public void onDragDown( CEvent e )
		{
			PlayClickSound();
			
			InteractOff();
			timeoutCount = 0;
			
			draggingObject = e.currentTarget as MovieClip;
			dragOffset = new Vector2(draggingObject.x - draggingObject.parent.mouseX, draggingObject.y - draggingObject.parent.mouseY);
			draggingObject.scaleX = draggingObject.scaleY = draggingObject.scaleX * 1.5f;
			
			stage.addEventListener( MouseEvent.MOUSE_MOVE, onDragMove );
			stage.addEventListener( MouseEvent.MOUSE_UP, onDragUp );
			
			pile.removeChild( draggingObject );
			pile.addChild( draggingObject );
			
			string[] splitName = draggingObject.name.Split( ';' );
			if( inPlace != null && inPlace.Count > 0 && splitName.Length > 2 )
				inPlace.Remove( int.Parse( splitName[2] ) );
		}
		
		public void onDragMove( CEvent e )
		{
			draggingObject.x = draggingObject.parent.mouseX+dragOffset.x;
			draggingObject.y = draggingObject.parent.mouseY+dragOffset.y;
		}
		
		public void onDragUp( CEvent e )
		{
			stage.removeEventListener( MouseEvent.MOUSE_MOVE, onDragMove );
			stage.removeEventListener( MouseEvent.MOUSE_UP, onDragUp );
			
			InteractOn();
			
			draggingObject.scaleX = draggingObject.scaleY = draggingObject.scaleX / 1.5f;
			
			string[] splitName = draggingObject.name.Split( ';' );
			//check where to snap to
			//shift point from pile space to view space to check overlap
			if( basketBounds.contains( draggingObject.x + pile.x, draggingObject.y + pile.y ) )
			{
				if( inPlace == null || inPlace.Count < 1 )
					target.addEventListener( MouseEvent.CLICK, AnswerSubmit );
					
				if( inPlace == null )
				{
					inPlace = new List<int>();
					stopTimer();
					playSound( "2G-Skill Instructions Audio/2G-when-youre-done-touch-shopkeeper", TOUCH_KEEPER );
					glowShopkeeper();
				}
				//is in the basket, snap in
				for( int i = currentOpp.basketNum; i < basketObjects.Count; i++ )
				{
					if( !inPlace.Contains( i ) )
					{
						MovieClip targItem = basketObjects[i];
						if( inScale < 0.1f && _level != SkillLevel.Completed && _level != SkillLevel.Developed )
						{
							float dragRotate = draggingObject.rotation, targRotate = targItem.rotation;
							draggingObject.rotation = 0;
							targItem.rotation = 0;
							float s = targItem.getBounds(view).width / draggingObject.getBounds(view).width;
							draggingObject.rotation = dragRotate;
							targItem.rotation = targRotate;
							inScale = s;
						}
						else if( inScale < 0.1f )
							inScale = pileScale * 0.85f;
						
						draggingObject.scaleX = draggingObject.scaleY = inScale;
						
						draggingObject.name = splitName[0] + ";" + splitName[1] + ";" + i.ToString();
						//shift co-ords from basket space to view space to pile space
						Vector2 targPoint = new Vector2( (targItem.x*basket.scaleX) + basket.x - (pile.x/pile.scaleX),
															(targItem.y*basket.scaleY) + basket.y - (pile.y/pile.scaleY) );
						if( (basketObjects.Count == 10 && (i == 4 || i == 9)) || (basketObjects.Count == 11 && (i == 5 || i == 10)) )
							targPoint.y += 10f;
						
						Tweener.addTween( draggingObject, Tweener.Hash( "time", 0.35f, "x", targPoint.x, "y", targPoint.y ) );
						inPlace.Add( i );
						
						AddNewActionData( formatActionData( "move", pileObjects.IndexOf(draggingObject) + 1, draggingObject.parent.localToGlobal( targPoint ) ) );
						break;
					}
				}
			}
			else
			{
				//is not in the basket, snap back to pile
				draggingObject.scaleX = draggingObject.scaleY = pileScale;
				
				draggingObject.name = splitName[0] + ";" + splitName[1];
				Vector2 point = new Vector2( float.Parse( splitName[0] ), float.Parse( splitName[1] ) );
				Tweener.addTween( draggingObject, Tweener.Hash( "time", 0.35f, "x", float.Parse( splitName[0] ), "y", float.Parse( splitName[1] ) ) );
				
				AddNewActionData( formatActionData( "snap", pileObjects.IndexOf(draggingObject) + 1, Vector2.zero ) );
			}
			draggingObject = null;
		}
		
		public void AnswerSubmit( CEvent e )
		{
			InteractOff();
			clearFilters();
			
			timeoutCount = 0;
			
			if( inPlace.Count == currentOpp.pileNum )
			{
				//correct
				target.setNumber( currentOpp.pileNum + currentOpp.basketNum );
				
				PlayCorrectSound();
				
				AddNewActionData( formatActionData( "tap", 0, Vector2.zero, true ) );
			}
			else
			{
				resetPlacement();
				opportunityCorrect = false;
				//incorrect
				failCount++;
				if( failCount == 3 )
					opportunityComplete = true;
				PlayIncorrectSound();
				
				AddNewActionData( formatActionData( "tap", 0, Vector2.zero, false ) );
			}
		}
		
		private void resetPlacement(){ resetPlacement(false); }
		private void resetPlacement( bool interact )
		{
			foreach( MovieClip mc in pileObjects )
			{
				string[] splitName = mc.name.Split( ';' );
				mc.name = splitName[0] + ";" + splitName[1];
				TweenerObj tween = Tweener.addTween( mc, Tweener.Hash( "time", 0.25f, "x", float.Parse( splitName[0] ), "y", float.Parse( splitName[1] ), "scaleX", pileScale, "scaleY", pileScale ) );
				if( interact )
				{
					tween.OnComplete( () => { InteractOn(); stopTimer(); } );
				}
			}
			if( inPlace != null )
			{
				inPlace.Clear();
				inPlace = null;
			}
		}
		
		private string autoClear()
		{
			//run the reenforcement audio
			string filename = folderLevelCode() + "/2G-"+ currentOpp.basketNum + "-" + objectType() + "-i-add-" + currentOpp.pileNum +
				"-now-" + (currentOpp.basketNum + currentOpp.pileNum);
			
			playSound( "2G-Inactivity Reinforcement Audio/" + filename, YOU_FOUND_INCORRECT );
			
			//and move objects
			if( inPlace == null )
				inPlace = new List<int>();
			
			string[] splitName;
			for( int i = 0; i < currentOpp.pileNum; i++ )
			{
				draggingObject = pileObjects[i];
				splitName = draggingObject.name.Split(';');
				
				MovieClip targItem = basketObjects[i + currentOpp.basketNum];
				if( _level != SkillLevel.Developed && _level != SkillLevel.Completed )
				{
					float dragRotate = draggingObject.rotation, targRotate = targItem.rotation;
					draggingObject.rotation = 0;
					targItem.rotation = 0;
					float s = targItem.getBounds(view).width / draggingObject.getBounds(view).width;
					draggingObject.rotation = dragRotate;
					targItem.rotation = targRotate;
					draggingObject.scaleX = draggingObject.scaleY = s;
				}
				
				draggingObject.name = splitName[0] + ";" + splitName[1] + ";" + i.ToString();
				//shift co-ords from basket space to view space to pile space
				Vector2 targPoint = new Vector2( (targItem.x*basket.scaleX) + basket.x - (pile.x/pile.scaleX),
													(targItem.y*basket.scaleY) + basket.y - (pile.y/pile.scaleY) );
				Tweener.addTween( draggingObject, Tweener.Hash( "time", 0.25f, "x", targPoint.x, "y", targPoint.y ) );
				inPlace.Add( i );
				
				draggingObject = null;
			}
			
			target.setNumber( currentOpp.pileNum + currentOpp.basketNum );
			
			glowSequence(2);
			
			return "2G-Inactivity Reinforcement Audio/" + filename;
		}
		
		public override void onTimerComplete()
		{
			base.onTimerComplete();
			
			stopTimer();
			timeoutCount++;
			
			string clipName = "";
			if(_level == SkillLevel.Tutorial)
			{
				if(timeoutCount == 1)
				{
					if( target.hasEventListener( MouseEvent.CLICK ) )
					{
						clipName = playSound("2G-Skill Instructions Audio/2G-when-youre-done-touch-shopkeeper", TOUCH_KEEPER);
						glowShopkeeper();
					}
					else
					{
						clipName = playSound("2G-Skill Instructions Audio/2G-now-its-your-turn", NOW_ITS);
					}
				}
				else
				{
					InteractOff();
					if(!opportunityAnswered)
					{
						opportunityAnswered = true;
						opportunityCorrect = true;
						
						clipName = playSound("2G-Skill Instructions Audio/2G-try-again", LETS_TRY);
					}
					else
					{
						opportunityCorrect = true;
						opportunityComplete = true;
						dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0"));
					}
				}
			}
			else
			{
				switch(timeoutCount)
				{
				case 1:
					if(target.hasEventListener( MouseEvent.CLICK ))
					{
						clipName = playSound("2G-Skill Instructions Audio/2G-when-youre-done-touch-shopkeeper", TOUCH_KEEPER);
						glowShopkeeper();
					}
					else
					{
						playInstruction();
						clipName = getInstructionFile();
					}
					break;
				case 2:
					clipName = playSound("2G-Skill Instructions Audio/2G-touch-green-button", PLEASE_FIND);
					break;
				case 3:
					opportunityCorrect = false;
					if( inPlace != null )
					{
						InteractOff();
						resetPlacement( true );
						clipName = playSound("2G-Skill Instructions Audio/2G-try-again", LETS_TRY);
					}
					else
					{
						playInstruction(1);
						clipName = getInstructionFile();
					}
					break;
				default:
					timeoutEnd = true;
					InteractOff();
					clipName = autoClear();
					break;
				}	
			}
			
			if( !string.IsNullOrEmpty(clipName) )
			{
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf( clipName ) );
			}
		}
		
		private void playInstruction(){ playInstruction(0); }
		private void playInstruction( int tier )
		{
			playSound( getInstructionFile(), PLEASE_FIND );
			
			glowSequence( tier );
		}
		
		private string getInstructionFile()
		{
			string fileName = folderLevelCode() + "/2G-"+ currentOpp.basketNum + "-" + objectType() + "-drag-"+ currentOpp.pileNum;
			return "2G-Skill Instructions Audio/2G-Initial Instructions/" + fileName;
		}
		
		private void playReinforcement()
		{
			string fileName = folderLevelCode() + "/2G-" + currentOpp.basketNum + "-" + objectType() + "-you-add-" + currentOpp.pileNum +
				"-now-" + (currentOpp.basketNum + currentOpp.pileNum);
			
			playSound( "2G-Reinforcement Audio/" + fileName, YOU_FOUND );
			
			glowSequence(2);
		}
		
		private string objectType()
		{
			string type = currentOpp.basketType.ToLower();
			if( _level == SkillLevel.Developed || _level == SkillLevel.Completed )
				type = "supply";
			return type;
		}
		
		private string folderLevelCode()
		{
			string levelCode = "";
			switch( _level )
			{
			case SkillLevel.Tutorial:
			case SkillLevel.Emerging:
				levelCode = "TTEG";
				break;
			case SkillLevel.Developing:
				levelCode = "DG";
				break;
			case SkillLevel.Developed:
			case SkillLevel.Completed:
				levelCode = "DDCD";
				break;
			}
			return levelCode;
		}
		
		private string playSound ( string clipName ) { return playSound(clipName, "" ); }
		private string playSound ( string clipName, string word )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.ADDITIONUPTOTEN, clipName );
			
			if(word != "")
				bundle.AddClip(clip, word, clip.length);
			else
				bundle.AddClip(clip);
			
			SoundEngine.Instance.PlayBundle(bundle);
			
			return clipName;
		}
		
		private void glowSequence( int tier )
		{
			//each timer replaces itself in sequence
			//.8 - 3.3 - glow the originally in
			glowTimer = TimerUtils.SetTimeout( 0.8f, () => {
				glowBaseFromBasket();
				clearGlowTimer();
				glowTimer = TimerUtils.SetTimeout( 2.5f, () => { //+ 0.8 = 3.3
					clearFilters();
					clearGlowTimer();
					if( tier > 0 ){
						//4.5 - 5.8 - glow the newly in
						glowTimer = TimerUtils.SetTimeout( 1.2f, () => { //+ 3.3 = 4.5
							glowPlacedInBasket();
							clearGlowTimer();
							glowTimer = TimerUtils.SetTimeout( 1.3f, () => { //+ 4.5 = 5.8
								clearFilters();
								clearGlowTimer();
								if( tier > 1 ){
									//7 - 9.3 - glow everything that's in
									glowTimer = TimerUtils.SetTimeout( 1.2f, () => { //+ 5.8 = 7
										glowAllInBasket();
										glowShopkeeper();
										clearGlowTimer();
										glowTimer = TimerUtils.SetTimeout( 2.3f, () => { //+ 7 = 9.3
											clearFilters();
											clearGlowTimer();
										});
									});
								}
							});
						});
					}
				});
			});
		}
		
		private void clearGlowTimer()
		{
			if( glowTimer != null )
			{
				glowTimer.StopTimer();
				glowTimer = null;
			}
		}
		
		private void glowBaseFromBasket()
		{
			for( int i = 0; i < currentOpp.basketNum; i++ )
			{
				addFilter( basketObjects[i] );
			}
		}
		
		private void glowAllInBasket()
		{
			glowBaseFromBasket();
			glowPlacedInBasket();
		}
		
		private void glowPlacedInBasket()
		{
			string[] splitName;
			int i = 0;
			foreach( MovieClip mc in pileObjects )
			{
				splitName = mc.name.Split(';');
				if( splitName.Length > 2 )
				{
					addFilter( mc );
					i++;
				}
			}
			
			if( i < currentOpp.pileNum )
			{
				foreach( MovieClip mc0 in pileObjects )
				{
					if( i == currentOpp.pileNum )
						break;
					
					splitName = mc0.name.Split(';');
					if( splitName.Length < 3 )
					{
						addFilter( mc0 );
						i++;
					}
				}
			}
		}
		
		private void glowPile()
		{
			addFilter( pile );
		}
		
		private void glowShopkeeper()
		{
			if( glowList == null )
				glowList = new List<FilterMovieClip>();
			
			DisplayObjectContainer _parent = target.parent;
			FilterGroupMovieClip fmc = new FilterGroupMovieClip( target );
			fmc.AddFilter( FiltersFactory.GetScaledYellowGlowFilter );
			fmc.removeChild( target );
			_parent.addChild( fmc );
			_parent.addChild( target );
			glowList.Add( fmc );
		}
		
		private void addFilter( MovieClip mc )
		{
			if( glowList == null )
				glowList = new List<FilterMovieClip>();
			
			DisplayObjectContainer _parent = mc.parent;
			FilterMovieClip fmc = new FilterMovieClip( mc );
			
			fmc.AddFilter( new GlowMovieClipFilter(50, 2, 0.05f, Color.yellow, 1.1f) );
			fmc.removeChild( mc );
			_parent.addChild( fmc );
			_parent.addChild( mc );
			glowList.Add( fmc );
		}
		
		private void clearFilters()
		{
			if( glowList == null )
				return;
			
			foreach( FilterMovieClip fmc in glowList )
			{
				if( fmc.parent != null )
					fmc.parent.removeChild( fmc );
				fmc.Destroy();
			}
			glowList.Clear();
		}
		
		public void Resize()
		{
			if( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA )
			{
				resize( 2048f, 1536f );
			}
			else
			{
				resize( 1024f, 768f );
			}
		}
		//size in should be the stage size from the flash file
		private void resize( int width, int height ) { resize( (float)width, (float)height ); }
		private void resize( float width, float height )
		{
			if( view == null )
				return;
			
			float scale = (float)Screen.width / width;
			
			view.scaleX = view.scaleY = scale;
			view.y = ((float)Screen.height - ( scale * height ))/2f;
			view.x = 0;
		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = "";
			
			foreach(int i in usedList)
			{
				if(data != "")
					data = data + "-";
				data = data + i.ToString();
			}
			
			return data;
		}
		
		private void parseSessionData (string data)
		{
			usedList = new List<int>();
			
			string[] sData = data.Split('-');
			
			foreach( string s in sData )
			{
				usedList.Add( int.Parse(s) );
			}
		}
		
		private static List<string> audioNames = new List<string>
		{	"",
			"2G-Story Introduction Audio/2G-Story-Introduction",
			"2G-Skill Instructions Audio/2G-we-need-the-right-number-of-chilies",
			"2G-Skill Instructions Audio/2G-when-youre-done-touch-shopkeeper",
			"2G-Skill Instructions Audio/2G-now-its-your-turn",
			"2G-Skill Instructions Audio/2G-Initial Instructions/TTEG/2G-5-chilipepper-drag-1",
			"2G-Reinforcement Audio/TTEG/2G-5-chilipepper-you-add-1-now-6",
			"2G-Skill Instructions Audio/2G-try-again",
			"2G-Skill Instructions Audio/2G-touch-green-button",
			"2G-Skill Instructions Audio/2G-lets-find-more-supplies-for-the-fiesta",
			"2G-Skill Instructions Audio/2G-Initial Instructions/TTEG/2G-4-corn-drag-2",
			"",
			"2G-Skill Instructions Audio/2G-Initial Instructions/TTEG/2G-5-tomato-drag-2",
			"2G-Skill Instructions Audio/2G-Initial Instructions/TTEG/2G-6-onion-drag-1",
			"2G-Skill Instructions Audio/2G-Initial Instructions/TTEG/2G-6-pomegranate-drag-2",
			"2G-Skill Instructions Audio/2G-Initial Instructions/TTEG/2G-7-apple-drag-1",
			"2G-Skill Instructions Audio/2G-Initial Instructions/TTEG/2G-7-pineapple-drag-2",
			"2G-Skill Instructions Audio/2G-Initial Instructions/TTEG/2G-8-chilipepper-drag-1",
			"2G-Skill Instructions Audio/2G-Initial Instructions/TTEG/2G-8-tomato-drag-2",
			"2G-Skill Instructions Audio/2G-Initial Instructions/TTEG/2G-9-onion-drag-1",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DG/2G-2-avocado-drag-4",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DG/2G-3-chayote-drag-3",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DG/2G-3-jicama-drag-4",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DG/2G-4-zucchini-drag-3",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DG/2G-4-squash-drag-4",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DG/2G-5-plantain-drag-3",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DG/2G-5-dragonfruit-drag-4",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DG/2G-6-avocado-drag-3",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DG/2G-6-zucchini-drag-4",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DG/2G-7-squash-drag-3",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DDCD/2G-1-supply-drag-5",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DDCD/2G-1-supply-drag-6",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DDCD/2G-1-supply-drag-7",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DDCD/2G-1-supply-drag-8",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DDCD/2G-1-supply-drag-9",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DDCD/2G-2-supply-drag-5",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DDCD/2G-2-supply-drag-6",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DDCD/2G-2-supply-drag-7",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DDCD/2G-2-supply-drag-8",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DDCD/2G-3-supply-drag-5",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DDCD/2G-3-supply-drag-6",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DDCD/2G-3-supply-drag-7",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DDCD/2G-4-supply-drag-5",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DDCD/2G-4-supply-drag-6",
			"2G-Skill Instructions Audio/2G-Initial Instructions/DDCD/2G-5-supply-drag-5",
			"2G-Reinforcement Audio/TTEG/2G-4-corn-you-add-2-now-6",
			"2G-Reinforcement Audio/TTEG/2G-5-chilipepper-you-add-1-now-6",
			"2G-Reinforcement Audio/TTEG/2G-5-tomato-you-add-2-now-7",
			"2G-Reinforcement Audio/TTEG/2G-6-onion-you-add-1-now-7",
			"2G-Reinforcement Audio/TTEG/2G-6-pomegranate-you-add-2-now-8",
			"2G-Reinforcement Audio/TTEG/2G-7-apple-you-add-1-now-8",
			"2G-Reinforcement Audio/TTEG/2G-7-pineapple-you-add-2-now-9",
			"2G-Reinforcement Audio/TTEG/2G-8-chilipepper-you-add-1-now-9",
			"2G-Reinforcement Audio/TTEG/2G-8-tomato-you-add-2-now-10",
			"2G-Reinforcement Audio/TTEG/2G-9-onion-you-add-1-now-10",
			"2G-Reinforcement Audio/DG/2G-2-avocado-you-add-4-now-6",
			"2G-Reinforcement Audio/DG/2G-3-chayote-you-add-3-now-6",
			"2G-Reinforcement Audio/DG/2G-3-jicama-you-add-4-now-7",
			"2G-Reinforcement Audio/DG/2G-4-zucchini-you-add-3-now-7",
			"2G-Reinforcement Audio/DG/2G-4-squash-you-add-4-now-8",
			"2G-Reinforcement Audio/DG/2G-5-plantain-you-add-3-now-8",
			"2G-Reinforcement Audio/DG/2G-5-dragonfruit-you-add-4-now-9",
			"2G-Reinforcement Audio/DG/2G-6-avocado-you-add-3-now-9",
			"2G-Reinforcement Audio/DG/2G-6-zucchini-you-add-4-now-10",
			"2G-Reinforcement Audio/DG/2G-7-squash-you-add-3-now-10",
			"2G-Reinforcement Audio/DDCD/2G-1-supply-you-add-5-now-6",
			"2G-Reinforcement Audio/DDCD/2G-1-supply-you-add-6-now-7",
			"2G-Reinforcement Audio/DDCD/2G-1-supply-you-add-6-now-7",
			"2G-Reinforcement Audio/DDCD/2G-1-supply-you-add-8-now-9",
			"2G-Reinforcement Audio/DDCD/2G-1-supply-you-add-9-now-10",
			"2G-Reinforcement Audio/DDCD/2G-2-supply-you-add-5-now-7",
			"2G-Reinforcement Audio/DDCD/2G-2-supply-you-add-6-now-8",
			"2G-Reinforcement Audio/DDCD/2G-2-supply-you-add-7-now-9",
			"2G-Reinforcement Audio/DDCD/2G-2-supply-you-add-8-now-10",
			"2G-Reinforcement Audio/DDCD/2G-3-supply-you-add-5-now-8",
			"2G-Reinforcement Audio/DDCD/2G-3-supply-you-add-6-now-9",
			"2G-Reinforcement Audio/DDCD/2G-3-supply-you-add-7-now-10",
			"2G-Reinforcement Audio/DDCD/2G-4-supply-you-add-5-now-9",
			"2G-Reinforcement Audio/DDCD/2G-4-supply-you-add-6-now-10",
			"2G-Reinforcement Audio/DDCD/2G-5-supply-you-add-5-now-10",
			"2G-Inactivity Reinforcement Audio/TTEG/2G-4-corn-i-add-2-now-6",
			"2G-Inactivity Reinforcement Audio/TTEG/2G-5-chilipepper-i-add-1-now-6",
			"2G-Inactivity Reinforcement Audio/TTEG/2G-5-tomato-i-add-2-now-7",
			"2G-Inactivity Reinforcement Audio/TTEG/2G-6-onion-i-add-1-now-7",
			"2G-Inactivity Reinforcement Audio/TTEG/2G-6-pomegranate-i-add-2-now-8",
			"2G-Inactivity Reinforcement Audio/TTEG/2G-7-apple-i-add-1-now-8",
			"2G-Inactivity Reinforcement Audio/TTEG/2G-7-pineapple-i-add-2-now-9",
			"2G-Inactivity Reinforcement Audio/TTEG/2G-8-chilipepper-i-add-1-now-9",
			"2G-Inactivity Reinforcement Audio/TTEG/2G-8-tomato-i-add-2-now-10",
			"2G-Inactivity Reinforcement Audio/TTEG/2G-9-onion-i-add-1-now-10",
			"2G-Inactivity Reinforcement Audio/DG/2G-2-avocado-i-add-4-now-6",
			"2G-Inactivity Reinforcement Audio/DG/2G-3-chayote-i-add-3-now-6",
			"2G-Inactivity Reinforcement Audio/DG/2G-3-jicama-i-add-4-now-7",
			"2G-Inactivity Reinforcement Audio/DG/2G-4-zucchini-i-add-3-now-7",
			"2G-Inactivity Reinforcement Audio/DG/2G-4-squash-i-add-4-now-8",
			"2G-Inactivity Reinforcement Audio/DG/2G-5-plantain-i-add-3-now-8",
			"2G-Inactivity Reinforcement Audio/DG/2G-5-dragonfruit-i-add-4-now-9",
			"2G-Inactivity Reinforcement Audio/DG/2G-6-avocado-i-add-3-now-9",
			"2G-Inactivity Reinforcement Audio/DG/2G-6-zucchini-i-add-4-now-10",
			"2G-Inactivity Reinforcement Audio/DG/2G-7-squash-i-add-3-now-10",
			"2G-Inactivity Reinforcement Audio/DDCD/2G-1-supply-i-add-5-now-6",
			"2G-Inactivity Reinforcement Audio/DDCD/2G-1-supply-i-add-6-now-7",
			"2G-Inactivity Reinforcement Audio/DDCD/2G-1-supply-i-add-7-now-8",
			"2G-Inactivity Reinforcement Audio/DDCD/2G-1-supply-i-add-8-now-9",
			"2G-Inactivity Reinforcement Audio/DDCD/2G-1-supply-i-add-9-now-10",
			"2G-Inactivity Reinforcement Audio/DDCD/2G-2-supply-i-add-5-now-7",
			"2G-Inactivity Reinforcement Audio/DDCD/2G-2-supply-i-add-6-now-8",
			"2G-Inactivity Reinforcement Audio/DDCD/2G-2-supply-i-add-7-now-9",
			"2G-Inactivity Reinforcement Audio/DDCD/2G-2-supply-i-add-8-now-10",
			"2G-Inactivity Reinforcement Audio/DDCD/2G-3-supply-i-add-5-now-8",
			"2G-Inactivity Reinforcement Audio/DDCD/2G-3-supply-i-add-6-now-9",
			"2G-Inactivity Reinforcement Audio/DDCD/2G-3-supply-i-add-7-now-10",
			"2G-Inactivity Reinforcement Audio/DDCD/2G-4-supply-i-add-5-now-9",
			"2G-Inactivity Reinforcement Audio/DDCD/2G-4-supply-i-add-6-now-10",
			"2G-Inactivity Reinforcement Audio/DDCD/2G-5-supply-i-add-5-now-10",
			"User-Earns-Artifact"
		};
	}
	
	public class AdditionToTenTarg : MovieClip
	{
		private MovieClip target;
		
		public AdditionToTenTarg()
		{
			target = MovieClipFactory.CreateAdditionUpToTenTarget();
			reset ();
			addChild(target);
		}
		
		public void reset()
		{
			target.gotoAndStop(1);
		}
		
		public void setNumber( int answer )
		{
			target.gotoAndStop(2);
			MovieClip targ = target.getChildByName<MovieClip>( "target" );
			targ = targ.getChildAt<MovieClip>( targ.numChildren - 1 );
			targ.gotoAndStop( answer );
		}
	}
	
	public class AddtoTenOpp
	{
		public int basketNum;
		public int pileNum;
		public string basketType;
		public string pileType;
		public bool upsizePile;
		
		public AddtoTenOpp( int _basketNum, int _pileNum, string _basketName )
		{ basketNum = _basketNum; pileNum = _pileNum; basketType = _basketName; pileType = _basketName; upsizePile = false; }
		
		public AddtoTenOpp( int _basketNum, int _pileNum, string _basketName, bool _upsizePile )
		{ basketNum = _basketNum; pileNum = _pileNum; basketType = _basketName; pileType = _basketName; upsizePile = _upsizePile; }
		
		public AddtoTenOpp( int _basketNum, int _pileNum, string _basketName, string _pileName )
		{ basketNum = _basketNum; pileNum = _pileNum; basketType = _basketName; pileType = _pileName; upsizePile = true; }
	}
}
