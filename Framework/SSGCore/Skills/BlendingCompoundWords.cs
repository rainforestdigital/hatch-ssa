using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.geom;

namespace SSGCore
{

	public class BlendingCompoundWords : BaseSkill
	{
		
		public enum BlendingCompoundWordsStates //The state of the game - a representation of the flow
		{ 
			INTRO = 0, //The intial state - the theme's intro
			TUTORIAL, //The game is currently in a tutorial state, all matters handled by the tutorial
			INITIAL_INSTRUCTION, //The instructions given to the player at the beggining of the skill
			SETUP_AND_PROMPT, //The prompt for the current compound word
			PLAYER_INTERACTION,  //Waiting for the Player interaction
			PLAYER_INTERACTING, //They're doing somethings
			SOLVE_FOR_PLAYER, //game solves the current opportunity for the player
			PLAYER_REINFORCEMENT, //Responses to the answer
			PLAYER_THEME_REWARD, //The reward + animations from the theme
			PLAYER_WRONG, //Responses and theme events if the player got the opportunity incorrect
			END_THEME //Player has gone through all opporutnities			
		};
		
		//Constants------------------------------------------------------
		
		//FLA Constants
		public const string COMPOUND_CARD_INSTANCE = "compoundWordBlending";
		public const string PUZZLE_LEFT_INSTANCE = "puzzlePieceLeftBlending";
		public const string PUZZLE_RIGHT_INSTANCE = "puzzlePieceRightBlending";
		public const string PUZZLE_PICTURES = "wordPictures";
		
		//Audio Word Constants
		public const string INITIAL_INSTRUCTION = "INITIAL_INSTRUCTION";
		public const string PROMPT = "PROMPT";
		public const string DETAILED_PROMPT = "DETAILED_PROMPT"; //word sent during the emerging prompt
		public const string CORRECT_REINFORCEMENT = "REINFORCEMENT";
		public const string INCORRECT_REINFORCEMENT = "INCORRECT_REINFORCEMENT";
		public const string PLAYER_INACTIVITY = "PLAYER_INACTIVITY"; //message sent when player has done nothing
		public const string SOLVE_FOR_PLAYER = "SOLVE_FOR_PLAYER"; //String sent 
		
		//Movie clip placements (All coordinates are percentages of the parent)
		private const float BOX_X_PERCENT = .5f; // Center of the box relative to the screen
		private const float BOX_Y_PERCENT = .3f;
		
		private const float ANSWER_PIECE_Y = .725f; //the height at which the answer pieces are placed. The answer pieces are placed based off of the center of rhte screen
		private const float ANSWER_SPACING = 35; //The spacing between the answer pieces
		
		//Timing constants
		private const float FADE_TIME = .35f;
		private const float DETAILED_PROMPT_TRANSITION_TIME = .5f;
		
		//Misc
		public const float MAGNIFY_SCALE_PRECENT = 1.25f; //When an object is highlighted and enlarged, the percent that it is scaled up
		public const float MAGNIFY_REVERT_PERCENT = .8f; //undoes the scaling from the highlight
		
		bool solveForPlayerBool = false; //If the game solved for the player, flags to move on to the next opportunity after "Try Again"
		
		//Game Data-------------------------------------------------------
		//Format = List of compound words
		//Compound words are in the form of a list with the full word first and the words that make up the word second and thrid
		private List<List<string>> compoundWords = new List<List<string>>()
		{
			new List<string>(){"BASEBALL", "BASE", "BALL"},
			new List<string>(){"BASKETBALL", "BASKET", "BALL"},
			new List<string>(){"BIRDCAGE", "BIRD", "CAGE"},
			new List<string>(){"BIRDHOUSE", "BIRD", "HOUSE"},
			new List<string>(){"BOATHOUSE", "BOAT", "HOUSE"},
			new List<string>(){"BOOKCASE", "BOOK", "CASE"},
			new List<string>(){"BOWTIE", "BOW", "TIE"},
			new List<string>(){"BOXCAR", "BOX", "CAR"},
			new List<string>(){"BULLHORN", "BULL", "HORN"},
			new List<string>(){"BUTTERCUP", "BUTTER", "CUP"},
			new List<string>(){"BUTTERFLY", "BUTTER", "FLY"},
			new List<string>(){"CHALKBOARD", "CHALK", "BOARD"},
			new List<string>(){"COWBOY", "COW", "BOY"},
			new List<string>(){"CUPCAKE", "CUP", "CAKE"},
			new List<string>(){"DOGHOUSE", "DOG", "HOUSE"},
			new List<string>(){"EARRING", "EAR", "RING"},
			new List<string>(){"FIREHOUSE", "FIRE", "HOUSE"},
			new List<string>(){"FIREMAN", "FIRE", "MAN"},
			new List<string>(){"FIREWOOD", "FIRE", "WOOD"},
			new List<string>(){"FISHBOWL", "FISH", "BOWL"},
			new List<string>(){"FOOTBALL", "FOOT", "BALL"},
			new List<string>(){"GOLDFISH", "GOLD", "FISH"},
			new List<string>(){"HOUSEBOAT", "HOUSE", "BOAT"},
			new List<string>(){"KEYHOLE", "KEY", "HOLE"},
			new List<string>(){"LADYBUG", "LADY", "BUG"},
			new List<string>(){"LIGHTHOUSE", "LIGHT", "HOUSE"},
			new List<string>(){"LIPSTICK", "LIP", "STICK"},
			new List<string>(){"MAILBOX", "MAIL", "BOX"},
			new List<string>(){"PANCAKE", "PAN", "CAKE"},
			new List<string>(){"PEANUT", "PEA", "NUT"},
			new List<string>(){"POCKETBOOK", "POCKET", "BOOK"},
			new List<string>(){"RAINBOW", "RAIN", "BOW"},
			new List<string>(){"RATTLESNAKE", "RATTLE", "SNAKE"},
			new List<string>(){"SANDPAPER", "SAND", "PAPER"},
			new List<string>(){"SAWHORSE", "SAW", "HORSE"},
			new List<string>(){"SKYSCRAPER", "SKY", "SCRAPER"},
			new List<string>(){"SNOWBALL", "SNOW", "BALL"},
			new List<string>(){"SNOWMAN", "SNOW", "MAN"},
			new List<string>(){"STARFISH", "STAR", "FISH"},
			new List<string>(){"SUNFLOWER", "SUN", "FLOWER"},
			new List<string>(){"SUNGLASSES", "SUN", "GLASSES"},
			new List<string>(){"SURFBOARD", "SURF", "BOARD"},
			new List<string>(){"TURTLENECK", "TURTLE", "NECK"},
			new List<string>(){"WALLPAPER", "WALL", "PAPER"},
			new List<string>(){"WHEELCHAIR", "WHEEL", "CHAIR"}			
		};
		
		
		//GameManagement
		private BlendingCompoundWordsStates currentState = BlendingCompoundWordsStates.INTRO;
		
		//Word/Answer Variables
		public List<string> currentCompoundWord; //The compound word with info on the words that make it up
		public List<List<string>> compoundWordOpportunityBank; //at beginning of skill, compound words coppied into here and are removed as they are used
		public List<List<string>> randomCompoundWordBank; //at beginning of skill, random words coppied into here and are removed as they are used
		public string correctWordAnswer; //The correct answer the player will have to choose
		private int numberOfPuzzlePieces; //The number of puzzle pieces per opportunity
		
		private List<FilterMovieClip> answerOptionsCompoundCards = new List<FilterMovieClip>(); //The answers for the player to choose from
		public List<FilterMovieClip> AnswerOptionsPuzzlePieces { get{ return answerOptionsCompoundCards; } }
		
		private List<FilterMovieClip> highlightedFilterMovieClips = new List<FilterMovieClip>(); //Tracks which FMC's are highlighted
		
		//Visuals Variables - Movieclips and Containers
		public DisplayObjectContainer backgroundContainer; //Table Background object
		private MovieClip answerBox; //Contains the box, the compound word card, the dotted outline, and the hint puzzle piece
		public FilterMovieClip compoundWordCard; //The card for the compound word prompt
		
		public FilterMovieClip puzzlePieceLeft;
		MovieClip puzzlePieceLeftPictures; //the pictures are childed to the puzzle pieces so I am saving a reference to that movieclip
		
		public FilterMovieClip puzzlePieceRight;
		MovieClip puzzlePieceRightPictures;
		
		//Misc
		private int stepCounter = 0; //Counts the steps in a linear process. Used with emergingPrompt, inactivity counts, and solving for player
		public int tutorialSetupCounter = 0; //Keeps track of which part of the tutorial to set up
		private Vector2 detailedPromptCardDestination = new Vector2(); //When centering and highlighting during emerging prompt, records its original position
		
		private FilterMovieClip draggedCompoundCard;
		private Vector2 draggedCompoundCardStartLocation; //the location of the puzzle piece when it is clicked so that it can go back if not on answer spot
		private float draggedScale;
		
		private float baseScale;
		private float refWidth;
		private float refHeight;
		
		private TimerObject glowTimer;
		
		private new BlendingCompoundWordsTutorial tutorial_mc;
		
		public BlendingCompoundWords( SkillLevel level, SessionInfo  currentSession ) : base( level, currentSession )
		{
			BACKGROUND = "Background";
			ARTIFACT = "Artifact";

			Init (null);
		}
		
		public override void Dispose ()
		{
			base.Dispose();
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			if(tutorial_mc != null)
				tutorial_mc.Dispose();
			foreach(FilterMovieClip fmc in answerOptionsCompoundCards)
			{
				fmc.Destroy();
			}
			compoundWordCard.Destroy();
			puzzlePieceLeft.Destroy();
			puzzlePieceRight.Destroy();
			if(draggedCompoundCard != null)
				draggedCompoundCard.Destroy();
			if(glowTimer != null)
				glowTimer.Unload();
			if(stage.hasEventListener(MouseEvent.MOUSE_UP))
				stage.removeEventListener( MouseEvent.MOUSE_UP, OnPieceUp );
		}
	
		private void PreCacheAssets()
		{
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle("BlendingCompoundWords", OnAssetsLoaded));
		}
		
		void OnAssetsLoaded(AssetBundle bundle)
		{
			if( bundle != null )
			{
				Init( null );
			}
		}
		
		public override void Init (MovieClip parent)
		{
			base.Init (parent);
			
			//MovieClip backgroundImage = MovieClipFactory.CreateBlendingCompoundWordsBackground();


			if( PlatformUtils.GetPlatform() == Platforms.WIN )
			{
				refWidth = 1920;
				refHeight = 1080;
			}
			else if( PlatformUtils.GetPlatform() == Platforms.IOSRETINA )
			{
				refWidth = 2048;
				refHeight = 1536;
			}
			else if( PlatformUtils.GetPlatform() == Platforms.ANDROID )
			{
				refWidth = 1024;
				refHeight = 600;
			}
			else
			{
				refWidth = 1280;
				refHeight = 720;
			}

			baseScale = ((float)Screen.height / refHeight);
						
			Debug.LogWarning( "PlatformUtils.GetPlatform(): " + PlatformUtils.GetPlatform() + ", refWidth : " + refWidth + ", refHeight: " + refHeight );

			//Add our OnAudioWordComplete to the delegate
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			//Setup Background
			backgroundContainer = new DisplayObjectContainer();
			addChild(backgroundContainer);	
						
			//add and center background image
			/*backgroundContainer.addChild(backgroundImage);
			backgroundImage.x = 0;
			backgroundImage.y = 0;
			//if(_level == SkillLevel.Tutorial)
			backgroundImage.alpha = 0;*/
			
			//Setup Answer Box
			//Setup the Container
			answerBox = MovieClipFactory.CreateEmptyMovieClip();
			//answerBox.gotoAndStop(2); //the second frame is blending
			backgroundContainer.addChild(answerBox); //add to background so all an be faded out together
			//I don't know why I have to do this, but it updates the size of the background image
			//So these two lines aren't ENTIRELY pointless. Just stupid. I don't know, just leave 'em.
			backgroundContainer.removeChild(answerBox);
			backgroundContainer.addChild(answerBox);

			if(PlatformUtils.GetPlatform() == Platforms.WIN)
			{
				answerBox.x = 178.3f;
				answerBox.y = 212.3f;       
			}
			else if(PlatformUtils.GetPlatform() == Platforms.IOSRETINA)
			{
				answerBox.x = 260;
				answerBox.y = 420;
			}
			else if(PlatformUtils.GetPlatform() == Platforms.ANDROID)
			{
				answerBox.x = 90;
				answerBox.y = 120;
			}
			else 
			{
				answerBox.x = 200;
				answerBox.y = 180;
			}
			
			//answerBox.x = ( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA ) ? 178.3f : 200;//refWidth * BOX_X_PERCENT - answerBox.width * .5f;
			//answerBox.y = ( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA ) ? 212.3f : 180;//refHeight * BOX_Y_PERCENT - answerBox.height * .5f;
			
			MovieClip answerBoxItems = MovieClipFactory.CreateBlendingCompoundWordsBoxItems();

			Debug.Log("Num of box children: " + answerBoxItems.numChildren);
			for(int i = 0 ; i < answerBoxItems.numChildren ; i++)
			{
				Debug.Log("Child " + i + ": " + answerBoxItems.getChildAt(i).name);
			}
						
			//Get the compound word from the answer box
			MovieClip tempMCRef = answerBoxItems.getChildByName<MovieClip>(COMPOUND_CARD_INSTANCE);
			Vector2 tempLocalPosition = new Vector2(tempMCRef.x, tempMCRef.y); //record position to reset target's local position once added to FMC
			compoundWordCard = new FilterMovieClip(tempMCRef); //Setup the compound word
			answerBox.addChild(compoundWordCard); //add the new filtered movie clip
			compoundWordCard.x = tempLocalPosition.x; //Set the FMC to the card's orignal position
			compoundWordCard.y = tempLocalPosition.y;
			compoundWordCard.Target.x = 0; //zero out the target's local position
			compoundWordCard.Target.y = 0;
			compoundWordCard.Target.getChildByName<MovieClip>(COMPOUND_CARD_INSTANCE).gotoAndStop(1);
			compoundWordCard.alpha = 0;
						
			tempMCRef = answerBoxItems.getChildByName<MovieClip>(PUZZLE_RIGHT_INSTANCE);
			tempLocalPosition = new Vector2(tempMCRef.x, tempMCRef.y); //record position to reset target's local position once added to FMC
			puzzlePieceRight = new FilterMovieClip(tempMCRef);
			puzzlePieceRightPictures = puzzlePieceRight.Target.getChildByName<MovieClip>(PUZZLE_RIGHT_INSTANCE).getChildByName<MovieClip>(PUZZLE_PICTURES);
			answerBox.addChild(puzzlePieceRight);
			puzzlePieceRight.x = tempLocalPosition.x; //Set the FMC to the card's orignal position
			puzzlePieceRight.y = tempLocalPosition.y;
			puzzlePieceRight.Target.x = 0; //zero out the target's local position
			puzzlePieceRight.Target.y = 0;
			puzzlePieceRight.width = compoundWordCard.Target.width; //Use the compound word card for consistency since other pieces are different sizes
			puzzlePieceRight.height = compoundWordCard.Target.height;
			puzzlePieceRightPictures.gotoAndStop(1);
			puzzlePieceRight.alpha = 0;

			//Setup puzzle pieces - get puzzle pieces from the box, get reference to picture mc child, set frame, hide them
			tempMCRef = answerBoxItems.getChildByName<MovieClip>(PUZZLE_LEFT_INSTANCE);
			tempLocalPosition = new Vector2(tempMCRef.x, tempMCRef.y); //record position to reset target's local position once added to FMC
			puzzlePieceLeft = new FilterMovieClip(tempMCRef);
			puzzlePieceLeftPictures = puzzlePieceLeft.Target.getChildByName<MovieClip>(PUZZLE_LEFT_INSTANCE).getChildByName<MovieClip>(PUZZLE_PICTURES); //Get Reference
			answerBox.addChild(puzzlePieceLeft);
			puzzlePieceLeft.x = tempLocalPosition.x; //Set the FMC to the card's orignal position
			puzzlePieceLeft.y = tempLocalPosition.y;
			puzzlePieceLeft.Target.x = 0; //zero out the target's local position
			puzzlePieceLeft.Target.y = 0;
			puzzlePieceLeft.width = compoundWordCard.Target.width; //Use the compound word card for consistency since other pieces are different sizes
			puzzlePieceLeft.height = compoundWordCard.Target.height;
			puzzlePieceLeftPictures.gotoAndStop(1); //Stop the movie clip from playing
			puzzlePieceLeft.alpha = 0;
			
			backgroundContainer.alpha = 0; //Hide background
						
			//Load tutorial and set it's reference
			tutorial_mc = new BlendingCompoundWordsTutorial();
			tutorial_mc.skillRef = this;
			
			//Set state to theme intro
			currentState = BlendingCompoundWordsStates.INTRO;
			
			//Copy lists of compound words and random words
			compoundWordOpportunityBank = new List<List<string>>(compoundWords);
			randomCompoundWordBank = new List<List<string>>(compoundWords);
			
			//Set the number of opportunities
			if(_level == SkillLevel.Tutorial)
			{
				totalOpportunities = 1;
				FadeOutContainer();
			}
			else totalOpportunities = 10;
			
			//Set the number of options
			switch(_level)
			{
			case SkillLevel.Tutorial: 
				numberOfPuzzlePieces = 1;
				break;
			case SkillLevel.Emerging:
				numberOfPuzzlePieces = 3;
				break;
			case SkillLevel.Developing:
				numberOfPuzzlePieces = 4;
				break;
			case SkillLevel.Developed: //Developed and completed both have 5 answers per opportunity
			case SkillLevel.Completed:
				numberOfPuzzlePieces = 5;
				break;
			}
			
			if(resumingSkill)
			{
				parseSessionData(currentSession.sessionData);
			//	nextOpportunity();
			}
			
			//Set timer length
			totalTime = 5;

			backgroundContainer.scaleX = backgroundContainer.scaleY = baseScale;
			backgroundContainer.x = (Screen.width - (refWidth*baseScale))/2;

		}
		
		public void ChangeState(BlendingCompoundWordsStates newState)
		{
			Debug.Log("STATE CHANGE: " + currentState.ToString() + " =to=> " + newState.ToString());
			
			ExitState(currentState, newState);

			EnterState(currentState, newState);
			
			//Change State
			currentState = newState;
		}
	
		private void ExitState(BlendingCompoundWordsStates oldState, BlendingCompoundWordsStates newState)
		{
			switch(oldState)
			{
			case BlendingCompoundWordsStates.INTRO:
			case BlendingCompoundWordsStates.TUTORIAL:
			case BlendingCompoundWordsStates.INITIAL_INSTRUCTION:
			case BlendingCompoundWordsStates.SETUP_AND_PROMPT:
				break;
				
			case BlendingCompoundWordsStates.PLAYER_INTERACTION:
			case BlendingCompoundWordsStates.PLAYER_INTERACTING:
				//Interaction over, disable interactions
				foreach(FilterMovieClip fmc in answerOptionsCompoundCards)
				{
					fmc.removeEventListener( MouseEvent.MOUSE_DOWN, OnPieceDown );
				}
				puzzlePieceLeft.removeEventListener(MouseEvent.MOUSE_DOWN, OnBoxPieceDown);
				puzzlePieceRight.removeEventListener(MouseEvent.MOUSE_DOWN, OnBoxPieceDown);
				
				if(newState != BlendingCompoundWordsStates.PLAYER_INTERACTING)
					MarkInstructionStart();
				stopTimer();
				break;
				
			case BlendingCompoundWordsStates.SOLVE_FOR_PLAYER:
			case BlendingCompoundWordsStates.PLAYER_REINFORCEMENT:
			case BlendingCompoundWordsStates.PLAYER_THEME_REWARD:
			case BlendingCompoundWordsStates.PLAYER_WRONG:
			case BlendingCompoundWordsStates.END_THEME:
				break;
			}
			
		}
		
		private void EnterState(BlendingCompoundWordsStates oldState, BlendingCompoundWordsStates newState)
		{
			switch(newState)
			{
				
			case BlendingCompoundWordsStates.INTRO:
				//Game is set up to be in the Intro state. No enter state.
				break;
				
			case BlendingCompoundWordsStates.TUTORIAL:	
				//Show background
				FadeInContainer();
				//Start Tutorial
				tutorial_mc.OnStart();
				break;
			
			case BlendingCompoundWordsStates.INITIAL_INSTRUCTION:
				if( !currentSession.skipIntro )
				{
					//String together the audio prompts
					SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
					AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS, "Narration/1C-Story Introduction Audio/1C-Story-Introduction" );
					bundle.AddClip(clip, INITIAL_INSTRUCTION, clip.length);
					SoundEngine.Instance.PlayBundle( bundle );
				}
				else
					OnAudioWordComplete( INITIAL_INSTRUCTION );

				//Fade in Background
				FadeInContainer();
				
				break;
				
			case BlendingCompoundWordsStates.SETUP_AND_PROMPT:		

				SetUpOpportunity();
				FadeInContainer();
				DetailedWordPrompt();
				
				break;
				
			case BlendingCompoundWordsStates.PLAYER_INTERACTION:	
				//add event listeners and enable interactions
				foreach(FilterMovieClip fmc in answerOptionsCompoundCards)
				{
					if(!fmc.hasEventListener(MouseEvent.MOUSE_DOWN))
						fmc.addEventListener( MouseEvent.MOUSE_DOWN, OnPieceDown );
				}
				if(!puzzlePieceLeft.hasEventListener(MouseEvent.MOUSE_DOWN))
					puzzlePieceLeft.addEventListener(MouseEvent.MOUSE_DOWN, OnBoxPieceDown);
				if(!puzzlePieceRight.hasEventListener(MouseEvent.MOUSE_DOWN))
					puzzlePieceRight.addEventListener(MouseEvent.MOUSE_DOWN, OnBoxPieceDown);
				if(_level != SkillLevel.Tutorial)
					MarkInstructionEnd();
				if(oldState != BlendingCompoundWordsStates.PLAYER_INTERACTING && _level != SkillLevel.Tutorial)
					startTimer(); //Starts inactivty timer
				break;
				
			case BlendingCompoundWordsStates.PLAYER_INTERACTING:
				SoundEngine.Instance.SendCancelToken();
				timeoutCount = 0;
				stopTimer();
				if(_level == SkillLevel.Tutorial)
				{
					tutorial_mc.InactivityTimerOff();
					tutorial_mc.inactivityCounter = 0;
				}
				break;
				
			case BlendingCompoundWordsStates.SOLVE_FOR_PLAYER:
				SolveForPlayer();
				break;
				
			case BlendingCompoundWordsStates.PLAYER_REINFORCEMENT:	
				//Check the dragged puzzle piece
				if(draggedCompoundCard.name == correctWordAnswer) //Player is correct and should be rewarded
				{
					//Mark opportunity answered, correct and complete
					if(opportunityAnswered == false) opportunityCorrect = true;
					opportunityAnswered = true;
					
					opportunityComplete = true;
					
					//OnAudioWordComplete will respond to CLICK_CORRECT and carry on the rest once sound is done.
					if(oldState != BlendingCompoundWordsStates.SOLVE_FOR_PLAYER) PlayCorrectSound(); 
					else PlayCorrectAnswerReinforcement(); //Problem was solved for the player so it must be treated as incorrect
				}
				else //Player is wrong, critisize them
				{
					//Mark opportunity answered and incorrect
					opportunityCorrect = false;
					opportunityAnswered = true;
					
					PlayIncorrectSound(); //OnAudioWordComplete will respond to CLICK_INCORRECT and carry on the rest once sound is done.
				}
				break;
				
			case BlendingCompoundWordsStates.PLAYER_THEME_REWARD:
				if(opportunityCorrect) //opportunity was answered correctly the first time, log it, and give the player a reward
				{
					//Fade Out Background
					FadeOutContainer();
					
					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
				}
			
				else //opportunity was not answered correctly the first time, log it and go to the next opportunity
				{
					FadeOutContainer();
					/*
					if(currentOpportunity == totalOpportunities)
						FadeOutContainer();
						*/

					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
					return;
				}
				break;
				
			case BlendingCompoundWordsStates.PLAYER_WRONG:	
				
				break;
				
			case BlendingCompoundWordsStates.END_THEME:	
				tallyAnswers();
				break;
			}
		}
		
		public void SetUpOpportunity()
		{
			Debug.Log("SETTING UP OPPORTUNITY!");
			//clear out the options
			foreach(FilterMovieClip fmc in answerOptionsCompoundCards)
			{
				fmc.Destroy();
				Tweener.removeTweens(fmc);
				backgroundContainer.removeChild(fmc);
			}
			if( draggedCompoundCard != null)
			{
				draggedCompoundCard.Destroy();
				if(draggedCompoundCard.parent != null)
					draggedCompoundCard.parent.removeChild(draggedCompoundCard);
				draggedCompoundCard = null;
			}
			answerOptionsCompoundCards.Clear ();
			
			//if not tutorial, randomly choose a word
			if(_level != SkillLevel.Tutorial)
			{
				int r = Random.Range(0, compoundWordOpportunityBank.Count);
				if(r == compoundWordOpportunityBank.Count) r--;
				if(resumingSkill)
				{
					r = 0;
					resumingSkill = false;
				}
				currentCompoundWord = compoundWordOpportunityBank[r];
				compoundWordOpportunityBank.Remove(currentCompoundWord);
				compoundWordCard.Target.getChildByName<MovieClip>(COMPOUND_CARD_INSTANCE).gotoAndStop(currentCompoundWord[0]);
			}
			else if(tutorialSetupCounter == 0) //first half of tutorial, setup doghouse
			{
				//Word is doghouse and dog is given
				foreach( List<string> compoundWordList in compoundWordOpportunityBank)
				{
					//Find doghouse in list and remove it
					if(compoundWordList[0] == "DOGHOUSE")
					{
						currentCompoundWord = compoundWordList;
						compoundWordCard.Target.getChildByName<MovieClip>(COMPOUND_CARD_INSTANCE).gotoAndStop(currentCompoundWord[0]);
						break;
					}
				}
				tutorialSetupCounter = 1;
			}
			else //Must be on second half of the tutorial, setup up birdcage
			{
				//Word is birdcage and bird is given
				foreach( List<string> compoundWordList in compoundWordOpportunityBank)
				{
					//Find birdcage in list and remove it
					if(compoundWordList[0] == "BIRDCAGE")
					{
						currentCompoundWord = compoundWordList;
						compoundWordCard.Target.getChildByName<MovieClip>(COMPOUND_CARD_INSTANCE).gotoAndStop(currentCompoundWord[0]);
						break;
					}
				}
				tutorialSetupCounter = 2;
			}
				
			Debug.Log("The current Compound Word is: " + currentCompoundWord[0] + " ::: " + currentCompoundWord[1] + " - " + currentCompoundWord[2]);
			
			//Setup answers
			correctWordAnswer = currentCompoundWord[0];
			
			puzzlePieceLeftPictures.gotoAndStop(currentCompoundWord[1]);
			puzzlePieceLeft.name = currentCompoundWord[1];
			puzzlePieceRightPictures.gotoAndStop(currentCompoundWord[2]);
			puzzlePieceRight.name = currentCompoundWord[2];
			compoundWordCard.alpha = 0; //hide the compound word piece	
			
			if(_level == SkillLevel.Tutorial && tutorialSetupCounter != 2)
			{
				//puzzlePieceLeft.alpha = 1;
				//puzzlePieceRight.alpha = 1;
			}
			else
			{
				puzzlePieceLeft.alpha = 0;
				puzzlePieceRight.alpha = 0;
			}

			//Set the compound word card
			compoundWordCard.Target.getChildByName<MovieClip>(COMPOUND_CARD_INSTANCE).gotoAndStop(currentCompoundWord[1]);

			//Set up the answer pieces
			//Calculate the initial x position for the pieces - get width of card based on random outcome
			//total width of placement, using compound word card as an average width
			float totalWidth = (numberOfPuzzlePieces * (compoundWordCard.Target.width + ANSWER_SPACING)) - ANSWER_SPACING;
			
			//Cut the width of the list so it can't spill off the edges, mostly needed for 4:3
			/*float maxWidth = ((float)Screen.width / baseScale) - ANSWER_SPACING;
			if(totalWidth > maxWidth)
			{
				draggedScale = maxWidth / totalWidth;
				totalWidth = maxWidth;
			}
			else
				draggedScale = 1f;

			*/

			draggedScale = 1;

			//the starting position from the center of the card
			//Note - I use the compound word card as a width reference because the puzzle pieces have different width due to off center childed movie clips to line up pictures
			float startingX = refWidth * .5f - totalWidth * .5f;

			//Create  a selection of words with the correct answer and random options
			List<string> answerWords = new List<string>();
			answerWords.Add(correctWordAnswer); //add correct answer

			int randomIndex;
			while(answerWords.Count < numberOfPuzzlePieces)
			{
				randomIndex = Random.Range(0, randomCompoundWordBank.Count);
				if(randomIndex == randomCompoundWordBank.Count) randomIndex--;
				if(!answerWords.Contains(randomCompoundWordBank[randomIndex][0]) &&
					correctWordAnswer != randomCompoundWordBank[randomIndex][0]) //the random word has not been used in this opportunity
				{
					answerWords.Add (randomCompoundWordBank[randomIndex][0]);			
				}
			}
			
			answerWords.Shuffle();

			FilterMovieClip tempFilterMovieClip;
			float xTally = 0; //the running total to add to the puzzle piece positions
			float yOffset = 0;

			if( PlatformUtils.GetPlatform() == Platforms.WIN ) yOffset = 50;
			else if( PlatformUtils.GetPlatform() == Platforms.IOSRETINA ) yOffset = 30;
			else if( PlatformUtils.GetPlatform() == Platforms.ANDROID ) yOffset = 30;

			//create and place a puzzle piece for each
			for(int i = 0 ; i < answerWords.Count ; ++i)
			{
				//Create a card
				tempFilterMovieClip = new FilterMovieClip(MovieClipFactory.CreateBlendingCompoundWordsCompoundWords());

				//Set the picture
				tempFilterMovieClip.Target.getChildByName<MovieClip>(COMPOUND_CARD_INSTANCE).gotoAndStop(answerWords[i]);

				//set the name to reference later
				tempFilterMovieClip.name = answerWords[i];

				//Place the piece
				tempFilterMovieClip.scaleX = tempFilterMovieClip.scaleY = draggedScale;
				tempFilterMovieClip.x = startingX + xTally;
				tempFilterMovieClip.y = (refHeight * ANSWER_PIECE_Y - compoundWordCard.Target.height * .5f) + yOffset;
				xTally += (compoundWordCard.Target.width + ANSWER_SPACING) * draggedScale;

				//Add the filters to a list and add them to the background
				answerOptionsCompoundCards.Add (tempFilterMovieClip);
				backgroundContainer.addChild(tempFilterMovieClip);
				
				if(_level != SkillLevel.Tutorial || tutorialSetupCounter == 2 || tutorialSetupCounter == 0)
					tempFilterMovieClip.alpha = 0;
			}

			if ((_level == SkillLevel.Tutorial))
			{
				AnswerOptionsPuzzlePieces[0].alpha = 0f;
			}
		}
		
		public override void nextOpportunity()
		{	
			//If skill is done, end theme
			if(_currentOpportunity == totalOpportunities) //Player has finished all opportunities, end theme
			{
				ChangeState(BlendingCompoundWordsStates.END_THEME);
				return;
			}
			else //otherwise, setup next opportunity
			{
				//Reset opportunity variables
				opportunityComplete = false;
				opportunityCorrect = false;
				opportunityAnswered = false;
				
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_START, true, false) );
				ChangeState(BlendingCompoundWordsStates.SETUP_AND_PROMPT);
			}
			
			_currentOpportunity++;
		}
		
		public void setObjectData()
		{
			Vector2 point = puzzlePieceRight.parent.localToGlobal(new Vector2( puzzlePieceRight.x, puzzlePieceRight.y ));
			AddNewObjectData( formatObjectData( puzzlePieceRight.name, point ) );
			
			point = puzzlePieceLeft.parent.localToGlobal(new Vector2( puzzlePieceLeft.x, puzzlePieceLeft.y ));
			AddNewObjectData( formatObjectData( puzzlePieceLeft.name, point ) );
			
			foreach( FilterMovieClip fmc in answerOptionsCompoundCards )
			{
				point = fmc.parent.localToGlobal( new Vector2( fmc.x, fmc.y ) );
				AddNewObjectData( formatObjectData( fmc.name, point, fmc.name == correctWordAnswer ) );
			}
		}
		
		
		public override void OnAudioWordComplete( string word )
		{
			switch(word)
			{
			case THEME_INTRO: //The theme has finished its intro
				//Change State to Initial Instruction, or if in tutorial, change to tutorial
				if(_level != SkillLevel.Tutorial) ChangeState(BlendingCompoundWordsStates.INITIAL_INSTRUCTION);
				else ChangeState(BlendingCompoundWordsStates.TUTORIAL);
				break;
				
			case INITIAL_INSTRUCTION: //The intro for the specific skill is complete
				//Start next opportunity which will change the state to PROMPT 
				nextOpportunity();
				break;
				
			case DETAILED_PROMPT:
				DetailedWordPrompt();
				break;
				
			case PROMPT: //Prompt is over, Player Interaction
				ChangeState (BlendingCompoundWordsStates.PLAYER_INTERACTION);
				break;
				
			case CLICK_CORRECT:
				//Request Compliment then reinforce on next audio word
				if(_level != SkillLevel.Tutorial) dispatchEvent(new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
				else tutorial_mc.TutorialActions();
				break;
				
			case THEME_COMPLIMENT:
				//Reinforce player, once that is complete, go to theme state
				if( _level != SkillLevel.Tutorial ) PlayCorrectAnswerReinforcement();
				else tutorial_mc.TutorialActions();
				break;
				
			case CORRECT_REINFORCEMENT:
				if(!solveForPlayerBool)
				{
					//Change to player theme reward - Enter state will handle fade out and calling the reward
					ChangeState (BlendingCompoundWordsStates.PLAYER_THEME_REWARD);
				}
				else
					goto case INCORRECT_REINFORCEMENT;
				break;
				
			case CLICK_INCORRECT:
				//Request criticism then play reinforcement on the word
				//ignore if tutorial
				if(_level != SkillLevel.Tutorial) dispatchEvent(new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
				break;
				
			case THEME_CRITICISM:
				//Reinforce the incorrect answer "wrong word, say wrong word"
				PlayIncorrectAnswerReinforcement();
				break;
				
			case INCORRECT_REINFORCEMENT:
				if(solveForPlayerBool)
				{
					//Move onto the next opportunity instead of looping into the same prompt
					solveForPlayerBool = false;
					//if(currentOpportunity == totalOpportunities)
						FadeOutContainer();
					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
				}
				else
				{
					//Fade out piece while playing "Try Again" // NEIN! Try again doesn't play, edited to go straight to the prompt
					Tweener.addTween(draggedCompoundCard, Tweener.Hash("time", FADE_TIME, "alpha", 0, "transition", Tweener.TransitionType.easeInOutQuint) ).OnComplete(ClearDragged);
					//Play the audio to prompt the player about the compoud word
					goto case TRY_AGAIN;
				}
				//Remove puzzle piece listener
				draggedCompoundCard.Destroy();
				draggedCompoundCard.removeEventListener( MouseEvent.MOUSE_MOVE, OnPieceMove );
				answerOptionsCompoundCards.Remove(draggedCompoundCard); //remove wrong piece from possible answers so listeners don't get reset
				break;
				
			case TRY_AGAIN:
				//"Try Again" has finished playing, play normal prompt again (skip prompt state, no setup requried),
				//then enter back into player interaction state
				//(onAudioWordComplete's reaction to prompt message is to move into player interaction)				
				NormalPrompt();
				break;
				
			case PLAYER_INACTIVITY:
				startTimer();
				break;
				
			case SOLVE_FOR_PLAYER:
				
				break;
				
			default:
				break;
			}
		}
		
		private void ClearDragged ()
		{
			backgroundContainer.removeChild(draggedCompoundCard);
			draggedCompoundCard.Destroy();
			answerOptionsCompoundCards.Remove(draggedCompoundCard);
			draggedCompoundCard = null;
		}
		
		public void OnTutorialComplete()
		{
			opportunityComplete = true;
			opportunityCorrect = tutorial_mc.correct;
			opportunityAnswered = true;
			currentOpportunity = 1;
			FadeOutContainer();
			if(opportunityCorrect)
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
			else
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
		}
		
		//Traverses through each card and piece one at a time and highlights them in the center of the screen, reads them, then moves them into place
		public void DetailedWordPrompt()
		{
			//Audio bundle and clip variables used
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;
			
			//position variable used during calculations
			Vector2 globalToLocalCenter;
			
			if(stepCounter == 0) //0. "Listen to the words and say them after me"
			{
				//Hide pieces
				puzzlePieceLeft.alpha = 0;
				puzzlePieceRight.alpha = 0;
					
				stepCounter++;
				if(_level != SkillLevel.Tutorial)
				{
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS, "Narration/1C-Skill Instructions Audio/1C-listen-to-the-words");
					bundle.AddClip(clip, DETAILED_PROMPT, clip.length);
					SoundEngine.Instance.PlayBundle( bundle );
					return;
				}
			}
			
			if(stepCounter == 1) // Highlight Left word
			{
				//Center and magnify left puzzle piece
				detailedPromptCardDestination = new Vector2( puzzlePieceLeft.x, puzzlePieceLeft.y); //record position to return to
				globalToLocalCenter = answerBox.globalToLocal(new Vector2(Screen.width * .5f, Screen.height * .5f));
				puzzlePieceLeft.x = globalToLocalCenter.x - puzzlePieceLeft.Target.width * .5f;
				puzzlePieceLeft.y = globalToLocalCenter.y - puzzlePieceLeft.Target.height * .5f;
				puzzlePieceLeft.alpha = 1f;
				
				//Magnify and Highlight
				puzzlePieceLeft.ScaleCentered( puzzlePieceLeft.scaleX * MAGNIFY_SCALE_PRECENT);
				HighlightOn(puzzlePieceLeft);
				
				//Play audio for the card
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS,
												"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-" + currentCompoundWord[1].ToLower());
				bundle.AddClip(clip, DETAILED_PROMPT, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
				
				stepCounter++;
				return;
			}
			
			if(stepCounter == 2) //Move left word back
			{
				//Turn off magnify and Highlight
				puzzlePieceLeft.ScaleCentered( puzzlePieceLeft.scaleX * MAGNIFY_REVERT_PERCENT);
				HighlightOff(puzzlePieceLeft);
				
				//Tween card into place
				TweenerObj tempTweener =  Tweener.addTween(puzzlePieceLeft, Tweener.Hash("time", DETAILED_PROMPT_TRANSITION_TIME, "x", detailedPromptCardDestination.x, 
												"y", detailedPromptCardDestination.y, "transition", Tweener.TransitionType.easeInOutQuint) );
				tempTweener.OnComplete(DetailedWordPrompt);
				
				stepCounter++;
				return;
			}
			
			if(stepCounter == 3) //3. Highlight right word
			{		
				//Center and magnify right puzzle piece
				detailedPromptCardDestination = new Vector2( puzzlePieceRight.x, puzzlePieceRight.y); //record position to return to
				globalToLocalCenter = answerBox.globalToLocal(new Vector2(Screen.width * .5f, Screen.height * .5f));
				puzzlePieceRight.x = globalToLocalCenter.x - puzzlePieceRight.Target.width * .5f;
				puzzlePieceRight.y = globalToLocalCenter.y - puzzlePieceRight.Target.height * .5f;
				puzzlePieceRight.alpha = 1;
				
				//Magnify and Highlight
				puzzlePieceRight.ScaleCentered( puzzlePieceRight.scaleX * MAGNIFY_SCALE_PRECENT);
				HighlightOn(puzzlePieceRight);
				
				//Play audio for the card
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS,
												"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-" + currentCompoundWord[2].ToLower());
				bundle.AddClip(clip, DETAILED_PROMPT, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
				
				stepCounter++;
				return;
			}
			
			if(stepCounter == 4) //4. Narration Over, move hint word into place
			{
				//Turn off magnify and Highlight
				puzzlePieceRight.ScaleCentered( puzzlePieceRight.scaleX * MAGNIFY_REVERT_PERCENT);
				HighlightOff(puzzlePieceRight);
				
				//Tween card into place
				TweenerObj tempTweener =  Tweener.addTween(puzzlePieceRight, Tweener.Hash("time", DETAILED_PROMPT_TRANSITION_TIME, "x", detailedPromptCardDestination.x, 
												"y", detailedPromptCardDestination.y, "transition", Tweener.TransitionType.easeInOutQuint) );
				tempTweener.OnComplete(DetailedWordPrompt);
				
				stepCounter++;
				return;
			}
			
			//If emerging, do the same with the options, else, reveal options and move on
			if(stepCounter >= 5 && stepCounter < 5 + answerOptionsCompoundCards.Count * 2)
			{
				if(_level == SkillLevel.Emerging)
				{
					int answerIndex = Mathf.FloorToInt((stepCounter - 5) / 2);
					
					//Check to see if we are on the first part or second part of the given index.
					if(((stepCounter - 5) % 2) == 0) //on the first half, highlight puzzle piece
					{
						//Center and magnify compound card
						detailedPromptCardDestination = new Vector2( answerOptionsCompoundCards[answerIndex].x, answerOptionsCompoundCards[answerIndex].y); //record position to return to
						answerOptionsCompoundCards[answerIndex].x = refWidth * .5f - compoundWordCard.Target.width * .5f;
						answerOptionsCompoundCards[answerIndex].y = refHeight * .5f - compoundWordCard.Target.height * .5f;
						answerOptionsCompoundCards[answerIndex].alpha = 1;
					
						//Magnify and Highlight
						answerOptionsCompoundCards[answerIndex].ScaleCentered( answerOptionsCompoundCards[answerIndex].scaleX * MAGNIFY_SCALE_PRECENT);
						HighlightOn(answerOptionsCompoundCards[answerIndex]);
						
						//Play audio for the card
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS,
							"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-" + answerOptionsCompoundCards[answerIndex].name.ToLower());
						bundle.AddClip(clip, DETAILED_PROMPT, clip.length);
						SoundEngine.Instance.PlayBundle( bundle );
						
						stepCounter++;
						return;
					}
					else //On the second half, tween the puzzle piece
					{
						//Turn off magnify and Highlight
						answerOptionsCompoundCards[answerIndex].ScaleCentered( answerOptionsCompoundCards[answerIndex].scaleX * MAGNIFY_REVERT_PERCENT);
						HighlightOff(answerOptionsCompoundCards[answerIndex]);
						
						//Tween card into place
						TweenerObj tempTweener =  Tweener.addTween(answerOptionsCompoundCards[answerIndex], Tweener.Hash("time", DETAILED_PROMPT_TRANSITION_TIME,
											"x", detailedPromptCardDestination.x, "y", detailedPromptCardDestination.y, "transition", Tweener.TransitionType.easeInOutQuint) );
						tempTweener.OnComplete(DetailedWordPrompt);
					
						stepCounter++;
						return;
					}
				}
				else //reveal answers and move on
				{
					foreach(FilterMovieClip fmc in answerOptionsCompoundCards)
					{
						Tweener.addTween(fmc, Tweener.Hash("time", FADE_TIME, "alpha", 1, "transition", Tweener.TransitionType.easeInOutQuint) );
					}
					
					stepCounter = 5 + answerOptionsCompoundCards.Count * 2;					
				}
			}
			
			if(stepCounter >= 5 + answerOptionsCompoundCards.Count * 2) //emerging prompt is over
			{
				stepCounter = 0;
				if(_level == SkillLevel.Tutorial)
				{
					AnswerOptionsPuzzlePieces[0].alpha = 1f;
					NormalPrompt("TUTORIAL_STEP");
				}
				else
				{
					NormalPrompt();
				}
				AddNewOppData(audioNames.IndexOf( "Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-" + currentCompoundWord[0].ToLower() ));
			}
			
			
		}
		
		public void NormalPrompt()
		{
			NormalPrompt(PROMPT); //Default to the prompt constant as the message
		}
		
		public void NormalPrompt(string message)
		{
			string filepath;
			
			filepath = "Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-" + currentCompoundWord[0].ToLower();
			
			//Play the audio to prompt the player about the compoud word
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS, filepath );
			bundle.AddClip(clip, message, clip.length);
			SoundEngine.Instance.PlayBundle( bundle );
		}
		
		public void InactivtyPrompt()
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;
			
			if(timeoutCount == 0)
			{
				//Play "Please touch the picture to hear the words again"
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS, "Narration/1C-Skill Instructions Audio/1C-touch-the-cubes-to-hear-their-sound");
				bundle.AddClip(clip, PLAYER_INACTIVITY, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
				timeoutCount++;
				
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf("Narration/1C-Skill Instructions Audio/1C-touch-the-cubes-to-hear-their-sound") );
				return;
			}
			
			if(timeoutCount == 1)
			{
				//Play "Please touch the help button to see Cami do it."
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS, "Narration/1C-Skill Instructions Audio/1C-touch-the-green-button");
				bundle.AddClip(clip, PLAYER_INACTIVITY, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
				timeoutCount++;
				
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf("Narration/1C-Skill Instructions Audio/1C-touch-the-green-button") );
				return;
			}
			
			if(timeoutCount == 2)
			{
				//Opportunity is now incorrect
				//Play normal prompt
				//highlight answer
				foreach(FilterMovieClip fmc in answerOptionsCompoundCards)
				{
					if(fmc.name == correctWordAnswer) //puzzle piece is the answer, highlight it
					{
						HighlightOn(fmc);
						break;
					}
				}
				
				NormalPrompt(PLAYER_INACTIVITY); //Normal Prompt with different message
				timeoutCount++;
				
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf("Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-" + currentCompoundWord[0].ToLower()) );
				return;
			}
			if(timeoutCount == 3)
			{
				timeoutEnd = true;
				
				//Reset step counter
				timeoutCount = 0;
				
				//Enter Solve for Player State
				ChangeState(BlendingCompoundWordsStates.SOLVE_FOR_PLAYER);
				
				timeoutCount++;
				
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf("Narration/1C-Reinforcement Audio/1C-Answer-" + currentCompoundWord[1].ToLower() + "-with-" + currentCompoundWord[2].ToLower()) );
				return;
			}
			
		}
		
		public override void onTimerComplete()
		{
			InactivtyPrompt();
		}
		
		public void SolveForPlayer()
		{
			Debug.Log ("SOLVING FOR PLAYER - Step " + stepCounter);
			TweenerObj tempTweener = null;
			if(stepCounter == 0)
			{				
				//highlight answer, fade wrong answers
				foreach(FilterMovieClip fmc in answerOptionsCompoundCards)
				{
					if(fmc.name == correctWordAnswer) //puzzle piece is the answer, highlight it
					{
						HighlightOn(fmc);
						draggedCompoundCard = fmc; //record correct piece
					}
					else
					{
						tempTweener = Tweener.addTween(fmc, Tweener.Hash("time", FADE_TIME, "alpha", 0, "transition", Tweener.TransitionType.easeInOutQuint) );	
					}
				}
				
		
				stepCounter++;
				SolveForPlayer();
				return;
			}
			
			if(stepCounter == 1)
			{
				//Find the correct answer piece
				//tween right answer to answer position
								
				//Need to recalculate right answer position
				Vector2 targetLocation; //The center of the answer location
				targetLocation = compoundWordCard.localToGlobal(new Vector2(0,0));
				targetLocation = backgroundContainer.globalToLocal(targetLocation);
				
				tempTweener =  Tweener.addTween(draggedCompoundCard, Tweener.Hash("time", DETAILED_PROMPT_TRANSITION_TIME, "x", targetLocation.x, 
												"y", targetLocation.y, "scaleX", 1f, "scaleY", 1f, "transition", Tweener.TransitionType.easeInOutQuint) );
				stepCounter++;
				tempTweener.OnComplete(SolveForPlayer);
				return;
			}
			
			if(stepCounter > 1)
			{
				//set opportunity as wrong, set the correct answer to "draggedCompoundCard", and change state to PLAYER_REINFORCEMENT
				opportunityAnswered = true;
				opportunityCorrect = false;
				stepCounter = 0;
				
				//Turn highlight off before moving on
				HighlightOff(draggedCompoundCard);

				solveForPlayerBool = true;
				ChangeState(BlendingCompoundWordsStates.PLAYER_REINFORCEMENT);
			}
		}
		
		public void PlayCorrectAnswerReinforcement()
		{
			//FIX
			
			string filepath = "";
			
			filepath = "Narration/1C-Reinforcement Audio/1C-Answer-" + currentCompoundWord[1].ToLower() + "-with-" + currentCompoundWord[2].ToLower();
			
			//Play the audio to prompt the player about the compoud word
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS, filepath );
			bundle.AddClip(clip, CORRECT_REINFORCEMENT, clip.length);
			SoundEngine.Instance.PlayBundle( bundle );
			
			glowSequence();
		}
		
		private void glowSequence()
		{
			string filepath = "Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-";
			AudioClip clip = null;
			
			switch (stepCounter)
			{
			case 0:
				HighlightOn(puzzlePieceLeft);
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS, filepath + currentCompoundWord[1].ToLower() );
				break;
			case 1:
				HighlightOff(puzzlePieceLeft);
				HighlightOn(puzzlePieceRight);
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS, filepath + currentCompoundWord[2].ToLower() );
				break;
			case 2:
				HighlightOff(puzzlePieceRight);
				HighlightOn(draggedCompoundCard);
				stepCounter = 0;
				break;
			}
			
			if(clip != null)
			{
				glowTimer = TimerUtils.SetTimeout( clip.length * 0.4f, glowSequence );
				stepCounter++;
			}
		}
		
		public void PlayIncorrectAnswerReinforcement()
		{
			//The instruction for the wrong word			
			//Play the audio to prompt the player about the compoud word
			string word = draggedCompoundCard.name;
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS, "Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-" + word);
			bundle.AddClip(clip, INCORRECT_REINFORCEMENT, clip.length);
			SoundEngine.Instance.PlayBundle( bundle );
		}
		
		public void HighlightOn(FilterMovieClip fmc)
		{
			if(highlightedFilterMovieClips.Contains(fmc)) //if the fmc is highlighted, don't highlight it again
				return;
	
			fmc.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
			fmc.filters[0].Visible = true;
			highlightedFilterMovieClips.Add(fmc);
		}
		
		public void HighlightOff(FilterMovieClip fmc)
		{
			if(!highlightedFilterMovieClips.Contains(fmc)) //if the fmc isn't highlighted, don't de-highlight it again
				return;
			fmc.Destroy();
			highlightedFilterMovieClips.Remove(fmc);
		}
	
		public TweenerObj FadeInContainer()
		{
			return Tweener.addTween(backgroundContainer, Tweener.Hash("time", FADE_TIME, "alpha", 1, "transition", Tweener.TransitionType.easeInOutQuint) );
		}
		
		public TweenerObj FadeOutContainer()
		{
			return Tweener.addTween(backgroundContainer, Tweener.Hash("time", FADE_TIME, "alpha", 0, "transition", Tweener.TransitionType.easeInOutQuint) );
		}
		
		private bool isdrag;
		private Vector2 cardOrig;
		public void OnPieceDown( CEvent e )
		{
			ChangeState(BlendingCompoundWordsStates.PLAYER_INTERACTING);
			PlayClickSound();
			//Set the clicked piece to drag
			if(e.currentTarget as FilterMovieClip != draggedCompoundCard)
			{
				draggedCompoundCard = e.currentTarget as FilterMovieClip;
				draggedCompoundCardStartLocation.x = draggedCompoundCard.x;
				draggedCompoundCardStartLocation.y = draggedCompoundCard.y;
			}
			else
			{
				Tweener.removeTweens(draggedCompoundCard);
			}
			draggedCompoundCard.ScaleCentered(draggedScale*(4f/3f));
			
			backgroundContainer.removeChild(draggedCompoundCard);
			backgroundContainer.addChild(draggedCompoundCard);
			
			//Add Mouse Move listener to the stage
			addEventListener( MouseEvent.MOUSE_MOVE, OnPieceMove );
			
			//Add Listener to stage in case mouse isn't over answerpiece then manually check mouse position
			stage.addEventListener( MouseEvent.MOUSE_UP, OnPieceUp );
			
			cardOrig = draggedCompoundCard.parent.localToGlobal( new Vector2( draggedCompoundCard.x, draggedCompoundCard.y ) );
			isdrag = false;
		}
		
		public void OnBoxPieceDown( CEvent e )
		{
			PlayClickSound();
			draggedCompoundCard = e.currentTarget as FilterMovieClip;
			addEventListener( MouseEvent.MOUSE_UP, OnBoxPieceUp );
		}
		
		private void OnPieceMove( CEvent e )
		{
			Vector2 newPos = new Vector2( backgroundContainer.mouseX - draggedCompoundCard.width * .5f, backgroundContainer.mouseY - draggedCompoundCard.height * .5f );
			
			draggedCompoundCard.x = newPos.x;
			draggedCompoundCard.y = newPos.y;
			
			newPos = draggedCompoundCard.parent.localToGlobal( newPos );
			
			if( Mathf.Abs(cardOrig.x - newPos.x + cardOrig.y - newPos.y) > 5 ){
				isdrag = true;
			}
		}
		
		private void OnPieceUp( CEvent e )
		{
			//Release the piece based off of position.
			//If over answer, snap to answer area
			//else, return to original position
			
			//Get the global position of the hidden puzzle piece
			Vector2 referenceGlobalPosition = answerBox.localToGlobal(new Vector2(compoundWordCard.x, compoundWordCard.y));
			Rectangle overlap = compoundWordCard.getBounds(backgroundContainer).intersection(draggedCompoundCard.getBounds(backgroundContainer));
			
			//Remove Move Listener
			removeEventListener( MouseEvent.MOUSE_MOVE, OnPieceMove );
			
			
			//Check to see that the mouse in the bounds of the hidden target piece
			if(overlap.width > 25 && overlap.height > 25)
			{
				Vector2 dataPoint = draggedCompoundCard.parent.localToGlobal( new Vector2( draggedCompoundCard.x, draggedCompoundCard.y ) );
				AddNewActionData( formatActionData( "move", answerOptionsCompoundCards.IndexOf( draggedCompoundCard ) + 2, dataPoint, draggedCompoundCard.name == correctWordAnswer ) );
				
				referenceGlobalPosition = backgroundContainer.globalToLocal(referenceGlobalPosition);
				Tweener.addTween(draggedCompoundCard, Tweener.Hash("time", DETAILED_PROMPT_TRANSITION_TIME, "x", referenceGlobalPosition.x, 
									"y", referenceGlobalPosition.y, "scaleX", 1f, "scaleY", 1f, "transition", Tweener.TransitionType.easeInOutQuint) );

				//Enter reinforcement - answer will be checked and acted upon
				if(_level != SkillLevel.Tutorial) ChangeState(BlendingCompoundWordsStates.PLAYER_REINFORCEMENT);
				else tutorial_mc.TutorialActions();
			}

			else//return to starting position
			{
				if( isdrag )
					AddNewActionData( formatActionData( "snap", answerOptionsCompoundCards.IndexOf( draggedCompoundCard ) + 2, Vector2.zero ) );
				else
					AddNewActionData( formatActionData( "tap", answerOptionsCompoundCards.IndexOf( draggedCompoundCard ) + 2, Vector2.zero ) );
				
				Tweener.addTween(draggedCompoundCard, Tweener.Hash("time", DETAILED_PROMPT_TRANSITION_TIME, "x", draggedCompoundCardStartLocation.x, 
									"y", draggedCompoundCardStartLocation.y, "scaleX", draggedScale, "scaleY", draggedScale, "transition", Tweener.TransitionType.easeInOutQuint) );
				
				//Play the piece's reinforcement
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS,
															"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-" + draggedCompoundCard.name);
				bundle.AddClip(clip, PROMPT, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
				ChangeState(BlendingCompoundWordsStates.PLAYER_INTERACTION);
			}
			
			//Remove up listener from stage
			stage.removeEventListener( MouseEvent.MOUSE_UP, OnPieceUp );
		}
		
		private void OnBoxPieceUp ( CEvent e )
		{
			if( draggedCompoundCard == puzzlePieceRight )
			{
				AddNewActionData( formatActionData( "tap", 0, Vector2.zero ) );
			}
			else
			{
				AddNewActionData( formatActionData( "tap", 1, Vector2.zero ) );
			}
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS,
														"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-" + draggedCompoundCard.name);
			bundle.AddClip(clip, PROMPT, clip.length);
			SoundEngine.Instance.PlayBundle( bundle );
			ChangeState(BlendingCompoundWordsStates.PLAYER_INTERACTION);
			
			removeEventListener( MouseEvent.MOUSE_UP, OnBoxPieceUp );
		}
		
		public void PlayCorrectSoundPublic()
		{
			PlayCorrectSound();
		}
		
		//Reset the tutorial by loading a new one
		public void RestartTutorial()
		{
			tutorialSetupCounter = 0;
			
			//create tutorial and pass it a reference to this
			tutorial_mc.Dispose();
			
			tutorial_mc = new BlendingCompoundWordsTutorial();
			tutorial_mc.skillRef = this;
			
			
			ChangeState(BlendingCompoundWordsStates.TUTORIAL);
		}

		public void fadeInTutorialPieces() {
			Tweener.addTween(puzzlePieceLeft, Tweener.Hash("time", FADE_TIME, "alpha", 1, "transition", Tweener.TransitionType.easeInOutQuint) );
			Tweener.addTween(puzzlePieceRight, Tweener.Hash("time", FADE_TIME, "alpha", 1, "transition", Tweener.TransitionType.easeInOutQuint) );
			Tweener.addTween(AnswerOptionsPuzzlePieces[0], Tweener.Hash("time", FADE_TIME, "alpha", 1, "transition", Tweener.TransitionType.easeInOutQuint) );

		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = "";
			foreach(string s in currentCompoundWord)
			{
				if(data != "")
					data = data + "-";
				data = data + s;
			}
			
			foreach(List<string> ls in compoundWordOpportunityBank)
			{
				data = data + ";";
				foreach(string s in ls)
				{
					if(data.LastIndexOf(';') != data.Length - 1)
						data = data + "-";
					data = data + s;
				}
			}
			
			return data;
		}
		
		private void parseSessionData (string data)
		{
			compoundWordOpportunityBank = new List<List<string>>();
			
			string[] sData = data.Split(';');
			string[] bData;
			List<string> sWords;
			
			foreach( string s in sData )
			{
				sWords = new List<string>();
				bData = s.Split('-');
				foreach( string b in bData )
				{
					sWords.Add(b);
				}
				compoundWordOpportunityBank.Add(sWords);
			}
		}
		
		public static List<string> audioNames = new List<string>
		{	"",
			"Narration/1C-Story Introduction Audio/1C-Story-Introduction",
			"Narration/1C-Skill Instructions Audio/1C-the-small-cubes-are-dog-and-house",
			"Narration/1C-Skill Instructions Audio/1C-watch-closely-as-platty",
			"Narration/1C-Skill Instructions Audio/1C-Now-you-try",
			"Narration/1C-Skill Instructions Audio/1C-drag-to-the-box",
			"Narration/1C-Skill Instructions Audio/1C-lets-keep-making-bigger-sounds",
			"Narration/1C-Skill Instructions Audio/1C-try-again",
			"Narration/1C-Skill Instructions Audio/1C-touch-the-cubes-to-hear-their-sound",
			"Narration/1C-Skill Instructions Audio/1C-touch-the-green-button",
			"Narration/1C-Skill Instructions Audio/1C-listen-to-the-words",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-baseball",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-basketball",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-birdcage",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-birdhouse",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-boathouse",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-bookcase",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-bowtie",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-boxcar",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-bullhorn",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-buttercup",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-butterfly",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-chalkboard",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-cowboy",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-cupcake",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-doghouse",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-earring",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-firehouse",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-fireman",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-firewood",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-fishbowl",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-football",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-goldfish",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-houseboat",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-keyhole",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-ladybug",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-lighthouse",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-lipstick",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-mailbox",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-pancake",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-peanut",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-pocketbook",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-rainbow",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-rattlesnake",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-sandpaper",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-sawhorse",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-skyscraper",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-snowball",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-snowman",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-starfish",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-sunflower",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-sunglasses",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-surfboard",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-turtleneck",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-wallpaper",
			"Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-wheelchair",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-apple",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-baby",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-ball",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-base",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-basket",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-bird",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-board",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-boat",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-book",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-bow",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-bowl",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-box",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-boy",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-bug",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-bull",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-butter",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-cage",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-cake",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-car",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-carrot",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-case",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-cat",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-chair",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-chalk",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-cloud",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-cow",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-cup",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-dog",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-ear",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-fire",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-fish",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-flower",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-fly",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-foot",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-glasses",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-gold",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-hole",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-horn",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-horse",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-house",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-key",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-lady",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-leaf",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-light",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-lion",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-lip",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-mail",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-man",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-neck",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-nut",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-pan",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-paper",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-pea",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-pen",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-pencil",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-pocket",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-rabbit",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-rain",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-rattle",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-ring",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-sand",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-saw",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-scraper",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-sky",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-snake",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-snow",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-star",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-stick",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-sun",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-surf",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-tie",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-turtle",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-wall",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-wheel",
			"Narration/1C-Skill Instructions Audio/1C-Word Segment Cards/1C-wood",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-baseball",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-basketball",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-birdcage",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-birdhouse",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-boathouse",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-bookcase",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-bowtie",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-boxcar",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-bullhorn",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-buttercup",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-butterfly",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-chalkboard",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-cowboy",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-cupcake",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-doghouse",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-earring",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-firehouse",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-fireman",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-firewood",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-fishbowl",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-football",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-goldfish",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-houseboat",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-keyhole",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-ladybug",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-lighthouse",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-lipstick",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-mailbox",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-pancake",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-peanut",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-pocketbook",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-rainbow",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-rattlesnake",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-sandpaper",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-sawhorse",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-skyscraper",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-snowball",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-snowman",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-starfish",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-sunflower",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-sunglasses",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-surfboard",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-turtleneck",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-wallpaper",
			"Narration/1C-Skill Instructions Audio/1C-Compound Word Cards/1C-wheelchair",
			"Narration/1C-Reinforcement Audio/1C-Answer-base-with-ball",
			"Narration/1C-Reinforcement Audio/1C-Answer-basket-with-ball",
			"Narration/1C-Reinforcement Audio/1C-Answer-bird-with-cage",
			"Narration/1C-Reinforcement Audio/1C-Answer-bird-with-house",
			"Narration/1C-Reinforcement Audio/1C-Answer-boat-with-house",
			"Narration/1C-Reinforcement Audio/1C-Answer-book-with-case",
			"Narration/1C-Reinforcement Audio/1C-Answer-bow-with-tie",
			"Narration/1C-Reinforcement Audio/1C-Answer-box-with-car",
			"Narration/1C-Reinforcement Audio/1C-Answer-bull-with-horn",
			"Narration/1C-Reinforcement Audio/1C-Answer-butter-with-cup",
			"Narration/1C-Reinforcement Audio/1C-Answer-butter-with-fly",
			"Narration/1C-Reinforcement Audio/1C-Answer-chalk-with-board",
			"Narration/1C-Reinforcement Audio/1C-Answer-cow-with-boy",
			"Narration/1C-Reinforcement Audio/1C-Answer-cup-with-cake",
			"Narration/1C-Reinforcement Audio/1C-Answer-dog-with-house",
			"Narration/1C-Reinforcement Audio/1C-Answer-ear-with-ring",
			"Narration/1C-Reinforcement Audio/1C-Answer-fire-with-house",
			"Narration/1C-Reinforcement Audio/1C-Answer-fire-with-man",
			"Narration/1C-Reinforcement Audio/1C-Answer-fire-with-wood",
			"Narration/1C-Reinforcement Audio/1C-Answer-fish-with-bowl",
			"Narration/1C-Reinforcement Audio/1C-Answer-foot-with-ball",
			"Narration/1C-Reinforcement Audio/1C-Answer-gold-with-fish",
			"Narration/1C-Reinforcement Audio/1C-Answer-house-with-boat",
			"Narration/1C-Reinforcement Audio/1C-Answer-key-with-hole",
			"Narration/1C-Reinforcement Audio/1C-Answer-lady-with-bug",
			"Narration/1C-Reinforcement Audio/1C-Answer-light-with-house",
			"Narration/1C-Reinforcement Audio/1C-Answer-lip-with-stick",
			"Narration/1C-Reinforcement Audio/1C-Answer-mail-with-box",
			"Narration/1C-Reinforcement Audio/1C-Answer-pan-with-cake",
			"Narration/1C-Reinforcement Audio/1C-Answer-pea-with-nut",
			"Narration/1C-Reinforcement Audio/1C-Answer-pocket-with-book",
			"Narration/1C-Reinforcement Audio/1C-Answer-rain-with-bow",
			"Narration/1C-Reinforcement Audio/1C-Answer-rattle-with-snake",
			"Narration/1C-Reinforcement Audio/1C-Answer-sand-with-paper",
			"Narration/1C-Reinforcement Audio/1C-Answer-saw-with-horse",
			"Narration/1C-Reinforcement Audio/1C-Answer-sky-with-scraper",
			"Narration/1C-Reinforcement Audio/1C-Answer-snow-with-ball",
			"Narration/1C-Reinforcement Audio/1C-Answer-snow-with-man",
			"Narration/1C-Reinforcement Audio/1C-Answer-star-with-fish",
			"Narration/1C-Reinforcement Audio/1C-Answer-sun-with-flower",
			"Narration/1C-Reinforcement Audio/1C-Answer-sun-with-glasses",
			"Narration/1C-Reinforcement Audio/1C-Answer-surf-with-board",
			"Narration/1C-Reinforcement Audio/1C-Answer-turtle-with-neck",
			"Narration/1C-Reinforcement Audio/1C-Answer-wall-with-paper",
			"Narration/1C-Reinforcement Audio/1C-Answer-wheel-with-chair",
			"User-Earns-Artifact"
		};
	}	
}