using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.geom;

namespace SSGCore
{
	public class Patterning : BaseSkill
	{
		private const string PRIME = "PatternPriming";
		private const string REVIEW = "LookAtItYouLittle";
		private const string ENDREVIEW = "LookAtWhatYou'veDone";
		private const string AGAIN = "TryAgain";
		private const string WATCH = "WatchThemDo";
		private const string WASNEXT = "WasNextFollows";
		private const string COMESNEXT = "ComesNextFollows";
		
		private const string ABA = "12";
		private const string AABB = "1122";
		private const string ABC = "123";
		
		private List<List<string>> egOppBank = new List<List<string>>()
		{    new List<string>(){ABA, "CLAVE", "TRIANGLE"},
			new List<string>(){ABA, "TRIANGLE", "XYLOPHONE"},
			new List<string>(){ABA, "GUITAR", "TRUMPET"},
			new List<string>(){ABA, "CLAVE", "XYLOPHONE"},
			new List<string>(){ABA, "BANJO", "TRUMPET"},
			new List<string>(){ABA, "CLAVE", "MARACA"},
			new List<string>(){ABA, "XYLOPHONE", "GUITAR"},
			new List<string>(){ABA, "CLAVE", "TRUMPET"},
			new List<string>(){ABA, "XYLOPHONE", "TAMBOURINE"},
			new List<string>(){ABA, "CLAVE", "GUITAR"},
			new List<string>(){ABA, "TAMBOURINE", "CLAVE"},
			new List<string>(){ABA, "TRIANGLE", "MARACA"},
			new List<string>(){ABA, "MARACA", "BANJO"},
			new List<string>(){ABA, "TRIANGLE", "BANJO"},
			new List<string>(){ABA, "BANJO", "XYLOPHONE"}};
		private List<List<string>> dgOppBank = new List<List<string>>()
		{    new List<string>(){AABB, "BELL", "TOMTOM"},
			new List<string>(){AABB, "TOMTOM", "HARMONICA"},
			new List<string>(){AABB, "VIOLIN", "PIPE"},
			new List<string>(){AABB, "CHIMES", "FRENCHHORN"},
			new List<string>(){AABB, "TROMBONE", "FLUTE"},
			new List<string>(){AABB, "FRENCHHORN", "KEYBOARD"},
			new List<string>(){AABB, "FLUTE", "BELL"},
			new List<string>(){AABB, "HARMONICA", "PIPE"},
			new List<string>(){AABB, "KEYBOARD", "CHIMES"},
			new List<string>(){AABB, "VIOLIN", "BELL"},
			new List<string>(){AABB, "PIPE", "TROMBONE"},
			new List<string>(){AABB, "BELL", "HARMONICA"},
			new List<string>(){AABB, "PIPE", "CHIMES"},
			new List<string>(){AABB, "KEYBOARD", "VIOLIN"},
			new List<string>(){AABB, "TOMTOM", "FLUTE"}};
		private List<List<string>> ddABABank = new List<List<string>>()
		{    new List<string>(){ABA, "WOODENBLOCK", "ACCORDION"},
			new List<string>(){ABA, "HARP", "CABASA"},
			new List<string>(){ABA, "CYMBALS", "BASSDRUM"},
			new List<string>(){ABA, "BASSDRUM", "CASTANETS"}};
		private List<List<string>> ddAABBBank = new List<List<string>>()
		{    new List<string>(){AABB, "RAINSTICK", "WHISTLE"},
			new List<string>(){AABB, "ACCORDION", "CASTANETS"},
			new List<string>(){AABB, "CASTANETS", "WOODENBLOCK"},
			new List<string>(){AABB, "KALIMBA", "CYMBALS"},
			new List<string>(){AABB, "CYMBALS", "WOODENBLOCK"},
			new List<string>(){AABB, "CABASA", "KALIMBA"}};
		private List<List<string>> ddABCBank = new List<List<string>>()
		{    new List<string>(){ABC, "CYMBALS", "RAINSTICK", "CABASA"},
			new List<string>(){ABC, "RAINSTICK", "CASTANETS", "WHISTLE"},
			new List<string>(){ABC, "CASTANETS", "BASSDRUM", "CYMBALS"},
			new List<string>(){ABC, "WHISTLE", "ACCORDION", "HARP"},
			new List<string>(){ABC, "CABASA", "KALIMBA", "WOODENBLOCK"},
			new List<string>(){ABC, "WOODENBLOCK", "HARP", "RAINSTICK"},
			new List<string>(){ABC, "CYMBALS", "ACCORDION", "WHISTLE"},
			new List<string>(){ABC, "KALIMBA", "CABASA", "RAINSTICK"},
			new List<string>(){ABC, "BASSDRUM", "CYMBALS", "CABASA"},
			new List<string>(){ABC, "HARP", "CABASA", "ACCORDION"}};
		
		//        private float neckRadius = 580f;
		//        private float[] neckCent = new float[]{960, 75};
		
		private DisplayObjectContainer container;
		private float baseScale;
		private float beadScale;
		private int baseWidth = 166;
		
		private List<List<string>> setList;
		private List<string> currentSet;
		
		private MovieClip targShelf;
		private List<PatterningBead> beads;
		private PatterningBead currentBead;
		private PatterningBead draggedBead;
		private float[] cBeadOrig = new float[2];
		private float[] mouseOffset = new float[2];
		private int inPattern;
		private int failCount;
		private int reviewCount;
		private bool correctionReview;
		private bool beadTweening = false;

		private new PatterningTutorial tutorial_mc;
		
		private string nextIn
		{
			get
			{
				int nextPattern = inPattern;
				while(nextPattern >= currentSet[0].Length)
				{
					nextPattern -= currentSet[0].Length;
				}
				return currentSet[int.Parse(currentSet[0][nextPattern].ToString())];
			}
		}
		
		public Patterning (SkillLevel level, SessionInfo  currentSession) : base(level, currentSession)
		{
			BACKGROUND = "Background";
			ARTIFACT = "Artifact";
			
			Init(null);
		}
		
		public override void Dispose ()
		{
			base.Dispose ();
			
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			beadsOff(true);
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, beadMove);
			stage.removeEventListener(MouseEvent.MOUSE_UP, beadUp);
			if(tutorial_mc != null && tutorial_mc.bead != null)
				tutorial_mc.bead.Destroy();
			if(currentBead != null)
				currentBead.Destroy();
			if(draggedBead != null)
				draggedBead.Destroy();
		}
		
		public override void Init (MovieClip parent)
		{
			base.Init(parent);
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			container = new DisplayObjectContainer();
			Resize(Screen.width, Screen.height);
			container.visible = false;
			container.alpha = 0;
			addChild(container);
			
			targShelf = MovieClipFactory.CreatePatterningNecklace();
			baseScale = 2013.9f / targShelf.width;
			targShelf.scaleX = targShelf.scaleY = baseScale;
			targShelf.x = 15.4f;
			targShelf.y = 653.05f;
			container.addChild(targShelf);
			
			if(_level == SkillLevel.Tutorial)
			{
				totalOpportunities = 1;
				setList = new List<List<string>>(){ new List<string>() {ABA, "TOMTOM", "BELL"} };
				tutorial_mc = new PatterningTutorial(baseScale);
				tutorial_mc.View.addEventListener(TutorialEvent.START, onTutorialStart);
				tutorial_mc.View.addEventListener(TutorialEvent.COMPLETE, onTutorialComplete);
			}
			else
			{
				totalOpportunities = 10;
				setList = new List<List<string>>();
				switch(_level)
				{
				case SkillLevel.Emerging:
					setList.AddRange(egOppBank.RandomSample(10));
					break;
				case SkillLevel.Developing:
					setList.AddRange(dgOppBank.RandomSample(10));
					break;
				default: //DD or CD
					setList.AddRange(ddABABank.RandomSample(2));
					setList.AddRange(ddAABBBank.RandomSample(3));
					setList.AddRange(ddABCBank.RandomSample(5));
					break;
				}
				setList.Shuffle();
				
				if(resumingSkill)
				{
					parseSessionData(currentSession.sessionData);
					//    nextOpportunity();
				}
			}
		}
		
		public override void OnAudioWordComplete (string word)
		{
			switch(word)
			{
			case THEME_INTRO:
				if( !currentSession.skipIntro )
					playSound("5C-Story Introduction Audio/5C-story-introduction", INTRO);
				else
					OnAudioWordComplete( INTRO );
				break;
				
			case INTRO:
				nextOpportunity();
				break;
				
			case PRIME:
				primePattern();
				break;
				
			case WATCH:
				tutorial_mc.OnStart();
				break;
				
			case NOW_ITS:
				playSound("5C-Skill Instruction Audio/5C-drag-the-next-instrument", PLEASE_FIND);
				break;
				
			case PLEASE_FIND:
				beadsOn();
				allGlowOff();
				break;
				
			case CLICK_CORRECT:
				if(!opportunityComplete)
					playSound("5C-Instrument Sounds/5C-" + getFromPattern(inPattern - 1).Type.ToLower() + "-sound", WASNEXT);
				else
					playSound("5C-Instrument Sounds/5C-" + getFromPattern(inPattern - 1).Type.ToLower() + "-sound", YOU_FOUND);
				break;
				
			case WASNEXT:
				playSound("5C-Reinforcement Audio/5C-Was Next/5C-the-" + getFromPattern(inPattern - 1).Type.ToLower() + "-was-next", PLEASE_FIND);
				break;
				
			case COMESNEXT:
				playSound("5C-Reinforcement Audio/5C-Comes Next/5C-the-" + nextIn.ToLower() + "-comes-next", NOW_ITS);
				break;
				
			case YOU_FOUND:
				dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
				allGlowOff();
				break;
				
			case THEME_COMPLIMENT:
				playSound("5C-Skill Instruction Audio/5C-watch-and-listen-to-the-pattern", ENDREVIEW);
				break;
				
			case CLICK_INCORRECT:
				if(opportunityComplete)
					finishPattern();
				else        
					playSound("5C-Instrument Sounds/5C-" + currentBead.Type.ToLower() + "-sound", YOU_FOUND_INCORRECT);
				break;
				
			case THEME_CRITICISM:
				playSound("5C-Skill Instruction Audio/5C-watch-and-listen-to-the-pattern", REVIEW);
				break;
				
			case YOU_FOUND_INCORRECT:
				dispatchEvent( new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
				break;
				
			case REVIEW:
				reviewPattern(true);
				break;
				
			case ENDREVIEW:
				reviewPattern(false);
				break;
				
			case AGAIN:
				resetTutorial();
				break;
				
			case THIS_IS:
				if(_level != SkillLevel.Tutorial)
					goto case NOW_LETS;
				playSound("5C-Skill Instruction Audio/5C-let-the-music-game-begin", NOW_LETS);
				break;
				
			case NOW_LETS:
				fadeOutContainer();
				if(opportunityCorrect)
					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
				else
					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
				break;
			}
		}
		
		public override void onTutorialStart (CEvent e)
		{
			base.onTutorialStart(e);
			tutorial_mc.View.removeEventListener( TutorialEvent.START, onTutorialStart );
		}
		
		public void onTutorialComplete (CEvent e)
		{
			tutorial_mc.View.removeEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			
			tweenToPattern(beads[2]).OnComplete(swapTutorial);
			
			playSound("5C-Skill Instruction Audio/5C-now-its-your-turn", NOW_ITS);
		}
		
		private void swapTutorial ()
		{
			beads[2].visible = true;
			removeChild(tutorial_mc.View);
		}
		
		protected override void resetTutorial ()
		{
			beadsOff();
			
			tutorial_mc.bead.Destroy();
			tutorial_mc = new PatterningTutorial(baseScale);
			tutorial_mc.View.addEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			
			removeChild(tutorial_mc.View);
			addChild(tutorial_mc.View);
			
			currentOpportunity--;
			OnAudioWordComplete(INTRO);
		}
		
		public override void nextOpportunity ()
		{
			if(currentOpportunity == totalOpportunities)
			{
				tallyAnswers();
				return;
			}
			
			opportunityComplete = false;
			timeoutCount = 0;
			inPattern = 0;
			failCount = 0;
			reviewCount = -1;
			correctionReview = false;
			
			if(_level == SkillLevel.Tutorial)
			{
				//inPattern--;
				addChild(tutorial_mc.View);
			}
			else
			{
				opportunityAnswered = false;
			}
			
			beadsOff(true);
			beads = new List<PatterningBead>();
			
			currentSet = new List<string>();
			currentSet.AddRange(setList[currentOpportunity]);
			
			setupOpp();
			
			string clipName = "pattern";
			for( int i = 1; i < currentSet.Count; i++ )
				clipName += "-" + currentSet[i].ToLower();
			AddNewOppData( audioNames.IndexOf( clipName) );
			
			currentOpportunity++;
			
			fadeInContainer();
			dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_START) );
		}
		
		private void setupOpp ()
		{
			PatterningBead tempBead;
			int[] rows;
			if(_level == SkillLevel.Tutorial)
			{
				rows = new int[]{1030};
				
				tempBead = new PatterningBead(currentSet[1], _level);
				beads.Add(tempBead);
				tempBead = new PatterningBead(currentSet[2], _level);
				beads.Add(tempBead);
				tempBead = new PatterningBead(currentSet[2], _level);
				beads.Add(tempBead);
				
				beads.Shuffle();
				
				tempBead = new PatterningBead(currentSet[1], _level);
				tempBead.visible = false;
				beads.Insert(2, tempBead);
			}
			else
			{
				int bNum = 1; //number of beads per type
				int bVar = 2; //number of types of beads
				switch(currentSet[0])
				{
				case ABC:
					bVar = 3;
					goto case ABA;
				case ABA:
					bNum = 4;
					break;
				case AABB:
					bNum = 6;
					break;
				}
				List<PatterningBead> temp1 = new List<PatterningBead>();
				List<PatterningBead> temp2 = new List<PatterningBead>();
				for(int i = 1; i <= bVar; i++)
				{
					int j;
					for( j = 0; j < 2; j++)
					{
						tempBead = new PatterningBead(currentSet[i], _level);
						temp1.Add(tempBead);
					}
					for( j = 0; j < bNum - 2; j++)
					{
						tempBead = new PatterningBead(currentSet[i], _level);
						temp2.Add(tempBead);
					}
					if(currentSet[i].IndexOf('_') > 0)
						currentSet[i] = currentSet[i].Split('_')[1];
				}
				temp1.Shuffle();
				temp2.Shuffle();
				
				beads.AddRange(temp1);
				beads.AddRange(temp2);
				
				rows = new int[]{1030};
			}
			
			int pad = 0;
			float totalWidth;
			
			beadScale = baseScale;
			//beadScale *= 0.9f;
			
			totalWidth = ((pad + baseWidth) * beads.Count) - (pad * rows.Length);
			int l = beads.Count - 1;
			float xStart;
			for(int k = rows.Length - 1; k >= 0; k--)
			{
				xStart = ((2048f - (totalWidth / (float)rows.Length)) / 2f) + (totalWidth / (float)rows.Length);
				int min = (beads.Count/rows.Length) * k;
				for( l = l + 1 - 1; l >= min; l--)
				{
					xStart -= baseWidth;
					
					tempBead = beads[l];
					container.addChild(tempBead);
					tempBead.scaleX = tempBead.scaleY = beadScale;
					tempBead.x = xStart;
					tempBead.y = rows[k];
				}
			}
		}
		
		private void primePattern ()
		{    
			if(currentBead != null)
				currentBead.glowOff();
			if(inPattern >= beads.Count / 2 || (currentSet[0] == AABB && inPattern >= 4))
			{
				if(_level != SkillLevel.Tutorial)
					OnAudioWordComplete(NOW_ITS);
				else
				{
					playSound("5C-Skill Instruction Audio/5C-watch-closely", WATCH);
				}
				
				int placeIn = 0, i = -1;
				PatterningBead b;
				Vector2 point;
				while( placeIn < beads.Count )
				{
					i++;
					if( i == beads.Count ) i = 0;
					b = beads[i];
					point = b.parent.localToGlobal( new Vector2( b.x, b.y ) );
					if( placeIn == b.placeIn )
					{
						AddNewObjectData( formatObjectData( b.Type, point ) );
						b.dataIndex = placeIn;
						placeIn++;
						if( placeIn >= inPattern )
						{
							i = 0;
							b = beads[0];
						}
					}
					
					if( placeIn >= inPattern && b.placeIn < 0 )
					{
						AddNewObjectData( formatObjectData( b.Type, point ) );
						b.dataIndex = placeIn;
						placeIn++;
					}
				}
			}
//			else if(inPattern < 0)
//			{
//				playSound("5C-Skill Instruction Audio/5C-heres-how-to-play-the-game", PRIME);
//				inPattern = 0;
//			}
			else
			{
				string nextOne = nextIn;
				for( int i = 0; i < beads.Count; i++ )
				{
					if( _level == SkillLevel.Tutorial && i == 2 )
						continue;
					
					if(beads[i].Type == nextOne && beads[i].placeIn < 0)
					{
						currentBead = beads[i];
						break;
					}
				}
				currentBead.glowOn();
				tweenToPattern(currentBead);
				playSound("5C-Instrument Sounds/5C-" + currentBead.Type.ToLower() + "-sound", PRIME);
			}
		}
		
		private TweenerObj tweenToPattern (PatterningBead target)
		{
			target.placeIn = inPattern;
			inPattern++;
			container.removeChild(target);
			container.addChild(target);
			
			float[] newPos = new float[2];
			newPos[0] = 23f + ((inPattern - 1) * baseWidth);
			newPos[1] = 682f;
			
			return Tweener.addTween(target, Tweener.Hash("time", 0.25f, "x", newPos[0], "y", newPos[1], "scaleX", beadScale, "scaleY", beadScale));
		}
		
		private bool shelfCollision (PatterningBead target)
		{
			Rectangle shelfRect = new Rectangle( targShelf.x, targShelf.y, 2013f, 220f );
			
			return shelfRect.intersection( target.getBounds(container) ).width > 0;
		}
		
		private void fadeInContainer ()
		{
			container.visible = true;
			Tweener.addTween(container, Tweener.Hash("time", 0.25f, "alpha", 1f)).OnComplete( primePattern );
		}
		
		private void fadeOutContainer ()
		{
			Tweener.addTween(container, Tweener.Hash("time", 0.125f, "alpha", 0f)).OnComplete(fullHide);
			if(_level == SkillLevel.Tutorial)
				Tweener.addTween(tutorial_mc, Tweener.Hash("time", 0.125f, "alpha", 0f));
		}
		private void fullHide () { if(opportunityComplete) container.visible = false; }
		
		private void beadsOn ()
		{
			if(beads == null || beads.Count == 0)
				return;
			
			foreach(PatterningBead pb in beads)
			{
				if(pb.placeIn < 0 && !pb.hasEventListener(MouseEvent.MOUSE_DOWN))
					pb.addEventListener(MouseEvent.MOUSE_DOWN, beadDown);
				if(timeoutCount > 0)
					pb.glowOff();
			}
			if(_level != SkillLevel.Tutorial)
				MarkInstructionEnd();
			startTimer();
		}
		
		private void beadsOff () { beadsOff(false); }
		private void beadsOff (bool nuke)
		{
			if(beads == null || beads.Count == 0)
				return;
			
			foreach(PatterningBead b in beads)
			{
				b.removeAllEventListeners(MouseEvent.MOUSE_DOWN);
				if(nuke)
				{
					b.Destroy();
					b.parent.removeChild(b);
				}
			}
			MarkInstructionStart();
			stopTimer();
		}
		
		private bool isdrag;
		private Vector2 cardOrig;
		private void beadDown ( CEvent e )
		{
			if (beadTweening) 
				return;

			draggedBead = e.currentTarget as PatterningBead;
			cBeadOrig[0] = draggedBead.x;
			cBeadOrig[1] = draggedBead.y;
			mouseOffset[0] = container.mouseX - draggedBead.x;
			mouseOffset[1] = container.mouseY - draggedBead.y;
			draggedBead.scaleX = draggedBead.scaleY = beadScale * (4f/3f);
			
			container.removeChild(draggedBead);
			container.addChild(draggedBead);
			
			beadsOff();
			timeoutCount = 0;
			if(_level != SkillLevel.Tutorial)
				MarkInstructionEnd();
			
			stage.addEventListener(MouseEvent.MOUSE_MOVE, beadMove);
			stage.addEventListener(MouseEvent.MOUSE_UP, beadUp);
			
			PlayClickSound();
			
			cardOrig = draggedBead.parent.localToGlobal( new Vector2( draggedBead.x, draggedBead.y ) );
			isdrag = false;
		}
		
		private void beadMove ( CEvent e )
		{
			draggedBead.x = container.mouseX - mouseOffset[0];
			draggedBead.y = container.mouseY - mouseOffset[1];
			if(shelfCollision(draggedBead))
				draggedBead.glowOn();
			else
				draggedBead.glowOff();
			
			Vector2 newPos = container.globalToLocal(new Vector2(container.mouseX - mouseOffset[0], container.mouseY - mouseOffset[1]));
			
			if( Mathf.Abs(cardOrig.x - newPos.x + cardOrig.y - newPos.y) > 5 ){
				isdrag = true;
			}
		}
		
		private void beadUp ( CEvent e )
		{
			currentBead = draggedBead;
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, beadMove);
			stage.removeEventListener(MouseEvent.MOUSE_UP, beadUp);
			
			beadsOff();
			
			if(shelfCollision(currentBead))
			{
				correctionReview = false;
				SoundEngine.Instance.SendCancelToken();
				allGlowOff();
				
				Vector2 point = draggedBead.parent.localToGlobal( new Vector2( draggedBead.x, draggedBead.y ) );
				if(currentBead.Type == nextIn)
				{
					AddNewActionData( formatActionData( "move", draggedBead.dataIndex, point, true ) );
					
					currentBead.glowOn();
					tweenToPattern(currentBead);
					
					if(inPattern == beads.Count)
					{
						if(!opportunityAnswered)
						{
							opportunityAnswered = true;
							opportunityCorrect = true;
						}
						opportunityComplete = true;
					}
					else
					{
						beadsOn();
						stopTimer();
					}
					
					PlayCorrectSound();
					return;
				}
				else
				{
					AddNewActionData( formatActionData( "move", draggedBead.dataIndex, point, false ) );
					
					opportunityAnswered = true;
					opportunityCorrect = false;
					failCount++;
					PlayIncorrectSound();
					if(failCount == 3)
					{
						opportunityComplete = true;
						return;
					}
					else
					{
						correctionReview = true;
					}
				}
			}
			else
			{
				AddNewActionData( formatActionData( isdrag ? "snap" : "tap", draggedBead.dataIndex, Vector2.zero ) );
			}
			
			if(correctionReview)
				stopTimer();
			else
				beadsOn();
			
			Tweener.addTween( currentBead, Tweener.Hash("time", 0.125f, "x", cBeadOrig[0], "y", cBeadOrig[1], "scaleX", beadScale, "scaleY", beadScale) ).OnComplete( beadTweenComplete );;
			beadTweening = true;
			currentBead.glowOff();
		}

		private void beadTweenComplete() {
			beadTweening = false;
		}

		private PatterningBead getFromPattern (int placeIn)
		{
			if(placeIn < inPattern)
			{
				foreach(PatterningBead pb in beads)
				{
					if(pb.placeIn == placeIn)
						return pb;
				}
			}
			else
			{
				string nextOne = nextIn;
				foreach(PatterningBead b in beads)
				{
					if(b.placeIn < 0 && b.Type == nextOne)
						return b;
				}
			}
			DebugConsole.LogError("Patterning || getFromPattern - Requested object " + placeIn + " not found.");
			return null;
		}
		
		private string patternLong ()
		{
			string ret = "";
			foreach(char c in currentSet[0])
			{
				if(ret != "") ret += "-";
				int i;
				if(int.TryParse(c.ToString(), out i))
					ret += currentSet[i].ToLower();
			}
			return ret;
		}
		
		private void reviewPattern ( bool notOver )
		{
			allGlowOff();
			if(reviewCount < 0)
			{
				if(notOver)
				{
					
					if( correctionReview )
						reviewCount = 0;
					else
						reviewCount = inPattern - currentSet[0].Length;
				}
				else
				{
					if(_level == SkillLevel.Tutorial)
						reviewCount = 0;
					else
					{
						switch(currentSet[0])
						{
						case ABA:
						case AABB:
							reviewCount = 4;
							break;
						case ABC:
							reviewCount = 6;
							break;
						}
					}
				}
			}
			string word;
			if((!correctionReview && reviewCount < inPattern - 1) || (correctionReview && (reviewCount < inPattern - 1 || (currentSet[0] == ABC && reviewCount < 6 - 1)) ))
			{
				
				currentBead = getFromPattern(reviewCount);
				currentBead.glowOn();
				
				word = "5C-Instrument Sounds/5C-" + currentBead.Type.ToLower() + "-sound";
				if(notOver)
				{
					playSound(word, REVIEW);
				}
				else
				{
					playSound(word, ENDREVIEW);
				}
				reviewCount++;
			}
			else if((!correctionReview && reviewCount < inPattern) || (correctionReview && (reviewCount < inPattern || (currentSet[0] == ABC && reviewCount < 6)) ))
			{
				currentBead = getFromPattern(reviewCount);
				currentBead.glowOn();
				
				if(notOver)
				{
					word = "5C-Instrument Sounds/5C-" + currentBead.Type.ToLower() + "-sound";
					playSound(word, REVIEW);
				}
				else
				{
					word = "5C-Instrument Sounds/5C-" + currentBead.Type.ToLower() + "-sound";
					playSound(word, ENDREVIEW);
				}
				reviewCount++;
			}    
			else
			{
				if(notOver) {
					
					playSound("5C-Instrument Sounds/5C-" + getFromPattern(inPattern).Type.ToLower() + "-sound", COMESNEXT);
				}
				else
				{
					
					OnAudioWordComplete(THIS_IS);
				}
				reviewCount = -1;
				correctionReview = false;
			}
		}
		
		private void endAfterPause ()
		{
			OnAudioWordComplete(THIS_IS);
		}
		
		private void finishPattern ()
		{
			while(inPattern < beads.Count)
			{
				tweenToPattern( getFromPattern(inPattern) );
			}
			OnAudioWordComplete(THEME_COMPLIMENT);
		}
		
		private void allGlowOff ()
		{
			foreach(PatterningBead pb in beads)
			{
				pb.glowOff();
			}
		}
		
		public override void onTimerComplete ()
		{
			base.onTimerComplete();
			
			timeoutCount++;
			
			string clipName = "";
			if(_level == SkillLevel.Tutorial)
			{
				if(timeoutCount == 1)
				{
					playSound("5C-Skill Instruction Audio/5C-now-its-your-turn", NOW_ITS);
					
					clipName = "5C-Skill Instruction Audio/5C-now-its-your-turn";
				}
				else
				{
					if(opportunityAnswered)
					{
						opportunityComplete = true;
						opportunityCorrect = false;
						OnAudioWordComplete(NOW_LETS);
						return;
					}
					opportunityAnswered = true;
					opportunityCorrect = true;
					
					playSound("5C-Skill Instruction Audio/5C-try-again", AGAIN);
					
					clipName = "5C-Skill Instruction Audio/5C-try-again";
				}
			}
			else
			{
				switch(timeoutCount)
				{
				case 1:
					OnAudioWordComplete(NOW_ITS);
					clipName = "5C-Skill Instruction Audio/5C-drag-the-next-instrument";
					
					break;
				case 2:
					playSound("5C-Skill Instruction Audio/5C-touch-the-green-button", PLEASE_FIND);
					
					clipName = "5C-Skill Instruction Audio/5C-touch-the-green-button";
					break;
				case 3:
					opportunityAnswered = true;
					opportunityCorrect = false;
					
					getFromPattern(inPattern).glowOn();
					OnAudioWordComplete(NOW_ITS);
					
					clipName = "5C-Skill Instruction Audio/5C-drag-the-next-instrument";
					break;
				case 4:
					timeoutEnd = true;
					beadsOff();
					finishPattern();
					OnAudioWordComplete(THEME_COMPLIMENT);
					
					clipName = "pattern";
					for( int i = 1; i < currentSet.Count; i++ )
						clipName += "-" + currentSet[i].ToLower();
					break;
				}
			}
			
			if( !string.IsNullOrEmpty( clipName ) )
			{
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf( clipName ) );
			}
		}
		
		private void playSound ( string clipName ) { playSound(clipName, ""); }
		private void playSound ( string clipName, string word )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.PATTERNING, clipName );
			
			if(clip == null)
			{
				DebugConsole.LogError("Patterning || Audio file not found: " + clipName + ".wav");
				if( word != "")
					OnAudioWordComplete(word);
			}
			else
			{
				if(word != "")
					bundle.AddClip(clip, word, clip.length);
				else
					bundle.AddClip(clip);
				
				SoundEngine.Instance.PlayBundle(bundle);
			}
		}
		
		public override void Resize (int _width, int _height)
		{
			this.width = _width;
			this.height = _height;
			container.scaleX = container.scaleY = _width/2048f;
			
			container.y = (_height - (1536f * container.scaleY))/2;
			container.x = 0;
		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = "";
			
			foreach(List<string> ls in setList)
			{
				if(data != "")
					data = data + ";";
				foreach(string s in ls)
				{
					if(data.LastIndexOf(';') != data.Length - 1)
						data = data + "-";
					data = data + s;
				}
			}
			return data;
		}
		
		private void parseSessionData (string data)
		{
			setList = new List<List<string>>();
			
			string[] sData = data.Split(';');
			
			foreach( string s in sData )
			{
				setList.Add(s.Split('-').ToListUtil());
			}
		}
		
		private static List<string> audioNames = new List<string>
		{	"",
			"5C-Story Introduction Audio/5C-story-introduction",
			"5C-Skill Instruction Audio/5C-watch-closely",
			"5C-Skill Instruction Audio/5C-drag-the-next-instrument",
			"5C-Skill Instruction Audio/5C-let-the-music-game-begin",
			"5C-Skill Instruction Audio/5C-now-its-your-turn",
			"5C-Skill Instruction Audio/5C-touch-the-green-button",
			"5C-Skill Instruction Audio/5C-try-again",
			"5C-Skill Instruction Audio/5C-watch-and-listen-to-the-pattern.wav",
			"","","","","","","","","","","","","","",//manifest skips 28 items...
			"","","","","","","","","","","","","","",
			"pattern-tomtom-bell",
			"pattern-clave-guitar",
			"pattern-clave-maraca",
			"pattern-clave-trumpet",
			"pattern-clave-xylophone",
			"pattern-clave-triangle",
			"pattern-guitar-trumpet",
			"pattern-banjo-trumpet",
			"pattern-banjo-xylophone",
			"pattern-maraca-banjo",
			"pattern-xylophone-guitar",
			"pattern-xylophone-tambourine",
			"pattern-tambourine-clave",
			"pattern-triangle-banjo",
			"pattern-triangle-maraca",
			"pattern-triangle-xylophone",
			"pattern-bell-tomtom",
			"pattern-bell-harmonica",
			"pattern-tomtom-flute",
			"pattern-tomtom-harmonica",
			"pattern-flute-bell",
			"pattern-violin-bell",
			"pattern-violin-pipe",
			"pattern-pipe-trombone",
			"pattern-pipe-chimes",
			"pattern-harmonica-pipe",
			"pattern-frenchhorn-keyboard",
			"pattern-trombone-flute",
			"pattern-keyboard-violin",
			"pattern-keyboard-chimes",
			"pattern-chimes-frenchhorn",
			"pattern-castanets-woodenblock",
			"pattern-castanets-bassdrum-cymbals",
			"pattern-rainstick-castanets-whistle",
			"pattern-rainstick-whistle",
			"pattern-kalimba-cymbals",
			"pattern-kalimba-cabasa-rainstick",
			"pattern-whistle-accordion-harp",
			"pattern-bassdrum-castanets",
			"pattern-bassdrum-cymbals-cabasa",
			"pattern-cabasa-kalimba-woodenblock",
			"pattern-cabasa-kalimba",
			"pattern-cymbals-rainstick-cabasa",
			"pattern-cymbals-woodenblock",
			"pattern-cymbals-accordion-whistle",
			"pattern-cymbals-bassdrum",
			"pattern-harp-cabasa-accordion",
			"pattern-harp-cabasa",
			"pattern-accordion-castanets",
			"pattern-woodenblock-harp-rainstick",
			"pattern-woodenblock-accordion",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-bell-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-tomtom-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-castanets-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-rainstick-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-kalimba-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-clave-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-whistle-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-bassdrum-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-cabasa-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-flute-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-guitar-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-violin-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-pipe-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-harmonica-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-banjo-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-frenchhorn-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-trombone-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-maraca-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-trumpet-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-cymbals-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-harp-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-xylophone-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-tambourine-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-keyboard-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-accordion-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-triangle-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-woodenblock-was-next",
			"5C-Reinforcement Audio/5C-Was Next/5C-the-chimes-was-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-bell-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-tomtom-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-castanets-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-rainstick-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-kalimba-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-clave-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-whistle-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-bassdrum-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-cabasa-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-flute-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-guitar-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-violin-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-pipe-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-harmonica-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-banjo-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-frenchhorn-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-trombone-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-maraca-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-trumpet-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-cymbals-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-harp-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-xylophone-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-tambourine-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-keyboard-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-accordion-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-triangle-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-woodenblock-comes-next",
			"5C-Reinforcement Audio/5C-Comes Next/5C-the-chimes-comes-next",
			"User-Earns-Artifact",
			"5C-Instrument Sounds/5C-bell-sound",
			"5C-Instrument Sounds/5C-tomtom-sound",
			"5C-Instrument Sounds/5C-castanets-sound",
			"5C-Instrument Sounds/5C-rainstick-sound",
			"5C-Instrument Sounds/5C-kalimba-sound",
			"5C-Instrument Sounds/5C-clave-sound",
			"5C-Instrument Sounds/5C-whistle-sound",
			"5C-Instrument Sounds/5C-bassdrum-sound",
			"5C-Instrument Sounds/5C-cabasa-sound",
			"5C-Instrument Sounds/5C-flute-sound",
			"5C-Instrument Sounds/5C-guitar-sound",
			"5C-Instrument Sounds/5C-violin-sound",
			"5C-Instrument Sounds/5C-pipe-sound",
			"5C-Instrument Sounds/5C-harmonica-sound",
			"5C-Instrument Sounds/5C-banjo-sound",
			"5C-Instrument Sounds/5C-frenchhorn-sound",
			"5C-Instrument Sounds/5C-trombone-sound",
			"5C-Instrument Sounds/5C-maraca-sound",
			"5C-Instrument Sounds/5C-trumpet-sound",
			"5C-Instrument Sounds/5C-cymbals-sound",
			"5C-Instrument Sounds/5C-harp-sound",
			"5C-Instrument Sounds/5C-xylophone-sound",
			"5C-Instrument Sounds/5C-tambourine-sound",
			"5C-Instrument Sounds/5C-keyboard-sound",
			"5C-Instrument Sounds/5C-accordion-sound",
			"5C-Instrument Sounds/5C-triangle-sound",
			"5C-Instrument Sounds/5C-woodenblock-sound",
			"5C-Instrument Sounds/5C-chimes-sound"
		};
	}
}