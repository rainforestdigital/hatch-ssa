using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.geom;

namespace SSGCore
{
	public class BlendingSoundsInWords : BaseSkill
	{
		private DisplayObjectContainer container;
		private FilterMovieClip sign;
		private FilterMovieClip signImage;
		private FilterMovieClip birdStage;
		private FilterMovieClip targetBox;

		private MovieClip birdStage_mc;
		private MovieClip targetBox_mc;

		private FilterMovieClip currentBird;
		private FilterMovieClip targetBird;
		private FilterMovieClip selectedStageBird;
		
		private List<FilterMovieClip> glowList;

		private BSIW_Opp[] TTOpps = new BSIW_Opp[] 
		{
			new BSIW_Opp ("Dog", "d", "o", "g", "", BSIW_missingType.initial)
		};

		private BSIW_Opp[] EGOpps = new BSIW_Opp[] 
		{
			new BSIW_Opp ("Ball", "b", "aw", "l", "", BSIW_missingType.initial),
			new BSIW_Opp ("Bat", "b", "aa", "t", "", BSIW_missingType.initial),
			new BSIW_Opp ("Dog", "d", "o", "g", "", BSIW_missingType.initial),
			new BSIW_Opp ("Cup", "c", "u", "p", "m", BSIW_missingType.ending),
			new BSIW_Opp ("Fig", "f", "i", "g", "n", BSIW_missingType.initial),
			new BSIW_Opp ("Log", "l", "o", "g", "", BSIW_missingType.ending),
			new BSIW_Opp ("Mat", "m", "aa", "t", "", BSIW_missingType.ending),
			new BSIW_Opp ("Pen", "p", "e", "n", "", BSIW_missingType.ending),
			new BSIW_Opp ("Pot", "p", "o", "t", "", BSIW_missingType.ending),
			new BSIW_Opp ("Sad", "s", "aa", "d", "n", BSIW_missingType.initial)
		};

		private BSIW_Opp[] DGOpps = new BSIW_Opp[] 
		{
			new BSIW_Opp ("Bag", "b", "aa", "g", "", BSIW_missingType.initial),
			new BSIW_Opp ("Bed", "b", "e", "d", "", BSIW_missingType.initial),
			new BSIW_Opp ("Cat", "c", "aa", "t", "", BSIW_missingType.initial),
			new BSIW_Opp ("Fan", "f", "aa", "n", "gp", BSIW_missingType.ending),
			new BSIW_Opp ("Hat", "h", "aa", "t", "", BSIW_missingType.medial),
			new BSIW_Opp ("Jet", "j", "e", "t", "", BSIW_missingType.ending),
			new BSIW_Opp ("Leg", "l", "e", "g", "", BSIW_missingType.ending),
			new BSIW_Opp ("Man", "m", "aa", "n", "", BSIW_missingType.ending),
			new BSIW_Opp ("Nap", "n", "aa", "p", "fjd", BSIW_missingType.initial),
			new BSIW_Opp ("Pan", "p", "aa", "n", "", BSIW_missingType.ending)
		};

		private BSIW_Opp[] DDOpps = new BSIW_Opp[] 
		{
			new BSIW_Opp ("Bug", "b", "u", "g", "", BSIW_missingType.initial),
			new BSIW_Opp ("Bun", "b", "u", "n", "", BSIW_missingType.initial),
			new BSIW_Opp ("Hen", "h", "e", "n", "l", BSIW_missingType.ending),
			new BSIW_Opp ("Mud", "m", "u", "d", "", BSIW_missingType.medial),
			new BSIW_Opp ("Mug", "m", "u", "g", "", BSIW_missingType.medial),
			new BSIW_Opp ("Nut", "n", "u", "t", "", BSIW_missingType.medial),
			new BSIW_Opp ("Pig", "p", "i", "g", "", BSIW_missingType.medial),
			new BSIW_Opp ("Red", "r", "e", "d", "", BSIW_missingType.ending),
			new BSIW_Opp ("Sun", "s", "u", "n", "", BSIW_missingType.ending),
			new BSIW_Opp ("Web", "w", "e", "b", "", BSIW_missingType.ending)
		};

		private BSIW_Opp[] availableOptions;
		private List<BSIW_Opp> allOpps = new List<BSIW_Opp>();
		private List<int> usedList;
		private BSIW_Opp currentOpp;

		private int[] availableQuestionTypes;
		private int[] TT_QuestionTypes = new int[]{1, 0, 0};
		private int[] EG_QuestionTypes = new int[]{5, 0, 5};
		private int[] DG_QuestionTypes = new int[]{3, 2, 5};
		private int[] DD_QuestionTypes = new int[]{2, 5, 3};

		private Platforms selectedPlatform;

		private float baseScale = 1f;
		private float birdScale = 1f;
		private float[] offset = new float[]{0,0};

		private string audioPrecursor = "1G-";
		private string audioFolder_InactivityPrompt 	= "Narration/1G-Inactivity Prompt Audio/";
		private string audioFolder_Reinforcement 		= "Narration/1G-Reinforcement Audio/";
		private string audioFolder_SkillInstruction 	= "Narration/1G-Skill Instructions Audio/";

		private string audioFolder_BirdSounds 			= "Narration/1G-Skill Instructions Audio/1G-Bird Sounds/";
		private string audioFolder_InitialInstructions 	= "Narration/1G-Skill Instructions Audio/1G-Initial Instructions/";

		private string audioFolder_InitialQuestions		= "Narration/1G-Skill Instructions Audio/1G-Initial-Question/";

		private BlendingSoundsInWordsTutorial tutorial;

		private List<string> initialDecoys = new List<string> (){"b", "c", "d", "f", "h", "j", "l", "m", "n", "p", "r", "s", "w"};
		private List<string> medialDecoys = new List<string> (){"aa", "aw", "e", "i", "o", "u"};
		private List<string> endingDecoys = new List<string> (){"g", "l", "t", "d", "n", "p", "b"};

		private bool pictureFadeOut;
		private bool pictureFadedIn = false;
		private List<FilterMovieClip> stageBirds;
		private List<FilterMovieClip> answerBirds;

		private const string INSTRUCTION_STEPPER = "Step Instruction";
		private const string REVEAL_ANSWER_BIRDS = "Reveal answer birds";
		private const string SPELL_WORD = "Spell Word";
		private const string SPELL_WORD_PAUSE = "Spell Word Pause";
		private const string OPP_SOLVED_PAUSE = "Spell Word Complete Paused";
		private const string RESTART_TIMER = "Restart Timer";

		private const string REPEAT_INSTRUCTION = "Repeat Tutorial Instruction";
		private const string HIGHLIGHT_TUTORIAL_BIRD = "Highlight Tutorial Bird";

		private const string CLOSE_BIRD_MOUTH = "Close bird mouth";
		private const string CLOSE_STAGE_BIRD_MOUTH = "Close stage bird mouth";

		private const string RESTART_TUTORIAL = "Restart Tutorial";

		private const string SIGN_HIGHLIGHT_OFF = "Turn off sign highlight";

		private const string OPP_SOLVED = "Solved for Player";

		private const string FADE_IN_SPOTLIGHT = "Fade Spotlight In";
		private const string FADE_OUT_SPOTLIGHT = "Fade Spotlight Out";

		private const string INSTRUCTION_REPEATED = "Instruction repeated";

		private int currentInstructionNo = 0;
		private int currentAnswerBirdStepper = 0;

		private BSIW_missingType currentMissingType;

		private float boundsThreshold = 50f;

		private int correctAnswerIndex = 0;
		private List<string> answerOptions;
		private int[] birdHaircuts = new int[]{1,1,1};

		private int spellWordLetterIndex = 0;

		private int inactivityCounter = 0;
		private bool solvedForPlayer = false;

		private int currentStageSpotlight = 0;

		private float spotlightDelay = 0.3f;

		private TimerObject questionSignTimer;
		private TimerObject reinforcementWaitTimer;
		private TimerObject glowTimer;

		private float signAppearTime = 4.2f;

		public BlendingSoundsInWords (SkillLevel level, SessionInfo  currentSession) : base(level, currentSession)
		{
			
			BACKGROUND = "Background";
			ARTIFACT = "Artifact";
			
			selectedPlatform = PlatformUtils.GetPlatform ();
			//selectedPlatform = Platforms.ANDROID;
			PreCacheAssets();
		}
		
		private void PreCacheAssets()
		{
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle("BlendingSoundsInWords", OnAssetsLoaded));
		}
		
		void OnAssetsLoaded(AssetBundle bundle)
		{
			if( bundle != null )
			{
				Init( null );
			}
		}
		
		public override void Dispose ()
		{
			base.Dispose ();
			
			if( stage != null )
			{
				stage.removeEventListener(MouseEvent.MOUSE_MOVE, onBirdMove);
				stage.removeEventListener(MouseEvent.MOUSE_UP, onBirdUp);
			}
			
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;

			destroyBirds (stageBirds);
			destroyBirds (answerBirds);
			
			if( sign != null )
				sign.Destroy();
			if( signImage != null )
				signImage.Destroy();
			
			if (tutorial != null) {
				tutorial.Dispose();
			}


			if (reinforcementWaitTimer != null) {
				reinforcementWaitTimer.StopTimer();
			}
			
			if( questionSignTimer != null )
				questionSignTimer.StopTimer();
			
			if( glowTimer != null )
				glowTimer.StopTimer();
		}

		public override void Init (MovieClip parent)
		{
			base.Init (parent);
			
			float _width = Screen.width;
			float _height = Screen.height;
			
			container = new DisplayObjectContainer ();
			container.width = _width;
			container.height = _height;
			
			container.alpha = 0;
			container.visible = false;
			addChild (container);
			
			Resize ();

			birdScale = baseScale;


			if (selectedPlatform == Platforms.WIN) {
				birdScale -= 0.02f;
			}

			setAllOps ();
			setWordList (_level);
			usedList = new List<int>();

			generateStage ();
			generateTargetBox ();
			generateSign ("Bag");
			generateStageBirds();

			availableQuestionTypes = new int[3];
			switch (_level) {
			case SkillLevel.Tutorial:
				TT_QuestionTypes.CopyTo(availableQuestionTypes, 0);
				break;
				
			case SkillLevel.Emerging:
				EG_QuestionTypes.CopyTo(availableQuestionTypes, 0);
				break;
				
			case SkillLevel.Developing:
				DG_QuestionTypes.CopyTo(availableQuestionTypes, 0);
				break;
				
			case SkillLevel.Developed:
			case SkillLevel.Completed:
				DD_QuestionTypes.CopyTo(availableQuestionTypes, 0);
				break;
			}

			container.visible = true;
			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;

			switch(_level)
			{
			case SkillLevel.Tutorial:
				totalOpportunities = 1;
				if(resumingSkill)
					nextOpportunity();
				else{


					tutorial = new BlendingSoundsInWordsTutorial();
					tutorial.View.addEventListener( TutorialEvent.START, onTutorialStart );
					tutorial.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
					tutorial.parentRef = this;
					tutorial.Resize(Screen.width, Screen.height);

				}
				break;
			default:
				totalOpportunities = 10;
				if(resumingSkill)
				{
					parseSessionData( currentSession.sessionData );
				}
				
				break;
			}


			if ((_level == SkillLevel.Developed) || (_level == SkillLevel.Completed)) {
				pictureFadeOut = true;
			}


		}

		public void Resize()
		{
			resize (Screen.width, Screen.height);	
		}

		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = "";

			foreach (int i in availableQuestionTypes) {
				if(data != "")
					data = data + "-";
				data = data + i.ToString();
			}

			data += "/";
			
			foreach(int i in usedList)
			{
				if(data.LastIndexOf('/') != data.Length - 1)
					data = data + "-";
				data = data + i.ToString();
			}
			
			data += "/" + ((int)currentMissingType).ToString();
			
			return data;
		}
		
		private void parseSessionData (string data)
		{
			usedList = new List<int>();
			availableQuestionTypes = new int[3]{0,0,0};

			string[] sData = data.Split('/');

			string[] remainingTypes = sData [0].Split ('-');
			for (int counter = 0; counter < 3; counter++) {
				availableQuestionTypes[counter] = int.Parse(remainingTypes[counter]);
			}

			string[] usedData = sData [1].Split ('-');

			foreach( string s in usedData )
			{
				usedList.Add( int.Parse(s) );
			}
			
			if( sData.Length > 2 )
				currentMissingType = (BSIW_missingType)int.Parse( sData[2] );
			else
				currentMissingType = BSIW_missingType.initial;
		}

		public override void OnAudioWordComplete ( string word )
		{
			switch (word) {
			case THEME_INTRO:
				if (!currentSession.skipIntro)
					playSound ("story-introduction", INTRO);
				else
					OnAudioWordComplete (INTRO);
				break;

			case INTRO:
				if (_level == SkillLevel.Tutorial) {
					tutorial.OnStart ();
				} else {
					nextOpportunity ();
				}
				break;

			case NOW_ITS:
				interactOff();
				playInstruction();
				break;

			case PLEASE_FIND:
				signHighlightOff();
				interactOn();
				break;

			case CLICK_CORRECT:
				dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
				break;
				
			case CLICK_INCORRECT:
				dispatchEvent( new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
				break;
				
			case THEME_COMPLIMENT:

				questionSignTimer = TimerUtils.SetTimeout(1.6f, timerSignHighlight);
				reinforcementWaitTimer = TimerUtils.SetTimeout(3.3f, openStageMouth);
				playSound(getReinforcement(currentOpp), OPP_SOLVED_PAUSE);

				break;
				
			case THEME_CRITICISM:

				if (pictureFadeOut) {
					if (!pictureFadedIn) {
						fadeOutPictorial();
					}
				}

				handleCritique();
				break;

			case YOU_FOUND:
				fadeOutSpotlight();
				clearGlowGroup(stageBirds[spellWordLetterIndex-1]);
				animateStageBird(spellWordLetterIndex-1, birdHaircuts[spellWordLetterIndex-1], true, false);
				
				if(_level == SkillLevel.Tutorial)
				{
					playSound(audioFolder_SkillInstruction + audioPrecursor + "lets-help-the-birds-sing", NOW_LETS);
					break;
				} 
				goto case NOW_LETS;

			case NOW_LETS:

				fadeOutContainer();

				if (solvedForPlayer) {
					opportunityAnswered = false;
					opportunityCorrect = false;
					opportunityComplete = true;
					
					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
				} else {
					if(opportunityCorrect)
					{
						dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
					}
					else
					{
						dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
					}
				}
				break;
				
			case YOU_FOUND_INCORRECT:

				// Close mouth
				//int birdIndex = answerBirds.IndexOf (currentBird);
				animateAnswerBirds(currentBird, (int) currentMissingType, birdHaircuts[answerBirds.IndexOf (currentBird)], false);
				


				break;

			case INSTRUCTION_REPEATED:
				signHighlightOff();
				interactOn();
				break;

			case INSTRUCTION_STEPPER:
				playInstruction();
				break;

			case FADE_IN_SPOTLIGHT:
				signHighlightOff();
				fadeInSpotlight(currentStageSpotlight);
				Tweener.addTween(birdStage.Target.getChildAt(0), Tweener.Hash("time", spotlightDelay)).OnComplete(playInstruction);
				break;

			case FADE_OUT_SPOTLIGHT:

				int tempBirdNo = currentInstructionNo - 1;
				if (tempBirdNo > 0) {
					closeStageBirdMouth();
					clearGlowGroup(stageBirds[tempBirdNo-1]);
				}

				fadeOutSpotlight();
				
				currentStageSpotlight++;
				Tweener.addTween(birdStage.Target.getChildAt(0), Tweener.Hash("time", spotlightDelay)).OnComplete(checkNextSpotlightStep);
				break;

			case REVEAL_ANSWER_BIRDS:
				signHighlightOff();
				revealAnswerBirds();
				break;

			case SPELL_WORD_PAUSE:
				// Close open mouths
				completeBirdWord();
				Tweener.addTween (stage, Tweener.Hash ("time", 0.1f)).OnComplete(spellWord);
				break;

			case SPELL_WORD:

				spellWord();

				break;

			case RESTART_TIMER:
				startTimer ();
				break;

			case REPEAT_INSTRUCTION:
				playSound(getInitialInstruction(currentOpp), HIGHLIGHT_TUTORIAL_BIRD); // Highlight bird
				questionSign_Timeout(signAppearTime);
				break;

			case HIGHLIGHT_TUTORIAL_BIRD:
				signHighlightOff();
				glowOnGroup(answerBirds[0]);
				interactOn();
				break;

			case RESTART_TUTORIAL:
				restart();
				break;

			case CLOSE_BIRD_MOUTH:
				closeCurrentBirdMouth();
				break;

			case CLOSE_STAGE_BIRD_MOUTH:
				closeStageBirdMouth();
				break;

			case SIGN_HIGHLIGHT_OFF:
				startTimer ();
				signHighlightOff();
				break;

			case OPP_SOLVED:

				opportunityAnswered = false;
				opportunityCorrect = false;
				opportunityComplete = true;
				
				questionSignTimer = TimerUtils.SetTimeout(1.6f, timerSignHighlight);
				reinforcementWaitTimer = TimerUtils.SetTimeout(3.3f, openStageMouth);
				
				playSound(getReinforcement(currentOpp), OPP_SOLVED_PAUSE);
				break;

			case OPP_SOLVED_PAUSE:
				clearFilters();
				completeBirdWord();
				Tweener.addTween (stage, Tweener.Hash ("time", 0.1f)).OnComplete(spellWord);

				break;
			}
		}

		private void openStageMouth() {

			glowOnGroup (stageBirds [(int)currentMissingType]);
			animateStageBird((int)currentMissingType, birdHaircuts[(int)currentMissingType], true, true);

		}

		public override void nextOpportunity()
		{
			DebugConsole.Log ("Next Opp");

			if(currentOpportunity == totalOpportunities)
			{
				tallyAnswers();
				return;
			}

			opportunityAnswered = false;
			opportunityComplete = false;
			solvedForPlayer = false;
			timeoutCount = 0;
			pictureFadedIn = false;
			currentInstructionNo = 0;
			currentAnswerBirdStepper = 0;
			spellWordLetterIndex = 0;
			currentStageSpotlight = 0;

			currentBird = null;
			selectedStageBird = null;

			
			if (_level == SkillLevel.Tutorial) {

				int oppIndex = 0;
				currentOpp = availableOptions[oppIndex];
				usedList.Add(oppIndex);

			} else {
				int randomIndex = 0;
				
				if( resumingSkill && usedList.Count > 0 )
				{
					randomIndex = usedList[ usedList.Count - 1 ];
				}
				else
				{
					bool indexFound = false;
					
					while (!indexFound) {
						randomIndex = Random.Range(0, availableOptions.Length);
						indexFound = checkIfAvailable(randomIndex);
						
					}
	
					usedList.Add(randomIndex);
				}
				
				currentOpp = availableOptions[randomIndex];

			}

			// Set the sign
			string displayWord = currentOpp.targetWord;
			
			if (pictureFadeOut) {
				displayWord += "2";
			}
			
			changeSignImage(displayWord);
			
			assignQuestionType();

			randomiseHaircuts();
			
			resetStageBirds();
			
			generateAnswerBirds(determineAnswerBirdAmount());
			
			targetBird = stageBirds[(int) currentMissingType];
			container.alpha = 0f;

			setSpotlightLocation (0);


			if (_level == SkillLevel.Tutorial) {

				answerOptions = new List<string> ();
				answerOptions.Add ( "d" );
				answerOptions.Add ( "" );

				correctAnswerIndex = 0;
				answerBirds[0].alpha = 1f;
				answerBirds[1].alpha = 0f;
				answerBirds[1].x = -5000f;

			} else {
				setAnswers ();
			}
			
			AddNewOppData( audioNames.IndexOf( getInitialInstruction(currentOpp) ) );
			
			AddNewObjectData( formatObjectData("sign", sign.parent.localToGlobal(new Vector2(sign.x, sign.y)) ) );
			
			Vector2 point;
			int i = -1;
			foreach( FilterMovieClip fmc in stageBirds )
			{
				i++;
				if( !(fmc.alpha > 0f) )
					continue;
				
				point = fmc.parent.localToGlobal( new Vector2( fmc.x, fmc.y ) );
				AddNewObjectData( formatObjectData( "stage:" + (i == 0 ? currentOpp.initial : (i == 1 ? currentOpp.medial : currentOpp.ending)), point ) );
			}
			i = 0;
			foreach( FilterMovieClip fmc in answerBirds )
			{
				if(_level == SkillLevel.Tutorial && i != correctAnswerIndex )
					continue;
				
				point = fmc.parent.localToGlobal( new Vector2( fmc.x, fmc.y ) );
				
				string letter = getBirdLetter(i).Split('-')[getBirdLetter(i).Split('-').Length - 1];
				AddNewObjectData( formatObjectData( letter, point, i == correctAnswerIndex ) );
				
				i++;
			}

			currentOpportunity++;

			fadeInContainer();
			interactOff ();
			dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_START, true, false) );
		}

		private void setSpotlightLocation(int nestNo) {

			string spotlightLabel = "nospotlight";

			if (nestNo > 0) {
				spotlightLabel = "spotlight" + nestNo.ToString();
			}

			birdStage.Target.gotoAndStop (spotlightLabel);
		}

		private bool checkIfAvailable(int index) {
			if (usedList.Contains(index)) {
				return false;
			}	
			
			return true;
		}

		private void assignQuestionType() {
			
			if( resumingSkill )
			{
				resumingSkill = false;
				return;
			}
			
			List<int> availableType_Indexes = new List<int> ();

			for (int counter = 0; counter < 3; counter++) {
				if (availableQuestionTypes[counter] > 0) {
					availableType_Indexes.Add(counter);
				}
			}

			int randomIndex = availableType_Indexes[Random.Range (0, availableType_Indexes.Count)];
			availableQuestionTypes [randomIndex]--;

			currentMissingType = (BSIW_missingType)randomIndex;
		}

		private int determineAnswerBirdAmount() {

			int birdCount = 2;

			switch (_level) {

			case SkillLevel.Emerging:
				if (currentMissingType == BSIW_missingType.initial) {
					birdCount = 3;
				} else {
					birdCount = 2;
				}
				break;

			case SkillLevel.Developing:

				switch(currentMissingType) {
				case BSIW_missingType.initial:
					birdCount = 3;
					break;

				case BSIW_missingType.medial:
					birdCount = 2;
					break;

				case BSIW_missingType.ending:
					birdCount = 3;
					break;
				}
				break;

			case SkillLevel.Developed:
			case SkillLevel.Completed:
				switch(currentMissingType) {
				case BSIW_missingType.initial:
					birdCount = 3;
					break;
					
				case BSIW_missingType.medial:
					birdCount = 2;
					break;
					
				case BSIW_missingType.ending:
					birdCount = 3;
					break;
				}
				break;

			}

			return birdCount;
		}

		private void setAnswers() {
			correctAnswerIndex = Random.Range (0, answerBirds.Count);
			answerOptions = new List<string> ();

			bool naughtyWordCheck = false;

			if (currentOpp.avoidDecoy.Length > 0) {
				naughtyWordCheck = true;
			}

			for (int counter = 0; counter < answerBirds.Count; counter++) {
				switch(currentMissingType) {
				case BSIW_missingType.initial:
					if (counter == correctAnswerIndex) {
						answerOptions.Add(currentOpp.initial);
					} else {
						// Find a random that isn't the onset and hasn't been used
						string randomInitial = currentOpp.initial;
						
						while (randomInitial.ToUpper() == currentOpp.initial.ToUpper()) {
							randomInitial = getRandomString(answerOptions, initialDecoys, currentOpp.initial);

							if (naughtyWordCheck) {
								if (currentOpp.avoidDecoy.Contains(randomInitial)) {
									DebugConsole.Log("Naughty letter found - swapping " + randomInitial);
									randomInitial = currentOpp.initial;
								}
							}

						}
						
						answerOptions.Add ( randomInitial );
					}
					break;
					
				case BSIW_missingType.medial:
					if (counter == correctAnswerIndex) {
						answerOptions.Add(currentOpp.medial);
					} else {
						// Find a random that isn't the onset and hasn't been used
						string randomMedial = currentOpp.medial;
						
						while (randomMedial.ToUpper() == currentOpp.medial.ToUpper()) {
							randomMedial = getRandomString(answerOptions, medialDecoys, currentOpp.medial);
														
							if (naughtyWordCheck) {
								if (currentOpp.avoidDecoy.Contains(randomMedial)) {
									DebugConsole.Log("Naughty letter found - swapping " + randomMedial);
									randomMedial = currentOpp.medial;
								}
							}
						}
						
						answerOptions.Add ( randomMedial );
					}
					break;

				case BSIW_missingType.ending:
					if (counter == correctAnswerIndex) {
						answerOptions.Add(currentOpp.ending);
					} else {
						// Find a random that isn't the onset and hasn't been used
						string randomEnding = currentOpp.ending;
						
						while (randomEnding.ToUpper() == currentOpp.ending.ToUpper()) {
							randomEnding = getRandomString(answerOptions, endingDecoys, currentOpp.ending);

							if (naughtyWordCheck) {
								if (currentOpp.avoidDecoy.Contains(randomEnding)) {
									DebugConsole.Log("Naughty letter found - swapping " + randomEnding);
									randomEnding = currentOpp.ending;
								}
							}
						}
						
						answerOptions.Add ( randomEnding );
					}
					break;
				}
			}
		}

		private FilterMovieClip generateSign(string displayImage) {
			sign = new FilterMovieClip (MovieClipFactory.CreateBlendingSoundsInWords_Sign ());
			sign.scaleX = sign.scaleY = baseScale;
			sign.x = birdStage.x;
			sign.y = birdStage.y - (birdStage.height * 0.39f);

			
			if (selectedPlatform == Platforms.WIN) {
				sign.x += 16f;
			}
			
			container.addChild (sign);
			changeSignImage (displayImage);

			return sign;

		}
		
		private void changeSignImage (string displayImage) {
			sign.Target.gotoAndStop (displayImage.ToUpper());
			signImage = addFilter ((MovieClip) sign.Target.getChildAt (1));

		}

		private void fadeOutPictorial() {
			pictureFadedIn = true;
			Tweener.addTween (sign.Target.getChildAt (1), Tweener.Hash ("time", 0.25f, "alpha", 0)).OnComplete(switchImage);
		}
		
		private void fadeInPictorial() {
			Tweener.addTween (sign.Target.getChildAt (1), Tweener.Hash ("time", 0.25f, "alpha", 1));
		}
		
		private void switchImage() {
			changeSignImage (currentOpp.targetWord);
		}


		private void generateStage() {

			birdStage_mc = MovieClipFactory.CreateBlendingSoundsInWords_Stage ();
			birdStage_mc.gotoAndStop (0);

			birdStage = new FilterMovieClip (birdStage_mc);
			birdStage.scaleX = birdStage.scaleY = baseScale;
			birdStage.x = (container.width * baseScale) * 0.5f;
			birdStage.y = (container.height * baseScale) * 0.44f;
			
			container.addChild (birdStage);
		}

		private void generateTargetBox() {
			targetBox_mc = MovieClipFactory.CreateBlendingSoundsInWords_TargetBox ();
			targetBox = new FilterMovieClip (targetBox_mc);
			targetBox.scaleX = (baseScale + 0.4f);
			targetBox.scaleY = (baseScale + 1f);
			targetBox.x = birdStage.x;
			targetBox.y = birdStage.y - (birdStage.height * 0.39f);

			if (selectedPlatform == Platforms.WIN) {
				targetBox.y -= 50f;
			}
			targetBox.alpha = 0f;

			container.addChild (targetBox);
		}

		private void generateStageBirds() {
			destroyBirds (stageBirds);
			stageBirds = new List<FilterMovieClip> ();

			for ( int counter = 0; counter < 3; counter++ ) {
				stageBirds.Add(generateStageBird((BSIW_missingType) counter));
				
				// Generate the glow
				//generateBirdGlow (stageBirds[counter], true, counter);
			}

			positionStageBirds ();
		}

		private void positionStageBirds() {
			for (int counter = 0; counter < stageBirds.Count; counter++) {

				FilterMovieClip birdRef = stageBirds[counter];
				birdRef.y = birdStage.y + ((birdStage.height * baseScale) * 0.07f) ;

				birdRef.x = birdStage.x;
				float xOffset = 0.225f;

				if (selectedPlatform == Platforms.WIN) {
					xOffset = 0.25f;
				}

				switch(counter) {
				case 0:
					birdRef.x -= (birdStage.width * baseScale) * xOffset;

					if (selectedPlatform == Platforms.WIN) {
						birdRef.x -= 20f;

					}

					break;

				case 1:

					break;

				case 2:
					birdRef.x += (birdStage.width * baseScale) * xOffset;
					break;
				}

			}
		}

		private FilterMovieClip generateStageBird(BSIW_missingType birdType) {

			MovieClip bird_mc;
//			int tempBirdNo = 0;
			switch(birdType) {
			

			case BSIW_missingType.medial:
				bird_mc = MovieClipFactory.CreateBlendingSoundsInWords_MiddleBird ();
//				tempBirdNo = 1;
				break;

			case BSIW_missingType.ending:
				bird_mc = MovieClipFactory.CreateBlendingSoundsInWords_RightBird ();
//				tempBirdNo = 2;
				break;

			case BSIW_missingType.initial:
			default:
				bird_mc = MovieClipFactory.CreateBlendingSoundsInWords_LeftBird ();
				break;
			}

			bird_mc.gotoAndStop (0);

			FilterMovieClip soundBird = new FilterMovieClip (bird_mc);
			soundBird.scaleX = soundBird.scaleY = baseScale;

			container.addChild (soundBird);

			return soundBird;
		}

		private void resetStageBirds() {

			for (int counter = 0; counter < stageBirds.Count; counter++) {
				animateStageBird(counter, birdHaircuts[counter], false, false);
				clearGlowGroup(stageBirds[counter]);
			}

			stageBirds [(int)currentMissingType].alpha = 0f;
		}

		private void completeBirdWord() {
			for (int counter = 0; counter < stageBirds.Count; counter++) {
				animateStageBird(counter, birdHaircuts[counter], true, false);
			}
		}

		private void animateStageBird(int birdNo, int haircut, bool completed, bool mouthOpen) {

			string birdLabel = "STAGEBIRD";

			switch (birdNo) {
			default:
			case 0:
				birdLabel += "LEFT";
				break;

			case 1:
				birdLabel += "MID";
				break;

			case 2:
				birdLabel += "RIGHT";
				break;
			}

			if (birdNo == 0) {

				birdLabel += haircut.ToString ();

				if (completed) {
					birdLabel += "GREEN";
				}

			} else {

				if (completed) {
					birdLabel += "GREEN";
				}

				birdLabel += haircut.ToString ();
			}

			if (mouthOpen) {
				birdLabel += "OPEN";
			} else {
				birdLabel += "CLOSED";
			}

			stageBirds [birdNo].Target.gotoAndStop (birdLabel);

		}

		private void generateAnswerBirds(int noOfBirds) {
			destroyBirds (answerBirds);
			answerBirds = new List<FilterMovieClip> ();

			for (int counter = 0; counter < noOfBirds; counter++) {

				FilterMovieClip answerBird = createAnswerBird();
				container.addChild (answerBird);
				answerBirds.Add(answerBird);

				animateAnswerBirds(answerBird, (int) currentMissingType, birdHaircuts[counter], false);
			}

			positionAnswerBirds ();

		}

		private void positionAnswerBirds() {
			for (int counter = 0; counter < answerBirds.Count; counter++) {
				resetBirdPosition(counter, false);
			}
		}

		private void animateAnswerBirds(FilterMovieClip birdFMC, int birdNo, int haircut, bool mouthOpen) {
			string birdLabel = "";
			
			switch (birdNo) {
			default:
			case 0:
				birdLabel += "BLUE";
				break;
				
			case 1:
				birdLabel += "PURPLE";
				break;
				
			case 2:
				birdLabel += "YELLOW";
				break;
			}

			birdLabel += "BIRD" + haircut.ToString ();

			if (mouthOpen) {
				birdLabel += "OPEN";
			} else {
				birdLabel += "CLOSED";
			}
			
			birdFMC.Target.gotoAndStop (birdLabel);
		}

		private void resetBirdPosition (int index, bool animate) {
			float pad = (birdStage.width * baseScale) * 0.1f;
			float birdWidth = (answerBirds [0].Target.width * baseScale) + pad;
			float startX = birdStage.x - ((birdWidth * (answerBirds.Count-1)) / 2f);
			float vPos = (container.height * baseScale) * 0.7f;

			switch (selectedPlatform) {
			case Platforms.ANDROID:
				vPos = (container.height * baseScale) * 0.75f;
				break;

			case Platforms.WIN:
				vPos = (container.height * baseScale) * 0.84f;
				break;
			}

			
			int counter = 0;
			while (counter < index) {
				counter++;
				startX += birdWidth;
			}

			if (animate) {
				Tweener.addTween (currentBird, Tweener.Hash ("time", 0.25f, "x", startX, "y", vPos, "scaleX", birdScale, "scaleY", birdScale));
			} else {
				FilterMovieClip fmc = answerBirds[index];
				fmc.x = startX;
				fmc.y = vPos;
			}
		}

		private void fadeBirds(bool fadeIn) {

			float _alpha = 0f;

			if (fadeIn) {
				_alpha = 1f;
			}

			foreach (FilterMovieClip bird in answerBirds) {
				Tweener.addTween (bird, Tweener.Hash ("time", 0.25f, "alpha", _alpha));
			}
		}

		private void destroyBirds(List<FilterMovieClip> birdList) {
			if (birdList != null) {
				foreach(FilterMovieClip fmc in birdList) {
					container.removeChild(fmc);
				}
			}

			birdList = null;
		}

		#region Containers

		public float getBaseScale() {
			return baseScale;
		}
		
		//size in should be the stage size from the flash file
		private void resize( int width, int height ) { resize( (float)width, (float)height ); }
		private void resize( float width, float height )
		{
			if( container == null )
				return;
			
			float scale = (float)Screen.width / width;
			baseScale = scale;

			switch (selectedPlatform) {

			case Platforms.WIN:
				baseScale = 0.89f;
				break;

			}
			
			container.scaleX = container.scaleY = baseScale;
			container.y = Screen.height - ( baseScale * height );
			container.x = 0;
			
			if (selectedPlatform == Platforms.WIN) {
				
				float winScale = (Mathf.Sqrt( Screen.height )/Mathf.Sqrt( 1080f ) + (Screen.height/1080f)) /2f;
				
				container.scaleX = container.scaleY *=  winScale;
				container.x = 180f / winScale;
				container.y = 120f / winScale;
			}
			
		}
		
		public DisplayObjectContainer getContainer() {
			return container;
		}
		
		private void fadeInContainer()
		{
			container.visible = true;
			Tweener.addTween (container, Tweener.Hash ("time", 0.25f, "alpha", 1)).OnComplete (playInstruction);

		}
		
		private void fadeOutContainer()
		{
			Tweener.addTween(container, Tweener.Hash("time", 0.25f, "alpha", 0)).OnComplete(hideContainer);
		}
		
		private void hideContainer()
		{ 
			container.visible = false; 
		}
		#endregion

		#region Audio
		
		private void playSound ( string clipName ){ playSound(clipName, "", 0f); }
		private void playSound ( string clipName, string word) {playSound(clipName, word, 0f); }
		private void playSound ( string clipName, string word, float length )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_SOUNDS_IN_WORDS, clipName );
			
			if (word != "") {
				if (length != 0f) {
					bundle.AddClip (clip, word, length);
				} else {
					bundle.AddClip (clip, word, clip.length);
				}
			} else {
				bundle.AddClip (clip);
			}
			
			SoundEngine.Instance.PlayBundle(bundle);
		}

		private void playDing(string word) {
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			var clip = SoundUtils.LoadGlobalSound("ding");
			bundle.AddClip (clip, word, clip.length);
			SoundEngine.Instance.PlayBundle(bundle);
		}

		private void playClick() {
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			var clip = SoundUtils.LoadGlobalSound("click");
			bundle.AddClip (clip);
			SoundEngine.Instance.PlayBundle(bundle);
		}

		private string getInitialInstruction (BSIW_Opp oppDetails) {
			string filename = audioFolder_InitialInstructions + audioPrecursor + "drag-" + oppDetails.targetWord.ToLower();

			return filename;
			
		}

		private string getLetterAudio(BSIW_Opp suppliedOpp, BSIW_missingType suggestedType) {
			string letter;

			switch (suggestedType) {
			case BSIW_missingType.initial:
			default:
				letter = suppliedOpp.initial;
				break;

			case BSIW_missingType.medial:
				letter = suppliedOpp.medial;
				break;

			case BSIW_missingType.ending:
				letter = suppliedOpp.ending;
				break;
			}

			string filename = audioFolder_BirdSounds + audioPrecursor + letter;
			return filename;
		}

		private string getReinforcementLetterAudio(BSIW_Opp suppliedOpp, BSIW_missingType suggestedType) {
			string letter;
			switch (suggestedType) {
			case BSIW_missingType.initial:
			default:
				letter = suppliedOpp.initial;
				break;
				
			case BSIW_missingType.medial:
				letter = suppliedOpp.medial;

				if (letter.Length > 1) {
					letter = letter[0].ToString();
				}

				break;
				
			case BSIW_missingType.ending:
				letter = suppliedOpp.ending;
				break;
			}


			letter = letter[0].ToString();

			
			string filename = audioFolder_Reinforcement + "1G-Word Sounds/" + audioPrecursor + suppliedOpp.targetWord.ToLower() + "-" + letter;
			return filename;
		}

		private string getBirdLetter(int birdNo) {

			string suggestedLetter = answerOptions [birdNo];
			BSIW_Opp foundOpp = TTOpps[0];
			foreach (BSIW_Opp _opp in allOpps) {
				switch(currentMissingType) {
				case BSIW_missingType.initial:
					if (_opp.initial == suggestedLetter) {
						foundOpp = _opp;
						break;
					}
					break;

				case BSIW_missingType.medial:
					if (_opp.medial == suggestedLetter) {
						foundOpp = _opp;
						break;
					}
					break;

				case BSIW_missingType.ending:
					if (_opp.ending == suggestedLetter) {
						foundOpp = _opp;
						break;
					}
					break;
				}
			}

			string filename = getReinforcementLetterAudio (foundOpp, currentMissingType);
			return filename;
		}

		private string getReinforcement (BSIW_Opp oppDetails) {
			string filename = audioFolder_Reinforcement + audioPrecursor;

			string position = "";
			string missingLetter = "";

			switch (currentMissingType) {
			case BSIW_missingType.initial:
				position = "first-";
				missingLetter = oppDetails.initial;
				break;

			case BSIW_missingType.medial:
				position = "middle-";
				missingLetter = oppDetails.medial;
				break;

			case BSIW_missingType.ending:
				position = "last-";
				missingLetter = oppDetails.ending;
				break;
			}

			missingLetter = missingLetter[0].ToString();

			filename = filename + position + oppDetails.targetWord.ToLower() + "-" + missingLetter;
			return filename;
		}

		#endregion

		public override void onTutorialStart( CEvent e )
		{
			base.onTutorialStart(e);
			tutorial.View.removeEventListener( TutorialEvent.START, onTutorialStart );
			addChild(tutorial.View);
		}
		
		private void onTutorialComplete( CEvent e )
		{

			tutorial.View.removeEventListener(TutorialEvent.COMPLETE, onTutorialComplete);
			removeChild(tutorial.View);
			
			container.alpha = 1;
			nextOpportunity();
		}

		public FilterMovieClip createBird(int birdNo) {

			MovieClip tempMC;

			switch (birdNo) {
			case 1:
				tempMC = MovieClipFactory.CreateBlendingSoundsInWords_MiddleBird();
				break;

			case 2:
				tempMC = MovieClipFactory.CreateBlendingSoundsInWords_RightBird();
				break;

			default:
				tempMC = MovieClipFactory.CreateBlendingSoundsInWords_LeftBird();
				break;
			}

			tempMC.gotoAndStop (0);

			FilterMovieClip fmc = new FilterMovieClip(tempMC);
			return fmc;
		}

		public FilterMovieClip createAnswerBird() {
			FilterMovieClip answerBird = new FilterMovieClip(MovieClipFactory.CreateBlendingSoundsInWords_AnswerBird ());
			answerBird.Target.gotoAndStop(0);
			answerBird.scaleX = answerBird.scaleY = baseScale;
			answerBird.alpha = 0f;

			return answerBird;
		}

		public FilterMovieClip createSign() {
			MovieClip tempMC = MovieClipFactory.CreateBlendingSoundsInWords_Sign ();
			tempMC.gotoAndStop (0);

			return new FilterMovieClip (tempMC);
		}

		private void setAllOps() {
			addOps (TTOpps);
			addOps (EGOpps);
			addOps (DGOpps);
			addOps (DDOpps);
		}

		private void addOps(BSIW_Opp[] oppList) {
			foreach (BSIW_Opp _opp in oppList) {
				allOpps.Add(_opp);
			}
		}

		private void setWordList(SkillLevel _suggestedLevel) {
			switch (_suggestedLevel) {
			case SkillLevel.Tutorial:
				availableOptions = TTOpps;
				break;
				
			case SkillLevel.Emerging:
				availableOptions = EGOpps;
				break;
				
			case SkillLevel.Developing:
				availableOptions = DGOpps;
				break;
				
			case SkillLevel.Developed:
			case SkillLevel.Completed:
				availableOptions = DDOpps;
				break;
			}
		}

		private void playInstruction() {
			// Play audio
			string audioFileName = "";
			string command = FADE_OUT_SPOTLIGHT;
			bool delayWait = false;

			switch (currentInstructionNo) {
			case 0:
				audioFileName = audioFolder_InitialQuestions + audioPrecursor + "which-" + currentOpp.targetWord;
				currentStageSpotlight = 1;
				command = FADE_IN_SPOTLIGHT;
				questionSign_Timeout (2.5f);

				break;

			case 1:
			case 2:
			case 3:
				int tempBirdNo = currentInstructionNo - 1;
				if (currentMissingType == (BSIW_missingType)tempBirdNo) {
					// Ignore this and wait

					delayWait = true;
				} else {
					selectedStageBird = stageBirds [tempBirdNo];
					glowOnGroup (selectedStageBird);
					animateStageBird (tempBirdNo, birdHaircuts [tempBirdNo], false, true);

					audioFileName = getReinforcementLetterAudio (currentOpp, (BSIW_missingType)tempBirdNo);
				}

				break;
			}

			currentInstructionNo++;

			if (delayWait) {

				Tweener.addTween(birdStage.Target.getChildAt(0), Tweener.Hash("time", spotlightDelay)).OnComplete(blankSpaceSpotlight);
			} else {
				playSound (audioFileName, command);
			}


		}

		private void fadeInSpotlight(int _location) {
			setSpotlightLocation (_location);

		}

		private void fadeOutSpotlight() {
			setSpotlightLocation (0);

		}

		private void blankSpaceSpotlight() {
			currentStageSpotlight++;
			fadeOutSpotlight ();
			Tweener.addTween(birdStage.Target.getChildAt(0), Tweener.Hash("time", spotlightDelay)).OnComplete(checkNextSpotlightStep);
		}

		private void checkNextSpotlightStep() {
			if (currentStageSpotlight > 3) {
				// Finished - set spotlight to the missing location
				fadeInSpotlight((int)currentMissingType + 1);

				switch(_level) {
				case SkillLevel.Tutorial:
					finalStep();
					break;

				case SkillLevel.Emerging:
					playSound(audioFolder_InitialInstructions + audioPrecursor + "listen-" + currentOpp.targetWord.ToString(), REVEAL_ANSWER_BIRDS);
					questionSign_Timeout(3f);
					//playSound(audioFolder_SkillInstruction + audioPrecursor + "listen-to-the-sounds-and-repeat", REVEAL_ANSWER_BIRDS);
					break;

				default:
					revealAnswerBirds();
					break;
				}

			} else {
				fadeInSpotlight(currentStageSpotlight);
				Tweener.addTween(birdStage.Target.getChildAt(0), Tweener.Hash("time", spotlightDelay)).OnComplete(playInstruction);
			}
		}

		private void finalStep() {
			playSound(getInitialInstruction(currentOpp), PLEASE_FIND);
			questionSign_Timeout(signAppearTime);
		}

		private void revealAnswerBirds() {

			resetStageBirds ();

			if (_level == SkillLevel.Emerging) {

				if (currentAnswerBirdStepper < answerBirds.Count) {
					fadeBirdIn(answerBirds[currentAnswerBirdStepper]);
				} else {
					// Move on
					playSound(getInitialInstruction(currentOpp), PLEASE_FIND);
					questionSign_Timeout(signAppearTime);
				}

				if (currentAnswerBirdStepper > 0) {
					clearGlowGroup (answerBirds [currentAnswerBirdStepper-1]);
					animateAnswerBirds(answerBirds[currentAnswerBirdStepper-1], (int) currentMissingType, birdHaircuts[currentAnswerBirdStepper-1], false);
					Tweener.addTween (answerBirds[currentAnswerBirdStepper-1], Tweener.Hash ("time", 0.25f, "scaleX", birdScale, "scaleY", birdScale));

				}


			} else if (_level != SkillLevel.Tutorial) {
				fadeBirds(true);
				playSound(getInitialInstruction(currentOpp), PLEASE_FIND);
				questionSign_Timeout(signAppearTime);
			}	
		}

		private void fadeBirdIn(FilterMovieClip bird_mc) {
			Tweener.addTween (bird_mc, Tweener.Hash ("time", 0.25f, "alpha", 1)).OnComplete(scaleBirdUp);
		}

		private void scaleBirdUp() {
			glowOnGroup (answerBirds [currentAnswerBirdStepper]);
			Tweener.addTween (answerBirds[currentAnswerBirdStepper], Tweener.Hash ("delay", 0.5f, "time", 0.25f, "scaleX", birdScale * 1.2f, "scaleY", birdScale * 1.2f)).OnComplete(birdSay);
		}

		private void birdSay() {
			animateAnswerBirds(answerBirds[currentAnswerBirdStepper], (int) currentMissingType, birdHaircuts[currentAnswerBirdStepper], true);
			playSound (getBirdLetter (currentAnswerBirdStepper), REVEAL_ANSWER_BIRDS);
			currentAnswerBirdStepper++;
		}

		private void generateBirdGlow(FilterMovieClip fmc, bool stageBird, int birdNo) {

			for (int counter = 0; counter < 3; counter++) {
				FilterMovieClip tempFMC = new FilterMovieClip ((MovieClip)fmc.Target.getChildAt(counter));
				
				fmc.addChildAt(tempFMC, 0);
				tempFMC.alpha = 0f;
				
				tempFMC.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
				tempFMC.filters[0].Visible = true;



				/*
				if (counter == 0) {
					FilterMovieClip tempFMC_2 = new FilterMovieClip ((MovieClip)tempFMC.Target.getChildAt(1));
					
					fmc.addChild (tempFMC_2);
					tempFMC_2.alpha = 1f;
					
					tempFMC_2.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
					tempFMC_2.filters[0].Visible = true;
				}*/
			}
		}

		private void birdGlowOn(FilterMovieClip fmc, bool stageBird)
		{
			for (int counter = 0; counter < 3; counter++) {
				FilterMovieClip tempFMC = (FilterMovieClip) fmc.Target.getChildAt(counter);
				FilterMovieClip glowRef = (FilterMovieClip)(tempFMC.Target.getChildAt(0));
				glowRef.alpha = 1f;
				
			}
		}

		private void glowOn(FilterMovieClip fmc)
		{

			while(fmc.filters.Count > 1)
			{
				fmc.filters[1].Destroy();
				fmc.filters[1].Visible = false;
				fmc.filters.RemoveAt(1);
			}
			if(fmc.filters.Count == 0)
				fmc.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
			fmc.filters[0].Visible = true;
		}
		
		private void glowOff(FilterMovieClip fmc)
		{

			while(fmc.filters.Count > 1)
			{
				fmc.filters[1].Destroy();
				fmc.filters[1].Visible = false;
				fmc.filters.RemoveAt(1);
			}
			if(fmc.filters.Count == 1)
				fmc.filters[0].Visible = false;
		}

		private void glowOnGroup(FilterMovieClip fmc) {
			addFilterGroup (fmc.Target);
		}

		private void hideGlowGroup () {

		}

		private void clearGlowGroup(FilterMovieClip fmc) {
			clearFilters ();
		}

		private void interactOn() {
			startTimer ();
			MarkInstructionEnd ();
			foreach (FilterMovieClip bird in answerBirds) {
				bird.addEventListener( MouseEvent.MOUSE_DOWN, onBirdDown );
			}

			foreach (FilterMovieClip bird in stageBirds) {
				bird.addEventListener( MouseEvent.MOUSE_DOWN, onStageBirdDown );
			}

			sign.addEventListener( MouseEvent.MOUSE_DOWN, onSignClicked );
		}

		private void interactOff() {
			MarkInstructionStart ();
			foreach (FilterMovieClip bird in answerBirds) {
				bird.removeEventListener( MouseEvent.MOUSE_DOWN, onBirdDown );
			}

			foreach (FilterMovieClip bird in stageBirds) {
				bird.removeEventListener( MouseEvent.MOUSE_DOWN, onStageBirdDown );
			}

			sign.removeEventListener( MouseEvent.MOUSE_DOWN, onSignClicked );
		}

		private void onSignClicked( CEvent e) {
			timeoutCount = 0;
			signHighlightOn ();
			playSound (getInitialInstruction (currentOpp), SIGN_HIGHLIGHT_OFF);

			closeCurrentBirdMouth ();
			closeStageBirdMouth ();

			stopTimer ();
			
			AddNewActionData( formatActionData( "tap", 0, Vector2.zero ) );
		}

		private void signHighlightOn() {
			//glowOn (signImage);
			glowOn (sign);
		}

		private void signHighlightOff() {
			//glowOff (signImage);
			glowOff (sign);
		}

		private void onStageBirdDown ( CEvent e ) {
			playClick ();
			closeCurrentBirdMouth ();
			closeStageBirdMouth ();
			signHighlightOff ();

			stopTimer ();
			startTimer ();
			timeoutCount = 0;

			selectedStageBird = e.currentTarget as FilterMovieClip;
			int birdIndex = stageBirds.IndexOf (selectedStageBird);

			if ((BSIW_missingType)birdIndex != currentMissingType) {
				// Highlight
				glowOnGroup (selectedStageBird);
				// Say audio
				playSound (getReinforcementLetterAudio (currentOpp, (BSIW_missingType)birdIndex), CLOSE_STAGE_BIRD_MOUTH);
				// Bird open mouth

				animateStageBird (birdIndex, birdHaircuts [birdIndex], false, true);
			}

			AddNewActionData( formatActionData( "tap", birdIndex == 0 ? 1 : (birdIndex == 2 ? 2: (currentMissingType == BSIW_missingType.initial ? 1 : 2)), Vector2.zero ) );
		}
		
		private bool isdrag;
		private Vector2 cardOrig;
		private void onBirdDown( CEvent e ) {
			playClick ();
			closeCurrentBirdMouth ();
			closeStageBirdMouth ();
			signHighlightOff ();
			stopTimer ();
			timeoutCount = 0;

			currentBird = e.currentTarget as FilterMovieClip;
			currentBird.ScaleCentered(currentBird.scaleX * 1.2f);
			offset[0] = container.mouseX - currentBird.x;
			offset[1] = container.mouseY - currentBird.y;
			
			container.removeChild( currentBird );
			container.addChild ( currentBird );

			stage.addEventListener(MouseEvent.MOUSE_MOVE, onBirdMove);
			stage.addEventListener(MouseEvent.MOUSE_UP, onBirdUp);

			cardOrig = currentBird.parent.localToGlobal( new Vector2(currentBird.x, currentBird.y) );
			isdrag = false;
		}

		private void onBirdMove( CEvent e ) {
			Vector2 point = new Vector2( container.mouseX - offset[0], container.mouseY - offset[1] );
			
			currentBird.x = point.x;
			currentBird.y = point.y;
			
			point = currentBird.parent.localToGlobal( point );
			if( Mathf.Abs(cardOrig.x - point.x + cardOrig.y - point.y) > 5 ){
				isdrag = true;
			}

			/*
			if (isOnTarget ()) {
				glowOnGroup(currentBird);
			} else {
				clearGlowGroup(currentBird);
			}*/
		}

		private void onBirdUp( CEvent e ) {
			stage.removeEventListener (MouseEvent.MOUSE_MOVE, onBirdMove);
			stage.removeEventListener (MouseEvent.MOUSE_UP, onBirdUp);

			submitAnswer ();
		}

		private void submitAnswer() {

			glowOnGroup(currentBird);
			//clearGlowGroup(currentBird);
			
			int birdIndex = answerBirds.IndexOf (currentBird);

			if (isOnTarget ()) {
				interactOff();
				
				Vector2 point  = currentBird.parent.localToGlobal(new Vector2( currentBird.x, currentBird.y ));
				AddNewActionData( formatActionData( "move", birdIndex + 3, point, birdIndex == correctAnswerIndex ) );
				
				Tweener.addTween (currentBird, Tweener.Hash ("time", 0.1f, "x", targetBird.x, "y", targetBird.y, "scaleX", birdScale, "scaleY", birdScale)).OnComplete(checkAnswer);

			} else {
				
				AddNewActionData( formatActionData( isdrag ? "snap" : "tap", birdIndex + 3, Vector2.zero ) );
				
				resetBirdPosition (birdIndex, true);
				playSound(getBirdLetter(birdIndex), CLOSE_BIRD_MOUTH);
				animateAnswerBirds(currentBird, (int) currentMissingType, birdHaircuts[birdIndex], true);
				startTimer();
			}
		}

		private void closeCurrentBirdMouth() {
			if (currentBird != null) {
				int birdIndex = answerBirds.IndexOf (currentBird);
				animateAnswerBirds (currentBird, (int)currentMissingType, birdHaircuts [birdIndex], false);
				clearGlowGroup(currentBird);
			}
		}

		private void closeStageBirdMouth() {
			if (selectedStageBird != null) {
				clearGlowGroup (selectedStageBird);

				int birdIndex = stageBirds.IndexOf(selectedStageBird);

				// Bird close mouth
				
				animateStageBird (birdIndex, birdHaircuts [birdIndex], false, false);
			}
		}

		private void solvedAnswer() {

			
			if (pictureFadeOut) {
				if (!pictureFadedIn) {
					fadeOutPictorial();
				}
			}

			fadeOutBirds();
			playDing (OPP_SOLVED);

			currentBird.alpha = 0f;
			targetBird.alpha = 1f;
			
			completeBirdWord();

		}

		private void checkAnswer() {
			int birdIndex = answerBirds.IndexOf (currentBird);

			// Check if it's correct answer

			if (birdIndex == correctAnswerIndex) {
				fadeOutSpotlight();
				// Correct answer!
				if(!opportunityAnswered)
				{
					opportunityAnswered = true;
					opportunityCorrect = true;
				}
				opportunityComplete = true;

				currentBird.alpha = 0f;
				targetBird.alpha = 1f;

				completeBirdWord();

				PlayCorrectSound();

				if (pictureFadeOut) {
					if (!pictureFadedIn) {
						fadeOutPictorial();
					}
				}

				if (timeoutCount == 4) {
					fadeOutBirds();
				}

			} else {
				// :( Wrong answer
				if(!opportunityAnswered)
				{
					opportunityAnswered = true;
					opportunityCorrect = false;
				}
				
				PlayIncorrectSound();
			}
		}

		private void fadeOutBirds() {
			foreach (FilterMovieClip fmc in answerBirds) {
				if (fmc.alpha > 0f) {
					fadeOutBird(fmc);
				}
			}
		}

		private void fadeOutBird(FilterMovieClip birdClip) {
			birdClip.removeEventListener(MouseEvent.MOUSE_DOWN, onBirdDown);
			Tweener.addTween (birdClip, Tweener.Hash ("time", 0.25f, "alpha", 0));
		}

		private string getRandomString (List<string> previousChoices, List<string> availableChoices, string correctVersion) {
			
			bool stringFound = false;
			string currentChoice = "";
			
			while (!stringFound) {
				currentChoice = availableChoices[Random.Range(0, availableChoices.Count)];
				
				if (currentChoice != correctVersion) {
					// Check if the choice has been used previous
					
					bool previousFound = false;
					foreach(string previousOption in previousChoices) {
						if (previousOption == "") {
							stringFound = true;
							break;
						} else if (previousOption == currentChoice) {
							// Find a new one
							previousFound = true;
							break;
						}
					}
					
					if (!previousFound) {
						stringFound = true;
					}
				}
			}
			
			return currentChoice;
			
		}

		private void handleCritique() {
			glowOnGroup (currentBird);
			int birdIndex = answerBirds.IndexOf (currentBird);
			animateAnswerBirds(currentBird, (int) currentMissingType, birdHaircuts[birdIndex], true);
			playSound(getBirdLetter(birdIndex), YOU_FOUND_INCORRECT);
			Tweener.addTween (currentBird, Tweener.Hash ("time", 0.9f)).OnComplete (continueWrongBirdReinforcement);

		}

		private void continueWrongBirdReinforcement() {
			fadeOutBird(currentBird);
			foundIncorrect ();
		}

		private void foundIncorrect() {
			playSound (getInitialInstruction(currentOpp), INSTRUCTION_REPEATED);
			questionSign_Timeout(signAppearTime);
		}

		private void randomiseHaircuts() {
			for (int counter = 0; counter < stageBirds.Count; counter++) {
				birdHaircuts[counter] = Random.Range(1,4);
			}
		}


		public override void onTimerComplete()
		{
			base.onTimerComplete ();
			
			stopTimer ();

			timeoutCount++;
			string clipName = "";

			if (_level == SkillLevel.Tutorial) {
				signHighlightOff();


				switch(timeoutCount)
				{
				case 1:

					playSound (audioFolder_SkillInstruction + audioPrecursor + "now-you-try", REPEAT_INSTRUCTION);
					
					clipName = audioFolder_SkillInstruction + audioPrecursor + "now-you-try";
					/*
					playerSolveStep = 0;
					playInstruction_Timeout();
					*/
					break;
				case 2:
					if( inactivityCounter < 1 ){
						playSound(audioFolder_SkillInstruction + audioPrecursor + "try-again", RESTART_TUTORIAL);
						
						clipName = audioFolder_SkillInstruction + audioPrecursor + "try-again";
					} else
						OnAudioWordComplete( RESTART_TUTORIAL );

					/*
					opportunityAnswered = true;
					opportunityCorrect = false;*/
					break;
				
				}	

			} else {
				signHighlightOff();
				switch (timeoutCount) {
				case 1:

					playSound(getInitialInstruction(currentOpp), RESTART_TIMER);
					
					clipName = getInitialInstruction(currentOpp);
					questionSign_Timeout(signAppearTime);
					break;

				case 2:
					playSound(audioFolder_SkillInstruction + audioPrecursor + "touch-and-listen-to-the-birds", RESTART_TIMER);
					
					clipName = audioFolder_SkillInstruction + audioPrecursor + "touch-and-listen-to-the-birds";
					break;

				case 3:
					opportunityAnswered = true;
					opportunityCorrect = false;

					playSound(audioFolder_InactivityPrompt + audioPrecursor + "touch-" + currentOpp.targetWord, RESTART_TIMER);
					
					clipName = audioFolder_InactivityPrompt + audioPrecursor + "touch-" + currentOpp.targetWord;
					glowOnGroup(answerBirds[correctAnswerIndex]);
					break;

				case 4:
					// solve
					timeoutEnd = true;
					solvedForPlayer = true;
					interactOff();
					currentBird = answerBirds[correctAnswerIndex];
					Tweener.addTween (currentBird, Tweener.Hash ("time", 0.5f, "x", targetBird.x, "y", targetBird.y, "scaleX", birdScale, "scaleY", birdScale)).OnComplete(solvedAnswer);

					clipName = getReinforcement(currentOpp);
					break;
				}

			}
			
			if( !string.IsNullOrEmpty( clipName ) )
			{
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf( clipName ) );
			}


		}

		public void highlightTutorialBird() {
			
		}

		private void restart() {

			switch (inactivityCounter) {
			case 0:
				inactivityCounter++;

				opportunityCorrect = false;
				usedList = new List<int>();

				TT_QuestionTypes.CopyTo(availableQuestionTypes, 0);

				destroyBirds (answerBirds);

				hideContainer ();
				container.alpha = 0f;

				currentOpportunity = 0;

				tutorial.View.addEventListener( TutorialEvent.START, onTutorialStart );
				tutorial.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
				
				tutorial.OnStart ();

				break;

			case 1:
				opportunityCorrect = false;
				opportunityComplete = true;
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0"));

				break;

			}

		}

		private void spellWord() {
			
			clearFilters();
			
			fadeOutSpotlight();
			currentStageSpotlight = spellWordLetterIndex + 1;
			fadeInSpotlight(currentStageSpotlight);
			
			glowOnGroup (stageBirds [spellWordLetterIndex]);
			animateStageBird (spellWordLetterIndex, birdHaircuts [spellWordLetterIndex], true, true);
			
			if (spellWordLetterIndex != 0) {
				/*
				foreach( FilterMovieClip fmc in glowList )
				{
					fmc.alpha = 0f;
				}*/
				//clearGlowGroup (stageBirds [spellWordLetterIndex - 1]);
				animateStageBird (spellWordLetterIndex - 1, birdHaircuts [spellWordLetterIndex - 1], true, false);
			}

			if (spellWordLetterIndex < 2) {
				playSound (getReinforcementLetterAudio (currentOpp, (BSIW_missingType)spellWordLetterIndex), SPELL_WORD_PAUSE);
			} else {
				playSound (getReinforcementLetterAudio (currentOpp, (BSIW_missingType)spellWordLetterIndex), YOU_FOUND);
			}
			
			spellWordLetterIndex++;

//			turnGlowOff_Timeout (0.5f);
		}


		private bool isOnTarget() {
			
//			Rectangle rTarget = targetBox.getBounds (container);
//			Rectangle vTarget = currentBird.getBounds (container);
			float intersectionAmount = targetBox.getBounds (container).intersection (currentBird.getBounds (container)).width;
			
			if (intersectionAmount > boundsThreshold) {
				return true;
			}
			
			return false;
		}

		private FilterMovieClip addFilter( MovieClip mc )
		{
			DisplayObjectContainer mcParent = mc.parent;
			FilterMovieClip fmc = new FilterMovieClip( mc );
			fmc.removeChild( mc );
			mcParent.addChild( mc );
			mc.addChildAt( fmc, 0 );
			fmc.x -= mc.x;
			fmc.y -= mc.y;

			fmc.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
			fmc.filters [0].Visible = false;
			
			return fmc;
		}

		private void addFilterGroup ( MovieClip mc) {
			
			if( glowList == null )
				glowList = new List<FilterMovieClip>();
			
			DisplayObjectContainer _parent = mc.parent;
			FilterGroupMovieClip fmc = new FilterGroupMovieClip( mc );
			fmc.AddFilter( FiltersFactory.GetScaledYellowGlowFilter );
			fmc.removeChild( mc );
			_parent.addChild( mc );
			_parent.addChildAt( fmc, _parent.numChildren - 1 );
			glowList.Add( fmc );

		}

		private void clearFilters()
		{
			if( glowList == null )
				return;
			
			foreach( FilterMovieClip fmc in glowList )
			{
				if( fmc.parent != null )
					fmc.parent.removeChild( fmc );
				fmc.Destroy();
			}
			glowList.Clear();
		}

		private void questionSign_Timeout(float timeToWait) {
			questionSignTimer = TimerUtils.SetTimeout(timeToWait, signHighlightOn);
		}

		private void turnGlowOff_Timeout(float waitTime) {
			glowTimer =	TimerUtils.SetTimeout (waitTime, clearFilters);
		}

		private void timerSignHighlight() {
			signHighlightOn ();
			questionSignTimer = TimerUtils.SetTimeout(0.6f, signHighlightOff);
		}
		
		private static List<string> audioNames = new List<string>
		{	"",
			"Story-Introduction",
			"Narration/1G-Skill Instructions Audio/1G-these-birds-want-to-sing-pen",
			"Narration/1G-Skill Instructions Audio/1G-watch-closely-as-henry-finds",
			"Narration/1G-Reinforcement Audio/1G-first-pen-p",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-pen-p",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-pen-e",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-pen-n",
			"Narration/1G-Skill Instructions Audio/1G-now-you-try",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-touch-dog",
			"Narration/1G-Reinforcement Audio/1G-first-dog-d",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-dog-d",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-dog-o",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-dog-g",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-and-listen-to-the-birds",
			"Narration/1G-Skill Instructions Audio/1G-listen-to-the-sounds-and-repeat",
			"Narration/1G-Skill Instructions Audio/1G-listen-again",
			"Narration/1G-Skill Instructions Audio/1G-lets-help-the-birds-sing",
			"",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-ball",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-bat",
			"",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-cup",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-fig",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-log",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-mat",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-pen",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-pot",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-sad",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-bag",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-bed",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-cat",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-fan",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-hat",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-jet",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-leg",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-man",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-nap",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-pan",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-bug",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-bun",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-hen",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-mud",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-mug",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-nut",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-pig",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-red",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-sun",
			"Narration/1G-Inactivity Prompt Audio/1G-touch-web",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-ball",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-bat",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-dog",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-cup",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-fig",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-log",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-mat",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-pen",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-pot",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-sad",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-bag",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-bed",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-cat",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-fan",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-hat",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-jet",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-leg",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-man",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-nap",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-pan",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-bug",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-bun",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-hen",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-mud",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-mug",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-nut",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-pig",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-red",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-sun",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-drag-web",
			"Narration/1G-Reinforcement Audio/1G-first-ball-b",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-ball-b",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-ball-a",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-ball-l",
			"Narration/1G-Reinforcement Audio/1G-last-ball-l",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-bat-b",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-bat-b",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-bat-a",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-bat-t",
			"Narration/1G-Reinforcement Audio/1G-last-bat-t",
			"", "", "", "", "", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-dog-g",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-cup-c",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-cup-c",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-cup-u",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-cup-p",
			"Narration/1G-Reinforcement Audio/1G-last-cup-p",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-fig-f",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-fig-f",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-fig-i",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-fig-g",
			"Narration/1G-Reinforcement Audio/1G-last-fig-g",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-log-l",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-log-l",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-log-o",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-log-g",
			"Narration/1G-Reinforcement Audio/1G-last-log-g",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-mat-m",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-mat-m",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-mat-a",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-mat-t",
			"Narration/1G-Reinforcement Audio/1G-last-mat-t",
			"", "", "", "", "", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-pen-n",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-pot-p",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-pot-p",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-pot-o",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-pot-t",
			"Narration/1G-Reinforcement Audio/1G-last-pot-t",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-sad-s",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-sad-s",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-sad-a",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-sad-d",
			"Narration/1G-Reinforcement Audio/1G-last-sad-d",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-bag-b",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-bag-b",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-bag-a",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-bag-g",
			"Narration/1G-Reinforcement Audio/1G-middle-bag-a",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-bag-g",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-bed-b",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-bed-b",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-bed-e",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-bed-d",
			"Narration/1G-Reinforcement Audio/1G-middle-bed-e",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-bed-d",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-cat-c",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-cat-c",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-cat-a",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-cat-t",
			"Narration/1G-Reinforcement Audio/1G-middle-cat-a",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-cat-t",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-fan-f",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-fan-f",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-fan-a",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-fan-n",
			"Narration/1G-Reinforcement Audio/1G-middle-fan-a",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-fan-n",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-fan-f",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-fan-a",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-fan-n",
			"Narration/1G-Reinforcement Audio/1G-first-hat-h",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-hat-h",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-hat-a",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-hat-t",
			"Narration/1G-Reinforcement Audio/1G-middle-hat-a",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-hat-t",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-jet-j",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-jet-j",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-jet-e",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-jet-t",
			"Narration/1G-Reinforcement Audio/1G-middle-jet-e",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-jet-t",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-leg-l",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-leg-l",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-leg-e",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-leg-g",
			"Narration/1G-Reinforcement Audio/1G-middle-leg-e",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-leg-g",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-man-m",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-man-m",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-man-a",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-man-n",
			"Narration/1G-Reinforcement Audio/1G-middle-man-a",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-man-n",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-nap-n",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-nap-n",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-nap-a",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-nap-p",
			"Narration/1G-Reinforcement Audio/1G-middle-nap-a",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-nap-p",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-pan-p",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-pan-p",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-pan-a",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-pan-n",
			"Narration/1G-Reinforcement Audio/1G-middle-pan-a",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-pan-n",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-bug-b",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-bug-b",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-bug-u",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-bug-g",
			"Narration/1G-Reinforcement Audio/1G-middle-bug-u",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-bug-g",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-bun-b",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-bun-b",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-bun-u",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-bun-n",
			"Narration/1G-Reinforcement Audio/1G-middle-bun-u",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-bun-n",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-hen-h",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-hen-h",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-hen-e",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-hen-n",
			"Narration/1G-Reinforcement Audio/1G-middle-hen-e",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-hen-n",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-mud-m",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-mud-m",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-mud-u",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-mud-d",
			"Narration/1G-Reinforcement Audio/1G-middle-mud-u",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-mud-d",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-mug-m",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-mug-m",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-mug-u",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-mug-g",
			"Narration/1G-Reinforcement Audio/1G-middle-mug-u",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-mug-g",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-nut-n",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-nut-n",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-nut-u",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-nut-t",
			"Narration/1G-Reinforcement Audio/1G-middle-nut-u",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-nut-t",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-pig-p",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-pig-p",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-pig-i",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-pig-g",
			"Narration/1G-Reinforcement Audio/1G-middle-pig-i",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-pig-g",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-red-r",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-red-r",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-red-e",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-red-d",
			"Narration/1G-Reinforcement Audio/1G-middle-red-e",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-red-d",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-sun-s",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-sun-s",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-sun-u",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-sun-n",
			"Narration/1G-Reinforcement Audio/1G-middle-sun-u",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-sun-n",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-first-web-w",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-web-w",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-web-e",
			"Narration/1G-Reinforcement Audio/1G-Word Sounds/1G-web-b",
			"Narration/1G-Reinforcement Audio/1G-middle-web-e",
			"", "", "",
			"Narration/1G-Reinforcement Audio/1G-last-web-b",
			"", "", "",
			"User-Earns-Artifact",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-ball",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-bat",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-dog",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-cup",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-fig",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-log",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-mat",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-pen",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-pot",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-sad",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-bag",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-bed",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-cat",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-fan",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-hat",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-jet",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-leg",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-man",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-nap",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-pan",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-bug",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-bun",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-hen",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-mud",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-mug",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-nut",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-pig",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-red",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-sun",
			"Narration/1G-Skill Instructions Audio/1G-Initial-Question/1G-which-web",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-ball",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-bat",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-dog",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-cup",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-fig",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-log",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-mat",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-pen",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-pot",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-sad",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-bag",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-bed",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-cat",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-fan",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-hat",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-jet",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-leg",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-man",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-nap",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-pan",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-bug",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-bun",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-hen",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-mud",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-mug",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-nut",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-pig",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-red",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-sun",
			"Narration/1G-Skill Instructions Audio/1G-Initial Instructions/1G-listen-web"
		};
	}



	public enum BSIW_missingType {
		initial,
		medial,
		ending
	}
	
	public class BSIW_Opp {
		public string targetWord;
		public string initial;
		public string medial;
		public string ending;
		public BSIW_missingType missing;
		public string avoidDecoy;
		
		public BSIW_Opp (string _targetWord, string _initial, string _medial, string _ending, string _avoidDecoy, BSIW_missingType _missing) {
			targetWord = _targetWord;
			initial = _initial;
			medial = _medial;
			ending = _ending;
			missing = _missing;
			avoidDecoy = _avoidDecoy;
		}
		
	}

}