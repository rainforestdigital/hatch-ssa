using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.geom;

namespace SSGCore
{
	
	public class LetterRecognition : BaseSkill
	{
			
		private List<string> egAnswers = new List<string>{	"a", "b", "c", "d", "e", "f", "g",
															"h", "i", "j", "k", "l", "m", "n", 
															"o", "p", "q", "r", "s", "t", "u", 
															"v", "w", "x", "y", "z", "T", "S", 
															"M", "C", "D", "G", "N", "P"};
		
		private List<string> dgAnswers = new List<string>{	"a", "b", "c", "d", "e", "f", "g", 
															"h", "i", "j", "k", "l", "m", "n", 
															"o", "p", "q", "r", "s", "t", "u", 
															"v", "w", "x", "y", "z", "T", "S", 
															"M", "C", "D", "G", "N", "P", "R",
															"L", "B", "H", "F", "W", "K", "Y"};
		
		private List<string> ddAnswers = new List<string>{	"a", "b", "c", "d", "e", "f", "g", 
															"h", "i", "j", "k", "l", "m", "n", 
															"o", "p", "q", "r", "s", "t", "u", 
															"v", "w", "x", "y", "z", "A", "B", 
															"C", "D", "E", "F", "G", "H", "I", 
															"J", "K", "L", "M", "N", "O", "P", 
															"Q", "R", "S", "T", "U", "V", "W", 
															"X", "Y", "Z"};
		
		private string[] eng_audioVersions = new string[]{	"Astronaut", "Bat", "Cow", "Duck", "Elephant", "Frog", "Grasshopper", 
															"Horse", "Igloo", "Jet", "Kangaroo", "Lettuce", "Milk", "Nail", 
															"Orange", "Pig", "Quilt", "Ring", "Sheep", "Turkey", "Ukulele", 
															"Violin", "Wagon", "Xylophone", "Yogurt", "Zucchini", "Apple", "Bike", 
															"Chameleon", "Dog", "Egg", "Fish", "Gorilla", "House", "Iguana", 
															"Jug", "Key", "Light", "Money", "Nest", "Owl", "Pencil", 
															"Quarter", "Rabbit", "Sock", "Turtle", "Umbrella", "Vase", "Whale", 
															"Xray", "Yoyo", "Zebra"};

		private string[] spn_audioVersions = new string[]{	"Arana", "Buho", "Cerdo", "Dedo", "Elefante", "Flor", "Globo", 
															"Hormiga", "Iglu", "Jirafa", "Koala", "Lechuga", "Mesa", "Nuez", 
															"Oso", "Pastel", "Quince", "Radio", "Sapo", "Tazon", "Uvas", 
															"Ventana", "Wafle", "Xilofono", "Yogur", "Zorro",
															"Arbol", "Ballena", "Cebra", "Diente", "Emu", "Falda", "Gato", 
															"Huevo", "Iguana", "Jarra", "Kiwi", "Limon", "Mono", "Nieve", 
															"Oro", "Pajaro", "Queso", "Raton", "Sol", "Taxi", "Una", 
															"Vaca", "Wapiti", "Xerografia", "Yoyo", "Zapatos", "Name", "Nandu"};

		private string[] audioVersions;
		
		private string[] levelAnswers;
		
		public float percent;
		
		private string currentLetter = "";
		private string letterAudioRef
		{
			get{
				var lref = currentLetter.ToUpper();
				return lref;
			}
		}

		private List<string> usedLetters;
		
		private List<FilterMovieClip> _cards;
		
		private FilterMovieClip _answerCard;
		private FilterMovieClip _incorrectAnswerCard;
		
		private string currentClickedLetter;
		
		private DisplayObjectContainer cardContainer_mc;
		
		private int rightPart = 0;
		private int wrongPart = 0;
		private int right = 0;
		private int wrong = 0;
		
		/// <summary>
		/// Gets or sets the number cards.
		/// </summary>
		/// <value>
		/// The number cards to be used by the current opportunity.
		/// </value>
		private int numCards
		{
			get{ return _cards.Count; }
			set
			{
				int i;
				
				disableCardButtons();
				
				for(i = 0; i < _cards.Count; i++)
				{
					_cards[i].Destroy();
					cardContainer_mc.removeChild( _cards[i] );
				}
				
				_cards.Clear();
				
				float cWidth;				
				if(_level == SkillLevel.Developed || _level == SkillLevel.Completed)
					cWidth = Screen.width/6;
				else
					cWidth = (Screen.width/5) - 8;
				
				if(PlatformUtils.GetClosestRatio() == ScreenRatio.SIXTEENxNINE) cWidth *= 7f/8f;
				
				FilterMovieClip answerButton = MovieClipFactory.CreateLetterRecognitionCard();
				answerButton.Target.getChildByName<MovieClip>("card_gr").gotoAndStop(currentLetter);
				answerButton.Target.scaleY = answerButton.Target.scaleX = cWidth/answerButton.Target.width;
				answerButton.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
				answerButton.filters[0].Visible = false;
				answerButton.name = currentLetter;
				_answerCard = answerButton;
				_cards.Add( answerButton );
				cardContainer_mc.addChild( answerButton );
				
				List<string> availableAnswers = new List<string>();
				
				switch(_level)
				{
					// Tutorial
					case SkillLevel.Tutorial:
						availableAnswers.Add("o");
					break;
					
					default:
						for(i = 0; i < levelAnswers.Length; i++)
						{
							if(	levelAnswers[i].ToUpper() != letterAudioRef && 
								(availableAnswers.IndexOf(levelAnswers[i].ToUpper()) == -1 || availableAnswers.IndexOf(levelAnswers[i].ToLower()) == -1))
							{
								availableAnswers.Add(levelAnswers[i]);
							}
						}
					break;
				}
				
				while(_cards.Count < value)
				{
					int rand;
					do{
						rand = Mathf.FloorToInt( Random.Range(0, availableAnswers.Count) );
						if(rand == availableAnswers.Count) rand--;
					}while(isUsedLetter(availableAnswers[rand]));
					FilterMovieClip newCard = MovieClipFactory.CreateLetterRecognitionCard();
					newCard.Target.scaleY = newCard.Target.scaleX = cWidth/newCard.Target.width;
					newCard.Target.getChildByName<MovieClip>("card_gr").gotoAndStop(availableAnswers[rand]);
					newCard.AddFilter( FiltersFactory.GetScaledYellowGlowFilter());
					newCard.filters[0].Visible = false;
					newCard.name = availableAnswers[rand];
					cardContainer_mc.addChild( newCard );
					
					//BasicButton btn = MovieClipFactory.CreateLetterRecognitionCardTest();
					//MovieClipOverlayCameraBehaviour.instance.stage.addChild( btn );
					
					DebugConsole.Log("new Card width: {0}, height: {1}", newCard.Target.width, newCard.Target.scaleX);
					
					_cards.Add( newCard );
					availableAnswers.RemoveAt(rand);
				}
				
				DebugConsole.Log("Card Container scale: {0}", cardContainer_mc.scaleX);
				DebugConsole.Log("Letter recognition stage w: {0}", this.width);
				
				
				isBadWord();
				
				switch(_level)
				{
					// Tutorial
					case SkillLevel.Tutorial:
						_cards[1].Target.alpha = 0;
					break;
					
					default:
						_cards.Shuffle();
						while(isBadWord())
						{
							_cards.Shuffle();
						}
					break;
				}
				Debug.Log(_cards[0].width);
				
				cardContainer_mc.alpha = 0;
				//cardContainer_mc.LayoutTiled( _cards, 1 );
				arrangeCards();
				
				//normalCardScale = _cards[0].scaleX;
				//DebugConsole.Log("Card size: {0} Matrix Scale : {1}", _cards[0].scaleX, _cards[0].getFullMatrix().getScaleX());
				
			}
		}
		
		private bool isUsedLetter(string letter)
		{
			foreach(FilterMovieClip fmc in _cards)
			{
				if(fmc.Target.getChildByName<MovieClip>("card_gr").currentLabel.ToLower() == letter.ToLower())
					return true;
			}
			return false;
		}
		
		private void arrangeCards()
		{
			float padding = 10;
			float startX = cardContainer_mc.width / 2;
			startX -= (((_cards[0].width + padding) * _cards.Count) - padding) / 2;
			float startY = (cardContainer_mc.height - _cards[0].height)/2;
			
			for(int i = 0; i < _cards.Count; i++)
			{
				FilterMovieClip fmc = _cards[i];
				fmc.y = startY;
				fmc.x = startX;
				fmc.visible = true;
				fmc.alpha = 1;
				startX += fmc.width + padding;
			}
		}
				
		public LetterRecognition( SkillLevel level, SessionInfo  currentSession) : base(level, currentSession)
		{
			BACKGROUND = "Background";
			ARTIFACT = "Artifact";
			
			PreCacheAssets();
		}
		
		private void PreCacheAssets()
		{
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle("LetterRecognition", OnAssetsLoaded));
		}
		
		void OnAssetsLoaded(AssetBundle bundle)
		{
			if( bundle != null )
			{


				Init( null );
			}
		}
		
		/// <summary>
		/// Releases all resource used by the <see cref="SSGCore.LetterRecognition"/> object.
		/// </summary>
		/// <remarks>
		/// Call <see cref="Dispose"/> when you are finished using the <see cref="SSGCore.LetterRecognition"/>. The
		/// <see cref="Dispose"/> method leaves the <see cref="SSGCore.LetterRecognition"/> in an unusable state. After
		/// calling <see cref="Dispose"/>, you must release all references to the <see cref="SSGCore.LetterRecognition"/> so
		/// the garbage collector can reclaim the memory that the <see cref="SSGCore.LetterRecognition"/> was occupying.
		/// </remarks>
		public override void Dispose ()
		{
			base.Dispose();
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			
			if(_cards != null)
			{
				foreach(FilterMovieClip fmc in _cards)
				{
					Tweener.removeTweens(fmc);
					fmc.Destroy();
				}
			}
			if(_incorrectAnswerCard != null)
				_incorrectAnswerCard.Destroy();
			if(_answerCard != null)
				_answerCard.Destroy();
		}
		
		/// <summary>
		/// Init the specified parent.
		/// </summary>
		/// <param name='parent'>
		/// Parent movieclip. Not currently used.
		/// </param>
		public override void Init (MovieClip parent)
		{
			base.Init (parent);

			checkSpanish();

			cardContainer_mc = new DisplayObjectContainer();
			cardContainer_mc.width = Screen.width;
			cardContainer_mc.height = Screen.height;
			addChild( cardContainer_mc );
			cardContainer_mc.visible = false;
			
			_cards = new List<FilterMovieClip>();
			usedLetters = new List<string>();
			
			timer = totalTime;

			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			switch(_level)
			{
				// Tutorial
				case SkillLevel.Tutorial:
					totalOpportunities = 1;
					opportunityComplete = false;
					if(resumingSkill) {
						nextOpportunity();
					} else {
						tutorial_mc = new LetterRecognitionTutorial();
						addChild( tutorial_mc.View );
						tutorial_mc.View.visible = false;
						tutorial_mc.View.addEventListener( TutorialEvent.START, onTutorialStart );
						tutorial_mc.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
						tutorial_mc.Resize( Screen.width, Screen.height );
					}
				
				break;
				
				//All other levels
				case SkillLevel.Emerging:
				
					levelAnswers = egAnswers.ToArray();
				
					if( !currentSession.SPN_FLAG )
						totalOpportunities = 15;
					else
						totalOpportunities = 16;
					
					if(resumingSkill)
					{
						parseSessionData( currentSession.sessionData );
					//	fadeInCardContainer().OnComplete( nextOpportunity );
					}
				break;
				case SkillLevel.Developing:
					
					levelAnswers = dgAnswers.ToArray();
					
					totalOpportunities = dgAnswers.Count;

					if(resumingSkill)
					{
						parseSessionData( currentSession.sessionData );
					//	fadeInCardContainer().OnComplete( nextOpportunity );
					}
				break;
				case SkillLevel.Developed:
				case SkillLevel.Completed:
				
					levelAnswers = ddAnswers.ToArray();

					// Transition at 11, 21, 32, 42, 52
					totalOpportunities = ddAnswers.Count; // 52 answers available
					
					if(resumingSkill)
					{
						parseSessionData( currentSession.sessionData );
					//	fadeInCardContainer().OnComplete( nextOpportunity );
					}
				break;
			}
			
			Resize( Screen.width, Screen.height );
			
		}

		private void checkSpanish ()
		{
			if(currentSession.SPN_FLAG)
			{
				DebugConsole.Log("Setting Audio Words to Spanish");
				audioVersions = spn_audioVersions;
				badWords = spn_badWords;
				egAnswers.Add( "Ntilde" );
				egAnswers.Add( "ntilde" );
				
				dgAnswers.Add( "ntilde" );
				
				ddAnswers.Add( "Ntilde" );
				ddAnswers.Add( "ntilde" );
				
				if( audioNames.IndexOf( "4A-Touch-Letter-NTILDE" ) < 0 )
					audioNames.Insert( 21, "4A-Touch-Letter-NTILDE" );
			}
			else
			{
				DebugConsole.Log("Setting Audio Words to English");
				audioVersions = eng_audioVersions;
				badWords = eng_badWords;
				
				if( audioNames.IndexOf( "4A-Touch-Letter-NTILDE" ) >= 0 )
					audioNames.Remove( "4A-Touch-Letter-NTILDE" );
			}
		}
		
		public override void Start()
		{
			MarkInstructionStart();
		}
		
		#region Audio Listeners
		
		private void OnAudioWord( string word )
		{
			
			foreach(FilterMovieClip lb in _cards)
			{
				if(lb.Target.currentLabel == word)
				{
					if(lb.filters.Count > 0) lb.filters[0].Visible = true;
					else lb.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
					break;
				}
			}
			
		}
		
		/// <summary>
		/// Raises the audio word complete event.
		/// Handles what happens after specific audio files are played
		/// </summary>
		/// <param name='word'>
		/// Parameter that gets passed in from the delegate.
		/// </param>
		public override void OnAudioWordComplete( string word )
		{
			
			DebugConsole.Log("LetterRecognition || OnAudioWordComplete word: " + word);
			
			AudioClip clip = null;
			SoundEngine.SoundBundle bundle = null;
			
			/*foreach(FilterMovieClip lb in _cards)
			{
				if(lb.filters[0] != null ) lb.filters[0].Visible = false;
			}*/
			
			if(opportunityComplete)
			{
			
				switch( word )
				{
										
					case YOU_FOUND:
					
						if(_level == SkillLevel.Tutorial)
						{
							bundle = new SoundEngine.SoundBundle();
							clip = SoundUtils.LoadGlobalSound(MovieClipFactory.LETTER_RECOGNITION, "4A-let-letter-hunt-begin");
							bundle.AddClip( clip, NOW_LETS, clip.length );
							SoundEngine.Instance.PlayBundle( bundle );
						}
						else
						{
							goto case THIS_IS;
						}
					break;
					case THEME_COMPLIMENT:
						bundle = new SoundEngine.SoundBundle();
						int audioVersion = System.Array.IndexOf(ddAnswers.ToArray(), currentLetter);
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LETTER_RECOGNITION, "4A-" + letterAudioRef + "-" + audioVersions[audioVersion] );
						bundle.AddClip( clip, YOU_FOUND, clip.length );
						SoundEngine.Instance.PlayBundle( bundle );
					break;
					
					case INTRO:
					case PLEASE_FIND:
					case NOW_ITS:
					case NOW_LETS:
					case YOU_FOUND_INCORRECT:
					case THIS_IS :
						Tweener.addTween( cardContainer_mc, Tweener.Hash("time",0.35f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuint) );
						
						if (timeoutEnd) {
							//quietContinue = true;
							dispatchEvent( new SkillEvent(SkillEvent.TIMEOUT_END, true, false) );
							timeoutEnd = false;
						} else {
							if(opportunityCorrect) {
								rightPart ++;
								dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
							} else {
								wrongPart ++;
								dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
							} 
						}
						
						/*
						if((rightPart/(float)totalOpportunities)*10 >= right + 1)
						{
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
							right ++;
							Debug.Log("RightMove");
						}
						else if((wrongPart/(float)totalOpportunities)*10 >= wrong + 1)
						{
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
							wrong++;
							Debug.Log("WrongMove");
						}
						else if(currentOpportunity == totalOpportunities)
						{
							if(opportunityCorrect) {
								dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
								Debug.Log("Opportunity Correct");
							} else {
								dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
								Debug.Log("Opportunity Wrong");
							}
						}
						else
						{
							if(timeoutEnd)
							{
								quietContinue = true;
								dispatchEvent( new SkillEvent(SkillEvent.TIMEOUT_END, true, false) );
								Debug.Log("Timeout End");
							}
							else {
								nextOpportunity();
								Debug.Log("Timeout Next Opp");
							}

							timeoutEnd = false;
						}
						*/
					break;
					case THEME_INTRO:
						if( !currentSession.skipIntro )
						{
							bundle = new SoundEngine.SoundBundle();
							clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LETTER_RECOGNITION, "4A-StoryIntroduction" );
							
							bundle.AddClip( clip, INTRO, clip.length );
							SoundEngine.Instance.PlayBundle( bundle );
						}
						else
							OnAudioWordComplete( INTRO );
					break;
					
					case CLICK_CORRECT:
						dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
					break;
										
				}
				
			}
			else
			{
				
				switch( word )
				{
					
					case THEME_CRITICISM:
						bundle = new SoundEngine.SoundBundle();
						int audioVersion = System.Array.IndexOf(ddAnswers.ToArray(), currentClickedLetter);
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LETTER_RECOGNITION, "4A-This-" + currentClickedLetter.ToUpper() + "-" + audioVersions[audioVersion] );
						bundle.AddClip( clip, YOU_FOUND, clip.length );
						SoundEngine.Instance.PlayBundle( bundle );
					break;
					
					case YOU_FOUND:
					case YOU_FOUND_INCORRECT:
						//PLEASE FIND current number - audio
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LETTER_RECOGNITION, "4A-Touch-Letter-" + letterAudioRef);
						bundle = new SoundEngine.SoundBundle();
						bundle.AddClip( clip, PLEASE_FIND, clip.length );
						SoundEngine.Instance.PlayBundle( bundle );
						clearIncorrectAnswer();
					break;
										
					case INTRO:
						cardContainer_mc.visible = true;
						if(_level == SkillLevel.Tutorial)
						{
							tutorial_mc.View.visible = true;
							tutorial_mc.OnStart();
						}
						else
						{
							nextOpportunity();
						}
					break;
					
					case NOW_LETS:
						resetTutorial();
					break;
					
					case PLEASE_FIND:
					case NOW_ITS:
					case THIS_IS :
						clearIncorrectAnswer();
						startTimer();
						enableCardButtons();
					break;
					
					case THEME_INTRO:
						if( !currentSession.skipIntro )
						{
							bundle = new SoundEngine.SoundBundle();
							clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LETTER_RECOGNITION, "4A-StoryIntroduction" );
							
							bundle.AddClip( clip, INTRO, clip.length );
							SoundEngine.Instance.PlayBundle( bundle );
						} else {
							OnAudioWordComplete( INTRO );
						}
					break;
					
					case CLICK_INCORRECT:
						dispatchEvent( new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
					break;
					
				}
				
			}
			
		}
		#endregion
		
		private void clearIncorrectAnswer()
		{
			if(_incorrectAnswerCard != null)
			{
				Tweener.addTween( _incorrectAnswerCard, Tweener.Hash("time", 0.35f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuad) ).OnComplete(nukeIncorrect);
			}
		}
		private void nukeIncorrect()
		{
			_incorrectAnswerCard.Destroy();
			cardContainer_mc.removeChild(_incorrectAnswerCard);
			_incorrectAnswerCard = null;
		}
		
		protected override void resetTutorial()
		{
			
			cardContainer_mc.alpha = 0;
			currentOpportunity = 0;
			
			timeoutCount = 0;
			
			DebugConsole.Log("resetting tutorial");
					
			tutorial_mc = null;
			tutorial_mc = new LetterRecognitionTutorial();
			
			addChild( tutorial_mc.View );
			tutorial_mc.View.visible = true;
			tutorial_mc.View.addEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			tutorial_mc.Resize( Screen.width, Screen.height );
			tutorial_mc.OnStart();
		}
		
		/// <summary>
		/// Handles the card click event.
		/// </summary>
		/// <param name='e'>
		/// Event param.
		/// </param>
		private void onCardClick( CEvent e )
		{
			
			disableCardButtons();
			
			//Plays global click audio
			PlayClickSound();
			
			FilterMovieClip cardClicked = e.currentTarget as FilterMovieClip;
			
			if(_level == SkillLevel.Tutorial)
				cardClicked.Target.ScaleCentered( cardClicked.Target.scaleX * (7f/6f) );
			else
				cardClicked.Target.ScaleCentered( cardClicked.Target.scaleX * (4f/3f) );
			
			SoundEngine.Instance.SendCancelToken();
			//SoundEngine.Instance.StopAll();	
			
			cardContainer_mc.removeChild( cardClicked );
			cardContainer_mc.addChild( cardClicked );
			
			Rectangle ccRect = cardClicked.getBounds(cardContainer_mc);
			if(ccRect.x < 25) cardClicked.x += (ccRect.x * -1) + 25;
			else if(ccRect.x + ccRect.width > cardContainer_mc.width - 25)
				cardClicked.x -= (((ccRect.x + ccRect.width) - cardContainer_mc.width)) + 25;
			if(_level != SkillLevel.Tutorial)
				cardClicked.y -= cardClicked.height/8f;
			
			stopTimer();
			timeoutCount = 0;
			
			currentClickedLetter = cardClicked.Target.getChildByName<MovieClip>("card_gr").currentLabel;
			
			if(cardClicked.filters.Count > 0)
			{
				for(int i = cardClicked.filters.Count - 1; i >= 0; i--)
				{
					MovieClipFilter mcf = cardClicked.filters[i];
					mcf.Visible = false;
					mcf.Destroy();
					cardClicked.removeChild(mcf.GetSprite());
					cardClicked.filters.Remove(mcf);
				}	
			}
			cardClicked.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
			cardClicked.filters[0].Visible = true;
						
			//If Right Answer
			if(cardClicked == _answerCard)
			{
				
				PlayCorrectSound();
				
				if(opportunityAnswered == false)
				{
					opportunityAnswered = true;
					opportunityCorrect = true;
				}
				opportunityComplete = true;
				
				disableCardButtons();
				
				AddNewActionData( formatActionData( "tap", _cards.IndexOf(cardClicked), Vector2.zero, true ) );
			}
			//If Wrong Answer
			else
			{
				
				PlayIncorrectSound();
				
				disableCardButtons();
				
				if(opportunityAnswered == false)
				{
					opportunityAnswered = true;
					opportunityCorrect = false;
				}
				
				_incorrectAnswerCard = cardClicked;
				
				AddNewActionData( formatActionData( "tap", _cards.IndexOf( cardClicked ), Vector2.zero , false ) );
			}
			
		}
		
		/// <summary>
		/// Initialized the next opportunity.
		/// </summary>
		public override void nextOpportunity()
		{
		
			switch(_level)
			{
				case SkillLevel.Tutorial:
				
					if(currentOpportunity > 0)
					{
						tallyAnswers();
					}
					else
					{
						currentOpportunity++;
						currentLetter = "A";
						numCards = 2;
						
						cardContainer_mc.alpha = 1;
						OpportunityStart();
					
					}
				break;
				
				case SkillLevel.Emerging:
					setNextOpportunity(3);
				break;
				
				case SkillLevel.Developing:
					setNextOpportunity(4);
				break;
				
				case SkillLevel.Completed:
				case SkillLevel.Developed:
					setNextOpportunity(5);
				break;
				
			}
			
			if(totalOpportunities > 10 && currentOpportunity > 1)
				dispatchEvent( new SkillEvent(SkillEvent.BOOKMARK_NOW) );
		}
		
		/// <summary>
		/// Sets the next opportunity's values.
		/// </summary>
		/// <param name='cards'>
		/// number of cards to use.
		/// </param>
		private void setNextOpportunity( int cards )
		{
			
			List<string> answersArray;
			int i;
						
			if(currentOpportunity > 0)
			{
				if(opportunityComplete)
				{
					if(opportunityCorrect)
					{
						//NumCorrect++;
					}
					else
					{
						//NumIncorrect++;
					}
				}
				else
				{
					//NumIncorrect++;
				}
			}
			if(currentOpportunity < totalOpportunities)
			{
				opportunityComplete = false;
				opportunityCorrect = false;
				opportunityAnswered = false;
			
				currentOpportunity++;
				
				answersArray = new List<string>();
			
				for(i = 0; i < levelAnswers.Length; i++)
				{
					if(usedLetters.IndexOf(levelAnswers[i]) == -1)
					{
						answersArray.Add(levelAnswers[i]);
					}
				}
				int rand = Mathf.FloorToInt(Random.Range(0, answersArray.Count));
				if(rand == answersArray.Count) rand--;
				
				if(resumingSkill)
				{
					currentLetter = usedLetters[usedLetters.Count - 1];
					resumingSkill = false;
				}
				else
				{
					currentLetter = answersArray[rand];
					usedLetters.Add(currentLetter);
				}
			
				numCards = cards;
			
				fadeInCardContainer().OnComplete( OpportunityStart );
			
			}
			else
			{
				tallyAnswers();
				numCards = 0;
			}
			
			Debug.Log("_numCorrect: " + NumCorrect);
			Debug.Log("_numIncorrect: " + NumIncorrect);
			
		}
		
		/// <summary>
		/// Fades in the card container.
		/// </summary>
		/// <returns>
		/// The TweenerObj used to fade the container.
		/// </returns>
		private TweenerObj fadeInCardContainer()
		{
			cardContainer_mc.visible = true;
			cardContainer_mc.alpha = 0;
			return Tweener.addTween(cardContainer_mc, Tweener.Hash("time", 0.35f, "alpha", 1, "transition", Tweener.TransitionType.easeInOutQuint) );
		}
		
		/// <summary>
		/// Starts the opportunity.
		/// </summary>
		public override void OpportunityStart()
		{
			
			AudioClip clip = null;
			string clipName = "";
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			string clipID = "";
			
			switch( _level )
			{
				case SkillLevel.Tutorial:
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LETTER_RECOGNITION, "4A-now-your-turn" );
					clipName = "4A-now-your-turn";
					clipID = NOW_ITS;
				break;
				default:
					//PLEASE FIND (currentNumber) - AUDIO
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LETTER_RECOGNITION, "4A-Touch-Letter-" + letterAudioRef);
					clipName = "4A-Touch-Letter-" + letterAudioRef;
					clipID = PLEASE_FIND;
				break;
			}
			
			bundle.AddClip(clip, clipID, clip.length);
			SoundEngine.Instance.PlayBundle( bundle );
			
			//enableCardButtons();
			
			timeoutCount = 0;
			
			AddNewOppData( audioNames.IndexOf( clipName ) < 0 ? 21 : audioNames.IndexOf( clipName ) );
			foreach( FilterMovieClip fmc in _cards )
			{
				Vector2 point = fmc.parent.localToGlobal(new Vector2( fmc.x, fmc.y ));
				AddNewObjectData( formatObjectData( fmc.name, point, fmc == _answerCard ) );
			}
			
			dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_START, true, false) );
			
		}
		
		/// <summary>
		/// Resize the specified width and height.
		/// </summary>
		/// <param name='width'>
		/// Width.
		/// </param>
		/// <param name='height'>
		/// Height.
		/// </param>
		public override void Resize( int width, int height )
		{
			this.width = width;
			this.height = height;
			
			cardContainer_mc.scaleX = width/cardContainer_mc.width;
			cardContainer_mc.scaleY = cardContainer_mc.scaleX;
			cardContainer_mc.x = 0;
			cardContainer_mc.y = (height - (cardContainer_mc.height*cardContainer_mc.scaleY))/2;
			//cardContainer_mc.width = this.width;
			//cardContainer_mc.height = this.height;
			//cardContainer_mc.FitToParent();
		}
				
		/// <summary>
		/// Called when BaseSkill's timer completes.
		/// </summary>
		public override void onTimerComplete()
		{
			base.onTimerComplete();
			
			stopTimer();
			
			timeoutCount++;
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = null;
			string clipName = "";
			string clipID = "";
			
			switch(timeoutCount)
			{
				case 1:
					if(_level == SkillLevel.Tutorial)
					{	
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LETTER_RECOGNITION, "4A-now-your-turn" );
						clipName = "4A-now-your-turn";
						clipID = NOW_ITS;
					}
					else
					{
						//PLEASE FIND currect number - audio
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LETTER_RECOGNITION, "4A-Touch-Letter-" + letterAudioRef);
						clipName = "4A-Touch-Letter-" + letterAudioRef.ToUpper();
						clipID = PLEASE_FIND;
					}
				break;
				case 2:
					if(_level == SkillLevel.Tutorial)
					{	
						disableCardButtons();
					
						if(opportunityAnswered)
						{
							opportunityComplete = true;
							opportunityCorrect = false;
							
							OnAudioWordComplete(NOW_LETS);
							return;
						}
					
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LETTER_RECOGNITION, "4A-try-again" );
						clipName = "4A-try-again";
						clipID = NOW_LETS;
				
						opportunityAnswered = true;
						opportunityCorrect = true;
					
					}
					else
					{
						//TOUCH - audio
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LETTER_RECOGNITION, "4A-touch-green-button" );
						clipName = "4A-touch-green-button";
						clipID = NOW_ITS; //Temp
					}
				break;
				case 3:
					if(opportunityAnswered == false)
					{
						opportunityAnswered = true;
						opportunityCorrect = false;
					}
					
					foreach(FilterMovieClip lb in _cards)
					{
						if(lb == _answerCard)
						{
							if(lb.filters.Count > 0)
								lb.filters[0].Visible = true;
							else
								lb.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
							break;
						}
					}
					
					//PLEASE FIND currect number - audio
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LETTER_RECOGNITION, "4A-Touch-Letter-" + letterAudioRef);
					clipName = "4A-Touch-Letter-" + letterAudioRef.ToUpper();
					clipID = PLEASE_FIND;
				break;
				case 4:
					timeoutEnd = true;
					opportunityComplete = true;
					
					disableCardButtons();
				
					foreach(FilterMovieClip lb in _cards)
					{
						if(lb != _answerCard)
						{
							Tweener.addTween(lb, Tweener.Hash("time",0.5f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuad) );
						}
					}
					_answerCard.Target.ScaleCentered(_answerCard.Target.scaleX + 0.2f);
				
					Rectangle ccRect = _answerCard.getBounds(cardContainer_mc);
					if(ccRect.x < 25) _answerCard.x += (ccRect.x * -1) + 25;
					else if(ccRect.x + ccRect.width > cardContainer_mc.width - 25)
						_answerCard.x -= (((ccRect.x + ccRect.width) - cardContainer_mc.width)) + 25;
				
					if(_answerCard.filters.Count > 0)
					{
						for(int i = _answerCard.filters.Count - 1; i >= 0; i--)
						{
							MovieClipFilter mcf = _answerCard.filters[i];
							mcf.Visible = false;
							mcf.Destroy();
							_answerCard.removeChild(mcf.GetSprite());
							_answerCard.filters.Remove(mcf);
						}	
					}
					_answerCard.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
					_answerCard.filters[0].Visible = true;
					
					//THIS IS currect letter - audio
					int audioVersion = System.Array.IndexOf(ddAnswers.ToArray(), currentLetter);
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LETTER_RECOGNITION, "4A-This-" + letterAudioRef + "-" + audioVersions[audioVersion] );
					clipName = "4A-This-" + letterAudioRef.ToUpper() + "-" + audioVersions[audioVersion];
					clipID = THIS_IS;
				break;
				default:
				break;
			}
			
			if(clip != null)
			{
				bundle.AddClip( clip, clipID, clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
				
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf( clipName ) );
			}
			
		}
		
		/// <summary>
		/// Called when the tutorial complete event is fired.
		/// </summary>
		/// <param name='e'>
		/// Event param.
		/// </param>
		private void onTutorialComplete( CEvent e )
		{
			tutorial_mc.View.removeEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.View.removeEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			
			removeChild( tutorial_mc.View );
			
			nextOpportunity();
			
		}
		
		/// <summary>
		/// Disables the card buttons.
		/// </summary>
		private void disableCardButtons()
		{
			for(int i = 0; i < _cards.Count; i++)
			{
				_cards[i].removeEventListener( MouseEvent.CLICK, onCardClick );
			}
			MarkInstructionStart();
		}
		
		/// <summary>
		/// Enables the card buttons.
		/// </summary>
		private void enableCardButtons()
		{
			for(int i = 0; i < _cards.Count; i++)
			{
				if(_cards[i].filters.Count > 0)
					_cards[i].filters[0].Visible = false;
				_cards[i].addEventListener( MouseEvent.CLICK, onCardClick );
			}
			if(_level != SkillLevel.Tutorial) MarkInstructionEnd();
		}
		
		/// <summary>
		/// The bad words.
		/// </summary>
		private List<string> eng_badWords = new List<string>(){	"anal", "anus", "arse", "ass","bhard","balls", "barf","bi", "bi-", "bitch",
																"bm", "boner", "bong","boobs", "boody","butt","clit","cock", "cocky", "crabs",
																"crack", "crack-whore", "crap","cum","cumm","cunt","damn","dd", "devil", "dike", 
																"dildo","dix", "dome", "dome", "dong", "dope","dre", "drunk","eatme", "eatme","evl", 
																"fagot", "fairy", "fart", "fatso","femme", "four20","four20","free4all", "free4all",
																"fuck", "fucka", "fuuck", "gunit","giehn", "gome","god","gook","got2","gunit","harem",
																"hell","ho", "homo", "hobo", "hole", "homo","hore","horny","hoser", "hot2trot","hussy",
																"idiot", "idoit", "ingin","jap", "jism", "jiz","jizim","jizz","joint","jugs", "kmart",
																"kill", "kkk","koon", "kotex", "krap","kum","kunt", "ky","laid", "lesbo", "lez", "lezbe",
																"lezbe","lezbo", "lezz", "lezzo","limy", "ll","mams", "muff", "naked","nig", "niger", "nigga",
																"nosex","nude", "oicu812","orgy", "ou812", "oui", "pimp","pee", "penis", "phque", "pimp", "piss",
																"poop", "porn","porno","pot","prick", "pubic", "pud", "pudd", "pussy", "dick","pwt", "queef", 
																"queer", "rape", "roach","sandm", "s&m","sandm", "satan","screw","semen", "sex","sexy", "shag",
																"shit", "shat", "shits","sixty9","skank", "skum", "slant", "slave", "slime", "slut", "sluts",
																"slutt","snot", "sob","sperm","spic", "spick", "spit", "stagg", "suck", "tang", "tit", "tits",
																"titty","tramp","trots", "turd", "urine","vd","vulva","whore", "xxx"};

		private List<string> spn_badWords = new List<string>(){	"bastardo", "bicho", "bolillo", "boludo","cabron","cabrUn", "caca","cachimba", "cachondo", "cagar",
																"capullo", "caracho", "carajito","carajo", "charnego","chichis","chichotas","chingar", "chingate", "chocha",
																"cholo", "chota", "choto","chucha","ch'pelo","cleso","cojeme","cojer", "cojUn", "cojones", 
																"cojonudo","collons", "conha", "coUo", "cuca", "culo","eme", "estoy cachondo","est'pido", "follar","ghiata", 
																"gilipollas", "gringo", "hijo de puta", "hijo de tu puta madre","hostia", "hueles a mierda","huey","idiota", "ignorante",
																"ito", "japo", "joder", "joto","kurepI", "lela","lelo","mamon","manflor","maqueto","maricUn",
																"maricona","maricueca", "marimacha", "mariquita", "mayate", "mierda","mojon","momada","ogt", "ojeta","orto",
																"pajero", "panocha", "paragua","payoponi", "pedo", "pendeja","pendejo","perra","pichula","pija", "pinche",
																"pinga", "piruia","pirulin", "pito", "polla","poronga","poto", "putta","puta", "putas", "puto", "tano",
																"teta","tetas", "tonta", "vato","vendejo", "verga","verija", "vorugua", "zopulpa","zorra"};

		private List<string> badWords;
		
		/// <summary>
		/// Checks to see if the current arrangement of letters spells a bad word.
		/// </summary>
		/// <returns>
		/// If bad word exists.
		/// </returns>
		private bool isBadWord()
		{
			
			string word = "";
			
			for(int i = 0; i < _cards.Count; i++)
			{
				word += _cards[i].Target.getChildByName<MovieClip>("card_gr").currentLabel.ToLower();
			}
						
			if(badWords.IndexOf(word) > -1)
				return true;
			else
				return false;
			
		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = "";
			
			foreach(string s in usedLetters)
			{
				if(data != "")
					data = data + "-";
				data = data + s;
			}
			
			data = data + ";" + rightPart.ToString() + "-" + right.ToString() + "-" + wrongPart.ToString() + "-" + wrong.ToString();
			
			return data;
		}
		
		private void parseSessionData (string data)
		{
			usedLetters = new List<string>();
			
			string partData = data.Split(';')[1];
			data = data.Split(';')[0];
			
			string[] sData = data.Split('-');
			
			foreach( string s in sData )
			{
				usedLetters.Add(s);
			}
			
			sData = partData.Split('-');
			if(sData.Length > 2)
			{
				rightPart = int.Parse(sData[0]);
				right = int.Parse(sData[1]);
				wrongPart = int.Parse(sData[2]);
				wrong = int.Parse(sData[3]);
			}
			else
			{
				rightPart = Mathf.RoundToInt(float.Parse(sData[0]) * totalOpportunities);
				right = currentSession.correct;
				wrongPart = Mathf.RoundToInt(float.Parse(sData[1]) * totalOpportunities);
				wrong = currentSession.incorrect;
			}
		}
		
		private static List<string> audioNames = new List<string>
		{	"",
			"4A-StoryIntroduction", "4A-watch-closely-cami", "4A-now-your-turn", "4A-let-letter-hunt-begin", "4A-touch-green-button", "4A-try-again",
			"4A-Touch-Letter-A", "4A-Touch-Letter-B", "4A-Touch-Letter-C", "4A-Touch-Letter-D", "4A-Touch-Letter-E", "4A-Touch-Letter-F",
			"4A-Touch-Letter-G", "4A-Touch-Letter-H", "4A-Touch-Letter-I", "4A-Touch-Letter-J", "4A-Touch-Letter-K", "4A-Touch-Letter-L",
			"4A-Touch-Letter-M", "4A-Touch-Letter-N", "4A-Touch-Letter-O", "4A-Touch-Letter-P", "4A-Touch-Letter-Q", "4A-Touch-Letter-R",
			"4A-Touch-Letter-S", "4A-Touch-Letter-T", "4A-Touch-Letter-U", "4A-Touch-Letter-V", "4A-Touch-Letter-W", "4A-Touch-Letter-X",
			"4A-Touch-Letter-Y", "4A-Touch-Letter-Z", "4A-Letter-O", "4A-A-Apple", "4A-A-Astronaut", "4A-B-Bat", "4A-B-Bike", "4A-C-Chameleon",
			"4A-C-Cow", "4A-D-Dog", "4A-D-Duck", "4A-E-Egg", "4A-E-Elephant", "4A-F-Fish", "4A-F-Frog", "4A-G-Gorilla", "4A-G-Grasshopper",
			"4A-H-Horse", "4A-H-House", "4A-I-Igloo", "4A-I-Iguana", "4A-J-Jet", "4A-J-Jug", "4A-K-Kangaroo", "4A-K-Key", "4A-L-Lettuce", "4A-L-Light",
			"4A-M-Milk", "4A-M-Money", "4A-N-Nail", "4A-N-Nest", "4A-O-Orange", "4A-O-Owl", "4A-P-Pencil", "4A-P-Pig", "4A-Q-Quarter", "4A-Q-Quilt",
			"4A-R-Rabbit", "4A-R-Ring", "4A-S-Sheep", "4A-S-Sock", "4A-T-Turkey", "4A-T-Turtle", "4A-U-Ukulele", "4A-U-Umbrella", "4A-V-Vase",
			"4A-V-Violin", "4A-W-Wagon", "4A-W-Whale", "4A-X-Xray", "4A-X-Xylophone", "4A-Y-Yogurt", "4A-Y-Yoyo", "4A-Z-Zebra", "4A-Z-Zucchini",
			"4A-This-A-Apple", "4A-This-A-Astronaut", "4A-This-B-Bat", "4A-This-B-Bike", "4A-This-C-Chameleon", "4A-This-C-Cow", "4A-This-D-Dog",
			"4A-This-D-Duck", "4A-This-E-Egg", "4A-This-E-Elephant", "4A-This-F-Fish", "4A-This-F-Frog", "4A-This-G-Gorilla", "4A-This-G-Grasshopper",
			"4A-This-H-Horse", "4A-This-H-House", "4A-This-I-Igloo", "4A-This-I-Iguana", "4A-This-J-Jet", "4A-This-J-Jug", "4A-This-K-Kangaroo",
			"4A-This-K-Key", "4A-This-L-Lettuce", "4A-This-L-Light", "4A-This-M-Milk", "4A-This-M-Money", "4A-This-N-Nail", "4A-This-N-Nest",
			"4A-This-O-Orange", "4A-This-O-Owl", "4A-This-P-Pencil", "4A-This-P-Pig", "4A-This-Q-Quarter", "4A-This-Q-Quilt", "4A-This-R-Rabbit",
			"4A-This-R-Ring", "4A-This-S-Sheep", "4A-This-S-Sock", "4A-This-T-Turkey", "4A-This-T-Turtle", "4A-This-U-Ukulele", "4A-This-U-Umbrella",
			"4A-This-V-Vase", "4A-This-V-Violin", "4A-This-W-Wagon", "4A-This-W-Whale", "4A-This-X-Xray", "4A-This-X-Xylophone", "4A-This-Y-Yoyo",
			"4A-This-Y-Yogurt", "4A-This-Z-Zebra", "4A-This-Z-Zucchini", "4A-This-NTILDE-Name", "4A-This-NTILDE-Nandu", "User-Earns-Artifact"
		};
			
	}
}