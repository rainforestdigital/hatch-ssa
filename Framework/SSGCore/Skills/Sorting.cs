using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.geom;
namespace SSGCore
{

	public class Sorting : BaseSkill
	{
		private const string KEEP_SHAPE = "KEEP_SHAPE";
		private const string KEEP_PICTURE = "KEEP_PICTURE";
		private const string LETS_TRY_AGAIN = "LETS_TRY_AGAIN";
		private const string HELP_PROMPT = "HELP_PROMPT";
		private const string TOUCH_HELP = "TOUCH_HELP_BUTTON";
		public const string HOUSE_IS1 = "HOUSE_IS1";
		public const string HOUSE_IS2 = "HOUSE_IS2";
		private const string TOUCH_DRAG = "TOUCH_DRAG";
		private const string YOU_HELPED = "YOU_HELPED";
		private const string BELONGS = "BELONGS";
		public const string CAMI_HELP = "CAMI_HELP";
		public const string YOU_NAP = "YOU_NAP";

		//public string host = "CAMI"; //will probably need to change
		public float percent;

		private DisplayObjectContainer sortContainer_mc;
		private DisplayObjectContainer houseContainer_mc;

		private SortingHouseObject house1_mc;
		private SortingHouseObject house2_mc;
//		private static List<string> _shapes = new string[]{"CIRCLE","OVAL","RECTANGLE","RHOMBUS","SQUARE","TRIANGLE"}.ToListUtil();
		private static string[] types = new string[]{"BALL","BEAR","BOOT","CAR","CIRCLE","COMB","DINOSAUR","EGG","FORK","FRUIT",
													"GLOVE","NEST","OVAL","PANTS","PLANE","RECTANGLE","RHOMBUS","SHIRT","SHOE","SPOON",
													"SQUARE","TOOTHBRUSH","TRAIN","TRIANGLE","TRUCK","GRAIN"};
		private static int[] typeNums = new int[]{8,6,8,10,6,8,8,8,8,8,
												8,8,6,8,8,6,6,8,8,8,
												6,8,8,6,11,8};
		private List<string[]> _ddHouses = new List<string[]>{
			new string[]{"TRUCKS","CARS"},
			new string[]{"TOOTHBRUSHES","COMBS"},
			new string[]{"PLANES","TRAINS"},
			new string[]{"BOOTS","GLOVES"},
			new string[]{"BALLS","SHOES"},
			new string[]{"BEARS","DINOSAURS"},
			new string[]{"FRUITS","GRAINS"},
			new string[]{"FORKS","SPOONS"},
			new string[]{"EGGS","NESTS"},
			new string[]{"SHIRTS","PANTS"}
		};
		private List<string[]> _ddPics = new List<string[]>{
			new string[]{"TRUCK","CAR"},
			new string[]{"TOOTHBRUSH","COMB"},
			new string[]{"PLANE","TRAIN"},
			new string[]{"BOOT","GLOVE"},
			new string[]{"BALL","SHOE"},
			new string[]{"BEAR","DINOSAUR"},
			new string[]{"FRUIT","GRAIN"},
			new string[]{"FORK","SPOON"},
			new string[]{"EGG","NEST"},
			new string[]{"SHIRT","PANTS"}
		};
		
		private List<string[]> _egHouses = new List<string[]>{
			new string[]{"TRIANGLE","SQUARE"},
			new string[]{"CIRCLE","RECTANGLE"},
			new string[]{"RHOMBUS","OVAL"},
			new string[]{"TRIANGLE","RECTANGLE"},
			new string[]{"CIRCLE","RHOMBUS"},
			new string[]{"OVAL","SQUARE"},
			new string[]{"RECTANGLE","OVAL"},
			new string[]{"SQUARE","CIRCLE"},
			new string[]{"OVAL","TRIANGLE"},
			new string[]{"RHOMBUS","TRIANGLE"}
		};
		
		private List<string[]> _dgHouses = new List<string[]>{
			new string[]{"BEARS","TRIANGLE"},
			new string[]{"CIRCLE","RECTANGLE"},
			new string[]{"CIRCLE","RHOMBUS"},
			new string[]{"OVAL","SQUARE"},
			new string[]{"RECTANGLE","OVAL"},
			new string[]{"RHOMBUS","BEARS"},
			new string[]{"RHOMBUS","OVAL"},
			new string[]{"SQUARE","BEARS"},
			new string[]{"TRIANGLE","RECTANGLE"},
			new string[]{"TRIANGLE","SQUARE"}
		};
		private List<string[]> _dgPics = new List<string[]>{
			new string[]{"BEAR","TRIANGLE"},
			new string[]{"CIRCLE","RECTANGLE"},
			new string[]{"CIRCLE","RHOMBUS"},
			new string[]{"OVAL","SQUARE"},
			new string[]{"RECTANGLE","OVAL"},
			new string[]{"RHOMBUS","BEAR"},
			new string[]{"RHOMBUS","OVAL"},
			new string[]{"SQUARE","BEAR"},
			new string[]{"TRIANGLE","RECTANGLE"},
			new string[]{"TRIANGLE","SQUARE"}
		};							   

		private string[] _currentHouses = new string[2];
		private string[] _currentHousesPics = new string[2];
		
		private List<SortingObject> _objects = new List<SortingObject>();
		private List<SortingObject> _answeredObjects = new List<SortingObject>();
		
		private bool _incorrect;
		private int _houseCount = 0;
		private bool draggingObj = false;
		private float baseScale;
		
		private SortingObject _currentObject;
		private int helpPrompted;
		
		private new SortingTutorial tutorial_mc;

		public Sorting( SkillLevel level, SessionInfo  currentSession) : base(level, currentSession)
		{
			BACKGROUND = "Background";
			ARTIFACT = "Artifact";
			
			Init (null);
		}
		
		public override void Dispose ()
		{
			base.Dispose();
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			
			if( stage == null ) return;
			
			if(stage.hasEventListener(MouseEvent.MOUSE_DOWN))
				stage.removeEventListener(MouseEvent.MOUSE_DOWN, onStageDown);
			if(stage.hasEventListener(MouseEvent.MOUSE_MOVE))
				stage.removeEventListener(MouseEvent.MOUSE_MOVE, onObjectMove);
			if(stage.hasEventListener(MouseEvent.MOUSE_UP))
				stage.removeEventListener(MouseEvent.MOUSE_UP, onObjectUp);
			if(house1_mc != null)
				house1_mc.Destroy();
			if(house2_mc != null)
				house2_mc.Destroy();
			if(_objects != null)
			{
				foreach(SortingObject o in _objects)
					o.Destroy();
			}
			if(_answeredObjects != null)
			{
				foreach(SortingObject obj in _answeredObjects)
					obj.Destroy();
			}
		}

		public Sorting( string swf ) : base( swf + ":Skill" )
		{
			BACKGROUND = "Background";
			ARTIFACT = "Artifact";
			
			PreCacheAssets();
		}
		
		private void PreCacheAssets()
		{
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle("Sorting", OnAssetsLoaded));
		}
		
		void OnAssetsLoaded(AssetBundle bundle)
		{
			if( bundle != null )
			{
				Init( null );
			}
		}
		
		public override void Init (MovieClip parent)
		{
			base.Init (parent);
			houseContainer_mc = new DisplayObjectContainer();
			houseContainer_mc.width = MainUI.STAGE_WIDTH;
			houseContainer_mc.height = MainUI.STAGE_HEIGHT;
			addChild( houseContainer_mc );
			houseContainer_mc.alpha = 0;
			
			sortContainer_mc = new DisplayObjectContainer();
			sortContainer_mc.width = MainUI.STAGE_WIDTH;
			sortContainer_mc.height = MainUI.STAGE_HEIGHT;
			addChild( sortContainer_mc );
			sortContainer_mc.alpha = 0;
			timer = totalTime;
			
			//todo positioning wtf?
			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			house1_mc = new SortingHouseObject(MovieClipFactory.CreateSortingHouse(), "CIRCLE");
			house2_mc = new SortingHouseObject(MovieClipFactory.CreateSortingHouse(), "CIRCLE");
			house1_mc.Target.gotoAndStop(1);
			house2_mc.Target.gotoAndStop(1);
			
			baseScale = 520f / house1_mc.width;
			house1_mc.scaleX = house1_mc.scaleY = baseScale;
			house2_mc.scaleX = house2_mc.scaleY = baseScale;
			
			house1_mc.x = 305;
			house2_mc.x = 1085;
			house2_mc.y = house1_mc.y = 30;
			
			house1_mc.alpha = 0;
			house2_mc.alpha = 0;
			houseContainer_mc.addChild( house1_mc );
			houseContainer_mc.addChild( house2_mc );
			
			
			List<SortingHouseObject> houses = new List<SortingHouseObject>();
			houses.Add(house1_mc);
			houses.Add(house2_mc);
			//houseContainer_mc.LayoutGameTiles( houses, 2f , 2);
			//houseContainer_mc.ShiftGameTiles(houses, new Vector2(0, -Screen.height*0.5f));
			
			Debug.Log("level: " + _level);
			switch(_level)
			{
				// Tutorial
				case SkillLevel.Tutorial:
					totalOpportunities = 1;
					opportunityComplete = false;

					tutorial_mc = new SortingTutorial();
					addChild( tutorial_mc.View );
					tutorial_mc.View.addEventListener( TutorialEvent.START, onTutorialStart );
					tutorial_mc.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
					tutorial_mc.Resize( Screen.width, Screen.height );				
					SetCurrentHouses("TRIANGLE", "CIRCLE", "TRIANGLE", "CIRCLE");
					if(resumingSkill) {
						tutorial_mc.OnStart();
						fadeInContainers().OnComplete( OpportunityStart );
					}
				break;
				
				//All other levels
				
				case SkillLevel.Emerging:
				case SkillLevel.Developing:
					totalOpportunities = 10;
				
					house1_mc.alpha = 0;
					house2_mc.alpha = 0;
				
					if(resumingSkill)
					{
						parseSessionData( currentSession.sessionData );
					//	nextOpportunity();
					}
				break;
				case SkillLevel.Developed:
				case SkillLevel.Completed:
					totalOpportunities = 10;
					
					house1_mc.alpha = 0;
					house2_mc.alpha = 0;
					//house1_mc.y = 30;
					//house2_mc.y = 30;
					
					if(resumingSkill)
					{
						parseSessionData( currentSession.sessionData );
					//	nextOpportunity();
					}
				break;
			}
			Resize( Screen.width, Screen.height );
		}
		
		public override void Start()
		{
			MarkInstructionStart();
		}
		
		protected override void resetTutorial()
		{
			objectsOff();
			while(_objects.Count > 0)
			{
				_objects[0].Destroy();
				sortContainer_mc.removeChild(_objects[0]);
				_objects.Remove(_objects[0]);
			}
			while(_answeredObjects.Count > 0)
			{
				_answeredObjects[0].Destroy();
				_answeredObjects.Remove(_answeredObjects[0]);
			}
			
			tutorial_mc = new SortingTutorial();
			tutorial_mc.Resize(Screen.width, Screen.height);
			addChild(tutorial_mc.View);
			tutorial_mc.View.addEventListener(TutorialEvent.START, onTutorialStart);
			tutorial_mc.View.addEventListener(TutorialEvent.COMPLETE, onTutorialComplete);
			tutorial_mc.OnStart();
			
			house1_mc.ShowFilter();
			
			currentOpportunity--;
		}
		
		private void SetCurrentHouses(string house1, string house2, string pic1, string pic2)
		{
			Debug.Log("SETTING HOUSES");
			_currentHouses[0] = house1;
			_currentHouses[1] = house2;
			
			//Debug.Log("SETTING TO: " + _currenho
			
			_currentHousesPics[0] = pic1;
			_currentHousesPics[1] = pic2;
			
			if(house1 != string.Empty)
				house1_mc.GoToImage(pic1);
			if(house2 != string.Empty)
				house2_mc.GoToImage(pic2);
		}
		
		
		#region Audio Listeners
		
		private void OnAudioWord( string word )
		{
		}
		
		//todo come back to this
		public override void OnAudioWordComplete( string word )
		{			
			Debug.Log("WORD: " + word);
			AudioClip clip = null;
			SoundEngine.SoundBundle bundle = null;
			foreach(SortingObject obj in _objects)
			{
				obj.HideFilter();
			}
			
			house1_mc.HideFilter();
			house2_mc.HideFilter();
			
			foreach(SortingObject obj in _answeredObjects)
			{
				if(obj.visible)
				{
					float tempScale = obj.scaleX;
					float midX = obj.x + obj.width / 2f;
					float midY = obj.y + obj.height / 2f;
					obj.scaleX = 0.2f * baseScale;
					obj.scaleY = 0.2f * baseScale;
					Tweener.addTween(obj, Tweener.Hash("time", 0.35f, "alpha", 0.0f, "x", midX - obj.width / 2f, "y", midY - obj.height / 2f,
										"scaleX", 0.2f * baseScale, "scaleY", 0.2f * baseScale, "visible",false, "transition", Tweener.TransitionType.easeOutQuad));
					obj.scaleX = tempScale;
					obj.scaleY = tempScale;
					
				}
			}
			
			if(opportunityComplete)
			{
				Debug.Log("OPPORTUNITY COMPLETE");
				switch( word )
				{
					case CLICK_CORRECT:
						dispatchEvent(new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false));
					break;
					case THEME_COMPLIMENT:
						Debug.Log("THEME COMPLEMENT");
						Debug.Log("CORRECT? "+ opportunityCorrect);
						if(_level == SkillLevel.Tutorial)
						{
							bundle = new SoundEngine.SoundBundle();
							clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SORTING, "Narration/5B-Reinforcement Audio/5B-Helped-EG/5B-Helped-triangle-circle");
							bundle.AddClip( clip, YOU_FOUND, clip.length/2 );
							SoundEngine.Instance.PlayBundle( bundle );
							
						}
						else
						{
							//All of these are wrong - I think I need new audio files
							switch(_level)
							{
								case SkillLevel.Emerging:
									bundle = new SoundEngine.SoundBundle();
									clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SORTING, "Narration/5B-Reinforcement Audio/5B-Helped-EG/5B-Helped-"+_currentHouses[0].ToString().ToLower()+"-"+_currentHouses[1].ToString().ToLower());
									bundle.AddClip( clip, YOU_HELPED, clip.length );
									SoundEngine.Instance.PlayBundle( bundle );
								break;
								case SkillLevel.Developing:
									bundle = new SoundEngine.SoundBundle();
									clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SORTING, "Narration/5B-Reinforcement Audio/5B-Helped-DG/5B-Helped-"+_currentHouses[0].ToString().ToLower()+"-"+_currentHouses[1].ToString().ToLower());
									bundle.AddClip( clip, YOU_HELPED, clip.length );
									SoundEngine.Instance.PlayBundle( bundle );
								break;
								case SkillLevel.Developed:
								case SkillLevel.Completed:
									bundle = new SoundEngine.SoundBundle();
									clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SORTING, "Narration/5B-Reinforcement Audio/5B-Helped-DDCD/5B-Helped-"+_currentHouses[0].ToString().ToLower()+"-"+_currentHouses[1].ToString().ToLower());
									bundle.AddClip( clip, YOU_HELPED, clip.length );
									SoundEngine.Instance.PlayBundle( bundle );
								break;
		
							}
						}
					break;
					case THEME_CRITICISM:
						bundle = new SoundEngine.SoundBundle();
						clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SORTING, "Narration/5B-Inactivity Reinforcement Audio/5B-Belong-" + _currentObject.type.ToLower());
						bundle.AddClip( clip, BELONGS, clip.length );
						SoundEngine.Instance.PlayBundle( bundle );
					break;
					case BELONGS:
						fadeOutContainers();
						dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
					
					break;
					case YOU_HELPED:
						switch(_level)
						{
						case SkillLevel.Completed:
						case SkillLevel.Developed:
							clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SORTING, "Narration/5B-Reinforcement Audio/5B-I-will-tell-the-gnomes");
							break;
						case SkillLevel.Developing:
						case SkillLevel.Emerging:
							clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SORTING, "Narration/5B-Reinforcement Audio/5B-The-gnomes-thank-you");
							break;
						}
						bundle = new SoundEngine.SoundBundle();
						bundle.AddClip( clip, YOU_NAP, clip.length );
						SoundEngine.Instance.PlayBundle(bundle);
					break;
					case YOU_NAP:
						fadeOutContainers();
						Debug.Log("CORRECT? "+ opportunityCorrect);
						if(opportunityCorrect)
						{
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
						}
						else
						{
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
						}
					break;
					case YOU_FOUND:
						bundle = new SoundEngine.SoundBundle();
						clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SORTING, "Narration/5B-Skill Instruction Audio/5B-lets-start-sorting");
						bundle.AddClip( clip, NOW_LETS, clip.length );
						SoundEngine.Instance.PlayBundle(bundle);
					break;
					case NOW_LETS:
						dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
					break;
					case NOW_ITS:
						startTimer();
						timeoutCount = 0;
					break;
					case LETS_TRY_AGAIN:
						dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
					break;
					case CLICK_INCORRECT:
						dispatchEvent(new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false));
					break;
					case HELP_PROMPT:
						doItForThem();
					break;
					default:
					break;
				}
				
			}
			else
			{
				switch( word )
				{
					//case "TUT_INTRO":
					case INTRO:
						if(_level == SkillLevel.Tutorial && word == INTRO)
						{
							tutorial_mc.OnStart();
							fadeInContainers();
						}
						house1_mc.ShowFilter();
						if(_level != SkillLevel.Tutorial)
							nextOpportunity();
					break;
					case HOUSE_IS1:
						house1_mc.HideFilter();
						house2_mc.ShowFilter();
						if(_level != SkillLevel.Tutorial)
							HouseAudio();
						else
							tutorial_mc.gotoCircleHouse();
					break;
					case HOUSE_IS2:
						house2_mc.HideFilter();
						if(_level != SkillLevel.Tutorial)
							HouseAudio();
						else
							tutorial_mc.goToDragCircle( currentSession.SPN_FLAG );
					break;
					case NOW_ITS:
					case TOUCH_DRAG:
						objectsOn();
						startTimer();
					break;					
					case THEME_INTRO:


						if( !currentSession.skipIntro )
						{
							clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SORTING, "Narration/5B-StoryIntroduction" );
							bundle = new SoundEngine.SoundBundle();
							bundle.AddClip(clip, INTRO, clip.length);
							SoundEngine.Instance.PlayBundle(bundle);
						}
						else
							OnAudioWordComplete( INTRO );
					break;
					case THEME_CRITICISM:
						bundle = new SoundEngine.SoundBundle();
						clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SORTING, "Narration/5B-Inactivity Reinforcement Audio/5B-Belong-" + _currentObject.type.ToLower());
						bundle.AddClip( clip, BELONGS, clip.length );
						SoundEngine.Instance.PlayBundle( bundle );
					
					break;
					case BELONGS:						
						if(_incorrect)
							_incorrect = false;
					
						bundle = new SoundEngine.SoundBundle();
						clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SORTING, "Narration/5B-Skill Instruction Audio/5B-Send-another-one");
						bundle.AddClip( clip);
						SoundEngine.Instance.PlayBundle( bundle );
						
						objectsOn();
						startTimer();
					break;
					case THEME_COMPLIMENT:
						Debug.Log("THEME COMPLEMENT");
						Debug.Log("CORRECT? "+ opportunityCorrect);
						if(_level == SkillLevel.Tutorial)
						{
							bundle = new SoundEngine.SoundBundle();
							clip = SoundUtils.LoadGlobalSound(MovieClipFactory.SORTING, "Narration/5B-Reinforcement Audio/5B-Helped-EG/5B-Helped-triangle-circle");
							bundle.AddClip( clip, YOU_FOUND, clip.length/2f );
							SoundEngine.Instance.PlayBundle( bundle );
							
						}
					break;
					case PLEASE_FIND:
					case THIS_IS :
						startTimer();
						//enableSortItems();
					break;
					case NOW_LETS:
						nextOpportunity();
					break;
					case TOUCH_HELP:
						startTimer();
					break;
					case KEEP_SHAPE:
					case KEEP_PICTURE:
						startTimer();
					break;
					case LETS_TRY_AGAIN:
						resetTutorial();
					break;
					case CLICK_INCORRECT:
						dispatchEvent(new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false));
					break;
					case HELP_PROMPT:
						getNextHelpPrompt();
					break;
					case CLICK_CORRECT:
						if(!draggingObj)
							startTimer();
					break;
					default:
						//startTimer();
					break;
				}
			}
			
		}
		#endregion
		
		private void onMouseDown( CEvent e )
		{
			onObjectDown(e.currentTarget as SortingObject);
		}
		private void onStageDown( CEvent e )
		{
			Vector2 cMouse = new Vector2(sortContainer_mc.mouseX, sortContainer_mc.mouseY);
			foreach(SortingObject so in _objects)
			{
				if(cMouse.x > so.x && cMouse.x < so.x + (257*0.8f) && cMouse.y > so.y && cMouse.y < so.y + (257*0.8f))
				{
					onObjectDown(so);
				}
			}
		}
		private void onObjectDown( SortingObject cObj )
		{
			if (currentSession.paused) 
				return;

			PlayClickSound();
			stopTimer();
			
			_incorrect = false;

			foreach(SortingObject obj in _answeredObjects)
			{
				if(obj.visible)//if(obj.enabled)
				{
					float tempScale = obj.scaleX;
					float midX = obj.x + obj.width / 2f;
					float midY = obj.y + obj.height / 2f;
					obj.scaleX = 0.2f * baseScale;
					obj.scaleY = 0.2f * baseScale;
					Tweener.addTween(obj, Tweener.Hash("time", 0.35f, "alpha", 0.0f, "x", midX - obj.width / 2f, "y", midY - obj.height / 2f,
									"scaleX", 0.2f * baseScale, "scaleY", 0.2f * baseScale, "visible", false, "transition", Tweener.TransitionType.easeOutQuad));
					obj.scaleX = tempScale;
					obj.scaleY = tempScale;
					
				}
			}
			/*
			instructions_mc.stop();
			thisIs_mc.stop();
			touch_mc.stop();
			words_mc.stop();
			you_mc.stop();
			
			timer.reset();
			timeoutCount = 0;
			
			for(item in _objects)
			{
				_objects[item].filters = [];
			}
			
			house1_mc.filters = [];
			house2_mc.filters = [];
			
			*/
			_currentObject = cObj;
			
			_currentObject.baseX = _currentObject.x;
			_currentObject.baseY = _currentObject.y;
			
			_currentObject.ScaleCentered(1.2f * baseScale);
			
			dragOffset = new Vector2(_currentObject.x-_currentObject.parent.mouseX, _currentObject.y-_currentObject.parent.mouseY);
			
			sortContainer_mc.setChildIndex(_currentObject,sortContainer_mc.numChildren - 1);
			
			stage.removeEventListener(MouseEvent.MOUSE_DOWN, onStageDown);
			_currentObject.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onObjectMove);
			stage.addEventListener(MouseEvent.MOUSE_UP, onObjectUp);
			
			onObjectMove(null);
			draggingObj = true;
		}
		
		protected Vector2 dragOffset;
		
		private void onObjectMove( CEvent e )
		{
			_currentObject.x = _currentObject.parent.mouseX+dragOffset.x;
			_currentObject.y = _currentObject.parent.mouseY+dragOffset.y;
			
			Rectangle r = _currentObject.getBounds(this);
			if(checkRectIntersect(r, house1_mc.getBounds(this)))
			{
				house1_mc.ShowFilter();
			}
			else
			{
				house1_mc.HideFilter();
			}
			
			if(checkRectIntersect(r, house2_mc.getBounds(this)))
			{
				house2_mc.ShowFilter();
			}
			else
			{
				house2_mc.HideFilter();
			}
		}
		
		private void onObjectUp( CEvent e )
		{
			draggingObj = false;
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, onObjectMove);
			stage.removeEventListener(MouseEvent.MOUSE_UP, onObjectUp);
			
			SoundEngine.Instance.SendCancelToken();
			//SoundEngine.Instance.StopAll();
			
			_currentObject.ScaleCentered(0.8f * baseScale);
			Rectangle cOBounds = _currentObject.getBounds(this);
			
			house1_mc.HideFilter();
			house2_mc.HideFilter();
			
			

			Rectangle h1 = house1_mc.getBounds(_currentObject.parent);
			Rectangle h2 = house2_mc.getBounds(_currentObject.parent);
			Rectangle cLocal = _currentObject.getBounds(_currentObject.parent);
			

			if(checkRectIntersect(cOBounds,house1_mc.getBounds(this)))
			{
				
				if(_currentObject.house == _currentHouses[0])
				{
					//_currentObject.enabled = false;
					_answeredObjects.Add(_currentObject);
					
					_objects.Remove(_currentObject);

					Tweener.addTween(_currentObject, Tweener.Hash("time", 0.35f, "x", h1.left + (h1.width - cLocal.width) * 0.5f, "y", h1.top + (h1.height - cLocal.height) * 0.5f, "transition", Tweener.TransitionType.easeOutQuad));
					foreach (SortingObject obj in _objects)
					{
						//obj.enabled = false;
					}
					_currentObject.ShowFilter();
					PlayCorrectSound();
					objectsOn();
					
					AddNewActionData(formatActionData( "move", _currentObject.id, _currentObject.parent.localToGlobal(new Vector2( _currentObject.x, _currentObject.y )), true ));
					
					if(_objects.Count == 0)
					{
						if(opportunityAnswered == false)
						{
							opportunityAnswered = true;
							opportunityCorrect = true;
						}
						opportunityComplete = true;
						objectsOff();
					}
				}
				else
				{
					if(_level == SkillLevel.Tutorial)
					{
						stage.addEventListener(MouseEvent.MOUSE_DOWN, onStageDown);
						_currentObject.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
						_currentObject.x = _currentObject.baseX;
						_currentObject.y = _currentObject.baseY;
						startTimer();
					}
					else
					{

						// incorrect
						Debug.Log("*******THIS IS AN INCORRECT ANSWER");
						foreach(SortingObject obj in _objects)
						{
							//obj.enabled = false;
						}		
						
						_incorrect = true;
		
						
						if(opportunityAnswered == false)
						{
							opportunityAnswered = true;
							opportunityCorrect = false;
						}
						
						if(_currentObject.house == _currentHouses[1])
						{
							//_currentObject.enabled = false;
							_answeredObjects.Add(_currentObject);
							
							

							Tweener.addTween(_currentObject, Tweener.Hash("time", 0.35f, "x",  h2.x + ((h2.width * 0.5f) - (cLocal.width * 0.5f)), "y", h2.y + ((h2.height * 0.5f) - (cLocal.height * 0.5f)), "transition", Tweener.TransitionType.easeOutQuad));
							
							_objects.Remove(_currentObject);
							
							if(_objects.Count == 0)
							{
								opportunityComplete = true;
							}
						}
						
						objectsOff();
						
						PlayIncorrectSound();
					
						AddNewActionData(formatActionData( "move", _currentObject.id, _currentObject.parent.localToGlobal(new Vector2( _currentObject.x, _currentObject.y )), false ));
					}
				}
			}
			
			else if(checkRectIntersect(_currentObject.getBounds(this),house2_mc.getBounds(this)))
			{
				
				//_currentObject.scaleX = 0.4f;
				//_currentObject.scaleY = 0.4f;
				
				if(_currentObject.house == _currentHouses[1])
				{
					//_currentObject.enabled = false;
					_answeredObjects.Add(_currentObject);
					
					_objects.Remove(_currentObject);
					
					Tweener.addTween(_currentObject, Tweener.Hash("time", 0.35f, "x",  h2.left + (h2.width - cLocal.width) * 0.5f, "y", h2.top + (h2.height - cLocal.height) * 0.5f, "transition", Tweener.TransitionType.easeOutQuad));
					
					foreach(SortingObject obj in _objects)
					{
						//_objects[item].object = false;
					}
					_currentObject.ShowFilter();
					PlayCorrectSound();
					objectsOn();
					
					AddNewActionData(formatActionData( "move", _currentObject.id, _currentObject.parent.localToGlobal(new Vector2( _currentObject.x, _currentObject.y )), true ));
					
					if(_objects.Count == 0)
					{
						if(opportunityAnswered == false)
						{
							opportunityAnswered = true;
							opportunityCorrect = true;
						}
						opportunityComplete = true;
						objectsOff();
					}
				}
				else
				{
					if(_level == SkillLevel.Tutorial)
					{
						stage.addEventListener(MouseEvent.MOUSE_DOWN, onStageDown);
						_currentObject.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
						_currentObject.x = _currentObject.baseX;
						_currentObject.y = _currentObject.baseY;
						startTimer();
					}
					else
					{
				
						// incorrect
						foreach(SortingObject obj in _objects)
						{
							//_objects[item].enabled = false;
						}
						
						_incorrect = true;
						
						if(opportunityAnswered == false)
						{
							opportunityAnswered = true;
							opportunityCorrect = false;
						}
						
						if(_currentObject.house == _currentHouses[0])
						{
							//_currentObject.enabled = false;
							_answeredObjects.Add(_currentObject);
							
							Tweener.addTween(_currentObject, Tweener.Hash("time", 0.35f, "x", h1.left + (h1.width - cLocal.width) * 0.5f, "y", h1.top + (h1.height - cLocal.height) * 0.5f, "transition", Tweener.TransitionType.easeOutQuad));
							
							_objects.Remove(_currentObject);
							
							if(_objects.Count == 0)
							{
								opportunityComplete = true;
							}
						}
						
						objectsOff();
						
						PlayIncorrectSound();
					
						AddNewActionData(formatActionData( "move", _currentObject.id, _currentObject.parent.localToGlobal(new Vector2( _currentObject.x, _currentObject.y )), false ));
					}
				}				
			}
			else
			{
				stage.addEventListener(MouseEvent.MOUSE_DOWN, onStageDown);
				_currentObject.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
				_currentObject.x = _currentObject.baseX;
				_currentObject.y = _currentObject.baseY;
				startTimer();
			}
		}
		
		public override void nextOpportunity()
		{
			MarkInstructionStart();
			
			Debug.LogWarning("Opportunity: " + currentOpportunity);
			helpPrompted = 0;
			
			switch(_level)
			{
				case SkillLevel.Tutorial:
					if(currentOpportunity > 0)
					{
						tallyAnswers();
					}
					else
					{
						currentOpportunity++;
						
						SetCurrentHouses("TRIANGLE","CIRCLE", "TRIANGLE", "CIRCLE");
						numObjs = 2;
						
						AddNewOppData( audioNames.IndexOf("Narration/5B-Skill Instruction Audio/5B-TT-Now-its-your-turn") );
						
						AddNewObjectData( formatObjectData( _objects[0].getLabel(), _objects[0].parent.localToGlobal(new Vector2(_objects[0].x,_objects[0].y)), true ) );
						_objects[0].id = 0;
					
						AddNewObjectData( formatObjectData( "house_" + house1_mc.type, house1_mc.parent.localToGlobal(new Vector2(house1_mc.x, house1_mc.y)) ) );
						AddNewObjectData( formatObjectData( "house_" + house2_mc.type, house2_mc.parent.localToGlobal(new Vector2(house2_mc.x, house2_mc.y)) ) );
						
						fadeInContainers().OnComplete( OpportunityStart );
					}
				break;
				
				case SkillLevel.Emerging:
					setNextOpportunity(2, false);
				break;
				
				case SkillLevel.Developing:
					setNextOpportunity(4, false);
				break;
				
				case SkillLevel.Developed:
					setNextOpportunity(8, true);
				break;
				
				case SkillLevel.Completed:
					setNextOpportunity(8, true);
				break;
				
			}
			
		}
		
		private void setNextOpportunity( int sortingObjs, bool ddHouses)
		{
			Debug.Log("SETTING OPPORTUNITY");
			Debug.Log(sortingObjs + " " + ddHouses);
			Debug.Log(currentOpportunity);
			
			if(currentOpportunity > 0)
			{
				if(opportunityComplete)
				{
					if(opportunityCorrect)
					{
						//NumCorrect++;
					}
					else
					{
						//NumIncorrect++;
					}
				}
				else
				{
					//NumIncorrect++;
				}
			}
			if(currentOpportunity < 10)
			{
				opportunityComplete = false;
				opportunityCorrect = false;
				opportunityAnswered = false;
				
				currentOpportunity++;
				
				SetCurrentHouses("", "", "", "");
				int ranNum = 0;

				switch(_level)
				{
					case SkillLevel.Emerging:
						ranNum = Mathf.FloorToInt(UnityEngine.Random.value * _egHouses.Count);
						if (ranNum > _egHouses.Count - 1)
							ranNum--;
						if(resumingSkill)
						{
							ranNum = 0;
							resumingSkill = false;
						}
						SetCurrentHouses(_egHouses[ranNum][0], _egHouses[ranNum][1], _egHouses[ranNum][0], _egHouses[ranNum][1]);
						_egHouses.RemoveAt(ranNum);
					break;
					case SkillLevel.Developed:
					case SkillLevel.Completed:
						ranNum = Mathf.FloorToInt(UnityEngine.Random.value * _ddHouses.Count);
						if (ranNum > _ddHouses.Count - 1)
							ranNum--;
						if(resumingSkill)
						{
							ranNum = 0;
							resumingSkill = false;
						}
						SetCurrentHouses(_ddHouses[ranNum][0], _ddHouses[ranNum][1], _ddPics[ranNum][0], _ddPics[ranNum][1]);
						_ddHouses.RemoveAt(ranNum);
						_ddPics.RemoveAt(ranNum);
					break;
					case SkillLevel.Developing:
						ranNum = Mathf.FloorToInt(UnityEngine.Random.value * _dgHouses.Count);
						if (ranNum > _dgHouses.Count - 1)
							ranNum--;
						if(resumingSkill)
						{
							ranNum = 0;
							resumingSkill = false;
						}
						SetCurrentHouses(_dgHouses[ranNum][0], _dgHouses[ranNum][1], _dgPics[ranNum][0], _dgPics[ranNum][1]);
						_dgHouses.RemoveAt(ranNum);
						_dgPics.RemoveAt(ranNum);
					break;
					
				}
				
				numObjs = sortingObjs;
				fadeInContainers().OnComplete( OpportunityStart );
				objectsOff();
				
			}
			else
			{
				/*// tally answers
				percent = Mathf.Round((NumCorrect / (NumCorrect + NumIncorrect)) * 100);
				
				//timer.reset();
				numObjs = 0;
				if(_complete == false)
				{
					_complete = true;
					dispatchEvent(new SkillEvent(SkillEvent.SKILL_COMPLETE, true, false));
				}*/
				stopTimer();
				tallyAnswers();
			}

		}
		
		private int numObjs
		{
			get{ return _objects.Count; }
			set
			{
				int i;
							
				foreach(SortingObject obj in _objects)
				{
					obj.Destroy();
					obj.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
					Tweener.removeTweens(obj);
					sortContainer_mc.removeChild(obj);
				}foreach(SortingObject obj in _answeredObjects)
				{
					obj.Destroy();
					Tweener.removeTweens(obj);
				}

				_objects = new List<SortingObject>();
				_answeredObjects = new List<SortingObject>();;
				
				List<int> typeNumArray = new List<int>();
				SortingObject tempObject;
				for(i = 0; i < typeNums[Array.IndexOf(types, _currentHousesPics[0])]; i++)
				{
					typeNumArray.Add(i);
				}

				for(i = 0; i < Mathf.FloorToInt(value / 2); i++)
				{
					tempObject = new SortingObject(MovieClipFactory.CreateSortingObject(), _currentHousesPics[0], _currentHouses[0]);
					
					int ranNum = Mathf.FloorToInt(UnityEngine.Random.value * typeNumArray.Count);
					if( ranNum > typeNumArray.Count - 1)
						ranNum--;
					tempObject.num = typeNumArray[ranNum];
				
					typeNumArray.RemoveAt(ranNum);
					
					tempObject.UpdateImage();
					//tempObject.mouseChildren = false;
					//tempObject.enabled = false;f
					//tempObject.buttonMode = true;
					
					//todo back to this
					//tempObject.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
					//tempObject.addEventListener(MouseEvent.MOUSE_MOVE, onMouseDrag);
					sortContainer_mc.addChild(tempObject);
					//tempObject.scaleX = 0.4f;
					//tempObject.scaleY = 0.4f;
					_objects.Add(tempObject);
				}
				
				if(_level != SkillLevel.Developing)
				{
					typeNumArray = new List<int>();
					
					for(i = 0; i < typeNums[Array.IndexOf(types, _currentHousesPics[1])]; i++)
					{
						typeNumArray.Add(i);
					}
				}

				for(i = 0; i < value - Mathf.FloorToInt(value / 2); i++)
				{
					tempObject = new SortingObject(MovieClipFactory.CreateSortingObject(), _currentHousesPics[1], _currentHouses[1]);
					
					if(_level == SkillLevel.Emerging)
						tempObject.num = _objects[0].num;
					else
					{
						int ranNum = Mathf.FloorToInt(UnityEngine.Random.value * typeNumArray.Count);
						if (ranNum > typeNumArray.Count - 1)
							ranNum--;
						tempObject.num = typeNumArray[ranNum];
					
						typeNumArray.RemoveAt(ranNum);
					}
					
					tempObject.UpdateImage();
					//tempObject.mouseChildren = false;
					//tempObject.enabled = false;
					//tempObject.buttonMode = true;
					
					//todo come back to this
					//tempObject.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
					//tempObject.addEventListener(MouseEvent.MOUSE_MOVE, onMouseDrag);
					sortContainer_mc.addChild(tempObject);
					//tempObject.scaleX = 0.4f;
					//tempObject.scaleY = 0.4f;
					_objects.Add(tempObject);
				}
				
				if(_level == SkillLevel.Tutorial)
				{
					if(_objects.Count >= 2)
					{
						_answeredObjects.Add(_objects[0]);
						_objects[0].type = "CIRCLE";
						_objects[0].house = "CIRCLE";
						_objects[0].num = 0;
						_objects[0].UpdateImage();
						_objects[0].alpha = 0;
						_objects[0].x = 745.2f;
						_objects[0].y = 810f;
						_objects[1].type = "TRIANGLE";
						_objects[1].house = "TRIANGLE";
						_objects[1].num = 1;
						_objects[1].UpdateImage();
						_objects[1].visible = true;
						_objects[1].ScaleCentered(0.8f * baseScale);
						_objects[1].x = sortContainer_mc.width/2 + 10;
						_objects[1].y = 810f;
						
						_objects.Remove(_objects[0]);
					}
					
				}
				else
				{
					_objects.Shuffle();
				
					float startX = sortContainer_mc.width/2;
					float startY = 810f;
					
					float wide = 270 * 0.8f;
					float high = 270 * 0.8f;

                    int xBuffer = 20;
                    bool developingAndroid = false;
                    if (PlatformUtils.GetPlatform() == Platforms.ANDROID) {
                        if (_level == SSGCore.SkillLevel.Developing) {
                            developingAndroid = true;
                            xBuffer = 200;
                        }
                    }

					
					if( _objects.Count > 4) {
						startX -= ((wide*4)/2) + 60;
                    } else {
                        float offsetX = ((wide*_objects.Count)/2) + (xBuffer * (_objects.Count - 1));

                        if (developingAndroid) {
                            offsetX = ((wide*_objects.Count)) + (xBuffer * (_objects.Count - 1));
                            offsetX *= 0.5f;
                            startX -= 100f;
                        }

                        startX -= offsetX;
                    }


					if (_objects.Count > 4) startY -= (high * 0.75f) + 10;
					float workingX = startX;
					float tempScale;

					for( i = 0; i< _objects.Count; i++)
					{
						tempScale = 0.8f * baseScale;
						
//						if( _objects[i].width * tempScale > wide )
//							tempScale = wide / _objects[i].width;
						
						_objects[i].scaleX = _objects[i].scaleY = tempScale;
						if( i == 4 ) workingX = startX;
						_objects[i].x = workingX;
                        workingX += wide + xBuffer;
						_objects[i].y = startY;
						if(i > 3) _objects[i].y += high + 20;
					}
					if(_level == SkillLevel.Emerging)
					{
						_objects[0].x -= 200;
						_objects[1].x += 200;
					}
				}
				
				//sortContainer_mc.LayoutGameTiles(_objects, 0.4f, 4);
				//sortContainer_mc.ShiftGameTiles(_objects, new Vector2(0, MainUI.STAGE_HEIGHT * 0.5f));
				//updateObjects(false);
			}
		}
		
		private void objectsOn()
		{
			foreach(SortingObject so in _objects)
			{
				so.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			}
			stage.addEventListener(MouseEvent.MOUSE_DOWN, onStageDown);
			if(_level != SkillLevel.Tutorial) MarkInstructionEnd();
		}
		
		private void objectsOff()
		{
			foreach(SortingObject so in _objects)
			{
				if(so.hasEventListener(MouseEvent.MOUSE_DOWN))
				{
					so.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
				}
			}
			if(stage != null && stage.hasEventListener(MouseEvent.MOUSE_DOWN))
				stage.removeEventListener(MouseEvent.MOUSE_DOWN, onStageDown);
			MarkInstructionStart();
		}
	
		private TweenerObj fadeInContainers()
		{
			
			foreach(SortingObject obj in _objects)
			{
				//obj.enabled = false;
			}

			Tweener.addTween(house1_mc, Tweener.Hash("time", 0.35f, "alpha", 1.0f, "transition", Tweener.TransitionType.easeOutQuad));
			Tweener.addTween(house2_mc, Tweener.Hash("time", 0.35f, "alpha", 1.0f, "transition", Tweener.TransitionType.easeOutQuad));
			Tweener.addTween(houseContainer_mc, Tweener.Hash("time", 0.35f, "alpha", 1.0f, "transition", Tweener.TransitionType.easeOutQuad));
			return Tweener.addTween(sortContainer_mc, Tweener.Hash("time", 0.35f, "alpha", 1.0f, "transition", Tweener.TransitionType.easeInOutQuint) );

		}
		
		private TweenerObj fadeOutContainers()
		{
			foreach(SortingObject obj in _objects)
			{
				//obj.enabled = false;
			}
			
			Tweener.addTween(house1_mc, Tweener.Hash("time", 0.35f, "alpha", 0.0f, "transition", Tweener.TransitionType.easeOutQuad));
			Tweener.addTween(house2_mc, Tweener.Hash("time", 0.35f, "alpha", 0.0f, "transition", Tweener.TransitionType.easeOutQuad));
			Tweener.addTween(houseContainer_mc, Tweener.Hash("time", 0.35f, "alpha", 0.0f, "transition", Tweener.TransitionType.easeOutQuad));
			return Tweener.addTween(sortContainer_mc, Tweener.Hash("time", 0.35f, "alpha", 0.0f, "transition", Tweener.TransitionType.easeInOutQuint) );

		}
		
		public override void OpportunityStart()
		{			
			if(_level == SkillLevel.Tutorial)
			{
					foreach(SortingObject obj in _objects)
					{
						obj.visible = true;
						//obj.enabled = true;
					}
			}
			else
			{
				_houseCount = 0;
				HouseAudio();
			}
			timeoutCount = 0;
			
			dispatchEvent(new SkillEvent(SkillEvent.OPPORTUNITY_START, true, false));
		}
		
		private void HouseAudio()
		{
			house1_mc.HideFilter();
			house2_mc.HideFilter();
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			string clipName = "";
			string clipID = "";
			if(_houseCount == _currentHouses.Length)
			{
				switch(_level)
				{
					case SkillLevel.Emerging:
						clipName = "Narration/5B-Skill Instruction Audio/5B-Initial Instructions TTEG/5B-Drag-"+_currentHouses[0].ToLower() + "-" +_currentHouses[1].ToLower();
						DebugConsole.Log("Audio path: " + clipName);
						clipID = TOUCH_DRAG;
					break;
					case SkillLevel.Developed:
					case SkillLevel.Completed:
						clipName = "Narration/5B-Skill Instruction Audio/5B-Initial Instructions DDCD/5B-Drag-"+_currentHouses[0].ToLower() + "-" +_currentHouses[1].ToLower();
						DebugConsole.Log("Audio path: " + clipName);
						clipID = TOUCH_DRAG;
					break;
					case SkillLevel.Developing:
						clipName = "Narration/5B-Skill Instruction Audio/5B-Initial Instructions DG/5B-Drag-"+_currentHouses[0].ToLower() + "-" +_currentHouses[1].ToLower();
						DebugConsole.Log("Audio path: " + clipName);
						clipID = TOUCH_DRAG;
					break;
				}
					
				AddNewOppData( audioNames.IndexOf( clipName ) );
				
				int i = 0;
				foreach(SortingObject obj in _objects)
				{
					obj.id = i;
					i++;
					
					AddNewObjectData(formatObjectData( obj.getLabel(), obj.parent.localToGlobal( new Vector2( obj.x, obj.y ) ) ));
				}
				AddNewObjectData( formatObjectData( "house_" + house1_mc.type, house1_mc.parent.localToGlobal(new Vector2(house1_mc.x, house1_mc.y)) ) );
				AddNewObjectData( formatObjectData( "house_" + house2_mc.type, house2_mc.parent.localToGlobal(new Vector2(house2_mc.x, house2_mc.y)) ) );
			}
			else
			{
				switch(_houseCount)
				{
				case 0:
					clipID = HOUSE_IS1;
					house1_mc.ShowFilter();
					break;
				case 1:
					clipID = HOUSE_IS2;
					house2_mc.ShowFilter();
					break;
				default:
					Debug.LogError("Too many houses?");
					break;
				}
				clipName = "Narration/5B-Skill Instruction Audio/5B-This House/5B-This-"+_currentHousesPics[_houseCount].ToLower()+"-house";
				DebugConsole.Log("Audio path: " + clipName);
				
				_houseCount++;
			}
			
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SORTING, clipName);	
			bundle.AddClip(clip, clipID, clip.length);
			
			SoundEngine.Instance.PlayBundle( bundle );
		}
		
		private Rectangle getMovieClipRect(MovieClip mc)
		{
			
			return mc.getBounds(mc.parent);
		}

		private bool checkRectIntersect(Rectangle rect1, Rectangle rect2)
		{
			Vector2 c1 = new Vector2(rect1.left+rect1.width*0.5f, rect1.top+rect1.height*0.5f);
			Vector2 c2 = new Vector2(rect2.left+rect2.width*0.5f, rect2.top+rect2.height*0.5f);
			return (Mathf.Abs(c1.x-c2.x) < rect2.width*0.5f) && (Mathf.Abs(c1.y-c2.y) < rect2.height*0.5f);
		}

		public override void Resize( int width, int height )
		{
			this.width = width;
			this.height = height;
			this.x = 0;
			this.y = 0;
			//sortContainer_mc.FitToParent();
			//houseContainer_mc.FitToParent();
			
			sortContainer_mc.scaleY = (((float)height)/((float)MainUI.STAGE_HEIGHT)) * 0.75f;
			sortContainer_mc.scaleX = sortContainer_mc.scaleY;
			sortContainer_mc.y = (height - (MainUI.STAGE_HEIGHT*sortContainer_mc.scaleY))/2f;
			sortContainer_mc.x = (width - (MainUI.STAGE_WIDTH*sortContainer_mc.scaleX))/2f;
			
			houseContainer_mc.scaleY = (((float)height)/((float)MainUI.STAGE_HEIGHT)) * 0.75f;
			houseContainer_mc.scaleX = houseContainer_mc.scaleY;
			houseContainer_mc.y = 35 + ((height - (MainUI.STAGE_HEIGHT*houseContainer_mc.scaleY))/2f);
			houseContainer_mc.x = (width - (MainUI.STAGE_WIDTH*houseContainer_mc.scaleX))/2f;
			
			//houseContainer_mc.y -= houseContainer_mc.height * 0.15f;
			//sortContainer_mc.y += sortContainer_mc.height*0.25f;
		}
		
		public override void onTimerComplete()
		{
			base.onTimerComplete();
			
			stopTimer();
			
			timeoutCount++;
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = null;
			string clipName = "";
			string clipID = "";
			
			switch(timeoutCount)
			{
				case 1:
				//Todo all of the audio needs to be grabbed
					if( _level == SkillLevel.Tutorial)
					{
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SORTING, "Narration/5B-Skill Instruction Audio/5B-now-its-your-turn" );
						clipName = "Narration/5B-Skill Instruction Audio/5B-now-its-your-turn";
						clipID = NOW_ITS;
					}
					else
					{
						if(_answeredObjects.Count == 0)
						{
							clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SORTING, "Narration/5B-Skill Instruction Audio/5B-Keep-your-finger-on");
							clipName = "Narration/5B-Skill Instruction Audio/5B-Keep-your-finger-on";
							clipID = KEEP_PICTURE;
						}
						else
						{
							clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SORTING, "Narration/5B-Skill Instruction Audio/5B-Send-another-one");
							clipName = "Narration/5B-Skill Instruction Audio/5B-Send-another-one";
							clipID = KEEP_PICTURE;
						}
					}
				break;
				case 2:
					if(_level == SkillLevel.Tutorial)
					{
						if(opportunityAnswered)
						{
							opportunityComplete = true;
							opportunityCorrect = false;
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
						}
						else
						{
							timeoutCount = 0;
							opportunityAnswered = true;
							opportunityCorrect = true;
							clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SORTING, "Narration/5B-Skill Instruction Audio/5B-Try-again");
							clipName = "Narration/5B-Skill Instruction Audio/5B-Try-again";
							clipID = LETS_TRY_AGAIN;
						}
					}
					else
					{
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SORTING, "Narration/5B-Skill Instruction Audio/5B-Touch-green-button");
						clipName = "Narration/5B-Skill Instruction Audio/5B-Touch-green-button";
						clipID = TOUCH_HELP;
					}
				break;
				case 3:
					opportunityAnswered = true;
					opportunityCorrect = false;
				
					_houseCount = 0;
					helpPrompted = 0;
					getNextHelpPrompt();
					clipName = "Narration/5B-Skill Instruction Audio/5B-Send-another-one";
				break;
				case 4:
					timeoutEnd = true;
					objectsOff();
					
					opportunityComplete = true;
					
					foreach(SortingObject obj in _objects)
					{
						//obj.enabled = false;
					}
					_houseCount = 0;
					helpPrompted = 0;
					doItForThem();
				break;
				default:
				break;
			}
			
			if(clip != null)
			{
				bundle.AddClip( clip, clipID, clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
			}
			if( !string.IsNullOrEmpty(clipName) )
			{
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf( clipName ) );
			}
		}
		
		private void getNextHelpPrompt()
		{
			house1_mc.HideFilter();
			house2_mc.HideFilter();
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = null;
			string clipID = "";
			
			string type = _objects[0].type;
			
			switch (helpPrompted)
			{
			case 1:
				int i = 1;
				while ( type == _objects[0].type && i < _objects.Count) {
					type = _objects[i].type;
					i++;
				}
				if (type == _objects[0].type)
				{
					helpPrompted++;
					goto case 2;
				}
				else
					goto case 0;
			case 0:
				if(type == house1_mc.type)
				{
					house1_mc.ShowFilter();
				}
				else
				{
					house2_mc.ShowFilter();
				}
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SORTING, "Narration/5B-Inactivity Reinforcement Audio/5B-Belong-" + type.ToLower());
				DebugConsole.Log("Audio path: " + "Narration/5B-Inactivity Reinforcement Audio/5B-Belong-" + type.ToLower());
				clipID = HELP_PROMPT;
				break;
			case 2:
				type = "";
				if(_answeredObjects.Count > 0 && !opportunityComplete)
				{
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SORTING, "Narration/5B-Skill Instruction Audio/5B-Send-another-one");
					clipID = HELP_PROMPT;
				}
				else
					goto default;
				break;
			default:
				type = "";
				startTimer();
				helpPrompted = -1;
				break;
			}
			for(int i = 0; i < _objects.Count; i++)
			{
				if(_objects[i].type == type)
					_objects[i].ShowFilter();
				else
					_objects[i].HideFilter();
			}
			helpPrompted++;
			if(clip != null)
			{
				bundle.AddClip( clip, clipID, clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
			}
		}
		
		private void doItForThem()
		{
			string type = _objects[0].type;
			List<SortingObject> movingObjects = new List<SortingObject>();
			
			foreach(SortingObject so in _objects)
			{
				if(so.type == type)
					movingObjects.Add(so);
			}
			
			switch (helpPrompted)
			{
			case 1:
				foreach(SortingObject so in movingObjects)
				{
					Tweener.addTween(so, Tweener.Hash("time", 0.35f, "scaleX", 0, "scaleY", 0, "alpha", 0));
				}
				
				int j = 1;
				while ( type == _objects[0].type && j < _objects.Count) {
					type = _objects[j].type;
					j++;
				}
				
				if (type == _objects[0].type)
					goto default;
				else
				{
					movingObjects = new List<SortingObject>();
					foreach(SortingObject so in _objects)
					{
						if(so.type == type)
							movingObjects.Add(so);
					}
					
					goto case 0;
				}
			case 0:
				float[] center = new float[2];
				Rectangle hbound;
				if(type == house1_mc.type)
				{
					hbound = house1_mc.getBounds(movingObjects[0].parent);
				}
				else
				{
					hbound = house2_mc.getBounds(movingObjects[0].parent);
				}
				
				center[0] = hbound.x + (hbound.width/2f);
				center[1] = hbound.y + (hbound.height/2f);
				
				Rectangle obound;
				
				float destX;
				float destY;
				
				if(movingObjects.Count == 1)
				{
					obound = movingObjects[0].getBounds(movingObjects[0].parent);
					destX = center[0] - (obound.width/2f) - (obound.x - movingObjects[0].x);
					destY = center[1] - (obound.height/2f) - (obound.y - movingObjects[0].y);
					Tweener.addTween(movingObjects[0], Tweener.Hash("time", 0.35f, "x", destX, "y", destY));
				}
				else
				{
					center[0] = hbound.x + (hbound.width/4f);
					center[1] = hbound.y + (hbound.height/3f);
					float angStep = (2*Mathf.PI)/movingObjects.Count;
					for(int i = 0; i < movingObjects.Count; i++)
					{
						SortingObject so = movingObjects[i];
						float circX = (Mathf.Cos( angStep*i ) * 270 * .8f) + center[0];
						float circY = (Mathf.Sin( angStep*i ) * 270 * .8f) + center[1];
						
						obound = so.getBounds(movingObjects[0].parent);
						destX = circX - (obound.width/2f) - (obound.x - so.x);
						destY = circY - (obound.height/2f) - (obound.y - so.y);
						
						Tweener.addTween(so, Tweener.Hash("time", 0.35f, "x", circX, "y", circY));
					}
				}
				break;
			default:
				type = "";
				break;
			}
			if(type != "")
			{
				getNextHelpPrompt();
			}
			else
			{
				fadeOutContainers();
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
			}
		}
		
		private void onTutorialComplete( CEvent e )
		{
			tutorial_mc.View.removeEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.View.removeEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			
			removeChild( tutorial_mc.View );
			
			nextOpportunity();
		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			List<string[]> houses = new List<string[]>();
			List<string[]> pics = new List<string[]>();
			switch(_level)
			{
			case SkillLevel.Emerging:
				houses = _egHouses;
				pics = null;
				break;
			case SkillLevel.Developing:
				houses = _dgHouses;
				pics = _dgPics;
				break;
			case SkillLevel.Developed:
			case SkillLevel.Completed:
				houses = _ddHouses;
				pics = _ddPics;
				break;
			}
			
			string data = _currentHouses[0] + "," + _currentHouses[1];
			
			foreach(string[] sa in houses)
			{
				data = data + "-" + sa[0] + "," + sa[1];
			}
			if(pics != null)
			{
				data = data + "&" + _currentHousesPics[0] + "," + _currentHousesPics[1];
				foreach(string[] sa in pics)
				{
					data = data + "-" + sa[0] + "," + sa[1];
				}
			}
			return data;
		}
		
		private void parseSessionData (string data)
		{
			List<string[]> houses = new List<string[]>();
			List<string[]> pics = new List<string[]>();
			string hData;
			string pData;
			string[] sData;
			if(data.IndexOf('&') >= 0)
			{
				sData = data.Split('&');
				hData = sData[0];
				pData = sData[1];
			}
			else
			{
				hData = data;
				pData = "";
			}
			
			sData = hData.Split('-');
			foreach(string s in sData)
			{
				houses.Add( s.Split(',') );
			}
			if(pData != "")
			{
				sData = pData.Split('-');
				foreach(string s in sData)
				{
					pics.Add( s.Split(',') );
				}
			}
			
			switch(_level)
			{
			case SkillLevel.Emerging:
				_egHouses = houses.GetRange(0, houses.Count);
				break;
			case SkillLevel.Developing:
				_dgHouses = houses.GetRange(0, houses.Count);
				_dgPics = pics.GetRange(0, houses.Count);
				break;
			case SkillLevel.Developed:
			case SkillLevel.Completed:
				_ddHouses = houses.GetRange(0, houses.Count);
				_ddPics = pics.GetRange(0, houses.Count);
				break;
			}
		}
		
		private static List<string> audioNames = new List<string>
		{	"",
			"Narration/5B-StoryIntroduction",
			"Narration/5B-Skill Instruction Audio/5B-TT-Watch-Platty",
			"Narration/5B-Skill Instruction Audio/5B-TT-Now-its-your-turn",
			"Narration/5B-Skill Instruction Audio/5B-TT-Lets-start-sorting",
			"Narration/5B-Skill Instruction Audio/5B-Try-again",
			"Narration/5B-Skill Instruction Audio/5B-Send-another-one",
			"Narration/5B-Skill Instruction Audio/5B-Keep-your-finger-on",
			"Narration/5B-Skill Instruction Audio/5B-Touch-green-button",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-ball-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-bear-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-boot-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-car-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-circle-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-comb-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-dinosaur-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-egg-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-fork-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-fruit-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-glove-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-nest-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-oval-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-pants-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-plane-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-rectangle-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-rhombus-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-shirt-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-shoe-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-spoon-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-square-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-toothbrush-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-train-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-triangle-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-truck-house",
			"Narration/5B-Skill Instruction Audio/5B-This House/5B-This-grain-house",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DDCD/5B-Drag-balls-shoes",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DDCD/5B-Drag-bears-dinosaurs",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DDCD/5B-Drag-boots-gloves",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DDCD/5B-Drag-eggs-nests",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DDCD/5B-Drag-forks-spoons",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DDCD/5B-Drag-fruits-grains",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DDCD/5B-Drag-planes-trains",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DDCD/5B-Drag-shirts-pants",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DDCD/5B-Drag-toothbrushes-combs",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DDCD/5B-Drag-trucks-cars",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DG/5B-Drag-bears-triangle",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DG/5B-Drag-circle-rectangle",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DG/5B-Drag-circle-rhombus",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DG/5B-Drag-oval-square",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DG/5B-Drag-rectangle-oval",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DG/5B-Drag-rhombus-bears",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DG/5B-Drag-rhombus-oval",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DG/5B-Drag-square-bears",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DG/5B-Drag-triangle-rectangle",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions DG/5B-Drag-triangle-square",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions TTEG/5B-Drag-circle-rectangle",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions TTEG/5B-Drag-circle-rhombus",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions TTEG/5B-Drag-oval-square",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions TTEG/5B-Drag-oval-triangle",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions TTEG/5B-Drag-rectangle-oval",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions TTEG/5B-Drag-rhombus-oval",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions TTEG/5B-Drag-rhombus-triangle",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions TTEG/5B-Drag-square-circle",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions TTEG/5B-Drag-triangle-circle",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions TTEG/5B-Drag-triangle-rectangle",
			"Narration/5B-Skill Instruction Audio/5B-Initial Instructions TTEG/5B-Drag-triangle-square",
			"Narration/5B-Reinforcement Audio/5B-The-gnomes-thank-you",
			"Narration/5B-Reinforcement Audio/5B-I-will-tell-the-gnomes",
			"Narration/5B-Reinforcement Audio/5B-Helped-DDCD/5B-Helped-balls-shoes",
			"Narration/5B-Reinforcement Audio/5B-Helped-DDCD/5B-Helped-bears-dinosaurs",
			"Narration/5B-Reinforcement Audio/5B-Helped-DDCD/5B-Helped-boots-gloves",
			"Narration/5B-Reinforcement Audio/5B-Helped-DDCD/5B-Helped-eggs-nests",
			"Narration/5B-Reinforcement Audio/5B-Helped-DDCD/5B-Helped-forks-spoons",
			"Narration/5B-Reinforcement Audio/5B-Helped-DDCD/5B-Helped-fruits-grains",
			"Narration/5B-Reinforcement Audio/5B-Helped-DDCD/5B-Helped-planes-trains",
			"Narration/5B-Reinforcement Audio/5B-Helped-DDCD/5B-Helped-shirts-pants",
			"Narration/5B-Reinforcement Audio/5B-Helped-DDCD/5B-Helped-toothbrushes-combs",
			"Narration/5B-Reinforcement Audio/5B-Helped-DDCD/5B-Helped-trucks-cars",
			"Narration/5B-Reinforcement Audio/5B-Helped-DG/5B-Helped-bears-triangles",
			"Narration/5B-Reinforcement Audio/5B-Helped-DG/5B-Helped-circles-rectangles",
			"Narration/5B-Reinforcement Audio/5B-Helped-DG/5B-Helped-circles-rhombuses",
			"Narration/5B-Reinforcement Audio/5B-Helped-DG/5B-Helped-ovals-squares",
			"Narration/5B-Reinforcement Audio/5B-Helped-DG/5B-Helped-rectangles-ovals",
			"Narration/5B-Reinforcement Audio/5B-Helped-DG/5B-Helped-rhombuses-bears",
			"Narration/5B-Reinforcement Audio/5B-Helped-DG/5B-Helped-rhombuses-ovals",
			"Narration/5B-Reinforcement Audio/5B-Helped-DG/5B-Helped-squares-bears",
			"Narration/5B-Reinforcement Audio/5B-Helped-DG/5B-Helped-triangles-rectangles",
			"Narration/5B-Reinforcement Audio/5B-Helped-DG/5B-Helped-triangles-squares",
			"Narration/5B-Reinforcement Audio/5B-Helped-TTEG/5B-Helped-circle-rectangle",
			"Narration/5B-Reinforcement Audio/5B-Helped-TTEG/5B-Helped-circle-rhombus",
			"Narration/5B-Reinforcement Audio/5B-Helped-TTEG/5B-Helped-oval-square",
			"Narration/5B-Reinforcement Audio/5B-Helped-TTEG/5B-Helped-oval-triangle",
			"Narration/5B-Reinforcement Audio/5B-Helped-TTEG/5B-Helped-rectangle-oval",
			"Narration/5B-Reinforcement Audio/5B-Helped-TTEG/5B-Helped-rhombus-oval",
			"Narration/5B-Reinforcement Audio/5B-Helped-TTEG/5B-Helped-rhombus-triangle",
			"Narration/5B-Reinforcement Audio/5B-Helped-TTEG/5B-Helped-square-circle",
			"Narration/5B-Reinforcement Audio/5B-Helped-TTEG/5B-Helped-triangle-circle",
			"Narration/5B-Reinforcement Audio/5B-Helped-TTEG/5B-Helped-triangle-rectangle",
			"Narration/5B-Reinforcement Audio/5B-Helped-TTEG/5B-Helped-triangle-square",
			"Narration/5B-Reinforcement Audio/5B-Helped-TTEG/5B-Helped-triangle",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-balls",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-bear",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-bears",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-boots",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-cars",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-circle",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-circles",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-combs",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-dinosaurs",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-eggs",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-forks",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-fruits",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-gloves",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-nests",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-oval",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-ovals",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-pants",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-planes",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-rectangle",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-rectangles",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-rhombus",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-rhombuses",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-shirts",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-shoes",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-spoons",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-square",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-squares",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-toothebrushes",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-trains",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-triangle",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-triangles",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-trucks",
			"Narration/5B-Inactivity Reinforcement Audio/5B-Belong-grains",
			"User-Earns-Artifact"
		};
	}
}
