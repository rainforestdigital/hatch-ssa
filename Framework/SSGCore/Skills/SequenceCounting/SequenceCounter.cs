using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;

namespace SSGCore
{
	public class SequenceCounter : FilterMovieClip
	{
		private int num = 0;
		public int Num
		{
			get{ return num; }
			set
			{
				num = value;
				target.getChildByName<MovieClip>("num").gotoAndStop(num);
			}
		}
		public bool inTarget;
		
		public SequenceCounter (string type, int number, int varient) : base(MovieClipFactory.CreateSequenceCountingCounter())
		{
			inTarget = false;
			target.gotoAndStop(type + varient);
			Num = number;
		}
		
		public void flipNum ()
		{
			MovieClip temp = target.getChildByName<MovieClip>("num");
			temp.scaleX *= -1;
			temp.x += temp.width;
		}
	}
}

