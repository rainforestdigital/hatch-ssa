using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;

namespace SSGCore
{
	public class SequenceTarget : MovieClip
	{
		private List<MovieClip> dummies;
		
		private string _type;
		public string type
		{
			get{ return _type; }
			set
			{
				if(dummies != null)
				{
					foreach(MovieClip mc in dummies)
					{
						mc.removeAllChildren();
					}
				}
				if(_type != value)
				{
					gotoAndStop(value);
					dummies = new List<MovieClip>();
					
					int i = 0;
					MovieClip temp = getChildByName<MovieClip>("place0");
					
					do{
						temp.stop();
						dummies.Add(temp);
						i++;
						temp = getChildByName<MovieClip>("place" + i);
					}while(temp != null);
					DebugConsole.Log("SequenceTarget || set type - first null place found at " + i);
				}
				numIn = 0;
				_type = value;
			}
		}
		
		private int numIn;
		public int numMax
		{
			get{ return dummies.Count; }
		}
		
		public SequenceTarget () : base("Targets.swf", "target")
		{
			stop();
		}
		
		public int fillDummy (SequenceCounter itemIn)
		{
			if(numIn == numMax)
				return -1;
			if(itemIn.parent != null)
				itemIn.parent.removeChild(itemIn);
			
			//itemIn.scaleX = itemIn.scaleY = 1f;
			
			float cWidth,cHeight;
			switch(type)
			{
			case "TURBINE":
				cWidth = cHeight = 390f;
				break;
			case "GEAR":
				cWidth = cHeight = 220f;
				break;
			case "BATTERY":
			default:
				cWidth = 180f;
				cHeight = 115f;
				break;
			}
			
			itemIn.x = 0 - (cWidth/2f);
			itemIn.y = 0 - (cHeight/2f);
			itemIn.inTarget = true;
			dummies[numIn].addChild(itemIn);
			if(type == "CAR" && numIn >= 5)
			{
				itemIn.scaleX *= -1f;
				itemIn.x += itemIn.width;
				itemIn.flipNum();
			}
			numIn++;
			return numIn;
		}
	}
}

