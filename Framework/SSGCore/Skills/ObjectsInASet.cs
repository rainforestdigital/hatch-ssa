using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.geom;


namespace SSGCore
{

	public class ObjectsInASet : BaseSkill
	{
	
		private const string YELLOW_ALIENS = "yellow";
		private const string GREEN_ALIENS = "green";
		private const string PINK_ALIENS = "pink";

		private const string MORE_ALIENS = "2D-this-spacecraft-has-more-travelers";
		private const string LESS_ALIENS = "2D-this-spacecraft-has-less-travelers";

		private const string CORRECTORANGEALIENS = "2D-this-spacecraft-has-";

		private const string CORRECTPINKALIENS = "2D-this-numeral-";

		private const string GREATERTHAN = "greaterthan";
		private const string LESSTHAN = "lessthan";

		private const string REINFORCEMENT_PATH = "Narration/2D-Reinforcement Audio/";
		private const string INSTRUCTION_PATH = "Narration/2D-Skill Instruction Audio/";
		private const string INITIAL_INSTRUCTION = "2D-Initial Instructions/";

		private DisplayObjectContainer basketContainer;

		private List<ObjectsInASetObject> baskets;
		private new ObjectsInASetTutorial tutorial_mc;
		private ObjectsInASetObject incorrectBasket;
		private ObjectsInASetObject correctBasket;
		private ObjectsInASetObject clickedBasket;
		private FilterMovieClip numberCard;
		private FilterMovieClip wrongNumberCard;

		private int answerNumber;
		private int otherNumber;

		private string basketType;
		private string conditionType;
		
		private List<ObjectsInASetOpp> setList;

		private List<int> availableNumbers;
		
		private TimerObject glowTimer;
		private float scaleMod;

		private Platforms selectedPlatform;
		private float basketOffset_Y = 0f;
		private float numberOffset_Y = 0f;
		private float platformScaleModifier = 0f;

		public ObjectsInASet( SkillLevel level, SessionInfo  currentSession) : base(level, currentSession)
		{
			BACKGROUND = "background";
			ARTIFACT = "artifact";
				
			PreCacheAssets();
		}
		
		private void PreCacheAssets()
		{
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle("ObjectsInASet", OnAssetsLoaded));
		}
		
		void OnAssetsLoaded(AssetBundle bundle)
		{
			if( bundle != null )
			{
				Init( null );
			}
		}

		public int numBaskets
		{
			set
			{
				int i = 0;

				clearBaskets();

				answerNumber = setList[currentOpportunity].correctNum;

				availableNumbers = new List<int> (){ 1, 2, 3, 4, 5 };
				if(conditionType == GREATERTHAN)
					while( availableNumbers[availableNumbers.Count - 1] >= answerNumber)
						availableNumbers.RemoveAt(availableNumbers.Count - 1);
				else
					while( availableNumbers[0] <= answerNumber )
						availableNumbers.RemoveAt(0);
					
				switch(_level)
				{
				case SkillLevel.Tutorial:
				case SkillLevel.Emerging:
						basketType = YELLOW_ALIENS;
						break;

				case SkillLevel.Developing:
						basketType = GREEN_ALIENS;
						break;

				case SkillLevel.Developed:
				case SkillLevel.Completed:
						basketType = PINK_ALIENS;
						break;
				default:
					basketType = YELLOW_ALIENS;
					break;
				}
				
				
				MovieClip newBasket = MovieClipFactory.CreateObjectsInASetObject(basketType + "_" + answerNumber);
				ObjectsInASetObject newFilterBasket = new ObjectsInASetObject (newBasket, basketType, answerNumber);
				basketContainer.addChild (newFilterBasket);
				baskets.Add (newFilterBasket);
				
				for( i = 1; i < value; i++)
				{
					int basketNumber;
					if (_level == SkillLevel.Tutorial) {
						basketNumber = 0;
					}
				 	else 
					{
						basketNumber = Mathf.FloorToInt(Random.Range(0, availableNumbers.Count));
						if(basketNumber == availableNumbers.Count) basketNumber--;
						availableNumbers.Shuffle();
					}
					newBasket = MovieClipFactory.CreateObjectsInASetObject(basketType + "_" + availableNumbers[basketNumber]);
					newFilterBasket = new ObjectsInASetObject (newBasket, basketType, availableNumbers[basketNumber]);
					basketContainer.addChild (newFilterBasket);
					baskets.Add (newFilterBasket);
					
					availableNumbers.RemoveAt (basketNumber);
				}

				if (_level == SkillLevel.Tutorial)
					baskets [1].visible = false;
				else
					baskets.Shuffle();
				
				if(_level == SkillLevel.Developed || _level == SkillLevel.Completed)
				{
					numberCard = new FilterMovieClip( MovieClipFactory.CreateObjectsInASetCard() );
					numberCard.Target.gotoAndStop( answerNumber );
					numberCard.scaleX = numberCard.scaleY = 0.8f;
					basketContainer.addChild(numberCard);
				}

				positionBaskets ();

			}
			get
			{
				return baskets.Count;
			}
		}

		private void positionBaskets()
		{
			if(baskets[0].Target.width < 600 )
			{
				//Used because their docs say to use a specific size, but they also said that specific size is too big on device
				scaleMod = ((float)Screen.height / (float)MainUI.STAGE_HEIGHT);
			}
			else
			{
				scaleMod = 1;
			}

			Debug.Log ("Scale Mode: " + scaleMod);

			scaleMod += platformScaleModifier;

			//scaleMod = 0.5f;

			float padding = 60 * scaleMod;
			if(_level == SkillLevel.Developed || _level == SkillLevel.Completed)
				padding = numberCard.getBounds(basketContainer).width * 0.5f;

			float totalWidth = ((baskets[0].Target.width + padding) * baskets.Count) - padding;

			for(int i = 0; i < baskets.Count; i++)
			{
				baskets[i].x = ((Screen.width * 0.5f) - (totalWidth * 0.5f)) + (i * (baskets[0].Target.width + padding));
				baskets[i].y = (Screen.height * 0.55f) - (baskets[i].Target.height * 0.5f) + basketOffset_Y;
				baskets[i].ScaleCentered( scaleMod );
			}

			if(numberCard != null)
			{
				numberCard.x = (Screen.width * 0.5f) - (numberCard.width * 0.5f);
				numberCard.y = baskets[0].getBounds(basketContainer).bottom + numberOffset_Y;// + (baskets[0].height * 0.1f); // - (numberCard.height * 0.6f);

			}

			Resize( Screen.width, Screen.height );

		}

		public override void Init (MovieClip parent)
		{
			base.Init (parent);

			baskets = new List<ObjectsInASetObject> ();

			basketContainer = new DisplayObjectContainer ();
			addChild (basketContainer);

			selectedPlatform = PlatformUtils.GetPlatform ();
			//selectedPlatform = Platforms.IOS;

			initPlatformValues ();

			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			switch(_level)
			{
			
				case SkillLevel.Tutorial:

					totalOpportunities = 1;
					opportunityComplete = false;
					basketType = YELLOW_ALIENS;
					if(resumingSkill) 
					{
						nextOpportunity();
					} 
					else 
					{	
						tutorial_mc = new ObjectsInASetTutorial();
						addChild( tutorial_mc.View );
						tutorial_mc.skillRef = this;
						tutorial_mc.View.visible = false;
						tutorial_mc.View.addEventListener( TutorialEvent.START, onTutorialStart );
						tutorial_mc.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
						tutorial_mc.Resize( Screen.width, Screen.height );
					}
					Resize( Screen.width, Screen.height );
				return;
				
				case SkillLevel.Emerging:
					totalOpportunities = 10;
					opportunityComplete = false;
					basketType = YELLOW_ALIENS;
				break;
				
				case SkillLevel.Developing:
					totalOpportunities = 10;
					opportunityComplete = false;
					basketType = GREEN_ALIENS;
				break;
				
				case SkillLevel.Developed:
				case SkillLevel.Completed:
					totalOpportunities = 10;
					opportunityComplete = false;
					basketType = PINK_ALIENS;
				break;
				
			}
			
			checkForResume();
			
			Resize( Screen.width, Screen.height );
			
		}

		private void initPlatformValues() {

			switch (selectedPlatform) {
			case Platforms.ANDROID:
				basketOffset_Y = -200f;
				platformScaleModifier = 0.4f;
				break;

			case Platforms.IOS:
				basketOffset_Y = -110f;
				platformScaleModifier = 0.3f;
				break;

			case Platforms.IOSRETINA:
				basketOffset_Y = -220f;
				numberOffset_Y = -20f;
				break;

			case Platforms.WIN:
				basketOffset_Y = -150f + (150 * (( 1080f-Screen.height) / 360f));
				break;
			}
		}

		private void checkForResume()
		{
			if( resumingSkill )
			{
				parseSessionData(currentSession.sessionData);
			//	nextOpportunity();
			}
			else
			{
				//Sets up the opportunity list
				setList = new List<ObjectsInASetOpp>(){ new ObjectsInASetOpp(1, LESSTHAN),
														new ObjectsInASetOpp(1, LESSTHAN),
														new ObjectsInASetOpp(5, GREATERTHAN),
														new ObjectsInASetOpp(5, GREATERTHAN)};
				
				List<int> availableNums = new List<int>(){ 2,2,3,3,4,4 };
				int r;
				
				if(_level == SkillLevel.Developing)
				{
					while(availableNums.Count > 1)
					{
						r = Mathf.FloorToInt(Random.Range(0, availableNums.Count));
						if(r == availableNums.Count) r--;
						setList.Add( new ObjectsInASetOpp(availableNums[r], GREATERTHAN) );
						availableNums.RemoveAt(r);
						
						r = Mathf.FloorToInt(Random.Range(0, availableNums.Count));
						if(r == availableNums.Count) r--;
						setList.Add( new ObjectsInASetOpp(availableNums[r], LESSTHAN) );
						availableNums.RemoveAt(r);
					}
				}
				else
				{
					while(availableNums.Count > 0)
					{
						r = Mathf.FloorToInt(Random.Range(0, availableNums.Count));
						if(r == availableNums.Count) r--;
						setList.Add( new ObjectsInASetOpp(availableNums[r], Random.value > .5f ? LESSTHAN : GREATERTHAN ) );
						availableNums.RemoveAt(r);
					}
				}
				
				setList.Shuffle();
				
				int lastNum = setList[0].correctNum;
				for( int i = 1; i < setList.Count; i++)
				{
					ObjectsInASetOpp thisSet = setList[i];
					if(lastNum == thisSet.correctNum)
					{
						setList.Remove(thisSet);
						if(i == 1)
						{
							r = Mathf.FloorToInt(Random.Range(2, setList.Count));
							if(r == setList.Count) r--;
							setList.Insert(r, thisSet);
							lastNum = setList[1].correctNum;
						}
						else
							setList.Insert(0, thisSet);
					}
					else
					{
						lastNum = thisSet.correctNum;
					}
				}
			}
		}

		private void onTutorialComplete( CEvent e )
		{
			tutorial_mc.View.removeEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.View.removeEventListener( TutorialEvent.COMPLETE, onTutorialComplete );

			removeChild( tutorial_mc.View );
			currentOpportunity++;
			opportunityAnswered = true;
			opportunityCorrect = tutorial_mc.correct;
			opportunityComplete = true;

			if(!opportunityCorrect)
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
			else
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );

		}
		
		/// <summary>
		/// Releases all resource used by the <see cref="SSGCore.ObjectsInASet"/> object.
		/// </summary>
		/// <remarks>
		/// Call <see cref="Dispose"/> when you are finished using the <see cref="SSGCore.ObjectsInASet"/>. The
		/// <see cref="Dispose"/> method leaves the <see cref="SSGCore.ObjectsInASet"/> in an unusable state. After
		/// calling <see cref="Dispose"/>, you must release all references to the <see cref="SSGCore.ObjectsInASet"/> so
		/// the garbage collector can reclaim the memory that the <see cref="SSGCore.ObjectsInASet"/> was occupying.
		/// </remarks>
		public override void Dispose ()
		{
			base.Dispose();

			clearBaskets ();

			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			if(tutorial_mc != null)
				tutorial_mc.Destroy();
			if(glowTimer != null)
				glowTimer.Unload();
		}
		
		public override void OnAudioWordComplete( string word )
		{
			if(incorrectBasket != null && !(word == CLICK_INCORRECT || word == THEME_CRITICISM))
			{
				incorrectBasket.Destroy();
				if(_level == SkillLevel.Completed || _level == SkillLevel.Developed)
				{
					Tweener.addTween( incorrectBasket, Tweener.Hash("time", 0.35f, "alpha",0) ).OnComplete(RemoveIncorrectBasketAfterTween);
					if(wrongNumberCard != null)
					{
						wrongNumberCard.Destroy();
						Tweener.addTween( wrongNumberCard, Tweener.Hash("time", 0.35f, "alpha",0) );
					}
				}
				else if( word != CLICK_CORRECT && word != THEME_COMPLIMENT && word != "CONFIRMCORRECT" )
				{
					Tweener.addTween( incorrectBasket, Tweener.Hash("time", 0.35f, "alpha", 0.5f) );
				}
				if( word != CLICK_CORRECT && word != THEME_COMPLIMENT )
				{
					correctBasket.ScaleCentered( scaleMod );
					incorrectBasket.ScaleCentered( scaleMod );
				}
			}


			if(opportunityComplete)
			{
				switch( word )
				{
				case YOU_FOUND:
					Tweener.addTween( basketContainer, Tweener.Hash("time",0.35f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuint) );
					if(opportunityCorrect)
					{
						dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );

					}
					else
					{
						dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
					}
					break;

				case THEME_COMPLIMENT:
					if(_level == SkillLevel.Tutorial)
					{
//							playInstructionAudio("3A-Skill Instruction Audio/3A-now-lets-find-henrys-cards", NOW_LETS);
					}
					else
					{
						playFoundCurrentAudio();
					}
					break;

					case "CONFIRMCORRECT":
						playConfirmCurrentAudio();
					break;

					case INTRO:
					case PLEASE_FIND:
					case NOW_ITS:
					case NOW_LETS:
					case YOU_FOUND_INCORRECT:
					case THIS_IS :
					if(opportunityCorrect)
					{
						dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
					}
					else
					{
						dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
					}
					break;
					case THEME_INTRO:
						playIntroAudio();
						break;
					case CLICK_CORRECT:
					case CLICK_INCORRECT:
						requestClickThemeFeedback();
					break;
					}
			}
			else
			{
				switch( word )
				{

				case THEME_CRITICISM:
					playCorrectionAudio();
					break;

				case YOU_FOUND:
				case YOU_FOUND_INCORRECT:
					playFindCurrentAudio();
					break;

				case NOW_LETS:
					resetTutorial();
					break;

				case PLEASE_FIND:
				case NOW_ITS:					
				case THIS_IS :
					startTimer();
					enableBasketClicks();
					if(_level != SkillLevel.Tutorial)
						MarkInstructionEnd();
					break;

				case "RESTART_TUTORIAL":
					tutorial_mc = new ObjectsInASetTutorial();
					break;

				case CLICK_CORRECT:
				case CLICK_INCORRECT:
					requestClickThemeFeedback();
					break;

				case INTRO:
					basketContainer.visible = true;
					if(_level == SkillLevel.Tutorial)
					{
						tutorial_mc.View.visible = true;
						tutorial_mc.OnStart();
					}
					else
					{
						nextOpportunity();
					}
					break;
					
				case THEME_INTRO:
					playIntroAudio();
					break;

				case "CONFIRMCORRECT":
					playConfirmCurrentAudio();
					break;

				}
			}
		}

		private void requestClickThemeFeedback() //The correct/incorrect sound has been played and now we need theme feedback
		{
			if(clickedBasket == correctBasket) dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
			else dispatchEvent( new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
		}

		private void clearBaskets ()
		{
			for(int i = 0; i < baskets.Count; i++)
			{
				baskets [i].removeEventListener (MouseEvent.CLICK, onBasketClick);
				basketContainer.removeChild (baskets [i]);
				baskets [i].Destroy ();
			}

			if(numberCard != null)
			{
				basketContainer.removeChild (numberCard);
				numberCard.Destroy();
				numberCard = null;
			}
			
			if(wrongNumberCard != null)
			{
				basketContainer.removeChild (wrongNumberCard);
				wrongNumberCard.Destroy();
				wrongNumberCard = null;
			}
			baskets.Clear ();
		}

		void onBasketClick (CEvent e)
		{
			ObjectsInASetObject clickedObject = e.currentTarget as ObjectsInASetObject;

			clickedBasket = clickedObject;
			Debug.Log("Basket Children: " + clickedBasket.numChildren);
			clickedBasket.ScaleCentered(clickedBasket.scaleX * 1.2f);

			ObjectsInASetObject otherBasket = null;

			basketContainer.removeChild( clickedObject );
			basketContainer.addChild( clickedObject );

			stopTimer();
			timeoutCount = 0;

			clickedObject.Destroy();

			clickedObject.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );

			clickedObject.filters[0].Visible = true;

			foreach(ObjectsInASetObject bObj in baskets)
			{
				if(bObj != clickedObject) otherBasket = bObj;
			}

			otherNumber = otherBasket.Amount;

			disableBasketClicks();
			
			if(clickedObject.Amount == answerNumber)
			{
				onCorrectAnswer(clickedObject, otherBasket);
				
				AddNewActionData( formatActionData( "tap", baskets.IndexOf( clickedObject ), Vector2.zero, true ) );
			}
			else
			{
				onIncorrectAnswer(otherBasket, clickedObject);
				
				AddNewActionData( formatActionData( "tap", baskets.IndexOf( clickedObject ), Vector2.zero, false ) );
			}

			PlayClickSound();

		}

		private void onCorrectAnswer(ObjectsInASetObject correct, ObjectsInASetObject incorrect)
		{

			MarkInstructionStart();

			PlayCorrectSound();

			if(opportunityAnswered == false)
			{
				opportunityAnswered = true;
				opportunityCorrect = true;
			}
			opportunityComplete = true;

			if(numberCard != null)
			{
				basketContainer.removeChild( numberCard );
				basketContainer.addChild( numberCard );

				numberCard.Destroy();
				
				Rectangle bound = correct.getBounds(basketContainer);
				float targX = bound.x + ((bound.width - numberCard.getBounds(basketContainer).width) / 2);

				Tweener.addTween( numberCard, Tweener.Hash( "x", targX, "time",0.35f, "transition",Tweener.TransitionType.easeOutQuad ) );
			}

			correctBasket = correct;
			incorrectBasket = incorrect;

			//dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );

		}

		private void onIncorrectAnswer(ObjectsInASetObject correct, ObjectsInASetObject incorrect)
		{

			MarkInstructionStart();

			PlayIncorrectSound();

			opportunityAnswered = true;
			opportunityCorrect = false;
			
			if(numberCard != null)
			{
				wrongNumberCard = new FilterMovieClip( MovieClipFactory.CreateObjectsInASetCard() );
				wrongNumberCard.Target.gotoAndStop( incorrect.Amount );
				
				Rectangle bound = incorrect.getBounds(basketContainer);
				float targX = bound.x + ((bound.width - numberCard.getBounds(basketContainer).width) / 2);
				
				wrongNumberCard.x = targX;
				wrongNumberCard.y = numberCard.y;
				
				basketContainer.addChild(wrongNumberCard);
			}

			correctBasket = correct;
			incorrectBasket = incorrect;

			//dispatchEvent( new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );

		}

		private void enableBasketClicks()
		{
			for(int i = 0; i < baskets.Count; i++)
			{
				if( baskets[i] != incorrectBasket )
					baskets [i].addEventListener (MouseEvent.CLICK, onBasketClick);
			}
		}

		private void disableBasketClicks()
		{
			for(int i = 0; i < baskets.Count; i++)
			{
				baskets [i].removeEventListener (MouseEvent.CLICK, onBasketClick);
			}
		}

		protected override void resetTutorial ()
		{
			tutorial_mc.Destroy();
		}

		public override void nextOpportunity()
		{

			if(currentOpportunity == totalOpportunities)
			{	
				tallyAnswers();
				return;
			}

			if(_level == SkillLevel.Tutorial)
			{
				if(currentOpportunity > 0)
				{
					tallyAnswers();
				}
				else
				{
					setList = new List<ObjectsInASetOpp>(){new ObjectsInASetOpp(1, LESSTHAN)};
					conditionType = LESSTHAN;
					numBaskets = 2;
					currentOpportunity++;
					fadeInBasketContainer().OnComplete( OpportunityStart );
				}
			}
			else
			{
				setNextOpportunity (2);
			}

		}

		private void setNextOpportunity(int _basketCount)
		{

			if(currentOpportunity < totalOpportunities)
			{
				opportunityComplete = false;
				opportunityCorrect = false;
				opportunityAnswered = false;

				if(resumingSkill)
				{
					resumingSkill = false;
				}
				conditionType = setList[currentOpportunity].conditionType;
				numBaskets = _basketCount;

				fadeInBasketContainer().OnComplete( OpportunityStart );

				currentOpportunity++;
			}
			else
			{
				Debug.Log("TALLYING ANSWERS");
				tallyAnswers();
				numBaskets = 0;
			}


		}

		public override void OpportunityStart()
		{

			DebugConsole.LogWarning("Objects in a set started");

			switch( _level )
			{
			case SkillLevel.Tutorial:
				//playInstructionAudio("2D-Skill Instruction Audio/2D-now-its-your-turn", NOW_ITS);
				break;
			default:
				string s = playFindCurrentAudio();
				
				AddNewOppData( audioNames.IndexOf( s ) );
				break;
			}

			timeoutCount = 0;
			
			foreach( ObjectsInASetObject obj in baskets )
			{
				AddNewObjectData( formatObjectData( obj.Amount.ToString(), Vector2.zero, obj.Amount == answerNumber ) );
			}

			dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_START, true, false) );
		}

		public override void onTimerComplete()
		{
			base.onTimerComplete();

			stopTimer();

			timeoutCount++;
			
			string clipName = "";

			switch(timeoutCount)
			{
			case 1:
				if(_level == SkillLevel.Tutorial)
				{	
					clipName = playInstructionAudio(INSTRUCTION_PATH + "2D-now-its-your-turn", NOW_ITS);
				}
				else
				{
					clipName = playFindCurrentAudio();
				}
				break;
			case 2:
				if(_level == SkillLevel.Tutorial)
				{	
					disableBasketClicks();
					if(opportunityAnswered)
					{
						opportunityComplete = true;

						OnAudioWordComplete(NOW_LETS);
						return;
					}

					opportunityAnswered = true;
					opportunityCorrect = false;

					clipName = playInstructionAudio(INSTRUCTION_PATH + "2D-lets-try-again", NOW_LETS);

				}
				else
				{
					clipName = playInstructionAudio(INSTRUCTION_PATH + "2D-touch-the-green-button", NOW_ITS);
				}
				break;
			case 3:
				if(opportunityAnswered == false)
				{
					opportunityAnswered = true;
					opportunityCorrect = false;
				}

				foreach(ObjectsInASetObject lb in baskets)
				{
					if(lb.Amount == answerNumber)
					{
						lb.Destroy();

						lb.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
						lb.filters[0].Visible = true;

						break;

					}
				}

				clipName = playFindCurrentAudio();
				break;
			case 4:
				timeoutEnd = true;
				disableBasketClicks();
				MarkInstructionStart();
				opportunityCorrect = false;
				opportunityComplete = true;

				foreach(ObjectsInASetObject lb in baskets)
				{
					lb.removeEventListener( MouseEvent.CLICK, onBasketClick );
					if(lb.Amount != answerNumber)
					{
						if(_level == SkillLevel.Completed || _level == SkillLevel.Developed)
							Tweener.addTween(lb, Tweener.Hash("time",0.5f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuad) );
						else
						{
							incorrectBasket = lb;
							otherNumber = lb.Amount;
						}
					}
					else
					{
						lb.Target.ScaleCentered( lb.Target.scaleX * 1.2f);
						lb.Destroy();

						lb.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
						lb.filters[0].Visible = true;
						
						correctBasket = lb;
					}
				}
				
				if(numberCard != null)
				{
					basketContainer.removeChild( numberCard );
					basketContainer.addChild( numberCard );
	
					numberCard.Destroy();
					
					Rectangle bound = correctBasket.getBounds(basketContainer);
					float targX = bound.x + ((bound.width - numberCard.getBounds(basketContainer).width) / 2);
	
					Tweener.addTween( numberCard, Tweener.Hash( "x", targX, "time",0.35f, "transition",Tweener.TransitionType.easeOutQuad ) );
				}

				clipName = playFoundCurrentAudio();
				break;
			}
			
			if( !string.IsNullOrEmpty( clipName ) )
			{
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf( clipName ) );
			}

		}

		private TweenerObj fadeInBasketContainer ()
		{
			basketContainer.visible = true;
			basketContainer.alpha = 0;
			return Tweener.addTween (basketContainer, Tweener.Hash ("alpha",1, "time",0.35f) );

		}

		private void playIntroAudio()
		{
			//OnAudioWordComplete( INTRO );

			if( !currentSession.skipIntro )
			{
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				
				AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.OBJECTS_IN_A_SET, "Narration/2D-Story Introduction Audio/2D-story-introduction" );
				bundle.AddClip(clip, INTRO, clip.length);
				SoundEngine.Instance.PlayBundle(bundle);
			}
			else
				OnAudioWordComplete( INTRO );
				
		}

		private string playFindCurrentAudio ()
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();

			string audioPath = "";

			string audioID = PLEASE_FIND;

			string compensationForPlural = (answerNumber == 1) ? "" : "s";

			switch(basketType)
			{
			case YELLOW_ALIENS:
				audioPath = INSTRUCTION_PATH + INITIAL_INSTRUCTION + "2D-touch-spacecraft-" + answerNumber + "-traveler" + compensationForPlural;
				break;

			case GREEN_ALIENS:

				string conditionFile = "2D-touch-spacecraft-more-travelers";
				if (conditionType == LESSTHAN) {
					conditionFile = "2D-touch-spacecraft-less-travelers";
				} 

				audioPath = INSTRUCTION_PATH + INITIAL_INSTRUCTION + conditionFile;

				break;

			case PINK_ALIENS:
				audioPath = INSTRUCTION_PATH + INITIAL_INSTRUCTION + "2D-touch-spacecraft-matches-numeral";
				break;
			}

			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.OBJECTS_IN_A_SET,  audioPath);

			if(clip != null)
			{
				bundle.AddClip(clip, audioID, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else
			{
				DebugConsole.LogWarning("Objects in a Set - Audio Clip is null: " + audioPath);
				OnAudioWordComplete(audioID);
			}
			
			return audioPath;
		}
		
		private void playCorrectionAudio ()
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();

			string audioPath = "";

			string audioID = YOU_FOUND_INCORRECT;

			string compensationForPlural = (incorrectBasket.Amount == 1) ? "" : "s";
			
			switch(basketType)
			{
			case YELLOW_ALIENS:
				audioPath = REINFORCEMENT_PATH + CORRECTORANGEALIENS + incorrectBasket.Amount + "-traveler" + compensationForPlural;
				break;

			case GREEN_ALIENS:
				string condition = (conditionType == GREATERTHAN) ? LESS_ALIENS : MORE_ALIENS;
				audioPath = REINFORCEMENT_PATH + condition;
				break;

			case PINK_ALIENS:
				audioPath = REINFORCEMENT_PATH + CORRECTPINKALIENS + incorrectBasket.Amount;
				break;
			}

			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.OBJECTS_IN_A_SET,  audioPath);

			if(clip != null)
			{
				bundle.AddClip(clip, audioID, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
				
				if(wrongNumberCard != null)
					glowTimer = TimerUtils.SetTimeout(3.5f, WrongNumCardGlow);
			}
			else
			{
				DebugConsole.LogWarning("Objects in a Set - Audio Clip is null: " + audioPath);
				OnAudioWordComplete(audioID);
			}
		}
		
		private void WrongNumCardGlow()
		{
			wrongNumberCard.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
		}

		private string playFoundCurrentAudio ()
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();

			string audioPath = "";

			string audioID = "CONFIRMCORRECT";

			string compensationForPlural = (correctBasket.Amount == 1) ? "" : "s";

			switch(basketType)
			{
			case YELLOW_ALIENS:
				audioPath = REINFORCEMENT_PATH + CORRECTORANGEALIENS + correctBasket.Amount.ToString() + "-traveler" + compensationForPlural;
				break;

			case GREEN_ALIENS:
				string condition = (conditionType == GREATERTHAN) ? MORE_ALIENS : LESS_ALIENS;
				audioPath = REINFORCEMENT_PATH + condition;
				break;

			case PINK_ALIENS:
				audioPath = REINFORCEMENT_PATH + CORRECTPINKALIENS + correctBasket.Amount.ToString();
				audioID = YOU_FOUND;
				glowTimer = TimerUtils.SetTimeout(3.5f, glowSwap);
				break;
			}

			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.OBJECTS_IN_A_SET,  audioPath);

			if(clip != null)
			{
				bundle.AddClip(clip, audioID, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else
			{
				DebugConsole.LogWarning("Objects in a Set - Audio Clip is null: " + audioPath);
				OnAudioWordComplete(audioID);
			}
			
			return audioPath;
		}
		
		private void glowSwap()
		{
			if(_level == SkillLevel.Completed || _level == SkillLevel.Developed)
			{
				numberCard.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
			}
			else
			{
				correctBasket.Destroy();
				incorrectBasket.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
				glowTimer = TimerUtils.SetTimeout( 0.8f, swappedGlowOff ); 
			}
		}
		
		private void swappedGlowOff()
		{
			incorrectBasket.Destroy();
		}

		private void playConfirmCurrentAudio ()
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();

			string audioPath = "";

			string audioID = YOU_FOUND;

			switch(conditionType)
			{
			case GREATERTHAN:
				audioPath = REINFORCEMENT_PATH + "2D-" + answerNumber.ToString() + "-more-than-" + otherNumber.ToString();
				break;

			case LESSTHAN:
				audioPath = REINFORCEMENT_PATH + "2D-" + answerNumber.ToString() + "-less-" + otherNumber.ToString();
				break;

			}

			DebugConsole.LogWarning("playConfirmCurrentAudio || audioPath: {0}", audioPath);

			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.OBJECTS_IN_A_SET,  audioPath);

			if(clip != null)
			{
				bundle.AddClip(clip, audioID, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
				
				glowTimer = TimerUtils.SetTimeout( 1.5f, glowSwap);
			}
			else
			{
				DebugConsole.LogWarning("Objects in a Set - Audio Clip is null: " + audioPath);
				OnAudioWordComplete(audioID);
			}
		}

		private string playInstructionAudio (string root, string audioID)
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();

			string audioPath = root;

			DebugConsole.LogWarning("playInstructionAudio || audioPath: {0}", audioPath);

			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.OBJECTS_IN_A_SET,  audioPath);

			if(clip != null)
			{
				bundle.AddClip(clip, audioID, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else
			{
				DebugConsole.LogWarning("Objects in a Set - Audio Clip is null: " + audioPath);
				OnAudioWordComplete(audioID);
			}
			
			return audioPath;
		}

		private void RemoveIncorrectBasketAfterTween()
		{
			incorrectBasket.Destroy();
			incorrectBasket.Target.mouseEnabled = false;
			incorrectBasket.removeAllEventListeners(MouseEvent.CLICK);
			incorrectBasket.Target.x = incorrectBasket.Target.y = -1000;
			incorrectBasket.x = incorrectBasket.y = -1000;
			incorrectBasket = null;
		}

		public override void Resize( int width, int height )
		{
			Rect clipping;
			if( selectedPlatform == Platforms.WIN || selectedPlatform == Platforms.IOSRETINA )
				clipping = new Rect( 1f, 1f, 2046f, 1534f );
			else
				clipping = new Rect( 0.5f, 0.5f, 1023f, 767f );
			
			float scale = (width + 4f) / (clipping.width);

			basketContainer.width = width;
			basketContainer.height = height;

			basketContainer.scaleX = basketContainer.scaleY = scale;

			basketContainer.x = (width - (basketContainer.width*basketContainer.scaleX))/2;
			basketContainer.y = 0;
		}
		
		private void parseSessionData(string data)
		{
			setList = new List<ObjectsInASetOpp>();
			
			string[] sData = data.Split('-');
			
			foreach(string s in sData)
			{
				setList.Add( new ObjectsInASetOpp(s) );
			}
		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = "";
			
			foreach( ObjectsInASetOpp opp in setList )
			{
				if(data != "")
					data += "-";
				data += opp.ToString();
			}
			
			return data;
		}
		
		public static List<string> audioNames = new List<string>
		{ 	"",
			"Narration/2D-Story Introduction Audio/2D-story-introduction",
			"Narration/2D-Skill Instruction Audio/2D-lets-count-and-compare",
			"Narration/2D-Skill Instruction Audio/2D-watch-closely-as-cami",
			"Narration/2D-Skill Instruction Audio/2D-now-its-your-turn",
			"Narration/2D-Skill Instruction Audio/2D-look-at-how-many-travelers",
			"Narration/2D-Skill Instruction Audio/2D-try-again",
			"Narration/2D-Skill Instruction Audio/2D-touch-the-green-button",
			"Narration/2D-Skill Instruction Audio/2D-Initial Instructions/2D-touch-spacecraft-1-traveler",
			"Narration/2D-Skill Instruction Audio/2D-Initial Instructions/2D-touch-spacecraft-2-travelers",
			"Narration/2D-Skill Instruction Audio/2D-Initial Instructions/2D-touch-spacecraft-3-travelers",
			"Narration/2D-Skill Instruction Audio/2D-Initial Instructions/2D-touch-spacecraft-4-travelers",
			"Narration/2D-Skill Instruction Audio/2D-Initial Instructions/2D-touch-spacecraft-5-travelers",
			"Narration/2D-Skill Instruction Audio/2D-Initial Instructions/2D-touch-spacecraft-less-travelers",
			"Narration/2D-Skill Instruction Audio/2D-Initial Instructions/2D-touch-spacecraft-more-travelers",
			"Narration/2D-Skill Instruction Audio/2D-Initial Instructions/2D-touch-spacecraft-matches-numeral",
			"Narration/2D-Reinforcement Audio/2D-1-less-2",
			"Narration/2D-Reinforcement Audio/2D-1-less-3",
			"Narration/2D-Reinforcement Audio/2D-1-less-4",
			"Narration/2D-Reinforcement Audio/2D-1-less-5",
			"Narration/2D-Reinforcement Audio/2D-2-less-3",
			"Narration/2D-Reinforcement Audio/2D-2-less-4",
			"Narration/2D-Reinforcement Audio/2D-2-less-5",
			"Narration/2D-Reinforcement Audio/2D-2-more-than-1",
			"Narration/2D-Reinforcement Audio/2D-3-less-4",
			"Narration/2D-Reinforcement Audio/2D-3-less-5",
			"Narration/2D-Reinforcement Audio/2D-3-more-than-1",
			"Narration/2D-Reinforcement Audio/2D-3-more-than-2",
			"Narration/2D-Reinforcement Audio/2D-4-less-5",
			"Narration/2D-Reinforcement Audio/2D-4-more-than-1",
			"Narration/2D-Reinforcement Audio/2D-4-more-than-2",
			"Narration/2D-Reinforcement Audio/2D-4-more-than-3",
			"Narration/2D-Reinforcement Audio/2D-5-more-than-1",
			"Narration/2D-Reinforcement Audio/2D-5-more-than-2",
			"Narration/2D-Reinforcement Audio/2D-5-more-than-3",
			"Narration/2D-Reinforcement Audio/2D-5-more-than-4",
			"Narration/2D-Reinforcement Audio/2D-this-spacecraft-has-1-traveler",
			"Narration/2D-Reinforcement Audio/2D-this-spacecraft-has-2-travelers",
			"Narration/2D-Reinforcement Audio/2D-this-spacecraft-has-3-travelers",
			"Narration/2D-Reinforcement Audio/2D-this-spacecraft-has-4-travelers",
			"Narration/2D-Reinforcement Audio/2D-this-spacecraft-has-5-travelers",
			"Narration/2D-Reinforcement Audio/2D-this-spacecraft-has-less-travelers",
			"Narration/2D-Reinforcement Audio/2D-this-spacecraft-has-less-travelers",
			"Narration/2D-Reinforcement Audio/2D-this-numeral-1",
			"Narration/2D-Reinforcement Audio/2D-this-numeral-2",
			"Narration/2D-Reinforcement Audio/2D-this-numeral-3",
			"Narration/2D-Reinforcement Audio/2D-this-numeral-4",
			"Narration/2D-Reinforcement Audio/2D-this-numeral-5",
			"User-Earns-Artifact"
		};
	}

	public class ObjectsInASetObject : FilterMovieClip
	{

		private string basketTypeID;
		public string BasketType
		{
			get{ return basketTypeID; }
		}

		private int amountID;
		public int Amount
		{
			get{ return amountID; }
		}

		public ObjectsInASetObject( MovieClip target_mc, string type, int amount ):base( target_mc )
		{
			basketTypeID = type;
			amountID = amount;
		}

	}
	
	public class ObjectsInASetOpp
	{
		public int correctNum;
		public string conditionType;
		
		public ObjectsInASetOpp( int num, string type )
		{
			correctNum = num;
			conditionType = type;
		}
		
		public ObjectsInASetOpp( string data )
		{
			correctNum = int.Parse(data[0].ToString());
			conditionType = data.Remove(0, 1);
		}
		
		public override string ToString()
		{
			return (correctNum.ToString() + conditionType);
		}
	}
	
}