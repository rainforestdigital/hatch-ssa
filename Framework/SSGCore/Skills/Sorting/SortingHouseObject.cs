using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;
using pumpkin.text;

/// <summary>
/// Counting object. Used for all game pieces in CountingFoundations games
/// </summary>
namespace SSGCore
{
	public class SortingHouseObject : FilterMovieClip
	{
		public int id = -1;
		public string type;
		
		public SortingHouseObject(MovieClip _target, string labelToGoTo):base(_target)
		{
			GoToImage(labelToGoTo);
			//DebugConsole.Log("COUNTING OBJECT CREATED: " + labelToGoTo + ", VISIBLE? " + Target.visible +", target label: " + Target.currentLabel);
		}
		
		private void SetFilter()
		{
			if( filters.Count > 0 )
			{
				Destroy();
				filters = new List<MovieClipFilter>();
			}
			MovieClipFilter filter = FiltersFactory.GetScaledYellowGlowFilter();
			if( filter == null ) DebugConsole.LogError("FILTER IS NULL FROM FACTORY");
			else AddFilter(filter);
			
		}
		
		public void ShowFilter()
		{
			if( filters.Count > 0 ) filters[0].Visible = true;
			else AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
		}
		
		/*public override void AddFilter(MovieClipFilter filter){
		
			filters.Add(filter);
			
			filter.SetTexture(target.getChildAt<Sprite>(1));
			addChildAt(filter.GetSprite(), 0);
			filter.ApplyEffect();
		}*/
		
		public void HideFilter()
		{
			if( filters.Count > 0 ) filters[0].Visible = false;
		}
		
		public void GoToImage(string _type)
		{
			type = _type;
			string tempType = type == "GRAIN" ? "BREAD" : type;
			Target.gotoAndStop(tempType);
			Target.visible = true;
			SetFilter();
			HideFilter();
			//DebugConsole.Log("FRAME TO GO TO FOR COUNTING OBJECT: " + type+ ", label at: " + Target.currentLabel + ", visible: " + Target.visible);
		}
	}
}

