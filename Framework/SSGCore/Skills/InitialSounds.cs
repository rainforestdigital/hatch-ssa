using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.geom;

namespace SSGCore
{

	public class InitialSounds : BaseSkill
	{
	
		private const string LAY_OF_LAND = "LAY_OF_LAND";
		private const string TOUCH_THE_PICTURES = "TOUCH_THE_PICTURES";
		private const string SAY_WORD = "SAY_WORD";
		private const string SAY_LETTER = "SAY_LETTER";
		private const string ANOTHER = "ANOTHER";
		
		private List<string> words = new List<string>(){ "CAKE","CAT","CAN","CAR","TENT","TOE","TIE","TAG","BALL","BAT",
												"BIKE","BOOK","PIG","PAN","PAIL","MAT","MUG","MOUSE","DOLL",
												"DUCK","DOG","GATE","GIRL","GOAT","HAT","HOUSE","HEART","ROCK",
												"RUG","RABBIT","SOCK","SOAP","SACK","LAMP","LIME","LOG","NAIL",
												"NUT","WATCH","WAGON"};
		
		private List<string> initialSounds = new List<string>(){"C","T","B","P","M","D","G","H","R","S"};
				
		private List<string> _currentWords;
		private List<string> _answeredWords;
		
		private FilterMovieClip _answerCard;
		private FilterMovieClip _currentCard;
		private FilterMovieClip _incorrectCard;
		
		private List<FilterMovieClip> _cards;
		private List<FilterMovieClip> _answeredCards;
		
		private string _currentSound;
		private string layOfLandWord;
		
		private int _layOfLandCount = 0;
//		private int _layOfLandSkip = -1;
		
		private FilterMovieClip ear_mc;
		private DisplayObjectContainer cardContainer_mc;

		private float cardOffsetX;
		private float cardOffsetY;
		
		private float baseScale;
		
		private TimerObject glowTimer;
		private TimerObject cardTimer;
		
		private new InitialSoundsTutorial tutorial_mc;
		private bool cardIsInEar = false;

		private Vector2 earOffset = new Vector2 (25f, 25f);

		//private float cardwidth;
		
		public InitialSounds( SkillLevel level, SessionInfo  currentSession) : base(level, currentSession)
		{
			BACKGROUND = "Background";
			ARTIFACT = "Artifact";
			
			//PreCacheAssets();
			Init( null );
		}
		
		private void PreCacheAssets()
		{
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle("InitialSounds", OnAssetsLoaded));
		}
		
		void OnAssetsLoaded(AssetBundle bundle)
		{
			if( bundle != null )
			{
				Init( null );
			}
		}
		
		/// <summary>
		/// Gets or sets the number cards.
		/// </summary>
		/// <value>
		/// The number cards to be used.
		/// </value>
		private int numCards
		{
			get{ return _cards.Count; }
			set
			{
				int i;
				
				for(i = 0; i < _cards.Count; i++)
				{
					_cards[i].removeEventListener( MouseEvent.MOUSE_DOWN, onCardDown );
					cardContainer_mc.removeChild( _cards[i] );
					_cards[i].Destroy();
				}
				foreach(FilterMovieClip a in _answeredCards)
					a.Destroy();
				
				_cards.Clear();
				
				_answeredCards.Clear();
				
				for(i = 0; i< value; i++)
				{
					MovieClip tempCard = MovieClipFactory.CreateInitialSoundsCard();
					FilterMovieClip tempCardFilter = new FilterMovieClip( tempCard );
					tempCardFilter.scaleX = tempCardFilter.scaleY = baseScale;
					tempCardFilter.AddFilter( FiltersFactory.GetYellowGlowFilter() );
					tempCardFilter.filters[0].Visible = false;
					tempCardFilter.filters[0].GetSprite().ScaleCentered(1.05f);
					tempCardFilter.filters[0].GetSprite().mouseEnabled = false;
					tempCard.gotoAndStop( _currentWords[i] );
					tempCardFilter.addEventListener( MouseEvent.MOUSE_DOWN, onCardDown );
					_cards.Add( tempCardFilter );
					if( _level != SkillLevel.Tutorial ) tempCardFilter.Target.alpha = 0;
					tempCardFilter.name = _currentWords[i];
					cardContainer_mc.addChild( tempCardFilter );
					tempCard.mouseChildrenEnabled = false;
					tempCard.mouseEnabled = false;
				}
				
				if(_level == SkillLevel.Tutorial)
				{
					MovieClip cardTarget = _cards[1].Target;
					cardTarget.visible = false;
					_answeredWords.Add(cardTarget.currentLabel.ToUpper());
					_answeredCards.Add(_cards[1]);
				}
				else
				{
					_cards.Shuffle();
				}
								
				//cardwidth = _cards[0].width;
				
				updateCards();//cardContainer_mc.LayoutTiled(_cards, 1);
			}
		}
		
		/// <summary>
		/// Init the specified parent.
		/// </summary>
		/// <param name='parent'>
		/// Parent MovieClip. Not currently used.
		/// </param>
		public override void Init(MovieClip parent)
		{
			
			base.Init(parent);
			DebugConsole.Log("Level: " + _level);
			
			_cards = new List<FilterMovieClip>();
			
			this.visible = false;
			
			cardContainer_mc = new DisplayObjectContainer();
			cardContainer_mc.width = MainUI.STAGE_WIDTH;
			cardContainer_mc.height = MainUI.STAGE_HEIGHT;
			addChild(cardContainer_mc);
			
			ear_mc = new FilterMovieClip ( MovieClipFactory.CreateInitialSoundsEar() );
			ear_mc.x = 960f;
			ear_mc.y = 145f;
			baseScale = 254.1f / ear_mc.width;
			ear_mc.scaleX = ear_mc.scaleY = baseScale;
			ear_mc.Target.addEventListener( MouseEvent.MOUSE_DOWN, onEarDown );
			ear_mc.Target.addEventListener( MouseEvent.CLICK, onEarClick );
			ear_mc.Target.mouseEnabled = false;
			ear_mc.AddFilter( FiltersFactory.GetYellowGlowFilter() );
			ear_mc.filters[0].Visible = false;
			ear_mc.filters[0].GetSprite().ScaleCentered(1.05f);
			//cardContainer_mc.addChild( ear_mc );

			_currentWords = new List<string>();
			_answeredWords = new List<string>();
			_answeredCards = new List<FilterMovieClip>();
			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			Resize( Screen.width, Screen.height );
			
			switch(_level)
			{
				case SkillLevel.Tutorial:
					opportunityComplete = false;
					//ear_mc.visible = false;
					totalOpportunities = 1;
					if(resumingSkill)
					{
						nextOpportunity();
					}
					else
					{
						tutorial_mc = new InitialSoundsTutorial(baseScale);
						tutorial_mc.Resize(Screen.width, Screen.height);
						tutorial_mc.View.addEventListener( TutorialEvent.START, onTutorialStart );
						tutorial_mc.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
					}
					
				break;
				
				default:
					totalOpportunities = 10;
					
					if(resumingSkill)
					{
						parseSessionData( currentSession.sessionData );
					//	nextOpportunity();
					}
				
				break;
				
			}
			
		}
		
		
		/// <summary>
		/// Initializes the nexts the opportunity.
		/// </summary>
		public override void nextOpportunity()
		{
			MarkInstructionStart();
					
			_answeredWords.Clear();
			addChild(cardContainer_mc);
			Resize( Screen.width, Screen.height );
			
			this.visible = true;
			
			switch(_level)
			{
				case SkillLevel.Tutorial:
					if(currentOpportunity > 0)
					{
						tallyAnswers();
					}
					else
					{
						_currentSound = "B";
						_currentWords = new List<string>(){"BAT", "BALL"};
						numCards = 2;
						ear_mc.alpha = 1f;
						cardContainer_mc.alpha = 1f;
						OpportunityStart();
					}
				break;
				
				case SkillLevel.Emerging:
					setNextOpportunity(2, 0, 2);
				break;
				
				case SkillLevel.Developing:
					setNextOpportunity(2, 1, 3);
				break;
				
				case SkillLevel.Developed:
				case SkillLevel.Completed:
					setNextOpportunity(3, 1, 4);
				break;
				
				
			}
				
			currentOpportunity++;
		
		}
		
		public override void Start()
		{
			MarkInstructionStart();
		}
		
		/// <summary>
		/// Sets the properties for the next opportunity.
		/// </summary>
		/// <param name='correctCount'>
		/// Number of correct options.
		/// </param>
		/// <param name='incorrectCount'>
		/// Number of incorrect options.
		/// </param>
		/// <param name='cardCount'>
		/// Number of cards to be used.
		/// </param>
		private void setNextOpportunity(int correctCount, int incorrectCount, int cardCount)
		{
			if(_answerCard != null)
				_answerCard.Destroy();
			if( _currentCard != null)
				_currentCard.Destroy();
			if(_incorrectCard != null)
				_incorrectCard.Destroy();
		
			if(_cards != null)
			{
				foreach(FilterMovieClip f in _cards)
					f.Destroy();
			}
			if(_answeredCards != null)
			{
				foreach(FilterMovieClip c in _answeredCards)
					c.Destroy();
			}
			
			int randomAnswer;
			List<string> correctArray = new List<string>();
			List<string> incorrectArray = new List<string>();
			
			int i = 0;
			
			if(currentOpportunity > 0)
			{
				if(opportunityComplete)
				{
					if(opportunityCorrect)
					{
						//NumCorrect++;
					}
					else
					{
						//NumIncorrect++;
					}
				}
				else
				{
					//NumIncorrect++;
				}
			}
			if(currentOpportunity < 10)
			{
				opportunityComplete = false;
				opportunityCorrect = false;
				opportunityAnswered = false;
				
				randomAnswer = Mathf.FloorToInt(Random.Range(0, initialSounds.Count));
				if(randomAnswer == initialSounds.Count)
					randomAnswer--;
				if(resumingSkill)
				{
					randomAnswer = 0;
					resumingSkill = false;
				}
				_currentSound = initialSounds[randomAnswer];
				initialSounds.RemoveAt(randomAnswer);
				
				_currentWords.Clear();
				
				for(i = 0; i < words.Count; i++)
				{
					string firstLetter = words[i].Substring(0,1);
					
					if(firstLetter == _currentSound)
					{
						correctArray.Add( words[i] );
					}
					else
					{
						incorrectArray.Add( words[i] );
					}
				}
				
				for(i = 0; i < correctCount; i++)
				{
					randomAnswer = Mathf.FloorToInt(Random.Range(0, correctArray.Count));
					if(randomAnswer == correctArray.Count)
						randomAnswer--;
					_currentWords.Add(correctArray[randomAnswer]);
					correctArray.RemoveAt(randomAnswer);
				}
				
				for(i = 0; i < incorrectCount; i++)
				{
					randomAnswer = Mathf.FloorToInt(Random.Range(0, incorrectArray.Count));
					if(randomAnswer == incorrectArray.Count)
						randomAnswer--;
					_currentWords.Add(incorrectArray[randomAnswer]);
					incorrectArray.RemoveAt(randomAnswer);
				}
				
				numCards = cardCount;
				
				Tweener.addTween( ear_mc, Tweener.Hash("time",0.35f, "alpha",1, "transition",Tweener.TransitionType.easeInOutQuint) );
				fadeInCardContainer().OnComplete( OpportunityStart );
				
			}
			else
			{
				//_percent = Mathf.Round((NumCorrect / (NumCorrect + NumIncorrect)) * 100);
				numCards = 0;
				tallyAnswers();
				
			}
			
		}
		
		/// <summary>
		/// Starts the opportunity.
		/// </summary>
		public override void OpportunityStart()
		{
			
			AudioClip clip = null;
			string clipName = "";
			string clipID = "";
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			
			disableCardButtons();
			
			ear_mc.visible = true;
			
			switch(_level)
			{
				case SkillLevel.Tutorial:
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.INITIAL_SOUNDS, "1B-Now-its-your-turn" );
					clipName = "1B-Now-its-your-turn";
					clipID = NOW_ITS;
				
					for(int i = 0; i < _cards.Count; i++)
					{
						if(_cards[i].Target.visible)
						{
							_cards[i].Target.mouseEnabled = true;
						}
					}
					
				break;
				default:
					
					_layOfLandCount = 0;
					
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.INITIAL_SOUNDS, "1B-Listen-to-the-words" );
					clipName = "1B-Listen-to-the-words";
					clipID = LAY_OF_LAND;
				
				break;
			}
			
			//ear_mc.Target.mouseEnabled = true;
			
			bundle.AddClip(clip, clipID, clip.length);
			SoundEngine.Instance.PlayBundle( bundle );
			
			cardContainer_mc.addChildAt(ear_mc, 0);
			
			AddNewOppData( audioNames.IndexOf( clipName ) );
			
			Vector2 point = ear_mc.parent.localToGlobal( new Vector2(ear_mc.x, ear_mc.y) );
			AddNewObjectData( formatObjectData( "ear", point ) );
			foreach( FilterMovieClip fmc in _cards )
			{
				point = fmc.parent.localToGlobal( new Vector2(fmc.x, fmc.y) );
				AddNewObjectData( formatObjectData( fmc.name, point, fmc.name.Substring(0,1) == _currentSound ) );
			}
			
			//enableCardButtons();
			
			timeoutCount = 0;
			
			//updateCards();//cardContainer_mc.LayoutTiled(_cards, 1);
			
			dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_START, true, false) );
			
		}
		
		/// <summary>
		/// Raises the audio word complete event.
		/// Handles what happens after specific audio files are played
		/// </summary>
		/// <param name='word'>
		/// Parameter that gets passed in from the delegate.
		/// </param>
		public override void OnAudioWordComplete( string word )
		{
			
			//clearAnswerCard();
			
			AudioClip clip = null;
			SoundEngine.SoundBundle bundle = null;
			
			for(int i = 0; i < _cards.Count; i++)
			{
				if(_cards[i].filters.Count > 0)
					_cards[i].filters[0].Visible = false;	
			}
			
			DebugConsole.Log("Level at Audio Complete:" + _level);
			if(ear_mc.filters.Count >0)
				ear_mc.filters[0].Visible = false;
			
			if(opportunityComplete)
			{
				
				switch( word )
				{
							
					case THEME_COMPLIMENT:
						wordBegins(_currentCard.Target.currentLabel);
					break;
						
					case YOU_FOUND:
						clearAnswerCard();
						if(_level == SkillLevel.Tutorial)
						{
							bundle = new SoundEngine.SoundBundle();
							clip = SoundUtils.LoadGlobalSound(MovieClipFactory.INITIAL_SOUNDS, "1B-play-game");
							bundle.AddClip( clip, NOW_LETS, clip.length );
							SoundEngine.Instance.PlayBundle( bundle );
						}
						else
						{
							Tweener.addTween( cardContainer_mc, Tweener.Hash("time",0.2f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuint) );
							
							if(opportunityCorrect)
							{
								dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
								
							}
							else
							{
								dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
							}
						}
					break;
					
					case LAY_OF_LAND:
						Tweener.addTween(_cards[_layOfLandCount], Tweener.Hash("time", 0.2f, "alpha", 0));
						layOfLandFail();
						break;
					
					case TRY_AGAIN:
					case INTRO:
					case PLEASE_FIND:
					case NOW_ITS:
					case NOW_LETS:
					case YOU_FOUND_INCORRECT:
					case THIS_IS :
					case TOUCH_THE_PICTURES:
					case SAY_LETTER:
					case SAY_WORD:
						Tweener.addTween( cardContainer_mc, Tweener.Hash("time",0.2f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuint) );
						earGlowOff();
						if(opportunityCorrect)
						{
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
						}
						else
						{
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
						}
					break;
					
					case CLICK_CORRECT:
						dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
					break;
					
					case CLICK_INCORRECT:
						dispatchEvent( new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
					break;
				}
				
			}
			else
			{
				switch(word)
				{
					case NOW_ITS:
						pleaseTouch();
					break;
					
					case YOU_FOUND_INCORRECT:
						pleaseTouch();
						clearAnswerCard();
						break;
					case THEME_CRITICISM:
						string s = _currentCard.Target.currentLabel.ToLower();
						
						bundle = new SoundEngine.SoundBundle();
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.INITIAL_SOUNDS, "1B-" + s + "-begins-with-" + s.Substring(0,1) );
				
						bundle.AddClip( clip, YOU_FOUND_INCORRECT, clip.length );
						SoundEngine.Instance.PlayBundle( bundle );
						
					break;
				
					case YOU_FOUND:
						bundle = new SoundEngine.SoundBundle();
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.INITIAL_SOUNDS, "1B-Another-" + _currentSound + "-Word" );
						
						bundle.AddClip( clip, ANOTHER, clip.length );
						SoundEngine.Instance.PlayBundle( bundle );
					
						clearAnswerCard();
						enableCardButtons();
					break;
					
					case ANOTHER:
						pleaseTouch();
					break;
					
					case LAY_OF_LAND:
						layOfLandComplete();
					break;
					
					case SAY_LETTER:
					case SAY_WORD:
						ear_mc.Target.mouseEnabled = true;
						earGlowOff();
						timeoutCount = 0;
						startTimer();
					break;
					
					case INTRO:
						if(_level == SkillLevel.Tutorial)
						{
							tutorial_mc.OnStart();
							addChild(tutorial_mc.View);
							this.visible = true;
						}
						else
						{
							nextOpportunity();
						}
					break;
					
					case PLEASE_FIND:
					case TOUCH_THE_PICTURES:
					case NOW_LETS:
					case THIS_IS :
						startTimer();
						enableCardButtons();
					break;
					
					case THEME_INTRO:

						if( !currentSession.skipIntro )
						{
							bundle = new SoundEngine.SoundBundle();
							clip = SoundUtils.LoadGlobalSound( MovieClipFactory.INITIAL_SOUNDS, "1B-Story-Introduction" );
							
							bundle.AddClip( clip, INTRO, clip.length );
							SoundEngine.Instance.PlayBundle( bundle );
						}
						else
							OnAudioWordComplete( INTRO );
					break;
					
					case TRY_AGAIN:
						resetTutorial();
					break;
					
					case CLICK_CORRECT:
						wordBegins(_currentCard.Target.currentLabel);
					break;
					
					case CLICK_INCORRECT:
						dispatchEvent( new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
					break;
				}
			}
			
		}
		
		/// <summary>
		/// Handles the "LayOfLand" audio complete.
		/// Called from OnAudioWordComplete
		/// </summary>
		private void layOfLandComplete()
		{
			for(int i = 0; i < _cards.Count; i++)
			{
				if(_cards[i].filters.Count > 0)
					_cards[i].filters[0].Visible = false;
			}
			
			if(_layOfLandCount > 0)
			{
				updateCards(_layOfLandCount - 1);
				cardContainer_mc.addChild(_cards[_layOfLandCount - 1]);
			}
			if(_layOfLandCount == _cards.Count)
			{
				//enableCardButtons();
				
				//ear_mc.Target.mouseEnabled = true;
				cardContainer_mc.addChildAt(ear_mc, 0);
				
				pleaseTouch();
			}
			else
			{
				float scale = baseScale * (5f/3f);
				Tweener.addTween(_cards[_layOfLandCount], Tweener.Hash("time", .25f, "x", ( cardContainer_mc.width - (257f * (5f/3f))) / 2f,
															"y",(cardContainer_mc.height - (150f * (5f/3f))) / 4f, "scaleX", scale, "scaleY", scale));
				
				cardContainer_mc.addChildAt(_cards[_layOfLandCount], cardContainer_mc.numChildren - 1);
				//_cards[_layOfLandCount].filters[0].Visible = true;
				_cards[_layOfLandCount].Target.alpha = 1;
				layOfLandWord = _cards[_layOfLandCount].Target.currentLabel;
				sayLayOfLandWord();
				_layOfLandCount++;
			}
			
		}
				
		/// <summary>
		/// Called when the tutorial complete event is fired.
		/// </summary>
		/// <param name='e'>
		/// Event param.
		/// </param>
		private void onTutorialComplete( CEvent e )
		{
			tutorial_mc.View.removeEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.View.removeEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			removeChild( tutorial_mc.View );
			nextOpportunity();
		}
		
		private void onEarDown( CEvent e )
		{
			PlayClickSound();
		}
		
		/// <summary>
		/// Handles when ear is clicked
		/// </summary>
		/// <param name='e'>
		/// Event param.
		/// </param>
		private void onEarClick( CEvent e )
		{	
			DebugConsole.Log( "Ear Click" );

			//Highlight Ear
			earGlowOn();

			//clearAnswerCard();
			
			Tweener.removeTweens(this);
			
			SoundEngine.Instance.SendCancelToken();
			
			//Timer reset
			timeoutCount = 0;
			stopTimer();
			
			letterSayLetter(_currentSound);
	
			AddNewActionData( formatActionData( "tap", 0, Vector2.zero ) );
		}
		private bool isdrag;
		private Vector2 cardOrig;
		/// <summary>
		/// Handles the card down mouse event.
		/// </summary>
		/// <param name='e'>
		/// Event param.
		/// </param>
		private void onCardDown( CEvent e )
		{

			if(e.currentTarget is FilterMovieClip)
			{

				//clearAnswerCard();
				
				PlayClickSound();
				
				Tweener.removeTweens(this);
				
				SoundEngine.Instance.SendCancelToken();
				
				timer = totalTime;
				timeoutCount = 0;
				stopTimer();
				
				_currentCard = e.currentTarget as FilterMovieClip;
				
				_currentCard.ScaleCentered( _currentCard.scaleX * 1.2f );
				
				//_currentCard.Target.ScaleCentered(1.2f);

				cardOrig = cardContainer_mc.localToGlobal( new Vector2(_currentCard.x, _currentCard.y) );
				
				cardOffsetX = cardOrig.x - mouseX;
				cardOffsetY = cardOrig.y - mouseY;

				stage.addEventListener( MouseEvent.MOUSE_UP, onCardUp );
				this.addEventListener( CEvent.ENTER_FRAME, moveCard );

				disableCardButtons();
				
				isdrag = false;
			}
			
		}
		
		/// <summary>
		/// Handles the card up mouse event.
		/// </summary>
		/// <param name='e'>
		/// Event param.
		/// </param>
		private void onCardUp( CEvent e )
		{

			stage.removeEventListener( MouseEvent.MOUSE_UP, onCardUp );
			this.removeEventListener( CEvent.ENTER_FRAME, moveCard );

			//enableCardButtons();
		
			if(_currentCard == null) return;

			_currentCard.ScaleCentered(baseScale);

			if(cardIsInEar)
			{
				SoundEngine.Instance.SendCancelToken();
				//SoundEngine.Instance.StopAll();

				_currentCard.removeEventListener( MouseEvent.MOUSE_DOWN, onCardDown );

				_currentCard.Target.mouseEnabled = false;
				_currentCard.Target.mouseChildrenEnabled = false;

				// Tween the current card to the center of the ear
				float targX = (ear_mc.x - (ear_mc.width/2)) + earOffset.x;
				float targY = (ear_mc.y - (ear_mc.height/2)) + earOffset.y;// - (_currentCard.height/2); // (MainUI.STAGE_HEIGHT) / 4f;

				Tweener.addTween(_currentCard, Tweener.Hash("time", .25f, "x", targX, "y", targY, "scaleX", baseScale, "scaleY", baseScale));
				
				string firstLetter = _currentCard.Target.currentLabel.Substring(0, 1);
				
				if(firstLetter == _currentSound)
				{
					
					DebugConsole.Log("is correct");
					_answerCard = _currentCard;
					_answeredCards.Add( _currentCard );
					_answeredWords.Add( _currentCard.Target.currentLabel );



					
					List<string> _answersLeft = answersLeft();

					DebugConsole.Log("answers left: " + _answersLeft.Count);
					
					if(_answersLeft.Count == 0 || _level == SkillLevel.Tutorial)
					{
						if(!opportunityAnswered)
						{
							opportunityAnswered = true;
							opportunityCorrect = true;
						}
						opportunityComplete = true; 
					}
					
					PlayCorrectSound();
					disableCardButtons();
					ear_mc.Target.mouseEnabled = false;
					
					AddNewActionData( formatActionData( "drag",
														_cards.IndexOf(_currentCard) + 1,
														_currentCard.parent.localToGlobal(new Vector2(_currentCard.x, _currentCard.y)),
														true ) );
					
					return;
					
				}
				else
				{
					DebugConsole.Log("not correct");
					if(!opportunityAnswered)
					{
						opportunityAnswered = true;
						opportunityCorrect = false;
					}
					
					_incorrectCard = _currentCard;
					//glow incorrect card
					
					PlayIncorrectSound();
					disableCardButtons();
					
					AddNewActionData( formatActionData( "drag",
														_cards.IndexOf(_currentCard) + 1,
														_currentCard.parent.localToGlobal(new Vector2(_currentCard.x, _currentCard.y)),
														false ) );
				}

				cardIsInEar = false;
			}
			else
			{
				//_currentCard.ScaleCentered( _currentCard.scaleX / 1.2f );
				wordSayWord(_currentCard.Target.currentLabel);
				timeoutCount = 0;
				enableCardButtons();
				updateCards();
				
				AddNewActionData( formatActionData( isdrag?"snap":"tap", _cards.IndexOf(_currentCard) + 1, Vector2.zero ) );
			}
		}
				
		/// <summary>
		/// EnterFrame event that handles when the card is being dragged around.
		/// </summary>
		/// <param name='e'>
		/// Event param.
		/// </param>
		private void moveCard( CEvent e )
		{
			
			timer = totalTime;
			
			Vector2 newPos = cardContainer_mc.globalToLocal(new Vector2(mouseX + cardOffsetX, mouseY + cardOffsetY));
			
			if( Mathf.Abs(cardOrig.x - newPos.x + cardOrig.y - newPos.y) > 5 ){
				isdrag = true;
			}
			
			_currentCard.x = newPos.x;
			_currentCard.y = newPos.y;

			Debug.Log ("Card Pos: " + newPos);

			//DebugConsole.Log(ear_mc.getUnfilteredBounds(cardContainer_mc).intersection( _currentCard.getUnfilteredBounds(cardContainer_mc) ).width.ToString());

			int boundThreshold = 200;
			if(PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA) boundThreshold = 200;

			if(ear_mc.getUnfilteredBounds(cardContainer_mc).intersection( _currentCard.getUnfilteredBounds(cardContainer_mc) ).width > boundThreshold)
			{
				enableFilter(ear_mc);
				enableFilter(_currentCard);
				cardIsInEar = true;
			}
			else
			{
				cardIsInEar = false;
				disableFilter(ear_mc);
				disableFilter(_currentCard);
			}
			
		}

		private void enableFilter(FilterMovieClip mc) 
		{
			if(mc.filters.Count > 0)
				mc.filters[0].Visible = true;
			else
				mc.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
		}
		
		private void disableFilter(FilterMovieClip mc) 
		{
			if(mc.filters.Count > 0)
				mc.filters[0].Visible = false;
		}

		/// <summary>
		/// Disables the card buttons.
		/// </summary>
		private void disableCardButtons()
		{
			for(int i = 0; i < _cards.Count; i++)
			{
				_cards[i].Target.mouseEnabled = false;
			}
			ear_mc.Target.mouseEnabled = false;
			
			MarkInstructionStart();
		}
		
		
		private void updateCards()
		{
			updateCards(-1);
		}
		
		/// <summary>
		/// Updates the card positions.
		/// </summary>
		private void updateCards( int i)
		{
			
			int padding = 20;
			
			float cW = 257f + padding;
			
			float totalWidth = (_cards.Count * cW) - padding;
			//DebugConsole.Log("cardwidth: " + cardwidth);
			float startX = (cardContainer_mc.width * 0.5f) - (totalWidth * 0.5f);
			float cardY = 430 + 151;
			
			if( i == -1 )
			{
				for( i = 0; i < _cards.Count; i++)
				{
					_cards[i].x = startX + (i * cW);
					_cards[i].y = cardY;
				}
			}
			else
			{
				Tweener.addTween(_cards[i], Tweener.Hash("time",.25f , "x", startX + (i * cW), "y", cardY, "scaleX", baseScale, "scaleY", baseScale));
			}
		}

		/// <summary>
		/// Enables the card buttons.
		/// </summary>
		private void enableCardButtons()
		{

			if(cardTimer != null)
				cardTimer.StopTimer();
			cardTimer = TimerUtils.SetTimeout(1f, onCardTimer);

		}

		//This is used to delay the active cards so the user can't select cards too fast, which breaks the game
		private void onCardTimer()
		{
			for(int i = 0; i < _cards.Count; i++)
			{
				_cards[i].Target.mouseEnabled = true;
			}
			ear_mc.Target.mouseEnabled = true;
			if(_level != SkillLevel.Tutorial) MarkInstructionEnd();
		}

		private void earGlow()
		{
			if(glowTimer != null)
				glowTimer.StopTimer();
			glowTimer = TimerUtils.SetTimeout(2.5f, earGlowOn);
		}
		
		private void earGlowOn()
		{
			if(ear_mc.filters.Count > 0)
				ear_mc.filters[0].Visible = true;
			else
				ear_mc.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
			if(glowTimer != null)
				glowTimer.StopTimer();
			glowTimer = TimerUtils.SetTimeout(2f, earGlowOff);
		}
		
		private void earGlowOff()
		{
			if(ear_mc.filters.Count > 0)
					ear_mc.filters[0].Visible = false;
			if(glowTimer != null)
				glowTimer.StopTimer();
		}
	
		/// <summary>
		/// Called when BaseSkill's timer completes.
		/// </summary>
		public override void onTimerComplete()
		{
			base.onTimerComplete();
			
			stopTimer();

			//disableCardButtons();

			timeoutCount++;
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = null;
			string clipName = "";
			string clipID = "";
			Debug.Log("Timer Complete 1");
			switch(timeoutCount)
			{
				case 1:
				Debug.Log("Timer Complete 2");
					if(_level == SkillLevel.Tutorial)
					{
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.INITIAL_SOUNDS, "1B-Now-its-your-turn" );
						clipName = "1B-Now-its-your-turn";
						clipID = NOW_ITS;
					}
					else
					{
					Debug.Log("Timer Complete 3");
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.INITIAL_SOUNDS, "1B-Touch-the-pictures-to-hear" );
						clipName = "1B-Touch-the-pictures-to-hear";
						clipID = TOUCH_THE_PICTURES;
					}
				break;
				
				case 2:
				Debug.Log("Timer Complete 4");
					if(_level == SkillLevel.Tutorial)
					{
						for(int i = 0; i < _cards.Count; i++)
						{
							_cards[i].Target.mouseEnabled = false;
						}
						
						if(opportunityAnswered == false)
						{
							opportunityAnswered = true;
							opportunityCorrect = true;
						}
						else
						{
							opportunityCorrect = false;
							opportunityComplete = true;
							OnAudioWordComplete(TRY_AGAIN);
							return;
						}
					
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.INITIAL_SOUNDS, "1B-try-again" );
						clipName = "1B-try-again";
						clipID = TRY_AGAIN;
					}
					else
					{
					Debug.Log("Timer Complete 5");
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.INITIAL_SOUNDS, "1B-help-button" );
						clipName = "1B-help-button";
						clipID = TOUCH_THE_PICTURES;
					}
				break;
				
				case 3:
					if(opportunityAnswered == false)
					{
					Debug.Log("Timer Complete 6");
						opportunityAnswered = true;
						opportunityCorrect = false;
					}
				
					for(int i = 0; i < _cards.Count; i++)
					{
					Debug.Log("Timer Complete 7: " + i);
						if(_cards[i].Target.currentLabel.Substring(0,1) == _currentSound)
						{
						Debug.Log("Timer Complete 7: " + i + " - a");
							if(ear_mc.filters.Count > 0)
							{
								if(_cards[i].filters.Count > 0)
								{
									_cards[i].filters[0].Visible = true;
								}
							}
							else
							{
								Debug.Log("Timer Complete 7: " + i + " - c");
								_cards[i].AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
							}
						}
					}
				
					clipName = pleaseTouch();
				break;
				
				case 4:
				Debug.Log("Timer Complete 8");
					disableCardButtons();
					timeoutEnd = true;
					opportunityComplete = true;
					_layOfLandCount = -1;
					layOfLandFail();
				break;
			}
			
			if(clip != null)
			{
				Debug.Log("Timer Complete 9");
				DebugConsole.LogError( "Clip is NOT null " + clipID + " timeoutCound: " + timeoutCount );
				bundle.AddClip( clip, clipID, clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else
			{
				Debug.Log("Timer Complete 10");
				DebugConsole.LogError( "Clip is null " + clipID + " timeoutCound: " + timeoutCount );
			}
			
			if( !string.IsNullOrEmpty(clipName) )
			{
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf(clipName) );
			}
		}
		
		protected override void resetTutorial ()
		{
			Tweener.addTween( cardContainer_mc, Tweener.Hash("time",0.2f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuint) );
			
			currentOpportunity = 0;
			
			tutorial_mc.decoy.Destroy();
			tutorial_mc = new InitialSoundsTutorial(baseScale);
			tutorial_mc.Resize(Screen.width, Screen.height);
			tutorial_mc.View.addEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			
			tutorial_mc.OnStart();
			addChild(tutorial_mc.View);
		}
		
		private void layOfLandFail()
		{
			if( _layOfLandCount < 0 )
			{
				for( int j = 0; j < _cards.Count; j++ )
				{
					string firstLetter = _cards[j].Target.currentLabel.Substring(0, 1);
					if(firstLetter != _currentSound)
					{
						Tweener.addTween(_cards[j], Tweener.Hash("time", 0.25f, "alpha", 0));
					}
				}
			}
			
			_layOfLandCount++;
			
			if(_layOfLandCount >= _cards.Count)
			{
				cardContainer_mc.removeChild(ear_mc);
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
				return;
			}
			
			string word = _cards[_layOfLandCount].Target.currentLabel.ToLower();
			
			if( word.Substring(0,1) != _currentSound.ToLower() )
			{
				layOfLandFail();
				return;
			}
			
			for(int i = 0; i < _answeredWords.Count; i++)
			{
				if( _answeredWords[i].ToLower() == word )
				{
					layOfLandFail();
					return;
				}
			}
			
			float scale = baseScale * (5f/3f);
			
			float targX = (MainUI.STAGE_WIDTH - (257f * (5f/3f))) / 2f;
			float targY = (MainUI.STAGE_HEIGHT - (314f * (5f/3f))) / 4f;
			
			Tweener.addTween(_cards[_layOfLandCount], Tweener.Hash("time", .25f, "x", targX, "y", targY, "scaleX", scale, "scaleY", scale));
			if(_cards[_layOfLandCount].filters.Count > 0)
				_cards[_layOfLandCount].filters[0].Visible = true;
			else
				_cards[_layOfLandCount].AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			string clipName = "1B-" + word + "-begins-with-" + word.Substring(0,1).ToLower();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.INITIAL_SOUNDS, clipName );;
			bundle.AddClip( clip, LAY_OF_LAND, clip.length );
			SoundEngine.Instance.PlayBundle( bundle );
			
			baseTheme.AddAudioSessionData( "time", audioNames.IndexOf(clipName) );
		}
		
		private List<string> answersLeft()
		{
			List<string> answersLeft = new List<string>();
			
			for(int i = 0; i < _cards.Count; i++)
			{
				string letter = _cards[i].Target.currentLabel.Substring(0, 1);
				if(_answeredWords.IndexOf(letter) == -1 && _cards[i].alpha != 0 && _cards[i].visible == true && letter == _currentSound && _cards[i] != _currentCard)
				{
					answersLeft.Add( _currentWords[i] );
				}
			}
			
			return answersLeft;
			
		}
		
		/// <summary>
		/// Clears the answer card.
		/// </summary>
		private void clearAnswerCard()
		{
			if(_answerCard != null)
			{
				Tweener.addTween(_answerCard, Tweener.Hash("time",0.25f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuad) ).OnComplete(nukeAnswerCard);
				foreach(MovieClipFilter f in _answerCard.filters)
				{
					f.GetSprite().mouseEnabled = false;
				}
				_answerCard.Target.mouseEnabled = false;
				_answerCard.removeAllEventListeners(MouseEvent.MOUSE_DOWN);
			}
			
			if(_incorrectCard != null)
			{
				Tweener.addTween(_incorrectCard, Tweener.Hash("time",0.25f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuad) ).OnComplete(nukeAnswerCard);
				foreach(MovieClipFilter mcf in _incorrectCard.filters)
				{
					mcf.GetSprite().mouseEnabled = false;
				}
				_incorrectCard.Target.mouseEnabled = false;
				_incorrectCard.removeAllEventListeners(MouseEvent.MOUSE_DOWN);
			}
		}
		private void nukeAnswerCard()
		{
			if(_answerCard != null)
			{
				if(_answerCard.parent != null)
					_answerCard.parent.removeChild(_answerCard);
				_answerCard.Destroy();
				_answerCard = null;
			}
			
			if(_incorrectCard != null)
			{
				if(_incorrectCard.parent != null)
					_incorrectCard.parent.removeChild(_incorrectCard);
				_incorrectCard.Destroy();
				_incorrectCard = null;
			}
		}
		
		/// <summary>
		/// Fades in the card container.
		/// </summary>
		/// <returns>
		/// Returns the TweenerObj used to fade in the container.
		/// </returns>
		private TweenerObj fadeInCardContainer()
		{
			return Tweener.addTween(cardContainer_mc, Tweener.Hash("time", 0.35f, "alpha", 1, "transition", Tweener.TransitionType.easeInOutQuint) );
		}
		
		/// <summary>
		/// Plays the specified "Say Letter" audio file.
		/// </summary>
		/// <param name='sound'>
		/// Sound.
		/// </param>
		private void letterSayLetter( string sound )
		{
			ear_mc.Target.mouseEnabled = false;
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.INITIAL_SOUNDS, "1B-Ear-"+ sound + "-say-" + sound );
			bundle.AddClip( clip, SAY_WORD, clip.length );
			SoundEngine.Instance.PlayBundle( bundle );
		}
		
		/// <summary>
		/// Plays the specified "Say Word" audio file.
		/// </summary>
		/// <param name='sound'>
		/// Sound.
		/// </param>
		private void wordSayWord( string word )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.INITIAL_SOUNDS, "1B-"+ word + "-say-" + word );
			bundle.AddClip( clip, SAY_WORD, clip.length );
			SoundEngine.Instance.PlayBundle( bundle );
		}
		
		/// <summary>
		/// Plays the specified "Word Begins" audio file.
		/// </summary>
		/// <param name='sound'>
		/// Sound.
		/// </param>
		private void wordBegins( string word )
		{
			DebugConsole.Log("word begins word: " + word);
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.INITIAL_SOUNDS, "1B-" + word.ToLower() + "-begins-with-" + word.Substring(0,1).ToLower());
			bundle.AddClip( clip, YOU_FOUND, clip.length );
			SoundEngine.Instance.PlayBundle( bundle );
		}
		
		/// <summary>
		/// Plays "Please Touch" the current sound audio file.
		/// </summary>
		/// <param name='sound'>
		/// Sound.
		/// </param>
		private string pleaseTouch()
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.INITIAL_SOUNDS, "1B-Touch-" + _currentSound);
			bundle.AddClip( clip, PLEASE_FIND, clip.length );
			SoundEngine.Instance.PlayBundle( bundle );
			
			earGlow();
			
			return "1B-Touch-" + _currentSound;
		}
		
		/// <summary>
		/// Plays the Lay of Land audio file.
		/// </summary>
		/// <param name='sound'>
		/// Sound.
		/// </param>
		private void sayLayOfLandWord()
		{
			
			DebugConsole.Log("layOfLandWord: " + layOfLandWord);
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.INITIAL_SOUNDS, "1B-"+ layOfLandWord + "-say-" + layOfLandWord );
			bundle.AddClip( clip, LAY_OF_LAND, clip.length );
			SoundEngine.Instance.PlayBundle( bundle );
		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = _currentSound;
			
			foreach(string s in initialSounds)
			{
				data = data + "-" + s;
			}
			return data;
		}
		
		private void parseSessionData (string data)
		{
			initialSounds = new List<string>();
			
			string[] sData = data.Split('-');
			
			foreach( string s in sData )
			{
				initialSounds.Add(s);
			}
		}
		
		/// <summary>
		/// Resize the specified width and height.
		/// </summary>
		/// <param name='width'>
		/// Width.
		/// </param>
		/// <param name='height'>
		/// Height.
		/// </param>
		public override void Resize( int width, int height )
		{
			this.width = width;
			this.height = height;
			this.x = 0;
			this.y = 0;
			cardContainer_mc.scaleY = height/cardContainer_mc.height;
			cardContainer_mc.scaleX = cardContainer_mc.scaleY;
			cardContainer_mc.y = 0;
			cardContainer_mc.x = (width - (cardContainer_mc.width*cardContainer_mc.scaleX))/2;
			//cardContainer_mc.FitToParent();
			//ear_mc.x = (cardContainer_mc.width/2);// + cardContainer_mc.x;
			//ear_mc.y = 303;

			float earscale = ( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA ) ? 0.85f : baseScale;

			ear_mc.scaleX = ear_mc.scaleY = earscale;
			ear_mc.x = (cardContainer_mc.width/2) - earOffset.x;// + cardContainer_mc.x;
			ear_mc.y = 372;

			Debug.LogError ("Ear Pos: " + ear_mc.x + " " + ear_mc.y);
			Debug.LogError ("Ear Dimensions: " + ear_mc.width + " " + ear_mc.height);
			Debug.LogError ("Ear Centre: " + (ear_mc.x - ear_mc.width / 2) + " " + (ear_mc.y - ear_mc.height / 2));
		} 
		
		/// <summary>
		/// Releases all resource used by the <see cref="SSGCore.InitialSounds"/> object.
		/// </summary>
		/// <remarks>
		/// Call <see cref="Dispose"/> when you are finished using the <see cref="SSGCore.InitialSounds"/>. The
		/// <see cref="Dispose"/> method leaves the <see cref="SSGCore.InitialSounds"/> in an unusable state. After calling
		/// <see cref="Dispose"/>, you must release all references to the <see cref="SSGCore.InitialSounds"/> so the garbage
		/// collector can reclaim the memory that the <see cref="SSGCore.InitialSounds"/> was occupying.
		/// </remarks>
		public override void Dispose ()
		{
			base.Dispose();
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			if(tutorial_mc != null && tutorial_mc.decoy != null)
				tutorial_mc.decoy.Destroy();
			if(_answerCard != null)
				_answerCard.Destroy();
			if( _currentCard != null)
				_currentCard.Destroy();
			if(_incorrectCard != null)
				_incorrectCard.Destroy();
			if(stage.hasEventListener(MouseEvent.MOUSE_UP))
				stage.removeEventListener( MouseEvent.MOUSE_UP, onCardUp );
		
			if(_cards != null)
			{
				foreach(FilterMovieClip f in _cards)
					f.Destroy();
			}
			if(_answeredCards != null)
			{
				foreach(FilterMovieClip c in _answeredCards)
					c.Destroy();
			}
			ear_mc.Destroy();
		}
		
		private static List<string> audioNames = new List<string>
		{	"",
			"1B-Story-Introduction", "1B-Tutorial-Instruction-Henry", "", "", "1B-TT-Instruction-Him", "1B-TT-Instruction-Her",
			"1B-play-game", "1B-Now-its-your-turn", "1B-Listen-to-the-words", "1B-Touch-the-pictures-to-hear", "1B-help-button",
			"1B-try-again", "1B-Touch-b", "1B-Touch-c", "1B-Touch-d", "1B-Touch-g", "1B-Touch-h", "1B-Touch-m", "1B-Touch-p",
			"1B-Touch-r", "1B-Touch-s", "1B-Touch-t", "1B-Ear-b-say-b", "1B-Ear-c-say-c", "1B-Ear-d-say-d", "1B-Ear-g-say-g",
			"1B-Ear-h-say-h", "1B-Ear-m-say-m", "1B-Ear-p-say-p", "1B-Ear-r-say-r", "1B-Ear-s-say-s", "1B-Ear-t-say-t",
			"1B-ball-say-ball", "1B-bat-say-bat", "1B-bike-say-bike", "1B-book-say-book", "1B-cake-say-cake", "1B-can-say-can",
			"1B-car-say-car", "1B-cat-say-cat", "1B-dog-say-dog", "1B-doll-say-doll", "1B-duck-say-duck", "1B-gate-say-gate",
			"1B-girl-say-girl", "1B-goat-say-goat", "1B-hat-say-hat", "1B-heart-say-heart", "1B-house-say-house",
			"1B-lamp-say-lamp", "1B-lime-say-lime", "1B-log-say-log", "1B-mat-say-mat", "1B-mouse-say-mouse", "1B-mug-say-mug",
			"1B-nail-say-nail", "1B-nut-say-nut", "1B-pail-say-pail", "1B-pan-say-pan", "1B-pig-say-pig", "1B-rabbit-say-rabbit",
			"1B-rock-say-rock", "1B-rug-say-rug", "1B-sack-say-sack", "1B-soap-say-soap", "1B-sock-say-sock", "1B-tag-say-tag",
			"1B-tent-say-tent", "1B-tie-say-tie", "1B-toe-say-toe", "1B-wagon-say-wagon", "1B-watch-say-watch", "1B-Another-b-Word",
			"1B-Another-c-Word", "1B-Another-d-Word", "1B-Another-g-Word", "1B-Another-h-Word", "1B-Another-m-Word",
			"1B-Another-p-Word", "1B-Another-r-Word", "1B-Another-s-Word", "1B-Another-t-Word", "1B-ball-begins-with-b",
			"1B-bat-begins-with-b", "1B-bike-begins-with-b", "1B-book-begins-with-b", "1B-cake-begins-with-c", "1B-can-beings-with-c",
			"1B-car-begins-with-c", "1B-cat-begins-with-c", "1B-dog-begins-with-d", "1B-doll-begins-with-d", "1B-duck-begins-with-d",
			"1B-gate-begins-with-g", "1B-girl-begins-with-g", "1B-goat-begins-with-g", "1B-hat-begins-with-h", "1B-heart-begins-with-h",
			"1B-house-begins-with-h", "1B-mat-begins-with-m", "1B-mouse-begins-with-m", "1B-mug-begins-with-m", "1B-pail-begins-with-p",
			"1B-pan-begins-with-p", "1B-pig-begins-with-p", "1B-rabbit-begins-with-r", "1B-rock-begins-with-r", "1B-rug-begins-with-r",
			"1B-sack-begins-with-s", "1B-soap-begins-with-s", "1B-sock-begins-with-s", "1B-tag-begins-with-t", "1B-tent-begins-with-t",
			"1B-tie-begins-with-t", "1B-toe-begins-with-t", "1B-lime-begins-with-l", "1B-log-begins-with-l", "1B-nail-begins-with-n",
			"1B-nut-begins-with-n", "1B-watch-begins-with-w", "1B-wagon-begins-with-w", "1B-lamp-begins-with-l", "User-Earns-Artifact"
		};
	}
}