using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.geom;

namespace SSGCore
{
	
	public class SegmentingCompoundWords : BaseSkill
	{
		
		public enum SegmentingCompoundWordsStates //The state of the game - a representation of the flow
		{  
			INTRO = 0, //The intial state - the theme's intro
			TUTORIAL, //The game is currently in a tutorial state, all matters handled by the tutorial
			INITIAL_INSTRUCTION, //The instructions given to the player at the beggining of the skill
			SETUP_AND_PROMPT, //The prompt for the current compound word
			PLAYER_INTERACTION,  //Waiting for the Player interaction
			PLAYER_INTERACTING, //They're doing somethings
			SOLVE_FOR_PLAYER, //game solves the current opportunity for the player
			PLAYER_REINFORCEMENT, //Responses to the answer
			PLAYER_THEME_REWARD, //The reward + animations from the theme
			PLAYER_WRONG, //Responses and theme events if the player got the opportunity incorrect
			END_THEME //Player has gone through all opporutnities			
		} ;
		
		//Constants------------------------------------------------------
		
		//FLA Constants
		public const string COMPOUND_CARD_INSTANCE = "compoundWordBlending";
		public const string PUZZLE_LEFT_INSTANCE = "puzzlePieceLeftBlending";
		public const string PUZZLE_RIGHT_INSTANCE = "puzzlePieceRightBlending";
		public const string PUZZLE_PICTURES = "wordPictures";
		
		//Audio Word Constants
		public const string INITIAL_INSTRUCTION = "INITIAL_INSTRUCTION";
		public const string PROMPT = "PROMPT";
		public const string DETAILED_PROMPT = "DETAILED_PROMPT"; //word sent during the emerging prompt
		public const string CORRECT_REINFORCEMENT = "REINFORCEMENT";
		public const string INCORRECT_REINFORCEMENT = "INCORRECT_REINFORCEMENT";
		public const string PLAYER_INACTIVITY = "PLAYER_INACTIVITY"; //message sent when player has done nothing
		public const string SOLVE_FOR_PLAYER = "SOLVE_FOR_PLAYER"; //String sent 
		
		//Movie clip placements (All coordinates are percentages of the parent)
		private const float BOX_X_PERCENT = .5f; // Center of the box relative to the screen
		private const float BOX_Y_PERCENT = .3f;
		
		private const float ANSWER_PIECE_Y = .475f; //the height at which the answer pieces are placed. The answer pieces are placed based off of the center of rhte screen
		private const float ANSWER_SPACING = 35; //The spacing between the answer pieces
		
		//Timing constants
		private const float FADE_TIME = .35f;
		private const float DETAILED_PROMPT_TRANSITION_TIME = .5f;
		
		//Misc
		public const float MAGNIFY_SCALE_PRECENT = 1.25f; //When an object is highlighted and enlarged, the percent that it is scaled up
		public const float MAGNIFY_REVERT_PERCENT = .8f; //undoes the scaling from the highlight
		
		bool solveForPlayerBool = false; //If the game solved for the player, flags to move on to the next opportunity after "Try Again"
		
		//Game Data-------------------------------------------------------
		//Format = List of compound words
		//Compound words are in the form of a list with the full word first and the words that make up the word second and thrid
		private List<List<string>> compoundWords = new List<List<string>>()
		{
			new List<string>(){"BASEBALL", "BASE", "BALL"} ,
			new List<string>(){"BASKETBALL", "BASKET", "BALL"} ,
			new List<string>(){"BIRDCAGE", "BIRD", "CAGE"} ,
			new List<string>(){"BIRDHOUSE", "BIRD", "HOUSE"} ,
			new List<string>(){"BOATHOUSE", "BOAT", "HOUSE"} ,
			new List<string>(){"BOOKCASE", "BOOK", "CASE"} ,
			new List<string>(){"BOWTIE", "BOW", "TIE"} ,
			new List<string>(){"BOXCAR", "BOX", "CAR"} ,
			new List<string>(){"BULLHORN", "BULL", "HORN"} ,
			new List<string>(){"BUTTERCUP", "BUTTER", "CUP"} ,
			new List<string>(){"BUTTERFLY", "BUTTER", "FLY"} ,
			new List<string>(){"CHALKBOARD", "CHALK", "BOARD"} ,
			new List<string>(){"COWBOY", "COW", "BOY"} ,
			new List<string>(){"CUPCAKE", "CUP", "CAKE"} ,
			new List<string>(){"DOGHOUSE", "DOG", "HOUSE"} ,
			new List<string>(){"EARRING", "EAR", "RING"} ,
			new List<string>(){"FIREHOUSE", "FIRE", "HOUSE"} ,
			new List<string>(){"FIREMAN", "FIRE", "MAN"} ,
			new List<string>(){"FIREWOOD", "FIRE", "WOOD"} ,
			new List<string>(){"FISHBOWL", "FISH", "BOWL"} ,
			new List<string>(){"FOOTBALL", "FOOT", "BALL"} ,
			new List<string>(){"GOLDFISH", "GOLD", "FISH"} ,
			new List<string>(){"HOUSEBOAT", "HOUSE", "BOAT"} ,
			new List<string>(){"KEYHOLE", "KEY", "HOLE"} ,
			new List<string>(){"LADYBUG", "LADY", "BUG"} ,
			new List<string>(){"LIGHTHOUSE", "LIGHT", "HOUSE"} ,
			new List<string>(){"LIPSTICK", "LIP", "STICK"} ,
			new List<string>(){"MAILBOX", "MAIL", "BOX"} ,
			new List<string>(){"PANCAKE", "PAN", "CAKE"} ,
			new List<string>(){"PEANUT", "PEA", "NUT"} ,
			new List<string>(){"POCKETBOOK", "POCKET", "BOOK"} ,
			new List<string>(){"RAINBOW", "RAIN", "BOW"} ,
			new List<string>(){"RATTLESNAKE", "RATTLE", "SNAKE"} ,
			new List<string>(){"SANDPAPER", "SAND", "PAPER"} ,
			new List<string>(){"SAWHORSE", "SAW", "HORSE"} ,
			new List<string>(){"SKYSCRAPER", "SKY", "SCRAPER"} ,
			new List<string>(){"SNOWBALL", "SNOW", "BALL"} ,
			new List<string>(){"SNOWMAN", "SNOW", "MAN"} ,
			new List<string>(){"STARFISH", "STAR", "FISH"} ,
			new List<string>(){"SUNFLOWER", "SUN", "FLOWER"} ,
			new List<string>(){"SUNGLASSES", "SUN", "GLASSES"} ,
			new List<string>(){"SURFBOARD", "SURF", "BOARD"} ,
			new List<string>(){"TURTLENECK", "TURTLE", "NECK"} ,
			new List<string>(){"WALLPAPER", "WALL", "PAPER"} ,
			new List<string>(){"WHEELCHAIR", "WHEEL", "CHAIR"}			
		} ;
		
		//Random words to use as wrong options
		List<string> randomWords = new List<string>(){
			"APPLE", 
			"BABY", "BALL", "BASE", "BASKET", "BIRD", "BOARD", "BOAT", "BOOK", "BOW", "BOWL", "BOX", "BOY", "BUG", "BULL", "BUTTER",
			"CAGE", "CAKE", "CAR", "CARROT", "CASE", "CAT", "CHAIR", "CHALK", "CLOUD", "COW", "CUP",
			"DOG",
			"EAR",
			"FIRE", "FISH", "FLY", "FOOT",
			"GLASSES", "GOLD",
			"HOLE", "HORN", "HORSE", "HOUSE",
			"LADY", "LEAF", "LIGHT", "LION", "LIP",
			"MAIL", "MAN", 
			"NECK", "NUT",
			"PAN","PAPER", "PEA", "PEN", "PENCIL", "POCKET",
			"RABBIT", "RAIN", "RATTLE", "RING",
			"SAND", "SAW", "SCRAPER", "SKY", "SNOW", "STAR", "STICK", "SUN", "SURF",
			"TIE", "TURTLE", 
			"WALL", "WHEEL", "WOOD"
		} ;
		
		
		//GameManagement
		private SegmentingCompoundWordsStates currentState = SegmentingCompoundWordsStates.INTRO;
		
		//Word/Answer Variables
		public List<string> currentCompoundWord; //The compound word with info on the words that make it up
		public List<List<string>> compoundWordOpportunityBank; //at beginning of skill, compound words coppied into here and are removed as they are used
		public List<string> randomWordBank; //at beginning of skill, random words coppied into here and are removed as they are used
		public string givenWord; //The word given as half of the answer
		public string correctWordAnswer; //The correct answer the player will have to choose
		private int numberOfPuzzlePieces; //The number of puzzle pieces per opportunity
		
		private List<FilterMovieClip> answerOptionsPuzzlePieces = new List<FilterMovieClip>(); //The answers for the player to choose from
		public List<FilterMovieClip> AnswerOptionsPuzzlePieces { get{  return answerOptionsPuzzlePieces; } }
		
		private List<FilterMovieClip> highlightedFilterMovieClips = new List<FilterMovieClip>(); //Tracks which FMC's are highlighted
		
		//Visuals Variables - Movieclips and Containers
		public DisplayObjectContainer backgroundContainer; //Table Background object
		private MovieClip answerBox; //Contains the box, the compound word card, the dotted outline, and the hint puzzle piece
		public FilterMovieClip compoundWordCard; //The card for the compound word prompt
		
		public FilterMovieClip puzzlePieceLeft;
		MovieClip puzzlePieceLeftPictures; //the pictures are childed to the puzzle pieces so I am saving a reference to that movieclip
		
		public FilterMovieClip puzzlePieceRight;
		MovieClip puzzlePieceRightPictures;
		
		//Misc
		private int stepCounter = 0; //Counts the steps in a linear process. Used with emergingPrompt, inactivity counts, and solving for player
		public int tutorialSetupCounter = 0; //Keeps track of which part of the tutorial to set up
		private Vector2 detailedPromptCardDestination = new Vector2();
		
		private FilterMovieClip draggedPuzzlePiece;
		private Vector2 draggedPuzzlePieceStartLocation; //the location of the puzzle piece when it is clicked so that it can go back if not on answer spot
		private float draggedScale;
		
		private float baseScale;
		private float refWidth;
		private float refHeight;
		
		private float resizedScale = 0.68f;
		private float answerScale = 0.675f;
		
		private float answerRowScaleModifier = 0f;
		
		private float answerBoxWidth;
		private float answerBoxHeight;
		private Vector2 answerBox_PlatformOffset = new Vector2 (100, 85);
		
		private float compoundWordOffset = 0f;
		private float rightHintOffset = 0f;
		
		private float answerPuzzlePieceScaler;
		private float initialOffsetX = 0;
//		private float answerBoxAnswerOffset_X = 0f;
//		private float answerBoxAnswerOffset_RightSide_X = 0f;
		private float answerSpacingMultiplier = 1f;
		
		private float answersStartingX_Offset_Tutorial = 125;
		private float answersStartingX_Offset_Emerging = 125;
		private float answersStartingX_Offset_Developing = 75;
		private float answersStartingX_Offset_Developed = 25;
		
		private float answersOffset_Emerging_Left = 20f;
		private float answersOffset_Developing_Left = 25f;
		private float answersOffset_Developed_Left = 20f;
		
		private float answerRowOffsetY = 0f;
		private float additionalSpacing = 0f;
		
		private Platforms selectedPlatform;
		
		private float tabletScaler = 0f;
//		private Vector2 compoundPlatformOffset = new Vector2();
//		private float hintPlatformOffset = 0f;
		private float answerScale_WIN = 0f;
		
//		private float answerWord_Scale_ANDROID = 1f;
		
		
		private TimerObject glowTimer;
		
		private new SegmentingCompoundWordsTutorial tutorial_mc;
		
		private float answerRowPieceScale = 1f;
		private bool leftSide;
		
		public SegmentingCompoundWords( SkillLevel level, SessionInfo  currentSession ) : base( level, currentSession )
		{
			host = "Cami";
			BACKGROUND = "Background";
			ARTIFACT = "Artifact";
			
			Init(null);
		}
		
		public override void Dispose ()
		{
			base.Dispose();
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			if(tutorial_mc != null)
				tutorial_mc.Dispose();
			foreach(FilterMovieClip fmc in answerOptionsPuzzlePieces)
			{
				fmc.Destroy();
			}
			compoundWordCard.Destroy();
			puzzlePieceLeft.Destroy();
			puzzlePieceRight.Destroy();
			if(draggedPuzzlePiece != null)
				draggedPuzzlePiece.Destroy();
			if(glowTimer != null)
				glowTimer.Unload();
			if(stage.hasEventListener(MouseEvent.MOUSE_UP))
				stage.removeEventListener( MouseEvent.MOUSE_UP, OnPieceUp );
		}
		
		private void PreCacheAssets()
		{
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle("SegmentingCompoundWords", OnAssetsLoaded));
		}
		
		void OnAssetsLoaded(AssetBundle bundle)
		{
			if( bundle != null )
			{
				Init( null );
			}
		}
		
		public override void Init (MovieClip parent)
		{
			base.Init (parent);
			
			//MovieClip backgroundImage = MovieClipFactory.CreateSegmentingCompoundWordsBackground();
			
			refWidth = MainUI.STAGE_WIDTH; // backgroundImage.width;
			refHeight = MainUI.STAGE_HEIGHT; // backgroundImage.height;
			baseScale = ((float)Screen.height / refHeight);
			
			selectedPlatform = PlatformUtils.GetPlatform ();
			
			initPlatformValues ();
			
			//Add our OnAudioWordComplete to the delegate
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			//Setup Background
			backgroundContainer = new DisplayObjectContainer();
			//backgroundContainer.scaleX = backgroundContainer.scaleY = baseScale;
			//backgroundContainer.x = (Screen.width - (refWidth*baseScale))/2;
			addChild(backgroundContainer);
			
			//add and center background image
			//backgroundContainer.addChild(backgroundImage);
			//backgroundImage.x = 0;
			//backgroundImage.y = 0;
			//if(_level == SkillLevel.Tutorial)
			//	backgroundImage.alpha = 0;
			
			//Setup Answer Box
			//Setup the Container
			answerBox = MovieClipFactory.CreateEmptyMovieClip();
			answerBox.gotoAndStop(2); //the first frame is segmenting
			//answerBox.width = 100f;
			
			//answerBox.x = (float)Screen.width/2;
			//answerBox.y = 
			answerBox.scaleX = answerBox.scaleY = baseScale;
			
			answerBox.width = answerBoxWidth ;
			answerBox.height = answerBoxHeight;
			
			
			backgroundContainer.addChild(answerBox); //add to background so all an be faded out together
			//I don't know why I have to do this, but it updates the size of the background image
			//So these two lines aren't ENTIRELY pointless. Just stupid. I don't know, just leave 'em.
			backgroundContainer.removeChild(answerBox);
			backgroundContainer.addChild(answerBox);
			
			
			
			
			MovieClip answerBoxItems = MovieClipFactory.CreateSegmentingCompoundWordsBoxSegmentingItems();
			
			Debug.Log("Num of box children: " + answerBoxItems.numChildren);
			for(int i = 0 ; i < answerBoxItems.numChildren ; i++)
			{
				Debug.Log("Child " + i + ": " + answerBoxItems.getChildAt(i).name);	
			}
			
			//Get the compound word from the answer box
			MovieClip tempMCRef = answerBoxItems.getChildByName<MovieClip>(COMPOUND_CARD_INSTANCE);
			
			Vector2 tempLocalPosition = new Vector2(tempMCRef.x, tempMCRef.y); //record position to reset target's local position once added to FMC
			tempLocalPosition.x += compoundWordOffset;
			compoundWordCard = new FilterMovieClip(tempMCRef); //Setup the compound word
			answerBox.addChild(compoundWordCard); //add the new filtered movie clip
			compoundWordCard.x = tempLocalPosition.x; //Set the FMC to the card's orignal position
			compoundWordCard.x -= initialOffsetX;
			compoundWordCard.y = tempLocalPosition.y;
			compoundWordCard.Target.x = 0; //zero out the target's local position
			compoundWordCard.Target.y = 0;
			compoundWordCard.Target.getChildByName<MovieClip>(COMPOUND_CARD_INSTANCE).gotoAndStop(1);
			compoundWordCard.alpha = 0;
			
			compoundWordCard.scaleX = compoundWordCard.scaleY = tabletScaler;
			
			
			//Setup puzzle pieces - get puzzle pieces from the box, get reference to picture mc child, set frame, hide them
			tempMCRef = answerBoxItems.getChildByName<MovieClip>(PUZZLE_LEFT_INSTANCE);
			tempLocalPosition = new Vector2(tempMCRef.x, tempMCRef.y); //record position to reset target's local position once added to FMC
			puzzlePieceLeft = new FilterMovieClip(tempMCRef);
			puzzlePieceLeftPictures = puzzlePieceLeft.Target.getChildByName<MovieClip>(PUZZLE_LEFT_INSTANCE).getChildByName<MovieClip>(PUZZLE_PICTURES); //Get Reference
			answerBox.addChild(puzzlePieceLeft);
			puzzlePieceLeft.x = tempLocalPosition.x; //Set the FMC to the card's orignal position
			puzzlePieceLeft.y = tempLocalPosition.y;
			puzzlePieceLeft.Target.x = 0; //zero out the target's local position
			puzzlePieceLeft.Target.y = 0;
			puzzlePieceLeft.width = compoundWordCard.Target.width; //Use the compound word card for consistency since other pieces are different sizes
			puzzlePieceLeft.height = compoundWordCard.Target.height;
			puzzlePieceLeft.scaleX = puzzlePieceLeft.scaleY = compoundWordCard.scaleX;
			puzzlePieceLeftPictures.gotoAndStop(1); //Stop the movie clip from playing
			puzzlePieceLeft.alpha = 0;
			
			tempMCRef = answerBoxItems.getChildByName<MovieClip>(PUZZLE_RIGHT_INSTANCE);
			tempLocalPosition = new Vector2(tempMCRef.x, tempMCRef.y); //record position to reset target's local position once added to FMC
			tempLocalPosition.x -= rightHintOffset;
			
			puzzlePieceRight = new FilterMovieClip(tempMCRef);
			puzzlePieceRightPictures = puzzlePieceRight.Target.getChildByName<MovieClip>(PUZZLE_RIGHT_INSTANCE).getChildByName<MovieClip>(PUZZLE_PICTURES);
			answerBox.addChild(puzzlePieceRight);
			puzzlePieceRight.x = tempLocalPosition.x; //Set the FMC to the card's orignal position
			puzzlePieceRight.y = tempLocalPosition.y;
			puzzlePieceRight.Target.x = 0; //zero out the target's local position
			puzzlePieceRight.Target.y = 0;
			puzzlePieceRight.width = compoundWordCard.Target.width; //Use the compound word card for consistency since other pieces are different sizes
			puzzlePieceRight.height = compoundWordCard.Target.height;
			puzzlePieceRight.scaleX = puzzlePieceRight.scaleY = compoundWordCard.scaleX;
			puzzlePieceRightPictures.gotoAndStop(1);
			puzzlePieceRight.alpha = 0;
			
			backgroundContainer.alpha = 0; //Hide background
			
			/*
			answerBox.width = compoundWordCard.width + puzzlePieceLeft.width + puzzlePieceRight.width;
			Debug.Log ("Answer box width: " + answerBox.width + " UI Width: " + MainUI.STAGE_WIDTH);
			answerBox.x = (MainUI.STAGE_WIDTH * BOX_X_PERCENT) - (answerBox.width * .5f);
			*/
			//answerBox.y = (float)Screen.height * BOX_Y_PERCENT - answerBox.height * .5f;
			
			//If tutorial, load tutorial and set it's reference
			tutorial_mc = new SegmentingCompoundWordsTutorial();
			tutorial_mc.skillRef = this;
			
			//Set state to theme intro
			currentState = SegmentingCompoundWordsStates.INTRO;
			
			//Copy lists of compound words and random words
			compoundWordOpportunityBank = new List<List<string>>(compoundWords);
			randomWordBank = new List<string>(randomWords);
			
			//Set the number of opportunities
			if(_level == SkillLevel.Tutorial) totalOpportunities = 1;
			else totalOpportunities = 10;
			
			//totalOpportunities = 1;
			
			//Set the number of options
			switch(_level)
			{
			case SkillLevel.Tutorial: 
				numberOfPuzzlePieces = 1;
				break;
			case SkillLevel.Emerging:
				numberOfPuzzlePieces = 3;
				break;
			case SkillLevel.Developing:
				numberOfPuzzlePieces = 4;
				break;
			case SkillLevel.Developed: //Developed and completed both have 5 answers per opportunity
			case SkillLevel.Completed:
				numberOfPuzzlePieces = 5;
				break;
			}
			
			if(resumingSkill)
			{
				parseSessionData(currentSession.sessionData);
				//	nextOpportunity();
			}
			
			//Set timer length
			totalTime = 5;
			
			Resize( MainUI.STAGE_WIDTH, MainUI.STAGE_HEIGHT );
			
		}
		
		void initPlatformValues () {
			
			
			tabletScaler = 1.4f;
			resizedScale = 0.68f;
			
			answerBoxWidth = 1000f;
			answerBoxHeight = 329.65f;
			
			answerBox_PlatformOffset.x = 65f;
			answerBox_PlatformOffset.y = 120f;
			
			switch (selectedPlatform) {
				
			case Platforms.WIN:
				initialOffsetX = 150f;
				answerPuzzlePieceScaler = 0.95f;
				
				answerRowOffsetY = 150f;
				
				answersStartingX_Offset_Emerging = 30f;
				answersStartingX_Offset_Developing = 10f;
				answersStartingX_Offset_Developed = 40f;
				
				answersOffset_Developed_Left = 30f;
				
				additionalSpacing = 120f;
				
				rightHintOffset = -115f;
				
				answerScale_WIN = answerPuzzlePieceScaler;
				
				compoundWordOffset = -20f;
				
				break;
				
			case Platforms.IOS:
				answerSpacingMultiplier = 3f;
				
				resizedScale = 1f;
				answerRowScaleModifier = -0.3f;
				
				tabletScaler = 1f;
				answerBoxWidth = 1000f;
				
				answerBox_PlatformOffset.x = 222f;
				answerBox_PlatformOffset.y = 160f;
				
				//compoundWordOffset = -65f;
				rightHintOffset = 2f;
				
				answerPuzzlePieceScaler = 1f;
				
				answerRowOffsetY = -70f;
				
				answersStartingX_Offset_Tutorial = 420f;
				answersStartingX_Offset_Emerging = 360f;
				answersStartingX_Offset_Developing = 330f;
				answersStartingX_Offset_Developed = 270f;
				
				answersOffset_Emerging_Left = -100f;
				answersOffset_Developing_Left = -80f;
				answersOffset_Developed_Left = -100f;
				
				break;
				
			case Platforms.IOSRETINA:
				
				answerSpacingMultiplier = 1f;
				
				resizedScale = 1f;
				//answerRowScaleModifier = 0.25f;
				
				tabletScaler = 0.99f;
				answerBoxWidth = 1000f;
				
				
				answerBox_PlatformOffset.x = -20f;
				answerBox_PlatformOffset.y = 326f;
				
				//compoundWordOffset = -65f;
				rightHintOffset = 2f;
				
				answerPuzzlePieceScaler = 1f;
				
				answerRowOffsetY = 380f;
				
				answersStartingX_Offset_Tutorial = -100f;
				answersStartingX_Offset_Emerging = -40f;
				answersStartingX_Offset_Developing = 120f;
				answersStartingX_Offset_Developed = 250f;
				
				answersOffset_Developing_Left = -20f;
				answersOffset_Developed_Left = 20f;
				
				additionalSpacing = 120f;
				
				break;
				
			case Platforms.ANDROID:
				
				answerSpacingMultiplier = -0.3f;
				
				resizedScale = 1f;
				answerRowScaleModifier = -0.5f;
				tabletScaler = 1.25f;
				answerBoxWidth = 1800f;
				answerBox_PlatformOffset.x = -270f;
				answerBox_PlatformOffset.y = 96;
				
				compoundWordOffset = -32f;
				rightHintOffset = -60f;
				
				answerPuzzlePieceScaler = tabletScaler;
				
				answerRowOffsetY = -72f;
				
				answersStartingX_Offset_Tutorial = 300;
				answersStartingX_Offset_Emerging = 400f; // 420f
				answersStartingX_Offset_Developing = 450f; // 450f
				answersStartingX_Offset_Developed = 470f; //600
				
				answersOffset_Emerging_Left = -100f;
				answersOffset_Developing_Left = -100f;
				answersOffset_Developed_Left = -100f;
				
				additionalSpacing = 120f;
				break;
				
				
			}
			
			answerScale = resizedScale - 0.005f;
			
		}
		
		/// <summary>
		/// Resize the specified width and height.
		/// </summary>
		/// <param name='width'>
		/// Width.
		/// </param>
		/// <param name='height'>
		/// Height.
		/// </param>
		public override void Resize( int width, int height )
		{
			this.width = width;
			this.height = height;
			
			backgroundContainer.width = width;
			backgroundContainer.height = height;
			
			backgroundContainer.scaleX = backgroundContainer.scaleY = height/backgroundContainer.height;
			
			backgroundContainer.y = 0;
			backgroundContainer.x = (width - (backgroundContainer.width*backgroundContainer.scaleX))/2;
			
			//answerBox.scaleX = answerBox.scaleY = 0.8f;//height/backgroundContainer.height;
			
			answerBox.ScaleCentered( resizedScale );
			
			answerBox.x = ((width - (answerBox.width*answerBox.scaleX))/2) - answerBox_PlatformOffset.x; // 100
			answerBox.y = answerBox_PlatformOffset.y;
			
			//			cardContainer_mc.FitToParent();
		}
		
		public void ChangeState(SegmentingCompoundWordsStates newState)
		{
			Debug.Log("STATE CHANGE: " + currentState.ToString() + " =to=> " + newState.ToString());
			
			ExitState(currentState, newState);			
			
			EnterState(currentState, newState);
			
			//Change State
			currentState = newState;
		}
		
		private void ExitState(SegmentingCompoundWordsStates oldState, SegmentingCompoundWordsStates newState)
		{
			switch(oldState)
			{
			case SegmentingCompoundWordsStates.INTRO:
			case SegmentingCompoundWordsStates.TUTORIAL:
			case SegmentingCompoundWordsStates.INITIAL_INSTRUCTION:
			case SegmentingCompoundWordsStates.SETUP_AND_PROMPT:
				break;
				
			case SegmentingCompoundWordsStates.PLAYER_INTERACTION:
			case SegmentingCompoundWordsStates.PLAYER_INTERACTING:
				//Interaction over, disable interactions
				foreach(FilterMovieClip fmc in answerOptionsPuzzlePieces)
				{
					fmc.removeEventListener( MouseEvent.MOUSE_DOWN, OnPieceDown );
				}
				compoundWordCard.removeEventListener(MouseEvent.MOUSE_DOWN, OnBoxPieceDown);
				puzzlePieceLeft.removeEventListener(MouseEvent.MOUSE_DOWN, OnBoxPieceDown);
				puzzlePieceRight.removeEventListener(MouseEvent.MOUSE_DOWN, OnBoxPieceDown);
				
				if(newState != SegmentingCompoundWordsStates.PLAYER_INTERACTING)
					MarkInstructionStart();
				stopTimer();				
				break;
				
			case SegmentingCompoundWordsStates.SOLVE_FOR_PLAYER:
			case SegmentingCompoundWordsStates.PLAYER_REINFORCEMENT:
			case SegmentingCompoundWordsStates.PLAYER_THEME_REWARD:
			case SegmentingCompoundWordsStates.PLAYER_WRONG:
			case SegmentingCompoundWordsStates.END_THEME:
				break;
			}
			
		}
		
		private void EnterState(SegmentingCompoundWordsStates oldState, SegmentingCompoundWordsStates newState)
		{
			switch(newState)
			{
			case SegmentingCompoundWordsStates.INTRO:	
				//Game is set up to be in the Intro state. No enter state.
				break;
				
			case SegmentingCompoundWordsStates.TUTORIAL:	
				//Show background
				FadeInContainer();
				//Start Tutorial
				tutorial_mc.OnStart();
				break;
				
			case SegmentingCompoundWordsStates.INITIAL_INSTRUCTION:
				
				//OnAudioWordComplete( INITIAL_INSTRUCTION );
				
				
				if( !currentSession.skipIntro )
				{
					//String together the audio prompts
					SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
					AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Story Introduction Audio/1D-story-introduction" );
					bundle.AddClip(clip, INITIAL_INSTRUCTION, clip.length);
					SoundEngine.Instance.PlayBundle( bundle );
				}
				else
					OnAudioWordComplete( INITIAL_INSTRUCTION );
				
				
				//Fade in Background
				FadeInContainer();
				
				break;
				
			case SegmentingCompoundWordsStates.SETUP_AND_PROMPT:		
				
				SetUpOpportunity();
				FadeInContainer();
				DetailedWordPrompt();
				
				break;
				
			case SegmentingCompoundWordsStates.PLAYER_INTERACTION:	
				//add event listeners and enable interactions
				foreach(FilterMovieClip fmc in answerOptionsPuzzlePieces)
				{
					if(!fmc.hasEventListener(MouseEvent.MOUSE_DOWN))
						fmc.addEventListener( MouseEvent.MOUSE_DOWN, OnPieceDown );
				}
				if(!compoundWordCard.hasEventListener(MouseEvent.MOUSE_DOWN))
					compoundWordCard.addEventListener(MouseEvent.MOUSE_DOWN, OnBoxPieceDown);
				if(correctWordAnswer == currentCompoundWord[1]) //Left puzzle piece is the answer, right is the hint
				{
					if(!puzzlePieceRight.hasEventListener(MouseEvent.MOUSE_DOWN))
						puzzlePieceRight.addEventListener(MouseEvent.MOUSE_DOWN, OnBoxPieceDown);
				}
				else
				{
					if(!puzzlePieceLeft.hasEventListener(MouseEvent.MOUSE_DOWN))
						puzzlePieceLeft.addEventListener(MouseEvent.MOUSE_DOWN, OnBoxPieceDown);
				}
				if(_level != SkillLevel.Tutorial)
					MarkInstructionEnd();
				if(oldState != SegmentingCompoundWordsStates.PLAYER_INTERACTING && _level != SkillLevel.Tutorial)
					startTimer(); //Starts inactivty timer
				break;
				
			case SegmentingCompoundWordsStates.PLAYER_INTERACTING:
				SoundEngine.Instance.SendCancelToken();
				timeoutCount = 0;
				stopTimer();
				if(_level == SkillLevel.Tutorial)
				{
					tutorial_mc.InactivityTimerOff();
					tutorial_mc.inactivityCounter = 0;
				}
				break;
				
			case SegmentingCompoundWordsStates.SOLVE_FOR_PLAYER:
				SolveForPlayer();
				break;
				
			case SegmentingCompoundWordsStates.PLAYER_REINFORCEMENT:	
				//Check the dragged puzzle piece
				if(draggedPuzzlePiece.name == correctWordAnswer) //Player is correct and should be rewarded
				{
					//Mark opportunity answered, correct and complete
					if(opportunityAnswered == false || _level == SkillLevel.Tutorial) opportunityCorrect = true;
					opportunityAnswered = true;
					
					opportunityComplete = true;
					
					//OnAudioWordComplete will respond to CLICK_CORRECT and carry on the rest once sound is done.
					if(oldState != SegmentingCompoundWordsStates.SOLVE_FOR_PLAYER) PlayCorrectSound();
					else PlayCorrectAnswerReinforcement(); //Problem was solved for the player so it must be treated as incorrect
				}
				else //Player is wrong, critisize them
				{
					//Mark opportunity answered and incorrect
					opportunityCorrect = false;
					opportunityAnswered = true;
					
					PlayIncorrectSound(); //OnAudioWordComplete will respond to CLICK_INCORRECT and carry on the rest once sound is done.
				}
				break;
				
			case SegmentingCompoundWordsStates.PLAYER_THEME_REWARD:
				if(opportunityCorrect) //opportunity was answered correctly the first time, log it, and give the player a reward
				{
					//Fade Out Background
					FadeOutContainer();
					
					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
				}
				
				else //opportunity was not answered correctly the first time, log it and go to the next opportunity
				{
					//if(currentOpportunity == totalOpportunities)
					FadeOutContainer();
					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
					return;
				}
				break;
				
			case SegmentingCompoundWordsStates.PLAYER_WRONG:	
				
				break;
				
			case SegmentingCompoundWordsStates.END_THEME:	
				tallyAnswers();
				break;
			}
		}
		
		public void SetUpOpportunity()
		{
			Debug.Log("SETTING UP OPPORTUNITY!");
			//clear out the options
			foreach(FilterMovieClip fmc in answerOptionsPuzzlePieces)
			{
				fmc.Destroy();
				Tweener.removeTweens(fmc);
				backgroundContainer.removeChild(fmc);
			}
			if( draggedPuzzlePiece != null)
			{
				draggedPuzzlePiece.Destroy();
				if(draggedPuzzlePiece.parent != null)
					draggedPuzzlePiece.parent.removeChild(draggedPuzzlePiece);
				draggedPuzzlePiece = null;
			}
			answerOptionsPuzzlePieces.Clear ();
			
			//create random variable
			int tempRandom;
			
			//if not tutorial, randomly choose a word
			if(_level != SkillLevel.Tutorial)
			{
				int r = Random.Range(0, compoundWordOpportunityBank.Count);
				if(r == compoundWordOpportunityBank.Count) r--;
				if(resumingSkill)
				{
					r = 0;
					resumingSkill = false;
				}
				currentCompoundWord = compoundWordOpportunityBank[r];
				compoundWordOpportunityBank.Remove(currentCompoundWord);
				compoundWordCard.Target.getChildByName<MovieClip>(COMPOUND_CARD_INSTANCE).gotoAndStop(currentCompoundWord[0]);
				tempRandom = Random.Range(1, 3); //Get a number 1 or 2
				if(tempRandom == 3) tempRandom--;
				
			}
			else if(tutorialSetupCounter == 0) //first half of tutorial, setup doghouse
			{
				//Word is doghouse and dog is given
				foreach( List<string> compoundWordList in compoundWordOpportunityBank)
				{
					//Find doghouse in list and remove it
					if(compoundWordList[0] == "DOGHOUSE")
					{
						currentCompoundWord = compoundWordList;
						compoundWordCard.Target.getChildByName<MovieClip>(COMPOUND_CARD_INSTANCE).gotoAndStop(currentCompoundWord[0]);
						//compoundWordCard.Target.getChildByName<MovieClip>(COMPOUND_CARD_INSTANCE).alpha = 0f;
						break;
					}
					
				}

				//puzzlePieceLeft.alpha = 0f;
				//puzzlePieceRight.alpha = 0f;
				
				
				//Set answer to house through manual manipulation of the random variable
				tempRandom = 2;
				
				tutorialSetupCounter = 1;
			}
			else //Must be on second half of the tutorial, setup up birdcage
			{
				//Word is birdcage and bird is given
				foreach( List<string> compoundWordList in compoundWordOpportunityBank)
				{
					//Find doghouse in list and remove it
					if(compoundWordList[0] == "BIRDCAGE")
					{
						currentCompoundWord = compoundWordList;
						compoundWordCard.Target.getChildByName<MovieClip>(COMPOUND_CARD_INSTANCE).gotoAndStop(currentCompoundWord[0]);
						break;
					}
				}
				tutorialSetupCounter = 2;
				
				//Set answer to house through manual manipulation of the random variable
				tempRandom = 2;
			}
			
			//tempRandom = 1;
			
			leftSide = false;
			if (tempRandom == 1) {
				leftSide = true;
			}
			
			Debug.Log("The current Compound Word is: " + tempRandom + " " + currentCompoundWord[0] + " ::: " + currentCompoundWord[1] + " - " + currentCompoundWord[2]);

			if(tempRandom == 1) //First word is the answer, second word is the hint
			{
				correctWordAnswer = currentCompoundWord[1];
				puzzlePieceLeft.alpha = 0; //hide the answer puzzle piece
				
				givenWord = currentCompoundWord[2];
				puzzlePieceRightPictures.gotoAndStop(currentCompoundWord[2]);
				puzzlePieceRight.name = currentCompoundWord[2];
				puzzlePieceRight.alpha = 1; //show the hint puzzle piece
			}
			else //Second word is the answer, first word is the hint
			{
				correctWordAnswer = currentCompoundWord[2];
				puzzlePieceRight.alpha = 0; //hide the answer puzzle piece
				
				givenWord = currentCompoundWord[1];
				puzzlePieceLeftPictures.gotoAndStop(currentCompoundWord[1]);
				puzzlePieceLeft.name = currentCompoundWord[1];
				puzzlePieceLeft.alpha = 1; //show the hint puzzle piece

			}
			
			
			//Set the compound word card
			compoundWordCard.Target.getChildByName<MovieClip>(COMPOUND_CARD_INSTANCE).gotoAndStop(currentCompoundWord[0]); //Set the appropriate picture for compound word
			compoundWordCard.name = currentCompoundWord[0];
			compoundWordCard.alpha = 0; //Make compound word card invisible, the detailed prompt will handle the reveal
			
			//Debug.LogError ("Set up opp");
			if(_level == SkillLevel.Tutorial)
			{
				puzzlePieceLeft.alpha = 0;
				puzzlePieceRight.alpha = 0;
				//puzzlePieceLeft.alpha = 1;
				//compoundWordCard.alpha = 1;
			}
			
			//Set up the answer pieces
			//Calculate the initial x position for the pieces - get width of card based on random outcome
			//total width of placement, using compound word card as an average width
			float totalWidth = (numberOfPuzzlePieces * (((tempRandom == 1) ? puzzlePieceLeft : puzzlePieceRight).width + (ANSWER_SPACING * answerSpacingMultiplier))) - (ANSWER_SPACING * answerSpacingMultiplier);
			
			//Cut the width of the list so it can't spill off the edges, mostly needed for 4:3
			float maxWidth = ((float)MainUI.STAGE_WIDTH / baseScale) - (ANSWER_SPACING * answerSpacingMultiplier); //((float)Screen.width / baseScale) - ANSWER_SPACING;
			if(totalWidth > maxWidth)
			{
				draggedScale = maxWidth / totalWidth;
				totalWidth = maxWidth;
			}
			else
				draggedScale = 0.7f;
			
			//Debug.Log ("Dragged Scale: " + draggedScale);
			
			//the starting position from the center of the card
			//Note - I use the compound word card as a width reference because the puzzle pieces have different width due to off center childed movie clips to line up pictures
			float startingX = (refWidth * .5f) - (totalWidth * .5f);
			
			// The tablets seem to offset slightly when using the left pieces
			
			switch(_level)
			{
			case SkillLevel.Tutorial: 
				
				startingX -= answersStartingX_Offset_Tutorial;
				break;
			case SkillLevel.Emerging:
				
				startingX -= answersStartingX_Offset_Emerging;
				
				if (tempRandom == 1) {
					
					startingX += answersOffset_Emerging_Left;
				}
				
				
				
				break;
			case SkillLevel.Developing:
				
				startingX -= answersStartingX_Offset_Developing;
				
				if (tempRandom == 1) {
					
					startingX += answersOffset_Developing_Left;
				}
				
				
				break;
			case SkillLevel.Developed: //Developed and completed both have 5 answers per opportunity
			case SkillLevel.Completed:
				
				startingX -= answersStartingX_Offset_Developed;
				
				if (tempRandom == 1) {
					
					startingX += answersOffset_Developed_Left;
				}
				
				break;
			}
			
			//startingX -= 20f; // This is dependent if on left or right pieces
			
			//Create  a selection of words with the correct answer and random options
			List<string> answerWords = new List<string>();
			answerWords.Add(correctWordAnswer); //add correct answer
			
			int randomIndex;
			while(answerWords.Count < numberOfPuzzlePieces)
			{
				randomIndex = Random.Range(0, randomWordBank.Count);
				if(randomIndex == randomWordBank.Count) randomIndex--;
				if(!answerWords.Contains(randomWordBank[randomIndex]) &&
				   givenWord != randomWordBank[randomIndex]) //the random word has not been used in this opportunity
				{
					answerWords.Add (randomWordBank[randomIndex]);
				}
			}
			
			answerWords.Shuffle();
			
			FilterMovieClip tempFilterMovieClip;
			float xTally = 0; //the running total to add to the puzzle piece positions
			
			float adjustedScale = baseScale;
			
			if (selectedPlatform == Platforms.IOSRETINA || selectedPlatform == Platforms.WIN) {
				adjustedScale = answerPuzzlePieceScaler;
			}
			
			if (tempRandom == 1) {
				//adjustedScale -= 0.05f;
			}
			
			adjustedScale -= answerRowScaleModifier;
			answerRowPieceScale = adjustedScale;
			
			//create and place a puzzle piece for each
			for(int i = 0 ; i < answerWords.Count ; ++i)
			{
				//Create a puzzle piece
				if(tempRandom ==1)
				{
					tempFilterMovieClip = new FilterMovieClip(MovieClipFactory.CreateSegmentingCompoundWordsPuzzlePieceLeft());
					//Set the picture
					tempFilterMovieClip.Target.getChildByName<MovieClip>(PUZZLE_LEFT_INSTANCE).getChildByName<MovieClip>("wordPictures").gotoAndStop(answerWords[i]);
				}
				else
				{
					tempFilterMovieClip = new FilterMovieClip(MovieClipFactory.CreateSegmentingCompoundWordsPuzzlePieceRight());
					//Set the picture
					tempFilterMovieClip.Target.getChildByName<MovieClip>(PUZZLE_RIGHT_INSTANCE).getChildByName<MovieClip>("wordPictures").gotoAndStop(answerWords[i]);
				}
				
				//set the name to reference later
				tempFilterMovieClip.name = answerWords[i];
				
				//Place the piece
				tempFilterMovieClip.scaleX = tempFilterMovieClip.scaleY = adjustedScale;
				tempFilterMovieClip.x =  startingX + xTally;
				tempFilterMovieClip.y = (refHeight * ANSWER_PIECE_Y) - (compoundWordCard.Target.height * .5f) + answerRowOffsetY;
				xTally += (compoundWordCard.Target.width + (ANSWER_SPACING * answerSpacingMultiplier)) * draggedScale + additionalSpacing;
				
				//Add the filters to a list and add them to the background
				answerOptionsPuzzlePieces.Add (tempFilterMovieClip);
				backgroundContainer.addChild(tempFilterMovieClip);
				
				//if(_level != SkillLevel.Tutorial)
				tempFilterMovieClip.alpha = 0;
			}
		}
		
		public override void nextOpportunity()
		{
			//If skill is done, end theme
			if(_currentOpportunity == totalOpportunities) //Player has finished all opportunities, end theme
			{
				ChangeState(SegmentingCompoundWordsStates.END_THEME);
				return;
			}
			else //otherwise, setup next opportunity
			{
				//Reset opportunity variables
				opportunityComplete = false;
				opportunityCorrect = false;
				opportunityAnswered = false;
				
				dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_START, true, false) );
				ChangeState(SegmentingCompoundWordsStates.SETUP_AND_PROMPT);
				
				AddNewOppData( audioNames.IndexOf( promptString() ) );
			}
			
			currentOpportunity++;
		}
		
		public void setObjectData()
		{
			Vector2 point = compoundWordCard.parent.localToGlobal(new Vector2( compoundWordCard.x, compoundWordCard.y ));
			AddNewObjectData( formatObjectData( "Box:" + compoundWordCard.name, point ) );
			
			if( correctWordAnswer == currentCompoundWord[1] )
			{
				point = puzzlePieceRight.parent.localToGlobal(new Vector2( puzzlePieceRight.x, puzzlePieceRight.y ));
				AddNewObjectData( formatObjectData( "Box:" + puzzlePieceRight.name, point ) );
			}
			else
			{
				point = puzzlePieceLeft.parent.localToGlobal(new Vector2( puzzlePieceLeft.x, puzzlePieceLeft.y ));
				AddNewObjectData( formatObjectData( "Box:" + puzzlePieceLeft.name, point ) );
			}
			
			foreach( FilterMovieClip fmc in answerOptionsPuzzlePieces )
			{
				point = fmc.parent.localToGlobal( new Vector2( fmc.x, fmc.y ) );
				AddNewObjectData( formatObjectData( fmc.name, point, fmc.name == correctWordAnswer ) );
			}
		}
		
		public override void OnAudioWordComplete( string word )
		{
			switch(word)
			{
			case THEME_INTRO: //The theme has finished its intro
				//Change State to Initial Instruction, or if in tutorial, change to tutorial
				if(_level != SkillLevel.Tutorial) ChangeState(SegmentingCompoundWordsStates.INITIAL_INSTRUCTION);
				else ChangeState(SegmentingCompoundWordsStates.TUTORIAL);
				break;
				
			case INITIAL_INSTRUCTION: //The intro for the specific skill is complete
				//Start next opportunity which will change the state to PROMPT 
				//nextOpportunity();
				if(_level == SkillLevel.Tutorial)
				{
					tutorial_mc.View.visible = true;
					tutorial_mc.OnStart();
				}
				else
				{
					nextOpportunity();
				}
				break;
				
			case DETAILED_PROMPT:
				DetailedWordPrompt();
				break;
				
			case PROMPT: //Prompt is over, Player Interaction
				ChangeState (SegmentingCompoundWordsStates.PLAYER_INTERACTION);
				break;
				
			case CLICK_CORRECT:
				//Request Compliment then reinforce on next audio word
				if(_level != SkillLevel.Tutorial) dispatchEvent(new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
				else tutorial_mc.TutorialActions();
				break;
				
			case THEME_COMPLIMENT:
				//Reinforce player, once that is complete, go to theme state
				if( _level != SkillLevel.Tutorial ) PlayCorrectAnswerReinforcement();
				else tutorial_mc.TutorialActions();
				break;
				
			case CORRECT_REINFORCEMENT:
				if(!solveForPlayerBool)
				{
					//Change to player theme reward - Enter state will handle fade out and calling the reward
					ChangeState (SegmentingCompoundWordsStates.PLAYER_THEME_REWARD);
				}
				else
					goto case INCORRECT_REINFORCEMENT;
				break;
				
			case CLICK_INCORRECT:
				//Request criticism then play reinforcement on the word
				//ignore if tutorial
				if(_level != SkillLevel.Tutorial) dispatchEvent(new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
				break;
				
			case THEME_CRITICISM:
				//Reinforce the incorrect answer "wrong word, say wrong word"
				PlayIncorrectAnswerReinforcement();
				break;
				
			case INCORRECT_REINFORCEMENT:
				if(solveForPlayerBool)
				{
					//Move onto the next opportunity instead of looping into the same prompt
					solveForPlayerBool = false;
					//if(currentOpportunity == totalOpportunities)
					FadeOutContainer();
					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
				}
				else
				{
					//Fade out piece while playing "Try Again" // NEIN! Try again doesn't play, edited to go straight to the prompt
					Tweener.addTween(draggedPuzzlePiece, Tweener.Hash("time", FADE_TIME, "alpha", 0, "transition", Tweener.TransitionType.easeInOutQuint) ).OnComplete(ClearDragged);
					//Play the audio to prompt the player about the compoud word
					goto case TRY_AGAIN;
				}
				
				//Remove puzzle piece listener
				draggedPuzzlePiece.Destroy();
				draggedPuzzlePiece.removeEventListener( MouseEvent.MOUSE_MOVE, OnPieceMove );
				answerOptionsPuzzlePieces.Remove(draggedPuzzlePiece); //remove wrong piece from possible answers so listeners don't get reset
				break;
				
			case TRY_AGAIN:
				//"Try Again" has finished playing, play normal prompt again (skip prompt state, no setup requried),
				//then enter back into player interaction state
				//(onAudioWordComplete's reaction to prompt message is to move into player interaction)				
				NormalPrompt();
				break;
				
			case PLAYER_INACTIVITY:
				startTimer();
				break;
				
			case SOLVE_FOR_PLAYER:
				
				break;
				
			default:
				break;
			}
		}
		
		private void ClearDragged ()
		{
			backgroundContainer.removeChild(draggedPuzzlePiece);
			draggedPuzzlePiece.Destroy();
			answerOptionsPuzzlePieces.Remove(draggedPuzzlePiece);
			draggedPuzzlePiece = null;
		}
		
		public void OnTutorialComplete()
		{
			opportunityComplete = true;
			opportunityCorrect = tutorial_mc.correct;
			opportunityAnswered = true;
			currentOpportunity = 1;
			FadeOutContainer();
			if (opportunityCorrect) {
				dispatchEvent (new SkillEvent (SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1"));
				
			}  else {
				dispatchEvent (new SkillEvent (SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0"));
			}
		}
		
		//Traverses through each card and piece one at a time and highlights them in the center of the screen, reads them, then moves them into place
		public void DetailedWordPrompt()
		{
			//Audio bundle and clip variables used
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;
			
			//position variable used during calculations
			Vector2 globalToLocalCenter;
			
			if(stepCounter == 0) //0. "Listen to the words and say them after me"
			{
				//Hide pieces
				puzzlePieceLeft.alpha = 0;
				puzzlePieceRight.alpha = 0;
				
				stepCounter++;
				if(_level != SkillLevel.Tutorial)
				{
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Skill Instruction Audio/1D-listen-to-the-words-and-say-them-after-me");
					bundle.AddClip(clip, DETAILED_PROMPT, clip.length);
					SoundEngine.Instance.PlayBundle( bundle );
					return;
				}
			}
			
			if(stepCounter == 1) //1. Hide all components to control their reveal, highlight compound word
			{
				//Center and magnify compound card
				detailedPromptCardDestination =  new Vector2( compoundWordCard.x, compoundWordCard.y); //record position to return to
				globalToLocalCenter = answerBox.globalToLocal(new Vector2(Screen.width * .5f, Screen.height * .5f));
				compoundWordCard.x = globalToLocalCenter.x - compoundWordCard.Target.width * .5f;
				compoundWordCard.y = globalToLocalCenter.y - compoundWordCard.Target.height * .5f;
				compoundWordCard.alpha = 1f;
				
				//Magnify and Highlight
				compoundWordCard.ScaleCentered( compoundWordCard.scaleX * MAGNIFY_SCALE_PRECENT);
				HighlightOn(compoundWordCard);
				
				//Play audio for the card
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS,
				                                  "Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-" + currentCompoundWord[0].ToLower());
				bundle.AddClip(clip, DETAILED_PROMPT, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
				
				stepCounter++;
				return;
			}
			
			if(stepCounter == 2) //2. Narration Over, move compound word into place
			{
				//Turn off magnify and Highlight
				compoundWordCard.ScaleCentered( compoundWordCard.scaleX * MAGNIFY_REVERT_PERCENT);
				HighlightOff(compoundWordCard);
				
				//Tween card into place
				TweenerObj tempTweener =  Tweener.addTween(compoundWordCard, Tweener.Hash("time", DETAILED_PROMPT_TRANSITION_TIME, "x", detailedPromptCardDestination.x, 
				                                                                          "y", detailedPromptCardDestination.y, "transition", Tweener.TransitionType.easeInOutQuint) );
				tempTweener.OnComplete(DetailedWordPrompt);
				
				stepCounter++;
				return;
			}
			
			if(stepCounter == 3) //3. Highlight the hint word
			{
				FilterMovieClip referenceFMC;
				string narrationWord;
				
				if(correctWordAnswer == currentCompoundWord[1]) //Left puzzle piece is the answer, right is the hint
				{
					referenceFMC = puzzlePieceRight;
					narrationWord = currentCompoundWord[2];
				}
				else
				{
					referenceFMC = puzzlePieceLeft;
					narrationWord = currentCompoundWord[1];
				}
				
				//Center and magnify compound card
				detailedPromptCardDestination = new Vector2( referenceFMC.x, referenceFMC.y); //record position to return to
				globalToLocalCenter = answerBox.globalToLocal(new Vector2(Screen.width * .5f, Screen.height * .5f));
				referenceFMC.x = globalToLocalCenter.x - referenceFMC.Target.width * .5f;
				referenceFMC.y = globalToLocalCenter.y - referenceFMC.Target.height * .5f;
				referenceFMC.alpha = 1;
				
				//Magnify and Highlight
				referenceFMC.ScaleCentered( referenceFMC.scaleX * MAGNIFY_SCALE_PRECENT);
				HighlightOn(referenceFMC);
				
				//Play audio for the card
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-" + narrationWord.ToLower());
				bundle.AddClip(clip, DETAILED_PROMPT, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
				
				stepCounter++;
				return;
			}
			
			if(stepCounter == 4) //4. Narration Over, move hint word into place
			{
				FilterMovieClip referenceFMC;
				
				if(correctWordAnswer == currentCompoundWord[1]) //Left puzzle piece is the answer, right is the hint
					referenceFMC = puzzlePieceRight;
				else
					referenceFMC = puzzlePieceLeft;
				
				//Turn off magnify and Highlight
				referenceFMC.ScaleCentered( referenceFMC.scaleX * MAGNIFY_REVERT_PERCENT);
				HighlightOff(referenceFMC);
				
				//Tween card into place
				TweenerObj tempTweener =  Tweener.addTween(referenceFMC, Tweener.Hash("time", DETAILED_PROMPT_TRANSITION_TIME, "x", detailedPromptCardDestination.x, 
				                                                                      "y", detailedPromptCardDestination.y, "transition", Tweener.TransitionType.easeInOutQuint) );
				tempTweener.OnComplete(DetailedWordPrompt);
				
				stepCounter++;
				return;
			}
			
			//If emerging, do the same with the options, else, reveal options and move on
			if(stepCounter >= 5 && stepCounter < 5 + answerOptionsPuzzlePieces.Count * 2)
			{
				
//				float startingX = (refWidth * .5f);
				if(_level == SkillLevel.Emerging)
				{
					int answerIndex = Mathf.FloorToInt((stepCounter - 5) / 2);
					
					//Check to see if we are on the first part or second part of the given index.
					if(((stepCounter - 5) % 2) == 0) //on the first half, highlight puzzle piece
					{
						//Center and magnify compound card
						FilterMovieClip answerPiece = answerOptionsPuzzlePieces[answerIndex]; 
						detailedPromptCardDestination = new Vector2( answerPiece.x, answerPiece.y); //record position to return to
						
						answerPiece.x = (Screen.width * .5f) - (answerPiece.width * answerPiece.scaleX)/2; 
						answerPiece.y = (Screen.height * .5f) - (answerPiece.height * answerPiece.scaleY)/2;;
						answerPiece.alpha = 1;
						
						//Magnify and Highlight
						answerOptionsPuzzlePieces[answerIndex].ScaleCentered(answerPiece.scaleX * MAGNIFY_SCALE_PRECENT);
						HighlightOn(answerPiece);
						
						//Play audio for the card
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS,
						                                  "Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-" + answerOptionsPuzzlePieces[answerIndex].name.ToLower());
						bundle.AddClip(clip, DETAILED_PROMPT, clip.length);
						SoundEngine.Instance.PlayBundle( bundle );
						
						stepCounter++;
						return;
					}
					else //On the second half, tween the puzzle piece
					{
						//Turn off magnify and Highlight
						answerOptionsPuzzlePieces[answerIndex].ScaleCentered( answerOptionsPuzzlePieces[answerIndex].scaleX * MAGNIFY_REVERT_PERCENT);
						HighlightOff(answerOptionsPuzzlePieces[answerIndex]);
						
						//Tween card into place
						TweenerObj tempTweener =  Tweener.addTween(answerOptionsPuzzlePieces[answerIndex], Tweener.Hash("time", DETAILED_PROMPT_TRANSITION_TIME,
						                                                                                                "x", detailedPromptCardDestination.x, "y", detailedPromptCardDestination.y, "transition", Tweener.TransitionType.easeInOutQuint) );
						tempTweener.OnComplete(DetailedWordPrompt);
						
						stepCounter++;
						return;
					}
				}
				else //reveal answers and move on
				{
					foreach(FilterMovieClip fmc in answerOptionsPuzzlePieces)
					{
						Tweener.addTween(fmc, Tweener.Hash("time", FADE_TIME, "alpha", 1, "transition", Tweener.TransitionType.easeInOutQuint) );
					}
					
					stepCounter = 5 + answerOptionsPuzzlePieces.Count * 2;					
				}
			}
			
			if(stepCounter >= 5 + answerOptionsPuzzlePieces.Count * 2) //emerging prompt is over
			{
				stepCounter = 0;
				if(_level == SkillLevel.Tutorial)
				{
					AnswerOptionsPuzzlePieces[0].alpha = 1f;
					NormalPrompt("TUTORIAL_STEP");
				}
				else
				{
					NormalPrompt();
				}
			}
			
			
		}
		
		public void NormalPrompt()
		{
			NormalPrompt(PROMPT); //Default to the prompt constant as the message
		}
		
		public void NormalPrompt(string message)
		{
			string filepath = promptString();
			
			//Play the audio to prompt the player about the compoud word
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, filepath );
			bundle.AddClip(clip, message, clip.length);
			SoundEngine.Instance.PlayBundle( bundle );
			
			PromptGlowSequence();
		}
		
		public string promptString()
		{
			string folder;
			string withoutWord;
			
			if(correctWordAnswer == currentCompoundWord[1]) //Left puzzle piece is the answer, right is the hint
			{
				folder = "1D-AB without B is";
				withoutWord = currentCompoundWord[2].ToLower();
			}
			else
			{
				folder = "1D-AB without A is";
				withoutWord = currentCompoundWord[1].ToLower();
			}
			
			return "Narration/1D-Skill Instruction Audio/1D-Initial Instruction/" + folder + "/1D-" + currentCompoundWord[0].ToLower() + "-without-" + withoutWord;
		}
		
		private void PromptGlowSequence()
		{
			string filepath = "Narration/1D-Skill Instruction Audio/";
			AudioClip clip = null;
			
			switch (stepCounter)
			{
			case 0:
				HighlightOn(compoundWordCard);
				filepath += "1D-Compound Word Cards/1D-" + currentCompoundWord[0].ToLower();
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, filepath );
				break;
			case 1:
				HighlightOff(compoundWordCard);
				filepath += "1D-Word Segment Cards/1D-";
				if(correctWordAnswer == currentCompoundWord[1])
				{
					HighlightOn(puzzlePieceRight);
					filepath += currentCompoundWord[2].ToLower();
				}
				else
				{
					HighlightOn(puzzlePieceLeft);
					filepath += currentCompoundWord[1].ToLower();
				}
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, filepath);
				break;
			case 2:
				HighlightOff(puzzlePieceLeft);
				HighlightOff(puzzlePieceRight);
				stepCounter = 0;
				break;
			}
			
			if(clip != null)
			{
				glowTimer = TimerUtils.SetTimeout( clip.length * 0.4f, PromptGlowSequence );
				stepCounter++;
			}
		}
		
		public void InactivtyPrompt()
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;
			
			if(timeoutCount == 0)
			{
				//Play "Please touch the picture to hear the words again"
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Skill Instruction Audio/1D-touch-the-tablets");
				bundle.AddClip(clip, PLAYER_INACTIVITY, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
				timeoutCount++;
				
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf("Narration/1D-Skill Instruction Audio/1D-touch-the-tablets") );
				return;
			}
			
			if(timeoutCount == 1)
			{
				//Play "Please touch the help button to see Cami do it."
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Skill Instruction Audio/1D-touch-the-green-button");
				bundle.AddClip(clip, PLAYER_INACTIVITY, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
				timeoutCount++;
				
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf("Narration/1D-Skill Instruction Audio/1D-touch-the-green-button") );
				return;
			}
			
			string folder;
			string withoutWord;
			if(correctWordAnswer == currentCompoundWord[1]) //Left puzzle piece is the answer, right is the hint
			{
				folder = "1D-AB without B is A";
				withoutWord = currentCompoundWord[2].ToLower();
			}
			else
			{
				folder = "1D-AB without A is B";
				withoutWord = currentCompoundWord[1].ToLower();
			}
			
			if(timeoutCount == 2)
			{
				//Opportunity is now incorrect
				//Play normal prompt
				//highlight answer
				foreach(FilterMovieClip fmc in answerOptionsPuzzlePieces)
				{
					if(fmc.name == correctWordAnswer) //puzzle piece is the answer, highlight it
					{
						HighlightOn(fmc);
						break;
					}
				}
				
				NormalPrompt(PLAYER_INACTIVITY); //Normal Prompt with different message
				timeoutCount++;
				
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf("Narration/1D-Skill Instruction Audio/1D-Initial Instruction/" + folder + "/1D-" + currentCompoundWord[0].ToLower() + "-without-" + withoutWord.ToLower()) );
				return;
			}
			if(timeoutCount == 3)
			{
				timeoutEnd = true;
				
				//Reset step counter
				timeoutCount = 0;
				
				//Enter Solve for Player State
				ChangeState(SegmentingCompoundWordsStates.SOLVE_FOR_PLAYER);
				
				timeoutCount++;
				
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf("Narration/1D-Reinforcement Audio/" + folder + "/1D-" + currentCompoundWord[0].ToLower() + "-without-" + withoutWord.ToLower()) );
				return;
			}
			
		}
		
		public override void onTimerComplete()
		{
			base.onTimerComplete();
			InactivtyPrompt();
		}
		
		public void SolveForPlayer()
		{
			Debug.Log ("SOLVING FOR PLAYER - Step " + stepCounter);
			TweenerObj tempTweener = null;
			if(stepCounter == 0)
			{				
				//highlight answer, fade wrong answers
				foreach(FilterMovieClip fmc in answerOptionsPuzzlePieces)
				{
					if(fmc.name == correctWordAnswer) //puzzle piece is the answer, highlight it
					{
						HighlightOn(fmc);
						draggedPuzzlePiece = fmc; //record correct piece
					}
					else
					{
						tempTweener = Tweener.addTween(fmc, Tweener.Hash("time", FADE_TIME, "alpha", 0, "transition", Tweener.TransitionType.easeInOutQuint) );	
					}
				}
				
				
				stepCounter++;
				SolveForPlayer();
				return;
			}
			
			if(stepCounter == 1)
			{
				//tween right answer to answer position
				//Find the correct answer piece
				
				float adjustedScale = answerScale;
				
				if (selectedPlatform == Platforms.WIN) {
					adjustedScale = answerScale_WIN;
				} else if (selectedPlatform == Platforms.ANDROID) {
					//adjustedScale
					adjustedScale = answerPuzzlePieceScaler;
				}
				
				FilterMovieClip answerReferenceFMC;
				if(correctWordAnswer == currentCompoundWord[1]) { //Left puzzle piece is the answer, right is the hint
					answerReferenceFMC = puzzlePieceLeft;
					adjustedScale += .005f;
				}  else {
					answerReferenceFMC = puzzlePieceRight;
				}
				
				//Need to recalculate right answer position
				Vector2 targetLocation; //The center of the answer location
				targetLocation = answerReferenceFMC.localToGlobal(new Vector2(0,0));
				targetLocation = backgroundContainer.globalToLocal(targetLocation);
				
				tempTweener =  Tweener.addTween(draggedPuzzlePiece, Tweener.Hash("time", DETAILED_PROMPT_TRANSITION_TIME, "x", targetLocation.x, 
				                                                                 "y", targetLocation.y, "scaleX", adjustedScale, "scaleY", adjustedScale, "transition", Tweener.TransitionType.easeInOutQuint) );
				stepCounter++;
				tempTweener.OnComplete(SolveForPlayer);				
				return;
			}
			
			if(stepCounter > 1)
			{
				//set opportunity as wrong, set the correct answer to "draggedPuzzlePiece", and change state to PLAYER_REINFORCEMENT
				opportunityAnswered = true;
				opportunityCorrect = false;
				stepCounter = 0;
				
				//Turn highlight off before moving on
				HighlightOff(draggedPuzzlePiece);
				
				solveForPlayerBool = true;
				ChangeState(SegmentingCompoundWordsStates.PLAYER_REINFORCEMENT);				
			}
		}
		
		public void PlayCorrectAnswerReinforcement()
		{
			string folder;
			string withoutWord;
			string filepath;
			
			if(correctWordAnswer == currentCompoundWord[1]) //Left puzzle piece is the answer, right is the hint
			{
				folder = "1D-AB without B is A";
				withoutWord = currentCompoundWord[2].ToLower();
			}
			else
			{
				folder = "1D-AB without A is B";
				withoutWord = currentCompoundWord[1].ToLower();
			}
			
			filepath = "Narration/1D-Reinforcement Audio/" + folder + "/1D-" + currentCompoundWord[0].ToLower() + "-without-" + withoutWord;
			
			//Play the audio to prompt the player about the compoud word
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, filepath );
			bundle.AddClip(clip, CORRECT_REINFORCEMENT, clip.length);
			SoundEngine.Instance.PlayBundle( bundle );
			
			endGlowSequence();
		}
		
		private void endGlowSequence()
		{
			string filepath = "Narration/1D-Skill Instruction Audio/";
			AudioClip clip = null;
			
			switch (stepCounter)
			{
			case 0:
				HighlightOn(compoundWordCard);
				filepath += "1D-Compound Word Cards/1D-" + currentCompoundWord[0].ToLower();
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, filepath );
				break;
			case 1:
				HighlightOff(compoundWordCard);
				filepath += "1D-Word Segment Cards/1D-";
				if(correctWordAnswer == currentCompoundWord[1])
				{
					HighlightOn(puzzlePieceRight);
					filepath += currentCompoundWord[2].ToLower();
				}
				else
				{
					HighlightOn(puzzlePieceLeft);
					filepath += currentCompoundWord[1].ToLower();
				}
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, filepath);
				break;
			case 2:
				HighlightOff(puzzlePieceLeft);
				HighlightOff(puzzlePieceRight);
				HighlightOn(draggedPuzzlePiece);
				stepCounter = 0;
				break;
			}
			
			if(clip != null)
			{
				glowTimer = TimerUtils.SetTimeout( clip.length * 0.4f, endGlowSequence );
				stepCounter++;
			}
		}
		
		public void PlayIncorrectAnswerReinforcement()
		{
			//The instruction for the wrong word			
			//Play the audio to prompt the player about the compoud word
			string word = draggedPuzzlePiece.name;
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-" + word);
			bundle.AddClip(clip, INCORRECT_REINFORCEMENT, clip.length);
			SoundEngine.Instance.PlayBundle( bundle );
		}
		
		public void HighlightOn(FilterMovieClip fmc)
		{
			if(highlightedFilterMovieClips.Contains(fmc)) //if the fmc is highlighted, don't highlight it again
				return;
			
			fmc.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
			fmc.filters[0].Visible = true;
			highlightedFilterMovieClips.Add(fmc);
		}
		
		public void HighlightOff(FilterMovieClip fmc)
		{
			if(!highlightedFilterMovieClips.Contains(fmc)) //if the fmc isn't highlighted, don't de-highlight it again
				return;
			fmc.Destroy();
			highlightedFilterMovieClips.Remove(fmc);
		}
		
		public TweenerObj FadeInContainer()
		{
			return Tweener.addTween(backgroundContainer, Tweener.Hash("time", FADE_TIME, "alpha", 1, "transition", Tweener.TransitionType.easeInOutQuint) );
		}
		
		public TweenerObj FadeOutContainer()
		{
			return Tweener.addTween(backgroundContainer, Tweener.Hash("time", FADE_TIME, "alpha", 0, "transition", Tweener.TransitionType.easeInOutQuint) );
		}
		
		private bool isdrag;
		private Vector2 cardOrig;
		public void OnPieceDown( CEvent e )
		{
			ChangeState(SegmentingCompoundWordsStates.PLAYER_INTERACTING);
			PlayClickSound();
			//Set the clicked piece to drag
			if(e.currentTarget as FilterMovieClip != draggedPuzzlePiece)
			{
				draggedPuzzlePiece = e.currentTarget as FilterMovieClip;
				draggedPuzzlePieceStartLocation.x = draggedPuzzlePiece.x;
				draggedPuzzlePieceStartLocation.y = draggedPuzzlePiece.y;
			}
			else
			{
				Tweener.removeTweens(draggedPuzzlePiece);
			}
			
			draggedPuzzlePiece.ScaleCentered(answerRowPieceScale * 1.25f); // This causes the multiple scale
			
			backgroundContainer.removeChild(draggedPuzzlePiece);
			backgroundContainer.addChild(draggedPuzzlePiece);
			
			//Add Mouse Move listener to the stage
			addEventListener( MouseEvent.MOUSE_MOVE, OnPieceMove );
			
			//Add Listener to stage in case mouse isn't over answerpiece then manually check mouse position
			stage.addEventListener( MouseEvent.MOUSE_UP, OnPieceUp );
			
			cardOrig = draggedPuzzlePiece.parent.localToGlobal( new Vector2( draggedPuzzlePiece.x, draggedPuzzlePiece.y ) );
			isdrag = false;
		}
		
		public void OnBoxPieceDown( CEvent e )
		{
			PlayClickSound();
			draggedPuzzlePiece = e.currentTarget as FilterMovieClip;
			addEventListener( MouseEvent.MOUSE_UP, OnBoxPieceUp );
		}
		
		private void OnPieceMove( CEvent e )
		{
			//Position the piece to the cursor
			
			float xPercent = .5f;
			
			if (leftSide) {
				xPercent = .8f;
			}
			
			Vector2 newPos = new Vector2( backgroundContainer.mouseX - draggedPuzzlePiece.width * xPercent, backgroundContainer.mouseY - draggedPuzzlePiece.height * .5f );
			
			draggedPuzzlePiece.x = newPos.x;
			draggedPuzzlePiece.y = newPos.y;
			
			newPos = draggedPuzzlePiece.parent.localToGlobal( newPos );
			
			if( Mathf.Abs(cardOrig.x - newPos.x + cardOrig.y - newPos.y) > 5 ){
				isdrag = true;
			}
		}
		
		private void OnPieceUp( CEvent e )
		{
			//Release the piece based off of position.
			//If over answer, snap to answer area
			//else, return to original position
			
			bool leftAnswer = true;
			float adjustedScale = answerPuzzlePieceScaler;
			
			
			if (selectedPlatform == Platforms.WIN) {
				adjustedScale = answerScale_WIN;
			}
			
			FilterMovieClip answerReferenceFMC; //Set to the hidden puzzle piece that is the answer
			
			//Get a reference to the answer piece
			if (correctWordAnswer == currentCompoundWord [1]) { //Left puzzle piece is the answer, right is the hint
				answerReferenceFMC = puzzlePieceLeft;
				adjustedScale += 0.005f;
			}  else {
				answerReferenceFMC = puzzlePieceRight;
				leftAnswer = false;
			}
			
			//Get the global position of the hidden puzzle piece
			Vector2 referenceGlobalPosition = answerBox.localToGlobal(new Vector2(answerReferenceFMC.x, answerReferenceFMC.y));
			Rectangle overlap = answerReferenceFMC.getBounds(backgroundContainer).intersection(draggedPuzzlePiece.getBounds(backgroundContainer));
			
			//Remove Move Listener
			removeEventListener( MouseEvent.MOUSE_MOVE, OnPieceMove );
			
			//Check to see that the mouse in the bounds of the hidden target piece
			if(overlap.width > 25 && overlap.height > 25)
			{
				Vector2 dataPoint = draggedPuzzlePiece.parent.localToGlobal( new Vector2( draggedPuzzlePiece.x, draggedPuzzlePiece.y ) );
				AddNewActionData( formatActionData( "move", answerOptionsPuzzlePieces.IndexOf( draggedPuzzlePiece ) + 2, dataPoint, draggedPuzzlePiece.name == correctWordAnswer ) );
				
				referenceGlobalPosition = backgroundContainer.globalToLocal(referenceGlobalPosition);
				Tweener.addTween(draggedPuzzlePiece, Tweener.Hash("time", DETAILED_PROMPT_TRANSITION_TIME, "x", referenceGlobalPosition.x, 
				                                                  "y", referenceGlobalPosition.y, "scaleX", adjustedScale, "scaleY", adjustedScale, "transition", Tweener.TransitionType.easeInOutQuint) );
				
				//Enter reinforcement - answer will be checked and acted upon
				if(_level != SkillLevel.Tutorial) ChangeState(SegmentingCompoundWordsStates.PLAYER_REINFORCEMENT);
				else tutorial_mc.TutorialActions();
			}
			
			else //return to starting position
			{
				if( isdrag )
					AddNewActionData( formatActionData( "snap", answerOptionsPuzzlePieces.IndexOf( draggedPuzzlePiece ) + 2, Vector2.zero ) );
				else
					AddNewActionData( formatActionData( "tap", answerOptionsPuzzlePieces.IndexOf( draggedPuzzlePiece ) + 2, Vector2.zero ) );
				
				float returnAdjustedScale = answerPuzzlePieceScaler;
				
				if (selectedPlatform == Platforms.WIN) {
					returnAdjustedScale = baseScale;
				}
				
				if (leftAnswer) {
					returnAdjustedScale -= 0.05f;
				}
				
				Tweener.addTween(draggedPuzzlePiece, Tweener.Hash("time", DETAILED_PROMPT_TRANSITION_TIME, "x", draggedPuzzlePieceStartLocation.x, 
				                                                  "y", draggedPuzzlePieceStartLocation.y, "scaleX", returnAdjustedScale, "scaleY", returnAdjustedScale, "transition", Tweener.TransitionType.easeInOutQuint) );
				
				//Play the piece's reinforcement
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS,
				                                            "Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-" + draggedPuzzlePiece.name);
				bundle.AddClip(clip, PROMPT, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
				ChangeState(SegmentingCompoundWordsStates.PLAYER_INTERACTION);
			}
			
			//Remove up listener from stage
			stage.removeEventListener( MouseEvent.MOUSE_UP, OnPieceUp );
		}
		
		public void OnBoxPieceUp( CEvent e )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;
			
			if(draggedPuzzlePiece == puzzlePieceLeft || draggedPuzzlePiece == puzzlePieceRight)
			{
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-" + draggedPuzzlePiece.name);
				bundle.AddClip(clip, PROMPT, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
				
				AddNewActionData( formatActionData( "tap", 1, Vector2.zero ) );
			}
			else
			{
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-" + draggedPuzzlePiece.name);
				bundle.AddClip(clip, PROMPT, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
				
				AddNewActionData( formatActionData( "tap", 0, Vector2.zero ) );
			}
			
			ChangeState(SegmentingCompoundWordsStates.PLAYER_INTERACTION);
			
			removeEventListener( MouseEvent.MOUSE_UP, OnBoxPieceUp );
		}
		
		public void PlayCorrectSoundPublic()
		{
			PlayCorrectSound();
		}
		
		//Reset the tutorial by loading a new one
		public void RestartTutorial()
		{
			tutorialSetupCounter = 0;
			
			//create tutorial and pass it a reference to this
			tutorial_mc.Dispose();
			tutorial_mc = new SegmentingCompoundWordsTutorial();
			tutorial_mc.skillRef = this;
			
			ChangeState(SegmentingCompoundWordsStates.TUTORIAL);
		}
		
		public void fadeInTutorialPieces() {
			//Debug.LogError ("Fade In");
			Tweener.addTween(puzzlePieceLeft, Tweener.Hash("time", FADE_TIME, "alpha", 1, "transition", Tweener.TransitionType.easeInOutQuint) );
			Tweener.addTween(compoundWordCard, Tweener.Hash("time", FADE_TIME, "alpha", 1, "transition", Tweener.TransitionType.easeInOutQuint) );
			Tweener.addTween(AnswerOptionsPuzzlePieces[0], Tweener.Hash("time", FADE_TIME, "alpha", 1, "transition", Tweener.TransitionType.easeInOutQuint) );

		}

		public void showBirdcagePuzzle() {
			puzzlePieceLeft.alpha = 1;
			compoundWordCard.alpha = 1;
			AnswerOptionsPuzzlePieces [0].alpha = 1;
		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = "";
			foreach(string s in currentCompoundWord)
			{
				if(data != "")
					data = data + "-";
				data = data + s;
			}
			
			foreach(List<string> ls in compoundWordOpportunityBank)
			{
				data = data + ";";
				foreach(string s in ls)
				{
					if(data.LastIndexOf(';') != data.Length - 1)
						data = data + "-";
					data = data + s;
				}
			}
			
			return data;
		}
		
		private void parseSessionData (string data)
		{
			compoundWordOpportunityBank = new List<List<string>>();
			
			string[] sData = data.Split(';');
			string[] bData;
			List<string> sWords;
			
			foreach( string s in sData )
			{
				sWords = new List<string>();
				bData = s.Split('-');
				foreach( string b in bData )
				{
					sWords.Add(b);
				}
				compoundWordOpportunityBank.Add(sWords);
			}
		}
		
		public static List<string> audioNames = new List<string>
		{	"",
			"Narration/1D-Story Introduction Audio/1D-story-introduction",
			"Narration/1D-Skill Instruction Audio/1D-we-need-doghouse",
			"Narration/1D-Skill Instruction Audio/1D-cami-thinks",
			"Narration/1D-Skill Instruction Audio/1D-now-its-your-turn",
			"Narration/1D-Skill Instruction Audio/1D-drag-birdcage",
			"Narration/1D-Skill Instruction Audio/1D-now-lets-play-the-game",
			"Narration/1D-Skill Instruction Audio/1D-try-again",
			"Narration/1D-Skill Instruction Audio/1D-touch-the-tablets",
			"Narration/1D-Skill Instruction Audio/1D-touch-the-green-button",
			"Narration/1D-Skill Instruction Audio/1D-listen-to-the-words-and-say-them-after-me",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-baseball-without-base",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-basketball-without-basket",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-birdcage-without-bird",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-birdhouse-without-bird",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-boathouse-without-boat",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-bookcase-without-book",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-bowtie-without-bow",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-boxcar-without-box",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-bullhorn-without-bull",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-buttercup-without-butter",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-butterfly-without-butter",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-chalkboard-without-chalk",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-cowboy-without-cow",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-cupcake-without-cup",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-doghouse-without-dog",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-earring-without-ear",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-firehouse-without-fire",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-fireman-without-fire",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-firewood-without-fire",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-fishbowl-without-fish",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-football-without-foot",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-goldfish-without-gold",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-houseboat-without-house",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-keyhole-without-key",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-ladybug-without-lady",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-lighthouse-without-light",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-lipstick-without-lip",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-mailbox-without-mail",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-pancake-without-pan",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-peanut-without-pea",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-pocketbook-without-pocket",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-rainbow-without-rain",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-rattlesnake-without-rattle",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-sandpaper-without-sand",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-sawhorse-without-saw",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-skyscraper-without-sky",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-snowball-without-snow",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-snowman-without-snow",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-starfish-without-star",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-sunflower-without-sun",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-sunglasses-without-sun",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-surfboard-without-surf",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-turtleneck-without-turtle",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-wallpaper-without-wall",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-wheelchair-without-wheel",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-baseball-without-ball",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-basketball-without-ball",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-birdcage-without-cage",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-birdhouse-without-house",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-boathouse-without-house",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-bookcase-without-case",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-bowtie-without-tie",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-boxcar-without-car",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-bullhorn-without-horn",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-buttercup-without-cup",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-butterfly-without-fly",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-chalkboard-without-board",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-cowboy-without-boy",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-cupcake-without-cake",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-doghouse-without-house",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-earring-without-ring",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-firehouse-without-house",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-fireman-without-man",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-firewood-without-wood",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-fishbowl-without-bowl",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-football-without-ball",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-goldfish-without-fish",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-houseboat-without-boat",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-keyhole-without-hole",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-ladybug-without-bug",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-lighthouse-without-house",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-lipstick-without-stick",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-mailbox-without-box",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-pancake-without-cake",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-peanut-without-nut",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-pocketbook-without-book",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-rainbow-without-bow",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-rattlesnake-without-snake",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-sandpaper-without-paper",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-sawhorse-without-horse",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-skyscraper-without-scraper",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-snowball-without-ball",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-snowman-without-man",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-starfish-without-fish",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-sunflower-without-flower",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-sunglasses-without-glasses",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-surfboard-without-board",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-turtleneck-without-neck",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-wallpaper-without-paper",
			"Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without B is/1D-wheelchair-without-chair",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-apple",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-baby",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-ball",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-base",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-basket",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-bird",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-board",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-boat",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-book",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-bow",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-bowl",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-box",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-boy",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-bug",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-bull",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-butter",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-cage",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-cake",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-car",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-carrot",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-case",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-cat",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-chair",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-chalk",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-cloud",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-cow",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-cup",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-dog",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-ear",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-fire",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-fish",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-flower",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-fly",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-foot",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-glasses",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-gold",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-hole",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-horn",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-horse",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-house",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-key",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-lady",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-leaf",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-light",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-lion",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-lip",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-mail",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-man",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-neck",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-nut",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-pan",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-paper",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-pea",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-pen",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-pencil",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-pocket",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-rabbit",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-rain",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-rattle",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-ring",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-sand",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-saw",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-scraper",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-sky",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-snake",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-snow",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-star",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-stick",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-sun",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-surf",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-tie",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-turtle",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-wall",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-wheel",
			"Narration/1D-Skill Instruction Audio/1D-Word Segment Cards/1D-wood",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-baseball",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-basketball",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-birdcage",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-birdhouse",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-boathouse",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-bookcase",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-bowtie",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-boxcar",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-bullhorn",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-buttercup",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-butterfly",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-chalkboard",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-cowboy",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-cupcake",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-doghouse",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-earring",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-firehouse",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-fireman",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-firewood",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-fishbowl",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-football",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-goldfish",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-houseboat",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-keyhole",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-ladybug",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-lighthouse",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-lipstick",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-mailbox",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-pancake",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-peanut",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-pocketbook",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-rainbow",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-rattlesnake",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-sandpaper",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-sawhorse",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-skyscraper",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-snowball",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-snowman",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-starfish",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-sunflower",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-sunglasses",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-surfboard",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-turtleneck",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-wallpaper",
			"Narration/1D-Skill Instruction Audio/1D-Compound Word Cards/1D-wheelchair",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-baseball-without-base",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-basketball-without-basket",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-birdcage-without-bird",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-birdhouse-without-bird",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-boathouse-without-boat",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-bookcase-without-book",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-bowtie-without-bow",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-boxcar-without-box",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-bullhorn-without-bull",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-buttercup-without-butter",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-butterfly-without-butter",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-chalkboard-without-chalk",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-cowboy-without-cow",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-cupcake-without-cup",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-doghouse-without-dog",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-earring-without-ear",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-firehouse-without-fire",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-fireman-without-fire",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-firewood-without-fire",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-fishbowl-without-fish",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-football-without-foot",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-goldfish-without-gold",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-houseboat-without-house",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-keyhole-without-key",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-ladybug-without-lady",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-lighthouse-without-light",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-lipstick-without-lip",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-mailbox-without-mail",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-pancake-without-pan",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-peanut-without-pea",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-pocketbook-without-pocket",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-rainbow-without-rain",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-rattlesnake-without-rattle",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-sandpaper-without-sand",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-sawhorse-without-saw",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-skyscraper-without-sky",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-snowball-without-snow",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-snowman-without-snow",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-starfish-without-star",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-sunflower-without-sun",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-sunglasses-without-sun",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-surfboard-without-surf",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-turtleneck-without-turtle",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-wallpaper-without-wall",
			"Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-wheelchair-without-wheel",
			"User-Earns-Artifact",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-baseball-without-ball",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-basketball-without-ball",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-birdcage-without-cage",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-birdhouse-without-house",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-boathouse-without-house",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-bookcase-without-case",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-bowtie-without-tie",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-boxcar-without-car",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-bullhorn-without-horn",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-buttercup-without-cup",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-butterfly-without-fly",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-chalkboard-without-board",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-cowboy-without-boy",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-cupcake-without-cake",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-doghouse-without-house",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-earring-without-ring",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-firehouse-without-house",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-fireman-without-man",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-firewood-without-wood",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-fishbowl-without-bowl",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-football-without-ball",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-goldfish-without-fish",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-houseboat-without-boat",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-keyhole-without-hole",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-ladybug-without-bug",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-lighthouse-without-house",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-lipstick-without-stick",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-mailbox-without-box",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-pancake-without-cake",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-peanut-without-nut",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-pocketbook-without-book",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-rainbow-without-bow",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-rattlesnake-without-snake",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-sandpaper-without-paper",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-sawhorse-without-horse",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-skyscraper-without-scraper",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-snowball-without-ball",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-snowman-without-man",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-starfish-without-fish",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-sunflower-without-flower",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-sunglasses-without-glasses",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-surfboard-without-board",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-turtleneck-without-neck",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-wallpaper-without-paper",
			"Narration/1D-Reinforcement Audio/1D-AB without B is A/1D-wheelchair-without-chair"
		};
	}	
}
