using System;
using System.Collections.Generic;
using UnityEngine;
using HatchFramework;
using SSGCore;
using pumpkin.display;
using pumpkin;

namespace SSGCore
{
	
	[RequireComponent(typeof(MovieClipOverlayCameraBehaviour))]
	public class DebugGameLoader : MonoBehaviour
	{
		
		
		protected UpgradeManager upgradeManager;
		protected Stage stage;
		
			
		public void Awake()
		{
			gameObject.AddComponent<AssetLoader>();
		}
		
		public void Start()
		{
		
			stage = MovieClipOverlayCameraBehaviour.instance.stage;
			
			AssetLoader.Instance.loadAssetBundlesLocally = true;
			
			// if we need to test downloading, uncomment next line to clear cache and force download
			//AssetLoader.Instance.clearCacheFirst = true;
			
			upgradeManager = new UpgradeManager();
			upgradeManager.OnCompleteEvent += HandleUpgradeManagerOnCompleteEvent;
			upgradeManager.CheckForUpdates();
						
		}
		
		void HandleUpgradeManagerOnCompleteEvent (bool success)
		{
			upgradeManager.OnCompleteEvent -= HandleUpgradeManagerOnCompleteEvent;
			upgradeManager = null;
			
			LoadSkill();
			
			
		}
		
		void LoadSkill(){
		
			
		}
		
	}
}

