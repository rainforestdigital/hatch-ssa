using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.geom;

namespace SSGCore
{

	public class Measurement : BaseSkill
	{
		private const string AGAIN = "TryAgain";
		private const string PERSON = "person";
		private const string OBJECT = "supply";
		
		private List<List<string>> egOpps = new List<List<string>>(){
										new List<string>(){PERSON, "shorter", "taller"},
										new List<string>(){PERSON, "shorter", "taller"},
										new List<string>(){PERSON, "shorter", "taller"},
										new List<string>(){PERSON, "shorter", "taller"},
										new List<string>(){PERSON, "shorter", "taller"},
										new List<string>(){PERSON, "taller", "shorter"},
										new List<string>(){PERSON, "taller", "shorter"},
										new List<string>(){PERSON, "taller", "shorter"},
										new List<string>(){PERSON, "taller", "shorter"},
										new List<string>(){PERSON, "taller", "shorter"}};
		private List<List<string>> dgOpps = new List<List<string>>(){
										new List<string>(){PERSON, "shortest", "shorter", "tallest"},
										new List<string>(){PERSON, "shortest", "shorter", "tallest"},
										new List<string>(){PERSON, "shortest", "shorter", "tallest"},
										new List<string>(){PERSON, "tallest", "taller", "shortest"},
										new List<string>(){PERSON, "tallest", "taller", "shortest"},
										new List<string>(){OBJECT, "longest", "longer", "shortest"},
										new List<string>(){OBJECT, "longest", "longer", "shortest"},
										new List<string>(){OBJECT, "longest", "longer", "shortest"},
										new List<string>(){OBJECT, "shortest", "shorter", "longest"},
										new List<string>(){OBJECT, "shortest", "shorter", "longest"}};
		private List<List<string>> ddOpps = new List<List<string>>(){
										new List<string>(){PERSON, "shorter", "taller"},
										new List<string>(){PERSON, "taller", "shorter"},
										new List<string>(){PERSON, "shortest", "shorter", "tallest"},
										new List<string>(){PERSON, "shortest", "shorter", "tallest"},
										new List<string>(){PERSON, "tallest", "taller", "shortest"},
										new List<string>(){OBJECT, "longer", "shorter"},
										new List<string>(){OBJECT, "shorter", "longer"},
										new List<string>(){OBJECT, "longest", "longer", "shortest"},
										new List<string>(){OBJECT, "longest", "longer", "shortest"},
										new List<string>(){OBJECT, "shortest", "shorter", "longest"}};
		
		private List<List<string>> setList;
		private List<string> currentSet;
		
		private List<FilterMovieClip> stuff;
		private FilterMovieClip currentThing;
		
		private DisplayObjectContainer container;
		
		private new MeasurementTutorial tutorial_mc;
		
		private MovieClip background;
		
		private float baseScale;
		private int failCount;

		private bool _doItForThem;
		
		public Measurement( SkillLevel level, SessionInfo  currentSession ) : base( level, currentSession )
		{
			BACKGROUND = "Background";
			ARTIFACT = "Artifact";
			
			Init(null);
		}
		
		public override void Dispose ()
		{
			base.Dispose ();
			
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			stuffOff(true);
			stage.removeEventListener(MouseEvent.MOUSE_UP, onUp);
			if(tutorial_mc != null && tutorial_mc.person != null)
				tutorial_mc.person.Destroy();
			if(stuff != null)
			{
				foreach(FilterMovieClip fmc in stuff)
					fmc.Destroy();
			}
			if(currentThing != null)
				currentThing.Destroy();
		}
		
		public override void Init (MovieClip parent)
		{
			base.Init(parent);

			_doItForThem = false;

			container = new DisplayObjectContainer();
			container.width = MainUI.STAGE_WIDTH;
			container.height = MainUI.STAGE_HEIGHT;
			container.visible = false;
			container.alpha = 0;
			addChild( container );
			
			Resize( Screen.width, Screen.height );
			
			background = MovieClipFactory.CreateMeasurementBackground();
			baseScale = 900f / background.height;
			Debug.Log (baseScale);
			background.scaleX = background.scaleY = baseScale;
			background.x = (MainUI.STAGE_WIDTH - (background.width * background.scaleX)) / 2f;
			background.y = (MainUI.STAGE_HEIGHT - (background.height * background.scaleY)) / 2f;
			container.addChild(background);
			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			if(_level == SkillLevel.Tutorial)
			{
				totalOpportunities = 1;
				setList = new List<List<string>>(){new List<string>(){PERSON, "shorter", "taller"}};
				tutorial_mc = new MeasurementTutorial(baseScale);
				tutorial_mc.View.addEventListener(TutorialEvent.START, onTutorialStart);
				tutorial_mc.View.addEventListener(TutorialEvent.COMPLETE, onTutorialComplete);
			}
			else
			{
				totalOpportunities = 10;
				if(resumingSkill)
				{
					parseSessionData(currentSession.sessionData);
				//	nextOpportunity();
				}
				else
				{
					switch(_level)
					{
					case SkillLevel.Emerging:
						setList = egOpps;
						break;
					case SkillLevel.Developing:
						setList = dgOpps;
						break;
					case SkillLevel.Developed:
					case SkillLevel.Completed:
						setList = ddOpps;
						break;
					}
					setList.Shuffle();
				}
			}
		}
		
		public override void OnAudioWordComplete (string word)
		{
			switch(word)
			{
			case THEME_INTRO:
				if( !currentSession.skipIntro )
					playSound(MeasurementSounds.SOUND_1, INTRO);
				else
					OnAudioWordComplete( INTRO );
				break;
				
			case INTRO:
				if(_level == SkillLevel.Tutorial)
					tutorial_mc.OnStart();
				else
					nextOpportunity();
				break;
				
			case NOW_ITS:
				playSound(MeasurementSounds.SOUND_13 + currentSet[1] + "-dinosaur", PLEASE_FIND);
				break;
				
			case PLEASE_FIND:
				stuffOn();
				break;
				
			case AGAIN:
				resetTutorial();
				break;
				
			case CLICK_CORRECT:
				dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
				break;
				
			case CLICK_INCORRECT:
				dispatchEvent( new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
				break;
				
			case THEME_COMPLIMENT:
				playSound(MeasurementSounds.SOUND_20 + currentSet[1] + "-dinosaur", YOU_FOUND);
				break;
				
			case THEME_CRITICISM:
				glowOff();

				if(_doItForThem)
					doItForThem();
				else
					playSound(MeasurementSounds.SOUND_16 + currentSet[currentSet.Count - 2] + "-dinosaur", PLEASE_FIND);
				break;
				
			case YOU_FOUND:
				if(_level != SkillLevel.Tutorial)
					goto case NOW_LETS;
				playSound(MeasurementSounds.SOUND_4, NOW_LETS);
				break;
				
			case NOW_LETS:
				fadeOutContainer();
				if(opportunityCorrect)
					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
				else
					dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );

				break;
			}
		}
		
		public override void onTutorialStart (CEvent e)
		{
			base.onTutorialStart (e);
			tutorial_mc.View.removeEventListener(TutorialEvent.START, onTutorialStart);
			addChild(tutorial_mc.View);
		}
		
		public void onTutorialComplete (CEvent e)
		{
			removeChild(tutorial_mc.View);
			tutorial_mc.View.removeEventListener(TutorialEvent.COMPLETE, onTutorialComplete);
			
			nextOpportunity();
		}
		
		protected override void resetTutorial ()
		{
			stuffOff(true);
			fadeOutContainer();
			
			currentOpportunity--;
			tutorial_mc.person.Destroy();
			
			tutorial_mc = new MeasurementTutorial(baseScale);
			tutorial_mc.View.addEventListener(TutorialEvent.START, onTutorialStart);
			tutorial_mc.View.addEventListener(TutorialEvent.COMPLETE, onTutorialComplete);
			tutorial_mc.OnStart();
		}
		
		public override void nextOpportunity ()
		{

			_doItForThem = false;

			if(stuff != null)
			{
				foreach(FilterMovieClip fmc in stuff)
					fmc.Destroy();
			}
			
			if(currentOpportunity == totalOpportunities)
			{
				tallyAnswers();
				return;
			}
			
			opportunityComplete = false;
			timeoutCount = 0;
			failCount = 0;
			
			if(_level != SkillLevel.Tutorial)
			{
				opportunityAnswered = false;
			}
			stuffOff(true);
			stuff = new List<FilterMovieClip>();
			
			currentSet = setList[currentOpportunity];
				
			setupOpp();
			
			AddNewOppData( audioNames.IndexOf( MeasurementSounds.SOUND_13 + currentSet[1] + "-dinosaur" ) );
			
			foreach( FilterMovieClip fmc in stuff )
			{
				AddNewObjectData( formatObjectData( fmc.name, fmc.parent.localToGlobal(new Vector2(fmc.x, fmc.y)), fmc.name == currentSet[1] ) );
			}
			
			currentOpportunity++;
			
			fadeInContainer();
			dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_START) );
			if(_level != SkillLevel.Tutorial)
				OnAudioWordComplete(NOW_ITS);
			else
				playSound (MeasurementSounds.SOUND_3, NOW_ITS);
		}
		
		private void setupOpp ()
		{
			FilterMovieClip tempClip;
			int numThings = currentSet.Count - 1;
			
			int variations;
			if(currentSet[0] == PERSON)
			{
				background.gotoAndStop(1);
				variations = 8;
			}
			else
			{
				background.gotoAndStop(2);
				variations = 9;
			}
			
			int r;
			if(currentSet[1].Substring(0, 1) == "s")
				r = 0;
			else
				r = variations + 1;
			float SorF = Random.value;
			for(int i = 1; i < currentSet.Count; i++)
			{
				if(currentSet[1].Substring(0, 1) == "s")
					r = Mathf.CeilToInt(Random.Range(r + 0.001f, variations - (numThings - i)));
				else
					r = Mathf.FloorToInt(Random.Range((numThings - i) + 1f, r - 0.001f));
				if(currentSet[0] == OBJECT)
				{
					tempClip = new FilterMovieClip(MovieClipFactory.CreateMeasurementObjects());
				}
				else if(SorF < 0.5f)
				{
					tempClip = new FilterMovieClip(MovieClipFactory.CreateMeasurementPeopleFront());
				}
				else
				{
					tempClip = new FilterMovieClip(MovieClipFactory.CreateMeasurementPeopleSide());
				}
				tempClip.Target.gotoAndStop(r);
				tempClip.name = currentSet[i];
				tempClip.scaleX = tempClip.scaleY = baseScale;
				stuff.Add(tempClip);
			}
			if(_level == SkillLevel.Tutorial)
			{
				stuff[0].Target.gotoAndStop(1);
				stuff[1].Target.gotoAndStop(6);
			}
			stuff.Shuffle();
			
			float fraction = 1f / (float)numThings;
			
			Rectangle bounds = background.getBounds(container);
			if( currentSet[0] == OBJECT )
			{
				bounds.y += 110 * background.scaleY;
				bounds.height -= 110 * background.scaleY;
			}
			
			for(int j = 0; j < stuff.Count; j++)
			{
				tempClip = stuff[j];
				if(currentSet[0] == OBJECT)
				{
					tempClip.x = bounds.x + 20;
					tempClip.y = bounds.y + (bounds.height * j * fraction) + ( bounds.height * fraction * 0.5f);
				}
				else
				{
					tempClip.y = bounds.bottom - 20;
					tempClip.x = bounds.x + (bounds.width * j * fraction) + ( bounds.width * fraction * 0.5f);
				}
				container.addChild(tempClip);
			}
		}
		
		private void fadeInContainer ()
		{
			container.visible = true;
			Tweener.addTween(container, Tweener.Hash("time", 0.25f, "alpha", 1f));
		}
		
		private void fadeOutContainer ()
		{
			Tweener.addTween(container, Tweener.Hash("time", 0.125f, "alpha", 0f)).OnComplete(fullHide);
			if(_level == SkillLevel.Tutorial)
				Tweener.addTween(tutorial_mc, Tweener.Hash("time", 0.125f, "alpha", 0f));
		}
		private void fullHide () { if(opportunityComplete) container.visible = false; }
		
		private void stuffOn ()
		{
			if(stuff == null || stuff.Count == 0)
				return;
			
			foreach(FilterMovieClip fmc in stuff)
			{
				if(!fmc.hasEventListener(MouseEvent.MOUSE_DOWN) && !(_level == SkillLevel.Tutorial && fmc.name != currentSet[1]))
					fmc.addEventListener(MouseEvent.MOUSE_DOWN, onDown);
				glowOff(fmc);
			}
			if(_level != SkillLevel.Tutorial)
				MarkInstructionEnd();
			startTimer();
		}
		
		private void stuffOff () { stuffOff(false); }
		private void stuffOff (bool nuke)
		{
			if(stuff == null)
				return;
			if(stuff.Count == 0)
				return;
			
			foreach(FilterMovieClip fmc in stuff)
			{
				fmc.removeAllEventListeners(MouseEvent.MOUSE_DOWN);
				if(nuke)
				{
					fmc.Destroy();
					fmc.parent.removeChild(fmc);
				}
			}
			if(nuke)
				stuff.Clear();
			MarkInstructionStart();
			stopTimer();
		}
		
		private void onDown ( CEvent e )
		{
			stuffOff();
			currentThing = e.currentTarget as FilterMovieClip;
			timeoutCount = 0;
			
			PlayClickSound();
			
			stage.addEventListener(MouseEvent.MOUSE_UP, onUp);
		}
		
		private void onUp (CEvent e )
		{
			stage.removeEventListener(MouseEvent.MOUSE_UP, onUp);
			glowOn(currentThing);
			
			if(currentThing.name == currentSet[1])
			{
				if(!opportunityAnswered)
				{
					opportunityAnswered = true;
					opportunityCorrect = true;
				}
				opportunityComplete = true;
				
				PlayCorrectSound();
				
				AddNewActionData( formatActionData( "tap", stuff.IndexOf( currentThing ), Vector2.zero, true ) );
			}
			else
			{
				opportunityAnswered = true;
				opportunityCorrect = false;
				
				failCount++;
				if(failCount >= 3)
				{
					_doItForThem = true;
					PlayIncorrectSound();

				}
				else
				{
					PlayIncorrectSound();
				}
				
				AddNewActionData( formatActionData( "tap", stuff.IndexOf( currentThing ), Vector2.zero, false ) );
			}
		}
		
		private void doItForThem ()
		{
			stuffOff();
			opportunityComplete = true;
			foreach(FilterMovieClip fmc in stuff)
			{
				if(fmc.name == currentSet[1])
					glowOn(fmc);
				else
					glowOff(fmc);
			}
			
			playSound(MeasurementSounds.SOUND_28 + currentSet [1] + "-dinosaur", NOW_LETS);
		}
		
		public override void onTimerComplete ()
		{
			base.onTimerComplete ();
			timeoutCount++;
			
			string clipName = "";
			if(_level == SkillLevel.Tutorial)
			{
				if(timeoutCount == 1)
				{
					playSound(MeasurementSounds.SOUND_3, NOW_ITS);
					clipName = MeasurementSounds.SOUND_3;
				}
				else
				{
					if(opportunityAnswered)
					{
						opportunityComplete = true;
						opportunityCorrect = false;
						OnAudioWordComplete(NOW_LETS);
						return;
					}
					opportunityAnswered = true;
					opportunityCorrect = true;
					
					playSound(MeasurementSounds.SOUND_5, AGAIN);
					clipName = MeasurementSounds.SOUND_5;
				}
			}
			else
			{
				switch(timeoutCount)
				{
				case 1:
					OnAudioWordComplete(NOW_ITS);
					clipName = MeasurementSounds.SOUND_13 + currentSet[1] + "-dinosaur";
					break;
				case 2:
					playSound(MeasurementSounds.SOUND_6, PLEASE_FIND);
					clipName = MeasurementSounds.SOUND_6;
					break;
				case 3:
					opportunityAnswered = true;
					opportunityCorrect = false;
					
					foreach(FilterMovieClip fmc in stuff)
					{
						if(fmc.name == currentSet[1])
							glowOn(fmc);
						else
							glowOff(fmc);
					}
					OnAudioWordComplete(NOW_ITS);
					
					clipName = MeasurementSounds.SOUND_13 + currentSet[1] + "-dinosaur";
					break;
				case 4:
					timeoutEnd = true;
					doItForThem();
					
					clipName = MeasurementSounds.SOUND_28 + currentSet [1] + "-dinosaur";
					break;
				}
			}
			
			if( !string.IsNullOrEmpty( clipName ) )
			{
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf( clipName ) );
			}
		}
		
		private void glowOn ( FilterMovieClip fmc )
		{
			while(fmc.filters.Count > 1)
			{
				fmc.filters[1].Destroy();
				fmc.filters[1].Visible = false;
				fmc.filters.RemoveAt(1);
			}
			if(fmc.filters.Count == 0)
				fmc.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
			fmc.filters[0].Visible = true;
		}
		
		private void glowOff () {
			foreach(FilterMovieClip fmc in stuff)
				glowOff(fmc); }
		private void glowOff ( FilterMovieClip fmc )
		{
			while(fmc.filters.Count > 0)
			{
				fmc.filters[0].Destroy();
				fmc.filters[0].Visible = false;
				fmc.filters.RemoveAt(0);
			}
		}
		
		private void playSound ( string clipName ) { playSound(clipName, ""); }
		private void playSound ( string clipName, string word )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.MEASUREMENT, clipName );
			
			if(word != "")
				bundle.AddClip(clip, word, clip.length);
			else
				bundle.AddClip(clip);
			
			SoundEngine.Instance.PlayBundle(bundle);
		}
		
		public override void Resize (int _width, int _height)
		{
			container.scaleX = container.scaleY = (_height / (float)MainUI.STAGE_HEIGHT);
			container.y = 0;
			container.x = ( _width - ( MainUI.STAGE_WIDTH * container.scaleX ) ) / 2f;
		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = "";
			
			foreach(List<string> ls in setList)
			{
				if(data != "")
					data = data + ";";
				foreach(string s in ls)
				{
					if(data.LastIndexOf(';') != data.Length - 1)
						data = data + "-";
					data = data + s;
				}
			}
			return data;
		}
		
		private void parseSessionData (string data)
		{
			setList = new List<List<string>>();
			
			string[] sData = data.Split(';');
			
			foreach( string s in sData )
			{
				setList.Add(s.Split('-').ToListUtil());
			}
		}
		
		private static List<string> audioNames = new List<string>
		{	"",
			"Narration/3C-Story Introduction Audio/3C-story-introduction",
			"Narration/3C-Skill Instruction Audio/3C-watch-closely-as-platty",
			"Narration/3C-Skill Instruction Audio/3C-now-its-your-turn",
			"Narration/3C-Skill Instruction Audio/3C-lets-measure-more-dinosaurs",
			"Narration/3C-Skill Instruction Audio/3C-try-again",
			"Narration/3C-Skill Instruction Audio/3C-touch-the-green-button",
			"Narration/3C-Skill Instruction Audio/3C-Initial Instructions/3C-touch-longer-dinosaur",
			"Narration/3C-Skill Instruction Audio/3C-Initial Instructions/3C-touch-longest-dinosaur",
			"Narration/3C-Skill Instruction Audio/3C-Initial Instructions/3C-touch-shorter-dinosaur",
			"",
			"Narration/3C-Skill Instruction Audio/3C-Initial Instructions/3C-touch-shortest-dinosaur",
			"",
			"Narration/3C-Skill Instruction Audio/3C-Initial Instructions/3C-touch-taller-dinosaur",
			"Narration/3C-Skill Instruction Audio/3C-Initial Instructions/3C-touch-tallest-dinosaur",
			"Narration/3C-Reinforcement Audio/3C-yes-shorter-dinosaur",
			"Narration/3C-Reinforcement Audio/3C-look-again-longer-dinosaur",
			"Narration/3C-Reinforcement Audio/3C-look-again-shorter-dinosaur",
			"",
			"Narration/3C-Reinforcement Audio/3C-look-again-taller-dinosaur",
			"Narration/3C-Reinforcement Audio/3C-chosen-longer-dinosaur",
			"Narration/3C-Reinforcement Audio/3C-chosen-longest-dinosaur",
			"Narration/3C-Reinforcement Audio/3C-chosen-shorter-dinosaur",
			"",
			"Narration/3C-Reinforcement Audio/3C-chosen-shortest-dinosaur",
			"",
			"Narration/3C-Reinforcement Audio/3C-chosen-taller-dinosaur",
			"Narration/3C-Reinforcement Audio/3C-chosen-tallest-dinosaur",
			"Narration/3C-Inactivity Reinforcement Audio/3C-this-longer-dinosaur",
			"Narration/3C-Inactivity Reinforcement Audio/3C-this-longest-dinosaur",
			"Narration/3C-Inactivity Reinforcement Audio/3C-this-shorter-dinosaur",
			"",
			"Narration/3C-Inactivity Reinforcement Audio/3C-this-shortest-dinosaur",
			"",
			"Narration/3C-Inactivity Reinforcement Audio/3C-this-taller-dinosaur",
			"Narration/3C-Inactivity Reinforcement Audio/3C-this-tallest-dinosaur",
			"User-Earns-Artifact"
		};
	}

	public class MeasurementSounds
	{
		public static string SOUND_1 = "Narration/3C-Story Introduction Audio/3C-story-introduction";
		public static string SOUND_2 = "Narration/3C-Skill Instruction Audio/3C-watch-closely-as-platty";
		public static string SOUND_3 = "Narration/3C-Skill Instruction Audio/3C-now-its-your-turn";
		public static string SOUND_4 = "Narration/3C-Skill Instruction Audio/3C-lets-measure-more-dinosaurs";
		public static string SOUND_5 = "Narration/3C-Skill Instruction Audio/3C-try-again";
		public static string SOUND_6 = "Narration/3C-Skill Instruction Audio/3C-touch-the-green-button";
		public static string SOUND_13 = "Narration/3C-Skill Instruction Audio/3C-Initial Instructions/3C-touch-";
		public static string SOUND_15 = "Narration/3C-Reinforcement Audio/3C-yes-shorter-dinosaur";
		public static string SOUND_16 = "Narration/3C-Reinforcement Audio/3C-look-again-";
		public static string SOUND_20 = "Narration/3C-Reinforcement Audio/3C-chosen-";
		public static string SOUND_28 = "Narration/3C-Inactivity Reinforcement Audio/3C-this-";
		public static string SOUND_36 = "Narration/3C-Story Introduction Audio/3C-end-of-game-get-artifact";
	}
}