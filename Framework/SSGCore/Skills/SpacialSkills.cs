using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;

namespace SSGCore
{

	public class SpacialSkills : BaseSkill
	{
		
		//Labels
		private const string ABOVE = "Above";
		private const string BESIDE = "Beside";
		private const string FARFROM = "FarFrom";
		private const string IN = "In";
		private const string NEAR = "Near";
		private const string NEXTTO = "NextTo";
		private const string ON = "On";
		private const string OVER = "Over";
		private const string UNDER = "Under";
		
		//Types
		private const string BIRD = "BIRD";
		private const string BUG = "DRAGON";
		private const string ASTRONAUGHT = "PARACHUTE";
		private const string BOOK = "BUTTERFLY";
				
		//Positions
		//Tree
		// 3/31/2015 - Tree Now BIRD
		private Vector2[] treePositionsLo = new Vector2[]{	new Vector2(498, 107), 	//ABOVE
															new Vector2(696, 275),	//BESIDE
															new Vector2(11, 381),	//FARFROM
															new Vector2(457, 275),	//IN
															new Vector2(697, 275),	//NEAR
															new Vector2(698, 275),	//NEXTTO
															new Vector2(339, 195),	//ON
															new Vector2(499, 107),	//OVER
															new Vector2(451, 472)};	//UNDER
		
		//Log
		// 3/31/2015 - Log Now DRAGON
		private Vector2[] logPositionsLo = new Vector2[]{	new Vector2(456, 132), 	//ABOVE
															new Vector2(726, 326),	//BESIDE
															new Vector2( 24, 250),	//FARFROM
															new Vector2(455, 268),	//IN
															new Vector2(727, 326),	//NEAR
															new Vector2(728, 326),	//NEXTTO
															new Vector2(625, 245),	//ON
															new Vector2(457, 132),	//OVER
															new Vector2(416, 433)};	//UNDER
			
		//Shuttle
		// 3/31/2015 - Shuttle Now PARACHUTE
		private Vector2[] shuttlePositionsLo =new Vector2[]{new Vector2(455f, 107f), 	//ABOVE
															new Vector2(254, 245f),	//BESIDE
															new Vector2(946, 276f),	//FARFROM
															new Vector2(461, 385),	//IN 
															new Vector2(255, 245),	//NEAR
															new Vector2(256, 245),	//NEXTTO
															new Vector2(615f, 194f),	//ON
															new Vector2(456f, 107f),	//OVER
															new Vector2(443f, 456f)};	//UNDER
		//Bag
		// 3/31/2015 - Bag Now BUTTERFLY
		private Vector2[] bagPositionsLo = new Vector2[]{	new Vector2(453, 93), 		//ABOVE
															new Vector2(286, 276),	//BESIDE
															new Vector2(897, 356),	//FARFROM
															new Vector2(419, 246),		//IN
															new Vector2(287, 276),	//NEAR
															new Vector2(288, 276),	//NEXTTO
															new Vector2(569, 151),	//ON
															new Vector2(454, 93),		//OVER
															new Vector2(443, 459)};	//UNDER

		//---------------------------------------------------------------------------------------------------------------- Hi
		
		//Positions
		//Tree
		// 3/31/2015 - Tree Now BIRD
		private Vector2[] treePositionsHi = new Vector2[]{		new Vector2(994,214), 	//ABOVE
																new Vector2(1390,550),	//BESIDE
																new Vector2(16, 764),	//FARFROM
																new Vector2(912, 550),	//IN
																new Vector2(1391,550),	//NEAR
																new Vector2(1392,550),	//NEXTTO
																new Vector2(676,390),	//ON
																new Vector2(995,214),	//OVER
																new Vector2(900, 944)};	//UNDER
		
		//Log
		// 3/31/2015 - Log Now DRAGON
		private Vector2[] logPositionsHi = new Vector2[]{		new Vector2(906,263), 	//ABOVE
																new Vector2(1418,613),	//BESIDE
																new Vector2( 42,506),	//FARFROM
																new Vector2(895, 544),	//IN
																new Vector2(1419,613),	//NEAR
																new Vector2(1420,613),	//NEXTTO
																new Vector2(1245,492),	//ON
																new Vector2(907,263),	//OVER
																new Vector2(824,868)};	//UNDER
		
		//Shuttle
		// 3/31/2015 - Shuttle Now PARACHUTE
		private Vector2[] shuttlePositionsHi = new Vector2[]{	new Vector2(920,262), 	//ABOVE
																new Vector2(562,506),	//BESIDE
																new Vector2(1894,553),	//FARFROM
																new Vector2(930, 730),	//IN
																new Vector2(563,506),	//NEAR
																new Vector2(564,506),	//NEXTTO
																new Vector2(1247,416),	//ON
																new Vector2(921,262),	//OVER
																new Vector2(900,965)};	//UNDER
		
		//Bag
		// 3/31/2015 - Bag Now BUTTERFLY
		private Vector2[] bagPositionsHi = new Vector2[]{		new Vector2(910,182), 		//ABOVE
																new Vector2(576,554),	//BESIDE
																new Vector2(1803,711),	//FARFROM
																new Vector2(867, 493),		//IN
																new Vector2(577,554),	//NEAR
																new Vector2(578,554),	//NEXTTO
																new Vector2(1140,305),	//ON
																new Vector2(911,182),		//OVER
																new Vector2(892,925)};	//UNDER
		
		private Vector2 treeOffsetsLo = new Vector2( -10, 40 );//new Vector2( 450, 210 );
		private Vector2 logOffsetsLo = new Vector2( -10, 40 );//new Vector2( 452, 215 );
		private Vector2 shuttleOffsetsLo = new Vector2( 0, 40 );//new Vector2( 440, 205 );
		private Vector2 bagOffsetsLo = new Vector2( 0, 40 );//new Vector2( 430, 225 );
		
		private Vector2 treeOffsetsHi = new Vector2( 20, 70 );
		private Vector2 logOffsetsHi = new Vector2( -20, 83 );
		private Vector2 shuttleOffsetsHi = new Vector2( -5, 85 );
		private Vector2 bagOffsetsHi = new Vector2( -30, 70 );
		
		private Vector2[] currentPositions;
		private Vector2 currentOffset;
		
		private List<string> labels;
		
		private List<string[]> egOpportunities = new List<string[]>{new string[]{OVER, UNDER},
															new string[]{UNDER, OVER},
															new string[]{FARFROM, NEAR},
															new string[]{NEAR, FARFROM},
															new string[]{UNDER, ABOVE},
															new string[]{ABOVE, UNDER},
															new string[]{IN, NEXTTO},
															new string[]{NEXTTO, IN},
															new string[]{BESIDE, IN},
															new string[]{ON, FARFROM}	};
		
		private List<string[]> dgOpportunities = new List<string[]>{new string[]{OVER, UNDER, IN},
															new string[]{UNDER, OVER, IN},
															new string[]{FARFROM, NEAR, ON},
															new string[]{NEAR, FARFROM, ON},
															new string[]{IN, UNDER, ABOVE},
															new string[]{ABOVE, UNDER, IN},
															new string[]{IN, NEXTTO, FARFROM},
															new string[]{NEXTTO, IN, FARFROM},
															new string[]{BESIDE, IN, FARFROM},
															new string[]{ON, FARFROM, IN}	};
		
		private List<string[]> ddOpportunities = new List<string[]>{new string[]{OVER, UNDER, IN, ON},
															new string[]{UNDER, OVER, IN, ON},
															new string[]{FARFROM, NEAR, ON, IN},
															new string[]{NEAR, FARFROM, ON, IN},
															new string[]{ON, UNDER, ABOVE, IN},
															new string[]{ABOVE, UNDER, IN, ON},
															new string[]{IN, NEXTTO, FARFROM, ON},
															new string[]{NEXTTO, IN, FARFROM, ON},
															new string[]{BESIDE, IN, FARFROM, ON},
															new string[]{ON, BESIDE, FARFROM, IN}	};


		private SpacialSkills_Object clickedMC;
		
		private List<string> _currentArray;
		private List<string> currentArray
		{
			get{ return _currentArray; }
			set
			{

				if(value != null)
				{
					_currentArray = value;
					for(int i = 0; i < objects.Count; i++)
					{
						var obj = objects[i];
						obj.visible = _currentArray != null && _currentArray.Contains(objects[i].OrientationType);
						Debug.Log(string.Format( "objects {0} orientation type: {1}, _currentArray contains?: {2}", i, objects[i].OrientationType, _currentArray.Contains(objects[i].OrientationType)));
						obj.alpha = 1;
						Tweener.removeTweens(obj);
					}
				}
			}
		}
		
		private List<string> _availableTypes = new List<string>() { BIRD, BUG, ASTRONAUGHT, BOOK };
		private string _currentType;
		public string currentType
		{
			get{ return _currentType; }	
			set
			{
				DebugConsole.LogError("_currentType Value: " + value);
				
				_currentType = value;
				
				switch(value)
				{
					case BIRD:
						if( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA )
						{
							currentPositions = treePositionsHi;
							currentOffset = treeOffsetsHi;	
						}
						else{
							currentPositions = treePositionsLo;
							currentOffset = treeOffsetsLo;
						}
					break;
					
					case BUG:
						if( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA )
						{
							currentPositions = logPositionsHi;
							currentOffset = logOffsetsHi;	
						}
						else{
							currentPositions = logPositionsLo;
							currentOffset = logOffsetsLo;
						}	
					break;
					
					case ASTRONAUGHT:
						if( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA )
						{
							currentPositions = shuttlePositionsHi;
							currentOffset = shuttleOffsetsHi;	
						}
						else{
						currentPositions = shuttlePositionsLo;
							currentOffset = shuttleOffsetsLo;
						}
					break;
					
					case BOOK:
						if( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA )
						{
							currentPositions = bagPositionsHi;
							currentOffset = bagOffsetsHi;	
						}
						else{
							currentPositions = bagPositionsLo;
							currentOffset = bagOffsetsLo;
						}
					break;
				}
				
				for(int i = 0; i < objects.Count; i++)
				{
					objects[i].Destroy();
					objects[i].Target.gotoAndStop(value);

					int tempOrientationIndex = System.Array.IndexOf( currentPositions, new Vector2( currentPositions[i].x, currentPositions[i].y ) );

					string tempOrientationType = labels[tempOrientationIndex];
					DebugConsole.Log("tempOrientationType {0}: {1}, value: {2}", i, tempOrientationType, value);
					//DebugConsole.Log("Spatial Skills positionScale: " + positionScale);

					objects[i].OrientationType = tempOrientationType;
					objects[i].x =  currentOffset.x + currentPositions[i].x;
					objects[i].y = currentOffset.y + currentPositions[i].y;
					//DebugConsole.Log(" objects[i].Target.numChildren: " + objects[i].Target.numChildren);
					//objects[i].AddFilter( FiltersFactory.GetYellowGlowFilter() );
				}

				DebugConsole.Log("objectContainer.scaleX: " + objectContainer.scaleX);
				 
				backgroundObject.gotoAndStop(value);
				backgroundObject.x = (objectContainer.width * 0.5f);// - (backgroundObject.width * objectContainer.scaleX * 0.5f);
				backgroundObject.y = (objectContainer.height * 0.5f);// - (backgroundObject.height * objectContainer.scaleY * 0.5f);
				
			}
		}

//		private float positionScale;

		private SpacialSkills_Object incorrectObject;
		
		private DisplayObjectContainer objectContainer;
		
		private MovieClip backgroundObject;
		
		private List<SpacialSkills_Object> objects;
		
		private new SpacialSkillsTutorial tutorial_mc;
		
		public SpacialSkills( SkillLevel level, SessionInfo  currentSession ) : base( level, currentSession)
		{
			BACKGROUND = "Background";
			ARTIFACT = "Artifact";

			_level = level;
			
			Init(null);
			
		}
		
		private string currentOrientationTypeClick;
		private string currentObjectClick;
		
		public override void Init (MovieClip parent)
		{

			base.Init (parent);

			labels = new List<string>(){ABOVE, BESIDE, FARFROM, IN, NEAR, NEXTTO, ON, OVER, UNDER};
			
			tutorial_mc = new SpacialSkillsTutorial();
			
			objectContainer = new DisplayObjectContainer();
			objectContainer.width = (PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA) ? 2048 : 1024;
			objectContainer.height = (PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA) ? 1535 : 768;
			objectContainer.alpha = 0;
			addChild(objectContainer);
			
			backgroundObject = new MovieClip("SpacialSkills.swf", "BackGround_Object");
			objectContainer.addChild(backgroundObject);
			
			objects = new List<SpacialSkills_Object>();
			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;

//			positionScale = 1;//(float)Screen.height / (float)MainUI.STAGE_HEIGHT;
			
			foreach(string lb in labels)
			{
				DebugConsole.Log("looking for mc: " + lb);
				MovieClip objMC = new MovieClip( "SpacialSkills.swf", lb );
				SpacialSkills_Object filterObj = new SpacialSkills_Object( objMC, lb );
				//filterObj.addEventListener( MouseEvent.MOUSE_DOWN, onObjectDown );
				objectContainer.addChild( filterObj );
				objects.Add( filterObj );
			}
			
			int ranNum = Mathf.FloorToInt(Random.Range(0, _availableTypes.Count));
			if(ranNum == _availableTypes.Count)
				ranNum--;
						
			switch(_level)
			{
				case SkillLevel.Tutorial:
					totalOpportunities = 1;
					opportunityComplete = false;
					currentType = BUG;
					currentArray = new List<string>(){ON, UNDER};
					if(resumingSkill) {
						nextOpportunity();
					} else {
						tutorial_mc.View.addEventListener( TutorialEvent.START, onTutorialStart);
						tutorial_mc.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete);
						//addChild( tutorial_mc.View );
						tutorial_mc.Resize( Screen.width, Screen.height );
					}
				break;
				
				default:
					totalOpportunities = 10;
				
					currentType = _availableTypes[ ranNum ];
					_availableTypes.RemoveAt(ranNum);
				
					if( resumingSkill )
					{
						parseSessionData( currentSession.sessionData );
					//	nextOpportunity();
					}
				break;
			}

			Resize( Screen.width, Screen.height );

			backgroundObject.x = (objectContainer.width * 0.5f);// - (backgroundObject.width * 0.5f);
			backgroundObject.y = (objectContainer.height * 0.5f);// - (backgroundObject.height * 0.5f);

		}
		
		public override void Start()
		{
			MarkInstructionStart();
		}
		
		public void onTutorialComplete( CEvent e )
		{
			
			tutorial_mc.View.removeEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.View.removeEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			removeChild( tutorial_mc.View );
			nextOpportunity();
			
		}
		
		public override void nextOpportunity ()
		{
			MarkInstructionStart();
			timeoutCount = 0;
			
			foreach(SpacialSkills_Object so in objects)
			{
				so.Target.mouseEnabled = true;
			}
			if(incorrectObject != null)
				incorrectObject = null;
			
			if( _availableTypes.Count == 0)
			{
				_availableTypes.Clear();
				_availableTypes = new List<string>() {BIRD, BUG, ASTRONAUGHT, BOOK};
			}
			
			switch(_level)
			{
				case SkillLevel.Tutorial:
					if(currentOpportunity > 0)
					{
						tallyAnswers();
					}
					else
					{
						currentOpportunity++;
					
						currentType = BUG;
						currentArray = new List<string>(){ON};
						objectContainer.alpha = 1;
						OpportunityStart();
					}
				break;
				
				case SkillLevel.Emerging:
					setNextOpportunity(egOpportunities);
				break;
				
				case SkillLevel.Developing:
					setNextOpportunity(dgOpportunities);
				break;
				
				case SkillLevel.Developed:
				case SkillLevel.Completed:
					setNextOpportunity(ddOpportunities);
				break;
				
			}
			
		}
		
		private void setNextOpportunity( List<string[]> level_opportunities )
		{

			Debug.Log("currentOpportunity: " + currentOpportunity);

			List<List<string>> tempOpportunities = new List<List<string>>();


			for(int i = 0; i < level_opportunities.Count; i++)
			{
				List<string> layerStrings = new List<string>();
				for(int j = 0; j < level_opportunities[i].GetLength(0); j++)
				{
					layerStrings.Add(level_opportunities[i][j]);
				}
				tempOpportunities.Add(layerStrings);
			}
			
			int ranNum;
			
			if(currentOpportunity > 0)
			{
				if(opportunityComplete)
				{
					if(opportunityCorrect)
					{
						//NumCorrect++;
					}
					else
					{
						//NumIncorrect++;
					}
				}
				else
				{
					//NumIncorrect++;
				}
			}
			if(currentOpportunity < 10)
			{
				opportunityComplete = false;
				opportunityCorrect = false;
				opportunityAnswered = false;
			
				currentOpportunity++;
			
				if(currentOpportunity != 1 && !resumingSkill)
				{
					ranNum = Mathf.FloorToInt( Random.Range(0, _availableTypes.Count) );
					if(ranNum == _availableTypes.Count)
						ranNum--;
					currentType = _availableTypes[ ranNum ];
					_availableTypes.RemoveAt( ranNum );
				}
			
				ranNum = Mathf.FloorToInt( Random.Range(0, tempOpportunities.Count) );
				if(ranNum == tempOpportunities.Count)
					ranNum--;
				if(resumingSkill)
				{
					ranNum = 0;
					resumingSkill = false;
				}
				currentArray = tempOpportunities[ ranNum ];
				tempOpportunities.RemoveAt( ranNum );
				level_opportunities.RemoveAt( ranNum );
							
				fadeInContainer().OnComplete( OpportunityStart );
				
			}
			else
			{
				//_percent = Mathf.Round((NumCorrect / (NumCorrect + NumIncorrect)) * 100);
				stopTimer();
				currentArray = null;
				tallyAnswers();
			}
		}
		
		public override void OpportunityStart()
		{
			string clipName = "";
			switch(_level)
			{
				case SkillLevel.Tutorial:
					clipName = playInstructionAudio( "3B-now-its-your-turn", NOW_ITS );
				break;
				default:
					clipName = playFindCurrentAudio();
				break;
			}
			
			AddNewOppData( audioNames.IndexOf( clipName ) );
			
			int id = 0;
			foreach( SpacialSkills_Object obj in objects )
			{
				if(obj.visible)
				{
					obj.id = id;
					id++;
					
					Vector2 point = obj.parent.localToGlobal( new Vector2( obj.x, obj.y ) );
					AddNewObjectData( formatObjectData( currentType + "_" + obj.OrientationType, point, obj.OrientationType == currentArray[0] ) );
				}
			}
		}

		private void HideIncorrectObject ()
		{
			if(incorrectObject != null)
			{
				if(incorrectObject.filters.Count > 0)
				{
					foreach(MovieClipFilter f in incorrectObject.filters)
					{
						f.Visible = false;
					}
				}
				//var obj = incorrectObject;
				Tweener.addTween( incorrectObject, Tweener.Hash("time", 0.35f, "alpha", 0, "transition",Tweener.TransitionType.easeInOutQuad) )
					.OnComplete( nukeIncorrectObject );
				incorrectObject.Target.mouseEnabled = true;
				//incorrectObject = null;
			}
		}
		private void nukeIncorrectObject()
		{
			incorrectObject.Target.mouseEnabled = false;
			incorrectObject = null;
		}
		
		public override void OnAudioWordComplete( string word )
		{
			
			
			if(opportunityComplete)
			{
				switch(word)
				{
					case YOU_FOUND:
						if(_level == SkillLevel.Tutorial)
						{
							playInstructionAudio("3B-what-else-do-you-see", NOW_LETS);
						}
						else
						{							
							goto case THIS_IS;
						}
					break;
					
					case THEME_COMPLIMENT:
						playFoundCurrentAudio();
					break;
					
					case INTRO:
					case PLEASE_FIND:
					case NOW_ITS:
					case NOW_LETS:
					case YOU_FOUND_INCORRECT:
					case THIS_IS :
						Tweener.addTween( objectContainer, Tweener.Hash("time",0.35f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuint) );
						if(opportunityCorrect)
						{
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
						}
						else
						{
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
						}
					break;
					
					case THEME_INTRO:
						playIntro();
					break;
					
					case CLICK_CORRECT:
						dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
						break;
				}
			}
			else
			{
				switch(word)
				{
					
					case THEME_CRITICISM:
						playFoundCurrentAudio();
					break;
					
					case YOU_FOUND:
					case YOU_FOUND_INCORRECT:
					case NOW_ITS:
						HideIncorrectObject();
						playFindCurrentAudio();
					break;
										
					case INTRO:
						if(_level == SkillLevel.Tutorial)
						{
							addChild(tutorial_mc.View);
							tutorial_mc.OnStart();
						}
						else
						{
							nextOpportunity();
						}
					break;
					
					case TRY_AGAIN:
						resetTutorial();
					break;
					
					case NOW_LETS:
					case THIS_IS :
						HideIncorrectObject();	
						goto case PLEASE_FIND;
					case PLEASE_FIND:
						startTimer();
						enableObjects();
					break;
					
					case THEME_INTRO:
						if( !currentSession.skipIntro )
						{
							playIntro();
						} else {
							OnAudioWordComplete( INTRO );
						}
					break;
					
					case CLICK_INCORRECT:
						dispatchEvent( new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
					break;
					
				}
			}
			
		}
		
		public override void onTimerComplete()
		{
			base.onTimerComplete();
			
			stopTimer();
			
			timeoutCount++;
			Debug.Log("Time has run out. Count: " + timeoutCount);
			
			string clipName = "";
			
			switch( timeoutCount )
			{
				case 1:
					if(_level == SkillLevel.Tutorial)
						clipName = playInstructionAudio( "3B-now-its-your-turn", NOW_ITS );
					else
						clipName = playFindCurrentAudio();
				break;
				
				case 2:
					if(_level == SkillLevel.Tutorial)
					{
						if(opportunityAnswered)
						{
							opportunityComplete = true;
							opportunityCorrect = false;
							dispatchEvent(new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0"));
							return;
						}
						opportunityAnswered = true;
						opportunityCorrect = true;
						clipName = playInstructionAudio( "3B-try-again", TRY_AGAIN );
					}
					else
					{
						clipName = playInstructionAudio( "3B-touch-the-green-button", NOW_LETS );
					}
				break;
				
				case 3:
					if(!opportunityAnswered)
					{
						opportunityAnswered = true;
						opportunityCorrect = false;
					}
				
					for(int i = 0; i < objects.Count; i++)
					{
						if(objects[i].OrientationType == currentArray[0])
						{
							AddGlow(objects[i]);
							break;
						}
					}
				
					clipName = playFindCurrentAudio();
				
				break;
				
				case 4:
					timeoutEnd = true;
					if(!opportunityAnswered)
					{
						opportunityAnswered = true;
						opportunityCorrect = false;
					}
					opportunityComplete = true;
				
					for(int i = 0; i < objects.Count; i++)
					{
						if(objects[i].OrientationType == currentArray[0])
						{
							AddGlow(objects[i]);
							currentOrientationTypeClick = currentArray[0];
							//break;
						}	
						else
						{
							Tweener.addTween( objects[i], Tweener.Hash( "alpha", 0, "time",0.35f ) );
						}
					}
					clipName = playThisCurrentAudio();
				
				break;
				
				default:
					timeoutCount = 0;
					onTimerComplete();
				break;
			}
			
			if( !string.IsNullOrEmpty( clipName) )
			{
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf( clipName ) );
			}
		}

		void AddGlow (SpacialSkills_Object clicked)
		{
			clicked.Destroy();
			clicked.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
		}
		
		private void onObjectDown( CEvent e )
		{
			PlayClickSound();
			
			clickedMC = e.currentTarget as SpacialSkills_Object;
			addEventListener(MouseEvent.MOUSE_UP, onObjectClick);
		}
		
		private void onObjectClick( CEvent e )
		{
			removeEventListener(MouseEvent.MOUSE_UP, onObjectClick);
			
			SoundEngine.Instance.SendCancelToken();
			//SoundEngine.Instance.StopAll();
			
			stopTimer();
			timeoutCount = 0;
			
			//PlayClickSound();
			
			AddGlow (clickedMC);
			
			Vector2 clickPos = new Vector2( clickedMC.x, clickedMC.y );
			
			DebugConsole.Log("currentPositions: {0}, clickPos: {1}", currentPositions, clickPos);
			
			currentOrientationTypeClick = clickedMC.OrientationType;
			currentObjectClick = currentType;
			
			//ANSWER CORRECT
			if(clickedMC.OrientationType == currentArray[0])
			{
				if(!opportunityAnswered)
				{
					opportunityAnswered = true;
					opportunityCorrect = true;
				}
				opportunityComplete = true;
				
				disableObjects();
				
				PlayCorrectSound();
				
				AddNewActionData( formatActionData( "tap", clickedMC.id, Vector2.zero, true ) );
				
			}
			//ANSWER INCORRECT
			else
			{
				disableObjects();
				
				if(!opportunityAnswered)
				{
					opportunityAnswered = true;
					opportunityCorrect = false;
				}
				
				incorrectObject = clickedMC;
				incorrectObject.Target.mouseEnabled = false;
				
				PlayIncorrectSound();
				
				AddNewActionData( formatActionData( "tap", clickedMC.id, Vector2.zero, false ) );
				
			}
		}
		
		protected override void resetTutorial()
		{
			
			objectContainer.alpha = 0;
			currentOpportunity = 0;
			
			timeoutCount = 0;
			
			DebugConsole.Log("resetting tutorial");
			tutorial_mc.bug.Destroy();

			removeChild( tutorial_mc.View );

			tutorial_mc = null;
			tutorial_mc = new SpacialSkillsTutorial();

			tutorial_mc.View.visible = true;
			tutorial_mc.View.addEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			tutorial_mc.Resize( Screen.width, Screen.height );
			addChild( tutorial_mc.View );
			tutorial_mc.OnStart();
		}
		
		private TweenerObj fadeInContainer()
		{
			return Tweener.addTween(objectContainer, Tweener.Hash("time", 0.35f, "alpha", 1, "transition", Tweener.TransitionType.easeInOutQuint) );
		}
		
		public override void Dispose ()
		{
			base.Dispose();
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			if(objects != null)
			{
				foreach(SpacialSkills_Object obj in objects)
					obj.Destroy();
			}
			if(tutorial_mc != null && tutorial_mc.bug != null)
				tutorial_mc.bug.Destroy();
		}
		
		private string playInstructionAudio( string type, string eventID )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SPATIAL_SKILLS, "3B-Skill-Instruction-Audio/" + type );
			if(clip != null)
			{
				bundle.AddClip(clip, eventID, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else
			{
				DebugConsole.LogError("Instruction Audio NULL");
				OnAudioWordComplete(eventID);
			}
			
			return "3B-Skill-Instruction-Audio/" + type;
		}
		
		private string playFindCurrentAudio()
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			
			string typePath = getTypePath();
			string bgPath = getBackgroundPath();
			string objPath = getObjectPath();
			
			string filePath = "3B-touch-" + objPath + "-" + typePath + bgPath;
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SPATIAL_SKILLS,  "3B-Skill-Instruction-Audio/3B-Initial Instruction/" + filePath );
			if(clip != null)
			{
				bundle.AddClip(clip, PLEASE_FIND, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else
			{
				DebugConsole.LogError("PLEASE_FIND Audio NULL");
				OnAudioWordComplete(PLEASE_FIND);
			}
			
			return "3B-Skill-Instruction-Audio/3B-Initial Instruction/" + filePath;
		}
		
		private string playThisCurrentAudio()
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			
			string typePath = getTypePath( currentOrientationTypeClick );
			if(typePath == "") typePath = getTypePath();
			string bgPath = getBackgroundPath();
			string objPath = getObjectPath( currentType );
			if(objPath == "") objPath = getObjectPath();
			
			string filePath = "3B-this-" + objPath + "-" + typePath + bgPath;
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SPATIAL_SKILLS,  "3B-Inactivity-Reinforcement-Audio/" + filePath );
			if(clip != null)
			{
				bundle.AddClip(clip, THIS_IS, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else
			{
				DebugConsole.LogError("THIS_IS Audio NULL");
				OnAudioWordComplete(THIS_IS);
			}
			
			return "3B-Inactivity-Reinforcement-Audio/" + filePath;
		}
		
		private void playFoundCurrentAudio()
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			
			DebugConsole.Log("currentOrientationTypeClick: {0}", currentOrientationTypeClick);
			
			string typePath = getTypePath( currentOrientationTypeClick );
			string bgPath = getBackgroundPath();
			string objPath = getObjectPath( currentObjectClick );
			
			string filePath = "3B-found-" + objPath + "-" + typePath + bgPath;
			
			DebugConsole.Log("Play Found Audio Path: {0}", filePath);
			
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SPATIAL_SKILLS,  "3B-Reinforcement-Audio/" + filePath );
			if(clip != null)
			{
				bundle.AddClip(clip, YOU_FOUND, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else
			{
				DebugConsole.LogError("YOU_FOUND Audio NULL");
				OnAudioWordComplete(YOU_FOUND);
			}
		}
		
		private void playIntro()
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SPATIAL_SKILLS, "3B-Story-Introduction-Audio/3B-story-introduction" );
			if(clip != null && !currentSession.skipIntro)
			{
				bundle.AddClip(clip, INTRO, clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else
			{
				DebugConsole.LogError("Intro Audio NULL");
				OnAudioWordComplete(INTRO);
			}
		}
		
		public override void Resize( int width, int height )
		{
			this.width = width;
			this.height = height;


			//objectContainer.FitToParent();
			objectContainer.scaleY = height/objectContainer.height;
			objectContainer.scaleX = objectContainer.scaleY;
			objectContainer.y = 0;
			objectContainer.x = (width - (objectContainer.width*objectContainer.scaleX))/2;
		}
		
		private void disableObjects()
		{
			foreach(SpacialSkills_Object spo in objects)
			{
				spo.removeEventListener(MouseEvent.MOUSE_DOWN, onObjectDown);
				MarkInstructionStart();
			}
		}
		
		private void enableObjects()
		{
			foreach(SpacialSkills_Object spo in objects)
			{
				spo.addEventListener(MouseEvent.MOUSE_DOWN, onObjectDown);
				if(spo.filters.Count > 0) spo.filters[0].Visible = false;
				if(_level != SkillLevel.Tutorial) MarkInstructionEnd();
			}
		}
		
		private string getTypePath()
		{
			return getTypePath("");
		}
		
		private string getTypePath( string typeOverride)
		{
			
			if(_currentArray == null || _currentArray.Count < 1) return "";
			
			string type = _currentArray[0];
			if(typeOverride != "") type = typeOverride;
			
			DebugConsole.Log("getTypePath type: " + type);
			
			string typePath = "";
			switch(type)
			{
				case ABOVE:
					typePath = "above";
				break;
				
				case BESIDE:
					typePath = "beside";
				break;
				
				case FARFROM:
					typePath = "far-from";
				break;
				
				case IN:
					typePath = "in";
				break;
				
				case NEAR:
					typePath = "near";
				break;
				
				case NEXTTO:
					typePath = "next-to";
				break;
				
				case ON:
					typePath = "on";
				break;
				
				case OVER:
					typePath = "over";
				break;
				
				case UNDER:
					typePath = "under";
				break;
			}
			
			return typePath;
			
		}
		
		private string getBackgroundPath(  )
		{
			
			string bgPath = "";
			switch(currentType)
			{
				case BUG:
					bgPath = "-airplane";
				break;
				
				case ASTRONAUGHT:
					bgPath = "-airship";
				break;
				
				case BIRD:
					bgPath = "-cloud";
				break;
				
				case BOOK:
					bgPath = "-bubble";
				break;
				
			}
			
			return bgPath;
			
		}
		
		private string getObjectPath()
		{
			return getObjectPath("");
		}
		
		private string getObjectPath( string typeOverride)
		{
			
			string type = currentType;
			if(typeOverride != "") type = typeOverride;
			
			DebugConsole.Log("getObjectPath type: " + type);
			
			string objPath = "";
			switch(type)
			{
				case BUG:
					objPath = "dragon";
				break;
				
				case ASTRONAUGHT:
					objPath = "parachutist";
				break;
				
				case BIRD:
					objPath = "bird";
				break;
				
				case BOOK:
					objPath = "butterfly";
				break;
				
			}
			
			return objPath;
			
		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = _currentType + "&";
			
			foreach(string s in _currentArray)
			{
				if(data.IndexOf('&') != data.Length - 1)
					data = data + ",";
				data = data + s;
			}
			
			List<string[]> levelOpps = new List<string[]>();
			switch(_level)
			{
				case SkillLevel.Emerging:
					levelOpps = egOpportunities;
				break;
				
				case SkillLevel.Developing:
					levelOpps = dgOpportunities;
				break;
				
				case SkillLevel.Developed:
				case SkillLevel.Completed:
					levelOpps = ddOpportunities;
				break;
			}
			
			foreach(string[] sa in levelOpps)
			{
				data = data + "-";
				foreach(string s in sa)
				{
					if(data.LastIndexOf('-') != data.Length - 1)
						data = data + ",";
					data = data + s;
				}
			}
			return data;
		}
		
		private void parseSessionData (string data)
		{
			string[] sData = data.Split('&');
			
			currentType = sData[0];
			sData = sData[1].Split('-');
			
			List<string[]> levelOpps = new List<string[]>();
			foreach( string s in sData )
			{
				levelOpps.Add(s.Split(','));
			}
			
			switch(_level)
			{
				case SkillLevel.Emerging:
					egOpportunities = levelOpps;
				break;
				
				case SkillLevel.Developing:
					dgOpportunities = levelOpps;
				break;
				
				case SkillLevel.Developed:
				case SkillLevel.Completed:
					ddOpportunities = levelOpps;
				break;
			}
		}
		
		private static List<string> audioNames = new List<string>
		{	"",
			"3B-Story Introduction Audio/3B-story-introduction",
			"3B-Skill-Instruction-Audio/3B-many-things-fly-above",
			"3B-Skill-Instruction-Audio/3B-yes-thats-the-parachutist",
			"3B-Skill-Instruction-Audio/3B-now-its-your-turn",
			"3B-Skill-Instruction-Audio/3B-what-else-do-you-see",
			"3B-Skill-Instruction-Audio/3B-touch-the-green-button",
			"3B-Skill-Instruction-Audio/3B-try-again",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-dragon-above-airplane",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-dragon-beside-airplane",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-dragon-far-from-airplane",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-dragon-in-airplane",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-dragon-near-airplane",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-dragon-next-to-airplane",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-dragon-on-airplane",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-dragon-over-airplane",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-dragon-under-airplane",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-bird-above-cloud",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-bird-beside-cloud",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-bird-far-from-cloud",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-bird-in-cloud",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-bird-near-cloud",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-bird-next-to-cloud",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-bird-on-cloud",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-bird-over-cloud",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-bird-under-cloud",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-butterfly-above-bubble",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-butterfly-beside-bubble",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-butterfly-far-from-bubble",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-butterfly-in-bubble",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-butterfly-near-bubble",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-butterfly-next-to-bubble",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-butterfly-on-bubble",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-butterfly-over-bubble",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-butterfly-under-bubble",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-parachutist-above-airship",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-parachutist-beside-airship",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-parachutist-far-from-airship",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-parachutist-in-airship",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-parachutist-near-airship",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-parachutist-next-to-airship",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-parachutist-on-airship",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-parachutist-over-airship",
			"3B-Skill-Instruction-Audio/3B-Initial Instruction/3B-touch-parachutist-under-airship",
			"3B-Reinforcement Audio/3B-found-dragon-above-airplane",
			"3B-Reinforcement Audio/3B-found-dragon-beside-airplane",
			"3B-Reinforcement Audio/3B-found-dragon-far-from-airplane",
			"3B-Reinforcement Audio/3B-found-dragon-in-airplane",
			"3B-Reinforcement Audio/3B-found-dragon-near-airplane",
			"3B-Reinforcement Audio/3B-found-dragon-next-to-airplane",
			"3B-Reinforcement Audio/3B-found-dragon-on-airplane",
			"3B-Reinforcement Audio/3B-found-dragon-over-airplane",
			"3B-Reinforcement Audio/3B-found-dragon-under-airplane",
			"3B-Reinforcement Audio/3B-found-bird-above-cloud",
			"3B-Reinforcement Audio/3B-found-bird-beside-cloud",
			"3B-Reinforcement Audio/3B-found-bird-far-from-cloud",
			"3B-Reinforcement Audio/3B-found-bird-in-cloud",
			"3B-Reinforcement Audio/3B-found-bird-near-cloud",
			"3B-Reinforcement Audio/3B-found-bird-next-to-cloud",
			"3B-Reinforcement Audio/3B-found-bird-on-cloud",
			"3B-Reinforcement Audio/3B-found-bird-over-cloud",
			"3B-Reinforcement Audio/3B-found-bird-under-cloud",
			"3B-Reinforcement Audio/3B-found-butterfly-above-bubble",
			"3B-Reinforcement Audio/3B-found-butterfly-beside-bubble",
			"3B-Reinforcement Audio/3B-found-butterfly-far-from-bubble",
			"3B-Reinforcement Audio/3B-found-butterfly-in-bubble",
			"3B-Reinforcement Audio/3B-found-butterfly-near-bubble",
			"3B-Reinforcement Audio/3B-found-butterfly-next-to-bubble",
			"3B-Reinforcement Audio/3B-found-butterfly-on-bubble",
			"3B-Reinforcement Audio/3B-found-butterfly-over-bubble",
			"3B-Reinforcement Audio/3B-found-butterfly-under-bubble",
			"3B-Reinforcement Audio/3B-found-parachutist-above-airship",
			"3B-Reinforcement Audio/3B-found-parachutist-beside-airship",
			"3B-Reinforcement Audio/3B-found-parachutist-far-from-airship",
			"3B-Reinforcement Audio/3B-found-parachutist-in-airship",
			"3B-Reinforcement Audio/3B-found-parachutist-near-airship",
			"3B-Reinforcement Audio/3B-found-parachutist-next-to-airship",
			"3B-Reinforcement Audio/3B-found-parachutist-on-airship",
			"3B-Reinforcement Audio/3B-found-parachutist-over-airship",
			"3B-Reinforcement Audio/3B-found-parachutist-under-airship",
			"3B-Inactivity-Reinforcement-Audio/3B-this-dragon-above-airplane",
			"3B-Inactivity-Reinforcement-Audio/3B-this-dragon-beside-airplane",
			"3B-Inactivity-Reinforcement-Audio/3B-this-dragon-far-from-airplane",
			"3B-Inactivity-Reinforcement-Audio/3B-this-dragon-in-airplane",
			"3B-Inactivity-Reinforcement-Audio/3B-this-dragon-near-airplane",
			"3B-Inactivity-Reinforcement-Audio/3B-this-dragon-next-to-airplane",
			"3B-Inactivity-Reinforcement-Audio/3B-this-dragon-on-airplane",
			"3B-Inactivity-Reinforcement-Audio/3B-this-dragon-over-airplane",
			"3B-Inactivity-Reinforcement-Audio/3B-this-dragon-under-airplane",
			"3B-Inactivity-Reinforcement-Audio/3B-this-bird-above-cloud",
			"3B-Inactivity-Reinforcement-Audio/3B-this-bird-beside-cloud",
			"3B-Inactivity-Reinforcement-Audio/3B-this-bird-far-from-cloud",
			"3B-Inactivity-Reinforcement-Audio/3B-this-bird-in-cloud",
			"3B-Inactivity-Reinforcement-Audio/3B-this-bird-near-cloud",
			"3B-Inactivity-Reinforcement-Audio/3B-this-bird-next-to-cloud",
			"3B-Inactivity-Reinforcement-Audio/3B-this-bird-on-cloud",
			"3B-Inactivity-Reinforcement-Audio/3B-this-bird-over-cloud",
			"3B-Inactivity-Reinforcement-Audio/3B-this-bird-under-cloud",
			"3B-Inactivity-Reinforcement-Audio/3B-this-butterfly-above-bubble",
			"3B-Inactivity-Reinforcement-Audio/3B-this-butterfly-beside-bubble",
			"3B-Inactivity-Reinforcement-Audio/3B-this-butterfly-far-from-bubble",
			"3B-Inactivity-Reinforcement-Audio/3B-this-butterfly-in-bubble",
			"3B-Inactivity-Reinforcement-Audio/3B-this-butterfly-near-bubble",
			"3B-Inactivity-Reinforcement-Audio/3B-this-butterfly-next-to-bubble",
			"3B-Inactivity-Reinforcement-Audio/3B-this-butterfly-on-bubble",
			"3B-Inactivity-Reinforcement-Audio/3B-this-butterfly-over-bubble",
			"3B-Inactivity-Reinforcement-Audio/3B-this-butterfly-under-bubble",
			"3B-Inactivity-Reinforcement-Audio/3B-this-parachutist-above-airship",
			"3B-Inactivity-Reinforcement-Audio/3B-this-parachutist-beside-airship",
			"3B-Inactivity-Reinforcement-Audio/3B-this-parachutist-far-from-airship",
			"3B-Inactivity-Reinforcement-Audio/3B-this-parachutist-in-airship",
			"3B-Inactivity-Reinforcement-Audio/3B-this-parachutist-near-airship",
			"3B-Inactivity-Reinforcement-Audio/3B-this-parachutist-next-to-airship",
			"3B-Inactivity-Reinforcement-Audio/3B-this-parachutist-on-airship",
			"3B-Inactivity-Reinforcement-Audio/3B-this-parachutist-over-airship",
			"3B-Inactivity-Reinforcement-Audio/3B-this-parachutist-under-airship",
			"User-Earns-Artifact"
		};
	}
}