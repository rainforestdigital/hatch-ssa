using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.geom;

namespace SSGCore
{
	
	public enum NumberLabels
	{
		ONE,
		TWO,
		THREE,
		FOUR,
		FIVE,
		SIX,
		SEVEN,
		EIGHT,
		NINE,
		TEN,
		ELEVEN,
		TWELVE,
		THIRTEEN,
		FOURTEEN,
		FIFTEEN
	}
	
	public class NumeralRecognition : BaseSkill
	{		
		public new string host = "PLATTIE"; //will probably need to change
		public float percent;
		
		private int currentNumber = 0;
		
		private List<int> usedNumbers;
		private List<int> answerList;
			
		private List<FilterMovieClip> _cards;
		
		private FilterMovieClip _answerCard;
		private FilterMovieClip _incorrectAnswerCard;
		
		private DisplayObjectContainer cardContainer_mc;
		
		private string currentClickedNumber;
		
		private int rightPart = 0;
		private int wrongPart = 0;
		private int right = 0;
		private int wrong = 0;
		
		
		private new NumeralRecognitionTutorial tutorial_mc;
//		private Rectangle tutCardLoc;
		
		public NumeralRecognition( SkillLevel level, SessionInfo  currentSession) : base(level, currentSession)
		{
			BACKGROUND = "Background";
			ARTIFACT = "Artifact";
			host = "Platty";
			
			Init (null);
		}
		
		/// <summary>
		/// Releases all resource used by the <see cref="SSGCore.NumeralRecognition"/> object.
		/// </summary>
		/// <remarks>
		/// Call <see cref="Dispose"/> when you are finished using the <see cref="SSGCore.NumeralRecognition"/>. The
		/// <see cref="Dispose"/> method leaves the <see cref="SSGCore.NumeralRecognition"/> in an unusable state. After
		/// calling <see cref="Dispose"/>, you must release all references to the <see cref="SSGCore.NumeralRecognition"/> so
		/// the garbage collector can reclaim the memory that the <see cref="SSGCore.NumeralRecognition"/> was occupying.
		/// </remarks>
		public override void Dispose ()
		{
			base.Dispose();
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			if (tutorial_mc != null)
				tutorial_mc.Destroy ();
			if(_answerCard != null)
				_answerCard.Destroy();
			if(_incorrectAnswerCard != null)
				_incorrectAnswerCard.Destroy();
			if(_cards != null)
			{
				foreach(FilterMovieClip fmc in _cards)
					fmc.Destroy();
			}
		}
		
		/// <summary>
		/// Gets or sets the number cards.
		/// </summary>
		/// <value>
		/// The number cards to be used in the opportunity.
		/// </value>
		private int numCards
		{
			get{ return _cards.Count; }
			set
			{
				int i;
				
				for(i = 0; i < _cards.Count; i++)
				{
					_cards[i].Destroy();
					_cards[i].removeEventListener( MouseEvent.CLICK, onCardClick );
					cardContainer_mc.removeChild( _cards[i] );
				}
				
				_cards.Clear();
				
				float cWidth;				
				if(_level == SkillLevel.Developed || _level == SkillLevel.Completed)
					cWidth = Screen.width/6;
				else
					cWidth = (Screen.width/5) - 8;
				
				if(PlatformUtils.GetClosestRatio() == ScreenRatio.SIXTEENxNINE) cWidth *= 7f/8f;
				
				LabeledButton answerButton = MovieClipFactory.CreateNumeralRecognitionCard();
				answerButton.gotoAndStop(currentNumber);
				//answerButton.mouseChildrenEnabled = false;
				_answerCard = new FilterMovieClip( answerButton );
				//_answerCard.addEventListener( MouseEvent.CLICK, onCardClick );
				_answerCard.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
				_answerCard.filters[0].Visible = false;
				_answerCard.scaleY = _answerCard.scaleX = cWidth/_answerCard.width;
				_answerCard.name = currentNumber.ToString();
				_cards.Add( _answerCard );
				cardContainer_mc.addChild(_answerCard);
				
				List<int> availableAnswers = new List<int>();
				
				for(i = 1; i <= 15; i++)
				{
					if(i != currentNumber)
						availableAnswers.Add( i );
				}
				
				while(_cards.Count < value)
				{
					int rand = Mathf.FloorToInt( Random.Range(0, availableAnswers.Count) );
					if(rand == availableAnswers.Count) rand--;
					LabeledButton newCard = MovieClipFactory.CreateNumeralRecognitionCard();
					newCard.gotoAndStop(availableAnswers[rand]);
					//newCard.mouseChildrenEnabled = false;
					FilterMovieClip newCardFilter = new FilterMovieClip( newCard );
					//newCardFilter.addEventListener( MouseEvent.CLICK, onCardClick );
					newCardFilter.scaleY = newCardFilter.scaleX = cWidth/newCardFilter.width;
					newCardFilter.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
					newCardFilter.filters[0].Visible = false;	
					newCardFilter.name = availableAnswers[rand].ToString();
					_cards.Add( newCardFilter );
					availableAnswers.RemoveAt(rand);
					cardContainer_mc.addChild( newCardFilter );
				}
				_cards.Shuffle();
				
				cardContainer_mc.alpha = 0;
				arrangeCards();
				
				//normalCardScale = _cards[0].scaleX;
				
			}
		}
		
		private void arrangeCards()
		{
			float padding = 10;
			float startX = cardContainer_mc.width / 2;
			startX -= (((_cards[0].width + padding) * _cards.Count) - padding) / 2;
			float startY = (cardContainer_mc.height - _cards[0].height)/2;
			
			Debug.Log("Starting at: " + startX + ", " + startY);
			
			for(int i = 0; i < _cards.Count; i++)
			{
				FilterMovieClip fmc = _cards[i];
				fmc.y = startY;
				fmc.x = startX;
				if(_level != SkillLevel.Tutorial || i > 0)
				{
					fmc.visible = true;
					fmc.alpha = 1;
				}
				startX += fmc.width + padding;
			}
		}
				
		public NumeralRecognition( string swf ) : base( swf + ":Skill" )
		{
			PreCacheAssets();
		}
		
		private void PreCacheAssets()
		{
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle("NumeralRecognition", OnAssetsLoaded));
		}
		
		void OnAssetsLoaded(AssetBundle bundle)
		{
			if( bundle != null )
			{
				Init( null );
			}
		}
		
		/// <summary>
		/// Init the specified parent.
		/// </summary>
		/// <param name='parent'>
		/// Parent movieclip. Not currently used
		/// </param>
		public override void Init (MovieClip parent)
		{
			base.Init (parent);

			cardContainer_mc = new DisplayObjectContainer();
			cardContainer_mc.width = Screen.width;
			cardContainer_mc.height = Screen.height;
			addChild( cardContainer_mc );
			cardContainer_mc.visible = false;
			
			_cards = new List<FilterMovieClip>();
			usedNumbers = new List<int>();
			
			timer = totalTime;
			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			switch(_level)
			{
				// Tutorial
				case SkillLevel.Tutorial:
					totalOpportunities = 1;
					opportunityComplete = false;
					if(resumingSkill)
						fadeInCardContainer().OnComplete(nextOpportunity);
					else
					{
						tutorial_mc = new NumeralRecognitionTutorial(this);
						addChild( tutorial_mc.View );
						tutorial_mc.View.addEventListener( TutorialEvent.START, onTutorialStart );
						tutorial_mc.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
						tutorial_mc.Resize( Screen.width, Screen.height );
//						tutCardLoc = tutorial_mc.getCardPos( cardContainer_mc );
						tutorial_mc.View.visible = false;
					}
				break;
				
				//All other levels
				case SkillLevel.Emerging:
					totalOpportunities = 10;
					answerList = new List<int>{1,2,3,4,5,1,2,3,4,5};
					if(resumingSkill)
					{
						parseSessionData( currentSession.sessionData );
						fadeInCardContainer().OnComplete( nextOpportunity );
					}
				break;
				case SkillLevel.Developing:
					totalOpportunities = 15;
					answerList = new List<int>{1,2,3,4,5,6,7,8,9,10,6,7,8,9,10};
					if(resumingSkill)
					{
						parseSessionData( currentSession.sessionData );
//						fadeInCardContainer().OnComplete( nextOpportunity );
					}
				break;
				case SkillLevel.Developed:
				case SkillLevel.Completed:
					totalOpportunities = 20;
					answerList = new List<int>{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,11,12,13,14,15};
					if(resumingSkill)
					{
						parseSessionData( currentSession.sessionData );
//						fadeInCardContainer().OnComplete( nextOpportunity );
					}
				break;
			}
			Resize( Screen.width, Screen.height );
			
		}
		
		public override void Start()
		{
			MarkInstructionStart();
		}
		
		#region Audio Listeners
		
		/// <summary>
		/// Raises the audio word complete event.
		/// Handles what happens after specific audio files are played
		/// </summary>
		/// <param name='word'>
		/// Parameter that gets passed in from the delegate.
		/// </param>
		public override void OnAudioWordComplete( string word )
		{		
			AudioClip clip = null;
			SoundEngine.SoundBundle bundle = null;

			/*foreach(FilterMovieClip lb in _cards)
			{
				if(lb.filters[0] != null) lb.filters[0].Visible = false;
			}*/

			if(opportunityComplete)
			{
			
				switch( word )
				{
					case THEME_COMPLIMENT:
						if(_level == SkillLevel.Tutorial)
						{
							tutorial_mc.OnComplimentComplete();
						}
						else {
							bundle = new SoundEngine.SoundBundle();
							clip = SoundUtils.LoadGlobalSound( MovieClipFactory.NUMERAL_RECOGNITION, getCurrentNumberAudioString(NumeralRecognitionSounds.SOUND_24));
							bundle.AddClip( clip, YOU_FOUND, clip.length );
							SoundEngine.Instance.PlayBundle( bundle );
						}
					break;
					
					case YOU_FOUND:
					case YOU_FOUND_INCORRECT:
					case INTRO:
					case PLEASE_FIND:
					case NOW_ITS:
					case NOW_LETS:
					case THIS_IS :
						Tweener.addTween( cardContainer_mc, Tweener.Hash("time",0.35f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuint) );

						// This fix for the transition animations not happening is the same as the one in LetterRecognition
						if (timeoutEnd) {
							//quietContinue = true;
							dispatchEvent( new SkillEvent(SkillEvent.TIMEOUT_END, true, false) );
							timeoutEnd = false;
						} else {
							if(opportunityCorrect) {
								rightPart ++;
								dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
							} else {
								wrongPart ++;
								dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
							} 
						}

					/*
						if(opportunityCorrect)
							rightPart ++;
						else
							wrongPart ++;
						
						if((rightPart/(float)totalOpportunities)*10 >= right + 1)
						{
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
							right ++;
						}
						else if((wrongPart/(float)totalOpportunities)*10 >= wrong + 1)
						{
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
							wrong++;
						}
						else if(currentOpportunity == totalOpportunities)
						{
							if(opportunityCorrect)
								dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
							else
								dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
						}
						else
						{
							if(timeoutEnd)
							{
								quietContinue = true;
								dispatchEvent( new SkillEvent(SkillEvent.TIMEOUT_END, true, false) );
							}
							else
								nextOpportunity();
							timeoutEnd = false;
						}
						*/
					break;
					
					case THEME_INTRO:
						if( !currentSession.skipIntro )
						{
							bundle = new SoundEngine.SoundBundle();
							clip = SoundUtils.LoadGlobalSound( MovieClipFactory.NUMERAL_RECOGNITION, NumeralRecognitionSounds.SOUND_1);
							bundle.AddClip( clip, INTRO, clip.length );
							SoundEngine.Instance.PlayBundle( bundle );
						}
						else
							OnAudioWordComplete( INTRO );
					break;
					
					case CLICK_CORRECT:
						dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
					break;
					
				}
				
			}
			else
			{
				
				switch( word )
				{
					
					case THEME_CRITICISM:
						bundle = new SoundEngine.SoundBundle();
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.NUMERAL_RECOGNITION, getCurrentNumberAudioString(NumeralRecognitionSounds.SOUND_24));
						bundle.AddClip( clip, YOU_FOUND, clip.length );
						SoundEngine.Instance.PlayBundle( bundle );
					break;
					
					case YOU_FOUND:
					case YOU_FOUND_INCORRECT:
						clearIncorrectAnswer();
						//PLEASE FIND current number - audio
						clip = SoundUtils.LoadGlobalSound( MovieClipFactory.NUMERAL_RECOGNITION, NumeralRecognitionSounds.SOUND_7 + currentNumber.ToString());
						bundle = new SoundEngine.SoundBundle();
						bundle.AddClip( clip, PLEASE_FIND, clip.length );
						SoundEngine.Instance.PlayBundle( bundle );
					break;
					
					case INTRO:
						cardContainer_mc.visible = true;
						if(_level == SkillLevel.Tutorial)
						{
							tutorial_mc.View.visible = true;
							tutorial_mc.OnStart();
						}
						else
						{
							nextOpportunity();
						}
					break;
					
					case NOW_LETS:
						resetTutorial();
					break;
					
					case PLEASE_FIND:
					case NOW_ITS:
					case THIS_IS :
						clearIncorrectAnswer();
						startTimer();
						enableCardButtons();
					break;
					
					case THEME_COMPLIMENT:
						nextOpportunity();
					break;
					
					case THEME_INTRO:
						if( !currentSession.skipIntro )
						{
							bundle = new SoundEngine.SoundBundle();
							clip = SoundUtils.LoadGlobalSound( MovieClipFactory.NUMERAL_RECOGNITION, NumeralRecognitionSounds.SOUND_1);
							bundle.AddClip( clip, INTRO, clip.length );
							SoundEngine.Instance.PlayBundle( bundle );
						} else {
							OnAudioWordComplete( INTRO );
						}
					break;
					
					case CLICK_INCORRECT:
						dispatchEvent( new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
					break;
					
				}
			}
			
		}
		#endregion
		
		private void clearIncorrectAnswer()
		{
			if(_incorrectAnswerCard != null)
			{
				Tweener.addTween( _incorrectAnswerCard, Tweener.Hash("time", 0.35f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuad) ).OnComplete(nukeIncorrect);
			}
		}
		private void nukeIncorrect()
		{
			_incorrectAnswerCard.Destroy();
			cardContainer_mc.removeChild(_incorrectAnswerCard);
			_incorrectAnswerCard = null;
		}
		
		private void StartTimerAfterSound()
		{
			StartTimerAfterSound("");
		}
		
		private void StartTimerAfterSound(string msg)
		{
			startTimer();
		}
		
		/// <summary>
		/// Handles the click event for the cards.
		/// </summary>
		/// <param name='e'>
		/// Event Param.
		/// </param>
		private void onCardClick( CEvent e )
		{
			
			PlayClickSound();
			
			disableCardButtons();
			
			FilterMovieClip cardClicked = e.currentTarget as FilterMovieClip;
			DebugConsole.Log( "cardClicked = null: " + (cardClicked == null) );
			
			if(_level == SkillLevel.Tutorial)
				cardClicked.ScaleCentered( cardClicked.scaleX * (7f/6f) );
			else
				cardClicked.ScaleCentered( cardClicked.scaleX * (4f/3f) );
			
			cardContainer_mc.removeChild( cardClicked );
			cardContainer_mc.addChild( cardClicked );
			
			Rectangle ccRect = cardClicked.getBounds(cardContainer_mc);
			if(ccRect.x < 25) cardClicked.x += (ccRect.x * -1) + 25;
			else if(ccRect.x + ccRect.width > cardContainer_mc.width - 25)
				cardClicked.x -= (((ccRect.x + ccRect.width) - cardContainer_mc.width)) + 25;
			if(_level != SkillLevel.Tutorial)
				cardClicked.y -= cardClicked.height/8f;
			
			SoundEngine.Instance.SendCancelToken();
			//SoundEngine.Instance.StopAll();
			
			stopTimer();
			timeoutCount = 0;
			
			LabeledButton btnClicked = cardClicked.Target as LabeledButton;
			
			currentClickedNumber = btnClicked.currentFrame.ToString();
			
			if(cardClicked.filters.Count != 0) cardClicked.filters[0].Visible = true;	
			else
			{
				cardClicked.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
				cardClicked.filters[0].Visible = true;
			}
			
			//If Right Answer
			if(cardClicked == _answerCard)
			{
				
				PlayCorrectSound();
				
				if(opportunityAnswered == false)
				{
					opportunityAnswered = true;
					opportunityCorrect = true;
				}
				opportunityComplete = true;
				
				disableCardButtons();
				
				AddNewActionData( formatActionData( "tap", _cards.IndexOf( cardClicked ), Vector2.zero, true ) );
			}
			//If Wrong Answer
			else
			{
				
				PlayIncorrectSound();
				
				disableCardButtons();
				
				if(opportunityAnswered == false)
				{
					opportunityAnswered = true;
					opportunityCorrect = false;
				}
				
				_incorrectAnswerCard = cardClicked;
				
				AddNewActionData( formatActionData( "tap", _cards.IndexOf( cardClicked ), Vector2.zero, false ) );
								
				//TimerUtils.SetTimeout(1.5f, StartTimerAfterSound);
			}
		}
		
		protected override void resetTutorial()
		{
			
			cardContainer_mc.visible = false;
			currentOpportunity = 0;
			
			timeoutCount = 0;
			
			DebugConsole.Log("resetting tutorial");
			
			tutorial_mc.Destroy();
			tutorial_mc = null;
			tutorial_mc = new NumeralRecognitionTutorial(this);
			
			addChild( tutorial_mc.View );
			tutorial_mc.View.visible = true;
			tutorial_mc.View.addEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			tutorial_mc.Resize( Screen.width, Screen.height );
			tutorial_mc.OnStart();
		}
		
		/// <summary>
		/// Initializes the next opportunity.
		/// </summary>
		public override void nextOpportunity()
		{
			
			switch(_level)
			{	
				case SkillLevel.Tutorial:
					tallyAnswers();
				break;

				case SkillLevel.Emerging:
					setNextOpportunity(3);
				break;
				
				case SkillLevel.Developing:
					setNextOpportunity(4);
				break;
				
				case SkillLevel.Developed:
					setNextOpportunity(5);
				break;
				
				case SkillLevel.Completed:
					setNextOpportunity(5);
				break;
				
			}
			
			if(totalOpportunities > 10 && currentOpportunity > 1)
				dispatchEvent( new SkillEvent(SkillEvent.BOOKMARK_NOW) );
		}
		
		/// <summary>
		/// Sets the next opportunity's values.
		/// </summary>
		/// <param name='min'>
		/// Minimum card value.
		/// </param>
		/// <param name='max'>
		/// Max card value.
		/// </param>
		/// <param name='cards'>
		/// number of cards.
		/// </param>
		private void setNextOpportunity( int cards )
		{
			
			List<int> answersArray;
			int i;

			// TODO: This clearly does nothing. Delete it?
			/* 
			if(currentOpportunity > 0)
			{
				if(opportunityComplete)
				{
					if(opportunityCorrect)
					{
						//NumCorrect++;
					}
					else
					{
						//NumIncorrect++;
					}
				}
				else
				{
					//NumIncorrect++;
				}
			}
			*/
			if(currentOpportunity < totalOpportunities)
			{
				opportunityComplete = false;
				opportunityCorrect = false;
				opportunityAnswered = false;
			
				currentOpportunity++;
				
				answersArray = new List<int>();
				
				if(resumingSkill)
				{
					currentNumber = usedNumbers[usedNumbers.Count - 1];
					resumingSkill = false;
				}
				else
				{
					for(i = 1; i <= 15; i++)
					{
						if(answerList.IndexOf(i) > -1 && (usedNumbers.Count == 0 || i != usedNumbers[usedNumbers.Count - 1]))
						{
							if(currentOpportunity == totalOpportunities - 3 && answerList.IndexOf(i) != answerList.LastIndexOf(i))
							{
								answersArray.Clear();
								answersArray.Add(i);
								break;
							}
							answersArray.Add(i);
						}
					}
					
					int rand = Mathf.FloorToInt(Random.Range(0, answersArray.Count));
					if(rand == answersArray.Count) rand--;
					currentNumber = answersArray[rand];
					usedNumbers.Add(currentNumber);
					answerList.Remove(currentNumber);
				}
			
				numCards = cards;
			
				fadeInCardContainer().OnComplete( OpportunityStart );
			}
			else
			{
				tallyAnswers();
				numCards = 0;
			}
			
			Debug.Log("_numCorrect: " + NumCorrect);
			Debug.Log("_numIncorrect: " + NumIncorrect);
			Debug.Log("currentOpportunity: " + currentOpportunity);
			
		}
		
		/// <summary>
		/// Fades in the card container.
		/// </summary>
		/// <returns>
		/// TweenerObj used when fading the container.
		/// </returns>
		private TweenerObj fadeInCardContainer()
		{
			cardContainer_mc.visible = true;
			return Tweener.addTween(cardContainer_mc, Tweener.Hash("time", 0.35f, "alpha", 1, "transition", Tweener.TransitionType.easeInOutQuint) );
		}
		
		/// <summary>
		/// Starts the Opportunity.
		/// </summary>
		public override void OpportunityStart()
		{
			AudioClip clip = null;
			string clipName = "";
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			string clipID = "";
			
			switch( _level )
			{
				case SkillLevel.Tutorial:
					//
				break;
				default:
					//PLEASE FIND (currentNumber) - AUDIO
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.NUMERAL_RECOGNITION, NumeralRecognitionSounds.SOUND_7 + currentNumber.ToString() );
					clipName = NumeralRecognitionSounds.SOUND_7 + currentNumber.ToString();
					clipID = PLEASE_FIND;
				break;
			}
			
			bundle.AddClip(clip, clipID, clip.length);
			SoundEngine.Instance.PlayBundle( bundle );
			
			AddNewOppData( audioNames.IndexOf( clipName ) );
			
			foreach( FilterMovieClip fmc in _cards )
			{
				Vector2 point = fmc.parent.localToGlobal(new Vector2(fmc.x, fmc.y));
				AddNewObjectData( formatObjectData( fmc.name, point, fmc == _answerCard ) );
			}
			
			//enableCardButtons();
			
			timeoutCount = 0;
			
			dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_START, true, false) );
			
		}
		
		/// <summary>
		/// Resize the specified width and height.
		/// </summary>
		/// <param name='width'>
		/// Width.
		/// </param>
		/// <param name='height'>
		/// Height.
		/// </param>
		public override void Resize( int width, int height )
		{
			this.width = width;
			this.height = height;
			
			cardContainer_mc.scaleX = width/cardContainer_mc.width;
			cardContainer_mc.scaleY = cardContainer_mc.scaleX;
			cardContainer_mc.x = 0;
			cardContainer_mc.y = (height - (cardContainer_mc.height*cardContainer_mc.scaleY))/2;
			//cardContainer_mc.FitToParent();
			//cardContainer_mc.width = this.width;
			//cardContainer_mc.height = this.height;
		}
		
		/// <summary>
		/// Called when BaseSkill's timer completes.
		/// </summary>
		public override void onTimerComplete()
		{
			base.onTimerComplete();
			
			stopTimer();
			
			timeoutCount++;

			if (_level == SkillLevel.Tutorial) {
				tutorial_mc.TimeoutHandler(timeoutCount);
				return;
			}

			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = null;
			string clipName = "";
			string clipID = "";
			
			switch(timeoutCount)
			{
				case 1:
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.NUMERAL_RECOGNITION, NumeralRecognitionSounds.SOUND_7 + currentNumber.ToString() );
					clipName = NumeralRecognitionSounds.SOUND_7 + currentNumber.ToString();
					clipID = PLEASE_FIND;
				break;
				case 2:
					clip = SoundUtils.LoadGlobalSound(MovieClipFactory.NUMERAL_RECOGNITION, NumeralRecognitionSounds.SOUND_5 );
					clipName = NumeralRecognitionSounds.SOUND_5;
					clipID = NOW_ITS; //TEMP
				break;
				case 3:
					if(opportunityAnswered == false)
					{
						opportunityAnswered = true;
						opportunityCorrect = false;
					}
					
					foreach(FilterMovieClip lb in _cards)
					{
						if(lb == _answerCard)
						{
							lb.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
							break;
						}
					}
					
					//PLEASE FIND currect number - audio
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.NUMERAL_RECOGNITION, NumeralRecognitionSounds.SOUND_7 + currentNumber.ToString());
					clipName = NumeralRecognitionSounds.SOUND_7 + currentNumber.ToString();
					clipID = PLEASE_FIND;
				break;
				case 4:
					timeoutEnd = true;
					opportunityComplete = true;
					
					foreach(FilterMovieClip lb in _cards)
					{
						lb.removeEventListener( MouseEvent.CLICK, onCardClick );
						if(lb != _answerCard)
						{
							Tweener.addTween(lb, Tweener.Hash("time",0.5f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuad) );
						}
						else
						{
							currentClickedNumber = lb.Target.currentFrame.ToString();
							lb.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
							lb.ScaleCentered(lb.scaleX * (4f/3f));
						}
					}
					
					//THIS IS currect number - audio
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.NUMERAL_RECOGNITION, getCurrentNumberAudioString(NumeralRecognitionSounds.SOUND_39));
					clipName = getCurrentNumberAudioString(NumeralRecognitionSounds.SOUND_39);
					clipID = THIS_IS;
				break;
				default:
				break;
			}
			
			if(clip != null)
			{
				bundle.AddClip( clip, clipID, clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
				
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf( clipName ) );
			}
			
		}
		
		/// <summary>
		/// Called when the tutorial complete event is fired.
		/// </summary>
		/// <param name='e'>
		/// Event param.
		/// </param>
		private void onTutorialComplete( CEvent e )
		{
			tutorial_mc.View.removeEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.View.removeEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			
			removeChild( tutorial_mc.View );
			
			dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
		//	nextOpportunity();
			
		}
		
		/// <summary>
		/// Disables the card buttons.
		/// </summary>
		private void disableCardButtons()
		{
			for(int i = 0; i < _cards.Count; i++)
			{
				_cards[i].removeEventListener( MouseEvent.CLICK, onCardClick );
			}
			MarkInstructionStart();
		}
		
		/// <summary>
		/// Enables the card buttons.
		/// </summary>
		private void enableCardButtons()
		{
			for(int i = 0; i < _cards.Count; i++)
			{
				if(_cards[i].filters.Count > 0)
				{
					foreach(MovieClipFilter f in _cards[i].filters)
					{
						f.Visible = false;
					}
				}
				_cards[i].addEventListener( MouseEvent.CLICK, onCardClick );
			}
			if(_level != SkillLevel.Tutorial) MarkInstructionEnd();
		}
		
		/// <summary>
		/// Gets the current number audio string.
		/// </summary>
		/// <returns>
		/// The current number audio string.
		/// </returns>
		/// <param name='intro'>
		/// beginning of file string value.
		/// </param>
		private string getCurrentNumberAudioString(string intro)
		{
			return intro + currentClickedNumber.ToString() + NumeralRecognitionSounds.SAY + currentClickedNumber.ToString();
		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = "";
			
			foreach(int i in usedNumbers)
			{
				if(data != "")
					data = data + "-";
				data = data + i.ToString();
			}
			
			data = data + ";" + rightPart.ToString() + "-" + right.ToString() + "-" + wrongPart.ToString() + "-" + wrong.ToString();
			
			return data;
		}
		
		private void parseSessionData (string data)
		{
			usedNumbers = new List<int>();
			List<int> tempAList = new List<int>();
			tempAList.InsertRange(0, answerList);
			
			string partData = data.Split(';')[1];
			data = data.Split(';')[0];
			
			string[] sData = data.Split('-');
			
			foreach( string s in sData )
			{
				usedNumbers.Add(int.Parse(s));
				tempAList.Remove(int.Parse(s));
			}
			
			if(tempAList.Count + currentOpportunity + 1 < totalOpportunities)
			{
				answerList.Remove(usedNumbers[usedNumbers.Count - 1]);
				while(answerList.Count > totalOpportunities - (currentOpportunity + 1))
				{
					int r = Mathf.FloorToInt(Random.Range(0, answerList.Count));
					if(r == answerList.Count) r--;
					answerList.RemoveAt(r);
				}
				if((answerList.Count == 2 && answerList.LastIndexOf(answerList[0]) > 0) || (answerList.Count == 1 && answerList[0] == usedNumbers[usedNumbers.Count - 1]))
				{
					if(answerList[0] > 1)
						answerList[0]--;
					else
						answerList[0]++;
				}
			}
			else answerList = tempAList;
			
			sData = partData.Split('-');
			if(sData.Length > 2)
			{
				rightPart = int.Parse(sData[0]);
				right = int.Parse(sData[1]);
				wrongPart = int.Parse(sData[2]);
				wrong = int.Parse(sData[3]);
			}
			else
			{
				rightPart = Mathf.RoundToInt(float.Parse(sData[0]) * totalOpportunities);
				right = currentSession.correct;
				wrongPart = Mathf.RoundToInt(float.Parse(sData[1]) * totalOpportunities);
				wrong = currentSession.incorrect;
			}
		}

		public void setOpportunityComplete() {
			opportunityComplete = true;
		}
		
		public static List<string> audioNames = new List<string>
		{	"",
			"Narration/2B-Story Introduction Audio/2B-Story-Introduction",
			"Narration/2B-Skill Instructions Audio/2B-TT-Platty-will-try",
			"Narration/2B-Skill Instructions Audio/2B-Now-its-your-turn",
			"Narration/2B-Skill Instructions Audio/2B-Let-the-treasure-hunt-begin",
			"Narration/2B-Skill Instructions Audio/2B-touch-the-green-button",
			"Narration/2B-Skill Instructions Audio/2B-try-again",
			"Narration/2B-Skill Instructions Audio/2B-Initial Instructions/2B-Touch-Numeral-1",
			"Narration/2B-Skill Instructions Audio/2B-Initial Instructions/2B-Touch-Numeral-2",
			"Narration/2B-Skill Instructions Audio/2B-Initial Instructions/2B-Touch-Numeral-3",
			"Narration/2B-Skill Instructions Audio/2B-Initial Instructions/2B-Touch-Numeral-4",
			"Narration/2B-Skill Instructions Audio/2B-Initial Instructions/2B-Touch-Numeral-5",
			"Narration/2B-Skill Instructions Audio/2B-Initial Instructions/2B-Touch-Numeral-6",
			"Narration/2B-Skill Instructions Audio/2B-Initial Instructions/2B-Touch-Numeral-7",
			"Narration/2B-Skill Instructions Audio/2B-Initial Instructions/2B-Touch-Numeral-8",
			"Narration/2B-Skill Instructions Audio/2B-Initial Instructions/2B-Touch-Numeral-9",
			"Narration/2B-Skill Instructions Audio/2B-Initial Instructions/2B-Touch-Numeral-10",
			"Narration/2B-Skill Instructions Audio/2B-Initial Instructions/2B-Touch-Numeral-11",
			"Narration/2B-Skill Instructions Audio/2B-Initial Instructions/2B-Touch-Numeral-12",
			"Narration/2B-Skill Instructions Audio/2B-Initial Instructions/2B-Touch-Numeral-13",
			"Narration/2B-Skill Instructions Audio/2B-Initial Instructions/2B-Touch-Numeral-14",
			"Narration/2B-Skill Instructions Audio/2B-Initial Instructions/2B-Touch-Numeral-15",
			"Narration/2B-Reinforcement Audio/2B-TT-that-is-the-numeral-2",
			"Narration/2B-Reinforcement Audio/2B-TT-you-found-numeral-4",
			"Narration/2B-Reinforcement Audio/2B-Found-1-say-1",
			"Narration/2B-Reinforcement Audio/2B-Found-2-say-2",
			"Narration/2B-Reinforcement Audio/2B-Found-3-say-3",
			"Narration/2B-Reinforcement Audio/2B-Found-4-say-4",
			"Narration/2B-Reinforcement Audio/2B-Found-5-say-5",
			"Narration/2B-Reinforcement Audio/2B-Found-6-say-6",
			"Narration/2B-Reinforcement Audio/2B-Found-7-say-7",
			"Narration/2B-Reinforcement Audio/2B-Found-8-say-8",
			"Narration/2B-Reinforcement Audio/2B-Found-9-say-9",
			"Narration/2B-Reinforcement Audio/2B-Found-10-say-10",
			"Narration/2B-Reinforcement Audio/2B-Found-11-say-11",
			"Narration/2B-Reinforcement Audio/2B-Found-12-say-12",
			"Narration/2B-Reinforcement Audio/2B-Found-13-say-13",
			"Narration/2B-Reinforcement Audio/2B-Found-14-say-14",
			"Narration/2B-Reinforcement Audio/2B-Found-15-say-15",
			"Narration/2B-Inactivity Reinforcement Audio/2B-This-1-say-1",
			"Narration/2B-Inactivity Reinforcement Audio/2B-This-2-say-2",
			"Narration/2B-Inactivity Reinforcement Audio/2B-This-3-say-3",
			"Narration/2B-Inactivity Reinforcement Audio/2B-This-4-say-4",
			"Narration/2B-Inactivity Reinforcement Audio/2B-This-5-say-5",
			"Narration/2B-Inactivity Reinforcement Audio/2B-This-6-say-6",
			"Narration/2B-Inactivity Reinforcement Audio/2B-This-7-say-7",
			"Narration/2B-Inactivity Reinforcement Audio/2B-This-8-say-8",
			"Narration/2B-Inactivity Reinforcement Audio/2B-This-9-say-9",
			"Narration/2B-Inactivity Reinforcement Audio/2B-This-10-say-10",
			"Narration/2B-Inactivity Reinforcement Audio/2B-This-11-say-11",
			"Narration/2B-Inactivity Reinforcement Audio/2B-This-12-say-12",
			"Narration/2B-Inactivity Reinforcement Audio/2B-This-13-say-13",
			"Narration/2B-Inactivity Reinforcement Audio/2B-This-14-say-14",
			"Narration/2B-Inactivity Reinforcement Audio/2B-This-15-say-15",
			"User-Earns-Artifact"
		};
	}

	public class NumeralRecognitionSounds
	{
		public static string SOUND_1 = "Narration/2B-Story Introduction Audio/2B-Story-Introduction";
		public static string SOUND_2 = "Narration/2B-Skill Instructions Audio/2B-TT-Platty-will-try";
		public static string SOUND_3 = "Narration/2B-Skill Instructions Audio/2B-Now-its-your-turn";
		public static string SOUND_4 = "Narration/2B-Skill Instructions Audio/2B-Let-the-treasure-hunt-begin";
		public static string SOUND_5 = "Narration/2B-Skill Instructions Audio/2B-touch-the-green-button";
		public static string SOUND_6 = "Narration/2B-Skill Instructions Audio/2B-try-again";
		
		public static string SOUND_7  = "Narration/2B-Skill Instructions Audio/2B-Initial Instructions/2B-Touch-Numeral-";
		public static string SOUND_24 = "Narration/2B-Reinforcement Audio/2B-Found-";
		public static string SOUND_39 = "Narration/2B-Inactivity Reinforcement Audio/2B-This-";
		public static string SAY 	  = "-say-";

	}
}
