using UnityEngine;
using System.Collections;

using HatchFramework;

using pumpkin;
using pumpkin.display;

namespace SSGCore
{

	public class CommonShapes_Shape : FilterMovieClip
	{
	
		private string shape;
		public string ShapeType
		{
			get{ return shape; }
		}
		private string color;
		public string ColorType
		{
			get{ return color; }
		}
		
		public CommonShapes_Shape(MovieClip mcTarget, string shape, string color) : base(mcTarget)
		{
			this.shape = shape;
			this.color = color;
		}
		
		public CommonShapes_Shape(MovieClip mcTarget) : base(mcTarget)
		{
			this.shape = "";
			this.color = "";
		}
		
		public void SetShapeType( string shape )
		{
			this.shape = shape;
		}
		
		public void SetColorType( string color )
		{
			this.color = color;
			target.gotoAndStop(color);
		}
		
		
	}

}