using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.geom;

namespace SSGCore
{

	public class CommonShapes : BaseSkill
	{
		
		private const string CIRCLE = "CIRCLE";
		private const string SQUARE = "SQUARE";
		private const string RECTANGLE = "RECTANGLE";
		private const string TRIANGLE = "TRIANGLE";
		private const string RHOMBUS = "RHOMBUS";
		
		private const string ORANGE = "ORANGE";
		private const string GREEN = "GREEN";
		private const string PURPLE = "PURPLE";
		private const string YELLOW = "YELLOW";
		private const string BLUE = "BLUE";
		private const string RED = "RED";
		
//		private readonly Dictionary<string, Color> colorForName = new Dictionary<string, Color>() {
//			{ ORANGE, new Color32(0xf4, 0x7f, 0x20, 0xFF) },
//			{ GREEN, new Color32(0x39, 0xb4, 0x4a, 0xFF) },
//			{ PURPLE, new Color32(0x6a, 0x2c, 0x90, 0xFF) },
//			{ YELLOW, new Color32(0xFF, 0xFF, 0x00, 0xFF) },
//			{ BLUE, new Color32(0x00, 0x00, 0xff, 0xFF) },
//			{ RED, new Color32(0xFF, 0x00, 0x00, 0xFF) }
//		};
//				
		private List<CommonShapes_Shape> _shapes;
		private CommonShapes_Shape incorrectShape;
		private CommonShapes_Shape currentShape;
		private string currentShapeType;
		private string currentShapeColor;
		private List<string> _availableTypes;
		private List<string> _availableColors;
		private List<string> _types;
		private List<string> _colors;
		
		private new CommonShapesTutorial tutorial_mc;
		
		private DisplayObjectContainer _shapeContainer_mc;
		
		private string currentClickedType;
		private float baseScale;
		
		/// <summary>
		/// Gets or sets the number shapes to be used per Opportuniyu.
		/// </summary>
		/// <value>
		/// The number shapes.
		/// </value>
		private int numShapes
		{
			get{  return _shapes.Count; }
			set
			{
				int i = 0;
				
				for(i = 0; i < _shapes.Count; i++)
				{
					_shapes[i].Target.mouseEnabled = false;
					_shapes[i].removeEventListener( MouseEvent.MOUSE_DOWN, shapeDown );
					Tweener.removeTweens( _shapes[i] );
					_shapeContainer_mc.removeChild( _shapes[i] );
				}
				
				_shapes.Clear();
				
				MovieClip answerShape_mc = MovieClipFactory.CreateCommonShapesObject(TRIANGLE);
				baseScale = 351.6f/answerShape_mc.width;
				
				answerShape_mc = MovieClipFactory.CreateCommonShapesObject(currentShapeType);
				//answerShape_mc.width = 1000;
				//answerShape_mc.height = 1000;
				CommonShapes_Shape answerShape = new CommonShapes_Shape( answerShape_mc, currentShapeType, currentShapeColor );
				answerShape.scaleX = answerShape.scaleY = baseScale;
				//answerShape.addEventListener( MouseEvent.CLICK, onShapeClick );
				SetShapeColor(answerShape, currentShapeColor);
				_shapes.Add( answerShape ); 
				_shapeContainer_mc.addChild( answerShape );
				
				string[] preShapes = new string[value];
				string randShape;
				string lastShape = "";
				
				List<string> tempTypes = new List<string>(_types);
				
				for(i = numShapes; i < value; i++)
				{
					do{
						if(tempTypes.Count == 0) tempTypes = new List<string>(_types);
						int r = Mathf.FloorToInt(Random.Range(0, tempTypes.Count));
						if(r == tempTypes.Count) r--;
						randShape = tempTypes[r];
						tempTypes.RemoveAt(r);
					}while(cantUseShape(randShape, lastShape, currentShapeType));
					
					preShapes[i] = lastShape = randShape;
				}
				
				CommonShapes_Shape tempShape;
				if(_level == SkillLevel.Tutorial) preShapes[1] = CIRCLE;
				if(_level == SkillLevel.Tutorial || _level == SkillLevel.Emerging)
				{
					answerShape.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
					answerShape.filters[0].Visible = false;
					
					for(i = numShapes; i < value; i++)
					{
						tempShape = new CommonShapes_Shape( new MovieClip("CommonShapes.swf", preShapes[i]), preShapes[i], currentShapeColor);
						tempShape.scaleX = tempShape.scaleY = baseScale;
						tempShape.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
						tempShape.filters[0].Visible = false;
						//tempShape.addEventListener( MouseEvent.CLICK, onShapeClick );
						SetShapeColor(tempShape, currentShapeColor);
						_shapes.Add( tempShape );
						_shapeContainer_mc.addChild( tempShape);
					}
				}
				else
				{
					string[] usedColors = new string[value];
					usedColors[0] = currentShapeColor;
					string randColor = "";
					
					for(i = numShapes; i < value; i++)
					{
						do{
							int r = Mathf.FloorToInt(Random.Range(0, _colors.Count));
							if(r == _colors.Count) r--;
							randColor = _colors[r];
						}while(haveUsedColor(randColor, usedColors, value));
						
						usedColors[i] = randColor;
						
						tempShape = new CommonShapes_Shape( new MovieClip("CommonShapes.swf", preShapes[i]), preShapes[i], randColor);
						tempShape.scaleX = tempShape.scaleY = baseScale;
						tempShape.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
						tempShape.filters[0].Visible = false;
						//tempShape.addEventListener( MouseEvent.CLICK, onShapeClick );
						SetShapeColor(tempShape, randColor);
						_shapes.Add( tempShape );
						_shapeContainer_mc.addChild( tempShape);
					}
				}
				
				if(_level == SkillLevel.Tutorial)
				{
					_shapes[0].x = (_shapeContainer_mc.width / 2) - 146;
					_shapes[0].y = _shapeContainer_mc.height / 2;
					_shapes[1].alpha = 0;
				}
				else
				{
					int r = Mathf.FloorToInt(Random.Range(0, _shapes.Count));
					if(r == _shapes.Count) r--;
					if(r != 0)
					{
						_shapes.RemoveAt(0);
						if(r == _shapes.Count - 1) _shapes.Add(answerShape);
						else _shapes.Insert(r, answerShape);
					}
				}
				
				if(_level == SkillLevel.Developed || _level == SkillLevel.Completed)
				{
					List<string> mods = new List<string>(){"Standard", "Standard", "Rotated", "Rotated", "Small"};
					int randVal;
					for(i = 0; i < value; i++)
					{
						//if(_shapes[i].ShapeType == CIRCLE) continue;
						randVal = Mathf.FloorToInt(Random.Range(0, mods.Count));
						if(randVal == mods.Count)
							randVal--;
						switch(mods[randVal])
						{
						case "Standard":
							break;
						case "Rotated":
							_shapes[i].rotation = 90;
							_shapes[i].scaleX = _shapes[i].scaleY = baseScale * 0.4f/0.6f;
							break;
						case "Small":
							_shapes[i].scaleX = _shapes[i].scaleY = baseScale * 0.2f/0.6f;
							break;
						}
						mods.RemoveAt(randVal);
					}
				}
				
				if(_level != SkillLevel.Tutorial)
					positionShapes();
			}
		}

		void SetShapeColor (CommonShapes_Shape shape, string color)
		{
			//shape.Target.colorTransform = colorForName[color];
			shape.Target.gotoAndStop( color );
		}
		
		private Sprite findSpriteWithDrawOp(Sprite clip) {
			if(clip.graphics.drawOPs.Count > 0) {
				return clip;
			}
			
			return findSpriteWithDrawOpImpl(clip);
		}
		
		private Sprite findSpriteWithDrawOpImpl(DisplayObjectContainer clip) {
			for(int i = 0; i < clip.numChildren; ++i) {
				var child = clip.getChildAt(i);
				
				var sprite = child as Sprite;
				if(sprite !=  null && sprite.graphics.drawOPs.Count > 0) {
					return sprite;
				}
				
				var container = child as DisplayObjectContainer;
				if(container != null) {
					var result = findSpriteWithDrawOpImpl(container);
					if(result != null) {
						return result;
					}
				}
			}
			return null;
		}
		
		private void positionShapes()
		{
			const int padding = 20;
			float totalWidth = 0;
			float[] widths = new float[_shapes.Count];
			for(int i = 0; i < _shapes.Count; i++ )
			{
				var shape = _shapes[i];
				
				// workaround for inconsistent UniSWF size measurement
				var sprite = findSpriteWithDrawOp(shape.Target);
				var drawRect = sprite.graphics.getBounds();
				if(sprite.rotation == 0) {
					widths[i] = drawRect.width * shape.scaleX;
				} else {
					widths[i] = drawRect.height * shape.scaleY;
				}
				
				totalWidth += widths[i];
				DebugConsole.Log("Measure size: {0}, {1}, {2}", drawRect.width, widths[i], shape.width);
			}
			totalWidth += padding * (_shapes.Count-1);
			
			float startX = (_shapeContainer_mc.width - totalWidth) * 0.5f;
			
			for(int i = 0; i < _shapes.Count; i++ )
			{
				var shape = _shapes[i];
				shape.x = startX + (widths[i] * 0.5f);
				shape.y = _shapeContainer_mc.height * 0.5f;
				startX += widths[i] + padding;
				DebugConsole.Log("Shapes " + i + " x: " + shape.x + " y: " + shape.y + ", " + widths[i]);
			}
			
		}
		
		private bool cantUseShape(string rShape, string lShape, string aShape)
		{
			if(rShape == aShape)
				return true;
			if(rShape == lShape)
				return true;
			if(rShape == SQUARE && (aShape == RHOMBUS || aShape == RECTANGLE))
				return true;
			return false;
		}
		
		private bool haveUsedColor(string rColor, string[] tempColors, int colLength)
		{
			for(int i = 0; i < colLength; i++)
			{
				if(rColor == tempColors[i])
					return true;
			}
			return false;
		}
		
		public CommonShapes( SkillLevel level, SessionInfo  currentSession) : base(level, currentSession)
		{
			Init(null);
		}
		
		void OnAssetsLoaded(AssetBundle bundle)
		{
			if( bundle != null )
			{
				Init( null );
			}
		}
		
		public override void Dispose ()
		{
			base.Dispose();
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			if(_shapes != null)
			{
				foreach(CommonShapes_Shape s in _shapes)
				{
					s.Destroy();
				}
			}
			if(currentShape != null)
				currentShape.Destroy();
			if(incorrectShape != null)
				incorrectShape.Destroy();
			if(tutorial_mc != null  && tutorial_mc.circle_mc != null)
				tutorial_mc.circle_mc.Destroy();
		}
		
		/// <summary>
		/// Init the specified parent.
		/// </summary>
		/// <param name='parent'>
		/// Parent MovieClip, not currently used.
		/// </param>
		public override void Init( MovieClip parent)
		{
			base.Init (parent);
			_types = new List<string>(){CIRCLE, SQUARE, RECTANGLE, TRIANGLE, RHOMBUS};
			_colors = new List<string>(){ORANGE, GREEN, PURPLE, YELLOW, BLUE, RED};
			
			_shapeContainer_mc = new DisplayObjectContainer();
			_shapeContainer_mc.width = MainUI.STAGE_WIDTH;
			_shapeContainer_mc.height = MainUI.STAGE_HEIGHT;
			_shapeContainer_mc.alpha = 0;
			addChild(_shapeContainer_mc);
			
			_availableTypes = new List<string>();
			_availableColors = new List<string>();
			_shapes = new List<CommonShapes_Shape>();
			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			switch(_level)
			{
				case SkillLevel.Tutorial:
					totalOpportunities = 1;
					opportunityComplete = false;
					if(!resumingSkill)
					{
						tutorial_mc = new CommonShapesTutorial();
						tutorial_mc.View.addEventListener( TutorialEvent.START, onTutorialStart );
						tutorial_mc.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
						tutorial_mc.Resize( Screen.width, Screen.height );
					}
					else {
						nextOpportunity();
					}
				break;
				default:
					totalOpportunities = 10;
					
					if(resumingSkill)
					{
						parseSessionData( currentSession.sessionData );
					//	nextOpportunity();
					}
				
				break;
			}
			
			Resize( Screen.width, Screen.height );
		}
		
		public override void Start()
		{
			MarkInstructionStart();
		}

		void HideIncorrectShape ()
		{
			foreach(CommonShapes_Shape cs in _shapes)
				if(cs.filters.Count > 0) cs.filters[0].Visible = false;
			if(incorrectShape != null)
			{
				var temp = incorrectShape;
				Tweener.addTween( incorrectShape, Tweener.Hash("time",0.35f, "alpha",0, "transition",Tweener.TransitionType.easeOutQuint) )
					.OnComplete(() => { temp.visible = false; } );
				incorrectShape = null;
			}
		}
		
		/// <summary>
		/// Raises the audio word complete event.
		/// Handles what happens after specific audio files are played
		/// </summary>
		/// <param name='word'>
		/// Parameter that gets passed in from the delegate.
		/// </param>
		public override void OnAudioWordComplete( string word )
		{
			
			for(int i = 0; i < _shapes.Count; i++)
			{
				//Hide DropShadow
			}
			
			if(opportunityComplete)
			{
				switch(word)
				{
					case CLICK_CORRECT:
						dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
					break;
					
					case THEME_COMPLIMENT: 
						playCorrectReinforcement();
					break;
					
					case YOU_FOUND:
						DebugConsole.Log("got here");
						if(_level == SkillLevel.Tutorial)
						{
							playInstruction("5A-Tutorial-Complete", NOW_LETS);
						}
						else
						{
							OnAudioWordComplete(NOW_LETS);
						}
					break;
					
					case INTRO:
					case PLEASE_FIND:
					case NOW_ITS:
					case NOW_LETS:
					case THIS_IS:
						incorrectShape = null;
						Tweener.addTween( _shapeContainer_mc, Tweener.Hash("time",0.35f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuint) );
						if(opportunityCorrect)
						{
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "1") );
						}
						else
						{
							dispatchEvent( new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0") );
						}
					break;
					case THEME_INTRO:
						playIntroAudio();
					break;
				}
			}
			else
			{
				switch( word )
				{
					case CLICK_INCORRECT:
						dispatchEvent( new SkillEvent(SkillEvent.CRITICISM_REQUEST, true, false) );
					break;
					
					case THEME_CRITICISM:
						playInactivity("5A-this-is-" + currentClickedType + "-say-" + currentClickedType, THIS_IS);
					break;
					
					case THIS_IS:
						HideIncorrectShape ();
						playCurrentShape();
					break;
										
					case INTRO:
						if(_level == SkillLevel.Tutorial)
						{
							tutorial_mc.OnStart();
							addChild( tutorial_mc.View );
						}
						else
						{
							nextOpportunity();
						}
					break;
					case NOW_LETS:
						if(opportunityAnswered)
						{
							resetTutorial();
						}
						else
						{
							goto case NOW_ITS;
						}
					break;
					case PLEASE_FIND:
					case NOW_ITS:
						HideIncorrectShape ();
						startTimer();
						enableCardButtons();
					break;
					case THEME_INTRO:
						if( !currentSession.skipIntro )
						{
							playIntroAudio();
						} else {
							OnAudioWordComplete( INTRO );
						}
					break;
				}
			}
			
		}
		
		/// <summary>
		/// Called when the tutorial complete event is fired.
		/// </summary>
		/// <param name='e'>
		/// Event param.
		/// </param>
		private void onTutorialComplete( CEvent e )
		{
			tutorial_mc.View.removeEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.View.removeEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			removeChild( tutorial_mc.View );
			nextOpportunity();
		}
		
		/// <summary>
		/// Initialized the next opportunity.
		/// </summary>
		public override void nextOpportunity ()
		{
			if(_shapes != null)
			{
				foreach(CommonShapes_Shape s in _shapes)
				{
					s.Destroy();
				}
			}
			MarkInstructionStart();
			int i = 0;
			
			timeoutCount = 0;
			
			if(_availableTypes.Count == 0)
			{
				_types.Shuffle();
				_availableTypes.AddRange(_types);
				_types.Shuffle();
				if(_types[0] == _availableTypes[_availableTypes.Count-1]) {
					var temp = _types[0];
					_types[0] = _types[1];
					_types[1] = temp;
				}
				_availableTypes.AddRange(_types);
				Debug.Log(_availableTypes);
			}
			
			if(_availableColors.Count == 0)
			{
				for(i = 0; i < _colors.Count; i++)
				{
					_availableColors.Add(_colors[i]);
				}
			}
			
			switch(_level)
			{
			case SkillLevel.Tutorial:
				if(currentOpportunity > 0)
				{
					//if(opportunityCorrect)
						//NumCorrect++;
					//else
						//NumIncorrect++;
					tallyAnswers();
				}
				else
				{
					currentOpportunity++;
					currentShapeType = TRIANGLE;
					currentShapeColor = GREEN;
					numShapes = 2;
					
					_shapeContainer_mc.alpha = 1;
					OpportunityStart();
				}
			break;
				
			case SkillLevel.Emerging:
				setNextOpportunity(3);
			break;
				
			case SkillLevel.Developing:
				setNextOpportunity(4);
			break;
				
			case SkillLevel.Developed:
			case SkillLevel.Completed:
				setNextOpportunity(5);
			break;
				
			}
			
		}
		
		/// <summary>
		/// Sets the next opportunity's values.
		/// </summary>
		/// <param name='shapes'>
		/// Number of shapes to be used.
		/// </param>
		private void setNextOpportunity(int shapes)
		{
			
			if(currentOpportunity > 0)
			{
				if(opportunityComplete)
				{
					//if(opportunityCorrect)
						//NumCorrect++;
					//else
						//NumIncorrect++;
				}
				else
				{
					//NumIncorrect++;
				}
			}
			
			if(currentOpportunity < 10)
			{
				opportunityComplete = false;
				opportunityCorrect = false;
				opportunityAnswered = false;
				
				currentOpportunity++;
				currentShapeType = _availableTypes[_availableTypes.Count-1];
				_availableTypes.RemoveAt(_availableTypes.Count-1);
				
				var ranNum = Mathf.FloorToInt(Random.Range( 0, _availableColors.Count ));
				if(ranNum == _availableColors.Count)
					ranNum--;
				currentShapeColor = _availableColors[ranNum];
				_availableColors.RemoveAt(ranNum);
				
				numShapes = shapes;
				
				fadeInShapeContainer().OnComplete( OpportunityStart );
				
			}
			
			else
			{
				tallyAnswers();
				numShapes = 0;
			}
		}
		
		/// <summary>
		/// Starts the current opportunity
		/// </summary>
		public override void OpportunityStart ()
		{
			
			DebugConsole.Log("CommonShapes || Opportunity Start");
			
			string clipName = "";
			switch(_level)
			{
				case SkillLevel.Tutorial:
					clipName = playInstruction( "5A-Tutorial-Instruction", NOW_ITS );
				break;
				
				default:
					clipName = playCurrentShape();
				break;
				
			}
			
			AddNewOppData( audioNames.IndexOf( clipName ) );
			
			foreach( CommonShapes_Shape shape in _shapes )
			{
				AddNewObjectData( formatObjectData( shape.ShapeType + shape.ColorType,
													shape.parent.localToGlobal( new Vector2( shape.x, shape.y ) ),
													shape.ShapeType == currentShapeType ) );
			}
		}
		
		private void shapeDown( CEvent e )
		{
			PlayClickSound();
			currentShape = e.currentTarget as CommonShapes_Shape;
			addEventListener(MouseEvent.MOUSE_UP, onShapeClick);
		}
		
		private float xBuffer = 15;
		/// <summary>
		/// Handles the shape click.
		/// </summary>
		/// <param name='e'>
		/// Event param.
		/// </param>
		private void onShapeClick( CEvent e )
		{
			removeEventListener(MouseEvent.MOUSE_UP, onShapeClick);
				
			disableCardButtons();
			
			SoundEngine.Instance.SendCancelToken();
			//SoundEngine.Instance.StopAll();
			
			stopTimer();
			timeoutCount = 0;
			
			CommonShapes_Shape clickedShape = currentShape;
			clickedShape.removeEventListener(MouseEvent.MOUSE_DOWN, shapeDown);
			
			clickedShape.scaleX = clickedShape.scaleY = baseScale * (4f/3f);
			_shapeContainer_mc.removeChild( clickedShape );
			_shapeContainer_mc.addChild( clickedShape );
			
			Rectangle csRect = clickedShape.getBounds(this);
			
			// This solution is hacky, but for some reason, the _shapeContainer_mc is not only a different size than the Stage, it's x is off screen by -170.
			// So this works perfectly.
			if(csRect.right > Screen.width - xBuffer)
			{
				while(csRect.right > Screen.width - xBuffer)
				{
					clickedShape.x -= 1;
					csRect = clickedShape.getBounds(this);
				}
			}
			else if( csRect.left < xBuffer )
			{
				while(csRect.left < xBuffer)
				{
					clickedShape.x += 1;
					csRect = clickedShape.getBounds(this);
				}
			}
			
			DebugConsole.Log("clickedShape.ShapeType: " + clickedShape.ShapeType);
			currentClickedType = clickedShape.ShapeType.ToLower();
			
			if(clickedShape.filters.Count < 1) clickedShape.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
			clickedShape.filters[0].Visible = true;
			
			
			HideIncorrectShape();
			
			if(clickedShape.ShapeType == currentShapeType && clickedShape.ColorType == currentShapeColor)
			{
				if(!opportunityAnswered)
				{
					opportunityAnswered = true;
					opportunityCorrect = true;
				}
				opportunityComplete = true;
				
				for(int i = 0; i < _shapes.Count; i++)
				{
					if(_shapes[i].ShapeType != currentShapeType && _shapes[i].ColorType != currentShapeColor)
						_shapes[i].visible = false;
				}
				
				if(clickedShape.filters.Count < 1) clickedShape.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
				clickedShape.filters[0].Visible = true;
				
				PlayCorrectSound();
				
				AddNewActionData( formatActionData( "tap", _shapes.IndexOf(clickedShape), Vector2.zero, true ) );
				
			}
			else
			{
				if(!opportunityAnswered)
				{
					opportunityAnswered = true;
					opportunityCorrect = false;
				}
				
				incorrectShape = clickedShape;
				
				if(clickedShape.filters.Count < 1) clickedShape.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
				clickedShape.filters[0].Visible = true;
				
				PlayIncorrectSound();
				
				AddNewActionData( formatActionData( "tap", _shapes.IndexOf(clickedShape), Vector2.zero, false ) );
			}
			
		}
		
		/// <summary>
		/// Called when BaseSkill's timer completes.
		/// </summary>
		public override void onTimerComplete()
		{
			base.onTimerComplete();
			
			stopTimer();
			
			timeoutCount++;
			
			string clipName = "";
			
			switch(timeoutCount)
			{
				case 1:
				if(_level == SkillLevel.Tutorial)
				{
					clipName = playInstruction("5A-Tutorial-Instruction", NOW_ITS);
				}
				else
				{
					clipName = playCurrentShape();
				}
				break;
				
				case 2:
					if(_level == SkillLevel.Tutorial)
					{
						if(!opportunityAnswered)
						{
							opportunityAnswered = true;
							opportunityCorrect = true;
							disableCardButtons();
							clipName = playInstruction("5A-Last-Tutorial-Inactivity-Prompt", NOW_LETS);
						}
						else
						{
							opportunityCorrect = false;
							opportunityComplete = true;
							dispatchEvent(new SkillEvent(SkillEvent.OPPORTUNITY_COMPLETE, true, false, "0"));
						}
					
					}
					else
					{
						clipName = playInstruction("5A-Inactivity-Button", PLEASE_FIND);
					}
				break;
				
				case 3:
					
					if(!opportunityAnswered)
					{
						opportunityAnswered = true;
						opportunityCorrect = false;
					}
					
					clipName = playCurrentShape();
					
					for(int i = 0; i < _shapes.Count; i++)
					{
						if(_shapes[i].ShapeType == currentShapeType)
						{
							if(_shapes[i].filters.Count < 1) _shapes[i].AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
							_shapes[i].filters[0].Visible = true;
						}
					}
				
				break;
				
				case 4:
					timeoutEnd = true;
					
					if(!opportunityAnswered)
					{
						opportunityAnswered = true;
						opportunityCorrect = false;
					}
					
					opportunityComplete = true;
					disableCardButtons();
			
					clipName = playInactivity(string.Format("5A-this-is-{0}-say-{0}", currentShapeType.ToLower()), THIS_IS);
				
					for(int i = 0; i < _shapes.Count; i++)
					{
						if(_shapes[i].ShapeType == currentShapeType)
						{
							CommonShapes_Shape rightShape = _shapes[i];
							rightShape.scaleX = rightShape.scaleY = baseScale * (4f/3f);
							if(rightShape.filters.Count < 1) rightShape.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
							rightShape.filters[0].Visible = true;
						
							Rectangle csRect = rightShape.getBounds(this);
			
							// This solution is hacky, but for some reason, the _shapeContainer_mc is not only a different size than the Stage, it's x is off screen by -170.
							// So this works perfectly.
							if(csRect.right > Screen.width - xBuffer)
							{
								while(csRect.right > Screen.width - xBuffer)
								{
									rightShape.x -= 1;
									csRect = rightShape.getBounds(this);
								}
							}
							else if( csRect.left < xBuffer )
							{
								while(csRect.left < xBuffer)
								{
									rightShape.x += 1;
									csRect = rightShape.getBounds(this);
								}
							}
						}
						else
						{
							Tweener.addTween( _shapes[i], Tweener.Hash("time",0.35f, "alpha",0, "transition",Tweener.TransitionType.easeOutQuint) );
						}
					}
				break;
			}
			
			if( !string.IsNullOrEmpty( clipName ) )
			{
				baseTheme.AddAudioSessionData( "time", audioNames.IndexOf(clipName) );
			}
			
		}
		
		protected override void resetTutorial()
		{
			_shapeContainer_mc.alpha = 0;
			currentOpportunity = 0;
			tutorial_mc.circle_mc.Destroy();
			
			timeoutCount = 0;
			
			DebugConsole.Log("resetting tutorial");
					
			tutorial_mc = null;
			tutorial_mc = new CommonShapesTutorial();
			
			addChild( tutorial_mc.View );
			tutorial_mc.View.visible = true;
			tutorial_mc.View.addEventListener( TutorialEvent.START, onTutorialStart );
			tutorial_mc.View.addEventListener( TutorialEvent.COMPLETE, onTutorialComplete );
			tutorial_mc.Resize( Screen.width, Screen.height );
			tutorial_mc.OnStart();
		}
		
		/// <summary>
		/// Fades in the shape container.
		/// </summary>
		/// <returns>
		/// Returns the TweenerObj used to fade the container.
		/// </returns>
		private TweenerObj fadeInShapeContainer()
		{
			return Tweener.addTween(_shapeContainer_mc, Tweener.Hash("time", 0.35f, "alpha", 1, "transition", Tweener.TransitionType.easeInOutQuint) );
		}
		
		/// <summary>
		/// Pla the intro audio.
		/// </summary>
		private string playIntroAudio()
		{
			if( !currentSession.skipIntro )
			{
				SoundEngine.SoundBundle bundle;
				AudioClip clip;
				
				bundle = new SoundEngine.SoundBundle();
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.COMMON_SHAPES, "Story_Introduction/5A-Story-Introduction" );
				bundle.AddClip( clip, INTRO, clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
			}
			else
				OnAudioWordComplete( INTRO );
			
			return "Story_Introduction/5A-Story-Introduction";
		}
		
		/// <summary>
		/// Plays the specified instruction audio. 
		/// </summary>
		/// <param name='type'>
		/// Type - Name of the audio file to be used from asset bundle
		/// </param>
		/// <param name='ID'>
		/// ID - word 'delegate' to be used in OnAudioWordComplete
		/// </param>
		private string playInstruction( string type, string ID )
		{
			SoundEngine.SoundBundle bundle;
			AudioClip clip;
			
			bundle = new SoundEngine.SoundBundle();
			clip = SoundUtils.LoadGlobalSound( MovieClipFactory.COMMON_SHAPES, "Skill_Instructions/" + type);
			
			//if we have the sound asset, play and wait for ID event to dispatch, else error logged and continue
			if(clip != null){
				bundle.AddClip( clip, ID, clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
			}else{
				OnAudioWordComplete(ID);	
			}
			
			return "Skill_Instructions/" + type;
		}
		
		private string playInactivity( string type, string ID )
		{
			SoundEngine.SoundBundle bundle;
			AudioClip clip;
			
			bundle = new SoundEngine.SoundBundle();
			clip = SoundUtils.LoadGlobalSound( MovieClipFactory.COMMON_SHAPES, "Inactivity_Reinforcement/" + type);
			
			//if we have the sound asset, play and wait for ID event to dispatch, else error logged and continue
			if(clip != null){
				bundle.AddClip( clip, ID, clip.length );
				SoundEngine.Instance.PlayBundle( bundle );
			}else{
				OnAudioWordComplete(ID);	
			}
			
			return "Inactivity_Reinforcement/" + type;
		}
		
		private string playCorrectReinforcement()
		{
			string path = string.Format("Reinforcement/5A-you-found-{0}-say-{0}", currentShapeType.ToLower());
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.COMMON_SHAPES, path);
			var bundle = new SoundEngine.SoundBundle();
			bundle.AddClip( clip, YOU_FOUND, clip.length );
			SoundEngine.Instance.PlayBundle( bundle );
			
			return path;
		}
		
		
		/// <summary>
		/// Play the current shape audio.
		/// </summary>
		private string playCurrentShape()
		{
			SoundEngine.SoundBundle bundle;
			AudioClip clip;
			
			DebugConsole.Log("currentShapeType: " + currentShapeType);
			
			bundle = new SoundEngine.SoundBundle();
			clip = SoundUtils.LoadGlobalSound( MovieClipFactory.COMMON_SHAPES, "Skill_Instructions/5A-Question-" + currentShapeType.ToLower());
			bundle.AddClip( clip, PLEASE_FIND, clip.length );
			SoundEngine.Instance.PlayBundle( bundle );
			
			return "Skill_Instructions/5A-Question-" + currentShapeType.ToLower();
		}
		
		/// <summary>
		/// Disables the card buttons.
		/// </summary>
		private void disableCardButtons()
		{
			for(int i = 0; i < _shapes.Count; i++)
			{
				if(_shapes[i].hasEventListener(MouseEvent.MOUSE_DOWN)) _shapes[i].removeEventListener(MouseEvent.MOUSE_DOWN, shapeDown);
			}
			MarkInstructionStart();
		}
		
		/// <summary>
		/// Enables the card buttons.
		/// </summary>
		private void enableCardButtons()
		{
			for(int i = 0; i < _shapes.Count; i++)
			{
				_shapes[i].addEventListener(MouseEvent.MOUSE_DOWN, shapeDown);
			}
			if(_level != SkillLevel.Tutorial) MarkInstructionEnd();
		}
		
		public override string GetSessionData ()
		{
			if(_level == SkillLevel.Tutorial)
				return "";
			
			string data = "";
			
			foreach(string s in _availableTypes)
			{
				data = data + s + "-";
			}
			
			data = data + currentShapeType;
			
			return data;
		}
		
		private void parseSessionData (string data)
		{
			_availableTypes = new List<string>();
			
			string[] sData = data.Split('-');
			
			foreach( string s in sData )
			{
				_availableTypes.Add(s);
			}
		}
		
		/// <summary>
		/// Resize the specified width and height.
		/// </summary>
		/// <param name='width'>
		/// Width.
		/// </param>
		/// <param name='height'>
		/// Height.
		/// </param>
		public override void Resize( int width, int height )
		{
			this.width = width;
			this.height = height;
			
			_shapeContainer_mc.scaleX = _shapeContainer_mc.scaleY = (float)height / (float)MainUI.STAGE_HEIGHT;
			_shapeContainer_mc.y = 0;
			_shapeContainer_mc.x = (width - (MainUI.STAGE_WIDTH*_shapeContainer_mc.scaleX))/2;
		}
		
		private static List<string> audioNames = new List<string>
		{	"",
			"Story Introduction/5A-Story-Introduction",
			"Skill_Instructions/5A-Tutorial-Intro-Henry",
			"Skill_Instructions/5A-Tutorial-Intro-Platty",
			"Skill_Instructions/5A-Tutorial-Intro-Cami",
			"Skill_Instructions/5A-Tutorial-Instruction",
			"Skill_Instructions/5A-Tutorial-Complete",
			"Skill_Instructions/5A-Inactivity-Button",
			"Skill_Instructions/5A-Last-Tutorial-Inactivity-Prompt",
			"Skill_Instructions/5A-Question-circle",
			"Skill_Instructions/5A-Question-rectangle",
			"Skill_Instructions/5A-Question-rhombus",
			"Skill_Instructions/5A-Question-square",
			"Skill_Instructions/5A-Question-triangle",
			"Reinforcement/5A-Character-Reinforcement",
			"Reinforcement/5A-TT-you-found-the-triangle",
			"Reinforcement/5A-you-found-circle-say-circle",
			"Reinforcement/5A-you-found-rectangle-say-rectangle",
			"Reinforcement/5A-you-found-rhombus-say-rhombus",
			"Reinforcement/5A-you-found-square-say-square",
			"Reinforcement/5A-you-found-triangle-say-triangle",
			"Inactivity_Reinforcement/5A-this-is-circle-say-circle",
			"Inactivity_Reinforcement/5A-this-is-rectangle-say-rectangle",
			"Inactivity_Reinforcement/5A-this-is-rhombus-say-rhombus",
			"Inactivity_Reinforcement/5A-this-is-square-say-square",
			"Inactivity_Reinforcement/5A-this-is-triangle-say-triangle",
			"User-Earns-Artifact"
		};
	}
}