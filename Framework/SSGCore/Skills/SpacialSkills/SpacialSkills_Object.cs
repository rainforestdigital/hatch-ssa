using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;

namespace SSGCore
{

	public class SpacialSkills_Object : FilterMovieClip
	{
		public int id = -1;
		private string orientationType;
		public string OrientationType
		{
			get{ return orientationType; }
			set{ orientationType = value; }
		}
		
		public SpacialSkills_Object( MovieClip mcTarget, string _type ) : base( mcTarget )
		{
			orientationType = _type;
		}
		
	}
	
}