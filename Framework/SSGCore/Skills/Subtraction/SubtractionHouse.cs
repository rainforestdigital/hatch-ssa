using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;

namespace SSGCore
{
	public class SubtractionHouse : DisplayObjectContainer
	{
		private MovieClip clip_mc;
		private FilterMovieClip door_mc;

		public FilterMovieClip door
		{
			get{ return door_mc; }
		}

		public MovieClip house
		{
			get { return clip_mc; }
		}
		
		public SubtractionHouse (string type)
		{
			clip_mc = MovieClipFactory.CreateSubtractionHouse();
			addChild(clip_mc);
			
			setType(type);
		}
		
		public void setType (string type)
		{
			switch(type)
			{
				case "EG":
					clip_mc.gotoAndStop(1);
					break;
				case "DG":
					clip_mc.gotoAndStop(3);
					break;
				case "DD":
					clip_mc.gotoAndStop(5);
					break;
				default:
					Debug.Log("SubtractionHouse || Invalid type, defaulting to bear.");
					goto case "EG";
			}
			getDoor();
		}
		
		public void openDoor()
		{
			if(clip_mc.currentFrame%2 == 0)
			{
				clip_mc.gotoAndStop(clip_mc.currentFrame - 1);
				getDoor();
			}
			glowOff();
		}
		
		public void closeDoor()
		{
			if(clip_mc.currentFrame%2 == 1)
			{
				clip_mc.gotoAndStop(clip_mc.currentFrame + 1);
				getDoor();
			}
			else
				Debug.Log("SubtractionHouse || Logic Error: Door told to close while already closed.");
			glowOff();
		}
		
		public void glowOn()
		{
			while(door_mc.filters.Count > 1)
			{
				door_mc.filters[1].Destroy();
				door_mc.filters[1].Visible = false;
				door_mc.RemoveFilter(door_mc.filters[1]);
			}
			if(door_mc.filters.Count == 0)
				door_mc.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
			door_mc.filters[0].Visible = true;
		}
		
		public void glowOff(){ glowOff(1); }
		public void glowOff( int num )
		{
			foreach(MovieClipFilter f in door_mc.filters)
			{
				f.Visible = false;
			}
			door_mc.Destroy();
		}
		private void getDoor()
		{
			if(door_mc != null)
			{
				glowOff(0);
				door_mc.Destroy();
				removeChild(door_mc);
				door_mc = null;
			}
			door_mc = new FilterMovieClip(clip_mc.getChildByName<MovieClip>("Switch"));
			addChild(door_mc);
		}
	}
}

