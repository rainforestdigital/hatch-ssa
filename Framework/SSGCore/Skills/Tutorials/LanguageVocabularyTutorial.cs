using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;

namespace SSGCore
{

	public class LanguageVocabularyTutorial : Tutorial
	{
		public float TUTORIAL_OFFSET_X = -935.4f + 35.0f; //BasePosition + nudging
		public float TUTORIAL_OFFSET_Y = 390.3f;
		
		public const float CARD_X_BUFFER  = .2f; //percent of screen
		
		public LanguageVocabulary skillRef;
		
		private DisplayObjectContainer view = new DisplayObjectContainer();
		
		private MovieClip tutorial_mc;
		
		public FilterMovieClip card_1;
		public FilterMovieClip card_2;
		
		private TimerObject inactivityTimer;
		private int inactivityCounter = 0;
		public bool restarted;
		public bool correct;
		
		public LanguageVocabularyTutorial()
		{
			tutorial_mc = MovieClipFactory.CreateLanguageVocabularyTutorial();
			tutorial_mc.gotoAndStop(2);
			tutorial_mc.width = MainUI.STAGE_WIDTH;
			tutorial_mc.height = MainUI.STAGE_HEIGHT;
			tutorial_mc.alpha = 0;
			
			view.x = 0;
			view.y = 0;
			
			MovieClip card_mc = new MovieClip( "LanguageVocabulary.swf", "Card" );
			card_1 = new FilterMovieClip(card_mc);
			card_1.Target.gotoAndStop("CAT ON A HOUSETOP");
			
			card_mc = new MovieClip( "LanguageVocabulary.swf", "Card" );
			card_2 = new FilterMovieClip(card_mc);
			card_2.Target.gotoAndStop("FISH IN A FISHBOWL");
			
			view.addChild(card_1);
			view.addChild(card_2);
					
			view.addChild( tutorial_mc );

			float xOffset = 0;
			float yOffset = 0;

			if( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA )
			{
				xOffset = card_2.x - TUTORIAL_OFFSET_X;
				yOffset = card_2.y - TUTORIAL_OFFSET_Y - (MainUI.STAGE_HEIGHT - card_2.y);
			}
			else if(PlatformUtils.GetPlatform() == Platforms.ANDROID)
			{
				xOffset = -400;

				yOffset = 0;
			}
			else
			{
				xOffset = -490;
				yOffset = 0;
			}

			//Position Henry's Arm
			tutorial_mc.x = xOffset;//card_2.x - TUTORIAL_OFFSET_X;
			tutorial_mc.y = yOffset;
			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			restarted = false;
		}
		
		public void OnStart()
		{
			//Setup
			correct = true;
			
			tutorial_mc.gotoAndStop(2);
			//Position and scale cards
//			float scale = (float)Screen.height/(float)Screen.width == .75f ? 1.25f : 1.5f;
//			if(PlatformUtils.GetPlatform() == Platforms.IOSRETINA) scale = 1.85f;

//			float scaleOffset = 0.65f; //Used to make the cards fit into the background section
			float cardOffset = 102;
			if(PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA)
			{
//				scale = 1f * scaleOffset;
			}
			else
			{
				cardOffset = 57;
//				scale = 1.25f * scaleOffset;
			}

//			view.y = 0;
			//card_1.Target.scaleX = card_1.Target.scaleY = scale;
			//card_2.Target.scaleX = card_2.Target.scaleY = scale;
			
			float startY = ((Screen.height *.5f) - (card_1.Target.getBounds(view).height * .5f));
			card_1.Target.y = startY - cardOffset;
			card_2.Target.y = startY - cardOffset;
			
			card_1.x = (Screen.width / 2.0f) - (card_1.Target.getBounds(view).width) - 10;
			card_2.x = (Screen.width / 2.0f) + 10;// + ((card_2.Target.getBounds(view).width * scale) * .5f);

			card_1.Target.gotoAndStop("the-cat-on-rooftop");
			card_2.Target.gotoAndStop("fish-in-fishbowl ");

			tutorial_mc.addFrameScript(50, henryCardClick);
			tutorial_mc.addFrameScript( 100, onHenryFound );
			tutorial_mc.alpha = 0;

			//Start
			View.visible = true;
			view.dispatchEvent( new TutorialEvent(TutorialEvent.START) );
//			tutorial_mc.gotoAndPlay(2);
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			//AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LANGUAGE_VOCABULARY, "3A-Skill Instruction Audio/3A-heres-how-to-play-the-game" );
			//bundle.AddClip(clip, "HOW_TO_PLAY", clip.length);
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LANGUAGE_VOCABULARY, "3A-Skill Instruction Audio/3A-2-TT-Instruction" );
			bundle.AddClip(clip, "HENRY_IS_LOOKING", clip.length);
			SoundEngine.Instance.PlayBundle(bundle);
			
			//Need to move the cards apart a bit due to new assets. Can only move let card because of the animation
//			card_1.x -= card_1.width * .5f;
		}
		
		private void henryCardClick(CEvent e)
		{
			card_2.Target.ScaleCentered(card_2.Target.scaleX * 1.25f);
			card_2.AddFilter( FiltersFactory.GetScaledYellowGlowFilter());
			card_2.filters[0].Visible = true;
			view.removeChild(card_2);
			view.addChildAt(card_2, 1);
			
			SoundUtils.PlaySimpleEffect("ding");
		}
		
		private void onHenryFound( CEvent e )
		{
			tutorial_mc.stop();
			Tweener.addTween(tutorial_mc, Tweener.Hash("time",0.5f, "alpha",0, "transition",Tweener.TransitionType.easeInOutQuad) );
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LANGUAGE_VOCABULARY, "3A-Reinforcement Audio/3A-36-TT-that-is-fish-in-fishbowl" );
			bundle.AddClip(clip, "HENRY_FOUND", clip.length);

			SoundEngine.Instance.PlayBundle(bundle);
		}
		
		private void tutorialComplete( CEvent e )
		{
//			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			OnComplete();	
		}
		
		public void OnComplete()
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
		}
		
		public void Resize( int width, int height )
		{
			view.width = width;
			view.height = height;
			
			float targWidth = view.width;
			if( PlatformUtils.GetPlatform() == Platforms.WIN ) targWidth = 1920f;
			if( PlatformUtils.GetPlatform() == Platforms.ANDROID ) targWidth = 1280f;
			
			view.scaleX = view.scaleY = width/targWidth;

			view.y = (height - (view.height*view.scaleY))/2f;
			view.x = (width - (view.width*view.scaleX))/2f;
			
//			if(PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA) tutorial_mc.FitToParent();
//			view.y = 0;
//			view.x = 0;
		}
		
		public DisplayObject View
		{
			get{ return view; }
		}
		
		public void PlayersTurn()
		{
			card_2.Target.ScaleCentered(card_1.Target.scaleX);
//			view.removeChild(card_2);
			card_2.Target.alpha = 0;
			if(card_2.filters.Count > 0) card_2.filters[0].Visible = false;
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LANGUAGE_VOCABULARY, "3A-Skill Instruction Audio/3A-3-TT-Now-your-turn" );
			bundle.AddClip(clip, "YOUR_TURN", clip.length);
			SoundEngine.Instance.PlayBundle( bundle );
			
			skillRef.AddNewOppData( LanguageVocabulary.audioNames.IndexOf( "3A-Skill Instruction Audio/3A-3-TT-Now-your-turn" ) );
			
			skillRef.AddNewObjectData( skillRef.formatObjectData( LanguageVocabularyLibrary.CAT_ON_A_HOUSETOP, card_1.parent.localToGlobal(new Vector2( card_1.x, card_1.y )), true ) );
			
			Debug.LogWarning( skillRef.CurrentSession.SerializedData.toString() );
		}
		
		public void OnCardClicked(CEvent e)
		{
			inactivityTimer.StopTimer();
			
			card_1.Target.ScaleCentered(card_1.Target.scaleX * 1.25f);
			card_1.AddFilter( FiltersFactory.GetScaledYellowGlowFilter());
			card_1.filters[0].Visible = true;
			view.removeChild(card_1);
			view.addChildAt(card_1, 1);
			
			Debug.Log ("CARD CLICKED - IS SKILLREF NULL?: " + (skillRef == null) + "Skill Ref type = " + skillRef.GetType().ToString());
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( "ding" );
			bundle.AddClip(clip, "TUT_DING", clip.length);
			SoundEngine.Instance.PlayBundle( bundle );
			
			card_1.removeEventListener(MouseEvent.CLICK, OnCardClicked);
			
			skillRef.AddNewActionData( skillRef.formatActionData( "tap", 0, Vector2.zero ) );
		}
		
		public void OnAudioWordComplete(string word)
		{
			Debug.LogError("LANGUAGE VOCAB TUTORIAL WORD: " + word);
			
			if(word == "HENRY_IS_LOOKING")
			{
				Debug.LogError("word is henry is looking");
				Tweener.addTween(tutorial_mc, Tweener.Hash("time",0.5f, "alpha",1, "transition",Tweener.TransitionType.easeInOutQuad) );
				tutorial_mc.gotoAndPlay(1);
			}
			
			if(word == "HENRY_FOUND")
			{
				PlayersTurn();		
			}
			
			if(word == "YOUR_TURN")
			{
				card_1.addEventListener(MouseEvent.CLICK, OnCardClicked);
				
				//Start inactivity timer
				inactivityTimer = TimerUtils.SetTimeout(5.0f, InactivityTimerComplete);
			}
			
			if(word == "START_TIMER")
			{
				inactivityTimer = TimerUtils.SetTimeout(5.0f, InactivityTimerComplete);
			}
			
			if(word == "TUT_DING")
			{
				view.parent.dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
			}
			
			if(word == "THEME_COMPLIMENT")
			{
				//Play Reinforcement
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LANGUAGE_VOCABULARY, "3A-Reinforcement Audio/3A-37-TT-you-found-the-cat-on-rooftop" );
				bundle.AddClip(clip, "REINFORCEMENT", clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
			}
			
			if(word == "REINFORCEMENT")	
			{
				
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LANGUAGE_VOCABULARY, "3A-Skill Instruction Audio/3A-4-TT-now-lets-show" );
				bundle.AddClip(clip, "FIND_HENRYS_CARDS", clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
			}
			
			if(word == "FIND_HENRYS_CARDS")
			{
				OnComplete();	
			}
			
			if(word == "RESTART")
			{
				Restart();
			}
			
			
		}
		
		public void Restart()
		{
			inactivityCounter = 0;
			inactivityTimer.StopTimer();
			card_1.Destroy();
			card_2.Destroy();
			card_2.Target.alpha = 1.0f;
			tutorial_mc.alpha = 1.0f;
			OnStart();
			restarted = true;
		}
		
		
		public void CardExpand(MovieClip mc)
		{
			//cardClicked.ScaleCentered( cardClicked.Target.scaleX + 0.15f );
			//Get the card's center position, scale it, get the new center position, and move it back according to the difference
			Vector2 oldPoint = mc.localToGlobal(new Vector2(mc.width * .5f, mc.height * .5f));
			
			mc.scaleX *= 1.25f;
			mc.scaleY *= 1.25f;
			
			Vector2 newPoint = mc.localToGlobal(new Vector2(mc.width * .5f, mc.height * .5f));
			
			Vector2 difference = newPoint - oldPoint;
			mc.x -= difference.x * .5f;
			mc.y -= difference.y * .5f;	
		}
		
		private void InactivityTimerComplete()
		{
			skillRef.dispatchEvent( new SkillEvent(SkillEvent.TIMER_COMPLETE, true, false) );
			if(inactivityCounter == 0)
			{
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LANGUAGE_VOCABULARY, "3A-Skill Instruction Audio/3A-3-TT-Now-your-turn" );
				bundle.AddClip(clip, "START_TIMER", clip.length);
				SoundEngine.Instance.PlayBundle( bundle );	
				inactivityCounter++;
				
				skillRef.baseTheme.AddAudioSessionData( "time", LanguageVocabulary.audioNames.IndexOf("3A-Skill Instruction Audio/3A-3-TT-Now-your-turn") );
				return;
			}
			
			if(inactivityCounter == 1)
			{
				if(restarted)
				{
					//skillRef.timeoutEnd = true;
					correct = false;
					view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
					return;
				}
				
				card_1.removeEventListener(MouseEvent.CLICK, OnCardClicked);
				
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LANGUAGE_VOCABULARY, "3A-Skill Instruction Audio/3A-6-try-again" );
				bundle.AddClip(clip, "RESTART", clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
				
				skillRef.baseTheme.AddAudioSessionData( "time", LanguageVocabulary.audioNames.IndexOf("3A-Skill Instruction Audio/3A-6-try-again") );
				return;
			}
		}
	}
}