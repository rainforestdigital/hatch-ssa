using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;
using pumpkin.geom;

namespace SSGCore
{
	public class AdditionUpToTenTutorial : Tutorial
	{
		public DisplayObjectContainer view;
		private MovieClip tutorial_mc;
		
		private List<FilterMovieClip> glows;
		
		public AdditionUpToTenTutorial ()
		{
			view = new DisplayObjectContainer();
			view.alpha = 0;
			
			tutorial_mc = MovieClipFactory.CreateAdditionUpToTenTutorial();
			tutorial_mc.gotoAndStop(1);
			tutorial_mc.looping = false;
			view.addChild( tutorial_mc );
			
			glows = new List<FilterMovieClip>();
			
			tutorial_mc.addFrameScript( 10, tutorialContinue);
			tutorial_mc.addFrameScript( 165, glowBowlChillies );
			tutorial_mc.addFrameScript( 230, endGlowAll );
			tutorial_mc.addFrameScript( 285, glowSingleChilli );
			tutorial_mc.addFrameScript( 340, endGlowAll );
			tutorial_mc.addFrameScript( 355, playClick );
			tutorial_mc.addFrameScript( 390, promptShopkeep );
			tutorial_mc.addFrameScript( 445, glowShopkeep );
			tutorial_mc.addFrameScript( 485, endGlowAll );
			tutorial_mc.addFrameScript( 505, playDing );
			tutorial_mc.addFrameScript( 535, tutorialReinforcement );
			tutorial_mc.addFrameScript( 560, glowBowlChillies );
			tutorial_mc.addFrameScript( 625, endGlowAll );
			tutorial_mc.addFrameScript( 680, glowSingleChilli );
			tutorial_mc.addFrameScript( 715, endGlowAll );
			tutorial_mc.addFrameScript( 755, glowAll );
			tutorial_mc.addFrameScript( 810, endGlowAll );
			tutorial_mc.addFrameScript( 840, nowIts );
			tutorial_mc.addFrameScript( 880, tutorialComplete );
		}
		
		public void OnStart ()
		{			
			view.dispatchEvent( new TutorialEvent(TutorialEvent.START) );
			
			Tweener.addTween( view, Tweener.Hash("time", 0.25f, "alpha", 1f) );
			
			tutorial_mc.play();
		}
		
		public void tutorialContinue ( CEvent e )
		{
			playSound( "2G-Skill Instructions Audio/2G-we-need-the-right-number-of-chilies");
		}
		
		public void glowBowlChillies( CEvent e )
		{
			//add a glow to chilles in the bowl (2)
			MovieClip mc = tutorial_mc.getChildAt<MovieClip>( 2 );
			
			for( int i = mc.numChildren; i >= 0; i-- )
			{
				MovieClip mcChild = mc.getChildAt<MovieClip>( i );
				if( mcChild != null )
					addFilter( mcChild );
			}
		}
		
		public void glowSingleChilli( CEvent e )
		{
			//add a glow to the chilli on the counter (3)
			addFilter( tutorial_mc.getChildAt<MovieClip>( 3 ) );
		}
		
		public void playClick( CEvent e )
		{
			SoundUtils.PlaySimpleEffect( "click" );
		}
		
		public void promptShopkeep( CEvent e )
		{
			playSound( "2G-Skill Instructions Audio/2G-when-youre-done-touch-shopkeeper");
		}
		
		public void glowShopkeep( CEvent e )
		{
			//add a glow to the shopkeeper (0)
			MovieClip mc = tutorial_mc.getChildAt<MovieClip>( 0 );
//			MovieClip mc0 = mc.getChildAt<MovieClip>( 0 );
			
			DisplayObjectContainer mcParent = mc.parent;
			FilterGroupMovieClip fmc = new FilterGroupMovieClip( mc );
			fmc.removeChild( mc );
			mcParent.addChild( mc );
			view.addChildAt( fmc, 0 );
			fmc.x += mc.x;
			fmc.y += mc.y;
			glows.Add( fmc );
			fmc.AddFilter( FiltersFactory.GetScaledYellowGlowFilter );
		}
		
		public void playDing( CEvent e )
		{
			SoundUtils.PlaySimpleEffect( "ding" );
		}
		
		public void tutorialReinforcement( CEvent e )
		{
			playSound( "2G-Reinforcement Audio/TTEG/2G-5-chilipepper-you-add-1-now-6");
		}
		
		public void glowAll( CEvent e )
		{
			//add a glow to everything
			glowBowlChillies(e);
			glowSingleChilli(e);
			glowShopkeep(e);
		}
		
		public void endGlowAll( CEvent e )
		{
			foreach( FilterMovieClip fmc in glows )
			{
				fmc.Destroy();
				if( fmc.parent != null )
					fmc.parent.removeChild( fmc );
			}
			glows.Clear();
		}
		
		public void nowIts( CEvent e )
		{
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			playSound( "2G-Skill Instructions Audio/2G-now-its-your-turn", "NOW_ITS_tutorial" );
		}
		
		public void OnAudioWordComplete( string word )
		{
			if( word == "NOW_ITS_tutorial" )
				OnComplete();
		}
		
		public void tutorialComplete( CEvent e )
		{
			tutorial_mc.stop ();
		}
		
		public void OnComplete()	
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
		}
		
		private void addFilter( MovieClip mc )
		{
			DisplayObjectContainer mcParent = mc.parent;
			FilterMovieClip fmc = new FilterMovieClip( mc );
			fmc.removeChild( mc );
			mcParent.addChild( mc );
			mc.addChildAt( fmc, 0 );
			fmc.x -= mc.x;
			fmc.y -= mc.y;
			glows.Add( fmc );
			fmc.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
		}
		
		private void playSound ( string clipName ) { playSound(clipName, ""); }
		private void playSound ( string clipName, string word )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.ADDITIONUPTOTEN, clipName );
			
			if(word != "")
				bundle.AddClip(clip, word, clip.length);
			else
				bundle.AddClip(clip);
			
			SoundEngine.Instance.PlayBundle(bundle);
		}
		
		public void Dispose()
		{
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			tutorial_mc.stop();
			
			endGlowAll(null);
		}

		public void Resize( int width, int height )
		{
		}
		
		public DisplayObject View
		{
			get{ return view; }
		}
	}
}