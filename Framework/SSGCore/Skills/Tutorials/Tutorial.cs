using System;
using pumpkin.display;

namespace SSGCore
{

	public interface Tutorial
	{
	
		/// <summary>
		/// Gets the display view.
		/// </summary>
		DisplayObject View{ get; }
		
		/// <summary>
		/// Raises the start event. Dispatches TutorialEvent.Start 
		/// </summary>
		void OnStart();
		
		/// <summary>
		/// Raises the complete event. Dispatches TutorialEvent.Complete 
		/// </summary>
		void OnComplete();
		
		/// <summary>
		/// Resize the view to the specified width and height.
		/// </summary>
		/// <param name='width'>
		/// Width.
		/// </param>
		/// <param name='height'>
		/// Height.
		/// </param>
		void Resize( int width, int height );
	}

}