using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;
using pumpkin.text;

namespace SSGCore
{

	public class ObjectsInASetTutorial : Tutorial
	{
		
		private DisplayObjectContainer view = new DisplayObjectContainer();
		
		private MovieClip tutorial_mc;

		public ObjectsInASet skillRef;

		private FilterMovieClip basketOne;
		private FilterMovieClip basketTwo;
		
		private List<FilterMovieClip> baskets;

		private TimerObject inactivityTimer;
		private int inactivityCounter = 0;
		public bool restarted;
		public bool correct;

//		private float scaleMod;

		private const string INSTRUCTION_PATH = "Narration/2D-Skill Instruction Audio/";
		private const string INITIAL_INSTRUCTION = "2D-Initial Instructions/";

		private Platforms selectedPlatform;

		public ObjectsInASetTutorial()
		{
						
			selectedPlatform = PlatformUtils.GetPlatform ();
			basketOne = new FilterMovieClip( MovieClipFactory.CreateObjectsInASetObject( "yellow_1" ) );
			view.addChild( basketOne );
			
			basketTwo = new FilterMovieClip( MovieClipFactory.CreateObjectsInASetObject( "yellow_2" ) );
			view.addChild( basketTwo );	
			
			baskets = new List<FilterMovieClip>();
			
			baskets.Add(basketOne);
			baskets.Add(basketTwo);
			
			tutorial_mc = MovieClipFactory.CreateObjectsInASetTutorial();
			tutorial_mc.width = MainUI.STAGE_WIDTH;
			tutorial_mc.height = MainUI.STAGE_HEIGHT;
			tutorial_mc.gotoAndStop(1);
			view.addChild(tutorial_mc);
			
			DebugConsole.Log("Basket 1 is null: {0}", (basketOne == null));
			
			positionBaskets();

//			if(basketTwo.Target.width < 600 )
//			{
//				scaleMod = ((float)Screen.height / (float)MainUI.STAGE_HEIGHT); //Used because their docs say to use a specific size, but they also said that specific size is too big on device
//			}
//			else
//			{
//				scaleMod = 1;
//			}


			//basketOne.ScaleCentered(scaleMod);
			//basketTwo.ScaleCentered(scaleMod);


			tutorial_mc.addFrameScript(Mathf.RoundToInt(244 * 0.5f), CountTravellersCompare);
			//tutorial_mc.addFrameScript(Mathf.RoundToInt(300 * 0.5f), CamiTouchBasket);
			tutorial_mc.addFrameScript(Mathf.RoundToInt(579f), HighlightTravellers);
			tutorial_mc.addFrameScript(Mathf.RoundToInt(580f), CamiDing);
			tutorial_mc.addFrameScript (Mathf.RoundToInt (650f), PlayersTurn);
			tutorial_mc.addFrameScript(Mathf.RoundToInt(680f), tutorialComplete);

			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			restarted = false;
			correct = true;
		}
		
		public DisplayObject View{ get {return view;} }
		
		private void positionBaskets()
		{
			
			int padding = 30;
            float additionalYOffset = 0f;

			switch (selectedPlatform) {
			case (Platforms.WIN):
				additionalYOffset = -130f;
				tutorial_mc.y -= 50f;
				break;

			case (Platforms.ANDROID): 
				tutorial_mc.x -= 370f;
				tutorial_mc.y -= 300f;
				additionalYOffset = -150f;
				break;

			case Platforms.IOSRETINA:
				additionalYOffset = -130f;
				break;

			case Platforms.IOS:
				additionalYOffset = -70f;
				tutorial_mc.x -= 370f;
				tutorial_mc.y -= 150f;
				break;
			}


			float totalWidth = (baskets[0].Target.width + padding) * baskets.Count;
			
			for(int i = 0; i < baskets.Count; i++)
			{
				baskets[i].x = ((Screen.width * 0.5f) - (totalWidth * 0.5f)) + (i * (baskets[0].Target.width + padding));
                baskets[i].y = (Screen.height * 0.5f) - (baskets[i].Target.height * 0.5f) + additionalYOffset;
			}

			Resize( Screen.width, Screen.height );
			
		}
		
		private void CountTravellersCompare(CEvent e)
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.OBJECTS_IN_A_SET, INSTRUCTION_PATH + "2D-lets-count-and-compare");
			//bundle.AddClip(clip);
			bundle.AddClip (clip, "CAMI_COUNT", clip.length);
			SoundEngine.Instance.PlayBundle(bundle);
		}

		private void JumpToHighlightFrame()
		{
			tutorial_mc.gotoAndPlay (Mathf.RoundToInt (1120 * 0.5f));
		}
		
		private void HighlightTravellers(CEvent e)
		{
			
			basketTwo.filters[0].Visible = true;
			
		}
		
		private void CamiTouchBasket()
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.OBJECTS_IN_A_SET, INSTRUCTION_PATH + "2D-watch-closely-as-cami" );

			//bundle.AddClip (clip);
			bundle.AddClip(clip, "CAMI_WATCH", clip.length);
			SoundEngine.Instance.PlayBundle(bundle);
			
			//SoundUtils.PlaySimpleEffect("click");
		}
		
		private void CamiDing(CEvent e)
		{
			SoundUtils.PlaySimpleEffect("ding");
		}
		
		public void OnStart()
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.START) );
			tutorial_mc.gotoAndPlay(Mathf.RoundToInt(230 * 0.5f));

			/*
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.OBJECTS_IN_A_SET, INSTRUCTION_PATH + "2D-lets-count-and-compare" );
			bundle.AddClip(clip);
			SoundEngine.Instance.PlayBundle(bundle);
			*/

			basketTwo.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
			basketTwo.filters[0].Visible = false;
			basketTwo.filters[0].GetSprite().mouseEnabled = false;
		}
		
		private void tutorialComplete(CEvent e)
		{
			tutorial_mc.stop();
			//OnComplete();
		}
		
		public void OnComplete()
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
		}

		private void PlayersTurn (CEvent e)
		{
			Debug.Log ("PlayersTurn");

			//tutorial_mc.stop();
			
			basketTwo.Target.alpha = 0;
			if(basketTwo.filters.Count > 0) basketTwo.filters[0].Visible = false;

			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.OBJECTS_IN_A_SET, INSTRUCTION_PATH + "2D-now-its-your-turn" );
			bundle.AddClip(clip, "YOUR_TURN", clip.length);
			SoundEngine.Instance.PlayBundle( bundle );
		}

		public void OnAudioWordComplete(string word)
		{
			Debug.Log(word);

			if(word == "HENRY_IS_LOOKING")
			{
				Tweener.addTween(tutorial_mc, Tweener.Hash("time",0.5f, "alpha",1, "transition",Tweener.TransitionType.easeInOutQuad) );
				tutorial_mc.gotoAndPlay(1);
			}

			if (word == "CAMI_COUNT") {
				//tutorial_mc.gotoAndPlay(Mathf.RoundToInt(560) );
				CamiTouchBasket();
			}

			if(word == "HENRY_FOUND")
			{
				DebugConsole.Log("OBJECTS IN A SET TUT HENRY FOUND");
				//PlayersTurn();		
			}

			if (word == "CAMI_WATCH") {
				JumpToHighlightFrame();
				//tutorial_mc.gotoAndPlay(560f );
			}

			if(word == "YOUR_TURN")
			{
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.OBJECTS_IN_A_SET, INSTRUCTION_PATH + INITIAL_INSTRUCTION + "2D-touch-spacecraft-1-traveler" );
				bundle.AddClip(clip, "FIND_ONE", clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
				
				skillRef.AddNewOppData( ObjectsInASet.audioNames.IndexOf( INSTRUCTION_PATH + INITIAL_INSTRUCTION + "2D-touch-spacecraft-1-traveler" ) );
				
				skillRef.AddNewObjectData( skillRef.formatObjectData( "1", basketOne.parent.localToGlobal(new Vector2(basketOne.x, basketOne.y)) ) );
			}
			
			if(word == "FIND_ONE")
			{
				basketOne.addEventListener(MouseEvent.CLICK, OnBasketClicked);

				//Start inactivity timer
				inactivityTimer = TimerUtils.SetTimeout(5.0f, InactivityTimerComplete);
			}

			if(word == "TUT_DING")
			{
				view.parent.dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
				Debug.Log("Player turn ding");
				//PlayersTurn();
			}

			if(word == "THEME_COMPLIMENT")
			{
				//Play Reinforcement
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.OBJECTS_IN_A_SET, "Narration/2D-Reinforcement Audio/2D-this-spacecraft-has-1-traveler" );
				bundle.AddClip(clip, "REINFORCEMENT", clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
			}

			if(word == "REINFORCEMENT")	
			{

				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.OBJECTS_IN_A_SET, INSTRUCTION_PATH + "2D-look-at-how-many-travelers" );
				bundle.AddClip(clip, "FIND_HENRYS_CARDS", clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
			}

			if(word == "FIND_HENRYS_CARDS")
			{
				OnComplete();	
			}

			if(word == "RESTART")
			{
				Restart();
			}


		}

		private void InactivityTimerComplete()
		{
			skillRef.dispatchEvent( new SkillEvent(SkillEvent.TIMER_COMPLETE, true, false) );
			if(inactivityCounter == 0)
			{
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.OBJECTS_IN_A_SET, INSTRUCTION_PATH + "2D-look-at-how-many-travelers" );
				bundle.AddClip(clip, "YOUR_TURN", clip.length);
				SoundEngine.Instance.PlayBundle( bundle );	
				inactivityCounter++;
				
				skillRef.baseTheme.AddAudioSessionData( "time", ObjectsInASet.audioNames.IndexOf( INSTRUCTION_PATH + "2D-look-at-how-many-travelers" ) );
				return;
			}

			if(inactivityCounter == 1)
			{
				if(restarted)
				{
					//skillRef.timeoutEnd = true;
					correct = false;
					view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
					return;
				}

				basketOne.removeEventListener(MouseEvent.CLICK, OnBasketClicked);

				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.OBJECTS_IN_A_SET, INSTRUCTION_PATH + "2D-try-again" );
				bundle.AddClip(clip, "RESTART", clip.length);
				SoundEngine.Instance.PlayBundle( bundle );
				
				skillRef.baseTheme.AddAudioSessionData( "time", ObjectsInASet.audioNames.IndexOf( INSTRUCTION_PATH + "2D-try-again" ) );
				return;
			}
		}

		private void OnBasketClicked (CEvent e)
		{
			inactivityTimer.StopTimer();
			basketOne.Target.ScaleCentered(basketOne.Target.scaleX * 1.25f);
			basketOne.AddFilter( FiltersFactory.GetScaledYellowGlowFilter());
			basketOne.filters[0].Visible = true;
			basketOne.filters[0].GetSprite().mouseEnabled = false;
			view.removeChild(basketOne);
			view.addChildAt(basketOne, 1);

			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip =  SoundUtils.LoadGlobalSound( "ding" );
			bundle.AddClip(clip, "TUT_DING", clip.length);
			SoundEngine.Instance.PlayBundle( bundle );

			basketOne.removeEventListener(MouseEvent.CLICK, OnBasketClicked);
			
			SoundUtils.PlaySimpleEffect("click");

			skillRef.AddNewActionData( skillRef.formatActionData( "tap", 0, Vector2.zero ) );
		}

		public void Restart()
		{
			inactivityCounter = 0;
			inactivityTimer.StopTimer();
			basketOne.Destroy();
			basketTwo.Destroy();
			basketTwo.Target.alpha = 1.0f;
			tutorial_mc.alpha = 1.0f;
			OnStart();
			restarted = true;
		}

		public void Resize( int width, int height )
		{

			Rect clipping;
			if( selectedPlatform == Platforms.WIN || selectedPlatform == Platforms.IOSRETINA )
				clipping = new Rect( 1f, 1f, 2046f, 1534f );
			else
				clipping = new Rect( 0.5f, 0.5f, 1023f, 767f );
			
			float scale = (width + 4f) / (clipping.width);
			
			view.width = width;
			view.height = height;
			
			view.scaleX = view.scaleY = scale;
			
			view.x = (width - (view.width*view.scaleX))/2;
			view.y = 0;

		}
		
		public void Destroy()
		{
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			
			foreach(FilterMovieClip fmc in baskets)
				fmc.Destroy();
			basketOne.Destroy();
			basketTwo.Destroy();
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
		}
	}
	
}