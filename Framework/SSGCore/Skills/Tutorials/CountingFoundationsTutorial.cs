using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;
using pumpkin.text;

namespace SSGCore
{
	public class CountingFoundationsTutorial : Tutorial
	{
		public bool interactivePortionComplete = false;
		private int stallCount;
		
		private DisplayObjectContainer view = new DisplayObjectContainer();
		public MovieClip tutorial_mc;
		
		public CountingObject frog1_mc;
		public CountingObject frog2_mc;
		
		public CountingFoundationsTutorial()
		{
			tutorial_mc = MovieClipFactory.CreateCountingFoundationsTutorial();
			tutorial_mc.gotoAndStop(1);
			tutorial_mc.width = MainUI.STAGE_WIDTH;
			tutorial_mc.height = MainUI.STAGE_HEIGHT;
			view.addChild( tutorial_mc );
			
			tutorial_mc.addFrameScript( 250/2, OnHenrySeesAFrog );
			tutorial_mc.addFrameScript( 701/2, OnFirstFrogClicked );
			tutorial_mc.addFrameScript( 720/2, OnPlayOne );
			tutorial_mc.addFrameScript( 760/2, stall);
			tutorial_mc.addFrameScript( 785/2, OnNowItsYourTurn );
			
			tutorial_mc.visible = false;
			tutorial_mc.alpha = 0;
		}
		
		private void stall ( CEvent e )
		{
			if(stallCount > 0)
				tutorial_mc.gotoAndPlay(tutorial_mc.currentFrame - 1);
			stallCount--;
		}
		
		public void Restart()
		{
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			frog1_mc.parent.removeChild(frog1_mc);
			frog1_mc.Destroy();
			frog1_mc = null;
			frog2_mc.parent.removeChild(frog2_mc);
			frog2_mc.Target.removeEventListener(MouseEvent.MOUSE_DOWN, OnSecondFrogClicked);
			frog2_mc.Destroy();
			frog2_mc = null;
			CountingFoundations.Instance.bgContainer.SetObjectInvisible(1);
			tutorial_mc.gotoAndStop(1);
			OnStart();
		}
		
		public void OnStart()
		{
			stallCount = 20;

			//PlaySound("Narration Clips/2A-Story Introduction Audio/2A-1-StoryIntroduction" );

			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			tutorial_mc.gotoAndPlay(100);
			
			view.dispatchEvent( new TutorialEvent(TutorialEvent.START) );
			
			frog1_mc = new CountingObject(tutorial_mc.getChildByName<MovieClip>("frog1_mc"), "POND1");
			frog2_mc = new CountingObject(tutorial_mc.getChildByName<MovieClip>("frog2_mc"), "POND3");
			frog1_mc.HideNumber();
			frog2_mc.HideNumber();
			view.addChildAt(frog1_mc, 0);
			view.addChildAt(frog2_mc, 0);
			
			tutorial_mc.visible = true;
			Tweener.addTween(tutorial_mc, Tweener.Hash("time",0.5f, "alpha",1, "transition",Tweener.TransitionType.easeInOutQuad) );
		}
		
		private void OnAudioWordComplete( string word )
		{
			switch( word )
			{
				case "HandleYourTurnEvent":
					HandleYourTurnEvent();
				break;
				
				case "Handle2ClickFinish":
					Handle2ClickFinish();
				break;
				
				case "HandleHenryGet2Frogs":
					HandleHenryGet2Frogs();
				break;
				
				case "HandleNowLetsPlayTheGame":
					CompleteTutorial();
					interactivePortionComplete = true;
				break;
				
				case "TUT_CLICK_CORRECT":
					PlayCompSound();
				break;
			}
		}
		
		private GlowMovieClipFilter GetGlowFilter()
		{		
			return new GlowMovieClipFilter(50, 2, 0.01f, Color.yellow);
		}
		
		public bool PlaySound(string snd)
		{
			return PlaySound(snd, "", 0);
		}
			
		public bool PlaySound(string snd, string evnt)
		{
			return PlaySound(snd, evnt, 0);
		}
				
		public bool PlaySound(string snd, float delay)
		{
			return PlaySound(snd, "", delay);
		}
		
		public bool PlaySound(string snd, string evnt, float delay)
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.COUNTING_FOUNDATIONS, snd );
			
			if( clip != null )
			{
				if( evnt.Length == 0 ) bundle.AddClip(clip);
				else 
				{
					if( delay > 0 ) bundle.AddClip(clip, evnt, delay, false);
					else bundle.AddClip(clip, evnt, clip.length, false);
				}
				SoundEngine.Instance.PlayBundle(bundle);
			}
			else 
			{
				DebugConsole.LogError("COUNTINGFOUNDATIONS TUTORIAL - PlaySound: audio clip is null: " + snd);
				return false;
			}
			
			return true;
		}
		
		public void Resize( int width, int height )
		{		
			view.scaleY = height/(float)MainUI.STAGE_HEIGHT;
			view.scaleX = view.scaleY;
			view.x = -22;//height/10f;
			view.y = -77;//((width - (MainUI.STAGE_WIDTH*view.scaleX))/2) + (width/10f);
		}
		
		private void OnHenrySeesAFrog(CEvent e)
		{
			DebugConsole.Log("ON HENRY SEES A FROG");
			PlaySound("Narration Clips/2A-Skill Instruction Audio/2A-Platty-will-try-first" );
			//CountingFoundations.Instance.henry.PlayAnimation(BaseCharacter.RESTING, false);
			CountingFoundations.Instance.dispatchEvent(new SkillEvent( SkillEvent.ANIMATION_REQUEST, true, false, BaseCharacter.RESTING));
			frog1_mc.ShowFilter();
			frog2_mc.ShowFilter();
		}
		
		private void OnFirstFrogClicked(CEvent e)
		{
			frog1_mc.alpha = 0;
			CountingFoundations.Instance.bgContainer.SetObjectVisible(1, "POND1");
			DebugConsole.Log("FIRST FROG CLICKED");
			
			SoundUtils.PlaySimpleSound("click");
			TimerUtils.SetTimeout(0.05f, click1Cleared);
		}
		
		private void click1Cleared()
		{
			frog1_mc.HideFilter();
			frog2_mc.HideFilter();
			CountingFoundations.Instance.SatisfyAnswer();
			tutorial_mc.play();
		}
		
		private void OnPlayOne(CEvent e)
		{
			DebugConsole.Log("PLAY ONE");
		}
		
		private void OnNowItsYourTurn( CEvent e )
		{
			tutorial_mc.gotoAndStop(tutorial_mc.totalFrames);
			bool playedSound = PlaySound("Narration Clips/2A-Skill Instruction Audio/2A-now-its-your-turn", "HandleYourTurnEvent");
			if( !playedSound ) HandleYourTurnEvent();
			
			CountingFoundations.Instance.AddNewOppData( CountingFoundations.audioNames.IndexOf("Narration Clips/2A-Skill Instruction Audio/2A-now-its-your-turn" ) );
			Vector2 point = frog2_mc.parent.localToGlobal(new Vector2(frog2_mc.x, frog2_mc.y));
			point  = new Vector2( Mathf.Abs(point.x * 2f), Mathf.Abs((Screen.height/2) + point.y) );
			CountingFoundations.Instance.AddNewObjectData( CountingFoundations.Instance.formatObjectData( "POND3", point, true ));
			
			Debug.LogWarning( "session print- " + CountingFoundations.Instance.CurrentSession.SerializedData.toString() );
		}
		
		public void HandleYourTurnEvent()
		{
			CountingFoundations.Instance.startTimer();
			CountingFoundations.Instance.dispatchEvent(new SkillEvent(SkillEvent.OPPORTUNITY_START));
			//TimerUtils.SetTimeout(5f, StartTimerAfterPause);
			frog2_mc.Target.addEventListener(MouseEvent.MOUSE_DOWN, OnSecondFrogClicked);
			frog2_mc.Target.mouseEnabled = true;
		}

		public void disableFrog2() {
			frog2_mc.Target.removeEventListener(MouseEvent.MOUSE_DOWN, OnSecondFrogClicked);
		}
		
		public void TimeoutDeactivate()
		{
			frog2_mc.Target.removeEventListener(MouseEvent.MOUSE_DOWN, OnSecondFrogClicked);
		}
		
		private void OnSecondFrogClicked(CEvent e)
		{
			DebugConsole.Log("SECOND FROG CLICKED");
			
			SoundUtils.PlaySimpleSound("click");
			frog2_mc.Target.removeEventListener(MouseEvent.MOUSE_DOWN, OnSecondFrogClicked);
			view.stage.addEventListener(MouseEvent.MOUSE_UP, HandleFrogClicked);
			CountingFoundations.Instance.stopAllTimers ();
		}
		
		/*private void StartTimerAfterPause(string msg="")
		{
			CountingFoundations.Instance.startTimer();
		}*/
		
		public void HandleFrogClicked(CEvent e)
		{
			DebugConsole.Log("FROG 2 CLICKED");
			view.stage.removeEventListener(MouseEvent.MOUSE_UP, HandleFrogClicked);
			
			frog2_mc.Target.visible = false;
			CountingFoundations.Instance.bgContainer.SetObjectVisible(2, "POND2");			
			
			bool playedSound = PlaySound ("Narration Clips/2A-Skill Game Audio/2A-Numerals/2A-2", "Handle2ClickFinish");	
			
			CountingFoundations.Instance.AddNewActionData( CountingFoundations.Instance.formatActionData("tap", 0, Vector2.zero, true) );
			
			if( !playedSound ) Handle2ClickFinish();
		}
		
		private void PlayCompSound()
		{
			PlayCompSound("");
		}
		
		private void PlayCompSound(string msg)
		{
			CountingFoundations.Instance.dispatchEvent( new SkillEvent(SkillEvent.COMPLIMENT_REQUEST, true, false) );
			
			/*CountingFoundations.Instance.henry.PlayAnimation(BaseCharacter.HAPPY, false);
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			
			string clipName = CountingFoundations.Instance.COMP[UnityEngine.Random.Range(0,CountingFoundations.Instance.COMP.Length)];
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SAFARI_THEME, "TH-SF-Henry/Henry-Complements/"+ clipName );
			
			if( clip != null )
			{
				bundle.AddClip(clip);
				SoundEngine.Instance.PlayBundle(bundle);
				TimerUtils.SetTimeout(clip.length, Handle2ClickFinishDelay); 
			}
			else
			{
				DebugConsole.LogError("AUDIO CLIP FOR PLAYCOMPSOUND RETURNED NULL: " + clipName);
				TimerUtils.SetTimeout(2f, Handle2ClickFinishDelay); 
			}*/
		}
		
		private void Handle2ClickFinish()
		{
			CountingFoundations.Instance.SatisfyAnswer(false);
			
			var clip = SoundUtils.LoadGlobalSound("ding");
			var bundle = new SoundEngine.SoundBundle();
			bundle.AddClip(clip, "TUT_CLICK_CORRECT", clip.length, false);
			SoundEngine.Instance.PlayBundle(bundle);
			
			//CountingFoundations.Instance.henry.PlayAnimation(BaseCharacter.HAPPY, false);
			//TimerUtils.SetTimeout(.5f, PlayCompSound);
			
			// we're at the final opportunity, so the theme will play a "hurray" audio click, so we wait for 4 seconds for that to clear, then we play the final audio
			//TimerUtils.SetTimeout(2f, Handle2ClickFinishDelay);
		}
		
		public void Handle2ClickFinishDelay()
		{
			Handle2ClickFinishDelay("");
		}
		
		public void Handle2ClickFinishDelay(string msg)
		{
			bool playedSound = PlaySound("Narration Clips/2A-Reinforcement Audio/2A-TT/2A-TT-2-frogs", "HandleHenryGet2Frogs");
			if( !playedSound ) HandleHenryGet2Frogs();
		}
		
		private void HandleHenryGet2Frogs()
		{
			bool playedSound = PlaySound("Narration Clips/2A-Skill Instruction Audio/2A-lets-play-tossncount", "HandleNowLetsPlayTheGame" );
			if( !playedSound ) 
			{
				CompleteTutorial();
				interactivePortionComplete = true;
			}
		}
		
		private void CompleteTutorial()
		{
			DebugConsole.Log("TUTORIAL COMPLETE");
			OnComplete();
		}
		
		public DisplayObject View
		{
			get{ return view; }
		}
		
		public void OnComplete()
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
		}
	}
}

