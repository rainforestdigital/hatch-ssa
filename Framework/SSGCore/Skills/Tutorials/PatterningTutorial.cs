using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;
using pumpkin.geom;

namespace SSGCore
{
	public class PatterningTutorial : Tutorial
	{
		private DisplayObjectContainer view;
		public DisplayObject View
		{
			get{ return view; }
		}
		
		private MovieClip tutorial_mc;
		public PatterningBead bead;
		
		public PatterningTutorial ()
		{
			Init(1f);
		}
		public PatterningTutorial (float scaleTo)
		{
			Init(scaleTo);
		}
		
		private void Init (float scaleTo)
		{
			view = new DisplayObjectContainer();
			
			tutorial_mc = MovieClipFactory.CreatePatterningTutorial();
			view.addChild(tutorial_mc);
			tutorial_mc.gotoAndStop(1);
			
			MovieClip temp = tutorial_mc.getChildByName<MovieClip>("tutObj");
			temp.gotoAndStop("TOMTOM");
			temp.removeAllChildren();
			
			bead = new PatterningBead("TOMTOM", SkillLevel.Tutorial);
			bead.scaleX = bead.scaleY = scaleTo;
			temp.addChild(bead);
			
			Resize(Screen.width, Screen.height);
			
			tutorial_mc.addFrameScript(12, click);
			tutorial_mc.addFrameScript(22, ding);
			tutorial_mc.addFrameScript(50, tomtom);
			tutorial_mc.addFrameScript(185, complete);
		}
		
		public void OnStart()
		{
			tutorial_mc.gotoAndPlay(1);
			view.dispatchEvent( new TutorialEvent(TutorialEvent.START, true, false) );
		}
			
		private void click( CEvent e )
		{
			SoundUtils.PlaySimpleEffect("click");
		}
		
		private void ding( CEvent e )
		{
			SoundUtils.PlaySimpleEffect("ding");
		}
		
		private void tomtom( CEvent e )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;
			clip = SoundUtils.LoadGlobalSound(MovieClipFactory.PATTERNING, "5C-Instrument Sounds/5C-tomtom-sound");
			bundle.AddClip(clip);
			clip = SoundUtils.LoadGlobalSound(MovieClipFactory.PATTERNING, "5C-Reinforcement Audio/5C-Was Next/5C-the-tomtom-was-next");
			bundle.AddClip(clip);
			SoundEngine.Instance.PlayBundle(bundle);
			
			bead.glowOn();
		}
		
		private void complete( CEvent e )
		{
			bead.glowOff();
			bead.Destroy();
			tutorial_mc.stop ();
			
			OnComplete();
		}
		
		public void OnComplete()
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE, true, false) );
		}
		
		public void Resize( int width, int height )
		{
			Rect targ;
			if( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA )
				targ = new Rect( -289.45f, 166.05f, 2048f, 1536f );
			else
			{
				targ = new Rect( -797.55f, -347.95f, 1024f, 768f );
				bead.scaleX = bead.scaleY = bead.scaleY/2f;
			}
			
			view.scaleX = view.scaleY = (float)width / (float)targ.width;
			view.y = ((height - (targ.height * view.scaleY)) / 2f);
			view.x = 0;
			
			tutorial_mc.x = targ.x;
			tutorial_mc.y = targ.y;
		}
	}
}

