using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;
using pumpkin.text;

namespace SSGCore
{

	public class SortingTutorial : Tutorial
	{
	
		private DisplayObjectContainer view = new DisplayObjectContainer();
		public const string HERES_HOW = "HERES_HOW";
		private MovieClip tutorial_mc;
		
		private int waitForFrames = 0;
		
		private MovieClip obj_1;
		private MovieClip obj_2;

		public SortingTutorial()
		{		
			tutorial_mc = MovieClipFactory.CreateSortingTutorial();
			UnityEngine.Debug.Log("sorting tutorial: " + tutorial_mc);
			tutorial_mc.gotoAndStop(2);
			tutorial_mc.width = MainUI.STAGE_WIDTH;
			tutorial_mc.height = MainUI.STAGE_HEIGHT;
			view.addChild( tutorial_mc );
			
			view.y += 40;

			obj_1 = tutorial_mc.getChildByName<MovieClip>("object1_mc");	
			obj_2 = tutorial_mc.getChildByName<MovieClip>("object2_mc");
			
			obj_1.gotoAndStop("CIRCLE_1");
			obj_2.gotoAndStop("TRIANGLE_2");

			tutorial_mc.addFrameScript( 132, triangleHouse );
			tutorial_mc.addFrameScript( 217, circleHouse );
			tutorial_mc.addFrameScript( 307, camiDragCircle );
			tutorial_mc.addFrameScript( 310, waitHere );
			tutorial_mc.addFrameScript( 400, clickSound );
			tutorial_mc.addFrameScript( 427, ding );
			tutorial_mc.addFrameScript( 457, camiHelped );
		}
		
		public void OnStart()
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.START) );
			tutorial_mc.gotoAndPlay(130);
			
//			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
//			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SORTING, "Narration/5B-Skill Instruction Audio/5B-heres-how-to-play-the-game" );
//			bundle.AddClip(clip, "TUT_INTRO", clip.length);
//			SoundEngine.Instance.PlayBundle(bundle);
		}
				
		private void triangleHouse( CEvent e )
		{
			//glow triangle house
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SORTING, "Narration/5B-Skill Instruction Audio/5B-This House/5B-This-triangle-house" );
			bundle.AddClip(clip, Sorting.HOUSE_IS1, clip.length);
			SoundEngine.Instance.PlayBundle(bundle);
			tutorial_mc.stop();
		}
		
		public void gotoCircleHouse()
		{
			tutorial_mc.gotoAndPlay(215);
		}
		
		private void circleHouse( CEvent e )
		{
			//glow circle house
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SORTING, "Narration/5B-Skill Instruction Audio/5B-This House/5B-This-circle-house" );
			bundle.AddClip(clip, Sorting.HOUSE_IS2, clip.length);
			SoundEngine.Instance.PlayBundle(bundle);
			tutorial_mc.stop();
		}
		
		public void goToDragCircle( bool isSpn )
		{
			if( isSpn )
				waitForFrames = 60;
			
			tutorial_mc.gotoAndPlay(305);
		}
		
		private void clickSound( CEvent e )
		{
			SoundUtils.PlaySimpleEffect("click");
		}
		
		private void camiDragCircle( CEvent e )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SORTING, "Narration/5B-Skill Instruction Audio/5B-Watch-Platty" );
			bundle.AddClip(clip);
			SoundEngine.Instance.PlayBundle(bundle);
		}
		
		private void waitHere( CEvent e )
		{
			waitForFrames--;
			
			if( waitForFrames > 0 )
				tutorial_mc.gotoAndPlay( 309 );
		}
		
		private void ding( CEvent e )
		{
			var clip = SoundUtils.LoadGlobalSound("ding");
			var bundle = new SoundEngine.SoundBundle();
			bundle.AddClip(clip);
			SoundEngine.Instance.PlayBundle(bundle);
		}
		
		private void camiHelped( CEvent e )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SORTING, "Narration/5B-Skill Instruction Audio/5B-now-its-your-turn" );
			bundle.AddClip(clip, "NOW_ITS", clip.length);
			SoundEngine.Instance.PlayBundle(bundle);
			
			OnComplete();
		}
		
		private void tutorialComplete( CEvent e )
		{
			OnComplete();
		}
		
		public void OnComplete()
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
		}
		
		public void Resize( int width, int height )
		{
			Platforms selectedPlatform = PlatformUtils.GetPlatform ();
			view.width = width;
			view.height = height;
			//tutorial_mc.FitToParent();

			float scaleResize = 0.8f;
			float positionModifier_X = 1.5f;
			float positionModifier_Y = positionModifier_X;

			if (selectedPlatform == Platforms.ANDROID || selectedPlatform == Platforms.IOS) {
				scaleResize = 1.47f;
				positionModifier_X = 1.75f;
				positionModifier_Y = 0.92f;
			}

			tutorial_mc.scaleY = (height/tutorial_mc.height) * scaleResize;
			tutorial_mc.scaleX = tutorial_mc.scaleY;
			tutorial_mc.y = (height - (tutorial_mc.height*tutorial_mc.scaleY))/positionModifier_Y;
			tutorial_mc.x = (width - (tutorial_mc.width*tutorial_mc.scaleX))/positionModifier_X;

		}
		
		public DisplayObject View
		{
			get{ return view; }
		}
		
	}
	
}