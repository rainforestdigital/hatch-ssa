using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;
using pumpkin.text;

namespace SSGCore
{

	public class LetterRecognitionTutorial : Tutorial
	{
	
		private DisplayObjectContainer view = new DisplayObjectContainer();
		
		private MovieClip tutorial_mc;
		
		private MovieClip card_1;
		private MovieClip card_2;
		
		private float scaleMod;
		
		private bool stalled;
		
		public LetterRecognitionTutorial()
		{
			
			tutorial_mc = MovieClipFactory.CreateLetterRecognitionTutorial();
			tutorial_mc.gotoAndStop(1);
			tutorial_mc.width = MainUI.STAGE_WIDTH;
			tutorial_mc.height = MainUI.STAGE_HEIGHT;
			view.addChild( tutorial_mc );
			
			card_1 = tutorial_mc.getChildByName<MovieClip>("card1_mc");
			card_1.getChildByName<MovieClip>("card_gr").gotoAndStop("A");
			card_2 = tutorial_mc.getChildByName<MovieClip>("card2_mc");
			card_2.getChildByName<MovieClip>("card_gr").gotoAndStop("o");
			
			scaleMod = 365f / card_2.width;

			tutorial_mc.addFrameScript( 124, onPlattieFound );
			tutorial_mc.addFrameScript( 324, clickSound );
			tutorial_mc.addFrameScript( 326, ding );
			tutorial_mc.addFrameScript( 356, onPlattieCorrect );
			tutorial_mc.addFrameScript( 425, stall);
			tutorial_mc.addFrameScript( 493, tutorialComplete );



			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;

		}
		
		public void OnStart()
		{
			stalled  = false;
			
			view.dispatchEvent( new TutorialEvent(TutorialEvent.START) );
			tutorial_mc.gotoAndPlay(2);
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LETTER_RECOGNITION, "4A-watch-closely-cami" );
			bundle.AddClip(clip, "CAMI_CLICK", clip.length);
			SoundEngine.Instance.PlayBundle(bundle);
		}

		public void OnAudioWordComplete(string word)
		{
			if (word == "CAMI_CLICK") {
				tutorial_mc.gotoAndPlay (279);
			} else if (word == "4A-Letter-O") {
				tutorial_mc.gotoAndPlay (490);
			}
		}
		
		public void OnComplete()
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
		}
		
		private void tutorialComplete(CEvent e)
		{
			tutorial_mc.stop();
			OnComplete();
		}
		
		private void onPlattieFound( CEvent e )
		{
			/*
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LETTER_RECOGNITION, "" );
			bundle.AddClip(clip);
			SoundEngine.Instance.PlayBundle(bundle);*/
		}
		
		private void stall( CEvent e )
		{

			if(!stalled)
			{
				tutorial_mc.gotoAndPlay(tutorial_mc.currentFrame - 30);
				stalled = true;
			}
		}
		
		private void clickSound( CEvent e )
		{
			SoundUtils.PlaySimpleEffect("click");
		}
		
		private void ding( CEvent e )
		{
			SoundUtils.PlaySimpleEffect("ding");
		}

		private void onPlattieCorrect( CEvent e )
		{
			tutorial_mc.stop ();
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.LETTER_RECOGNITION, "4A-Letter-O" );
			bundle.AddClip(clip, "4A-Letter-O", clip.length);
			SoundEngine.Instance.PlayBundle(bundle);
		}
				
		public void Resize( int width, int height )
		{
			view.width = width;
			view.height = height;
			//tutorial_mc.FitToParent();
			//tutorial_mc.width = view.width;
			//tutorial_mc.height = view.height;
			tutorial_mc.scaleY = tutorial_mc.scaleX = (width / tutorial_mc.width) * scaleMod;
			//tutorial_mc.scaleY = tutorial_mc.scaleX = 0.5f;
			tutorial_mc.x = (width - (tutorial_mc.width*tutorial_mc.scaleX))/2;
			tutorial_mc.y = (height - (tutorial_mc.height*tutorial_mc.scaleY))/2 - (0.05f * height);
		}
		
		public DisplayObject View
		{
			get{ return view; }
		}

		public void Destroy() {
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
		}
		
	}
	
}