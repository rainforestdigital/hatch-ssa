using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;
using pumpkin.geom;

namespace SSGCore
{
	public class SequenceCountingTutorial : Tutorial
	{
		private const string INTRO_CONTUNUE = "The tutorial is playing its intro.";
		private const string TUT_WORD = "The tutoral is speaking, be quiet.";

		public float baseScale = 1f;
		public SequenceTarget target;
		public DisplayObjectContainer container;

		private DisplayObjectContainer view;
		public DisplayObject View
		{
			get{ return view; }
		}
		
		private MovieClip tutorial_mc;
		public List<SequenceCounter> counters;
		private int count;
	//	private bool isLowRes = false;
		
		public SequenceCountingTutorial ()
		{
			view = new DisplayObjectContainer();
			view.width = MainUI.STAGE_WIDTH;
			view.height = MainUI.STAGE_HEIGHT;
			
			tutorial_mc = MovieClipFactory.CreateSequenceCountingTutorial();
			tutorial_mc.stop();

			tutorial_mc.addFrameScript(110, introContinue);//120 - batteries-and-wind-turbines-give
			tutorial_mc.addFrameScript(328, henryThinks);//260 - henry-sees-1-2-3
			tutorial_mc.addFrameScript(360, oneOn);//314 - (#1 glows)
			tutorial_mc.addFrameScript(390, twoOn);//343 - (#2 glows)
			tutorial_mc.addFrameScript(425, threeOn);//380 - (#3 glows)
			tutorial_mc.addFrameScript(447, threeOff);//410 - (glow off)
			tutorial_mc.addFrameScript(497, clickIt);
			tutorial_mc.addFrameScript(501, dingIt);
			tutorial_mc.addFrameScript(920, completed);
		}
		
		public void OnStart()
		{
			SequenceCounter sc = counters [counters.Count - 2];
			tutorial_mc.x = sc.x + sc.width * 0.5f;;
			tutorial_mc.y = sc.y + sc.height * 0.5f;
			view.addChild(tutorial_mc);

			tutorial_mc.gotoAndPlay(105);
			view.dispatchEvent( new TutorialEvent(TutorialEvent.START, true, false) );
			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioComplete;
			
			//playSound("Instructions/heres-how-to-play-the-game-listen-carefully");
		}
		
		private void introContinue(CEvent e)
		{
			playSound("Instructions/batteries-and-wind-turbines-give", INTRO_CONTUNUE);
			
			tutorial_mc.gotoAndStop(323);
		}
		
		private void henryThinks(CEvent e)
		{
			//playSound ("Instructions/henry-sees-1-2-3");
			playSound("Instructions/2C-Henry-sees-1-2-3/2C-1-Henry-sees");
		}
		
		private void oneOn(CEvent e)
		{
			glowOn(counters[0]);
			playSound("Instructions/2C-Henry-sees-1-2-3/2C-2-one");
		}
		
		private void twoOn(CEvent e)
		{
			glowOff(counters[0]);
			glowOn(counters[1]);
			playSound("Instructions/2C-Henry-sees-1-2-3/2C-3-two");
		}
		
		private void threeOn(CEvent e)
		{
			glowOff(counters[1]);
			glowOn(counters[2]);
			playSound("Instructions/2C-Henry-sees-1-2-3/2C-4-three");
		}
		
		private void threeOff(CEvent e)
		{
			glowOff(counters[2]);
			tutorial_mc.gotoAndStop( tutorial_mc.currentFrame );
			TimerUtils.SetTimeout( 5f, () => { if( counters != null ) tutorial_mc.gotoAndPlay( tutorial_mc.currentFrame ); } );
			TimerUtils.SetTimeout (2f, () => { playSound ("Instructions/2C-Henry-sees-1-2-3/2C-6-WatchClosely"); });
			playSound("Instructions/2C-Henry-sees-1-2-3/2C-5-Turbines");
		}
		
		private void clickIt(CEvent e)
		{
			SoundUtils.PlaySimpleEffect("click");
		}
		
		private void dingIt(CEvent e)
		{
			target.fillDummy (counters [counters.Count - 2]);
			var clip = SoundUtils.LoadGlobalSound("ding");
			var bundle = new SoundEngine.SoundBundle();
			bundle.AddClip(clip, TUT_WORD, clip.length, false);
			SoundEngine.Instance.PlayBundle(bundle);
			
			count = 0;
		}
		
		public void OnAudioComplete(string word)
		{
			if(word == INTRO_CONTUNUE)
			{
				tutorial_mc.play();
			}
			if(word == TUT_WORD)
			{
				if(count > 0 && count < 5)
					glowOn(counters[count - 1]);
				if(count > 1 && count < 6)
					glowOff(counters[count - 2]);
				switch(count)
				{
				case 0:
				case 4:
					playSound("Numerals/4", TUT_WORD);
					break;
				case 1:
				case 2:
				case 3:
					playSound("Counting Numerals/" + count, TUT_WORD);
					break;
				case 5:
					playSound("Reinforcement/turbines-are-spinning", TUT_WORD);
					break;
				default:
					tutorial_mc.gotoAndPlay(919);
					SoundEngine.Instance.OnAudioWordEvent -= OnAudioComplete;
					break;
				}
				count++;
			}
		}
		
		private void completed(CEvent e)
		{
			OnComplete();
		}
		
		public void OnComplete()
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
			tutorial_mc.stop();
		}
		
		private void glowOn(FilterMovieClip fmc)
		{
			while(fmc.filters.Count > 0)
			{
				fmc.filters[0].Destroy();
				fmc.filters[0].Visible = false;
				fmc.filters.RemoveAt(0);
			}
			if(fmc.filters.Count == 0)
				fmc.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
			fmc.filters[0].Visible = true;
		}
		
		private void glowOff(FilterMovieClip fmc)
		{
			while(fmc.filters.Count > 0)
			{
				fmc.filters[0].Destroy();
				fmc.filters[0].Visible = false;
				fmc.filters.RemoveAt(0);
			}
		}
		
		private void playSound ( string clipName ){ playSound(clipName, ""); }
		private void playSound ( string clipName, string word )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();;
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEQUENCE_COUNTING, clipName );
			
			if(word != "")
				bundle.AddClip(clip, word, clip.length);
			else
				bundle.AddClip(clip);
			
			SoundEngine.Instance.PlayBundle(bundle);
		}
		
		public void Resize( int wide, int high )
		{
			view.scaleY = view.scaleX = wide/2048f;
			view.x = 0;
			view.y = (high - (1536f * view.scaleY))/2;
		}
		
		public void Destroy ()
		{
			if( counters != null )
			{
				foreach (SequenceCounter c in counters) {
					//c.parent.removeChild (c);
					c.Destroy ();
				}
			}
			counters = null;
			target = null;
			container = null;
			counters = null;
		}
	}
}

