using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;
using pumpkin.text;

namespace SSGCore
{

	public class CommonShapesTutorial : Tutorial
	{
		private const string TUTORIAL_READY = "CommonShapesTutorial_READY";
		private const string TUTORIAL_DONE = "CommonShapesTutorial_DONE";
		private DisplayObjectContainer view = new DisplayObjectContainer();
		
		private MovieClip tutorial_mc;
		
		public FilterMovieClip circle_mc;
		private MovieClip triangle_mc;
		private DisplayObjectContainer behind = new DisplayObjectContainer();
		
		private float baseScale = 1;
		
		public CommonShapesTutorial()
		{
			
			behind.width = MainUI.STAGE_WIDTH;
			behind.height = MainUI.STAGE_HEIGHT;
			view.addChild(behind);
			
			tutorial_mc = MovieClipFactory.CreateCommonShapesTutorial();
			tutorial_mc.gotoAndStop(1);
			tutorial_mc.width = MainUI.STAGE_WIDTH;
			tutorial_mc.height = MainUI.STAGE_HEIGHT;
			view.addChild( tutorial_mc );
			
			//var green = new Color32(0x39, 0xb4, 0x4a, 0xFF);
			var circle = tutorial_mc.getChildByName<MovieClip>("circle_mc");
			circle.stop();
			//circle.colorTransform = Color.clear;
			circle_mc = new FilterMovieClip(MovieClipFactory.CreateCommonShapesObject("CIRCLE"));
			//circle_mc.x = circle.x;
			//circle_mc.y = circle.y;
			//circle_mc.scaleX = circle_mc.scaleY = circle.scaleX;
			//circle_mc.Target.colorTransform = green;
			circle_mc.Target.gotoAndStop("GREEN");
			//behind.addChild(circle_mc);
			circle_mc.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
			circle_mc.filters[0].Visible = false;
			circle.addChildAt(circle_mc, 0);
			for(int i = circle.numChildren - 1; i > 0; i--)
			{
				circle.removeChildAt(i);
			}
			
			triangle_mc = tutorial_mc.getChildByName<MovieClip>("triangle_mc");
			//triangle_mc.colorTransform = green;
			triangle_mc.gotoAndStop("GREEN");
			
			baseScale = 351.6f / triangle_mc.width;
			
			tutorial_mc.addFrameScript( 558, highlightObject );
			tutorial_mc.addFrameScript( 589, plattieFound );
			tutorial_mc.addFrameScript( 609, tutorialComplete );
			
			tutorial_mc.visible = false;
			tutorial_mc.alpha = 0;
		}
		
		public void OnStart()
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.START) );
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
//			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.COMMON_SHAPES, "Skill_Instructions/5A-heres-how-to-play-the-game" );
//			bundle.AddClip(clip);
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.COMMON_SHAPES, "Skill_Instructions/5A-Tutorial-Intro-Henry" );
			bundle.AddClip(clip, TUTORIAL_READY, clip.length);
			SoundEngine.Instance.PlayBundle(bundle);
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordEvent;
			
			tutorial_mc.visible = true;
			Tweener.addTween(tutorial_mc, Tweener.Hash("time",0.5f, "alpha",1, "transition",Tweener.TransitionType.easeInOutQuad) );
		}

		void OnAudioWordEvent (string eventID)
		{
			if(eventID == TUTORIAL_READY) {
				tutorial_mc.gotoAndPlay(491);
				tutorial_mc.addEventListener( CEvent.ENTER_FRAME, skipHalf );
				SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordEvent;
			}
			else if(eventID == TUTORIAL_DONE) {
				SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordEvent;
				OnComplete();
			}
		}
		
		private void skipHalf( CEvent e )
		{
			if(tutorial_mc.currentFrame % 2 == 0)
				tutorial_mc.gotoAndPlay(tutorial_mc.currentFrame + 1);
			if(tutorial_mc.currentFrame == 589)
				tutorial_mc.removeEventListener( CEvent.ENTER_FRAME, skipHalf );
		}
					
		private void plattieFound( CEvent e )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.COMMON_SHAPES, "Reinforcement/5A-Character-Reinforcement" );
			bundle.AddClip(clip, TUTORIAL_DONE, clip.length);
			SoundEngine.Instance.PlayBundle(bundle);
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordEvent;
		}
		
		private void highlightObject( CEvent e )
		{
			SoundUtils.PlaySimpleEffect("click");
			if(circle_mc.filters.Count > 0)
				circle_mc.filters[0].Visible = true;
			else
				circle_mc.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
			circle_mc.scaleX = circle_mc.scaleY *= 4f/3f;
			SoundUtils.PlaySimpleEffect("ding");
		}
		
		private void tutorialComplete( CEvent e )
		{
			tutorial_mc.stop();
		}
		
		public void OnComplete()
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
		}
		
		public void Resize( int width, int height )
		{
			view.scaleY = view.scaleX = ((float)height / (float)MainUI.STAGE_HEIGHT);
			view.y = 0;
			view.x = (width - (MainUI.STAGE_WIDTH * view.scaleX))/2;
			
			behind.x = tutorial_mc.x = (MainUI.STAGE_WIDTH * 1.05f)/2;
			behind.y = tutorial_mc.y = MainUI.STAGE_HEIGHT/2;
			tutorial_mc.scaleX = behind.scaleX = tutorial_mc.scaleY = behind.scaleY = baseScale;
		}
		
		public DisplayObject View
		{
			get{ return view; }
		}
		
	}

}