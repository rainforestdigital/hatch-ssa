using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;
using pumpkin.text;
using pumpkin.geom;

namespace SSGCore
{

	public class NumeralRecognitionTutorial : Tutorial
	{
		protected const string THEME_INTRO = "THEME_INTRO";

		private const int HAND_ANIM_START = 299;
		private const int HAND_ANIM_TOUCH = 314;
		private const int HAND_ANIM_END = 372;

		private DisplayObjectContainer view = new DisplayObjectContainer();
		
		private MovieClip tutorial_mc;
		
		private MovieClip card_1;
		private MovieClip card_2;
		private FilterMovieClip card_2_filter;
		
		private float baseScale;
		private NumeralRecognition main;

		public NumeralRecognitionTutorial(NumeralRecognition main)
		{
			this.main = main;

			tutorial_mc = MovieClipFactory.CreateNumeralRecognitionTutorial();
			tutorial_mc.gotoAndStop(1);
			tutorial_mc.width = MainUI.STAGE_WIDTH;
			tutorial_mc.height = MainUI.STAGE_HEIGHT;
			view.addChild( tutorial_mc );
			
			card_1 = tutorial_mc.getChildByName<MovieClip>("card1_mc");
			card_2 = tutorial_mc.getChildByName<MovieClip>("card2_mc");

			baseScale = 365f / card_2.width;

			init ();
		}

		private void init() {	
			card_1.gotoAndStop(2);
			card_2.gotoAndStop(4);
			tutorial_mc.gotoAndStop (1);
		}

		public void OnStart()
		{
			view.dispatchEvent (new TutorialEvent (TutorialEvent.START));
			start ();
		}

		private void start() {
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.NUMERAL_RECOGNITION, NumeralRecognitionSounds.SOUND_2 );
			bundle.AddClip(clip);
			SoundEngine.Instance.PlayBundle(bundle);

			SoundEngine.Instance.OnAudioCompleteEvent += HandleSound2CompleteEvent;
		}

		private void HandleSound2CompleteEvent ()
		{
			SoundEngine.Instance.OnAudioCompleteEvent -= HandleSound2CompleteEvent;
			tutorial_mc.gotoAndPlay (HAND_ANIM_START);
			tutorial_mc.addFrameScript( HAND_ANIM_END, onHandAnimComplete );
			tutorial_mc.addFrameScript( HAND_ANIM_TOUCH, onHandAnimTouch );
		}

		private void onHandAnimTouch( CEvent e )
		{
			SoundUtils.PlaySimpleEffect("click");
			SoundUtils.PlaySimpleEffect("ding");
		}

		private void onHandAnimComplete( CEvent e )
		{
			tutorial_mc.stop ();
			
			float delay = 0;
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.NUMERAL_RECOGNITION, "Narration/2B-Reinforcement Audio/2B-TT-that-is-the-numeral-2" );
			bundle.AddClip(clip);
			delay += clip.length;
			
			clip = SoundUtils.LoadGlobalSound( MovieClipFactory.NUMERAL_RECOGNITION, NumeralRecognitionSounds.SOUND_3 );
			bundle.AddClip(clip);
			delay += clip.length;
			
			SoundEngine.Instance.PlayBundle(bundle);
			
			main.AddNewOppData( NumeralRecognition.audioNames.IndexOf(NumeralRecognitionSounds.SOUND_3) );
			main.AddNewObjectData( main.formatObjectData( "2", card_1.parent.localToGlobal(new Vector2(card_1.x, card_1.y)), false ) );
			main.AddNewObjectData( main.formatObjectData( "4", card_2.parent.localToGlobal(new Vector2(card_2.x, card_2.y)), true ) );
			
			TimerUtils.SetTimeout( delay, HandleSound3CompleteEvent );
		}

		private void HandleSound3CompleteEvent ()
		{
			card_2.addEventListener( MouseEvent.CLICK, onCardClick );
			main.startTimer();
		}

		private void onCardClick( CEvent e )
		{
			card_2.removeEventListener( MouseEvent.CLICK, onCardClick );

			SoundEngine.Instance.OnAudioCompleteEvent -= timerPromptCompleteHandler;
			SoundEngine.Instance.OnAudioCompleteEvent -= timerPromptRestartHandler;
			main.stopTimer();

			Vector2 card2Coords = new Vector2 (card_2.x, card_2.y);
			card_2.x = 0;
			card_2.y = 0;
			card_2_filter = new FilterMovieClip (card_2);
			card_2_filter.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
			card_2_filter.x = card2Coords.x;
			card_2_filter.y = card2Coords.y;
			tutorial_mc.addChild (card_2_filter);
			card_2_filter.ScaleCentered( card_2.scaleX * (7f/6f) );
			SoundUtils.PlaySimpleEffect("click");
			SoundUtils.PlaySimpleEffect("ding");
			
			main.AddNewActionData( main.formatActionData( "tap", 1, Vector2.zero, true ) );

			Tweener.addTween(card_2_filter, Tweener.Hash("time", 0.5f, "alpha", 1)).OnComplete( delayCompleteHandler );
		}

		private void delayCompleteHandler() {
			main.setOpportunityComplete ();
			main.dispatchEvent (new SkillEvent (SkillEvent.COMPLIMENT_REQUEST, true, false));
		}

		public void OnComplimentComplete() {
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle ();
			AudioClip clip = SoundUtils.LoadGlobalSound (MovieClipFactory.NUMERAL_RECOGNITION, NumeralRecognitionSounds.SOUND_24 + "4" + NumeralRecognitionSounds.SAY + "4");
			bundle.AddClip (clip);
			SoundEngine.Instance.PlayBundle (bundle);
			view.dispatchEvent (new SkillEvent (SkillEvent.CORRECT, true, false));
			SoundEngine.Instance.OnAudioCompleteEvent += OnTouchedAudioComplete;
		}

		private void OnTouchedAudioComplete(){
			SoundEngine.Instance.OnAudioCompleteEvent -= OnTouchedAudioComplete;
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.NUMERAL_RECOGNITION, NumeralRecognitionSounds.SOUND_4 );
			bundle.AddClip(clip);
			SoundEngine.Instance.PlayBundle(bundle);
			view.dispatchEvent( new SkillEvent(SkillEvent.CORRECT, true,  false) );

			Tweener.addTween(card_2_filter, Tweener.Hash("time", 0.5f, "alpha", 0));
			SoundEngine.Instance.OnAudioCompleteEvent += tutorialCompleteHandler;
		}
		
		private void tutorialCompleteHandler()
		{
			SoundEngine.Instance.OnAudioCompleteEvent -= tutorialCompleteHandler;
			OnComplete();
		}
		
		public void OnComplete()
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
		}
		
		public void Resize( int width, int height )
		{
			view.width = width;
			view.height = height;
			//tutorial_mc.FitToParent();
			//tutorial_mc.x = (width * 0.5f) - (tutorial_mc.width * 0.5f);
			//tutorial_mc.y = (height * 0.5f) - (tutorial_mc.height * 0.5f);
			tutorial_mc.scaleX = (width/tutorial_mc.width) * baseScale;
			tutorial_mc.scaleY = tutorial_mc.scaleX;
			tutorial_mc.x = (width - (MainUI.STAGE_WIDTH*tutorial_mc.scaleX))/2.0275f;
			tutorial_mc.y = (height - (MainUI.STAGE_HEIGHT*tutorial_mc.scaleY))/2.875f;
			tutorial_mc.ScaleCentered(tutorial_mc.scaleX * 1.025f);
		}
		
		public Rectangle getCardPos(DisplayObjectContainer target)
		{
			return card_2.getBounds( target );
		}
		
		public DisplayObject View
		{
			get{ return view; }
		}

		public void TimeoutHandler(int timeoutCount) {
			string clipName = "";
			if (timeoutCount == 1) {
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.NUMERAL_RECOGNITION, NumeralRecognitionSounds.SOUND_3 );
				clipName = NumeralRecognitionSounds.SOUND_3;
				bundle.AddClip(clip);
				SoundEngine.Instance.OnAudioCompleteEvent += timerPromptCompleteHandler;
				SoundEngine.Instance.PlayBundle(bundle);
			} else if(timeoutCount == 2){
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.NUMERAL_RECOGNITION, NumeralRecognitionSounds.SOUND_6 );
				clipName = NumeralRecognitionSounds.SOUND_6;
				bundle.AddClip(clip);
				SoundEngine.Instance.OnAudioCompleteEvent += timerPromptRestartHandler;
				SoundEngine.Instance.PlayBundle(bundle);
			} else {
				//main.timeoutEnd = true;
				//main.dispatchEvent( new SkillEvent(SkillEvent.TIMEOUT_END, true, false) );
				view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
			}
			
			if( !string.IsNullOrEmpty(clipName) ){
				main.baseTheme.AddAudioSessionData( "time", NumeralRecognition.audioNames.IndexOf(clipName) );
			}
		}

		private void timerPromptCompleteHandler() {
			SoundEngine.Instance.OnAudioCompleteEvent -= timerPromptCompleteHandler;
			main.startTimer ();
		}
		
		private void timerPromptRestartHandler() {
			SoundEngine.Instance.OnAudioCompleteEvent -= timerPromptRestartHandler;
			card_2.removeEventListener( MouseEvent.CLICK, onCardClick );
			init ();
			start ();
		}

		public void Destroy() {
			if( card_2_filter != null )
				card_2_filter.Destroy();
		}
	}
	
}