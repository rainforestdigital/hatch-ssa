using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;
using pumpkin.geom;

namespace SSGCore
{
	public class MeasurementTutorial : Tutorial
	{
		private DisplayObjectContainer view;
		public DisplayObject View
		{
			get{ return view; }
		}
		
		private MovieClip tutorial_mc;
		public FilterMovieClip person;
		
		public MeasurementTutorial ()
		{
			Init (1f);
		}
		
		public MeasurementTutorial ( float scaleTo )
		{
			Init(scaleTo);
		}
		
		private void Init( float scaleTo )
		{
			view = new DisplayObjectContainer();
			
			Resize(Screen.width, Screen.height);
			
			tutorial_mc = MovieClipFactory.CreateMeasurementTutorial();
			tutorial_mc.gotoAndStop(1);
			
			MovieClip tempClip = tutorial_mc.getChildByName<MovieClip>("bg");
			Debug.Log ("The tutorial exists: " + (tutorial_mc != null) + ". The tempClip exists: " + (tempClip != null));
			tempClip.gotoAndStop(1);
			tempClip.scaleX = tempClip.scaleY = scaleTo;
			
			tempClip = tutorial_mc.getChildByName<MovieClip>("person_2");
			tempClip.gotoAndStop(6);
			tempClip.scaleX = tempClip.scaleY = scaleTo;
			
			tempClip = tutorial_mc.getChildByName<MovieClip>("person_1");
			tempClip.gotoAndStop(1);
			tempClip.scaleX = tempClip.scaleY = scaleTo;
			tempClip.removeAllChildren();
			
			person = new FilterMovieClip(MovieClipFactory.CreateMeasurementPeopleSide());
			person.Target.gotoAndStop(1);
			tempClip.addChild(person);
			
			view.addChild(tutorial_mc);
			
			tutorial_mc.addFrameScript(110, henryPlay);
			tutorial_mc.addFrameScript(267, click);
			tutorial_mc.addFrameScript(269, ding);
			tutorial_mc.addFrameScript(297, henryFound);
			tutorial_mc.addFrameScript(350, speedUp);
			tutorial_mc.addFrameScript(440, done);
		}
		
		public void OnStart ()
		{
			tutorial_mc.gotoAndPlay(105);
			view.dispatchEvent( new TutorialEvent(TutorialEvent.START, true, false) );

		//	playSound("Skill Instruction/heres-how-to-play");
		}
		
		private void henryPlay (CEvent e)
		{
			playSound(MeasurementSounds.SOUND_2);
			//playSound("Skill Instruction/Henry-is-thinking");
		}
		
		private void click (CEvent e)
		{
			SoundUtils.PlaySimpleEffect("click");
		}
		
		private void ding (CEvent e)
		{
			person.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
			person.filters[0].Visible = true;
			
			SoundUtils.PlaySimpleEffect("ding");
		}
		
		private void henryFound (CEvent e)
		{
			playSound(MeasurementSounds.SOUND_15);
			//playSound("Reinforcement/Henry-found-the-shorter-person");
		}
		
		private void speedUp (CEvent e)
		{
			tutorial_mc.gotoAndPlay(tutorial_mc.currentFrame + 15);
		}
		
		private void done (CEvent e)
		{
			if(person.filters.Count > 0)
				person.filters[0].Visible = false;
			OnComplete();
			tutorial_mc.stop ();
		}
		
		public void OnComplete()
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE, true, false) );
		}
		
		public void Resize( int width, int height )
		{
			view.scaleX = view.scaleY = (height / (float)MainUI.STAGE_HEIGHT);
			view.y = 0;
			view.x = ( width - ( MainUI.STAGE_WIDTH * view.scaleX ) ) / 2f;
		}
		
		private void playSound ( string clipName ) { playSound(clipName, ""); }
		private void playSound ( string clipName, string word )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.MEASUREMENT, clipName );
			
			if(word != "")
				bundle.AddClip(clip, word, clip.length);
			else
				bundle.AddClip(clip);
			
			SoundEngine.Instance.PlayBundle(bundle);
		}
	}
}

