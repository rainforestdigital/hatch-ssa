using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;

namespace SSGCore
{

	public class InitialSoundsTutorial : Tutorial
	{
	
		private DisplayObjectContainer view;

		private DisplayObjectContainer cardContainer;

		private MovieClip tutorial_mc;
		
		private MovieClip card_1;
		private MovieClip card_2;

		private MovieClip ear_mc;

		private List<MovieClip> _cards;

		public FilterMovieClip decoy;

//		private float baseScale;

		public InitialSoundsTutorial( float baseScale )
		{

//			this.baseScale = baseScale;

			view = new DisplayObjectContainer();

			cardContainer = new DisplayObjectContainer();
			cardContainer.width = MainUI.STAGE_WIDTH;
			cardContainer.height = MainUI.STAGE_HEIGHT;

			tutorial_mc = MovieClipFactory.CreateInitialSoundsTutorial();
			tutorial_mc.width = MainUI.STAGE_WIDTH;
			tutorial_mc.height = MainUI.STAGE_HEIGHT;
			tutorial_mc.gotoAndStop(1);

			_cards = new List<MovieClip>();

			card_1 = new MovieClip( "InitialSounds.swf", "Card" );
			card_1.gotoAndStop(2);
			
			card_2 = new MovieClip( "InitialSounds.swf", "Card" );
			card_2.gotoAndStop(1);

			ear_mc = new MovieClip( "InitialSounds.swf", "Ear" );
			//ear_mc.scaleX = ear_mc.scaleY = 0.85f;
			ear_mc.x = (cardContainer.width/2) - 25;// + cardContainer_mc.x;
			ear_mc.y = 372;
			ear_mc.scaleX = ear_mc.scaleY = baseScale;

			_cards.Add(card_1);
			_cards.Add(card_2);
			card_1.scaleX = card_1.scaleY = baseScale;
			card_2.scaleX = card_2.scaleY = baseScale;

			view.addChild( cardContainer );
			cardContainer.addChild( ear_mc );
			cardContainer.addChild( card_1 );
			cardContainer.addChild( card_2 );

			view.addChild( tutorial_mc );

			updateCards(0);
			updateCards(1);

			tutorial_mc.addFrameScript(113, skipFrames);
			tutorial_mc.addFrameScript(124, introContinue);
			tutorial_mc.addFrameScript(274, introWatch);
			tutorial_mc.addFrameScript(444, skipThird);
			tutorial_mc.addFrameScript(464, cardGrab);
			tutorial_mc.addFrameScript(487, ding);
			tutorial_mc.addFrameScript(517, sayWord);
			tutorial_mc.addFrameScript(625, completeTutorial);



		}

		/// <summary>
		/// Updates the card positions.
		/// </summary>
		private void updateCards( int i )
		{
			
			int padding = 20;
			
			float cW = 257f + padding;
			
			float totalWidth = (_cards.Count * cW) - padding;
			//DebugConsole.Log("cardwidth: " + cardwidth);
			float startX = (cardContainer.width * 0.5f) - (totalWidth * 0.5f);
			float cardY = 430 + 151;
			
			for( i = 0; i < _cards.Count; i++)
			{
				_cards[i].x = startX + (i * cW);
				_cards[i].y = cardY;
			}
		}

		public DisplayObject View{ get{ return view; } }
		public void OnStart()
		{
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.INITIAL_SOUNDS, "1B-Heres-how-to-play-the-game" );
			bundle.AddClip( clip );
			SoundEngine.Instance.PlayBundle( bundle );
			tutorial_mc.gotoAndPlay(1);
			card_1.gotoAndStop(2);
			card_2.gotoAndStop(1);
			view.dispatchEvent( new TutorialEvent(TutorialEvent.START) );
		}
		
		private void skipFrames( CEvent e )
		{
			tutorial_mc.gotoAndPlay(tutorial_mc.currentFrame + 10);
		}
		
		private void introContinue( CEvent e)
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.INITIAL_SOUNDS, "1B-Tutorial-Instruction-Henry" );
			bundle.AddClip( clip );
			SoundEngine.Instance.PlayBundle( bundle );
		}
		
		private void introWatch(CEvent e)
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.INITIAL_SOUNDS, "1B-Instruction-Him" );
			bundle.AddClip( clip );
			SoundEngine.Instance.PlayBundle( bundle );
		}
		
		private void skipThird( CEvent e )
		{
			if(tutorial_mc.currentFrame % 3 == 0)
				tutorial_mc.gotoAndPlay(tutorial_mc.currentFrame + 1);
			if(!tutorial_mc.hasEventListener(CEvent.ENTER_FRAME))
				tutorial_mc.addEventListener(CEvent.ENTER_FRAME, skipThird);
			else if(tutorial_mc.currentFrame == 499)
				tutorial_mc.removeEventListener(CEvent.ENTER_FRAME, skipThird);
		}
		
		private void cardGrab(CEvent e)
		{
			decoy = new FilterMovieClip( MovieClipFactory.CreateInitialSoundsCard() );
			decoy.Target.gotoAndStop("BALL");
			card_2.addChild(decoy);
			decoy.AddFilter(FiltersFactory.GetYellowGlowFilter());
			decoy.filters[0].GetSprite().ScaleCentered(1.05f);
			decoy.filters[0].Visible = true;

			Tweener.addTween( card_2, Tweener.Hash( "x", ear_mc.x - (ear_mc.width/2), "y", ear_mc.y- (ear_mc.height/2), "time", 0.2f ) );

			SoundUtils.PlaySimpleSound("click");
		}
			
		private void ding(CEvent e)
		{
			SoundUtils.PlaySimpleEffect("ding");
			//var clip = SoundUtils.LoadGlobalSound("ding");
			//var bundle = new SoundEngine.SoundBundle();
			//bundle.AddClip(clip, "TUT_DING", clip.length, false);
			//SoundEngine.Instance.PlayBundle(bundle);
			
			//SoundEngine.Instance.OnAudioWordEvent += onDing;
		}
		
		private void onDing(string aWord)
		{
			
			tutorial_mc.play();
			SoundEngine.Instance.OnAudioWordEvent -= onDing;
		}
		
		private void sayWord(CEvent e)
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.INITIAL_SOUNDS, "1B-ball-begins-with-b" );
			bundle.AddClip( clip );
			SoundEngine.Instance.PlayBundle( bundle );
			
			//tutorial_mc.stop();
		}
		
		private void completeTutorial(CEvent e)
		{
			tutorial_mc.stop();
			OnComplete();
		}
		
		public void OnComplete()
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
		}
		
		public void Resize( int width, int height )
		{
			
			view.width = width;
			view.height = height;
			
			//tutorial_mc.FitToParent();
			tutorial_mc.scaleY = height/(tutorial_mc.height);
			tutorial_mc.scaleX = tutorial_mc.scaleY;
			tutorial_mc.y = 0;
			tutorial_mc.x = (width - (tutorial_mc.width*tutorial_mc.scaleX))/2;

			cardContainer.scaleY = height/(cardContainer.height);
			cardContainer.scaleX = cardContainer.scaleY;
			cardContainer.y = 0;
			cardContainer.x = (width - (cardContainer.width*cardContainer.scaleX))/2;

		}
		
	}
	
}