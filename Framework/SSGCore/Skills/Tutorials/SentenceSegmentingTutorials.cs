using UnityEngine;
using System.Collections;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;

namespace SSGCore
{
	
	public class SentenceSegmentingTutorials : Tutorial
	{
		
		private const float SWIM_GLOW_AUDIO_TIME = .625f;
	
		private DisplayObjectContainer view = new DisplayObjectContainer();
		
		public MovieClip tutorial_mc;
		public FilterMovieClip obj_1;
		public FilterMovieClip obj_2;
		public FilterMovieClip obj_3;
		public TimerObject gtime;

		public SentenceSegmenting skill;

		private TimerObject highlightTimer;

		public SentenceSegmentingTutorials(SentenceSegmenting skillRef)
		{
			Debug.Log ("SKILL REF NULL? " + skillRef == null);
			skill = skillRef;
		}
		
		public void OnStart()
		{
			tutorial_mc = MovieClipFactory.CreateSentenceSegmentingTutorial();
			tutorial_mc.gotoAndStop(2);
			view.addChild( tutorial_mc );
			
			obj_1 = MovieClipFactory.CreateSentenceSegmentingCounter();
			obj_2 = MovieClipFactory.CreateSentenceSegmentingCounter();
			obj_3 = MovieClipFactory.CreateSentenceSegmentingCounter();
				
			obj_1.Target.gotoAndStop("BEAR_BLUE");
			obj_2.Target.gotoAndStop("BEAR_BLUE");
			obj_3.Target.gotoAndStop("BEAR_BLUE");
			
			skill.counterContainer.addChild(obj_1);
			skill.counterContainer.addChild(obj_2);
			skill.counterContainer.addChild(obj_3);
			
			float scale;
				
			if( PlatformUtils.GetClosestRatio() == ScreenRatio.FOURxTHREE)
			{
				if(Screen.width > 1024)
					scale = 1.4f;
				else
					scale = .95f;
			}
			else
				scale = 1.0f;
			
			obj_1.scaleX = obj_1.scaleY = obj_2.scaleX = obj_2.scaleY = obj_3.scaleX = obj_3.scaleY = scale;
			
			obj_1.x = (skill.adjustedContainerLineBase.x);
			obj_1.y = skill.adjustedContainerLineBase.y;
			
			obj_2.x = (skill.adjustedContainerLineBase.x) + (obj_1.width + SentenceSegmenting.COUNTER_SPACING);
			obj_2.y = skill.adjustedContainerLineBase.y;
			
			obj_3.x = (skill.adjustedContainerLineBase.x) + (obj_1.width + SentenceSegmenting.COUNTER_SPACING) * 2f;
			obj_3.y = skill.adjustedContainerLineBase.y;

			tutorial_mc.x = skill.adjustedContainerLineBase.x;
			tutorial_mc.y = skill.adjustedContainerLineBase.y;
			tutorial_mc.gotoAndStop(1);
			tutorial_mc.alpha = 0;
			
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;

			tutorial_mc.addFrameScript(26, HandleTouchCount);
			tutorial_mc.addFrameScript(34, StopTutorialAnimation);	

			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			/*
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, "Narration/1A-Skill Instruction Audio/1A-heres-how-to-play-the-game" );
			bundle.AddClip(clip, "HOW_TO_PLAY", clip.length);
			*/
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_2 );
			bundle.AddClip(clip, "IS_TOUCHING", clip.length);
			
			SoundEngine.Instance.PlayBundle(bundle);
			skill.RemoveListenerPageFlipSubmit();
			skill.canPlayerInteract = false;

			fishGlow ();
			highlightTimer = TimerUtils.SetTimeout( 1f, swimHighlightHandler );
		}

		private void swimHighlightHandler() {
			swimGlow (obj_2);
			highlightTimer = TimerUtils.SetTimeout( 2f, stopCounterGlow );
		}

		private void glowCounters() 
		{
			fishGlow ();
			swimGlow (obj_2);
			swimGlow (obj_3);
		}

		private void fishGlow()
		{
			//Apply glow to bears but hide it for now
			obj_1.AddFilter( FiltersFactory.GetScaledYellowGlowFilter());
			
			obj_1.filters[0].Visible = true;
			
			//remove and add FilterMovieClips
			skill.counterContainer.removeChild(obj_1.Target);
			skill.counterContainer.addChild(obj_1);
						
		}
		
		private void swimGlow(FilterMovieClip obj)
		{
			//Apply glow to bears but hide it for now
			obj.AddFilter( FiltersFactory.GetScaledYellowGlowFilter());
			
			obj.filters[0].Visible = true;
			
			//remove and add FilterMovieClips
			skill.counterContainer.removeChild(obj.Target);
			skill.counterContainer.addChild(obj);
		}
		
		private void stopCounterGlow()
		{
			if(obj_1.filters.Count > 0) obj_1.filters[0].Visible = false;
			if(obj_2.filters.Count > 0) obj_2.filters[0].Visible = false;
			if(obj_3.filters.Count > 0) obj_3.filters[0].Visible = false;
		}
		
		private void IsTouching( CEvent e )
		{
			//Host shows the player how it's done
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_14);
			bundle.AddClip(clip);
			SoundEngine.Instance.PlayBundle(bundle);
			//Counter should glow
		}
		
		public bool HasTouched = false;
		private void HandleTouchCount(CEvent e)
		{
			HasTouched = true;
			
		//	fishGlow();
			
			Vector2 targetPos = getTargetPos ();

			Tweener.addTween(obj_1, Tweener.Hash("time", SentenceSegmenting.COUNTER_TRANSITION_TIME, "x",targetPos.x, "y", targetPos.y, "scaleX", 1f, "scaleY", 1f, "transition", Tweener.TransitionType.easeInOutQuint));

			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;
		
			clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_14);
			bundle.AddClip(clip, "FISH", clip.length + 1.0f); //adding a little buffer
			SoundEngine.Instance.PlayBundle(bundle);
			
			clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_3 );
			bundle.AddClip(clip, "YOUR_TURN", clip.length);
			SoundEngine.Instance.PlayBundle(bundle);
			gtime = TimerUtils.SetTimeout(clip.length, StartTimerAfterSound);

			SoundUtils.PlaySimpleEffect("click");
			
			skill.AddNewOppData( SentenceSegmenting.audioNames.IndexOf(SentenceSegmentingSounds.SOUND_3) );
			
			skill.AddNewObjectData( skill.formatObjectData( "PageFlipSubmit", skill.pageFlip.target.parent.localToGlobal( new Vector2(skill.pageFlip.target.x, skill.pageFlip.target.y) ) ) );
			skill.AddNewObjectData( skill.formatObjectData( "COUNTER_PURPLE", obj_1.parent.localToGlobal( targetPos ) ) );
			skill.AddNewObjectData( skill.formatObjectData( "COUNTER_PURPLE", obj_2.parent.localToGlobal( new Vector2(obj_2.x, obj_2.y) ) ) );
			skill.AddNewObjectData( skill.formatObjectData( "COUNTER_PURPLE", obj_3.parent.localToGlobal( new Vector2(obj_3.x, obj_3.y) ) ) );
		}
		
		private void StartTimerAfterSound()
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.YOUR_TURN) );
		}
		
		public bool repeatFinalInstruction = false;
		private void HandleObj2Click(CEvent e)
		{		
			skill.KillTutorialRestartTimer ();

			//Deactivate the ability to click
			obj_2.Target.removeEventListener(MouseEvent.CLICK, HandleObj2Click);
			obj_3.Target.removeEventListener(MouseEvent.CLICK, HandleObj2Click);

			FilterMovieClip target = e.target == obj_2.Target ? obj_2 : obj_3;

			//Glow Counter
			swimGlow(target);
			
			//Move counter
			Vector2 targetPos = getTargetPos ();
			targetPos.x += skill.sourceCounterSize.x + SentenceSegmenting.COUNTER_SPACING;

			Tweener.addTween(target, Tweener.Hash("time", SentenceSegmenting.COUNTER_TRANSITION_TIME, "x",targetPos.x, "y", targetPos.y, "scaleX", 1f, "scaleY", 1f, "transition", Tweener.TransitionType.easeInOutQuint));
			
			//Activate pageflip click
			//skill.AddListenerPageFlipSubmit();
			
			// second counter clicked
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;
			clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SENTENCE_SEGMENTING, SentenceSegmentingSounds.SOUND_15);
			bundle.AddClip(clip, "SWIM", clip.length);
			SoundEngine.Instance.PlayBundle(bundle);
			
			SoundUtils.PlaySimpleEffect("click");
			
			repeatFinalInstruction = true;
			skill.canPlayerInteract = true;
			skill.AddListenerPageFlipSubmit();
			
			view.dispatchEvent( new TutorialEvent(TutorialEvent.STOP_TIMER) );
			
			skill.AddNewActionData( skill.formatActionData( "tap", target == obj_2 ? 2 : 3, Vector2.zero ) );
		}

		private Vector2 getTargetPos() 
		{
			return new Vector2 (
								skill.adjustedAnswerLineBase.x,
								skill.adjustedAnswerLineBase.y - skill.sourceCounterSize.y - 2f
			);
		}

		public void disableCounters() {
			obj_2.Target.removeEventListener(MouseEvent.CLICK, HandleObj2Click);
			obj_3.Target.removeEventListener(MouseEvent.CLICK, HandleObj2Click);
		}

		public void Restart()
		{
			HasTouched = false;
			repeatFinalInstruction = false;
			skill.canPlayerInteract = false;
			tutorial_mc.gotoAndStop(2);
			
			//Remove items from last tutoiral
			obj_1.Destroy();
			skill.counterContainer.removeChild(obj_1);
			obj_2.Destroy();
			skill.counterContainer.removeChild(obj_2);
			obj_3.Destroy();
			skill.counterContainer.removeChild(obj_3);
			
			if( gtime != null )
				gtime.StopTimer();
			
			if (highlightTimer != null) {
				highlightTimer.StopTimer();
			}

			OnStart();
		}
		
		public void OnComplete()
		{
			
		}

		public void Dispose() {
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;

			if( gtime != null )
				gtime.StopTimer();

			if (highlightTimer != null) {
				highlightTimer.StopTimer();
			}
		}

		public void Resize( int width, int height )
		{
			view.width = width;
			view.height = height;
			
//			tutorial_mc.width = width;
//			tutorial_mc.height = height;
			
//			tutorial_mc.FitToParent();
		}
		
		public DisplayObject View
		{
			get{ return view; }
		}
		
		public void tutorialComplete( CEvent e )
		{
			Debug.LogWarning("should dispatch complete event");
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			tutorial_mc.gotoAndStop(tutorial_mc.totalFrames);
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
		}
		
		//Stop the animation from looping at the end
		private void StopTutorialAnimation (CEvent e )
		{
			tutorial_mc.stop();
			Tweener.addTween(tutorial_mc, Tweener.Hash("time",1f, "y", Screen.height + tutorial_mc.height, "transition",Tweener.TransitionType.easeInOutQuad) );
		}
		
		private void OnAudioWordComplete( string word )
		{
			if(word == "IS_TOUCHING")
			{
				obj_1.Destroy();
				obj_2.Destroy();
				obj_3.Destroy();
				
				tutorial_mc.gotoAndPlay(2);
				Tweener.addTween(tutorial_mc, Tweener.Hash("time",0.5f, "alpha",1, "transition",Tweener.TransitionType.easeInOutQuad) );
			}
			
			if(word == "SWIM")
			{
				PlayFinalInstruction();
				stopCounterGlow();
			}

			if (word == "FISH") {
				stopCounterGlow();
			}

			if(word == "YOUR_TURN")
			{
				obj_2.Target.addEventListener(MouseEvent.CLICK, HandleObj2Click);
				obj_3.Target.addEventListener(MouseEvent.CLICK, HandleObj2Click);
			}
			
		}
		
		private void PlayFinalInstruction()
		{
			skill.PlayFinalInstructions("FINAL_INSTRUCTION");
		}
		
//		private void stopMovieClip( CEvent e)
//		{
//			tutorial_mc.stop();
//		}
	}

}