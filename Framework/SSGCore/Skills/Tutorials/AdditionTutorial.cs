using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;
using pumpkin.geom;

namespace SSGCore
{
	public class AdditionTutorial : Tutorial
	{

		private const string EG = "2E-EG";
		private const string DG = "2E-DG";
		private const string DDCD = "2E-DDCD";

		private DisplayObjectContainer view;
		private MovieClip tutorial_mc;
		
		private bool stopPlayToggle = false;
		
		public AdditionDragger[] bears;
		public AdditionHouse house;
		
		public AdditionTutorial ()
		{
			Init(1f);
		}
		public AdditionTutorial ( float baseScale )
		{
			Init(baseScale);
		}
		private void Init ( float houseScale )
		{
			view = new DisplayObjectContainer();
			view.width = MainUI.STAGE_WIDTH;
			view.height = MainUI.STAGE_HEIGHT;
			
			tutorial_mc = MovieClipFactory.CreateAdditionTutorial();
			tutorial_mc.gotoAndStop(1);
			//tutorial_mc.width = MainUI.STAGE_WIDTH;
			//tutorial_mc.height = MainUI.STAGE_HEIGHT;
			//tutorial_mc.scaleX = tutorial_mc.scaleY = houseScale;

			float houseX;
			
			if(PlatformUtils.GetPlatform() == Platforms.WIN)
			{
				houseX = 58;
			}
			else
			{
				houseX = 250;
			}

			house = new AdditionHouse(EG);
			house.scaleX = house.scaleY = houseScale;
			house.x = houseX;//(PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA) ? 58 : 250;
			house.y = 200;
			
			bears = new AdditionDragger[3];
			
			//bear 1,2: 9f/12f
			//bear3_mc: 5f/12f
			
			AdditionDragger tbear;
			MovieClip bear;
			for(int i = 0; i < 2; i++)
			{
				bear = tutorial_mc.getChildByName<MovieClip>("bear" + (i+1) + "_mc");
				bear.gotoAndStop(1);
				bear = bear.getChildByName<MovieClip>("bean_mc");
				bear.gotoAndStop(2);
				tbear = new AdditionDragger(EG, 9f/12f, 1f);
				bear.addChildAt(tbear, 0);
				//bear.scaleX = bear.scaleY = houseScale;
				tbear.Target.alpha = 0;
				bears[i] = tbear;
			}
			bear = tutorial_mc.getChildByName<MovieClip>("bear" + 3 + "_mc");
			bear.gotoAndStop(1);
			bear = bear.getChildByName<MovieClip>("bean_mc");
			bear.gotoAndStop(2);
			tbear = new AdditionDragger(EG, 5f/12f, 1f);
			bear.addChildAt(tbear, 0);
			tbear.Target.alpha = 0;
			bears[2] = tbear;
			
//			view.removeChild(bears[2]);
//			view.addChild(bears[2]);
			
			tutorial_mc.addFrameScript(127, tutorialContinue);
			tutorial_mc.addFrameScript(390, clickSound);
			//tutorial_mc.addFrameScript(400, plattyMoved);
			tutorial_mc.addFrameScript(434, plattyStopped);
			tutorial_mc.addFrameScript(464, doorGlowOn);
			tutorial_mc.addFrameScript(566, clickSound);
			tutorial_mc.addFrameScript(468, doorClick);
			tutorial_mc.addFrameScript(497, doorGlowOff);
			tutorial_mc.addFrameScript(500, plattyWrapUp);
			tutorial_mc.addFrameScript(542, glowAll);
			tutorial_mc.addFrameScript(572, glowAllOff);
			tutorial_mc.addFrameScript(605, glowOne);
			tutorial_mc.addFrameScript(667, glowOneOff);
			tutorial_mc.addFrameScript(722, glowOthers);
			tutorial_mc.addFrameScript(800, glowOthersOff);
			tutorial_mc.addFrameScript(850, tutorialComplete);
//			tutorial_mc.addEventListener(CEvent.ENTER_FRAME, eachFrame);
		}
		
		public void OnStart ()
		{
			//house.x *= tutorial_mc.scaleX;
			//house.y *= tutorial_mc.scaleY;
			//house.scaleX = house.scaleY *= tutorial_mc.scaleY;
			
			view.dispatchEvent( new TutorialEvent(TutorialEvent.START) );
			
			house.alpha = 0;
			Tweener.addTween( house, Tweener.Hash("time", 0.35f, "alpha", 1f) );
			view.addChildAt(house, 0);
			
			view.addChild(tutorial_mc);
			tutorial_mc.looping = false;
			
			tutorial_mc.play();

			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			playSound( "2E-Skill Instruction Audio/2E-these-plants-need-to-be-planted", "PLANTS");
		}
		
		private void eachFrame( CEvent e )
		{
//			AdditionDragger d;
//			MovieClip bear;
//			Rectangle box;
//			for(int i = 0; i < 3; i++)
//			{
//				d = bears[i];
//				bear = tutorial_mc.getChildByName<MovieClip>("bear" + (i+1) + "_mc");
//				box = bear.getBounds(view);
//				d.x = box.x + (box.width/2);
//				d.y = box.y + (box.height/2);
//				d.scaleX = d.scaleY = box.width/d.Target.width;
//				d.alpha = bear.alpha;
//				bear.alpha = 0;
//			}
		}
		
		private void tutorialContinue( CEvent e )
		{
			//playSound( "2E-Skill Instruction Audio/2E-there-were-two-beans" );
		}
		
		private void clickSound ( CEvent e )
		{
			SoundUtils.PlaySimpleEffect("click");
		}
		
		private void plattyMoved( CEvent e )
		{
		//	playSound( "2E-Skill Instruction Audio/2E-when-youre-done-touch-the-switch" );
		}

		private void plattyStopped( CEvent e )
		{
			if( !stopPlayToggle )
			{
				tutorial_mc.stopAll ();
				stopPlayToggle = true;
//				eachFrame(e);
//				tutorial_mc.removeEventListener(CEvent.ENTER_FRAME, eachFrame);
			}
		}

		public void OnAudioWordComplete ( string word)
		{
			if (word == "PLANTS") {
				playSound( "2E-Skill Instruction Audio/2E-when-youre-done-touch-the-switch", "TOUCH THE SWITCH" );
			}
			else if (word == "TOUCH THE SWITCH") {
				SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
				if( stopPlayToggle )
				{
					tutorial_mc.play ();
//					tutorial_mc.addEventListener(CEvent.ENTER_FRAME, eachFrame);
				}
				else
					stopPlayToggle = true;
			}
		}

		private void doorGlowOn( CEvent e )
		{
			house.glowOn();
		}
		
		private void doorClick( CEvent e )
		{
			Debug.Log ("CLICK");
			house.closeDoor();
			house.glowOn();
			SoundUtils.PlaySimpleEffect("ding");
		}
		
		private void doorGlowOff( CEvent e )
		{
			house.glowOff();
		}
		
		private void plattyWrapUp( CEvent e )
		{
			playSound( "2E-Skill Instruction Audio/2E-there-were-two-beans" );
		}
		
		private void glowAll( CEvent e )
		{
			for(int i = 0; i < 2; i++)
			{
				bears[i].glowOn();
			}
		}
		
		private void glowAllOff( CEvent e )
		{
			for(int i = 0; i < 2; i++)
			{
				bears[i].glowOff();
			}
		}
		
		private void glowOne( CEvent e )
		{
			bears[2].glowOn();
		}
		
		private void glowOneOff( CEvent e )
		{
			bears[2].glowOff();
		}
		
		private void glowOthers( CEvent e )
		{
			for(int i = 0; i < 3; i++)
			{
				bears[i].glowOn();
			}
		}
		
		private void glowOthersOff( CEvent e )
		{
			for(int i = 0; i < 3; i++)
			{
				bears[i].Destroy();
				bears[i].glowOff();
			}
		}
		
		private void tutorialComplete( CEvent e )
		{
			house.door.Destroy();
			OnComplete();
		}
		
		public void OnComplete()	
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
		}
		
		private void playSound ( string clipName ) { playSound(clipName, ""); }
		private void playSound ( string clipName, string word )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.ADDITION, clipName );
			
			if(word != "")
				bundle.AddClip(clip, word, clip.length);
			else
				bundle.AddClip(clip);
			
			SoundEngine.Instance.PlayBundle(bundle);
		}

		public void Resize( int width, int height )
		{
			view.width = width;
			view.height = height;
			
			//tutorial_mc.scaleY = (float)height/(float)MainUI.STAGE_HEIGHT;
			//tutorial_mc.scaleX = tutorial_mc.scaleY = house.scaleX;

			if(PlatformUtils.GetPlatform() == Platforms.WIN)
			{
				tutorial_mc.scaleX = tutorial_mc.scaleY = house.scaleX * 0.7f;
				tutorial_mc.y = 200;
				tutorial_mc.x = ((width - (MainUI.STAGE_WIDTH*tutorial_mc.scaleX))/2) - 450;
			}
			else if(PlatformUtils.GetPlatform() == Platforms.IOSRETINA)
			{
				tutorial_mc.scaleX = tutorial_mc.scaleY = house.scaleX * 0.7f;
				tutorial_mc.y = 200;
				tutorial_mc.x = ((width - (MainUI.STAGE_WIDTH*tutorial_mc.scaleX))/2) - 300;
			}
			else if(PlatformUtils.GetPlatform() == Platforms.ANDROID)
			{
				tutorial_mc.y = ((height - (MainUI.STAGE_HEIGHT*tutorial_mc.scaleY))/2);
				tutorial_mc.x = (((width) - (MainUI.STAGE_WIDTH*tutorial_mc.scaleX))/2) + 120;
			}
			else
			{
				tutorial_mc.y = ((height - (MainUI.STAGE_HEIGHT*tutorial_mc.scaleY))/2);
				tutorial_mc.x = (((width) - (MainUI.STAGE_WIDTH*tutorial_mc.scaleX))/2) + 220;
			}

			//house.x += (tutorial_mc.x / tutorial_mc.scaleX);
		}
		
		public DisplayObject View
		{
			get{ return view; }
		}
	}
}