using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;

namespace SSGCore
{

	public class BlendingCompoundWordsTutorial : Tutorial
	{
		//Constants
		//Placement
		private const float ARM_ANIMATION_OFFSET_Y = 150f;
		private const float ARM_ANIMATION_OFFSET_X = -300f;
		
		//Timing
		private const float TWEEN_TIMING = .35f;
		private const float ARM_ANIMATION_TIMING = 1.35f;
		private const float INACTIVITY_TIMER_LENGTH = 5.0f;
		
		//strings
		private const string INTRO = "INTRO";
		private const string TUTORIAL_STEP = "TUTORIAL_STEP";
		private const string TUTORIAL_INACTIVITY = "TUTORIAL_INACTIVITY";
		private const string RESTART_TUTORIAL = "RESTART_TUTORIAL";
		
		//Reference to the skill
		public BlendingCompoundWords skillRef;
		
		private MovieClip arm_mc;
		
		private TimerObject inactivityTimer;
		public int inactivityCounter = 0;
		
		private TimerObject glowTimer;
		private int glowCount = 0;
		
		private int stepCounter = 0;
		
		private bool resetOnce;
		public bool correct = true;
		private bool firstGlowTime = true;

		public BlendingCompoundWordsTutorial()
		{
			//create arm motion
			arm_mc = MovieClipFactory.CreateBlendingCompoundWordsPuzzlePieceArm();
			arm_mc.gotoAndStop(1);
			
			//Register OnAudioWordComplete to recieve audio word complete events
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			resetOnce = false;
		}
		
		public void OnStart()
		{
			//Called after theme intro
			arm_mc.addFrameScript(45, TutorialActions); //Arm at height of action
			arm_mc.stop ();
			arm_mc.alpha = 0;
			
			//reset counters
			inactivityCounter = 0;
			stepCounter = 0;
			glowCount = 0;
			skillRef.tutorialSetupCounter = 0;

			Debug.Log ("OnStart() TutorialActions()");
			TutorialActions ();
		}
		
		public void TutorialActions(CEvent e) //Proxy function to recieve events
		{
			Debug.Log ("TutorialActions(CEvent e) TutorialActions()");
			TutorialActions();
		}
		
		public void TutorialActions() //A linear list of actions for the tutorial
		{
			Debug.Log ("Tutorial STEP COUNTER = " + stepCounter );
			
			//Bundle for clumping audio
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;


			Debug.Log ("TutorialActions() stepCounter = " + stepCounter );
			if(stepCounter == 0)
			{
				
				//0
				//Doghouse is set up and dog is given
				//"Cami needs to celan up her puzzle game" plays
				//"Here's how to play the game" plays
				//Cami is cleaning up a puzzle. Camis is thinking..." plays
				//"Cami is dragging her house" plays

				//Setup "DogHouse"
				skillRef.SetUpOpportunity();

				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS, "Narration/1C-Story Introduction Audio/1C-Story-Introduction");
				bundle.AddClip(clip, "INTRO", clip.length); //No message needed yet

				SoundEngine.Instance.PlayBundle( bundle );

				return;
				
			}
			
			if(stepCounter == 1)
			{
				//1
				//Animation 1 plays - puzzlepiece to picture - need frame event	
				
				//Set up arm - add to stage, move to position, and play
				skillRef.backgroundContainer.addChild(arm_mc);
				arm_mc.x = skillRef.AnswerOptionsPuzzlePieces[0].x - ARM_ANIMATION_OFFSET_X;
				arm_mc.y = skillRef.AnswerOptionsPuzzlePieces[0].y - ARM_ANIMATION_OFFSET_Y;
				arm_mc.gotoAndPlay(40);
				arm_mc.alpha = 1;
				stepCounter++;
				return;
			}
			
			if(stepCounter == 2)
			{
				//2
				//Animaiton 2 - arm to puzzle piece resting place
				//Stop animation then tween both arm + piece to answer location
				arm_mc.gotoAndStop(46);
				Vector2 targetLocation = skillRef.compoundWordCard.localToGlobal(new Vector2(0,0));
				targetLocation = skillRef.backgroundContainer.globalToLocal(targetLocation);
				
				TweenerObj tempTweener;
				tempTweener =  Tweener.addTween(skillRef.AnswerOptionsPuzzlePieces[0], Tweener.Hash("time", ARM_ANIMATION_TIMING, "x", targetLocation.x, 
												"y", targetLocation.y) );
				Tweener.addTween(arm_mc, Tweener.Hash("time", ARM_ANIMATION_TIMING, "x", targetLocation.x - (1.75f*ARM_ANIMATION_OFFSET_X), 
												"y", targetLocation.y, "rotation", 30f) );
				tempTweener.OnComplete(TutorialActions);
				
				SoundUtils.PlaySimpleEffect("click");
				
				stepCounter++;
				return;				
			}
			
			if(stepCounter == 3)
			{
				//3
				//Highlight on the card
				//Correct Answer Sound
				skillRef.HighlightOn(skillRef.AnswerOptionsPuzzlePieces[0]);
				
				clip = SoundUtils.LoadGlobalSound("ding");
				bundle.AddClip(clip, TUTORIAL_STEP, clip.length); 
				SoundEngine.Instance.PlayBundle( bundle );
				
				stepCounter++;
				return;
			}
			
			if(stepCounter == 4)
			{			
				//4
				//Tutorial Reset - birdcage
				//"Now you try"
				//"Birdcage without bird is"
				//"Drag the picture that completes birdcage"
				//Activate piece
				
				//Turn off the glow
				skillRef.HighlightOff(skillRef.AnswerOptionsPuzzlePieces[0]);
				//Fade out arm
				Tweener.addTween(arm_mc, Tweener.Hash("time", TWEEN_TIMING, "alpha", 0, "transition", Tweener.TransitionType.easeInOutQuint) );

				
				//Setup next tutorial opportunity
				skillRef.SetUpOpportunity();
				
				//Add "Now you try"
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS, "Narration/1C-Skill Instructions Audio/1C-now-you-try");
				bundle.AddClip(clip, "DETAILED_PROMPT", clip.length);		
				SoundEngine.Instance.PlayBundle( bundle );
				
				stepCounter++;
				
				skillRef.AddNewOppData( BlendingCompoundWords.audioNames.IndexOf( "Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-birdcage" ) );
				skillRef.setObjectData();
				return;
				
			}
			
			if(stepCounter == 5)
			{
				skillRef.backgroundContainer.removeChild(arm_mc);
				//Players turn, add event listener
				skillRef.AnswerOptionsPuzzlePieces[0].addEventListener( MouseEvent.MOUSE_DOWN, skillRef.OnPieceDown);
				
				//Add event listeners to make sure inactivity timer won't fire while the player is interacting with the puzzle pieces
				skillRef.addEventListener( MouseEvent.MOUSE_DOWN, InactivityTimerOff );
				skillRef.addEventListener( MouseEvent.MOUSE_UP, InactivityTimerOn );
				
				//Start Timer
				InactivityTimerOn();
				
				stepCounter++;
				return;

			}
			
			if(stepCounter == 6)
			{
				//6
				
				//stop timer and remove listeners
				InactivityTimerOff();
				skillRef.removeEventListener( MouseEvent.MOUSE_DOWN, InactivityTimerOff );
				skillRef.removeEventListener( MouseEvent.MOUSE_UP, InactivityTimerOn );
				
				SoundEngine.Instance.SendCancelToken();
				
				skillRef.PlayCorrectSoundPublic();
				stepCounter++;
				return;
			}
			
			if(stepCounter == 7)
			{
				skillRef.dispatchEvent( new SkillEvent( SkillEvent.COMPLIMENT_REQUEST, true, false ) );
				
				stepCounter++;
				return;
			}
			
			if(stepCounter == 8)
			{
				//"birdcage without bird is cage"
				//"now let's play the game
				//Add "birdcage without bird is cage"
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS, "Narration/1C-Reinforcement Audio/1C-Answer-bird-with-cage");
				bundle.AddClip(clip, "", clip.length); //No message needed yet
				glowTimer = TimerUtils.SetTimeout(0.25f, GlowTime);
				
				//Add "now let's play the game"
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS, "Narration/1C-Skill Instructions Audio/1C-lets-keep-making-bigger-sounds");
				bundle.AddClip(clip, TUTORIAL_STEP, clip.length); 
				SoundEngine.Instance.PlayBundle( bundle );
				
				stepCounter++;
				return;
				
			}
			
			if(stepCounter == 9)
			{
			 //End Tutorial
				OnComplete();
			}
	
		}

		public void ThemeIntroComplete() {
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;

			//Add "Cami needs to clean up her puzzle game"
			
			clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS, "Narration/1C-Skill Instructions Audio/1C-the-small-cubes-are-dog-and-house");
			bundle.AddClip(clip, "", clip.length); //No message needed yet
			
			//Add "Here's how to play the game"
			clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS, "Narration/1C-Skill Instructions Audio/1C-watch-closely-as-platty");
			bundle.AddClip(clip, TUTORIAL_STEP, clip.length); //No message needed yet

			SoundEngine.Instance.PlayBundle( bundle );

			glowTimer = TimerUtils.SetTimeout(1f, GlowTime);
			skillRef.fadeInTutorialPieces ();
			
			stepCounter++;
		}

		//Fires everytime the player is inactive
		public void InactivityTimerComplete()
		{
			skillRef.dispatchEvent( new SkillEvent(SkillEvent.TIMER_COMPLETE, true, false) );
			//Bundle for clumping audio
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;
			
			if(inactivityCounter ==0)
			{
				//Remove Event listeners so they will not interrupt prompt
				//Remove Event listeners first so that they will not interrupt prompt
				//skillRef.removeEventListener( MouseEvent.MOUSE_DOWN, InactivityTimerOff );
				//skillRef.removeEventListener( MouseEvent.MOUSE_UP, InactivityTimerOn );
				//skillRef.AnswerOptionsPuzzlePieces[0].removeEventListener( MouseEvent.MOUSE_DOWN, skillRef.OnPieceDown);
				
				//Add "Now you try"
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS, "Narration/1C-Skill Instructions Audio/1C-now-you-try");
				bundle.AddClip(clip, "", clip.length); //No message needed yet
				
				//Add "Drag the picture that completes birdcage"
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS, "Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-birdcage");
				bundle.AddClip(clip, TUTORIAL_INACTIVITY, clip.length);
				
				SoundEngine.Instance.PlayBundle( bundle );
				
				inactivityCounter++;
				
				skillRef.baseTheme.AddAudioSessionData( "time", BlendingCompoundWords.audioNames.IndexOf("Narration/1C-Skill Instructions Audio/1C-Initial Instructions/1C-Drag-birdcage") );
				return;
			}
			if(inactivityCounter == 1)
			{
				if(resetOnce)
				{
					//skillRef.timeoutEnd = true;
					correct = false;
					OnComplete();
				}
				else
				{
					//Player is not responding, reset Tutorial
					
					//Remove Event listeners first so that they will not interrupt prompt
					skillRef.removeEventListener( MouseEvent.MOUSE_DOWN, InactivityTimerOff );
					skillRef.removeEventListener( MouseEvent.MOUSE_UP, InactivityTimerOn );
					skillRef.AnswerOptionsPuzzlePieces[0].removeEventListener( MouseEvent.MOUSE_DOWN, skillRef.OnPieceDown);
					
					//Play "Let's try again"
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_COMPOUND_WORDS, "Narration/1C-Skill Instructions Audio/1C-try-again");
					bundle.AddClip(clip, RESTART_TUTORIAL, clip.length); 
					SoundEngine.Instance.PlayBundle( bundle );
					
					resetOnce = true;
					
					skillRef.baseTheme.AddAudioSessionData( "time", BlendingCompoundWords.audioNames.IndexOf("Narration/1C-Skill Instructions Audio/1C-try-again") );
				}
			}
		}
	
		public void OnComplete()
		{
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			skillRef.OnTutorialComplete();
		}
		
		public void Resize( int width, int height )
		{
		}
		
		public DisplayObject View
		{
			get{ return null; }
		}
		
		public void OnAudioWordComplete(string word)
		{
			switch(word)
			{
			case INTRO:
				ThemeIntroComplete();
				break;

			case TUTORIAL_STEP:
				TutorialActions();
				break;
			case TUTORIAL_INACTIVITY:
				//Reset listeners
				//Remove Event listeners first so that they will not interrupt prompt
				skillRef.addEventListener( MouseEvent.MOUSE_DOWN, InactivityTimerOff );
				skillRef.addEventListener( MouseEvent.MOUSE_UP, InactivityTimerOn );
				skillRef.AnswerOptionsPuzzlePieces[0].addEventListener( MouseEvent.MOUSE_DOWN, skillRef.OnPieceDown);
				
				InactivityTimerOn();
				break;
			case RESTART_TUTORIAL:
				Restart();
				break;
			}
		}
				
		private void GlowTime()
		{
			switch(glowCount)
			{
			case 0:
				skillRef.HighlightOn( skillRef.puzzlePieceLeft );
				glowTimer = TimerUtils.SetTimeout(1.6f, GlowTime);
				break;
			case 1:
				skillRef.HighlightOn( skillRef.puzzlePieceRight );
				glowTimer = TimerUtils.SetTimeout(firstGlowTime ? 6f : 1f, GlowTime);
				if (firstGlowTime)
					firstGlowTime = false;
				break;
			case 2:
				skillRef.HighlightOff( skillRef.puzzlePieceLeft );
				skillRef.HighlightOff( skillRef.puzzlePieceRight );
				if(stepCounter !=4 && stepCounter !=5)
				{
					skillRef.HighlightOn( skillRef.AnswerOptionsPuzzlePieces[0] );
					glowTimer = TimerUtils.SetTimeout(3f, GlowTime);
				}
				else
					glowCount = -1;
				break;
			case 3:
				skillRef.HighlightOff( skillRef.AnswerOptionsPuzzlePieces[0] );
				glowCount = -1;
				break;
			}
			glowCount++;
		}
		
		public void Restart()
		{
			OnStart();
		}
		
		private void InactivityTimerOn(CEvent e)
		{
			InactivityTimerOn();
		}
		
		private void InactivityTimerOn()
		{
			inactivityTimer = TimerUtils.SetTimeout(INACTIVITY_TIMER_LENGTH, InactivityTimerComplete);
		}
		
		public void InactivityTimerOff(CEvent e)
		{
			InactivityTimerOff();
		}
		
		public void InactivityTimerOff()
		{
			if(inactivityTimer != null) inactivityTimer.StopTimer();
		}
		
		public void Dispose()
		{
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			InactivityTimerOff();
			if(glowTimer != null) glowTimer.Unload();
		}
		
	}
	
}