using UnityEngine;
using System.Collections;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;
using pumpkin.geom;

namespace SSGCore
{

	public class SpacialSkillsTutorial : Tutorial
	{
	
		
		private DisplayObjectContainer view = new DisplayObjectContainer();
		public DisplayObject View
		{
			get{ return view; }
		}
		
		private MovieClip tutorial_mc;
		private DisplayObjectContainer behind = new DisplayObjectContainer();
		public FilterMovieClip bug;
		
		public SpacialSkillsTutorial()
		{

			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;

			behind.width = MainUI.STAGE_WIDTH;
			behind.height = MainUI.STAGE_HEIGHT;
			view.addChild(behind);
			
			tutorial_mc = MovieClipFactory.CreateSpatialSkillsTutorial();
			tutorial_mc.gotoAndStop(1);
			tutorial_mc.width = MainUI.STAGE_WIDTH;
			tutorial_mc.height = MainUI.STAGE_HEIGHT;
			tutorial_mc.alpha = 0f;
			view.addChild( tutorial_mc );

			tutorial_mc.gotoAndStop(1);

			//tutorial_mc.addFrameScript(124, continueAudio);
			//tutorial_mc.addFrameScript(250, finishAudio);
			tutorial_mc.addFrameScript(270, glowUp);
			tutorial_mc.addFrameScript(280, dingIt);
			tutorial_mc.addFrameScript (300, confirmationAudio);
			tutorial_mc.addFrameScript(355, completeTutorial);
		}

		public void OnAudioWordComplete(string word)
		{
			if(word == "Tutorial_Intro")
			{
				DebugConsole.Log("Playing Cami Hand");
				tutorial_mc.gotoAndPlay(200);
			}

			if (word == "Tutorial_Finish") 
			{
				tutorial_mc.gotoAndPlay(350);
			}
		}
		
		public void OnStart()
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.START) );

			tutorial_mc.gotoAndStop(200);

			Tweener.addTween(tutorial_mc, Tweener.Hash("time", .25f, "alpha", 1f));

			bug = new FilterMovieClip(new MovieClip("SpacialSkills.swf", "Under"));
			bug.Target.gotoAndStop("PARACHUTE");
			//bugClip.x = bugClip.y = 0;
			
			bug.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
			bug.filters[0].Visible = false;

			MovieClip bug_mc = tutorial_mc.getChildByName<MovieClip>("bug");
			bug_mc.removeAllChildren();
			view.addChildAt(bug, 0);
			bug.x = (tutorial_mc.x + bug_mc.x);
			bug.y = (tutorial_mc.y + ( bug_mc.y * tutorial_mc.scaleY));
			//bug.x = bug.y = 0;
			Debug.Log("below paracute alpha: " + bug.alpha);

			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SPATIAL_SKILLS, "3B-Skill-Instruction-Audio/3B-many-things-fly-above" );
			bundle.AddClip(clip, "Tutorial_Intro", clip.length);
			SoundEngine.Instance.PlayBundle( bundle );

		}
		
		private void continueAudio( CEvent e )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SPATIAL_SKILLS, "3B-Skill-Instruction-Audio/3B-many-things-fly-above" );
			bundle.AddClip(clip);
			SoundEngine.Instance.PlayBundle( bundle );
		}
		
		private void glowUp( CEvent e )
		{
			if(bug.filters.Count > 0)
				bug.filters[0].Visible = true;
			else
				bug.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());

			bug.scaleX = bug.scaleY = 1.25f;

			SoundUtils.PlaySimpleEffect("click");
		}
		
		private void dingIt( CEvent e )
		{
			SoundUtils.PlaySimpleEffect("ding");
		}

		private void confirmationAudio (CEvent e)
		{
			tutorial_mc.gotoAndStop (310);
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SPATIAL_SKILLS, "3B-Skill-Instruction-Audio/3B-yes-thats-the-parachutist" );
			bundle.AddClip(clip, "Tutorial_Finish", clip.length);
			SoundEngine.Instance.PlayBundle( bundle );
		}
		
		private void finishAudio( CEvent e )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SPATIAL_SKILLS, "3B-Skill-Instruction-Audio/3B-she-is-touching" );
			bundle.AddClip(clip);
			SoundEngine.Instance.PlayBundle( bundle );
			if(bug.filters.Count > 0)
				bug.filters[0].Visible = true;
			else
				bug.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
		}
		
		private void completeTutorial( CEvent e )
		{
			OnComplete();
		}
		
		public void OnComplete()
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
		}
		
		public void Resize( int width, int height )
		{
			view.width = width;
			view.height = height;
			//tutorial_mc.FitToParent();
			
			tutorial_mc.scaleY = height/tutorial_mc.height;
			tutorial_mc.scaleX = tutorial_mc.scaleY;
			tutorial_mc.y = height * 0.125f;
			tutorial_mc.x = (width * 0.5f) - 25;
			
			//behind.FitToParent();
		}
		
	}

}