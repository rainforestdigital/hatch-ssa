using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.events;
using pumpkin.tweener;

namespace SSGCore
{
	
	public class SegmentingCompoundWordsTutorial : Tutorial
	{
		//Constants
		//Placement
		private const float ARM_ANIMATION_OFFSET_Y = 300;
		private const float ARM_ANIMATION_OFFSET_X = 800;
		
		//Timing
		private const string INTRO = "INTRO";
		private const float TWEEN_TIMING = .35f;
		private const float ARM_ANIMATION_TIMING = 1.35f;
		private const float INACTIVITY_TIMER_LENGTH = 5.0f;
		
		//strings
		private const string TUTORIAL_STEP = "TUTORIAL_STEP";
		private const string INACTIVITY = "INACTIVITY";
		private const string RESTART_TUTORIAL = "RESTART_TUTORIAL";
		
		//Reference to the skill
		public SegmentingCompoundWords skillRef;
		
		private MovieClip arm_mc;
		
		private TimerObject inactivityTimer;
		public int inactivityCounter = 0;
		
		private TimerObject glowTimer;
		private int glowCount = 0;
		
		private int stepCounter = 0;
		
		private bool resetOnce;
		public bool correct = true;
		
		//Platform placement modifiers
		private float armModifier_X = 0;
		private float armModifier_Y = 0;
		private float animationOffset_Modifier_X = 0f;
		
		private float initialArm_X = 0;
		private float initialArm_Y = 0;
		
		public SegmentingCompoundWordsTutorial()
		{
			//create arm motion
			arm_mc = MovieClipFactory.CreateSegmentingCompoundWordsPuzzlePieceArm();
			arm_mc.gotoAndStop(1);
			
			//Register OnAudioWordComplete to recieve audio word complete events
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			
			resetOnce = false;
		}
		
		public void OnStart()
		{
			//Called after theme intro
			arm_mc.addFrameScript(Mathf.RoundToInt(45 * 0.5f), TutorialActions); //Arm at height of action
			arm_mc.stop ();
			//arm_mc.alpha = 0;
			
			if (PlatformUtils.GetPlatform() == Platforms.WIN) {
				arm_mc.scaleX = arm_mc.scaleY = 0.7f;
			}
			
			//reset counters
			inactivityCounter = 0;
			stepCounter = 0;
			glowCount = 0;
			skillRef.tutorialSetupCounter = 0;
			
			initPlatformValues ();
			
			TutorialActions ();
			
			
		}
		
		void initPlatformValues () {
			Platforms selectedPlatform = PlatformUtils.GetPlatform ();
			//selectedPlatform = Platforms.ANDROID;
			
			switch (selectedPlatform) {
			case Platforms.ANDROID:
				//armModifier_X = 180f;
				armModifier_Y = 200f;
				break;
				
			case Platforms.IOS:
				armModifier_X = 180f;
				armModifier_Y = 200f;
				break;
				
			case Platforms.IOSRETINA:
				
				animationOffset_Modifier_X = 200f;
				armModifier_X = 180f;
				armModifier_Y = 500f;
				break;
				
			case Platforms.WIN:
				break;
			}
		}
		
		public void TutorialActions(CEvent e) //Proxy function to recieve events
		{
			TutorialActions();
		}
		
		public void TutorialActions() //A linear list of actions for the tutorial
		{
			Debug.Log ("Tutorial STEP COUNTER = " + stepCounter );
			
			//Bundle for clumping audio
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;
			
			if(stepCounter == 0)
			{
				
				//0
				//Doghouse is set up and dog is given
				//"Cami needs to celan up her puzzle game" plays
				//"Here's how to play the game" plays
				//Cami is cleaning up a puzzle. Camis is thinking..." plays
				//"Cami is dragging her house" plays
				
				//Setup "DogHouse"
				skillRef.SetUpOpportunity();
				
				//Add "Cami needs to clean up her puzzle game"
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Story Introduction Audio/1D-story-introduction");
				bundle.AddClip(clip, "INTRO", clip.length);
				
				//Add "Cami is dragging her house"
				/*
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Skill Instruction Audio/1D-now-its-your-turn");
				bundle.AddClip(clip, TUTORIAL_STEP, clip.length); 
				*/
				SoundEngine.Instance.PlayBundle( bundle );
				
				
				
				//stepCounter++;
				return;
				
			}
			
			if(stepCounter == 1)
			{
				
				//1
				//Animation 1 plays - puzzlepiece to picture - need frame event	
				
				//Set up arm - add to stage, move to position, and play
				skillRef.backgroundContainer.addChild(arm_mc);
				
				//arm_mc.x = skillRef.AnswerOptionsPuzzlePieces[0].x - ARM_ANIMATION_OFFSET_X;
				//arm_mc.y = skillRef.AnswerOptionsPuzzlePieces[0].y - ARM_ANIMATION_OFFSET_Y;
				if (!resetOnce) {
					Debug.Log("Reset false");
					arm_mc.x += ARM_ANIMATION_OFFSET_X + animationOffset_Modifier_X;
					arm_mc.y -= ARM_ANIMATION_OFFSET_Y;
					
					initialArm_X = arm_mc.x;
					initialArm_Y = arm_mc.y;
				}  else {
					arm_mc.x = initialArm_X;
					arm_mc.y = initialArm_Y;
				}
				
				Debug.Log("ArmX: " + arm_mc.x + " ArmY: " + arm_mc.y);
				
				//arm_mc.gotoAndPlay( Mathf.RoundToInt(40 * 0.5f));
				arm_mc.gotoAndPlay(0);
				
				arm_mc.alpha = 1;
				stepCounter++;
				return;
			}
			
			if(stepCounter == 2)
			{
				//2
				//Animaiton 2 - arm to puzzle piece resting place
				//Stop animation then tween both arm + piece to answer location
				arm_mc.gotoAndStop(46);
				//arm_mc.gotoAndPlay(46);
				//arm_mc.gotoAndStop(Mathf.RoundToInt(46 * 0.5f));
				Vector2 targetLocation = skillRef.puzzlePieceRight.localToGlobal(new Vector2(0,0));
				targetLocation = skillRef.backgroundContainer.globalToLocal(targetLocation);
				
				
				TweenerObj tempTweener;
				tempTweener =  Tweener.addTween(skillRef.AnswerOptionsPuzzlePieces[0], Tweener.Hash("time", ARM_ANIMATION_TIMING, "x", targetLocation.x, 
				                                                                                    "y", targetLocation.y) );
				
				Tweener.addTween(arm_mc, Tweener.Hash("time", ARM_ANIMATION_TIMING, "x", targetLocation.x  + arm_mc.x - armModifier_X , 
				                                      "y", (targetLocation.y + arm_mc.y) - armModifier_Y, "rotation", 30f) );
				tempTweener.OnComplete(TutorialActions);
				
				SoundUtils.PlaySimpleEffect("click");
				
				stepCounter++;
				return;				
			}
			
			if(stepCounter == 3)
			{
				//3
				//Highlight on the card
				//Correct Answer Sound
				skillRef.HighlightOn(skillRef.AnswerOptionsPuzzlePieces[0]);
				
				clip = SoundUtils.LoadGlobalSound("ding");
				bundle.AddClip(clip, TUTORIAL_STEP, clip.length); 
				SoundEngine.Instance.PlayBundle( bundle );
				
				stepCounter++;
				return;
			}
			
			if(stepCounter == 4)
			{			
				//4
				//Tutorial Reset - birdcage
				//"Now you try"
				//"Birdcage without bird is"
				//"Drag the picture that completes birdcage"
				//Activate piece
				
				//Turn off the glow
				skillRef.HighlightOff(skillRef.AnswerOptionsPuzzlePieces[0]);
				//Fade out arm
				Tweener.addTween(arm_mc, Tweener.Hash("time", TWEEN_TIMING, "alpha", 0, "y", 500f, "rotation", 0f) );
				
				
				//Setup next tutorial opportunity
				skillRef.SetUpOpportunity();
				
				//Add "Now you try"
				skillRef.showBirdcagePuzzle();
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Skill Instruction Audio/1D-now-its-your-turn");
				bundle.AddClip(clip, "", clip.length); //No message needed yet
				
				//Add "Birdcage without bird is"
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-birdcage-without-bird");
				bundle.AddClip(clip, "", clip.length); //No message needed yet
				
				//Add "Drag the picture that completes birdcage"
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Skill Instruction Audio/1D-drag-birdcage");
				bundle.AddClip(clip, TUTORIAL_STEP, clip.length);				
				SoundEngine.Instance.PlayBundle( bundle );
				
				glowTimer = TimerUtils.SetTimeout(2.5f, GlowTime);
				
				stepCounter++;
				
				skillRef.AddNewOppData( SegmentingCompoundWords.audioNames.IndexOf( "Narration/1D-Skill Instruction Audio/1D-drag-birdcage" ) );
				skillRef.setObjectData();
				return;
				
			}
			
			if(stepCounter == 5)
			{
				skillRef.backgroundContainer.removeChild(arm_mc);
				//Players turn, add event listener
				skillRef.AnswerOptionsPuzzlePieces[0].addEventListener( MouseEvent.MOUSE_DOWN, skillRef.OnPieceDown);
				
				//Add event listeners to make sure inactivity timer won't fire while the player is interacting with the puzzle pieces
				skillRef.addEventListener( MouseEvent.MOUSE_DOWN, InactivityTimerOff );
				skillRef.addEventListener( MouseEvent.MOUSE_UP, InactivityTimerOn );
				
				//Start Timer
				InactivityTimerOn();
				
				stepCounter++;
				return;
				
			}
			
			if(stepCounter == 6)
			{
				//6
				//stop timer and remove listeners
				InactivityTimerOff();
				skillRef.removeEventListener( MouseEvent.MOUSE_DOWN, InactivityTimerOff );
				skillRef.removeEventListener( MouseEvent.MOUSE_UP, InactivityTimerOn );
				
				SoundEngine.Instance.SendCancelToken();
				
				skillRef.PlayCorrectSoundPublic();
				stepCounter++;
				return;
			}
			
			if(stepCounter == 7)
			{
				skillRef.dispatchEvent( new SkillEvent( SkillEvent.COMPLIMENT_REQUEST, true, false ) );
				
				stepCounter++;
				return;
			}
			
			if(stepCounter == 8)
			{
				//"birdcage without bird is cage"
				//"now let's play the game
				//Add "birdcage without bird is cage"
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Reinforcement Audio/1D-AB without A is B/1D-birdcage-without-bird");
				bundle.AddClip(clip, "", clip.length); //No message needed yet
				glowTimer = TimerUtils.SetTimeout(0.25f, GlowTime);
				
				//Add "now let's play the game"
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Skill Instruction Audio/1D-now-lets-play-the-game");
				bundle.AddClip(clip, TUTORIAL_STEP, clip.length); 
				SoundEngine.Instance.PlayBundle( bundle );
				
				correct = true;
				
				stepCounter++;
				return;
				
			}
			
			if(stepCounter == 9)
			{
				//End Tutorial
				//Debug.LogError("Step Counter 8");
				OnComplete();
			}
			
		}
		
		public void ThemeIntroComplete() {
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;
			
			//Add "Here's how to play the game"
			clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Skill Instruction Audio/1D-we-need-doghouse");
			bundle.AddClip(clip, "", clip.length); //No message needed yet
			
			//Add "Cami is cleaning up a puzzle. Camis is thinking..."
			clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Skill Instruction Audio/1D-cami-thinks");
			bundle.AddClip(clip, TUTORIAL_STEP, clip.length); //No message needed yet
			
			SoundEngine.Instance.PlayBundle( bundle );
			
			skillRef.fadeInTutorialPieces ();
			glowTimer = TimerUtils.SetTimeout(3.5f, GlowTime);
			stepCounter++;
		}
		
		//Fires everytime the player is inactive
		public void InactivityTimerComplete()
		{
			skillRef.dispatchEvent( new SkillEvent(SkillEvent.TIMER_COMPLETE, true, false) );
			//Bundle for clumping audio
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip;
			
			if(inactivityCounter == 0)
			{
				//Remove Event listeners so they will not interrupt prompt
				//Remove Event listeners first so that they will not interrupt prompt
				//skillRef.removeEventListener( MouseEvent.MOUSE_DOWN, InactivityTimerOff );
				//skillRef.removeEventListener( MouseEvent.MOUSE_UP, InactivityTimerOn );
				//skillRef.AnswerOptionsPuzzlePieces[0].removeEventListener( MouseEvent.MOUSE_DOWN, skillRef.OnPieceDown);
				
				//Add "Now you try"
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Skill Instruction Audio/1D-now-its-your-turn");
				bundle.AddClip(clip, "", clip.length); //No message needed yet
				
				//Add "birdcage without bird is cage"
				clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-birdcage-without-bird");
				bundle.AddClip(clip, INACTIVITY, clip.length);
				
				SoundEngine.Instance.PlayBundle( bundle );
				
				glowTimer = TimerUtils.SetTimeout(2.5f, GlowTime);
				
				inactivityCounter++;
				
				skillRef.baseTheme.AddAudioSessionData( "time", SegmentingCompoundWords.audioNames.IndexOf("Narration/1D-Skill Instruction Audio/1D-Initial Instruction/1D-AB without A is/1D-birdcage-without-bird") );
				return;
			}
			if(inactivityCounter == 1)
			{
				if(resetOnce)
				{
					//skillRef.timeoutEnd = true;
					correct = false;
					OnComplete();
				}
				else
				{
					//Player is not responding, reset Tutorial
					
					//Remove Event listeners first so that they will not interrupt prompt
					skillRef.removeEventListener( MouseEvent.MOUSE_DOWN, InactivityTimerOff );
					skillRef.removeEventListener( MouseEvent.MOUSE_UP, InactivityTimerOn );
					skillRef.AnswerOptionsPuzzlePieces[0].removeEventListener( MouseEvent.MOUSE_DOWN, skillRef.OnPieceDown);
					
					//Play "Let's try again"
					clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SEGMENTING_COMPOUND_WORDS, "Narration/1D-Skill Instruction Audio/1D-try-again");
					bundle.AddClip(clip, RESTART_TUTORIAL, clip.length); 
					SoundEngine.Instance.PlayBundle( bundle );
					
					resetOnce = true;
					
					skillRef.baseTheme.AddAudioSessionData( "time", SegmentingCompoundWords.audioNames.IndexOf("Narration/1D-Skill Instruction Audio/1D-try-again") );
				}
			}
		}
		
		public void OnComplete()
		{
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			skillRef.OnTutorialComplete();
		}
		
		public void Resize( int width, int height )
		{
			
			
		}
		
		public DisplayObject View
		{
			get{  return null; }
		}
		
		public void OnAudioWordComplete(string word)
		{
			switch(word)
			{
			case INTRO:
				ThemeIntroComplete();
				break;

			case TUTORIAL_STEP:
				TutorialActions();
				break;
			case INACTIVITY:
				//Reset listeners
				//Remove Event listeners first so that they will not interrupt prompt
				skillRef.addEventListener( MouseEvent.MOUSE_DOWN, InactivityTimerOff );
				skillRef.addEventListener( MouseEvent.MOUSE_UP, InactivityTimerOn );
				skillRef.AnswerOptionsPuzzlePieces[0].addEventListener( MouseEvent.MOUSE_DOWN, skillRef.OnPieceDown);
				
				InactivityTimerOn();
				break;
			case RESTART_TUTORIAL:
				Restart();
				break;
			}
		}
		
		private void GlowTime()
		{
			switch(glowCount)
			{
			case 0:
				skillRef.HighlightOn( skillRef.compoundWordCard );
				glowTimer = TimerUtils.SetTimeout(1.6f, GlowTime);
				break;
			case 1:
				skillRef.HighlightOff( skillRef.compoundWordCard );
				skillRef.HighlightOn( skillRef.puzzlePieceLeft );
				glowTimer = TimerUtils.SetTimeout(1.6f, GlowTime);
				break;
			case 2:
				skillRef.HighlightOff( skillRef.puzzlePieceLeft );
				if(stepCounter != 4 && stepCounter != 5)
				{
					skillRef.HighlightOn( skillRef.AnswerOptionsPuzzlePieces[0] );
					glowTimer = TimerUtils.SetTimeout(1.6f, GlowTime);
				}
				else
					glowCount = -1;
				break;
			case 3:
				skillRef.HighlightOff( skillRef.AnswerOptionsPuzzlePieces[0] );
				glowCount = -1;
				break;
			}
			glowCount++;
		}
		
		public void Restart()
		{
			OnStart();
		}
		
		private void InactivityTimerOn(CEvent e)
		{
			InactivityTimerOn();
		}
		
		private void InactivityTimerOn()
		{
			inactivityTimer = TimerUtils.SetTimeout(INACTIVITY_TIMER_LENGTH, InactivityTimerComplete);
		}
		
		public void InactivityTimerOff(CEvent e)
		{
			InactivityTimerOff();
		}
		
		public void InactivityTimerOff()
		{
			if(inactivityTimer != null) inactivityTimer.StopTimer();
		}
		
		public void Dispose()
		{
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			InactivityTimerOff();
			if(glowTimer != null) glowTimer.Unload();
		}
		
	}
	
}
