using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;
using pumpkin.geom;

namespace SSGCore
{
	//TODO - Fix Frame numbers / timing
	public class SubtractionFromTenTutorial : Tutorial
	{
		public DisplayObjectContainer view;
		private MovieClip tutorial_mc;
		
		private List<FilterMovieClip> glows;
		
		public SubtractionFromTenTutorial ()
		{
			view = new DisplayObjectContainer();
			view.alpha = 0;
			
			tutorial_mc = MovieClipFactory.CreateSubtractionFromTenTutorial();
			tutorial_mc.gotoAndStop(1);
			tutorial_mc.looping = false;
			view.addChild( tutorial_mc );
			
			glows = new List<FilterMovieClip>();
			
			tutorial_mc.addFrameScript( 10, tutorialContinue);
			tutorial_mc.addFrameScript( 140, glowAllDogs );
			tutorial_mc.addFrameScript( 205, endGlowAll );
			tutorial_mc.addFrameScript( 348, playClick );
			tutorial_mc.addFrameScript( 407, promptShopkeep );
			tutorial_mc.addFrameScript( 475, glowCaretaker );
			tutorial_mc.addFrameScript( 505, endGlowAll );
			tutorial_mc.addFrameScript( 530, playDing );
			tutorial_mc.addFrameScript( 550, tutorialReinforcement );
			tutorial_mc.addFrameScript( 575, glowAllDogs );
			tutorial_mc.addFrameScript( 635, endGlowAll );
			tutorial_mc.addFrameScript( 675, glowSingleDog );
			tutorial_mc.addFrameScript( 720, endGlowAll );
			tutorial_mc.addFrameScript( 755, glowAll );
			tutorial_mc.addFrameScript( 810, endGlowAll );
			tutorial_mc.addFrameScript( 850, nowIts );
			tutorial_mc.addFrameScript( 895, tutorialComplete );
		}
		
		public void OnStart ()
		{			
			view.dispatchEvent( new TutorialEvent(TutorialEvent.START) );
			
			Tweener.addTween( view, Tweener.Hash("time", 0.25f, "alpha", 1f) );
			
			tutorial_mc.play();
		}
		
		public void tutorialContinue ( CEvent e )
		{
			playSound( "2H-Skill Instructions Audio/2H-some-of-these-pets-have-found homes");
		}
		
		public void glowWagonDogs( CEvent e )
		{
			//add a glow to dog that will stay put (1)
			MovieClip mc = tutorial_mc.getChildAt<MovieClip>( 1 );
			
			for( int i = mc.numChildren; i >= 0; i-- )
			{
				MovieClip mcChild = mc.getChildAt<MovieClip>( i );
				if( mcChild != null )
					addFilter( mcChild );
			}
		}
		
		public void glowSingleDog( CEvent e )
		{
			//add a glow to the dog to be dragged (2)
			addFilter( tutorial_mc.getChildAt<MovieClip>( 2 ) );
		}
		
		public void glowAllDogs( CEvent e )
		{
			//add a glow to dog to all of the dogs
			glowSingleDog(e);
			glowWagonDogs(e);
		}
		
		public void playClick( CEvent e )
		{
			SoundUtils.PlaySimpleEffect( "click" );
		}
		
		public void promptShopkeep( CEvent e )
		{
			playSound( "2H-Skill Instructions Audio/2H-when-youre-done-touch-caretaker");
		}
		
		public void glowCaretaker( CEvent e )
		{
			//add a glow to the caretaker (3)
			MovieClip mc = tutorial_mc.getChildAt<MovieClip>( 3 );
			
			DisplayObjectContainer mcParent = mc.parent;
			FilterGroupMovieClip fmc = new FilterGroupMovieClip( mc );
			fmc.removeChild( mc );
			mcParent.addChild( mc );
			view.addChildAt( fmc, 0 );
			fmc.x += mc.x;
			fmc.y += mc.y;
			glows.Add( fmc );
			fmc.AddFilter( FiltersFactory.GetScaledYellowGlowFilter );
		}
		
		public void playDing( CEvent e )
		{
			SoundUtils.PlaySimpleEffect( "ding" );
		}
		
		public void tutorialReinforcement( CEvent e )
		{
			playSound( "2H-Reinforcement Audio/TTEG/2H-7-dog-you-move-1-now-6");
		}
		
		public void glowAll( CEvent e )
		{
			//add a glow to the dogs in the wagon and the caretaker
			glowWagonDogs(e);
			glowCaretaker(e);
		}
		
		public void endGlowAll( CEvent e )
		{
			foreach( FilterMovieClip fmc in glows )
			{
				fmc.Destroy();
				if( fmc.parent != null )
					fmc.parent.removeChild( fmc );
			}
			glows.Clear();
		}
		
		public void nowIts( CEvent e )
		{
			SoundEngine.Instance.OnAudioWordEvent += OnAudioWordComplete;
			playSound( "2H-Skill Instructions Audio/2H-now-its-your-turn", "NOW_ITS_tutorial" );
		}
		
		public void OnAudioWordComplete( string word )
		{
			if( word == "NOW_ITS_tutorial" )
				OnComplete();
		}
		
		public void tutorialComplete( CEvent e )
		{
			tutorial_mc.stop ();
		}
		
		public void OnComplete()	
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
		}
		
		private void addFilter( MovieClip mc )
		{
			DisplayObjectContainer mcParent = mc.parent;
			FilterMovieClip fmc = new FilterMovieClip( mc );
			fmc.removeChild( mc );
			mcParent.addChild( mc );
			mc.addChildAt( fmc, 0 );
			fmc.x -= mc.x;
			fmc.y -= mc.y;
			glows.Add( fmc );
			fmc.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
		}
		
		private void playSound ( string clipName ) { playSound(clipName, ""); }
		private void playSound ( string clipName, string word )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SUBTRACTIONFROMTEN, clipName );
			
			if(word != "")
				bundle.AddClip(clip, word, clip.length);
			else
				bundle.AddClip(clip);
			
			SoundEngine.Instance.PlayBundle(bundle);
		}
		
		public void Dispose()
		{
			SoundEngine.Instance.OnAudioWordEvent -= OnAudioWordComplete;
			tutorial_mc.stop();
			
			endGlowAll(null);
		}

		public void Resize( int width, int height )
		{
		}
		
		public DisplayObject View
		{
			get{ return view; }
		}
	}
}