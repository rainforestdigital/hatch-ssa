using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;
using pumpkin.geom;

namespace SSGCore
{
	public class BlendingSoundsInWordsTutorial : Tutorial
	{
		private DisplayObjectContainer view;
		public DisplayObject View
		{
			get{  return view; }
		}
		private MovieClip tutorial_mc;
		
		public BlendingSoundsInWords parentRef;

		private FilterMovieClip leftBird;
		private FilterMovieClip middleBird;
		private FilterMovieClip rightBird;

		private FilterMovieClip answerBird_Left;
		private FilterMovieClip answerBird_Right;
		
		private FilterMovieClip marqueeSign;
		private FilterMovieClip penSign;

		private FilterMovieClip armFMC;
		private MovieClip armMC;

		private MovieClip tutorial_mc_2;

		private string audioPrecursor = "1G-";
//		private string audioFolder_InactivityPrompt 	= "Narration/1G-Inactivity Prompt Audio/";
		private string audioFolder_Reinforcement 		= "Narration/1G-Reinforcement Audio/";
		private string audioFolder_ReinforcementWordSounds 		= "Narration/1G-Reinforcement Audio/1G-Word Sounds/";
		private string audioFolder_SkillInstruction 	= "Narration/1G-Skill Instructions Audio/";
		private string audioFolder_SkillInstruction_BirdSounds 	= "Narration/1G-Skill Instructions Audio/1G-Bird Sounds/";
		
		private const string INTRODUCE_BIRDS = "Introduce Birds";
		
		private List<FilterMovieClip> glows;


		
		public BlendingSoundsInWordsTutorial () {
			
			view = new DisplayObjectContainer();
			view.width = MainUI.STAGE_WIDTH;
			view.height = MainUI.STAGE_HEIGHT;
			
			tutorial_mc = MovieClipFactory.CreateBlendingSoundsInWordsTutorial();
			tutorial_mc.stop();
			view.addChild(tutorial_mc);
			
			tutorial_mc.addFrameScript(49, sayTheseBirds);
			tutorial_mc.addFrameScript (52, createAssets);
			tutorial_mc.addFrameScript(114, glowOnPen);
			tutorial_mc.addFrameScript(147, glowOffPen);
			tutorial_mc.addFrameScript(283, singE);
			tutorial_mc.addFrameScript (286, glowOnMiddleBird);
			tutorial_mc.addFrameScript (309, glowOffMiddleBird);
			
			tutorial_mc.addFrameScript(331, singN);
			tutorial_mc.addFrameScript (332, glowOnRightBird);
			tutorial_mc.addFrameScript (354, glowOffRightBird);
			
			tutorial_mc.addFrameScript (385, watchClosely);
			
			tutorial_mc.addFrameScript(632, glowOnPen);
			tutorial_mc.addFrameScript(651, glowOffPen);
			
			tutorial_mc.addFrameScript (678, sayD);
			tutorial_mc.addFrameScript (679, glowOnAnswerBird_Left);
			tutorial_mc.addFrameScript (687, glowOffAnswerBird_Left);

			tutorial_mc.addFrameScript (730, singP);
			tutorial_mc.addFrameScript (731, glowOnAnswerBird_Right);
			tutorial_mc.addFrameScript (740, glowOffAnswerBird_Right);

			tutorial_mc.addFrameScript (808, dingNoise);
			
			tutorial_mc.addFrameScript (830, sayPen);
			
			tutorial_mc.addFrameScript (882, glowOnPen);
			tutorial_mc.addFrameScript(901, glowOffPen);
			
			tutorial_mc.addFrameScript (971, singP);
			tutorial_mc.addFrameScript (1007, singE);
			tutorial_mc.addFrameScript (1046, singN);

			tutorial_mc.addFrameScript (971, glowOnLeftBird);
			tutorial_mc.addFrameScript (981, glowOffLeftBird);


			tutorial_mc.addFrameScript (1010, glowOnMiddleBird_2);
			tutorial_mc.addFrameScript (1019, glowOffMiddleBird);

			tutorial_mc.addFrameScript (1047, glowOnRightBird_2);
			tutorial_mc.addFrameScript (1059, glowOffRightBird);
			
			tutorial_mc.addFrameScript (1082, nowYouTry);
			
			tutorial_mc.addFrameScript (1140, endGlowAll);
			tutorial_mc.addFrameScript (1145, tutorialComplete);
			
			glows = new List<FilterMovieClip>();
			
			
		}
		
		public void OnStart()
		{
			view.dispatchEvent(new TutorialEvent(TutorialEvent.START));
			tutorial_mc.gotoAndPlay (1);
		}


		
		public void Dispose()
		{
			tutorial_mc.stop();


			clearFMCs ();

		}

		private void Update() {
			if (armFMC != null) {
				DebugConsole.Log("Updating");
				armFMC.x = armMC.x;
				armFMC.y = armMC.y;
			}
		}

		void clearFMCs() {
			leftBird = removeFMC (leftBird);
			middleBird = removeFMC (middleBird);
			rightBird = removeFMC (rightBird);
			answerBird_Left = removeFMC (answerBird_Left);
			answerBird_Right = removeFMC (answerBird_Right);
			
			marqueeSign = removeFMC (marqueeSign);
			penSign = removeFMC (penSign);

			if (tutorial_mc_2 != null) {
				view.removeChild(tutorial_mc_2);
			}
		}

		FilterMovieClip removeFMC(FilterMovieClip fmcRef) {
			if (fmcRef != null) {
				if( fmcRef.parent != null )
					fmcRef.parent.removeChild( fmcRef );
				fmcRef.Destroy();
				fmcRef = null;
			}

			return null;
		}
		
		private void tutorialComplete (CEvent e) {
			
			//removeBirds ();
			clearFMCs ();
			OnComplete ();
		}
		
		void removeBirds() {
			MovieClip middleBird_mc = tutorial_mc.getChildAt<MovieClip> (4);
			middleBird_mc.removeChild (middleBird);
			
			MovieClip rightBird_mc = tutorial_mc.getChildAt<MovieClip> (5);
			rightBird_mc.removeChild (rightBird);
		}
		
		public void OnComplete()	
		{
			DebugConsole.Log ("OnComplete");
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
			tutorial_mc.stop();
		}
		
		public void Resize()
		{
			Resize (Screen.width, Screen.height);
		}
		
		public void Resize( int width, int height ) 
		{
			Resize ((float)width, (float)height);
		}
		
		public void Resize( float width, float height ) 
		{
			float scale = parentRef.getBaseScale ();
			view.scaleX = view.scaleY = scale;
			//view.y = Screen.height - ( scale * height );
			
			float xOffset = 0f;
			float yOffset = Screen.height - ( scale * height );
			
			Platforms selectedPlatform = PlatformUtils.GetPlatform ();
			//selectedPlatform = Platforms.ANDROID;
			// Center this
			
			switch (selectedPlatform) {
			case Platforms.ANDROID:
				scale += 0.01f;
				view.scaleX = view.scaleY = scale;
				yOffset = Screen.height - ( scale * height );
				
				xOffset = 120f;
				yOffset += 30f;
				break;
				
			case Platforms.WIN:
				scale = 0.8f;
				view.scaleX = view.scaleY = scale;
				xOffset = 120f;
				yOffset = Screen.height - ( view.height );
				yOffset -= 50f;
				break;
			}
			
			
			view.x = xOffset;
			view.y = yOffset;
		}
		
		private void playSound ( string clipName ){ playSound(clipName, ""); }
		private void playSound ( string clipName, string word )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.BLENDING_SOUNDS_IN_WORDS, clipName );
			
			if(word != "")
				bundle.AddClip(clip, word, clip.length);
			else
				bundle.AddClip(clip);
			
			SoundEngine.Instance.PlayBundle(bundle);
		}

		private void createAssets (CEvent e) {

			middleBird = createBird (4, 1, true);
			rightBird = createBird (5, 2, true);

			//createSign ();
			penSign = addFilter ((MovieClip) tutorial_mc.getChildAt (3));
			penSign.alpha = 0f;

			marqueeSign = addFilter ((MovieClip) tutorial_mc.getChildAt (2));
			marqueeSign.alpha = 0f;

			//createArm ();

		}

		private FilterMovieClip createBird(int childNo, int birdNo, bool stageBird) {
			MovieClip mc = tutorial_mc.getChildAt<MovieClip> (childNo);
			//DebugConsole.Log ("Adding middle bird");
			FilterMovieClip birdRef;

			if (stageBird) {
				birdRef = parentRef.createBird (birdNo);
			} else {
				birdRef = parentRef.createAnswerBird();
			}

			//mc.addChildAt (birdRef, 0);
			view.addChild (birdRef);
			//tutorial_mc.addChildAt (birdRef, 7);

			birdRef.x = mc.x;
			birdRef.y = mc.y;
			birdRef.scaleX = mc.scaleX;
			birdRef.scaleY = mc.scaleY;
			
			addFilterGroup (birdRef.Target);
			birdRef.alpha = 0f;

			return birdRef;
		}

		private void createArm() {
			armMC = tutorial_mc.getChildAt<MovieClip> (8);
			armFMC = new FilterMovieClip( armMC );

			view.addChild (armFMC);

			armFMC.x = -armMC.x;
			armFMC.y = -armMC.y;
			armFMC.scaleX = armMC.scaleX;
			armFMC.scaleY = armMC.scaleY;

			DebugConsole.Log ("Arm: " + armFMC.x + " " + armFMC.y);

		}

		private FilterMovieClip createSign() {

			MovieClip mc = tutorial_mc.getChildAt<MovieClip> (2);

			marqueeSign = parentRef.createSign ();
			marqueeSign.Target.gotoAndStop ("PEN");

			view.addChild (marqueeSign);

			marqueeSign.x = mc.x;
			marqueeSign.y = mc.y;
			marqueeSign.scaleX = mc.scaleX;
			marqueeSign.scaleY = mc.scaleY;

			addFilterGroup (marqueeSign.Target);

			return marqueeSign;

		}
		
		private void sayTheseBirds (CEvent e) {
			playSound(audioFolder_SkillInstruction + audioPrecursor + "these-birds-want-to-sing-pen");
		}
		
		private void sayE( CEvent e ) {
			playSound (audioFolder_SkillInstruction_BirdSounds + audioPrecursor + "e");
		}
		
		private void sayN( CEvent e ) {
			playSound (audioFolder_SkillInstruction_BirdSounds + audioPrecursor + "n");
		}
		
		private void sayD( CEvent e ) {
			playSound (audioFolder_ReinforcementWordSounds + audioPrecursor + "dog-d");
			//playSound (audioFolder_SkillInstruction_BirdSounds + audioPrecursor + "d");
		}
		
		private void sayP( CEvent e ) {
			playSound (audioFolder_SkillInstruction_BirdSounds + audioPrecursor + "p");
		}
		
		private void dingNoise (CEvent e) {
			var clip = SoundUtils.LoadGlobalSound("ding");
			var bundle = new SoundEngine.SoundBundle();
			bundle.AddClip(clip);
			SoundEngine.Instance.PlayBundle(bundle);
		}
		
		private void sayPen( CEvent e ) {
			playSound (audioFolder_Reinforcement + audioPrecursor + "first-pen-p");
		}
		
		private void singP( CEvent e ) {
			playSound (audioFolder_ReinforcementWordSounds + audioPrecursor + "pen-p");
		}
		
		private void singE( CEvent e ) {
			playSound (audioFolder_ReinforcementWordSounds + audioPrecursor + "pen-e");
		}
		
		private void singN( CEvent e ) {
			playSound (audioFolder_ReinforcementWordSounds + audioPrecursor + "pen-n");
		}
		
		private void nowYouTry( CEvent e ) {
			playSound (audioFolder_SkillInstruction + audioPrecursor + "now-you-try");
		}
		
		
		private void watchClosely (CEvent e ) {
			playSound (audioFolder_SkillInstruction + audioPrecursor + "watch-closely-as-henry-finds");
			
		}
		
		private void glowOnMiddleBird( CEvent e ) {
			//createMiddleBird ();
			//addFilterGroup (middleBird.Target);
			middleBird.Target.gotoAndStop ("STAGEBIRDMID3OPEN");
			middleBird.alpha = 1f;
		}
		
		private void glowOffMiddleBird( CEvent e ) {
			//glowOff (middleBird);

			//clearFilters ();
			middleBird.alpha = 0f;
		}

		private void glowOnLeftBird( CEvent e) {
			leftBird = createBird(8, 0, true);
			leftBird.Target.gotoAndStop ("STAGEBIRDLEFT2GREENOPEN");
			leftBird.alpha = 1f;
			addFilterGroup (leftBird.Target);
		}

		private void glowOffLeftBird( CEvent e) {
			leftBird.alpha = 0f;

		}

		private void glowOnMiddleBird_2( CEvent e){ 
			middleBird.Target.gotoAndStop ("STAGEBIRDMIDGREEN3OPEN");
			clearFilters ();
			middleBird.alpha = 1f;
			addFilterGroup (middleBird.Target);
		}
		
		private void glowOnRightBird( CEvent e ) {
			//createRightBird ();
			//addFilterGroup (rightBird.Target);
			rightBird.Target.gotoAndStop ("STAGEBIRDRIGHT1OPEN");
			rightBird.alpha = 1f;
		}
		
		private void glowOffRightBird( CEvent e ) {
			//glowOff (rightBird);
			//clearFilters ();
			rightBird.alpha = 0f;
		}

		private void glowOnRightBird_2( CEvent e){ 
			rightBird.Target.gotoAndStop ("STAGEBIRDRIGHTGREEN1OPEN");
			clearFilters ();
			rightBird.alpha = 1f;
			addFilterGroup (rightBird.Target);
		}

		private void glowOnAnswerBird_Left(CEvent e) {
			answerBird_Left = createBird (7, 0, false);
			answerBird_Left.alpha = 1f;


			createTutorialOverlay (679);

			for (int counter = 679; counter < 687; counter++) {
				tutorial_mc_2.addFrameScript (counter, hideStage);
			}

		}

		private void createTutorialOverlay(int frameNo) {
			tutorial_mc_2 = MovieClipFactory.CreateBlendingSoundsInWordsTutorial();
			tutorial_mc_2.gotoAndPlay(frameNo);
			view.addChild(tutorial_mc_2);
		}

		private void hideStage(CEvent e) {
			for (int counter = 0; counter < 5; counter++) {
				tutorial_mc_2.getChildAt (counter).alpha = 0f;

			}
		}

		private void glowOffAnswerBird_Left(CEvent e) {
			answerBird_Left.alpha = 0f;

			view.removeChild(tutorial_mc_2);
		}

		private void glowOnAnswerBird_Right (CEvent e) {
			answerBird_Right = createBird (8, 0, false);
			answerBird_Right.alpha = 1f;
			//answerBird_Right.alpha = 1f;
			createTutorialOverlay (731);

			for (int counter = 731; counter < 740; counter++) {
				tutorial_mc_2.addFrameScript (counter, hideStage);
			}
		}

		private void glowOffAnswerBird_Right(CEvent e) {
			answerBird_Right.alpha = 0f;

			view.removeChild (tutorial_mc_2);
		}


		private void glowOnPen ( CEvent e ) {
			//pen.filters [0].Visible = true;

			marqueeSign.alpha = 1f;
			//penSign.alpha = 1f;
		}
		
		private void glowOffPen ( CEvent e ) {

			marqueeSign.alpha = 0f;
			//penSign.alpha = 0f;

		}
		
		private void glowOn( FilterMovieClip fmc )
		{
			/*
			fmc.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
			//addFilter (fmc);
			
			//fmc.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
			fmc.filters[0].Visible = true;
			*/
		}
		
		private void glowOff( FilterMovieClip fmc )
		{
			//fmc.RemoveFilter(fmc.filters[0]);
		}

		private FilterMovieClip addFilter( MovieClip mc )
		{
			DisplayObjectContainer mcParent = mc.parent;
			FilterMovieClip fmc = new FilterMovieClip( mc );
			fmc.removeChild( mc );
			mcParent.addChild( mc );
			mc.addChildAt( fmc, 0 );
			fmc.x -= mc.x;
			fmc.y -= mc.y;
			glows.Add( fmc );
			fmc.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
			fmc.alpha = 0f;
			return fmc;
		}

		private FilterMovieClip addFilterGroup ( MovieClip mc) {

			if( glows == null )
				glows = new List<FilterMovieClip>();
			
			DisplayObjectContainer _parent = mc.parent;
			FilterGroupMovieClip fmc = new FilterGroupMovieClip( mc );
			fmc.AddFilter( FiltersFactory.GetScaledYellowGlowFilter );
			fmc.removeChild( mc );
			_parent.addChild( mc );
			_parent.addChildAt( fmc, _parent.numChildren - 1 );
			glows.Add( fmc );

			return fmc;
		}

		private void clearFilters()
		{
			if( glows == null )
				return;
			
			foreach( FilterMovieClip fmc in glows )
			{
				if( fmc.parent != null )
					fmc.parent.removeChild( fmc );
				fmc.Destroy();
			}
			glows.Clear();
		}

		/*
		private FilterMovieClip addFilter( MovieClip mc )
		{
			//DisplayObjectContainer _parent = mc.parent;
			DisplayObjectContainer mcParent = mc.parent;
			FilterMovieClip fmc = new FilterMovieClip( mc );
			//fmc.AddFilter( new GlowMovieClipFilter(50, 2, 0.05f, Color.yellow, 1.1f) );
			
			fmc.removeChild( mc );
			mcParent.addChild( mc );
			mc.addChildAt( fmc, 0 );

			//_parent.addChild( mc );
			//_parent.addChildAt( fmc, _parent.numChildren - 1 );

			fmc.x -= mc.x;
			fmc.y -= mc.y;
			
			
			glows.Add( fmc );
			fmc.AddFilter( FiltersFactory.GetScaledYellowGlowFilter() );
			fmc.filters [0].Visible = false;
			
			return fmc;
		}*/
		
		public void endGlowAll( CEvent e )
		{
			clearFilters ();
		}

	}
	
}
