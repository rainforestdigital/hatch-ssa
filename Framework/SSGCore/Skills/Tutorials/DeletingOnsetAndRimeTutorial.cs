using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;
using pumpkin.geom;

namespace SSGCore
{
	public class DeletingOnsetAndRimeTutorial : Tutorial
	{
		private DisplayObjectContainer view;
		public DisplayObject View
		{
			get{ return view; }
		}
		private MovieClip tutorial_mc;

		public DeletingOnsetAndRime parentRef;

		private string audioPrecursor = "1F-";
		private string audioFolder_Reinforcement 		= "Narration/1F-Reinforcement Audio/";
		private string audioFolder_SkillInstruction 	= "Narration/1F-Skill Instructions Audio/";
//		private string audioFolder_InitialInstructions 	= "Narration/1F-Skill Instructions Audio/1F-Initial Instructions/";
		private string audioFolder_Onset 				= "Narration/1F-Skill Instructions Audio/1F-Onset Veggie Sounds/";
//		private string audioFolder_Rime 				= "Narration/1F-Skill Instructions Audio/1F-Rime Veggies Sounds/";

		private FilterMovieClip leftVeg;
		private FilterMovieClip rightVeg;
		private FilterMovieClip targetVeg;

		private FilterMovieClip sign;

		float scale = 1f;

		public DeletingOnsetAndRimeTutorial ()
		{
			view = new DisplayObjectContainer();
			view.width = MainUI.STAGE_WIDTH;
			view.height = MainUI.STAGE_HEIGHT;

			tutorial_mc = MovieClipFactory.CreateDeletingOnsetAndRimeTutorial();
			tutorial_mc.stop();
			view.addChild(tutorial_mc);

			tutorial_mc.addFrameScript(142, bedGlowOn);
			tutorial_mc.addFrameScript(291, bedGlowOff);
			tutorial_mc.addFrameScript(401, sayLeftVeg);
			tutorial_mc.addFrameScript(409, leftGlowOn);
			tutorial_mc.addFrameScript(476, leftGlowOff);
			tutorial_mc.addFrameScript(520, sayRightVeg);
			tutorial_mc.addFrameScript(527, rightGlowOn);
			tutorial_mc.addFrameScript(600, describeRightVeg);
			tutorial_mc.addFrameScript(745, rightGlowOff);

			tutorial_mc.addFrameScript (776, repositionTargetVeg);
			tutorial_mc.addFrameScript(777, playDing);
			tutorial_mc.addFrameScript(780, revealVeg);
			tutorial_mc.addFrameScript(802, reinforcementAudio);
			tutorial_mc.addFrameScript(803, bedGlowOn);
			tutorial_mc.addFrameScript(901, bedGlowOff);

			tutorial_mc.addFrameScript(901, targetVegetable_mouthOpen);
			tutorial_mc.addFrameScript(912, targetVegetable_mouthClosed);

			tutorial_mc.addFrameScript(926, yourTurn);
			tutorial_mc.addFrameScript(980, tutorialComplete);



		}

		public void OnStart()
		{
			view.dispatchEvent(new TutorialEvent(TutorialEvent.START));
			
			tutorial_mc.gotoAndPlay (1);//.play();
			
			playSound(audioFolder_SkillInstruction + audioPrecursor + "listen-which-vegetable");

			MovieClip leftVeg_mc = tutorial_mc.getChildAt<MovieClip> (3);
			leftVeg = createVegetable("Radish1".ToUpper());
			leftVeg_mc.addChildAt (leftVeg, 0);

			MovieClip rightVeg_mc = tutorial_mc.getChildAt<MovieClip> (5);
			rightVeg = createVegetable("Radish1".ToUpper());
			rightVeg_mc.addChildAt (rightVeg, 0);

			targetVeg = createVegetable("Radish1".ToUpper());
			view.addChild (targetVeg);
			targetVeg.scaleX = targetVeg.scaleY = scale += .1f;
			targetVeg.alpha = 0f;

			MovieClip sign_mc = tutorial_mc.getChildAt<MovieClip> (0);
			sign = generateSign ("cat");
			sign_mc.addChildAt (sign, 0);

			Platforms selectedPlatform = PlatformUtils.GetPlatform ();
			if ((selectedPlatform == Platforms.WIN) || (selectedPlatform == Platforms.IOSRETINA)) {
				sign.scaleX = sign.scaleY = 0.7f;
			}
			//sign.alpha = 1f;
		}

		public void Dispose()
		{
			if (targetVeg != null) {
				view.removeChild( targetVeg );
				targetVeg.Destroy();
				targetVeg = null;
			}

			if( leftVeg != null ) {
				if( leftVeg.parent != null )
					leftVeg.parent.removeChild( leftVeg );
				leftVeg.Destroy();
				leftVeg = null;
			}
			
			if( rightVeg != null ) {
				if( rightVeg.parent != null )
					rightVeg.parent.removeChild( rightVeg );
				rightVeg.Destroy();
				rightVeg = null;
			}

			if (sign != null) {
				if( sign.parent != null )
					sign.parent.removeChild( sign );
				sign.Destroy();
				sign = null;
			}
				
			tutorial_mc.stop();
		}

		public void OnComplete()	
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
			tutorial_mc.stop();
		}

		private void playSound ( string clipName ){ playSound(clipName, ""); }
		private void playSound ( string clipName, string word )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.DELETING_ONSET_AND_RIME, clipName );
			
			if(word != "")
				bundle.AddClip(clip, word, clip.length);
			else
				bundle.AddClip(clip);
			
			SoundEngine.Instance.PlayBundle(bundle);
		}

		public void Resize()
		{
			Resize (Screen.width, Screen.height);
		}

		public void Resize( int width, int height ) 
		{
			Resize ((float)width, (float)height);
		}

		public void Resize( float width, float height ) 
		{
			scale = parentRef.getBaseScale ();
			view.scaleX = view.scaleY = scale;
			//view.y = Screen.height - ( scale * height );

			float xOffset = 0f;
			float yOffset = Screen.height - ( scale * height );

			Platforms selectedPlatform = PlatformUtils.GetPlatform ();
			//selectedPlatform = Platforms.ANDROID;

			// Center this
			switch (selectedPlatform) {
			case Platforms.WIN:
				view.scaleX = view.scaleY = 0.92f;
				xOffset= 5f;
				yOffset = -156f;
				break;

			case Platforms.ANDROID:
				xOffset = 130f;
				yOffset += 13f;
				break;
			}

			view.x = xOffset;
			view.y = yOffset;
			/*
			view.scaleY = height / (float)MainUI.STAGE_HEIGHT;
			view.scaleX = view.scaleY;*/
		}

		private void sayLeftVeg( CEvent e ) {
			playSound (audioFolder_Onset + audioPrecursor + "c");
		}

		private void sayRightVeg( CEvent e ) {
			playSound (audioFolder_Onset + audioPrecursor + "b");
		}

		private void describeRightVeg( CEvent e ) {
			playSound (audioFolder_SkillInstruction + audioPrecursor + "this-vegetable-says-b");
		}

		private void playDing( CEvent e ) {
			SoundUtils.PlaySimpleEffect("ding");
		}

		private void repositionTargetVeg(CEvent e) {
			MovieClip rightVeg_mc = (MovieClip) tutorial_mc.getChildAt (5);
			targetVeg.x = rightVeg_mc.x;
			targetVeg.y = rightVeg_mc.y;
		}

		private void revealVeg (CEvent e) {
			targetVeg.alpha = 1f;
		}

		private void reinforcementAudio( CEvent e ) {
			playSound (audioFolder_Reinforcement + audioPrecursor + "bed-without-ed-is-b");
		}

		private void yourTurn( CEvent e ) {
			playSound (audioFolder_SkillInstruction + audioPrecursor + "now-you-try");
		}

		private void leftGlowOn( CEvent e ) {

			glowOn (leftVeg);
		}

		private void leftGlowOff( CEvent e ) {
			glowOff(leftVeg);
		}

		private void rightGlowOn( CEvent e ) {
			glowOn (rightVeg);
		}
		
		private void rightGlowOff( CEvent e ) {
			glowOff(rightVeg);
			SoundUtils.PlaySimpleEffect("click");
		}

		private void bedGlowOn( CEvent e ) {
			sign.alpha = 1f;
			glowOn (sign);
			//glowOn (bed);
		}

		private void bedGlowOff( CEvent e ) {
			sign.alpha = 0f;
			glowOff (sign);
			//glowOff(bed);
		}

		private void targetVegetable_mouthOpen( CEvent e ) {
			targetVeg.Target.gotoAndStop ("RADISH2");
			glowOn (targetVeg);
		}

		private void targetVegetable_mouthClosed( CEvent e ) {
			targetVeg.Target.gotoAndStop ("RADISH1");
			glowOff (targetVeg);
		}

		private void tutorialComplete (CEvent e) {
			view.removeChild (targetVeg);
			targetVeg = null;
			OnComplete ();
		}

		private void glowOn( FilterMovieClip fmc )
		{
			fmc.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
			fmc.filters[0].Visible = true;
		}

		private void glowOff( FilterMovieClip fmc )
		{

			fmc.RemoveFilter(fmc.filters[0]);
			fmc.Destroy();
		}

		private FilterMovieClip createVegetable(string vegetableName) {
			FilterMovieClip fmc = new FilterMovieClip(MovieClipFactory.CreateDeletingOnsetAndRimeVegetable());
			DebugConsole.Log (vegetableName);
			fmc.Target.gotoAndStop (vegetableName);
			fmc.scaleX = fmc.scaleY = 0.7f;
			return fmc;
		}

		private FilterMovieClip generateSign(string displayImage) {
			FilterMovieClip fmc = new FilterMovieClip (MovieClipFactory.CreateDeletingOnsetAndRimeSign ());
			fmc.scaleX = fmc.scaleY = 1f;
			fmc.Target.gotoAndStop (displayImage.ToUpper());
			return fmc;
		}

	}
}