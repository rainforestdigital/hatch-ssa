using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;
using pumpkin.geom;

namespace SSGCore
{
	public class SubtractionTutorial : Tutorial
	{
		private DisplayObjectContainer view;
		private MovieClip tutorial_mc;
		
		public SubtractionDragger[] bears;
		public SubtractionHouse house;

		private const string basePath = "Narration/";
		private const string inactivityReinforcementPath = "2F-Inactivity Reinforcement Audio/";
		private const string reinforcementPath = "2F-Reinforcement Audio/";
		private const string skillInstructionPath = "2F-Skill Instruction Audio/";
		private const string storyIntroductionPath = "2F-Story Introduction Audio/";

//		private Platforms selectedPlatform;
//		private float storedScale;
		//private float dragXOffset = 60f;
		//private float dragYOffset = 0f;
		public SubtractionTutorial ()
		{
			Init(1f);
		}
		
		public SubtractionTutorial( float baseScale )
		{
			Init(baseScale);
		}
		
		private void Init ( float houseScale )
		{
			view = new DisplayObjectContainer();
			view.width = MainUI.STAGE_WIDTH;
			view.height = MainUI.STAGE_HEIGHT;
			
			tutorial_mc = MovieClipFactory.CreateSubtractionTutorial();
			tutorial_mc.gotoAndStop(1);
		//	tutorial_mc.width = MainUI.STAGE_WIDTH;
		//	tutorial_mc.height = MainUI.STAGE_HEIGHT;

			/*
			house = new SubtractionHouse("EG");
			house.scaleX = house.scaleY = houseScale;
			house.x = (PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA) ? 34 : 250;//Mathf.CeilToInt(0.0385f * thisScreenWidth);//74;
			house.y = 68; // Win only?
			*/

			bears = new SubtractionDragger[5];
			
			SubtractionDragger tbear;
			MovieClip bear;
			for(int i = 0; i < 5; i++)
			{
				bear = tutorial_mc.getChildByName<MovieClip>("bear" + (i+1) + "_mc");
				bear.gotoAndStop(1);
				bear = bear.getChildByName<MovieClip>("bear_mc");
				bear.gotoAndStop(2);
				tbear = new SubtractionDragger("EG", 3f/12f);
				bear.addChildAt(tbear, 0);
				tbear.Target.alpha = 0;
				//tbear.x *= scale;
				//tbear.y *= scale;
				bears[i] = tbear;
			}
//			view.removeChild(bears[0]);
//			view.addChild(bears[0]);

			tutorial_mc.addFrameScript (10, tutorialJump);
			tutorial_mc.addFrameScript(127, tutorialContinue);
			tutorial_mc.addFrameScript (200, tutorialMove);
			tutorial_mc.addFrameScript(382, clickSound);
			tutorial_mc.addFrameScript(437, plattyMoved);
			tutorial_mc.addFrameScript(469, doorGlowOn);
			tutorial_mc.addFrameScript(533, clickSound);
			tutorial_mc.addFrameScript(535, doorClick);
			tutorial_mc.addFrameScript(537, doorGlowOff);
			tutorial_mc.addFrameScript(577, plattyWrapUp);
			tutorial_mc.addFrameScript(582, glowAll);
			tutorial_mc.addFrameScript(612, glowAllOff);
			tutorial_mc.addFrameScript(655, glowOne);
			tutorial_mc.addFrameScript(717, glowOneOff);
			tutorial_mc.addFrameScript(772, glowOthers);
			tutorial_mc.addFrameScript(850, glowOthersOff);
			tutorial_mc.addFrameScript(900, tutorialComplete);
			//tutorial_mc.addEventListener(CEvent.ENTER_FRAME, eachFrame);

//			storedScale = houseScale;
//			selectedPlatform = PlatformUtils.GetPlatform ();
			//selectedPlatform = Platforms.ANDROID;


			/*
			dragXOffset = 0f;

			if (selectedPlatform == Platforms.IOS || selectedPlatform == Platforms.IOSRETINA)
            {
                dragXOffset = 60f;

                if (selectedPlatform == Platforms.IOSRETINA)
                {
                    dragXOffset = 200f;
                    dragYOffset = 150f;
                }

            } else if (selectedPlatform == Platforms.ANDROID)
            {
                dragXOffset = 80f;
            } else if (selectedPlatform == Platforms.WIN)
            {
                dragYOffset = 50f;
            }
			*/
		}

		public void setHouse (SubtractionHouse _house) {
			house = _house;
		}
		
		public void OnStart ()
		{

			//house.x *= tutorial_mc.scaleX;
			//house.y *= tutorial_mc.scaleY;
			//house.scaleX = house.scaleY *= tutorial_mc.scaleY;

			view.dispatchEvent( new TutorialEvent(TutorialEvent.START) );
			
			house.alpha = 0;
			Tweener.addTween( house, Tweener.Hash("time", 0.35f, "alpha", 1f) );
			view.addChildAt(house, 0);

			tutorial_mc.x = Screen.width * 0.5f;
			tutorial_mc.y = Screen.height * 0.5f;

			tutorial_mc.scaleX = tutorial_mc.scaleY = house.scaleX;

			view.addChild(tutorial_mc);
			
			tutorial_mc.play();
		
		}
		
		private void eachFrame( CEvent e )
		{


			/*
			SubtractionDragger d;
			MovieClip bear;
			Rectangle box;
			for(int i = 0; i < 5; i++)
			{
				d = bears[i];
				bear = tutorial_mc.getChildByName<MovieClip>("bear" + (i+1) + "_mc");
				box = bear.getBounds(view);

				float newX = box.x - ((box.width/3) * 2);
				float newY = box.y - (box.height/3) * 5;

				newX += dragXOffset; // 60f
				newY += dragYOffset;

				d.x = newX;
				d.y = newY;
				//d.scaleX = d.scaleY = box.width/d.Target.width;
				d.scaleX = d.scaleY = storedScale;
				d.alpha = bear.alpha;
				bear.alpha = 0;
			}
			*/
			/*
			tutorial_mc.x = Screen.width * 0.5f;
			tutorial_mc.y = Screen.height * 0.5f;

			switch (selectedPlatform) {
			case Platforms.IOS:
				tutorial_mc.x = -300f;
				tutorial_mc.y = -170f;
				break;

			case Platforms.IOSRETINA:
				tutorial_mc.x = -200f;
				tutorial_mc.y = 0f;
				break;

			case Platforms.ANDROID:
				tutorial_mc.x = -50f;
				tutorial_mc.y = 40f;
				break;

                case Platforms.WIN:
                    //tutorial_mc.x = -50f;
                    tutorial_mc.y = -50f;
                    break;
			}
			*/

		}

		private void tutorialJump( CEvent e)
		{
			tutorial_mc.gotoAndPlay (120);
		}

		private void tutorialMove(CEvent e)
		{
			tutorial_mc.gotoAndPlay (280);
		}

		private void tutorialContinue( CEvent e )
		{
			playSound (basePath + skillInstructionPath + "2F-Henry-please-move-one-starfish" );
		}
		
		private void clickSound ( CEvent e )
		{
			SoundUtils.PlaySimpleEffect("click");
		}
		
		private void plattyMoved( CEvent e )
		{
			playSound (basePath + skillInstructionPath + "2F-when-youre-done-touch-the-switch");
			
			tutorial_mc.stop();
			TimerUtils.SetTimeout( 3f, () => { if( view.parent != null ) tutorial_mc.gotoAndPlay(447); } );
		}
		
		private void doorGlowOn( CEvent e )
		{
			house.glowOn();
			tutorial_mc.gotoAndPlay(530);
		}
		
		private void doorClick( CEvent e )
		{
			house.closeDoor();
			house.glowOn();
			SoundUtils.PlaySimpleEffect("ding");
		}
		
		private void doorGlowOff( CEvent e )
		{
			house.glowOff();
		}
		
		private void plattyWrapUp( CEvent e )
		{
			playSound (basePath + skillInstructionPath + "2F-there-were-5-starfish-on-the-submarine");
		}
		
		private void glowAll( CEvent e )
		{
			for(int i = 0; i < 5; i++)
			{
				bears[i].glowOn();
			}
		}
		
		private void glowAllOff( CEvent e )
		{
			for(int i = 0; i < 5; i++)
			{
				bears[i].glowOff();
			}
		}
		
		private void glowOne( CEvent e )
		{
			bears[0].glowOn();
		}
		
		private void glowOneOff( CEvent e )
		{
			bears[0].glowOff();
		}
		
		private void glowOthers( CEvent e )
		{
			for(int i = 1; i < 5; i++)
			{
				bears[i].glowOn();
			}
		}
		
		private void glowOthersOff( CEvent e )
		{
			for(int i = 1; i < 5; i++)
			{
				bears[i].Destroy();
				bears[i].glowOff();
			}
		}
		
		private void tutorialComplete( CEvent e )
		{
			OnComplete();
		}
		
		public void OnComplete()	
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
		}

		private void playSound ( string clipName ) { playSound(clipName, ""); }
		private void playSound ( string clipName, string word )
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.SUBTRACTION, clipName );
			
			if(word != "")
				bundle.AddClip(clip, word, clip.length);
			else
				bundle.AddClip(clip);
			
			SoundEngine.Instance.PlayBundle(bundle);
		}
		/*
		public void setScale (float animalScale) {
			storedScale = animalScale;
		}
		*/
		public void Resize( int width, int height)
		{

			view.width = width;
			view.height = height;

			//tutorial_mc.FitToParent();
			/*
			tutorial_mc.scaleY = (float)height/(float)MainUI.STAGE_HEIGHT;

			if (selectedPlatform == Platforms.IOS) {
				tutorial_mc.scaleY += 0.1f;
			} else if (selectedPlatform == Platforms.IOSRETINA ) {
				tutorial_mc.scaleY -= 0.4f;
			} else if (selectedPlatform == Platforms.ANDROID) {
				tutorial_mc.scaleY -= 0.1f;
			}

			tutorial_mc.scaleX = tutorial_mc.scaleY;
			tutorial_mc.y = 0;
			tutorial_mc.x = ((width - (MainUI.STAGE_WIDTH*tutorial_mc.scaleX))/2);
			*/
			//house.x += (tutorial_mc.x / tutorial_mc.scaleX);
		}
		
		public DisplayObject View
		{
			get{ return view; }
		}


	}
}