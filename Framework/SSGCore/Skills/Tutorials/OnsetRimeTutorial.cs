using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using HatchFramework;

using pumpkin;
using pumpkin.display;
using pumpkin.tweener;
using pumpkin.events;
using pumpkin.geom;

namespace SSGCore
{
	public class OnsetRimeTutorial : Tutorial
	{
		private DisplayObjectContainer view;
		public DisplayObject View
		{
			get{ return view; }
		}
		private MovieClip tutorial_mc;
		public FilterMovieClip ear;
		public FilterMovieClip backCard;
		
		int stallCount = 190;
		
		public OnsetRimeTutorial ()
		{
			view = new DisplayObjectContainer();
			view.width = MainUI.STAGE_WIDTH;
			view.height = MainUI.STAGE_HEIGHT;
			
			Rect earRect;
			if( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA )
				earRect = new Rect( 858f, 369f, 629f, 0f );
			else
				earRect = new Rect( 431f, 185f, 314.5f, 0f );
			
			ear = new FilterMovieClip(MovieClipFactory.CreateOnsetRimeEar());
			ear.x = earRect.x;
			ear.y = earRect.y;
			float baseScale = earRect.width / ear.width;
			ear.scaleX = ear.scaleY = baseScale;
			view.addChild(ear);
			
			tutorial_mc = MovieClipFactory.CreateOnsetRimeTutorial();
			tutorial_mc.stop();
			view.addChild(tutorial_mc);
			
			backCard = new FilterMovieClip(MovieClipFactory.CreateOnsetRimeCard());
			backCard.Target.gotoAndStop("BAT");
			backCard.alpha = 0f;
			
			MovieClip follow = tutorial_mc.getChildByName<MovieClip>("card1_mc");
			follow.gotoAndStop("BAT");
			
			follow.addChildAt(backCard, 0);
			
			follow = tutorial_mc.getChildByName<MovieClip>("card2_mc");
			follow.gotoAndStop("BALL");
			
			tutorial_mc.addFrameScript(122, introContinue);
			tutorial_mc.addFrameScript(298, watchHim);
			tutorial_mc.addFrameScript(382, stall);
			tutorial_mc.addFrameScript(398, moving);
			tutorial_mc.addFrameScript(430, glow);
			tutorial_mc.addFrameScript(580, ding);
			tutorial_mc.addFrameScript(611, batBat);
			tutorial_mc.addFrameScript(765, frameComplete);
		}
		
		public void OnStart()
		{
			view.dispatchEvent(new TutorialEvent(TutorialEvent.START));
			
			tutorial_mc.gotoAndPlay (90);//.play();
			
			//playSound("Instruction/heres-how-to-play-the-game");
		}
		
		private void introContinue( CEvent e )
		{
			playSound("Instruction/using-their-ear-horns");
		}
		
		private void watchHim( CEvent e )
		{
			playSound("Instruction/Cami-which-picture");
		}
		
		private void stall( CEvent e )
		{
			if(stallCount > 0)
			{
				tutorial_mc.gotoAndPlay(tutorial_mc.currentFrame - 1);
				stallCount--;
			}
		}
		
		private void moving( CEvent e )
		{
			SoundUtils.PlaySimpleEffect("click");
			playSound("Instruction/the-bat-picture-goes-in-the-horn");
		}
		
		private void glow( CEvent e )
		{
			backCard.alpha = 1f;
			backCard.AddFilter(FiltersFactory.GetScaledYellowGlowFilter());
			backCard.filters[0].Visible = true;
		}
		
		private void ding( CEvent e )
		{
			SoundUtils.PlaySimpleEffect("ding");
		}
		
		private void batBat( CEvent e )
		{
			playSound("Ear/bat");
		}
		
		private void frameComplete( CEvent e )
		{
			OnComplete();
		}
		
		public void OnComplete()	
		{
			view.dispatchEvent( new TutorialEvent(TutorialEvent.COMPLETE) );
			tutorial_mc.stop();
		}
		
		private void playSound(string clipName)
		{
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();;
			AudioClip clip = SoundUtils.LoadGlobalSound( MovieClipFactory.ONSET_RIME, clipName );
			bundle.AddClip(clip);
			SoundEngine.Instance.PlayBundle(bundle);
		}
		
		public void Resize( int wide, int high)
		{
			Rect targ;
			if( PlatformUtils.GetPlatform() == Platforms.WIN || PlatformUtils.GetPlatform() == Platforms.IOSRETINA )
				targ = new Rect( -29.6f, 341.9f, 2048f, 1536f );
			else
				targ = new Rect( -271.8f, -120.85f, 1024f, 768f );
			
			view.scaleX = view.scaleY = (float)wide / (float)targ.width;
			view.y = ((high - (targ.height * view.scaleY)) / 2f);
			view.x = 0;
			
			tutorial_mc.x = targ.x;
			tutorial_mc.y = targ.y;
		}
	}
}