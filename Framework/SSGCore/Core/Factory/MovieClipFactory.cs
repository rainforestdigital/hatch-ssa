using System;
using HatchFramework;
using pumpkin.swf;
using pumpkin.display;
using pumpkin.text;
using UnityEngine;
using System.Collections.Generic;

namespace SSGCore
{
	public class MovieClipFactory
	{
		public static string PATH_MAIN = "Menu.swf";
		public static string PATH_SPLASH = "UI/SplashScreen_v1.swf";
		public static string PATH_NO_SKILLS_AVAILABLE = "NoSkillsAvailable.swf";
		public static string PATH_BACKGROUND = "UI/Background.swf";
		public static string PATH_UPDATE = "UI/UpdateView.swf";
		public static string GLOBAL = "GlobalAssets";
		public static string ADDITION = "Addition";
		public static string NUMERAL_RECOGNITION = "NumeralRecognition";
		public static string LETTER_RECOGNITION = "LetterRecognition";
		public static string INITIAL_SOUNDS = "InitialSounds";
		public static string COMMON_SHAPES = "CommonShapes";
		public static string SORTING = "Sorting";
		public static string SPATIAL_SKILLS = "SpatialSkills";
		public static string SAFARI_THEME = "SafariTheme";
		public static string TRAVEL_THEME = "TravelTheme";
		public static string GARDEN_THEME = "GardenTheme";
		public static string COUNTING_FOUNDATIONS = "CountingFoundations";
		public static string LANGUAGE_VOCABULARY = "LanguageVocabulary";
		public static string SENTENCE_SEGMENTING = "SentenceSegmenting";
		public static string SEGMENTING_COMPOUND_WORDS = "SegmentingCompoundWords";
		public static string BLENDING_COMPOUND_WORDS = "BlendingCompoundWords";
		public static string SUBTRACTION = "Subtraction";
		public static string ONSET_RIME = "OnsetRime";
		public static string DELETING_ONSET_AND_RIME = "DeletingOnsetAndRime";
		public static string SEQUENCE_COUNTING = "SequenceCounting";
		public static string OBJECTS_IN_A_SET = "ObjectsInASet";
		public static string PATTERNING = "Patterning";
		public static string MEASUREMENT = "Measurement";
		public static string ADDITIONUPTOTEN = "AdditionUpToTen";
		public static string BLENDING_SOUNDS_IN_WORDS = "BlendingSoundsInWords";
		public static string SUBTRACTIONFROMTEN = "SubtractionFromTen";
		public static string MUSEUM = "Museum";
		
		public static MovieClip CreateIntroAnimation()
		{
			return new MovieClip(PATH_SPLASH, "IntroAnimation");
		}
		
		public static MainUI CreateMainUI(DBObject db)
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			
			MainUI ui = new MainUI(db);
			
			return ui;	
		}
		
		public static DisplayObject CreateMainUISpinner()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			if( bundle.Contains(PATH_MAIN) )
			{
				
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			
				MovieClip mc = new MovieClip(PATH_MAIN, "Spinner");
				
				var clip = new DisplayObjectContainer();
				clip.width = Screen.width;
				clip.height = Screen.height;
				clip.addChild(CreateBlocker());
				clip.addChild(mc);
				mc.AlignCenteredToScreen();
				
				return clip;
			}
			
			return null;
		}
		
		public static MovieClip CreateTeacherModeBar()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			
			MovieClip ui = new MovieClip(PATH_MAIN, "TeacherMode");
			
			return ui;	
		}
		
		// leaving this in as an example of how to construct a method to return an asset
		public static Texture2D GetTestIOSFile()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			if( bundle.Contains("iOSTexture") )
			{
				return bundle.Load("iOSTexture") as Texture2D;
			}
			else return null;
		}
		
		
		public static MovieClip CreateEmptyMovieClip(){
		
			return new MovieClip(PATH_MAIN, "Empty");
			
		}
		
		public static MovieClip CreateMovieClip( string swf, string symbolname ){
		
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(swf);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( swf + ".swf", symbolname);
			
			return ui;
			
		}
		
		public static MovieClip CreateCharacterPopIn( string host ){
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "characterCompliments.swf", host.ToLower() + "compliment" );
			
			return ui;
		}
		
		public static ThemeView CreateThemeView( string swf, string symbolname ){
		
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(swf);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			
		
			ThemeView view = new ThemeView(swf + ".swf", symbolname);
			
			return view;
			
		}
		
		public static MovieClip GetMuseum(){
			
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(MUSEUM);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			
			string clipName;
			switch( MuseumDataObject.user.Profile )
			{
			case LanguageProfile.Spanish:
				clipName = "BACKGROUNDALLSPANISH";
				break;
			case LanguageProfile.SpanishLit:
				clipName = "BACKGROUNDDUALMATHENGLISH";
				break;
			case LanguageProfile.SpanishMath:
				clipName = "BACKGROUNDDUALMATHSPANISH";
				break;
			case LanguageProfile.English:
			default:
				clipName = "BACKGROUNDALLENGLISH";
				break;
			}
			
			MovieClip ui = new MovieClip( MUSEUM + ".swf" , clipName );
			
			return ui;
			
		}
		
		public static MovieClip GetArtifactShard( string s ) {
			
			string clipName = "ART" + s.ToUpperInvariant();
			
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(MUSEUM);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( MUSEUM + ".swf" , clipName );
			
			return ui;
		}
		
		public static MovieClip GetCloseArtifact( string s ) {
			
			string clipName = "CLOSE" + s.ToUpperInvariant();
			
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(MUSEUM);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( MUSEUM + ".swf" , clipName );
			
			return ui;
		}
		
		public static MovieClip GetEndAnim( string lvl ) {
			
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(MUSEUM);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			
			MovieClip ui = null;
			
			string clipName = "ANIM";
			switch( MuseumDataObject.user.Profile )
			{
			case LanguageProfile.Spanish:
				clipName += "ALLSPANISH";
				break;
			case LanguageProfile.SpanishLit:
				clipName += "DUALMATHENGLISH";
				break;
			case LanguageProfile.SpanishMath:
				clipName += "DUALLITENGLISH";
				break;
			case LanguageProfile.English:
			default:
				clipName += "ALLENGLISH";
				break;
			}
			
			if( lvl == "DD" )
				clipName += "DEVELOPED";
			else if ( lvl == "CD" )
				clipName += "COMPLETE";
			
			ui = new MovieClip( MUSEUM + ".swf" , clipName );
			
			return ui;
		}
		
		public static MovieClip GetSkillMC( string skill, string clipName ) {

			string swfName = skill;
			if(swfName == "SpatialSkills") swfName = "SpacialSkills";

			DebugConsole.Log( "Creating MC for " + clipName + " from " + swfName + ".swf" );
			
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle( skill );
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( swfName + ".swf" , clipName );
			
			return ui;
		}
		
		public static DatePicker CreateDatePicker(){
		
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			DatePicker ui = new DatePicker( PATH_MAIN, "DatePicker", "DatePicker_Button" );
			
			return ui;
			
		}
		
		public static DropDown<T> CreateDropDown<T>( string buttonSymbolName, string listItemSymbolName, float itemHeight, int maxViewableItems, string[] listItems)
		{
			
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			return new DropDown<T>( PATH_MAIN, buttonSymbolName, listItemSymbolName, itemHeight);
		}
		
		public static DropDown<T> CreateEditChildDropDown<T>()
		{
			
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			return new DropDown<T>( PATH_MAIN, "EditChild_DropDown", "EditChild_DropDown_ListItem", 44.85f);
		}
		
		public static BasicButton CreateBasicButton( string symbolName )
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			BasicButton ui = new BasicButton( PATH_MAIN, symbolName );
			
			return ui;
		}
		
		public static LabeledButton CreateLabeledButton( string symbolName )
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			LabeledButton ui = new LabeledButton( PATH_MAIN, symbolName );
			
			return ui;
		}
		
		public static LabeledButton CreateLabeledButton( string swf, string symbolName )
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(swf);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			LabeledButton ui = new LabeledButton( swf + ".swf", symbolName );
			
			return ui;
		}
		
		public static ScrollBar CreateScrollBar( string symbolName )
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			ScrollBar ui = new ScrollBar( PATH_MAIN, symbolName );
			
			return ui;	
		}
		
		public static MovieClip CreateChildPictureBlankIcon()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( PATH_MAIN, "BlankChildIcon" );
			
			return ui;	
		}
		
		public static MovieClip CreateChildPictureGuideIcon()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( PATH_MAIN, "CameraGuide" );
			
			return ui;	
		}
		
		public static MovieClip CreateEditChildScreen()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( PATH_MAIN, "EditChild_Screen" );
			
			return ui;	
		}
				
		public static BasicButton CreateCaptureImageButton()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			BasicButton ui = new BasicButton( PATH_MAIN, "CaptureImageButton" );
			
			return ui;	
		}
		
		public static BasicButton CreateClearImageButton()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			BasicButton ui = new BasicButton( PATH_MAIN, "ClearButton" );
			
			return ui;	
		}
		
		public static BasicButton CreateSaveChildButton()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			BasicButton ui = new BasicButton( PATH_MAIN, "SaveButton" );
			
			return ui;	
		}
		
		public static BasicButton CreateCancelChildButton()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			BasicButton ui = new BasicButton( PATH_MAIN, "CancelButton" );
			
			return ui;	
		}
		
		public static BasicButton CreateDatePickerButton()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			BasicButton ui = new BasicButton( PATH_MAIN, "SelectDateButton" );
			
			return ui;
		}
		
		public static MovieClip CreateDateScrollerView()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			BasicButton ui = new BasicButton( PATH_MAIN, "DatePicker_DropDown" );
			
			return ui;
		}
		
		public static MovieClip CreateDateScrollerListItem()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			BasicButton ui = new BasicButton( PATH_MAIN, "DatePicker_DropDown_ListItem" );
			
			return ui;
		}

		
		public static LabeledButton CreateNumeralRecognitionCard()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(NUMERAL_RECOGNITION);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			LabeledButton ui = new LabeledButton( "NumeralRecognition.swf", "Card" );
			
			return ui;
		}
		
		public static MovieClip CreateNumeralRecognitionTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(NUMERAL_RECOGNITION);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "NumeralRecognition.swf", "Tutorial" );
			
			return ui;
		}
		
		public static FilterMovieClip CreateLetterRecognitionCard()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(LETTER_RECOGNITION);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			BasicButton ui = new BasicButton( "LetterRecognition.swf", "Card" );
			
			DebugConsole.Log("Create Letter Recognition Card Basic Button width: {0}", ui.width);
			
			FilterMovieClip filterClip = new FilterMovieClip(ui);
			return filterClip;
		}
		
		public static BasicButton CreateLetterRecognitionCardTest()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(LETTER_RECOGNITION);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			BasicButton ui = new BasicButton( "LetterRecognition.swf", "Card" );
			DebugConsole.Log("Create Letter Recognition Card Basic Button Test width: {0}", ui.width);
			//FilterMovieClip filterClip = new FilterMovieClip(ui);
			return ui;
		}
		
		public static MovieClip CreateLetterRecognitionTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(LETTER_RECOGNITION);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "LetterRecognition.swf", "Tutorial" );
			
			return ui;
		}
		
		public static MovieClip CreateInitialSoundsTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(INITIAL_SOUNDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "InitialSounds.swf", "Tutorial" );
			
			return ui;
		}
		
		public static MovieClip CreateInitialSoundsEar()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(INITIAL_SOUNDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "InitialSounds.swf", "Ear" );
			
			return ui;
		}
		
		public static MovieClip CreateInitialSoundsCard()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(INITIAL_SOUNDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "InitialSounds.swf", "Card" );
			
			return ui;
		}
		
		public static MovieClip CreateCommonShapesTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(COMMON_SHAPES);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "CommonShapes.swf", "Tutorial" );
			
			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		public static MovieClip CreateSortingTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SORTING);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Sorting.swf", "Tutorial" );

			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		public static MovieClip CreateSortingHouse()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SORTING);
			Debug.Log ("BUNDLEL : " + bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Sorting.swf", "House" );
			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		public static MovieClip CreateSortingObject()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SORTING);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Sorting.swf", "SortObject" );
			
			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		public static MovieClip CreateCommonShapesObject(string type)
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(COMMON_SHAPES);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			return new MovieClip( "CommonShapes.swf", type );
		}
		
		public static MovieClip CreateCommonShapesCard()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(COMMON_SHAPES);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "CommonShapes.swf", "Shape" );
			
			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		public static FilterMovieClip CreateSentenceSegmentingCounter()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SENTENCE_SEGMENTING);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			BasicButton ui = new BasicButton( "SentenceSegmenting.swf", "Counter" );
			FilterMovieClip filterClip = new FilterMovieClip(ui);
			return filterClip;
		}
		
		public static MovieClip CreateSentenceSegmentingTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SENTENCE_SEGMENTING);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "SentenceSegmenting.swf", "Tutorial" );

			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		public static MovieClip CreateSentenceSegmentingPicture()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SENTENCE_SEGMENTING);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "SentenceSegmenting.swf", "Pictures" );

			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		public static MovieClip CreateSentenceSegmentingBook()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SENTENCE_SEGMENTING);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Book.swf", "Book" );

			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		public static MovieClip CreateSentenceSegmentingBookAnim()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SENTENCE_SEGMENTING);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "SentenceSegmenting.swf", "Book_Pageflip" );

			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		private static void clearBundle(string bundleName)
		{
			AssetBundle targetBundle = AssetLoader.Instance.GetAssetBundle(bundleName);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( loader.bundles.Contains( targetBundle ) )
				loader.removeAssetBundle(targetBundle);
		}
		
		public static MovieClip CreateSegmentingCompoundWordsBackground()		
		{
			//clearBundle(BLENDING_COMPOUND_WORDS);
			
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SEGMENTING_COMPOUND_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Backings.swf", "Background" );

			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		public static MovieClip CreateSegmentingCompoundWordsBoxSegmenting()		
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SEGMENTING_COMPOUND_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Backings.swf", "BoxSegmenting" );

			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		public static MovieClip CreateSegmentingCompoundWordsBoxSegmentingItems()		
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SEGMENTING_COMPOUND_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "SegmentingCompoundWords.swf", "BoxSegmenting" );

			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		
		public static MovieClip CreateSegmentingCompoundWordsCompoundWords()		
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SEGMENTING_COMPOUND_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "SegmentingCompoundWords.swf", "CompoundWords" );

			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}

		public static MovieClip CreateSegmentingCompoundWordsPuzzlePieceLeft()		
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SEGMENTING_COMPOUND_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "SegmentingCompoundWords.swf", "PuzzlePieceLeft" );

			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		public static MovieClip CreateSegmentingCompoundWordsPuzzlePieceRight()		
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SEGMENTING_COMPOUND_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "SegmentingCompoundWords.swf", "PuzzlePieceRight" );

			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		public static MovieClip CreateSegmentingCompoundWordsPuzzlePieceArm()		
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SEGMENTING_COMPOUND_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "SegmentingCompoundWords.swf", "ArmMotion" );

			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		public static MovieClip CreateBlendingCompoundWordsBackground()		
		{
			//clearBundle(SEGMENTING_COMPOUND_WORDS);
			
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(BLENDING_COMPOUND_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Backings.swf", "Background" );

			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		public static MovieClip CreateBlendingCompoundWordsBox()		
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(BLENDING_COMPOUND_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			
			MovieClip ui =  new MovieClip( "Backings.swf", "BoxBlending" );

			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		public static MovieClip CreateBlendingCompoundWordsBoxItems()		
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(BLENDING_COMPOUND_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			
			MovieClip ui = new MovieClip( "BlendingCompoundWords.swf", "BoxSegmenting" );

			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		public static MovieClip CreateBlendingCompoundWordsCompoundWords()		
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(BLENDING_COMPOUND_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "BlendingCompoundWords.swf", "CompoundWords" );

			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}

		public static MovieClip CreateBlendingCompoundWordsPuzzlePieceLeft()		
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(BLENDING_COMPOUND_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "BlendingCompoundWords.swf", "PuzzlePieceLeft" );

			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		public static MovieClip CreateBlendingCompoundWordsPuzzlePieceRight()		
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(BLENDING_COMPOUND_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "BlendingCompoundWords.swf", "PuzzlePieceRight" );

			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		public static MovieClip CreateBlendingCompoundWordsPuzzlePieceArm()		
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(BLENDING_COMPOUND_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "BlendingCompoundWords.swf", "ArmMotion" );

			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;
		}
		
		public static MovieClip CreateLanguageVocabularyTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(LANGUAGE_VOCABULARY);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "LanguageVocabulary.swf", "Tutorial" );

			return ui;
		}
		
		public static MovieClip CreateSafariClip(string symbol)
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SAFARI_THEME);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip("HATCH_Theme_Safari.swf", symbol );
			
			return ui;	
		}
		
		public static SafariThemeView CreateSafariTheme()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SAFARI_THEME);
			
			if( bundle != null && bundle.Contains("HATCH_Theme_Safari.swf") )
			{
				DebugConsole.Log("BUNDLE RETREIVED FROM ASSET LOADER IN MOVIECLIPFACTORY");
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				
				//find out which is closest size - 
				//float ratio = (float)Screen.height/Screen.width;
				SafariThemeView ui = null;
				if(PlatformUtils.GetClosestRatio() == ScreenRatio.FOURxTHREE){
					//THIS IS IPAD!!!	
					ui = new SafariThemeView("HATCH_Theme_Safari.swf", "SafariContainer4x3");
					Debug.Log ("********* USING SAFARI THEME IPAD**********");
				}else{
				
					ui = new SafariThemeView("HATCH_Theme_Safari.swf", "SafariContainer");
				}
				return ui;
			}
			else // bundle was unloaded
			{
				DebugConsole.LogError("CreateSafariTheme: BUNDLE NULL SAFARI_TEMEM, TRY TO LOAD HATCH_Theme_Safari.swf");
			}
			
			return null;		
		} 
		
		public static MovieClip CreateTravelClip(string symbol)
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(TRAVEL_THEME);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip("HATCH_Theme_Travel.swf", symbol );
			
			return ui;	
		}
		
		public static TravelThemeView CreateTravelTheme()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(TRAVEL_THEME);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			
			
			
			string themeView = "TravelTheme";
			if(PlatformUtils.GetClosestRatio() == ScreenRatio.FOURxTHREE){
				themeView = "TravelTheme4x3";
			}
			
			
			TravelThemeView ui = new TravelThemeView("HATCH_Theme_Travel.swf", themeView);
			
			return ui;
		} 
		
		public static MovieClip CreateGardenClip(string symbol)
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GARDEN_THEME);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip("HATCH_Theme_Garden.swf", symbol );
			
			return ui;	
		}
		
		public static GardenThemeView CreateGardenTheme()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GARDEN_THEME);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);			
			
			string container = "GardenContainer";
			if(PlatformUtils.GetClosestRatio() == ScreenRatio.FOURxTHREE){
				container = "GardenContainer4x3";
			}
			
			GardenThemeView ui = new GardenThemeView("HATCH_Theme_Garden.swf", container);
			
			return ui;
		} 
		
		
		public static DeviceWizardView GetDeviceWizardView()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			if( bundle.Contains("DeviceWizard.swf") )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				return new DeviceWizardView("DeviceWizard.swf", "DeviceWizardView");
			}
			else return null;
		}
		
		public static MovieClip CreateCountingFoundationsTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(COUNTING_FOUNDATIONS);
			
			if( bundle.Contains("CountingFoundations.swf") )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				return new MovieClip("CountingFoundations.swf", "Tutorial");
			}
			else return null;
		}
		
		public static CountingFoundationsBGContainer GetCountingFoundationsBGContainer()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(COUNTING_FOUNDATIONS);
			
			if( bundle.Contains("CountingFoundations.swf") )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				return new CountingFoundationsBGContainer("CountingFoundations.swf", "GamesContainer");
			}
			else return null;
		}
		
		public static MovieClip GetCountingFoundationsTutorialBG()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(COUNTING_FOUNDATIONS);
			
			if( bundle.Contains("CountingFoundations.swf") )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				return new CountingFoundationsBGContainer("CountingFoundations.swf", "TutorialBG");
			}
			else return null;
		}
		
		public static MovieClip GetCountingFoundationsGameBG()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(COUNTING_FOUNDATIONS);
			
			if( bundle.Contains("CountingFoundations.swf") )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				return new CountingFoundationsBGContainer("CountingFoundations.swf", "GameBG");
			}
			else return null;
		}
		
		/*public static MovieClip GetAdditionAssets(string name)
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(ADDITION);
			
			if( bundle.Contains("Addition.swf") )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				return new MovieClip("Addition.swf", name);
			}
			else return null;
		}*/
		
		public static MovieClip CreateAdditionHouse()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(ADDITION);
			
			if( bundle.Contains("Addition.swf") )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				return new MovieClip("Addition.swf", "Targets");
			}
			else return null;

		}
		
		public static MovieClip CreateAdditionDragger()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(ADDITION);
			
			if( bundle.Contains("Addition.swf") )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				return new MovieClip("Addition.swf", "DragObject");
			}
			else return null;
		}
		
		public static MovieClip CreateAdditionTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(ADDITION);
			
			if( bundle.Contains("Addition.swf") )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				return new MovieClip("Addition.swf", "Tutorial");
			}
			else return null;
		}
		
		public static MovieClip CreateSpatialSkillsTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SPATIAL_SKILLS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "SpacialSkills.swf", "Tutorial" );
			
			return ui;
		}
		
		public static MovieClip CreateSubtractionTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SUBTRACTION);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Subtraction.swf", "Tutorial" );
			
			return ui;
		}
		
		public static MovieClip CreateSubtractionDragger()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SUBTRACTION);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Subtraction.swf", "DragObject" );
			//FilterMovieClip filteredClip = new FilterMovieClip(ui);
			
			return ui;//filteredClip;
		}
		
		public static MovieClip CreateSubtractionHouse()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SUBTRACTION);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Subtraction.swf", "Submarine" );
			
			return ui;
		}
		
		public static MovieClip CreateOnsetRimeEar()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(ONSET_RIME);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "OnsetRime.swf", "Ear" );
			
			return ui;
		}
		
		public static MovieClip CreateOnsetRimeCard()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(ONSET_RIME);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "OnsetRime.swf", "Card" );
			
			return ui;
		}
		
		public static MovieClip CreateOnsetRimeTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(ONSET_RIME);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "OnsetRime.swf", "Tutorial" );
			
			return ui;
		}

		public static MovieClip CreateDeletingOnsetAndRimeSign()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(DELETING_ONSET_AND_RIME);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "DeletingOnsetAndRime.swf", "Photos" );
			
			return ui;
		}

		public static MovieClip CreateDeletingOnsetAndRimeTarget()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(DELETING_ONSET_AND_RIME);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "DeletingOnsetAndRime.swf", "Target" );
			
			return ui;
		}

		public static MovieClip CreateDeletingOnsetAndRimeVegetable()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(DELETING_ONSET_AND_RIME);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "DeletingOnsetAndRime.swf", "AnswerChoice" );
			
			return ui;
		}

		public static MovieClip CreateDeletingOnsetAndRimeTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(DELETING_ONSET_AND_RIME);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "DeletingOnsetAndRime.swf", "Tutorial" );
			
			return ui;
		}

		public static MovieClip CreateBlendingSoundsInWordsTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(BLENDING_SOUNDS_IN_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			DebugConsole.Log ("Loading Tutorial");
			MovieClip ui = new MovieClip( "BlendingSoundsInWords.swf", "Tutorial" );
			DebugConsole.Log ("Returning");
			return ui;
		}

		public static MovieClip CreateBlendingSoundsInWords_Sign()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(BLENDING_SOUNDS_IN_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "BlendingSoundsInWords.swf", "Photos" );
			
			return ui;
		}

		public static MovieClip CreateBlendingSoundsInWords_Stage()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(BLENDING_SOUNDS_IN_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "BlendingSoundsInWords.swf", "Stage" );
			
			return ui;
		}

		public static MovieClip CreateBlendingSoundsInWords_LeftBird()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(BLENDING_SOUNDS_IN_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "BlendingSoundsInWords.swf", "Stagebirdleft" );
			
			return ui;
		}

		public static MovieClip CreateBlendingSoundsInWords_MiddleBird()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(BLENDING_SOUNDS_IN_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "BlendingSoundsInWords.swf", "Stagebirdmid" );
			
			return ui;
		}

		public static MovieClip CreateBlendingSoundsInWords_RightBird()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(BLENDING_SOUNDS_IN_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "BlendingSoundsInWords.swf", "Stagebirdright" );
			
			return ui;
		}

		public static MovieClip CreateBlendingSoundsInWords_AnswerBird()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(BLENDING_SOUNDS_IN_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "BlendingSoundsInWords.swf", "Answers" );
			
			return ui;
		}

		public static MovieClip CreateBlendingSoundsInWords_TargetBox()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(BLENDING_SOUNDS_IN_WORDS);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "BlendingSoundsInWords.swf", "Targetbox" );
			
			return ui;
		}

		public static MovieClip CreateObjectsInASetTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(OBJECTS_IN_A_SET);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "ObjectsInASet.swf", "Tutorial" );
			
			return ui;
		}
		
		public static MovieClip CreateObjectsInASetObject( string obj_type )
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(OBJECTS_IN_A_SET);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "ObjectsInASet.swf", obj_type );
			
			return ui;
		}
		
		public static MovieClip CreateObjectsInASetCard()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(OBJECTS_IN_A_SET);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "ObjectsInASet.swf", "Card" );
			
			return ui;
		}
		
		public static SequenceTarget CreateSequenceCountingTarget()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SEQUENCE_COUNTING);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			SequenceTarget ui = new SequenceTarget();
			
			return ui;
		}
		
		public static MovieClip CreateSequenceCountingCounter()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SEQUENCE_COUNTING);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "SequenceCounting.swf", "counter" );
			
			return ui;
		}
		
		public static MovieClip CreateSequenceCountingTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SEQUENCE_COUNTING);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "SequenceCounting.swf", "tutorial" );
			
			return ui;
		}
		
		public static MovieClip CreatePatterningTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(PATTERNING);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Patterning.swf", "tutorial" );
			
			return ui;
		}
		
		public static MovieClip CreatePatterningBalls()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(PATTERNING);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Patterning.swf", "Balls" );
			
			return ui;
		}
		
		public static MovieClip CreatePatterningFruits()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(PATTERNING);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Patterning.swf", "Fruits" );
			
			return ui;
		}
		
		public static MovieClip CreatePatterningShapes()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(PATTERNING);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Patterning.swf", "Shapes" );
			
			return ui;
		}
		
		public static MovieClip CreatePatterningNecklace()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(PATTERNING);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Patterning.swf", "Necklace" );
			
			return ui;
		}
		
		public static MovieClip CreateMeasurementBackground()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(MEASUREMENT);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Measurement.swf", "Paper" );
			
			return ui;
		}
		
		public static MovieClip CreateMeasurementObjects()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(MEASUREMENT);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Measurement.swf", "Objects" );
			
			return ui;
		}
		
		public static MovieClip CreateMeasurementPeopleFront()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(MEASUREMENT);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Measurement.swf", "PeopleFront" );
			
			return ui;
		}
		
		public static MovieClip CreateMeasurementPeopleSide()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(MEASUREMENT);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Measurement.swf", "PeopleSide" );
			
			return ui;
		}
		
		public static MovieClip CreateMeasurementTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(MEASUREMENT);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Measurement.swf", "Tutorial" );
			
			return ui;
		}
		
		public static MovieClip CreateAdditionUpToTenPile()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(ADDITIONUPTOTEN);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( ADDITIONUPTOTEN + ".swf", "Pile" );
			
			return ui;
		}
		
		public static MovieClip CreateAdditionUpToTenBasket()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(ADDITIONUPTOTEN);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( ADDITIONUPTOTEN + ".swf", "Basket" );
			
			return ui;
		}
		
		public static MovieClip CreateAdditionUpToTenObject()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(ADDITIONUPTOTEN);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( ADDITIONUPTOTEN + ".swf", "GameObjects" );
			
			return ui;
		}
		
		public static MovieClip CreateAdditionUpToTenTarget()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(ADDITIONUPTOTEN);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( ADDITIONUPTOTEN + ".swf", "Target" );
			
			return ui;
		}
		
		public static MovieClip CreateAdditionUpToTenTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(ADDITIONUPTOTEN);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( ADDITIONUPTOTEN + ".swf", "Tutorial");
			
			return ui;
		}
		
		public static MovieClip CreateSubtractionFromTenWagon()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SUBTRACTIONFROMTEN);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( SUBTRACTIONFROMTEN + ".swf", "Wagon" );
			
			return ui;
		}
		
		public static MovieClip CreateSubtractionFromTenTarget()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SUBTRACTIONFROMTEN);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( SUBTRACTIONFROMTEN + ".swf", "Target" );
			
			return ui;
		}
		
		public static MovieClip CreateSubtractionFromTenTutorial()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SUBTRACTIONFROMTEN);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( SUBTRACTIONFROMTEN + ".swf", "Tutorial");
			
			return ui;
		}
		
		public static MenuOverlay CreateMenuOverlay(SessionController _sessionController)
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			if( bundle.Contains("MenuOverlay.swf") )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				MenuOverlay ui = new MenuOverlay( "MenuOverlay.swf", "MenuOverlay", _sessionController );
			
				return ui;
			}
			else return null;
		}
		
		public static MovieClip CreateHelpContinueButton()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( "Menu.swf", "HelpContinue_Button" );
			
			return ui;
		}
		
		private static DisplayObject CreateBlocker()
		{
			var clip = new MovieClip();
			clip.width = Screen.width;
			clip.height = Screen.height;
			clip.graphics.drawSolidRectangle(new Color(0, 0, 0, 0.5f),
				0, 0, clip.width, clip.height);
			return clip;
		}
		
		public static DisplayObject CreateNoSkillsAvailable()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			Debug.Log (bundle);
			
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
			MovieClip ui = new MovieClip( PATH_NO_SKILLS_AVAILABLE, "NoSkillsAvailableScreen" );
			ui.stopAll();
			
			var clip = new DisplayObjectContainer();
			clip.width = Screen.width;
			clip.height = Screen.height;
			clip.addChild(CreateBlocker());
			clip.addChild(ui);
			ui.AlignCenteredToScreen();
			
			return clip;
		}
		
		public static bool UnloadSwf(string pathToSwf)
		{
			BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
			return loader.unloadSWF(pathToSwf);
		}
		
		private static Dictionary< string, List<string> > swfTextures;
		
		public static void UnloadTexturesWithPrefix (string pathToSwf)
		{
			if( swfTextures == null )
				swfTextures = new Dictionary< string, List<string> >();
			List<string> textureList = swfTextures.ContainsKey(pathToSwf) ? swfTextures[pathToSwf] : new List<string>();
			
			DebugConsole.Log("Destroying textures: {0}", pathToSwf);
			Texture[] textures = (Texture[])Resources.FindObjectsOfTypeAll (typeof(Texture));
			foreach (Texture texture in textures) {
				if(texture.name.StartsWith(pathToSwf)) {
					UnityEngine.Object.DestroyImmediate(texture, true);
					//if( !textureList.Contains( texture.name ) )
					//	textureList.Add( texture.name );
					//Resources.UnloadAsset(texture);
				}
			}
			if( textureList.Count > 0 )
				swfTextures[pathToSwf] = textureList;
		}
		
		public static void clearTextureSpecificSkill( string skillName )
		{
			if( skillName != "SpatialSkills" )
				UnloadAndDestroySwf( skillName + ".swf" );
			else
				UnloadAndDestroySwf( "SpacialSkills.swf" );
			
			switch( skillName )
			{
			case "SegmentingCompoundWords":
			case "BlendingCopoundWords":
				UnloadAndDestroySwf( "Backings.swf" );
				break;
				
			case "SentenceSegmenting":
				UnloadAndDestroySwf( "LineCover.swf" );
				UnloadAndDestroySwf( "Book.swf" );
				break;
				
			case "SequenceCounting":
				UnloadAndDestroySwf( "Targets.swf" );
				break;
				
			default:
				break;
			}
		}
		
		public static void reloadTextureSpecificSkill( string skillName )
		{
			if( swfTextures == null )
				return;
			
			AssetBundle targetBundle = AssetLoader.Instance.GetAssetBundle(skillName);
			
			if( skillName != "SpatialSkills" )
				ReloadTexturesWithPrefix( skillName + ".swf", targetBundle );
			else
				ReloadTexturesWithPrefix( "SpacialSkills.swf", targetBundle );
			
			switch( skillName )
			{
			case "SegmentingCompoundWords":
			case "BlendingCopoundWords":
				ReloadTexturesWithPrefix( "Backings.swf", targetBundle );
				break;
				
			case "SentenceSegmenting":
				ReloadTexturesWithPrefix( "LineCover.swf", targetBundle );
				ReloadTexturesWithPrefix( "Book.swf", targetBundle );
				break;
				
			case "SequenceCounting":
				ReloadTexturesWithPrefix( "Targets.swf", targetBundle );
				break;
				
			default:
				break;
			}
		}
		
		public static void ReloadTexturesWithPrefix (string pathToSwf, AssetBundle bundle)
		{
			if( swfTextures == null || !swfTextures.ContainsKey(pathToSwf) )
				return;
			
			foreach( string textureName in swfTextures[pathToSwf] )
			{
				bundle.Load( textureName );
			}
		}
		
		public static bool UnloadAndDestroySwf(string pathToSwf)
		{
			bool unloaded = UnloadSwf(pathToSwf);
			
			// UniSwf unloadSWF calls Resources.UnloadAsset, which does not work for
			// assets loaded from asset bundles. For stuff in GlobalAssets, we need to
			// destroy these explicitly. Once destroyed, they cannot be re-loaded, so
			// only use for swfs that not be used again.
			if(unloaded) {
				UnloadTexturesWithPrefix (pathToSwf);
			}
			return unloaded;
		}
		
		public static ConfirmationMenu CreateConfirmationMenu(String language)
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			if( bundle.Contains("YesNoAnimations.swf") )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				ConfirmationMenu ui = new ConfirmationMenu( "YesNoAnimations.swf", "Dialog", language );

				return ui;
			}
			else return null;
		}
		
		public static TimeoutMenu CreateTimeoutMenu( Child user, string language )
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			if( bundle.Contains("YesNoAnimations.swf") )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				TimeoutMenu ui = new TimeoutMenu( "YesNoAnimations.swf", "Dialog2", user, language );

				return ui;
			}
			else return null;
		}
		
		public static MovieClip GetGardenBackground()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GARDEN_THEME);
			
			if( bundle.Contains("GardenBackground.swf") )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				MovieClip mc = new MovieClip("GardenBackground.swf", "background");
			
				return mc;
			}
			else return null;
		}
		
		public static MovieClip GetSafariBackground()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SAFARI_THEME);
			
			if( bundle.Contains("SafariBackground.swf") )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				MovieClip mc = new MovieClip("SafariBackground.swf", "background");
			
				return mc;
			}
			else return null;
		}
		
		public static MovieClip GetTravelBackground()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(TRAVEL_THEME);
			
			if( bundle.Contains("TravelBackground.swf") )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				string bg = PlatformUtils.GetClosestRatio() == ScreenRatio.FOURxTHREE ? "background4x3" : "background";
				MovieClip mc = new MovieClip("TravelBackground.swf", bg);
			
				return mc;
			}
			else return null;
		}
		
		public static MovieClip GetLineBlank()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SENTENCE_SEGMENTING);
			
			if( bundle.Contains("LineCover.swf") )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				// LineCover4x3 is the smaller one, LineCover is for retina/larger book
				string bg = "LineCover4x3";
				switch(PlatformUtils.GetPlatform())
				{
					case Platforms.ANDROID:
					case Platforms.IOS:
						bg = "LineCover4x3";
						break;
					case Platforms.IOSRETINA:
					case Platforms.WIN:
						bg = "LineCover";
						break;
				}
				//string bg = Screen.width == 1024 || (PlatformUtils.GetClosestRatio() != ScreenRatio.FOURxTHREE && PlatformUtils.GetPlatform() != Platforms.WIN) ? "LineCover4x3" : "LineCover";
				MovieClip mc = new MovieClip("LineCover.swf", bg);
			
				return mc;
			}
			else return null;
		}
		
		/*public static PageAnimation CreatePageAnimation()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SENTENCE_SEGMENTING);
			
			if( bundle.Contains("PageFlip.swf") )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				PageAnimation ui = new PageAnimation( "PageFlip.swf", "PageAnimation" );
			
				return ui;
			}
			else return null;
		}*/
		
	}
}

