using System;
using HatchFramework;
using pumpkin.swf;
using pumpkin.display;
using UnityEngine;

namespace SSGCore
{
	public enum CharacterNames:int
	{
		Cam,
		Henry,
		Platty
	}
	
	public class CharacterFactory
	{
		public const string GLOBAL = "GlobalAssets";
		public const string GARDEN_THEME = "GardenTheme";
		public const string SAFARI_THEME = "SafariTheme";
		public const string TRAVEL_THEME = "TravelTheme";
		
		public const string STANDARD = "Characters_STANDARD.swf";
		public const string GARDEN = "Characters_GARDEN.swf";
		public const string TRAVEL = "Characters_TRAVEL.swf";
		public const string SAFARI = "Characters_SAFARI.swf";
		
		/*
		 SSSSS TTTTTT   AAA   NN  NN DDDDDD    AAA   RRRRR   DDDDDD  
		SS       TT    AAAAA  NNN NN DD   DD  AAAAA  RR  RR  DD   DD 
		 SSSS    TT   AA   AA NNNNNN DD   DD AA   AA RRRRR   DD   DD 
		    SS   TT   AAAAAAA NN NNN DD   DD AAAAAAA RR  RR  DD   DD 
		SSSSS    TT   AA   AA NN  NN DDDDDD  AA   AA RR   RR DDDDDD  
		*/
		public static BaseCam GetStandardCamCharacter()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			if( bundle.Contains(STANDARD) )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				
				return new BaseCam(STANDARD, CharacterNames.Cam.ToString());
			}
			else return null;
		} 
		
		public static BaseHenry GetStandardHenryCharacter()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			if( bundle.Contains(STANDARD) )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				
				return new BaseHenry(STANDARD, CharacterNames.Henry.ToString());
			}
			else return null;
		} 
		
		public static BasePlatty GetStandardPlattyCharacter()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GLOBAL);
			
			if( bundle.Contains(STANDARD) )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				
				return new BasePlatty(STANDARD, CharacterNames.Platty.ToString());
			}
			else return null;
		} 
		
		/*
		 GGGGG    AAA   RRRRR   DDDDDD  EEEEEEE NN  NN 
		GG       AAAAA  RR  RR  DD   DD EE      NNN NN 
		GG  GGG AA   AA RRRRR   DD   DD EEEE    NNNNNN 
		GG   GG AAAAAAA RR  RR  DD   DD EE      NN NNN 
		 GGGGG  AA   AA RR   RR DDDDDD  EEEEEEE NN  NN 
		*/
		
		public static BaseCam GetGardenCamCharacter()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GARDEN_THEME);
			
			if( bundle.Contains(GARDEN) )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				
				return new BaseCam(GARDEN, CharacterNames.Cam.ToString());
			}
			else return null;
		} 
		
		public static BaseHenry GetGardenHenryCharacter()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GARDEN_THEME);
			
			if( bundle.Contains(GARDEN) )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				
				return new BaseHenry(GARDEN, CharacterNames.Henry.ToString());
			}
			else return null;
		} 
		
		public static BasePlatty GetGardenPlattyCharacter()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(GARDEN_THEME);
			
			if( bundle.Contains(GARDEN) )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				
				return new BasePlatty(GARDEN, CharacterNames.Platty.ToString());
			}
			else return null;
		} 
		
		/*
		 SSSSS   AAA   FFFFFF   AAA   RRRRR   IIIIII 
		SS      AAAAA  FF      AAAAA  RR  RR    II   
		 SSSS  AA   AA FFFF   AA   AA RRRRR     II   
		    SS AAAAAAA FF     AAAAAAA RR  RR    II   
		SSSSS  AA   AA FF     AA   AA RR   RR IIIIII 
		*/
		public static BaseCam GetSafariCamCharacter()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SAFARI_THEME);
			
			if( bundle.Contains(SAFARI) )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				
				return new BaseCam(SAFARI, CharacterNames.Cam.ToString());
			}
			else return null;
		} 
		
		public static BaseHenry GetSafariHenryCharacter()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SAFARI_THEME);
			
			if( bundle.Contains(SAFARI) )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				
				return new BaseHenry(SAFARI, CharacterNames.Henry.ToString());
			}
			else return null;
		} 
		
		public static BasePlatty GetSafariPlattyCharacter()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(SAFARI_THEME);
			
			if( bundle.Contains(SAFARI) )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				return new BasePlatty(SAFARI, CharacterNames.Platty.ToString());
			}
			else return null;
		} 
		
		/*
		TTTTTT RRRRR     AAA   V     V EEEEEEE LL      
		  TT   RR  RR   AAAAA  V     V EE      LL      
		  TT   RRRRR   AA   AA  V   V  EEEE    LL      
		  TT   RR  RR  AAAAAAA   V V   EE      LL      
		  TT   RR   RR AA   AA    V    EEEEEEE LLLLLLL 
		*/
		public static BaseCam GetTravelCamCharacter()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(TRAVEL_THEME);
			
			if( bundle.Contains(TRAVEL) )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				
				return new BaseCam(TRAVEL, CharacterNames.Cam.ToString());
			}
			else return null;
		} 
		
		public static BaseHenry GetTravelHenryCharacter()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(TRAVEL_THEME);
			
			if( bundle.Contains(TRAVEL) )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				
				return new BaseHenry(TRAVEL, CharacterNames.Henry.ToString());
			}
			else return null;
		} 
		
		public static BasePlatty GetTravelPlattyCharacter()
		{
			AssetBundle bundle = AssetLoader.Instance.GetAssetBundle(TRAVEL_THEME);
			
			if( bundle.Contains(TRAVEL) )
			{
				BuiltinResourceLoader loader = MovieClip.rootResourceLoader as BuiltinResourceLoader;
				if( !loader.bundles.Contains(bundle) ) loader.addAssetBundle(bundle);
				
				return new BasePlatty(TRAVEL, CharacterNames.Platty.ToString());
			}
			else return null;
		} 
	}
}

