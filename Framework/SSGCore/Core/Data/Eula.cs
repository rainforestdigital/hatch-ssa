using UnityEngine;
using System.IO;
using System;
using HatchFramework;

namespace SSGCore
{
	internal static class Eula
	{
		internal class Info {
			public string username;
			public DateTime date;
			public bool shouldSync;
		}
		
		
		private static string path{
			
			get{
				return Path.Combine(PlatformUtils.InternalDataPath, GlobalConfig.GetProperty<string>("LOCAL_DIR")+"/EULA_Entered");	
			}
		}
		
		public static bool ShouldDisplay
		{
			get
			{
				return !File.Exists(path);
			}
		}
		
		public static void StoreEulaAcceptance(string username)
		{
			username = username.Replace("\n", "");
			string contents = username + "\n" + DateTime.UtcNow.ToString();
			File.WriteAllText(path, contents);
		}
		
		public static void StoreEulaSync(string username, DateTime date, string device_uid)
		{
			username = username.Replace("\n", "");
			string contents = username + "\n" + date.ToString() + "\n" + device_uid;
			File.WriteAllText(path, contents);
		}
		
		public static Info LoadEulaInfo()
		{
			var info = new Info();
			
			if(!File.Exists(path)) {
				return info;
			}
			
			string[] lines = File.ReadAllLines(path);
			if(lines.Length < 2) {
				return info;
			}
			
			info.username = lines[0];
			if (!DateTime.TryParse(lines[1], out info.date)) {
				return info;
			}
			
			info.shouldSync = lines.Length <= 2 || String.IsNullOrEmpty(lines[2]);
			return info;
		}
	}
}
