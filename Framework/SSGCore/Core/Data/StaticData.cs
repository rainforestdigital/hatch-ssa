using System;
using System.Linq;
using HatchFramework;
using System.Collections.Generic;

namespace SSGCore
{
	public class StaticData
	{
		static string[] createDB = new string[]{
			// Classroom table
			//"DROP TABLE IF EXISTS classroom;",
			
			"CREATE TABLE IF NOT EXISTS classroom (id integer primary key, name varchar, user_defined_id varchar, teacher_id int, classroom_pic varchar, teacher_pic varchar, themes varchar);",
			
			// User table
			//"DROP TABLE IF EXISTS user",
			"CREATE TABLE IF NOT EXISTS user (id integer primary key, uid varchar, custom_id varchar, first_name varchar, last_name varchar, birth_date int, picture varchar, picture_data text, classroom_id int, need_sync int);",
			
			// Theme table
			"DROP TABLE IF EXISTS theme;",
			"CREATE TABLE IF NOT EXISTS theme (id primary key, name varchar, title varchar);",
			"INSERT INTO theme (id, name, title) VALUES (1, 'travel', 'Travel');",
			"INSERT INTO theme (id, name, title) VALUES (2, 'garden', 'Garden');",
			"INSERT INTO theme (id, name, title) VALUES (3, 'safari', 'Safari');",
			
			// Level table
			"DROP TABLE IF EXISTS level;",
			"CREATE TABLE IF NOT EXISTS level (id primary key, code varchar, name varchar);",
			"INSERT INTO level (id, code, name) VALUES (1, 'TT', 'Tutorial');",
			"INSERT INTO level (id, code, name) VALUES (2, 'EG', 'Emerging');",
			"INSERT INTO level (id, code, name) VALUES (3, 'DG', 'Developing');",
			"INSERT INTO level (id, code, name) VALUES (4, 'DD', 'Developed');",
			"INSERT INTO level (id, code, name) VALUES (5, 'CD', 'Complete');",
			"INSERT INTO level (id, code, name) VALUES (6, 'LD', 'Locked');",
			
			// Skill Family table
			"DROP TABLE IF EXISTS family;",
			"CREATE TABLE IF NOT EXISTS family (id primary key, name varchar);",
			"INSERT INTO family (id, name) VALUES (1, 'Phonological Awareness');",
			"INSERT INTO family (id, name) VALUES (2, 'Numeric Operations');",
			"INSERT INTO family (id, name) VALUES (3, 'Language Development');",
			"INSERT INTO family (id, name) VALUES (4, 'Alphabet Knowledge');",
			"INSERT INTO family (id, name) VALUES (5, 'Logic and Reasoning');",
			"INSERT INTO family (id, name) VALUES (6, 'Numeric Operations - Spanish');",
			"INSERT INTO family (id, name) VALUES (7, 'Language Development - Spanish');",
			"INSERT INTO family (id, name) VALUES (8, 'Alphabet Knowledge - Spanish');",
			"INSERT INTO family (id, name) VALUES (9, 'Logic and Reasoning - Spanish');",
			
			// Skill table
			"DROP TABLE IF EXISTS skill;",
			"CREATE TABLE IF NOT EXISTS skill (id primary key, name varchar, family_id int, spanish_id int);",
			
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (1, 'Sentence Segmenting',1,0);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (2, 'Initial Sounds',1,0);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (3, 'Blending Compound Words',1,0);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (4, 'Segmenting Compound Words',1,0);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (5, 'Onset Rime',1,0);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (6, 'Counting Foundations', 2, 19);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (7, 'Numeral Recognition', 2, 20);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (8, 'Sequence Counting', 2, 21);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (9, 'Objects in a Set', 2, 22);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (10, 'Addition', 2, 23);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (11, 'Subtraction', 2, 24);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (12, 'Language Vocabulary', 3, 25);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (13, 'Spatial Skills', 3, 26);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (14, 'Measurement', 3, 27);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (15, 'Letter Recognition', 4, 28);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (16, 'Common Shapes', 5, 29);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (17, 'Sorting', 5, 30);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (18, 'Patterning', 5, 31);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (32, 'DeletingOnsetAndRime', 1, 0);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (33, 'BlendingSoundsInWords', 1, 0);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (34, 'AdditionUpToTen', 2, 0);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (35, 'SubtractionFromTen', 2, 0);",
			
			// Admin table
			
			"CREATE TABLE IF NOT EXISTS admin (id integer primary key autoincrement, name varchar, organization varchar, phone varchar, email varchar);",
			// TODO: Add some real default fallback admin here
			//"INSERT INTO admin (id, name, organization, phone, email) VALUES (1, 'Hatch Administrator', 'Hatch', '(555) 555-5555', 'admin@hatchearlychildhood.com');",
			
			// Settings table
			
			"CREATE TABLE IF NOT EXISTS settings (id integer primary key autoincrement, rms_url varchar, device_id int, device_uid varchar, organization_id int, school_id int, salt varchar, app_version varchar, db_version varchar, device_alias varchar);",
			//"INSERT INTO settings (id, app_version, db_version, device_alias) VALUES (1, '" + GlobalConfig.GetProperty<string>("version") + "', '" + GlobalConfig.GetProperty<string>("dbversion")+"', '');", // reserve id = 1 with defaults
			
			// Session table
			
			"CREATE TABLE IF NOT EXISTS session (id integer primary key autoincrement, user_id int, uid varchar, started varchar, duration int, skill_id int, skill_level_id int, skill_level varchar, percent float, opportunities varchar, need_sync int);",
			
			// Refocus table
			
			"CREATE TABLE IF NOT EXISTS refocus (id integer primary key autoincrement, user_id int, start_date varchar, expiration_date varchar, families_id varchar);",
			
			// Locks table
			
			"CREATE TABLE IF NOT EXISTS lock (id integer primary key autoincrement, user_id int, uid varchar, skill_id int, is_enabled int, created varchar, need_sync int);",
			
			
			"CREATE TABLE IF NOT EXISTS weight (id integer primary key autoincrement, skill_id int, skill_level int, weight int)",
			};
		
		
		static string[] createDemoDB = new string[]{		
			"DROP TABLE IF EXISTS classroom;",
			"CREATE TABLE IF NOT EXISTS classroom (id primary key, name varchar, user_defined_id varchar, teacher_id int, classroom_pic varchar, teacher_pic varchar, themes varchar);",
			"INSERT INTO classroom (id, name, user_defined_id, teacher_id, classroom_pic, teacher_pic, themes) VALUES (1, 'Anteater', 'class_1', 1, 'img/demo_data/classrooms/classroom_1.png', 'img/demo_data/teachers/teacher_1.png', '1,2,3');",
			"INSERT INTO classroom (id, name, user_defined_id, teacher_id, classroom_pic, teacher_pic, themes) VALUES (2, 'Dolphin', 'class_2', 2, null, 'img/demo_data/teachers/teacher_2.png', '1,2,3');",
			"INSERT INTO classroom (id, name, user_defined_id, teacher_id, classroom_pic, teacher_pic, themes) VALUES (3, 'Tulips', 'class_3', 1, 'img/demo_data/classrooms/classroom_3.png', 'img/demo_data/teachers/teacher_1.png', '1,2,3');",
			
			// User table
			"DROP TABLE IF EXISTS user;",
			//	"CREATE TABLE IF NOT EXISTS demo_user (id integer primary key autoincrement, custom_id varchar, first_name varchar, last_name varchar, birth_date int, picture varchar, picture_data text, classroom_id int);",
			"CREATE TABLE IF NOT EXISTS user (id integer primary key autoincrement, uid varchar, custom_id varchar, first_name varchar, last_name varchar, birth_date int, picture varchar, picture_data text, classroom_id int, need_sync int);",
			// Classroom 1, "Anteater"
			"INSERT INTO user (id, custom_id, first_name, last_name, birth_date, picture, classroom_id) VALUES (1, 'custom-1', 'Ralph', 'Jacobs', date('2006-04-26'), 'img/demo_data/users/user_1.png', 1);",
			"INSERT INTO user (id, custom_id, first_name, last_name, birth_date, picture, classroom_id) VALUES (2, 'custom-2', 'Latoya', 'Webster', date('2008-02-17'), 'img/demo_data/users/user_2.png', 1);",
			"INSERT INTO user (id, custom_id, first_name, last_name, birth_date, picture, classroom_id) VALUES (3, 'custom-3', 'Christopher', 'Muff', date('2007-07-10'), 'img/demo_data/users/user_3.png', 1);",
			"INSERT INTO user (id, custom_id, first_name, last_name, birth_date, picture, classroom_id) VALUES (4, 'custom-4', 'Laura', 'Patterson', date('2007-12-09'), 'img/demo_data/users/user_4.png', 1);",
			"INSERT INTO user (id, custom_id, first_name, last_name, birth_date, picture, classroom_id) VALUES (5, 'custom-5', 'Sean', 'Hendrix', date('2008-10-06'), 'img/demo_data/users/user_5.png', 1);",
			// Classroom 2, "Dolphin"
			"INSERT INTO user (id, custom_id, first_name, last_name, birth_date, picture, classroom_id) VALUES (6, 'custom-6', 'Susan', 'Butler', date('2008-09-24'), 'img/demo_data/users/user_6.png', 2);",
			"INSERT INTO user (id, custom_id, first_name, last_name, birth_date, picture, classroom_id) VALUES (7, 'custom-7', 'Jeffrey', 'Torres', date('2008-11-24'), 'img/demo_data/users/user_7.png', 2);",
			"INSERT INTO user (id, custom_id, first_name, last_name, birth_date, picture, classroom_id) VALUES (8, 'custom-8', 'Kathryn', 'Parker', date('2006-05-17'), 'img/demo_data/users/user_8.png', 2);",
			"INSERT INTO user (id, custom_id, first_name, last_name, birth_date, picture, classroom_id) VALUES (9, 'custom-9', 'Stewart', 'Clark', date('2006-12-03'), 'img/demo_data/users/user_9.png', 2);",
			"INSERT INTO user (id, custom_id, first_name, last_name, birth_date, picture, classroom_id) VALUES (10, 'custom-10', 'Janet', 'Perez', date('2008-09-18'), 'img/demo_data/users/user_10.png', 2);",
			"INSERT INTO user (id, custom_id, first_name, last_name, birth_date, picture, classroom_id) VALUES (11, 'custom-11', 'Joey', 'Bealer', date('2007-09-22'), 'img/demo_data/users/user_11.png', 2);",
			"INSERT INTO user (id, custom_id, first_name, last_name, birth_date, picture, classroom_id) VALUES (12, 'custom-12', 'Maggie', 'Wood', date('2006-06-26'), 'img/demo_data/users/user_12.png', 2);",
			"INSERT INTO user (id, custom_id, first_name, last_name, birth_date, picture, classroom_id) VALUES (13, 'custom-13', 'Antonio', 'Clark', date('2007-12-07'), 'img/demo_data/users/user_13.png', 2);",
			"INSERT INTO user (id, custom_id, first_name, last_name, birth_date, picture, classroom_id) VALUES (14, 'custom-14', 'Thelma', 'King', date('2006-10-07'), 'img/demo_data/users/user_14.png', 2);",
			// Classroom 3, "Tulips"
			"INSERT INTO user (id, custom_id, first_name, last_name, birth_date, picture, classroom_id) VALUES (15, 'custom-15', 'Leah', 'Oliver', date('2006-12-12'), 'img/demo_data/users/user_15.png', 3);",
			"INSERT INTO user (id, custom_id, first_name, last_name, birth_date, picture, classroom_id) VALUES (16, 'custom-16', 'Ben', 'Cole', date('2006-11-14'), 'img/demo_data/users/user_16.png', 3);",
			"INSERT INTO user (id, custom_id, first_name, last_name, birth_date, picture, classroom_id) VALUES (17, 'custom-17', 'Jasmine', 'Douglas', date('2006-07-26'), 'img/demo_data/users/user_17.png', 3);",
			"INSERT INTO user (id, custom_id, first_name, last_name, birth_date, picture, classroom_id) VALUES (18, 'custom-18', 'William', 'Maher', date('2007-05-25'), 'img/demo_data/users/user_18.png', 3);",
			
			// Skills Assignments table
			"DROP TABLE IF EXISTS demo_assignment;",
			"CREATE TABLE IF NOT EXISTS demo_assignment (id, skill_id int, level varchar);",
			// Classroom 1, "Anteater"
			"INSERT INTO demo_assignment (id, skill_id, level) VALUES (1,   1, 'EG');", // Sentence Segmenting
			"INSERT INTO demo_assignment (id, skill_id, level) VALUES (2,   2, 'DG');", // Initial Sounds
			"INSERT INTO demo_assignment (id, skill_id, level) VALUES (3,   3, 'DD');", // Blending Compound Words
			"INSERT INTO demo_assignment (id, skill_id, level) VALUES (4,   4, 'CD');", // Segmenting Compound Words
			"INSERT INTO demo_assignment (id, skill_id, level) VALUES (5,   5, 'DG');", // Onset Rime
			// Classroom 2, "Dolphin"
			"INSERT INTO demo_assignment (id, skill_id, level) VALUES (6,   6, 'EG');", // Counting Foundations
			"INSERT INTO demo_assignment (id, skill_id, level) VALUES (7,   7, 'DG');", // Numeral Recognition
			"INSERT INTO demo_assignment (id, skill_id, level) VALUES (8,   8, 'DD');", // Sequence Counting
			"INSERT INTO demo_assignment (id, skill_id, level) VALUES (9,   9, 'CD');", // Objects in a Set
			"INSERT INTO demo_assignment (id, skill_id, level) VALUES (10, 10, 'LD');", // Addition
			"INSERT INTO demo_assignment (id, skill_id, level) VALUES (11, 11, 'EG');", // Subtraction
			"INSERT INTO demo_assignment (id, skill_id, level) VALUES (12, 16, 'EG');", // Common Shapes
			"INSERT INTO demo_assignment (id, skill_id, level) VALUES (13, 17, 'DG');", // Sorting
			"INSERT INTO demo_assignment (id, skill_id, level) VALUES (14, 18, 'TT');", // Patterning
			// Classroom 3, "Tulips"
			"INSERT INTO demo_assignment (id, skill_id, level) VALUES (15, 12, 'DG');", // Language Vocabulary
			"INSERT INTO demo_assignment (id, skill_id, level) VALUES (16, 13, 'EG');", // Spatial Skills
			"INSERT INTO demo_assignment (id, skill_id, level) VALUES (17, 14, 'DD');", // Measurement
			"INSERT INTO demo_assignment (id, skill_id, level) VALUES (18, 15, 'DG');", // Letter Recognition
			
			// Theme table
			"DROP TABLE IF EXISTS theme;",
			"CREATE TABLE IF NOT EXISTS theme (id primary key, name varchar, title varchar);",
			"INSERT INTO theme (id, name, title) VALUES (1, 'travel', 'Travel');",
			"INSERT INTO theme (id, name, title) VALUES (2, 'garden', 'Garden');",
			"INSERT INTO theme (id, name, title) VALUES (3, 'safari', 'Safari');",
			
			// Level table
			"DROP TABLE IF EXISTS level;",
			"CREATE TABLE IF NOT EXISTS level (id primary key, code varchar, name varchar);",
			"INSERT INTO level (id, code, name) VALUES (1, 'TT', 'Tutorial');",
			"INSERT INTO level (id, code, name) VALUES (2, 'EG', 'Emerging');",
			"INSERT INTO level (id, code, name) VALUES (3, 'DG', 'Developing');",
			"INSERT INTO level (id, code, name) VALUES (4, 'DD', 'Developed');",
			"INSERT INTO level (id, code, name) VALUES (5, 'CD', 'Complete');",
			"INSERT INTO level (id, code, name) VALUES (6, 'LD', 'Locked');",
			
			// Skill Family table
			"DROP TABLE IF EXISTS family;",
			"CREATE TABLE IF NOT EXISTS family (id primary key, name varchar);",
			"INSERT INTO family (id, name) VALUES (1, 'Phonological Awareness');",
			"INSERT INTO family (id, name) VALUES (2, 'Numeric Operations');",
			"INSERT INTO family (id, name) VALUES (3, 'Language Development');",
			"INSERT INTO family (id, name) VALUES (4, 'Alphabet Knowledge');",
			"INSERT INTO family (id, name) VALUES (5, 'Logic and Reasoning');",
			"INSERT INTO family (id, name) VALUES (6, 'Numeric Operations - Spanish');",
			"INSERT INTO family (id, name) VALUES (7, 'Language Development - Spanish');",
			"INSERT INTO family (id, name) VALUES (8, 'Alphabet Knowledge - Spanish');",
			"INSERT INTO family (id, name) VALUES (9, 'Logic and Reasoning - Spanish');",
			
			// Skill table
			"DROP TABLE IF EXISTS skill;",
			"CREATE TABLE IF NOT EXISTS skill (id primary key, name varchar, family_id int, spanish_id int);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (1, 'Sentence Segmenting',1,0);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (2, 'Initial Sounds',1,0);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (3, 'Blending Compound Words',1,0);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (4, 'Segmenting Compound Words',1,0);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (5, 'Onset Rime',1,0);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (6, 'Counting Foundations', 2, 19);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (7, 'Numeral Recognition', 2, 20);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (8, 'Sequence Counting', 2, 21);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (9, 'Objects in a Set', 2, 22);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (10, 'Addition', 2, 23);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (11, 'Subtraction', 2, 24);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (12, 'Language Vocabulary', 3, 25);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (13, 'Spatial Skills', 3, 26);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (14, 'Measurement', 3, 27);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (15, 'Letter Recognition', 4, 28);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (16, 'Common Shapes', 5, 29);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (17, 'Sorting', 5, 30);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (18, 'Patterning', 5, 31);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (32, 'AdditionUpToTen', 2, 0);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (33, 'DeletingOnsetAndRime', 1, 0);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (34, 'SubtractionFromTen', 2, 0);",
			"INSERT INTO skill (id, name, family_id, spanish_id) VALUES (35, 'BlendingSoundsInWords', 1, 0);",
			
			"DROP TABLE IF EXISTS admin;",
			"CREATE TABLE IF NOT EXISTS admin (id integer primary key autoincrement, name varchar, organization varchar, phone varchar, email varchar);",
			"INSERT INTO admin (id, name, organization, phone, email) VALUES (1, 'Paul Stubbs', 'Organization Name', '(555) 555-5555', 'p.stubbs@k5.nc.edu');",
			"INSERT INTO admin (id, name, organization, phone, email) VALUES (2, 'Bill Jones', 'Organization Name', '(555) 555-5555', 'b.jones@k5.nc.edu');",
			
			 // Session table
			"DROP TABLE IF EXISTS session;",
			"CREATE TABLE IF NOT EXISTS session (id integer primary key autoincrement, user_id int, uid varchar, started varchar, duration int, skill_id int, skill_level_id int, skill_level varchar, percent float, opportunities varchar, need_sync int);",
			
			// Refocus table
			"DROP TABLE IF EXISTS refocus;",
			"CREATE TABLE IF NOT EXISTS refocus (id integer primary key autoincrement, user_id int, start_date varchar, expiration_date varchar, families_id varchar);",
			"INSERT INTO refocus (user_id, start_date, expiration_date, families_id) VALUES (1, date('2006-04-26'), date('2017-04-26'), '1');",
			
			// Locks table
			"DROP TABLE IF EXISTS lock;",
			"CREATE TABLE IF NOT EXISTS lock (id integer primary key autoincrement, user_id int, uid varchar, skill_id int, is_enabled int, created varchar, need_sync int);",
		};
			
		
		public static SQLBatch BatchFromArray(string[] ar){
			SQLBatch batch = new SQLBatch();
			foreach(string s in ar){
					batch.AddStatement(s);
			}
			return batch;
		}
		
		public static void CreateDatabase(SQLiteDB db){
			
			//build themes
			BatchFromArray(createDB).RunStatements(db);
			
		}
		
		public static void CreateDemoDatabase(SQLiteDB db){
			
			BatchFromArray(createDemoDB).RunStatements(db);
		}
		
		public static List<Classroom> GenerateDemoDataSet()
		{
			var themes = MockData.GenerateTestThemes();
			return new List<Classroom>() {
				new Classroom(0, "Class 1", 
					NullImageSource.Instance,
					new DemoImageSource("Demo/Teachers/Ms-Anna"),
					new List<User>() {
						new Child(0, "Wyatt Bradford", new DemoImageSource("Demo/Class1/1-wyatt-bradford")),
						new Child(1, "Daya Patel", new DemoImageSource("Demo/Class1/2-daya-patel")),
						new Child(2, "Allyson Bennet", new DemoImageSource("Demo/Class1/3-allyson-bennett")),
						new Child(3, "Dalen Jenkins", new DemoImageSource("Demo/Class1/4-dalen-jenkins")),
						new Child(4, "Jared Grimaldi", new DemoImageSource("Demo/Class1/5-jared-grimaldi")),
						new Child(5, "Paige Wilson", new DemoImageSource("Demo/Class1/6-paige-wilson")),
						new Child(6, "Justin Hawkins", new DemoImageSource("Demo/Class1/7-justin-hawkins")),
						new Child(7, "Edward Singh", new DemoImageSource("Demo/Class1/8-edward-singh")),
						new Child(8, "Janet Wong", new DemoImageSource("Demo/Class1/9-janet-wong")),
						new Child(9, "Julie Parker", new DemoImageSource("Demo/Class1/10-julie-parker"))
					},
					themes
				),
				new Classroom(1, "Class 2", 
					NullImageSource.Instance,
					new DemoImageSource("Demo/Teachers/Ms.Olivia"),
					new List<User>() {
						new Child(10, "Luis Soto", new DemoImageSource("Demo/Class2/1-luis-soto")),
						new Child(11, "Sheree Watts", new DemoImageSource("Demo/Class2/2-sheree-watts")),
						new Child(12, "David Ganz", new DemoImageSource("Demo/Class2/3-david-ganz")),
						new Child(13, "Miguel Cruz", new DemoImageSource("Demo/Class2/4-miguel-cruz")),
						new Child(14, "Henry Kim", new DemoImageSource("Demo/Class2/5-henry-kim")),
						new Child(15, "Maya Tambe", new DemoImageSource("Demo/Class2/6-maya-tambe")),
						new Child(16, "Jack Berry", new DemoImageSource("Demo/Class2/7-jack-berry")),
						new Child(17, "Sophia Vega", new DemoImageSource("Demo/Class2/8-sophia-vega")),
						new Child(18, "Cesar Medina", new DemoImageSource("Demo/Class2/9-cesar-medina")),
						new Child(19, "Daniela Rios", new DemoImageSource("Demo/Class2/10-daniela-rios"))
					},
					themes
				),
				new Classroom(2, "Class 3", 
					NullImageSource.Instance,
					new DemoImageSource("Demo/Teachers/Mr.Emilio"),
					new List<User>() {
						new Child(20, "Agustin Flores", new DemoImageSource("Demo/Class3/9-agustin-flores"), LanguageProfile.Spanish),
						new Child(21, "Alejandro Lopez", new DemoImageSource("Demo/Class3/4-alejandro-lopez"), LanguageProfile.Spanish),
						new Child(22, "Camila Garcia", new DemoImageSource("Demo/Class3/1-camila-garcia"), LanguageProfile.Spanish),
						new Child(23, "Catalina Torres", new DemoImageSource("Demo/Class3/6-catalina-torres"), LanguageProfile.Spanish),
						new Child(24, "Diego Cruz", new DemoImageSource("Demo/Class3/11-diego-cruz"), LanguageProfile.Spanish),
						new Child(25, "Gabriel Sanchez", new DemoImageSource("Demo/Class3/7-gabriel-sanchez"), LanguageProfile.Spanish),
						new Child(26, "Julieta Ramirez", new DemoImageSource("Demo/Class3/8-julieta-ramirez"), LanguageProfile.Spanish),
						new Child(27, "Lucia Ruiz", new DemoImageSource("Demo/Class3/14-lucia-ruiz"), LanguageProfile.Spanish),
						new Child(28, "Matías Castillo", new DemoImageSource("Demo/Class3/13-matias-castillo"), LanguageProfile.Spanish),
						new Child(29, "Mateo Martinez", new DemoImageSource("Demo/Class3/3-mateo-martinez"), LanguageProfile.Spanish),
						new Child(30, "Natalia Perez", new DemoImageSource("Demo/Class3/12-natalia-perez"), LanguageProfile.Spanish),
						new Child(31, "Santiago Ramos", new DemoImageSource("Demo/Class3/15-santiago-ramos"), LanguageProfile.Spanish),
						new Child(32, "Sara Hernandez", new DemoImageSource("Demo/Class3/5-sara-hernandez"), LanguageProfile.Spanish),
						new Child(33, "Valentina Rodriguez", new DemoImageSource("Demo/Class3/2-valentina-rodriguez"), LanguageProfile.Spanish),
						new Child(34, "Valeria Diaz", new DemoImageSource("Demo/Class3/10-valeria-diaz"), LanguageProfile.Spanish),
					},
					themes
				)
			};
		}
		
		private enum SkillIndex {
			SentenceSegmenting = 1,
			InitialSounds,
			BlendingCompoundWords,
			SegmentingCompoundWords,
			OnsetRime,
			CountingFoundations,
			NumeralRecognition,
			SequenceCounting,
			ObjectsinaSet,
			Addition,
			Subtraction,
			LanguageVocabulary,
			SpatialSkills,
			Measurement,
			LetterRecognition,
			CommonShapes,
			Sorting,
			Patterning,
			DeletingOnsetAndRime
		}
		
		private static Dictionary<int, KeyValuePair<int, SkillLevel>> demoAssignments;
		
		private static void AddAssignment(int childId, SkillIndex skillId, SkillLevel level)
		{
			demoAssignments[childId] = new KeyValuePair<int, SkillLevel>((int)skillId, level);
		}
		
		private static void PopulateDemoAssignments()
		{
			demoAssignments = new Dictionary<int, KeyValuePair<int, SkillLevel>>();
			// class 1
			AddAssignment(0, SkillIndex.LanguageVocabulary, SkillLevel.Tutorial);
			AddAssignment(1, SkillIndex.InitialSounds, SkillLevel.Tutorial);
			AddAssignment(2, SkillIndex.SentenceSegmenting, SkillLevel.Tutorial);
			AddAssignment(3, SkillIndex.LanguageVocabulary, SkillLevel.Developed);
			AddAssignment(4, SkillIndex.Sorting, SkillLevel.Tutorial);
			AddAssignment(5, SkillIndex.CountingFoundations, SkillLevel.Tutorial);
			AddAssignment(6, SkillIndex.NumeralRecognition, SkillLevel.Tutorial);
			AddAssignment(7, SkillIndex.CommonShapes, SkillLevel.Tutorial);
			AddAssignment(8, SkillIndex.LetterRecognition, SkillLevel.Tutorial);
			AddAssignment(9, SkillIndex.SpatialSkills, SkillLevel.Tutorial);
			
			// class 2
			AddAssignment(10, SkillIndex.BlendingCompoundWords, SkillLevel.Tutorial);
			AddAssignment(11, SkillIndex.Subtraction, SkillLevel.Tutorial);
			AddAssignment(12, SkillIndex.Measurement, SkillLevel.Tutorial);
			AddAssignment(13, SkillIndex.SegmentingCompoundWords, SkillLevel.Tutorial);
			AddAssignment(14, SkillIndex.OnsetRime, SkillLevel.Tutorial);
			AddAssignment(15, SkillIndex.Patterning, SkillLevel.Tutorial);
			AddAssignment(16, SkillIndex.Patterning, SkillLevel.Developed);
			AddAssignment(17, SkillIndex.SequenceCounting, SkillLevel.Tutorial);
			AddAssignment(18, SkillIndex.ObjectsinaSet, SkillLevel.Tutorial);
			AddAssignment(19, SkillIndex.Addition, SkillLevel.Tutorial);

			// class 3
			AddAssignment(20, SkillIndex.Measurement, SkillLevel.Tutorial);
			AddAssignment(21, SkillIndex.ObjectsinaSet, SkillLevel.Tutorial);
			AddAssignment(22, SkillIndex.CountingFoundations, SkillLevel.Tutorial);
			AddAssignment(23, SkillIndex.Subtraction, SkillLevel.Tutorial);
			AddAssignment(24, SkillIndex.CommonShapes, SkillLevel.Tutorial);
			AddAssignment(25, SkillIndex.LanguageVocabulary, SkillLevel.Tutorial);
			AddAssignment(26, SkillIndex.SpatialSkills, SkillLevel.Tutorial);
			AddAssignment(27, SkillIndex.Patterning, SkillLevel.Developed);
			AddAssignment(28, SkillIndex.Patterning, SkillLevel.Tutorial);
			AddAssignment(29, SkillIndex.SequenceCounting, SkillLevel.Tutorial);
			AddAssignment(30, SkillIndex.Sorting, SkillLevel.Tutorial);
			AddAssignment(31, SkillIndex.LanguageVocabulary, SkillLevel.Developed);
			AddAssignment(32, SkillIndex.Addition, SkillLevel.Tutorial);
			AddAssignment(33, SkillIndex.NumeralRecognition, SkillLevel.Tutorial);
			AddAssignment(34, SkillIndex.LetterRecognition, SkillLevel.Tutorial);
		}
		
		public static SkillLevelPair GetDemoAssignment(int childId, IEnumerable<Skill> candidateSkills)
		{
			if(demoAssignments == null) {
				PopulateDemoAssignments();
			}
			
			if(!demoAssignments.ContainsKey(childId)) {
				DebugConsole.LogWarning("No assignment for childId: {0}", childId);
				childId = 0;
			}
			
			var result = demoAssignments[childId];
			var skillId = (int)result.Key;
			var skill = candidateSkills.Filter((Skill s) => s.ID == skillId || s.SPN_ID == skillId).FirstOrDefault();
			
			return new SkillLevelPair(skill, result.Value);
		}
	}
}

