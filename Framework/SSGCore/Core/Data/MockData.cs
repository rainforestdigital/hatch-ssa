using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSGCore
{
	public class MockData
	{
		
		public static string QADEVICE1 = "qaaa-aaaa-aaaa-aaa1";
		public static string QADEVICE2 = "qaaa-aaaa-aaaa-aaa2";
		public static string QADEVICE3 = "qaaa-aaaa-aaaa-aaa3";
		public static string QADEVICE4 = "qaaa-aaaa-aaaa-aaa4";
		
		
		//------------------------------------------------------------------------------------------------------Temporary User Data
		private static string temp_teacher_data = @"{""teachers"":  
											[{""name"":""Jim Jones"", ""children"":[
												{""name"":""Sally Fields"", ""age"":""5""},
												{""name"":""Timmy Dalton"", ""age"":""6""},
												{""name"":""Billy Kidd"", ""age"":""5""},
												{""name"":""Mace Windu"", ""age"":""6""},
												{""name"":""Giorgio Moroder"", ""age"":""4""},
												{""name"":""Marauder Shields"", ""age"":""6""},
												{""name"":""Ron Swanson"", ""age"":""5""}
											]}, 
											{""name"":""Kim Kones"", ""children"":[
												{""name"":""Boba Fett"", ""age"":""7""},
												{""name"":""Dothraki Horselord"", ""age"":""6""},
												{""name"":""Roland DeChain"", ""age"":""5""}
											]}]
										}";
		public static string TempTeacherData{ get{ return temp_teacher_data; } }
		
		public static IList<Child> GenerateTestChildren(Texture texture__) {
			
			var texture = NullImageSource.Instance;
			return new List<Child> {
				new Child(0, "Sally Fields", texture),
				new Child(1,"Timmy Dalton", texture),
				new Child(2,"Zort Helkon", texture),
				new Child(3,"Gleef Bligely", texture),
				new Child(4,"Mace Windu", texture),
				new Child(5,"Giorgio Moroder", texture),
				new Child(6, "Ron Swanson", texture),
				new Child(7, "Aubergine Caruthers", texture),
				new Child(8,"George Masterstone", texture),
				new Child(9,"Acute Intestinal Bleeding", texture)
			};
		}
		
		public static IList<Classroom> GenerateTestClassrooms(Texture texture__) {
			var themes = GenerateTestThemes();
			var texture = NullImageSource.Instance;
			return new List<Classroom> {
				new Classroom("Garden Palace", NullImageSource.Instance,
					NullImageSource.Instance,
					new List<User> {
						new Child(0,"Timmy Dalton", texture),
						new Child(1,"Zort Helkon", texture),
						new Child(2,"Gleef Bligely", texture)
					},
					new List<Theme> {
						themes[0]
				}),
				new Classroom("Overgrown Sneeze", NullImageSource.Instance,
					NullImageSource.Instance,
					new List<User> {
						new Child(0, "Sally Fields", texture),
						new Child(1, "Timmy Dalton", texture),
						new Child(2, "Zort Helkon", texture),
						new Child(3, "Gleef Bligely", texture),
						new Child(4, "Mace Windu", texture),
						new Child(5, "Giorgio Moroder", texture),
						new Child(6, "Ron Swanson", texture),
						new Child(7, "Aubergine Caruthers", texture),
						new Child(8, "George Masterstone", texture),
						new Child(9, "Acute Intestinal Bleeding", texture)
					},
					themes),
				new Classroom("Too Many!", NullImageSource.Instance,
					NullImageSource.Instance,
					new List<User> {
						new Child(0, "Sally Fields", texture),
						new Child(1, "Timmy Dalton", texture),
						new Child(2, "Zort Helkon", texture),
						new Child(3, "Gleef Bligely", texture),
						new Child(4, "Mace Windu", texture),
						new Child(5, "Giorgio Moroder", texture),
						new Child(6, "Ron Swanson", texture),
						new Child(7, "Aubergine Caruthers", texture),
						new Child(8, "George Masterstone", texture),
						new Child(9, "Acute Intestinal Bleeding", texture),
						new Child(10, "Kid 1", texture),
						new Child(11, "Kid 2", texture),
						new Child(12, "Kid 3", texture),
						new Child(13, "Kid 4", texture),
						new Child(14, "Kid 5", texture),
						new Child(15, "Kid 6", texture),
						new Child(16, "Kid 7", texture),
						new Child(17, "Kid 8", texture),
						new Child(18, "Kid 9", texture),
						new Child(19, "Kid 10", texture),
						new Child(20, "Kid 11", texture),
						new Child(21, "Kid 12", texture),
						new Child(22, "Kid 13", texture)
					},
					themes),
			};
		}
		
		public static IList<Theme> GenerateTestThemes() {
			return new List<Theme> {
				new Theme("Travel"),
				new Theme("Garden"),
				new Theme("Safari")
			};
		}
		
		public static List<Skill> GenerateTestSkills(){
		
			return new List<Skill>{ 
				new Skill("Addition"),
				new Skill("LetterRecognition"),
				new Skill("NumeralRecognition")};
		}
		
		public static List<Admin> GenerateTestAdmins(){
		
			return new List<Admin>{ 
				new Admin() {
					Name = "Fake Person",
					Organization = "Evil Tech Support",
					PhoneNumber = "(555) 555-5555",
					Email = "fake-person@example.com"
				},
				new Admin() {
					Name = "Other Guy",
					Organization = "Evil Tech Support",
					PhoneNumber = "(555) 555-5555",
					Email = "other.guy@example.com"
				}
			};
		}
	}
}
