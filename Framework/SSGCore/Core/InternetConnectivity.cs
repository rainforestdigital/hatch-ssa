using System;
using UnityEngine;
using System.Collections;
using HatchFramework;

namespace SSGCore
{
	public class InternetConnectivity : MonoBehaviour
	{
		public float maxTime = 5;
		public float currentTime = 0;
		const float successRetry = 300;
		const float failRetry = 20;
		public static bool hasConnection{ get; private set;}
		
		public void OnEnable(){
			StartCoroutine(ConnectivityLoop());
		}
		

		public IEnumerator ConnectivityLoop(){
			
			while(this.enabled){
				currentTime = 0;
				
				WWW test = new WWW(GlobalConfig.GetProperty("RMS_URL")+"/onlinecheck.php");
				while(!test.isDone){
					
					if(currentTime  > maxTime){
						hasConnection = false;
						Debug.Log("ICONN Timed out!");
						break;	
					}
					yield return null;
				
					currentTime += Time.deltaTime;
				}
				
				if(currentTime < maxTime && String.IsNullOrEmpty(test.error)){

					hasConnection = true && !GlobalConfig.GetProperty<bool>("ForceOffline");
					currentTime = 0;
					
					Ping check;
					float pingTime;
					while(currentTime < successRetry){
						
						pingTime = currentTime;
						check = new Ping("8.8.8.8");//google dns server
						while(!check.isDone){
							
							if( currentTime - pingTime > maxTime ){
								Debug.Log("PING timed out!");
								break;
							}
							yield return null;
							currentTime += Time.deltaTime;
						}
						
						if(!check.isDone){
							hasConnection = false;
						}
						else if(!hasConnection){
							yield return new WaitForSeconds(successRetry / 10f);
							break;
						}
						yield return new WaitForSeconds(successRetry / 10f);
						
						currentTime += Time.deltaTime;
					}
				} else {
					Debug.Log ("ICONN ERROR: " + test.error);
					hasConnection = false;
					yield return new WaitForSeconds(failRetry);
				}
				
				yield return null;
					
			}
		}
	}
}

