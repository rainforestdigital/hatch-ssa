using System;
using UnityEngine;
using HatchFramework;
using pumpkin.display;
using pumpkin.events;
using System.Collections;
using pumpkin.swf;

namespace SSGCore
{
	public class SSGThemeLoadTest : MonoBehaviour
	{		
		public SafariThemeView safari;
		public ThemeController themeController;
			
		public UpgradeManager upgradeManager;
		bool canShowGUI;
		float percent = 0;		
		Texture2D frameImg;

		public void Start()
		{
			upgradeManager = new UpgradeManager(); 
			upgradeManager.OnCompleteEvent += HandleUpgradeManagerOnCompleteEvent;
			
			// delete later - tempGUI
			frameImg = Resources.Load("UI/Frame250x100") as Texture2D;
			AssetLoader.Instance.OnTotalProgressUpdateEvent += delegate(float percentLoaded) 
			{
				percent = percentLoaded;
			};
			
			upgradeManager.CheckForUpdates();
		}

		void HandleUpgradeManagerOnCompleteEvent (bool success)
		{
			Debug.Log("UPGRADE MANAGER COMPLETE EVENT: SUCCESS? " + success);
			canShowGUI = false;
			upgradeManager.OnCompleteEvent -= HandleUpgradeManagerOnCompleteEvent;
			upgradeManager = null;
			Debug.Log ("Preloading safari Asset");
			StartCoroutine( WaitAndCallForCachedSafariAssets() );
		}
		
		private IEnumerator WaitAndCallForCachedSafariAssets()
		{
			yield return new WaitForSeconds(.5f);
			//SSGEngine.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle(CharacterFactory.SAFARI_THEME, null));
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle("SafariTheme", OnSafariAssetsLoaded));
		}
		
		void OnSafariAssetsLoaded(AssetBundle bundle)
		{		
			if( bundle != null )
			{
				Debug.Log ("Safari Assets Loaded");	
			}
			else Debug.LogError("SAFARI ASSETS FAILED TO LOAD FROM CACHE");
		}
		
		public void LoadSafariTheme()
		{
			//HARDCODED SAFARI THEME FOR THIS BUILD
			SafariThemeView themeView = null;//MovieClipFactory.CreateSafariTheme();
			
			if( themeView != null )
			{
				MovieClipOverlayCameraBehaviour.instance.stage.addChild( themeView );

				
				
			}
			
			Theme safariTheme = new Theme("Safari");
			themeController = new SafariThemeController(safariTheme, themeView);
			Debug.Log("*****SAFARI THEME");
			//AssetLoader.Instance.UnloadBundle(CharacterFactory.SAFARI_THEME);
			
			
			/*
			safari = MovieClipFactory.CreateSafariTheme();

			//safari.parent.FitToParent();
			if( safari != null )
			{
				MovieClipOverlayCameraBehaviour.instance.stage.addChild( safari );
				safari.parent.width =  Screen.width;
				safari.parent.height = Screen.height;
				
				safari.width = MainUI.STAGE_WIDTH;
				safari.height = MainUI.STAGE_HEIGHT;
				safari.FitToParent();
				safari.y = Screen.height/2f;
				safari.x = Screen.width/2f;
			}
			Theme theme = new Theme("Safari");
			themeController = new ThemeController(theme, safari);
				
			BasePlatty plat = CharacterFactory.GetSafariPlattyCharacter();
			Debug.Log("theme controller: " + themeController);
			Debug.Log("play: "+ plat);
			themeController.SetHost(plat);	
			*/
		}
		
		public void OnGUI()
		{
			if( canShowGUI ) 
			{
				if( frameImg != null )
				{
					GUILayout.BeginArea(new Rect((Screen.width*.5f) - (frameImg.width*.5f), (Screen.height*.5f)-(frameImg.height*.5f), frameImg.width, frameImg.height), frameImg);
						GUILayout.BeginArea(new Rect(35, 25, 180, 50));
						
							GUI.contentColor = Color.black;
							if( upgradeManager.mode == UpgradeModes.CONFIG_DOWNLOAD )
								GUILayout.Label("Downloading Update Information");
							else
							{
								GUILayout.Label("Downloading Assets");
								GUILayout.Label("Loaded: " + Mathf.FloorToInt(percent*100).ToString() + "%");
							}
						
						GUILayout.EndArea();
					GUILayout.EndArea();
				}
			}
			else
			{
				GUILayout.BeginHorizontal();
					if(GUILayout.Button("LOAD SAFARI THEME"))
						LoadSafariTheme();
				
					if(GUILayout.Button("TURN OFF OVERLAY"))
						themeController.HideOverlay();
					if(GUILayout.Button("TURN ON OVERLAY"))
						themeController.ShowOverlay();
				
				GUILayout.EndHorizontal();
				
				GUILayout.BeginHorizontal();
					if(GUILayout.Button("CORRECT OPPORTUNITY HOST"))
						themeController.PlayCompliment(null);
					if(GUILayout.Button("CORRECT OPPORTUNITY THEME"))
				{	
					Debug.Log("wtf");
						themeController.themeView.CorrectOpportunity(true);
				}

				
				GUILayout.EndHorizontal();
				
				GUILayout.BeginHorizontal();
				GUILayout.EndHorizontal();
				
			}
			
		}
		
	}
}

