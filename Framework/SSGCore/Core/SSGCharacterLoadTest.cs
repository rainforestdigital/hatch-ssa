using System;
using UnityEngine;
using HatchFramework;
using pumpkin.display;
using pumpkin.events;
using System.Collections;

namespace SSGCore
{
	public class SSGCharacterLoadTest : MonoBehaviour
	{		
		public BaseCam cam;
		public BaseHenry henry;
		public BasePlatty platty;
		
		public BaseCharacter currentCharacter;
		
		public UpgradeManager upgradeManager;
		
		public void Start()
		{
			upgradeManager = new UpgradeManager(); 
			upgradeManager.OnCompleteEvent += HandleUpgradeManagerOnCompleteEvent;
			
			// delete later - tempGUI
			frameImg = Resources.Load("UI/Frame250x100") as Texture2D;
			AssetLoader.Instance.OnTotalProgressUpdateEvent += delegate(float percentLoaded) 
			{
				percent = percentLoaded;
			};
			
			upgradeManager.CheckForUpdates();
		}

		void HandleUpgradeManagerOnCompleteEvent (bool success)
		{
			Debug.Log("UPGRADE MANAGER COMPLETE EVENT: SUCCESS? " + success);
			canShowGUI = false;
			upgradeManager.OnCompleteEvent -= HandleUpgradeManagerOnCompleteEvent;
			upgradeManager = null;
			Debug.Log ("Preloading global Asset");
			StartCoroutine( WaitAndCallForCachedGlobalAssets() );
		}
		
		private IEnumerator WaitAndCallForCachedGlobalAssets()
		{
			yield return new WaitForSeconds(.5f);
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle("GlobalAssets", OnGlobalAssetsLoaded));
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle(CharacterFactory.SAFARI_THEME, OnSafariThemeLoaded));
		}
		
		void OnGlobalAssetsLoaded(AssetBundle bundle)
		{		
			if( bundle != null )
			{
				Debug.Log ("Global Assets Loaded");
				LoadDeviceWizard();
			}
			else Debug.LogError("GLOBAL ASSETS FAILED TO LOAD FROM CACHE");
		}
		
		void OnSafariThemeLoaded(AssetBundle bundle)
		{		
			if( bundle != null )
			{
				Debug.Log ("Safari Theme Loaded");
			}
			else Debug.LogError("SAFARI THEME FAILED TO LOAD FROM CACHE");
			
		}
		
		public void LoadDeviceWizard()
		{
			DeviceWizardView dwv = MovieClipFactory.GetDeviceWizardView();
			MovieClipOverlayCameraBehaviour.instance.stage.addChild(dwv);
		}
		
		public void LoadCharacter(CharacterNames name, string theme)
		{
			if( currentCharacter != null )
			{
				MovieClipOverlayCameraBehaviour.instance.stage.removeChild( currentCharacter );
				currentCharacter = null;
			}
			
			switch(name)
			{
				case CharacterNames.Cam:
					switch(theme)
					{
						case CharacterFactory.STANDARD: currentCharacter = CharacterFactory.GetStandardCamCharacter(); break;
						case CharacterFactory.GARDEN: currentCharacter = CharacterFactory.GetGardenCamCharacter(); break;
						case CharacterFactory.SAFARI: currentCharacter = CharacterFactory.GetSafariCamCharacter(); break;
						case CharacterFactory.TRAVEL: currentCharacter = CharacterFactory.GetTravelCamCharacter(); break;
					}
				break;
				
				case CharacterNames.Henry:
					switch(theme)
					{
						case CharacterFactory.STANDARD: currentCharacter = CharacterFactory.GetStandardHenryCharacter(); break;
						case CharacterFactory.GARDEN: currentCharacter = CharacterFactory.GetGardenHenryCharacter(); break;
						case CharacterFactory.SAFARI: currentCharacter = CharacterFactory.GetSafariHenryCharacter(); break;
						case CharacterFactory.TRAVEL: currentCharacter = CharacterFactory.GetTravelHenryCharacter(); break;
					}
				break;
				
				case CharacterNames.Platty:
					switch(theme)
					{
						case CharacterFactory.STANDARD: currentCharacter = CharacterFactory.GetStandardPlattyCharacter(); break;
						case CharacterFactory.GARDEN: currentCharacter = CharacterFactory.GetGardenPlattyCharacter(); break;
						case CharacterFactory.SAFARI: currentCharacter = CharacterFactory.GetSafariPlattyCharacter(); break;
						case CharacterFactory.TRAVEL: currentCharacter = CharacterFactory.GetTravelPlattyCharacter(); break;
					}
				break;
			}
			
			if( currentCharacter != null )
			{
				currentCharacter.x = (Screen.width*.5f) - (currentCharacter.width*.5f);
				currentCharacter.y = (Screen.height*.5f) - (currentCharacter.height*.5f);
				MovieClipOverlayCameraBehaviour.instance.stage.addChild( currentCharacter );
			}
			else Debug.Log("CHARACTER WAS NULL AND DID NOT LOAD");
		}
		
		float percent = 0;		
		Texture2D frameImg;
		bool canShowGUI = true;
		public void OnGUI()
		{
			if( canShowGUI ) 
			{
				if( frameImg != null )
				{
					GUILayout.BeginArea(new Rect((Screen.width*.5f) - (frameImg.width*.5f), (Screen.height*.5f)-(frameImg.height*.5f), frameImg.width, frameImg.height), frameImg);
						GUILayout.BeginArea(new Rect(35, 25, 180, 50));
						
							GUI.contentColor = Color.black;
							if( upgradeManager.mode == UpgradeModes.CONFIG_DOWNLOAD )
								GUILayout.Label("Downloading Update Information");
							else
							{
								GUILayout.Label("Downloading Assets");
								GUILayout.Label("Loaded: " + Mathf.FloorToInt(percent*100).ToString() + "%");
							}
						
						GUILayout.EndArea();
					GUILayout.EndArea();
				}
			}
			else
			{
				GUILayout.BeginHorizontal();
					if(GUILayout.Button("LOAD CAM STANDARD"))
						LoadCharacter(CharacterNames.Cam, CharacterFactory.STANDARD);
					if(GUILayout.Button("LOAD HENRY STANDARD"))
						LoadCharacter(CharacterNames.Henry, CharacterFactory.STANDARD);
					if(GUILayout.Button("LOAD PLATTY STANDARD"))
						LoadCharacter(CharacterNames.Platty, CharacterFactory.STANDARD);
				GUILayout.EndHorizontal();
				
				GUILayout.BeginHorizontal();
					if(GUILayout.Button("LOAD CAM GARDEN"))
						LoadCharacter(CharacterNames.Cam, CharacterFactory.GARDEN);
					if(GUILayout.Button("LOAD HENRY GARDEN"))
						LoadCharacter(CharacterNames.Henry, CharacterFactory.GARDEN);
					if(GUILayout.Button("LOAD PLATTY GARDEN"))
						LoadCharacter(CharacterNames.Platty, CharacterFactory.GARDEN);
				GUILayout.EndHorizontal();
				
				GUILayout.BeginHorizontal();
					if(GUILayout.Button("LOAD CAM SAFARI"))
						LoadCharacter(CharacterNames.Cam, CharacterFactory.SAFARI);
					if(GUILayout.Button("LOAD HENRY SAFARI"))
						LoadCharacter(CharacterNames.Henry, CharacterFactory.SAFARI);
					if(GUILayout.Button("LOAD PLATTY SAFARI"))
						LoadCharacter(CharacterNames.Platty, CharacterFactory.SAFARI);
				GUILayout.EndHorizontal();
				
				GUILayout.BeginHorizontal();
					if(GUILayout.Button("LOAD CAM TRAVEL"))
						LoadCharacter(CharacterNames.Cam, CharacterFactory.TRAVEL);
					if(GUILayout.Button("LOAD HENRY TRAVEL"))
						LoadCharacter(CharacterNames.Henry, CharacterFactory.TRAVEL);
					if(GUILayout.Button("LOAD PLATTY TRAVEL"))
						LoadCharacter(CharacterNames.Platty, CharacterFactory.TRAVEL);
				GUILayout.EndHorizontal();
				
				if( currentCharacter == null ) return;
				
				GUILayout.BeginVertical();
					if(GUILayout.Button("LOOK_RIGHT"))
						currentCharacter.PlayAnimation(BaseCharacter.LOOK_RIGHT);
					if(GUILayout.Button ("LOOK LEFT"))
						currentCharacter.PlayAnimation(BaseCharacter.LOOK_LEFT);
					if(GUILayout.Button ("HAPPY"))
						currentCharacter.PlayAnimation(BaseCharacter.HAPPY);
					if(GUILayout.Button ("SAD"))
						currentCharacter.PlayAnimation(BaseCharacter.SAD);
					if(GUILayout.Button ("RIGHT TALKING"))
						currentCharacter.PlayAnimation(BaseCharacter.RIGHT_TALKING, true);
					if(GUILayout.Button ("RIGHT TALKING LOOP"))
						currentCharacter.PlayAnimation(BaseCharacter.RIGHT_TALKING_LOOP);
					if(GUILayout.Button ("RIGHT TALKING END"))
						currentCharacter.PlayAnimation(BaseCharacter.RIGHT_TALKING_END);
					if(GUILayout.Button ("LEFT TALKING"))
						currentCharacter.PlayAnimation(BaseCharacter.LEFT_TALKING, true);
					if(GUILayout.Button ("LEFT TALKING LOOP"))
						currentCharacter.PlayAnimation(BaseCharacter.LEFT_TALKING_LOOP);
					if(GUILayout.Button ("LEFT TALKING END"))
						currentCharacter.PlayAnimation(BaseCharacter.LEFT_TALKING_END);
					if(GUILayout.Button ("PAYOFF"))
						currentCharacter.PlayAnimation(BaseCharacter.PAYOFF);
					if(GUILayout.Button ("PAYOFF END"))
						currentCharacter.PlayAnimation(BaseCharacter.PAYOFF_END);
					if(GUILayout.Button ("RESTING"))
						currentCharacter.PlayAnimation(BaseCharacter.RESTING);
				GUILayout.EndVertical();
			}
			
		}
	}
}

