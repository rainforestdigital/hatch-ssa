using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using HatchFramework;

namespace SSGCore
{
	public delegate void OnConfigLoaded(string xml);
	public delegate void OnConfigLoadFail(string err);
	
	public class UpgradeManagerLoader : MonoBehaviour
	{
		private bool useLocalResponse = true;
		private static UpgradeManagerLoader instance;
		public static UpgradeManagerLoader Instance
		{
			get
			{
				return instance;
			}
	
			set
			{
				instance = value;
			}
		}
		
		OnConfigLoaded OnConfigLoadedInvoker;
		public event OnConfigLoaded OnConfigLoadedEvent{ add{OnConfigLoadedInvoker += value;} remove{OnConfigLoadedInvoker -= value;}}
		
		OnConfigLoadFail OnConfigLoadFailInvoker;
		public event OnConfigLoadFail OnConfigLoadFailEvent{ add{OnConfigLoadFailInvoker += value;} remove{OnConfigLoadFailInvoker -= value;}}
			
		
		public void Awake()
		{
			instance = this;
		}
	
		public void Start ()
		{
			
		}
	
		public void LoadXMLConfig()
		{
			
			
			StartCoroutine( DoLoadXMLConfig() );
		}
		
		private WWW www = null;
		private IEnumerator DoLoadXMLConfig()
		{
			if(useLocalResponse) 
			{
				yield return new WaitForSeconds(0.5f);
				Object o = Resources.Load("Config/RMSResponse");
				
				if( o != null)
				{
					TextAsset txt = GameObject.Instantiate(o) as TextAsset;
					if( OnConfigLoadedInvoker != null ) OnConfigLoadedInvoker( txt.text );
				}
				else
				{
					DebugConsole.LogWarning("NO XML FOUND FOR THIS APPLICATION -- LOCATING LOCAL RMSResponse.xml");	
					if( OnConfigLoadFailInvoker != null ) OnConfigLoadFailInvoker("NO XML FOUND FOR THIS APPLICATION -- LOCATING LOCAL RMSResponse.xml");
				}
			}
			else
			{
				www = new WWW(GlobalConfig.GetProperty<string>("RMS_URL")+"RMSResponse.xml");
				
				yield return www;
				
				if( !string.IsNullOrEmpty(www.error) )
				{
					DebugConsole.LogError("WWW ERROR WHILE LOADING CONFIG FROM RMS: " + www.error);
					if( OnConfigLoadFailInvoker != null ) OnConfigLoadFailInvoker("NO XML FOUND FOR THIS APPLICATION: "+ www.error);
				}
				else
				{
					//if( OnConfigLoadedInvoker != null ) OnConfigLoadedInvoker( (www. as TextAsset).text );
				}
			}
			
		}
	}
}
