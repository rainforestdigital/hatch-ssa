using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using HatchFramework;

/// <summary>
/// Upgrade manager.  Responsible for managing version control of bundles and that application
/// </summary>
namespace SSGCore
{
	public delegate void OnComplete(bool success); 
	public delegate void OnConfigDownloadStart();
	public delegate void OnConfigDownloadComplete(UpdatesDataObject result);
	
	public enum UpgradeModes:int{ CONFIG_DOWNLOAD, ASSETBUNDLE_DOWNLOAD };
	
	public class UpgradeManager
	{
		OnComplete OnCompleteInvoker;
		public event OnComplete OnCompleteEvent{ add{OnCompleteInvoker += value;} remove{OnCompleteInvoker -= value;} }
		//public event OnConfigDownloadStart OnConfigDownloadStartEvent;
		OnConfigDownloadComplete OnConfigDownloadInvoker;
		public event OnConfigDownloadComplete OnConfigDownloadCompleteEvent { add{OnConfigDownloadInvoker += value;} remove{OnConfigDownloadInvoker -= value;} }
		
		public UpgradeModes mode = UpgradeModes.CONFIG_DOWNLOAD;
		
		
		private Dictionary<string, string> bundlesToDownload;
		
		public UpgradeManager()
		{

			//Create an assetloader if one has not been created yet
			if(AssetLoader.Instance == null){
				GameObject loader = new GameObject("AssetLoader");
				loader.AddComponent<AssetLoader>();
				
			}
		}
		
		public void CheckForUpdates()
		{
			if(GlobalConfig.GetProperty<bool>("useLocalAssetConfig")){
				DoUpdates((Resources.Load("Config/RMSResponse") as TextAsset).text);
			}else if(!InternetConnectivity.hasConnection){
				OnConfigLoadFailed("No Internet");
			}else{
				RMS.DataSync.Instance.SyncBundles(OnSyncBundlesComplete);
			}

		}
		
		private void OnSyncBundlesComplete(bool status){
			
			if(!status){
				OnConfigLoadFailed(RMS.Webservice.GetAppBundle.error);
			}else{
				DoUpdates(RMS.Webservice.GetAppBundle.fetched);
			}
			
		}
				
		private void OnConfigLoadFailed(string reason)
		{
			var previousBundles = GetStoredVersions();
			
			// If we have stored bundles, use previous config results
			if(previousBundles.Count > 0) {
				var result = new UpdatesDataObject();
				result.bundleList = previousBundles;
				bundlesToDownload = previousBundles;
				if(OnConfigDownloadInvoker != null) {
					OnConfigDownloadInvoker(result);
				}
			}
			else {
				if( OnCompleteInvoker != null ) {
					OnCompleteInvoker(false);
				}
			}
		}
		
		public void DoUpdates(string xml)
		{
			// asset bundles that were previously cached are not guaranteed to stay that way.
			// try to load them all unless we filter the cached ones out in advance
			UpdatesDataObject result = new UpdatesDataObject();
			bundlesToDownload = GetStoredVersions();
			
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(xml);			
			XmlNodeList bundleList = xmlDoc.GetElementsByTagName("bundle");
			 
			var application = xmlDoc.GetElementsByTagName("application")[0];
			var applicationVersion = application.Attributes["version"].Value.Replace("_", ".");
			
			result.applicationVersion = new System.Version(applicationVersion);
			
			
			//update RMS if applicable
			if(!GlobalConfig.GetProperty<bool>("useLocalAssetConfig")){
				string newURL = xmlDoc.GetElementsByTagName("bundles")[0].Attributes["rooturl"].Value;
				DebugConsole.Log("Setting root url: "+newURL);
				GlobalConfig.SetProperty("ASSET_BUNDLE_URL", newURL);
				PlayerPrefs.SetString("AssetBundleURL", newURL);
			}
			
			result.forcedApplicationUpdate = application.Attributes["forceupdate"].Value == "true";
			Debug.Log ("bundles : "+bundleList);
			foreach (XmlNode bundleInfo in bundleList)
  			{
				var name = bundleInfo.Attributes["name"].Value;
				var version = bundleInfo.Attributes["version"].Value;
				bundlesToDownload[name] = version;
			}
			
			StoreBundleVersions(bundlesToDownload);
			
			DebugConsole.Log("BUNDLE LIST LENGTH: " + bundleList.Count + ", forceupdate? " + result.forcedApplicationUpdate);
			if(OnConfigDownloadInvoker != null) {
				OnConfigDownloadInvoker(result);
			}
		}
		
		private Dictionary<string, string> GetStoredVersions()
		{
			Dictionary<string, string> storedBundles = new Dictionary<string, string>();
			
			// if the version changes, clear stored bundles in case they are not compatible
			// a new config is required.
			
			if(VersionControl.CurrentVersionString != PlayerPrefs.GetString("PreviousAppVersion") || GlobalConfig.GetProperty<bool>("clearAssetCache")) {
				DebugConsole.Log("Clearing Stored Bundle Versions");
				PlayerPrefs.SetString("BundleVersions", "");
			}else if( !GlobalConfig.GetProperty<bool>("useLocalAssetConfig") ) {
				string url = PlayerPrefs.GetString("AssetBundleURL");
				if( !string.IsNullOrEmpty(url) )
					GlobalConfig.SetProperty("ASSET_BUNDLE_URL", url);
			}
			
			string bundleVersions = PlayerPrefs.GetString("BundleVersions");
			string[] bundles = bundleVersions.Split(',');
			
			foreach( string s in bundles )
			{
				string[] a = s.Split('|');
				if(a.Length == 2) {
					storedBundles.Add(a[0], a[1]);
				}
			}
			
			return storedBundles;
		}

		private void StoreBundleVersions (Dictionary<string, string> storedVersions)
		{
			var builder = new StringBuilder();
			foreach( KeyValuePair<string,string> pair in storedVersions )
			{
				builder.Append(pair.Key);
				builder.Append('|');
				builder.Append(pair.Value);
				builder.Append(',');
			}
			
			PlayerPrefs.SetString("BundleVersions", builder.ToString());
			PlayerPrefs.SetString("PreviousAppVersion", VersionControl.CurrentVersionString);
		}
		
		public void DownloadUpdatedAssetBundles()
		{
			DownloadUpdatedAssetBundles(bundlesToDownload);
		}
		
		private void DownloadUpdatedAssetBundles(Dictionary<string, string> list) // bundleName, version
		{
			AssetLoader.Instance.OnInitialAssetBundlesLoadedEvent += HandleAssetLoaderInstanceOnInitialAssetBundlesLoadedEvent;
			AssetLoader.Instance.LoadAssetBundleList(list);
			mode = UpgradeModes.ASSETBUNDLE_DOWNLOAD;
		}
		
		void HandleAssetLoaderInstanceOnInitialAssetBundlesLoadedEvent (bool success)
		{
			if( OnCompleteInvoker != null ) OnCompleteInvoker(success);
			
		}
	}
}