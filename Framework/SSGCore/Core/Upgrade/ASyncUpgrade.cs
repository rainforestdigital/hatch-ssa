using System;
using System.IO;
using System.Net;
using System.Collections;
using System.Security.Cryptography;
using UnityEngine;
using HatchFramework;
using LitJson;

namespace SSGCore
{
	public class ASyncUpgrade
	{
		private HttpWebRequest httpRequest;
		private bool requestWait;
		private bool saveWait;
		private WebResponse httpResponse;
		private Stream responseStream;
		private int ioVal;
		private string updateUrl = GlobalConfig.GetProperty("RMS_URL") + "WebService/Updates_aioV2?device_uid=";
		private Action returnCall;
		private int downloaded;
		private int totalBytes;
		private FileStream dataOut;
		private byte[] buffer;
		
		public ASyncUpgrade( string uid, Action callback )
		{
			ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => { return true; };
			
			returnCall = callback;
			
			if(PlayerPrefs.HasKey("AIO_Update_Ready") && File.Exists(Application.dataPath + "/UpdateAdventures.exe"))
				returnCall();
			
			SSGEngine.Instance.StartCoroutine(runUpdate( uid ));
		}
		
		private IEnumerator runUpdate ( string uid )
		{			
			string[] version = GlobalConfig.GetProperty("version").Split('.');
			int[] verNum = new int[version.Length];
			for(int i = 0; i < version.Length; i++)
			{
				if( !int.TryParse(version[i], out verNum[i]) )
				{
					DebugConsole.LogError("UPDATE CANCELLED - INVALID LOCAL VERSION FORMAT - Part " + i + " is NaN");
					if(returnCall != null)
					{
						returnCall();
						returnCall = null;
					}
					yield break;
				}
			}
			
			WWW www = new WWW( updateUrl + uid );
			
			yield return www;
			
			if(!string.IsNullOrEmpty(www.error))
			{
				DebugConsole.LogError("UPDATE CANCELLED - Could not acquire server data because of an error: " + www.error);
				if(returnCall != null)
					returnCall();
			}
			
			UpdatesData serverData = JsonMapper.ToObject<UpdatesData>(www.text);
			www = null;
			
			if(serverData.GetNewVersion() == null)
			{
				if(returnCall != null)
				{
					returnCall();
					returnCall = null;
				}
			}
			
			string filePath = Application.dataPath + "/UpdateAdventures.exe";
			string dataPath = filePath + ".part";
			
			//check for download in progress, compare version in progress to listed on server
			
			bool inProgress = PlayerPrefs.HasKey("AIO_Progress") && File.Exists(dataPath);
			
			if(inProgress && (!PlayerPrefs.HasKey("AIO_Version_In_Progress") || PlayerPrefs.GetString("AIO_Version_In_Progress") != serverData.whats_new ||
				!PlayerPrefs.HasKey("AIO_MD5SUM") || PlayerPrefs.GetString("AIO_MD5SUM") != serverData.md5sum))
			{
				inProgress = false;
				PlayerPrefs.DeleteKey("AIO_Progress");
				PlayerPrefs.DeleteKey("AIO_Version_In_Progress");
				PlayerPrefs.DeleteKey("AIO_MD5SUM");
				//Wipe data, if we're downloading anything, it's coming from the server link
				File.Delete(filePath);
			}
			
			if(!inProgress && !verIsHigher(verNum, serverData.GetNewVersion()))
			{
				if(returnCall != null) returnCall(); //There's nothing to do here, destroy the object's external reference
				DebugConsole.Log("UPDATE CANCELED - Nothing to download");
				yield break;
			}
			
			dataOut = File.Open( dataPath, FileMode.OpenOrCreate );
			
			//Get total size of the update file through http head			
			httpRequest = (HttpWebRequest)WebRequest.Create(serverData.download_url);
			httpRequest.Proxy = null;
			httpRequest.Method = "HEAD";
			
			totalBytes = 0;
			using(WebResponse resp = httpRequest.GetResponse())
			{
				totalBytes = (int)resp.ContentLength;
			}
			if(totalBytes <= 0)
			{
				DebugConsole.LogError("UPDATE CANCELLED - File is invalid or has an invalid header");
				destroy();
				yield break;
			}
			
			if(inProgress)
				downloaded = PlayerPrefs.GetInt("AIO_Progress");//start at last point that a write action finished completely
			else{
				downloaded = 0;
				PlayerPrefs.SetString("AIO_Version_In_Progress", serverData.whats_new);
				PlayerPrefs.SetString("AIO_MD5SUM", serverData.md5sum);
			}
			
			DebugConsole.Log("ASync Update download in progress: {0} out of {1} bytes.", downloaded, totalBytes);
			
			httpRequest = (HttpWebRequest)WebRequest.Create(serverData.download_url);
			httpRequest.Proxy = null;
			
			if(downloaded != 0)
				httpRequest.AddRange(downloaded);
			
			requestWait = true;
			
			//Send for the request, this happenes on another thread, so this function hangs out while it calls
			httpRequest.BeginGetResponse(new AsyncCallback(FinishWebRequest), null);
			
			while(requestWait)
			{
				yield return new WaitForSeconds(0.05f);
			}
			saveWait = true;
			
			SSGEngine.Instance.StartCoroutine( readFromResponse( httpResponse ) );
			
			while(saveWait)
			{
				yield return new WaitForSeconds(2.5f);
			}
			
			//clean up
			dataOut.Close();
			dataOut = null;
			
			if(File.Exists(filePath))
				File.Delete(filePath);
			
			File.Move(dataPath, filePath);
			
			PlayerPrefs.DeleteKey("AIO_Progress");
			PlayerPrefs.DeleteKey("AIO_Version_In_Progress");
			PlayerPrefs.DeleteKey("AIO_MD5SUM");
			
			if( testMD5(filePath, serverData.md5sum) )
			{
				DebugConsole.Log("ASync Update Download Complete, ready for execution.");
				//The rest will be called the next time SSG is opened
				PlayerPrefs.SetString("AIO_Update_Ready", "true");
				if(returnCall != null)
				{
					returnCall();
					returnCall = null;
				}
			}
			else
			{
				DebugConsole.Log("Async Update Failed - Invalid MD5");
				//something's gone pear-shaped, wipe it and try again
				File.Delete(filePath);
			}
		}
		
		private void FinishWebRequest(IAsyncResult result)
		{
			//pass results back into a coroutine
			httpResponse = httpRequest.EndGetResponse(result);
			requestWait = false;
		}
		
		private IEnumerator readFromResponse( WebResponse response )
		{
			//pull out the data stream
			responseStream = response.GetResponseStream();
			
			while(totalBytes > downloaded)
			{
				int step = totalBytes - downloaded;
				buffer = new byte[step];
				
				ioVal = -1;
				//read the data into the buffer
				responseStream.BeginRead(buffer, 0, step, new AsyncCallback(streamHasRead), null);
				
				//pause for the async read
				while(ioVal < 0)
				{
					yield return new WaitForSeconds(0.01f);
				}
				//just in case it didn't finish, check how much it said it moved
				
				if(ioVal == 0)
					continue;
				
				step = ioVal;
				
				dataOut.Seek(downloaded, SeekOrigin.Begin);
				//write the data to disk
				dataOut.BeginWrite(buffer, 0, step, new AsyncCallback(streamWrote), null);
				
				while(ioVal > 0)
				{
					yield return new WaitForSeconds(0.01f);
				}
				
				downloaded += step;
				PlayerPrefs.SetInt("AIO_Progress", downloaded);
			}
			
			//clean up
    		responseStream.Close();
			responseStream = null;
			
			response.Close();
			
			saveWait = false;
		}
		
		private void streamHasRead(IAsyncResult result)
		{
			//set amount that's been read from the stream to continue the read loop
			ioVal = responseStream.EndRead(result);
		}
		
		private void streamWrote(IAsyncResult result)
		{
			//clean up and send it all back - this write is done, move on to the next request
			dataOut.EndWrite(result);
			
			ioVal = 0;
		}
		
		public void destroy()
		{
			if(httpRequest != null)
				httpRequest.Abort();
			if(responseStream != null)
				responseStream.Close();
			if(dataOut != null)
				dataOut.Close();
			if(returnCall != null)
			{
				returnCall();
				returnCall = null;
			}
		}
		
		private bool testMD5( string fileLoc, string originHash )
		{
			byte[] tempBytes = null;
			using (var md5 = MD5.Create())
			{
			    using (var stream = File.OpenRead(fileLoc))
			    {
			        tempBytes = md5.ComputeHash(stream);
			    }
			}
			
			string response;
				
			if(tempBytes != null)
			{
				response = BitConverter.ToString(tempBytes).ToLower();
				response = response.Replace("-", "");
			}
			else
				response = "Not happening I guess";
			
			return response == originHash;
		}
		
		private bool verIsHigher( int[] baseVer, int[] newVer )
		{
			int l = Math.Min(baseVer.Length, newVer.Length);
			for( int i = 0; i < l; i++ )
			{
				if(newVer[i] > baseVer[i])
					return true;
				if(newVer[i] < baseVer[i])
					return false;
			}
			while( newVer.Length > l)
			{
				if(newVer[l] > 0)
					return true;
				l++;
			}
			return false;
		}
	}
	
	public class UpdatesData
	{
		public string version;
		public string preview;
		public string whats_new;
		public string download_url;
		public string md5sum;
		
		public bool HasPreview()
		{
			return preview != "false";
		}
		
		public int[] GetNewVersion()
		{
			string[] s = (whats_new.Split(' ')[1]).Split('.');
			int[] val = new int[s.Length];
			for(int i = 0; i < s.Length; i++)
			{
				if( !int.TryParse(s[i], out val[i]) )
				{
					DebugConsole.LogError("UPDATE CANCELED - INVALID EXTERNAL VERSION FORMAT - Part " + i + " is NaN");
					return null;
				}
			}
			return val;
		}
		
		public string GetFileName()
		{
			string[] data = download_url.Split('/');
			return data[data.Length - 1];
		}
	}
}