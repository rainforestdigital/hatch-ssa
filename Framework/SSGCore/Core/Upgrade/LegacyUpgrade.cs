using System;
using System.IO;
using UnityEngine;

namespace SSGCore
{
	public class LegacyUpgrade
	{
		public static WWW www;
		public static string lUID;

	
		/// <summary>
		/// Gets the legacy user ID saved on the device.
		/// </summary>
		/// <returns>The legacy user interface.</returns>
		public static string GetLegacyUID(){
			string currentKey;
			#if UNITY_ANDROID
			//TODO: Implement Android decryption
			string code;
			if(File.Exists("sdcard/ssg/uid.txt"))
				code = File.ReadAllText("sdcard/ssg/uid.txt");
			else
				return "";
			Debug.Log(code);
			code = WWW.EscapeURL(code);
			code.Replace("%2f", "/");
        	string url = "https://rms.hatchearlychildhood.com/aes_convert.php?encryptedText='" + code + "'";
			Debug.Log(url);
        	www = new WWW(url);
			return "";
			#elif UNITY_IPHONE
			//There is no legacy support for ios
			DebugConsole.Log("LegacyUpgrade - will not upgrade - 3.0 earlier iOS version");
			return "";
			#else
			DebugConsole.Log("LegacyUpgrade - Checking for legacy versions of SSG");
			//must be windows!
			//check to make sure we are on windows and not in debug
			if(Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor)
			{
				currentKey = (string)Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\HatchSSG","DeviceUid", "");
				Debug.Log ("LegacyUID: '"+currentKey+"'");
				return currentKey;
			}
			else
			{
			//no UID on OSX if we dont want - you can add debug here for testing
				return "";
			}
			#endif

		}
	}

}

