
using UnityEngine;
using System.Collections.Generic;
using HatchFramework;
using pumpkin.display;
using pumpkin.events;
using System.Diagnostics;
using System.Collections;
using System.IO;
using pumpkin.swf;
using pumpkin;


namespace SSGCore
{
	public delegate void OnGlobalAssetsCached(AssetBundle bundle);
	
	/// <summary>
	/// Core engine for functionality
	/// </summary>
	[RequireComponent(typeof(MovieClipOverlayCameraBehaviour))]
	public class SSGEngine : MonoBehaviour
	{
		
#if UNITY_ANDROID
		public static string DLL_PLATFORM="ANDROID";
#elif UNITY_IPHONE
		public static string DLL_PLATFORM="IOS";
#else
		public static string DLL_PLATFORM="WIN";
#endif
		
		private static SSGEngine instance;
		public static SSGEngine Instance
		{
			get
			{
				return instance;
			}
			
			set
			{
				instance = value;
			}
		}
		
		OnGlobalAssetsCached OnGlobalAssetsCachedInvoker;
		public event OnGlobalAssetsCached OnGlobalAssetsCachedEvent{ add{OnGlobalAssetsCachedInvoker += value;} remove{OnGlobalAssetsCachedInvoker -= value;} }
		
		protected MainUI main;
		protected UpgradeManager upgradeManager;
		private UpdateView updateView;
		private IntroAnimation introAnim;
		protected Stage stage;

		// -------------------------------------
		// 			INACTIVITY TIMER
		// -------------------------------------	
		private const float inactivityTime = 60*5;
		private float currentInactivityTime;

		protected SessionController sessionControl;
		public static DBObject databaseObject;
		
		private GameObject swfViewerContainer;
		private UpdatesDataObject currentUpdates;
		private ASyncUpgrade AIOUpgrader;
		private bool AIOUpdateRun;
		
		protected static int UpdateSkips{
		
			get{
				return PlayerPrefs.GetInt("UpdateSkips");	
			}
			set {
				PlayerPrefs.SetInt("UpdateSkips", value);
			}
		}
				
		public void Awake()
		{
			instance = this;
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
			gameObject.AddComponent<InternetConnectivity>();
			gameObject.AddComponent<UniswfTextureMonitor>();
		}
		
		public void Start()
		{
			if( PlatformUtils.GetPlatform()	== Platforms.WIN && !Application.isEditor ) Screen.fullScreen = true;	
			
			TextureManager.allowEditorUnload = true;
			
			DebugConsole.Instance.RegisterBehaviour(this);
			DebugConsole.Instance.enabled = GlobalConfig.GetProperty<bool>("ShowDebugConsole");
			
			//Setup Version Output et. al
			
			string buildConfig = GlobalConfig.GetProperty<string>("buildConfig");
			if(buildConfig == "") buildConfig = "Debug";
			
			if(buildConfig != "Release")
			{
				VersionBanner vb = gameObject.AddComponent<VersionBanner>();
				vb.versionOutput = VersionControl.CurrentVersion+" "+buildConfig;			
			}
			
			bool useSWFViewer = GlobalConfig.GetProperty<bool>("UseSWFViewer");
			if( useSWFViewer )
			{
				// SWF Viewer in debug only
				swfViewerContainer = new GameObject("swfViewerContainer");
				swfViewerContainer.AddComponent<SWFViewer>();
			}
			
			databaseObject = new DBObject();
			databaseObject.Open();
			
			VersionDisplay versionDisplay = gameObject.AddComponent<VersionDisplay>();
			versionDisplay.UpdateText(databaseObject);
			
			DebugConsole.Log(DebugConsole.LogLevel.SYSTEM, "","Running version: "+VersionControl.CurrentVersion+" "+buildConfig);
			
			
			stage = MovieClipOverlayCameraBehaviour.instance.stage;
			
			//Create local save path if it doesnt exist yet!
			
			string path = Path.Combine(Application.persistentDataPath, GlobalConfig.GetProperty<string>("LOCAL_DIR"));
			if(!Directory.Exists(path))
				Directory.CreateDirectory(path);
			
			path = Path.Combine(PlatformUtils.InternalDataPath, GlobalConfig.GetProperty<string>("LOCAL_DIR"));
			if(!Directory.Exists(path))
				Directory.CreateDirectory(path);
			DebugConsole.Log(path);
				
			PlayIntroAnimation ();
			AIOUpdateRun = false;
		}
		
		public void UpdateLater(){
			UpdateSkips++;
			LoadAssetBundles();
		}
		
		public void OnDebugDeviceSelected(string deviceID){
		
			GlobalConfig.SetProperty("DEBUG_DEVICE_ID", deviceID);

		}

		void CreateUpgradeManager ()
		{
			if(upgradeManager == null) {
				upgradeManager = new UpgradeManager(); 
				upgradeManager.OnCompleteEvent += HandleUpgradeManagerOnCompleteEvent;
				upgradeManager.OnConfigDownloadCompleteEvent += HandleConfig;
			}
		}
		
		public void PlayIntroAnimation()
		{
			if(!GlobalConfig.GetProperty<bool>("SkipIntro")){
				introAnim = new IntroAnimation();
				introAnim.OnVideoCompleteEvent += onIntroComplete;
				stage.addChild(introAnim);
			}else{
				StartInitialUpdate();	
			}
		}
		
		private void onIntroComplete()
		{
			stage.removeChild(introAnim);
			introAnim.OnVideoCompleteEvent -= onIntroComplete;
			introAnim = null;

			MovieClipFactory.UnloadTexturesWithPrefix("SplashScreen_v1.swf");
			MovieClipFactory.UnloadSwf(MovieClipFactory.PATH_SPLASH);

			
			StartInitialUpdate();
		}

		public void StartInitialUpdate ()
		{
			//Figure out where to interrupt it for the new thingy or other...
			
			CreateUpgradeManager();
			
			updateView = new UpdateView();
			updateView.UpdateLaterClicked = UpdateLater;
			updateView.UpdateClicked = OpenUpdater;
			updateView.RetryClicked = StartUpdates;
			updateView.ExitClicked = Quit;
			stage.addChild(updateView.View);
			
			AssetLoader.Instance.OnTotalProgressUpdateEvent += updateView.SetProgress;
			
			StartUpdates();
		}
			
		void StartUpdates(){
			CreateUpgradeManager();
			updateView.ShowSpinner();
			upgradeManager.CheckForUpdates();
		}

		void LoadAssetBundles()
		{
			updateView.ShowProgress();
			upgradeManager.DownloadUpdatedAssetBundles();
		}
		
		void OpenUpdater()
		{
			
			UpdateSkips = 0;
			ApplicationUpdater.OpenApplicationUpdater(currentUpdates, updateView);

		}



		
		void HandleConfig(UpdatesDataObject result)
		{
			currentUpdates = result;

			if(PlatformUtils.GetPlatform() != Platforms.ANDROID || VersionControl.CurrentVersion >= result.applicationVersion) {
				//no update is needed... continue on.
				LoadAssetBundles ();
			}
#if !UNITY_IPHONE
			else if(result.forcedApplicationUpdate || UpdateSkips > 3) {
				updateView.ShowRequiredUpgrade();
			}
			else {
				updateView.ShowOptionalUpgrade();
			}
#endif
		}

		void HandleUpgradeManagerOnCompleteEvent (bool success)
		{
			DebugConsole.Log("UPGRADE MANAGER COMPLETE EVENT: SUCCESS? " + success);
			upgradeManager.OnCompleteEvent -= HandleUpgradeManagerOnCompleteEvent;
			upgradeManager = null;
			
			if(success) {
				updateView.HideWindow();
				StartCoroutine( WaitAndCallForCachedGlobalAssets() );
			} else {
				updateView.ShowFailure();
			}
		}
		
		private IEnumerator WaitAndCallForCachedGlobalAssets()
		{
			yield return new WaitForSeconds(.5f);
			AssetLoader.Instance.StartCoroutine(AssetLoader.Instance.PreloadAssetBundle("GlobalAssets", OnGlobalAssetsLoaded));
		}
		
		void OnGlobalAssetsLoaded(AssetBundle bundle)
		{		
			if( bundle != null )
			{
				if( OnGlobalAssetsCachedInvoker != null ) OnGlobalAssetsCachedInvoker(bundle);
				LoadMainUI();
			}
			else
			{
				DebugConsole.LogError("GLOBAL ASSETS FAILED TO LOAD FROM CACHE");
				updateView.ShowFailure();
			}
		}
		
		
		/// <summary>
		/// Loads the main UI and setup timers
		/// </summary>
		void LoadMainUI()
		{	
			
			
			main = MovieClipFactory.CreateMainUI(databaseObject);

			stage.addChild(main);
			if( SWFViewer.Instance != null ) SWFViewer.Instance.AddClip("MainUI", main);
			stage.removeChild(updateView.View);
			updateView.Unload();
			updateView = null;
			

			ResetInactivity ();
			
			if(PlatformUtils.GetPlatform() == Platforms.WIN && !AIOUpdateRun)
			{
				AIOUpdateRun = true;
				AIOUpgrader = new ASyncUpgrade(databaseObject.GetDeviceUid(), closeAsyncUpgrader);
			}
		}
		
		private void closeAsyncUpgrader()
		{
			AIOUpgrader = null;
		}
		
		private void OnApplicationPause()
		{
			if(AIOUpgrader != null)
				AIOUpgrader.destroy();
		}
		private void OnApplicationQuit()
		{
			if(AIOUpgrader != null)
			{
				AIOUpgrader.destroy();
				AIOUpgrader = null;
			}
			
			if( PlatformUtils.GetPlatform() == Platforms.WIN && PlayerPrefs.HasKey("AIO_Update_Ready"))
			{
				string updateFilePath = Application.dataPath + "/UpdateAdventures.exe";	
			
				PlayerPrefs.DeleteKey("AIO_Update_Ready");
				
				if( File.Exists(updateFilePath) )
				{
					Process upgrader = new Process();
					upgrader.StartInfo.FileName = updateFilePath;
					upgrader.Start();
				}
			}
		}
		
		public void Quit()
		{
			if(AIOUpgrader != null)
			{
				AIOUpgrader.destroy();
				AIOUpgrader = null;
			}
			
			if( PlatformUtils.GetPlatform() == Platforms.WIN && PlayerPrefs.HasKey("AIO_Update_Ready"))
			{
				string updateFilePath = Application.dataPath + "/UpdateAdventures.exe";	
			
				PlayerPrefs.DeleteKey("AIO_Update_Ready");
				
				if( File.Exists(updateFilePath) )
				{
					Process upgrader = new Process();
					upgrader.StartInfo.FileName = updateFilePath;
					upgrader.Start();
				}
			}
			
			PlatformUtils.Quit();
		}
		
		
		public void ResetInactivity(){
		
			currentInactivityTime = Time.realtimeSinceStartup;
		
		}
		
		[DebugConsoleMethod("SetInactivityTime", "- set remaining time left to timeout")]
		public void SetRemainingInactivityTime(int seconds){
		
			currentInactivityTime = seconds;
		}
		
		[DebugConsoleMethod("loadskill","loadskill [skill name] [skill level]")]
		public void LoadSkill(string name, int level){
			
			main.LoadSkill(name, debugTheme, level);
		}
		
		protected string debugTheme = "Safari";
		
		[DebugConsoleMethod("settheme","- [theme name] (Travel, Safari) - Set theme for next loadskill")]
		public void SetTheme(string themeName){
			this.debugTheme = themeName;	
		}
		
		[DebugConsoleMethod("clearcache", " - clear local cache of all downloaded content")]
		public void ClearCache(){
		
			Caching.CleanCache();
		}

		[DebugConsoleMethod("showserial", " - show serial for the device")]
		public void ShowSerial(){

			DebugConsole.Log ("Device serial: {0}", PlatformUtils.GetSerial ());
		}

		[DebugConsoleMethod("releasememory", " - release all memory")]
		public void ReleaseMemory(){
		
			UniswfTextureMonitor.clearTextureCache();
			MovieClip.clearContextCache ();
		}
			
		[DebugConsoleMethod("memory", "get list of all loaded objects")]
		public void Debug_Memory(){

			Object[] objects = FindObjectsOfType(typeof (UnityEngine.Object));

			Dictionary<string, int> dictionary = new Dictionary<string, int>();

			foreach(Object obj in objects)
			{
				string key = obj.GetType().ToString();
				if(dictionary.ContainsKey(key))
				{
					dictionary[key]++;
				} 
				else
				{
					dictionary[key] = 1;
				}
			}

			List<KeyValuePair<string, int>> myList = new List<KeyValuePair<string, int>>(dictionary);
			myList.Sort(
				delegate(KeyValuePair<string, int> firstPair,
					KeyValuePair<string, int> nextPair)
				{
					return nextPair.Value.CompareTo((firstPair.Value));
				}
			);

			string results = "";
			foreach (KeyValuePair<string, int> entry in myList)
			{
				results += entry.Key + ": " + entry.Value+"\n";
			}

			DebugConsole.Log (results);

		}

		[DebugConsoleMethod("debug_textures", "get list of textures currently loaded")]
		public void Debug_Textures(){
		
			
			Texture[] textures = Resources.FindObjectsOfTypeAll(typeof(Texture)) as Texture[];
			string results = "Textures ("+textures.Length+"):\n";
			foreach(Texture t in textures){
				results += t.name + " "+t.width+" "+t.height+"\n";
			}
			
			results += "--------- UNISWF TEXTURES ---------\n";
			
			foreach(string k in TextureManager.instance.textures.Keys){
			
					Texture t = TextureManager.instance.textures[k];
					if(t != null)
						results += t.name + "\n";
					else
						results += "NULL TEXTURE for "+k+"\n";
			}
				
			DebugConsole.Log(results);
		}
		
//	public void OnGUI(){
//		GUILayout.Label(currentInactivityTime.ToString(), "box");	
//	}

	
		public void Update(){
	
			
			
			if(main == null) return;
				
			if(Input.GetMouseButtonDown(0))
			{
					ResetInactivity();
			}		
	
	
			if(Time.realtimeSinceStartup - currentInactivityTime >= inactivityTime)
			{
				
				DebugConsole.Log("Timeout has occured.  Resetting for new user");
	
				
				ResetInactivity();
				SoundEngine.Instance.PlayTheme(null);
				main.HandleTimeout();
			}
		}
		
		public void SessionEndMuseum( SkillLevelPair lastSkill ) {
			main.BackToMuseum( lastSkill );
		}
	}
}

