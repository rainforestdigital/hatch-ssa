Shader "Filters/CenterTexture" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		inputScale ("InputScale", float) = 1
		offsetPosition ("Offset", Vector) = (0,0,0,0)
		
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		Pass{
		  CGPROGRAM 

			  #include "UnityCG.cginc"
			  #pragma vertex vert_img
			  #pragma fragment frag
			  
			   float inputScale;
			   float4 offsetPosition;
			   sampler2D _MainTex;
			  
				half4 frag(v2f_img i) : COLOR {
				
					half2 scaler = i.uv.xy/inputScale;
					
					half2 samplerUV = scaler - offsetPosition.xy;
					
					
					
					half4 color = tex2D(_MainTex, samplerUV);
					float inbounds =  step(offsetPosition.x*0.5, i.uv.x)*step(offsetPosition.y*0.5, i.uv.y)
									  * step(i.uv.x, offsetPosition.x*0.5+inputScale)*step(i.uv.y, offsetPosition.y*0.5+inputScale);
					
					color = lerp(half4(0,0,0,1), color, inbounds);
					return color;
				}
				
				
			 ENDCG
		}
	} 
	FallBack "Diffuse"
}
