using UnityEngine;
using System.Collections;

using pumpkin.display;
using pumpkin;
using pumpkin.events;
using pumpkin.text;

public class TestSWFLoader : MovieClipOverlayCameraBehaviour
{
	public static TestSWFLoader Instance;
	
	public Vector3 scale = new Vector3(1,-1,0);
	
	public MovieClip mc;
	/*
	public override void OnStart ()
	{
		base.OnStart ();
		
		Instance = this;
		
		mc = new MovieClip("SSGAssets/UNIVERSAL/Resources/Characters_STANDARD.swf:Platty");
		mc.x = Screen.width - (mc.width+25);
		mc.y = 15;
		mc.addEventListener("OnLookRightComplete", HandleLookRightComplete);
		stage.addChild(mc);
	}
	*/
	private void HandleLookRightComplete(CEvent e)
	{
		Debug.Log("ON LOOK RIGHT COMPLETE: " + e.type);
	}
	
/*	public override void OnUpdate ()
	{
		base.OnUpdate ();
	}
*/
	public void OnGUI()
	{
		if( GUILayout.Button("Look Right") ) mc.gotoAndPlay("LOOK RIGHT");
	}
}