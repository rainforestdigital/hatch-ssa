using UnityEngine;
using System.Collections;
using pumpkin.display;


public class LoadCharacterAnim : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	void OnGUI(){
	
		GUILayout.BeginArea(new Rect(10, 200, 200, 20000));
		if(GUILayout.Button("Make henry")){
			MovieClip mc = new MovieClip("SSGAssets/UNIVERSAL/Resources/Characters_SAFARI.swf:Henry");
			MovieClipOverlayCameraBehaviour.instance.stage.addChild(mc);
			mc.play();
		}
		GUILayout.Label("All " + FindObjectsOfTypeAll(typeof(UnityEngine.Object)).Length);
		GUILayout.Label("Textures " + FindObjectsOfTypeAll(typeof(Texture)).Length);
		GUILayout.Label("AudioClips " + FindObjectsOfTypeAll(typeof(AudioClip)).Length);
		GUILayout.Label("Meshes " + FindObjectsOfTypeAll(typeof(Mesh)).Length);
		GUILayout.Label("Materials " + FindObjectsOfTypeAll(typeof(Material)).Length);
		GUILayout.Label("GameObjects " + FindObjectsOfTypeAll(typeof(GameObject)).Length);
		GUILayout.Label("Components " + FindObjectsOfTypeAll(typeof(Component)).Length);
		if(GUILayout.Button("Clear memory")){
			Resources.UnloadUnusedAssets();	
		}
		if(GUILayout.Button("Clear GC")){
			System.GC.Collect();
		}
		GUILayout.EndArea();
	}
	
	
}
