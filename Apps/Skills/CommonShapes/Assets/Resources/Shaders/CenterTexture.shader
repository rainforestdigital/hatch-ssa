Shader "Filters/CenterTexture" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		inputScale ("InputScale", float) = 1
		offsetPosition ("Offset", Vector) = (0,0,0,0)
		drawRect ("Offset", Vector) = (0,0,0,0)
		
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		Pass{
		  CGPROGRAM 

			  #include "UnityCG.cginc"
			  #pragma vertex vert_img
			  #pragma fragment frag
			  
			   float inputScale;
			   float4 offsetPosition;
			   float4 drawRect;
			   sampler2D _MainTex;
			  
				half4 frag(v2f_img i) : COLOR {
				
					half2 scaler = i.uv.xy/inputScale;
					
					half2 samplerUV = scaler - offsetPosition.xy;
					
					float inbounds =  step( 0, samplerUV.x)*step(0, samplerUV.y)*step( samplerUV.x, 1)*step(samplerUV.y, 1);
					
					samplerUV.x = samplerUV.x*(drawRect.z)+drawRect.x;
					samplerUV.y = samplerUV.y*(drawRect.w)+drawRect.y;
					
					//samplerUV.y = 1-samplerUV.y;
					
					half4 color = tex2D(_MainTex, samplerUV);
					 
					
					
					color = lerp(half4(0,0,0,0), color, inbounds);
					return color;
				}
				
				
			 ENDCG
		}
	} 
	FallBack "Diffuse"
}
