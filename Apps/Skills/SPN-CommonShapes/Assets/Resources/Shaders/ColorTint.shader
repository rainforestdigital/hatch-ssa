Shader "Filters/ColorTint" {
    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _Color("Color", Color) = (1,1,1,1)
    }
    SubShader {
        Pass {
        	
            // Apply base texture
            SetTexture [_MainTex] {
            	constantColor[_Color]
                combine constant, constant*texture
            }
            
            
         
        }
    }
} 