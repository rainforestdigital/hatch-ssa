using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Linq;

public class MyAllPostprocessor : AssetPostprocessor {
    static void OnPostprocessAllAssets ( string[] importedAssets,  string[] deletedAssets  ,  string[] movedAssets , string[] movedFromAssetPaths ) {
    
		if(importedAssets.Any(x=>x.Contains("SSGCore.dll") || x.Contains("HatchFramework.dll"))){
		
			Debug.Log ("Updated core/framework dlls");
			//new DLL - validate build version matches platform!
			string dllPlatform = SSGCore.SSGEngine.DLL_PLATFORM;
#if UNITY_ANDROID
			if(dllPlatform != "ANDROID") Debug.LogError("DLL PLATFORM DOES NOT MATCH BUILD PLATFORM: "+dllPlatform);
#elif UNITY_IPHONE
			if(dllPlatform != "IOS") Debug.LogError("DLL PLATFORM DOES NOT MATCH BUILD PLATFORM: "+dllPlatform);
#else
			if(dllPlatform != "WIN") Debug.LogError("DLL PLATFORM DOES NOT MATCH BUILD PLATFORM: " +dllPlatform);
#endif

			
			
		}
		
		
    }
}