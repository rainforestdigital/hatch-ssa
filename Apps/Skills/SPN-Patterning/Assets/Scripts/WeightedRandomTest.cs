using UnityEngine;
using System.Collections;
using HatchFramework;
using System.Collections.Generic;

public class WeightedRandomTest : MonoBehaviour {
	static IEnumerator TestSelectRandom ()
	{
		List<int> values = new List<int> { 0, 1, 2, 3, 4 };
		List<int> frequencies = new List<int> { 0, 0, 0, 0, 0 };
		
		const int sampleCount = 100 * 1000;
		for (int i = 0; i < sampleCount; ++i) {
			int result = values.SelectWeighted(GetWeight);
			frequencies[result]++;
			
			if ((i%100) == 0) {
				yield return new WaitForFixedUpdate();
			}
		}
		
		for(int i = 0; i < values.Count; ++i) {
			Debug.Log(string.Format("{0} was selected {1} times", values[i], frequencies[i]));
		}
	}

	// Use this for initialization
	void Start () {
		StartCoroutine(TestSelectRandom ());
	}
	
	static int GetWeight(int val) {
		return val;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
