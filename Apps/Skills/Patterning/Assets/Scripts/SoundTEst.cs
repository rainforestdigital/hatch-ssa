using UnityEngine;
using System.Collections;
using HatchFramework;

public class SoundTEst : MonoBehaviour {
	
	public AudioClip[] clips;
	public AudioClip[] sourceClips;
	public int lastRnd;
	// Use this for initialization
	void Start () {
	
	
	}
	
	
	
	void OnGUI(){
	
		if(GUILayout.Button("Play Sound", GUILayout.Height(200))){
		
			lastRnd = Random.Range(0,2);
			int rnd = 2+lastRnd;
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			bundle.AddClip(clips[0]);
			bundle.AddClip(clips[rnd]);
			bundle.AddClip(clips[1]);
			bundle.AddClip(clips[rnd]);
			SoundEngine.Instance.PlayBundle(bundle);
		}
		if(GUILayout.Button("Play Source Sound",  GUILayout.Height(200))){
		
			
			SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
			bundle.AddClip(sourceClips[lastRnd]);
			
			SoundEngine.Instance.PlayBundle(bundle);
		}
	}
	
	
}
