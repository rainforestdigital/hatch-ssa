uUnit 2.0.0.0 (Release 2013-08-24)

Changed : Engine rewritten to support Yielding tests
Added   : Lots of additional Assertions
Added   : Moq dll and usage information

Known Issues

- Command line functionality is no longer via a static class and so will be harder to use.
- Some assertion messages are longer than they need to be.


uUnit 1.2.0.0 (Released 2013-05-16)

Changed : TestManagerBase will now discover unit tests in c# and Javascript script files automatically.
Added   : Assert.Throws<>() as per nUnit
Added   : [Values], [Range] & [Random] parameter attributes as per nUnit 
Docs    : Added Getting started / Step by Step guide to using uUnit.
Docs    : Added information for UnityScript / Javascript users.
Docs    : Improved Assert section to include all methods available.
Docs    : Added licence and URL for bundled Simple.Mocking.
Removed : Example unit test script files.

Known Issues

- Long running tests may 'hang' the UI during execution due to uUnits change of threading model. The UI will become responsive 
again after the test has finished.

uUnit 1.1.0.0 (Released 2012-10-02)

Changed : Test executor now runs in a co-routine rather than .net native threads.
Changed : [Ignore] attribute can now be applied to [TestFixture] as well as [Test].
Added   : Clickable column headers for sorting results
Added   : Expected Exception test output messages are more specific and descriptive.
Added   : Game Objects created during tests are now automatically tidied up between tests (configurable).
Fixed   : Corrected Assertion message display in test output
Fixed   : Test setup methods are run from base classes first, then derived classes.

Uses icons from "RRZE Icon Pack by Bdate Kaspar/Franziska Sponsel"

uUnit 1.0.0.0 (Released 2012-06-29)

Initial Release