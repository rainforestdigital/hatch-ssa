using UnityEngine;
using System.Collections;
using pumpkin;
using pumpkin.display;

public class SSGCharacterLoaderInitializer : MonoBehaviour 
{
	// Use this for initialization
	public void Start () 
	{
		Debug.Log("GAMEOBJECT NULL? " + (gameObject == null));
		gameObject.AddComponent<SSGCore.SSGCharacterLoadTest>();
		Destroy (this);
		
	}	
}
