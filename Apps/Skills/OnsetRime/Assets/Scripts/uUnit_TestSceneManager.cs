using System;
using System.Reflection;
using System.Collections.Generic;

public class uUnit_TestSceneManager : TestManagerBase
{

    /// <summary>
    /// Used to replace default uUnit behaviour of finding all unit tests in the Unity project scripts.
    /// </summary>
    protected override System.Reflection.Assembly[] TestAssembly
    {
        get
        {
			var testAssemblies = new List<Assembly>();
			var externalTests = Assembly.Load("SSGUnitTests");
			testAssemblies.Add(externalTests);
			// Include the standard classes
			testAssemblies.AddRange(base.TestAssembly);
			return testAssemblies.ToArray();
			
        }
    }

	
	

}
