using UnityEngine;
using System.Collections;
using SSGCore;

public class SkilSelectionTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
		StartCoroutine(TestSkillSelection());
	
	}
	
	float RandomResult(SkillLevel level) {
		if(level == SkillLevel.Tutorial) {
			return (Random.Range(0, 10) < 8) ? 1 : 0;
		}
		else {
			const int trials = 10;
			int successes = 0;
			for (int i = 0; i < trials; i++) {
				if(Random.Range(0, 10) < 7) {
					successes++;
				}
			}
			
			return successes / (float)trials;
		}
	}
	
	IEnumerator TestSkillSelection() {
		
		var context = new TestSkillContext();
		var session = new SkillSession(context);
		
		var next = session.ChooseFirstSkill();
		Debug.Log(string.Format("Got Skill: {0} {1}", next.Skill.linkage, next.Level));
		
		while (next != null) {
			float result = RandomResult(next.Level);
			Debug.Log(string.Format("Result: {0}", result));
			
			next = session.ChooseSkillForResult(next.Skill, next.Level, result);
			Debug.Log(string.Format("Got Skill: {0} {1}", next.Skill.linkage, next.Level));
			yield return new WaitForEndOfFrame();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
