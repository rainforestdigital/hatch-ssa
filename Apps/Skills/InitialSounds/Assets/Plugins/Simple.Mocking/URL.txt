Simple.Mocking is an open source mocking library available at:

http://simpledotnet.codeplex.com/

The version bundled with uUnit v.1.2.0.0:

Simple Mocking v1.1.0.2 .Net 3.5 (2012-05-04)

Simple.Mocking is provided under the Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0.html)

Please note: Use of Simple.Mocking is not covered by the Unity3d License agreement, you should read and understand the Apache 2.0 license before using Simple.Mocking.