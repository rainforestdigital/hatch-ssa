using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
    
public class MyBuildPostprocessor
{
	[PostProcessBuild]
	public static void OnPostprocessBuild (BuildTarget target, string pathToBuiltProject)
	{
		if (target == BuildTarget.StandaloneWindows) {
			FileUtil.CopyFileOrDirectory ("Chromium", System.IO.Path.GetDirectoryName(pathToBuiltProject)+ "/Chromium");
			Debug.Log (pathToBuiltProject);
		}
	}
}