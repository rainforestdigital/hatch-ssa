using UnityEngine;
using System.Collections;
using pumpkin;
using pumpkin.display;
/// <summary>
/// SSG engine Initializer - creates the SSGEngine and attaches to the current Behavior
/// </summary>

[RequireComponent(typeof(MovieClipOverlayCameraBehaviour))]
public class SSGEngineInitializer : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
		gameObject.AddComponent<SSGCore.SSGEngine>();
		
		Destroy (this);
		
	}
	
	
}
