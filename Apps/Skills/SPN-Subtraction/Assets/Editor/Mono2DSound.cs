using UnityEngine;
using System.Collections;
using UnityEditor;

public class Mono2DSound : AssetPostprocessor {
    void OnPreprocessAudio () {
        AudioImporter audioImporter = (AudioImporter)assetImporter;
        audioImporter.forceToMono = true;
		audioImporter.threeD = false;
    }
}
