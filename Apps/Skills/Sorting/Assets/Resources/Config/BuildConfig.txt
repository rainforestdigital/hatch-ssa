#VERSIONS
version = 0.1.4.2

#buildConfig - Debug | QA | Release
buildConfig = Debug
buildDate = 06-31-2013

#AssetLoader
loadAssetsFromRMS = false
clearAssetCache = false

#HATCH Properties
RMS_URL = http://184.106.133.84/


#DEBUG
DebugXMLConfigName = "RMSResponse.xml";
MuteThemeSong = true

#LogMode - LOG | CONSOLE
LogMode = LOG

#ENTRY
SkipIntro = true
#EntryPoints - Classrooms, Eula, Session [family] [level]
EntryPoint = Session Sorting 4