using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class HUDFPS : MonoBehaviour
{
	public float updateInterval = 0.5F;	 
	private float accum = 0;
	private int   frames = 0;
	private float timeleft;
	private string s = " ";
	
	private float min = 0;
	private float max = 0;
	private float avg = 0;
	
	protected List<float> accumulatedFPS;
	
	protected bool running = false;
	protected float runningTime = 10;
	
	protected bool visible = false;
	
	void StartAction()
	{	 
	    timeleft = updateInterval;  
		running = true;
		accumulatedFPS = new List<float>();
		runningTime = 10;
	}
	 
	void Update()
	{
	    timeleft -= Time.deltaTime;
	    accum += Time.timeScale / Time.deltaTime;
	    ++frames;

	    if( timeleft <= 0.0 ) {
		    float fps = accum / frames;
		    string format = fps.ToString() + " FPS";
		   	s = format;
		
	        timeleft = updateInterval;
	        accum = 0.0F;
	        frames = 0;
			
			
			if(running){
			
				accumulatedFPS.Add(fps);
				min = accumulatedFPS.Min();
				max = accumulatedFPS.Max();
				avg = accumulatedFPS.Average();
				
				
			}
			
	    }
		
		if(running){
			runningTime -= 1*Time.deltaTime;
			if(runningTime < 0)
				running = false;
		}
		
		if(Input.GetKeyUp(KeyCode.F)){
		
			visible = !visible;
		}
		
		
			
	}
	
	void OnGUI() {
		
		if(!visible)
			return;
		
		GUILayout.BeginArea(new Rect(10, 10, 200, 200), GUI.skin.box);
		
		if(!running){
		
			if(GUILayout.Button("Run Test")){
					StartAction();
			}
		}else{
			GUILayout.Label("RUNNING: "+runningTime.ToString("f2"));	
		}
		GUILayout.Label(s.ToString());
		GUILayout.Label("---------------------");
		GUILayout.Label("AVG: "+avg.ToString("f2"));
		GUILayout.Label("MIN: "+min.ToString("f2"));
		GUILayout.Label("MAX: "+max.ToString("f2"));
		GUILayout.EndArea();
		
		
	}
		
}
	
