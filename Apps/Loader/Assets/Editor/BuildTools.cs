using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


public class BuildTools {
	
	private static string[] scenes = FindEnabledEditorScenes();
	private static string targetDir = "../../Builds";
	
	[MenuItem ( "Build/Build All Targets" )]
	static void BuildAllTargets() { 
		BuildTarget startingTarget = EditorUserBuildSettings.activeBuildTarget;
 
		//BuildMac();
		//BuildWindows();
		//BuildWeb();
		BuildIOS();
		BuildAndroid();
 
		EditorUserBuildSettings.SwitchActiveBuildTarget( startingTarget );
	}
 
	[MenuItem ( "Build/Build Android" )]
	static void BuildAndroid() {
		//	Android
		
		string target = targetDir+"/Build_ANDROID";
		
		FileUtil.DeleteFileOrDirectory( target );
		Directory.CreateDirectory(target);
		
		PlayerSettings.keyaliasPass = "H@tch2012";
		PlayerSettings.keystorePass = "H@tch2012";


		string vstring = PlayerSettings.bundleVersion;
		int vInt = int.Parse(vstring.Replace(".", ""));

		PlayerSettings.Android.bundleVersionCode = vInt;


		Build( "/Build_ANDROID/SSGPlaces.apk", BuildTarget.Android );
	}
	
	[MenuItem ( "Build/Build iOS" )]  
	static void BuildIOS() {
		//	iOS
		string target = targetDir+"/Build_IOS";
		
		FileUtil.DeleteFileOrDirectory( target );
		Directory.CreateDirectory(target);
		
		Build( "/Build_IOS", BuildTarget.iPhone );
	}
	
	[MenuItem ( "Build/Build Windows" )]
	static void BuildWindows() {
		
		bool prefSett = PlayerSettings.MTRendering;
		PlayerSettings.MTRendering = false;
		//	Windows
		//Build( appName + ".exe", BuildTarget.StandaloneWindows );
		M2HPatcher.LoadSettings();
		M2HPatcher_CreatePatch.CheckPatchers();
		M2HPatcher.Publish();
		
		PlayerSettings.MTRendering = prefSett;
	}
	
	static void Build( string target, BuildTarget buildTarget, BuildOptions buildOptions = BuildOptions.None ) {
		GenericBuild( scenes, targetDir + "/" + target, buildTarget, buildOptions );
	}
	
	static void GenericBuild(string[] scenes, string target_dir, BuildTarget build_target, BuildOptions build_options) {
		EditorUserBuildSettings.SwitchActiveBuildTarget( build_target );
		string res = BuildPipeline.BuildPlayer( scenes, target_dir, build_target, build_options );
		if ( res.Length > 0 ) {
			throw new Exception( "BuildPlayer failure: " + res );
		}
	}
	
	private static string[] FindEnabledEditorScenes() {
		List<string> EditorScenes = new List<string>();
		foreach( EditorBuildSettingsScene scene in EditorBuildSettings.scenes ) {
			if ( !scene.enabled ) continue;
			EditorScenes.Add( scene.path );
		}
		return EditorScenes.ToArray();
	}
	
	public static void BuildBundles(){
		SSGEditorTools.BuildSupport.BuildBundles();
	}
}