using UnityEngine;
using System.Collections;
using pumpkin;
using pumpkin.display;
using SSGCore;
using HatchFramework;
/// <summary>
/// SSG engine Initializer - creates the SSGEngine and attaches to the current Behavior
/// </summary>

[RequireComponent(typeof(MovieClipOverlayCameraBehaviour))]
public class SSGEngineInitializer : MonoBehaviour {

	void Awake() {
		Application.targetFrameRate = 30;
		DisplayObject._dev_enable_depreciatedDisplayObjectSizing = true;
		pumpkin.swf.BitmapTextField.enableDeprecialtedTextWrapping = true;
		//DisplayObject._dev_enable_depreciatedGlobalToLocalTranslation = true;
	}

	// Use this for initialization
	void Start () {
	
		StartEngine();
		
	}
	
//	void OnGUI(){
//		
//		
//	
//		float scale = (float)Screen.height / 512f;
//		
//		GUIUtility.ScaleAroundPivot(new Vector2(scale, scale), Vector2.zero);
//		
//		GUILayout.BeginVertical();
//			if(GUILayout.Button("qaaa-aaaa-aaaa-aaa1", GUILayout.MinHeight(50))){
//				GlobalConfig.SetProperty("DEBUG_DEVICE_ID", "qaaa-aaaa-aaaa-aaa1");
//				StartEngine();
//			}
//			if(GUILayout.Button("qaaa-aaaa-aaaa-aaa2", GUILayout.MinHeight(50))){
//				GlobalConfig.SetProperty("DEBUG_DEVICE_ID", "qaaa-aaaa-aaaa-aaa2");
//			StartEngine();
//			}
//			if(GUILayout.Button("qaaa-aaaa-aaaa-aaa3", GUILayout.MinHeight(50))){
//				GlobalConfig.SetProperty("DEBUG_DEVICE_ID", "qaaa-aaaa-aaaa-aaa3");
//			StartEngine();
//			}
//			if(GUILayout.Button("qaaa-aaaa-aaaa-aaa4", GUILayout.MinHeight(50))){
//				GlobalConfig.SetProperty("DEBUG_DEVICE_ID", "qaaa-aaaa-aaaa-aaa4");
//			StartEngine();
//			}
//			if(GUILayout.Button("Unregistered", GUILayout.MinHeight(50))){
//				GlobalConfig.SetProperty("DEBUG_DEVICE_ID", "");
//				StartEngine();
//			}
//			if(GUILayout.Button("Skip", GUILayout.MinHeight(50))){
//				
//				StartEngine();
//			}
//			
//		GUILayout.EndVertical();
//		
//	}
	
	public void StartEngine(){
		gameObject.AddComponent<DebugConsole>();
		gameObject.AddComponent<SSGCore.SSGEngine>();
		Destroy (this);
	}
	
	
}
