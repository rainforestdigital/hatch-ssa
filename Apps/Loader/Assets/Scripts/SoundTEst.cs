using UnityEngine;
using System.Collections;
using HatchFramework;
using SSGCore;

public class SoundTEst : MonoBehaviour {
	
	public AudioClip[] clips;
	public AudioClip[] sourceClips;
	public int lastRnd;
	
	public float progress = 0;
	public AssetBundle assetBundle;
	// Use this for initialization
	IEnumerator Start () {
	
		WWW www = WWW.LoadFromCacheOrDownload("http://184.106.133.84/IOS_Sorting_0_0_0_6.unity3d", 1);
		
		while(!www.isDone){
			progress = www.progress;
			yield return null;
		}
		
		assetBundle = www.assetBundle;
		www.Dispose();
		www = null;
		progress  = 2;
		
	}
	
	
	public AudioClip GetAudioClip( string assetPath)
		{
			AudioClip clip = null;
			Debug.Log ("asset bundle: "+assetBundle);
			if(assetBundle!= null) {
				clip = (AudioClip)assetBundle.Load(assetPath, typeof(AudioClip));
			}
			Debug.Log (clip);
			return clip;
		}
	
	void OnGUI(){
	
		if(progress < 2){
			GUILayout.Label(progress.ToString()+"%");	
		}else{
		
			if(GUILayout.Button("Play Sound", GUILayout.Height(200))){

				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				bundle.AddClip(GetAudioClip("Narration/5B-Skill Instruction Audio/5B-Keep-Your-Finger-On-Shapes"));
				bundle.AddClip(GetAudioClip("Narration/5B-Skill Instruction Audio/5B-Keep-Your-Finger-On-Pictures"));
				bundle.AddClip(GetAudioClip("Narration/5B-Skill Instruction Audio/5B-now-its-your-turn"));
				bundle.AddClip(GetAudioClip("Narration/5B-Skill Instruction Audio/5B-lets-try-again"));
				SoundEngine.Instance.PlayBundle(bundle);
			}
			if(GUILayout.Button("Play Source Sound",  GUILayout.Height(200))){
			
				
				SoundEngine.SoundBundle bundle = new SoundEngine.SoundBundle();
				bundle.AddClip(sourceClips[lastRnd]);
				
				SoundEngine.Instance.PlayBundle(bundle);
			}
		}
	}
	
	
}
