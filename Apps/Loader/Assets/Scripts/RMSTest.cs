using UnityEngine;
using System.Collections;
using System.Collections.Generic;
			using Community.CsharpSqlite;

using LitJson;
using SSGCore;

public class RMSTest : MonoBehaviour
{
	SSGCore.RMS.Admin rms;
	public TextMesh log;
	private bool justExited = false;
	SSGCore.DBObject databaseObject = new DBObject ();
	private bool showWebserviceTests = false;
	
	void Start ()
	{
		rms = new SSGCore.RMS.Admin ();
		rms.ExitedEvent += OnRMSAdminExit;
	}

	// OnRMSAdminExit may be called from inside a thread
	// which doesn't have permission to interact with gameobjects
	void OnRMSAdminExit ()
	{
		justExited = true;
	}
	
	void Update ()
	{

		if (justExited) {
			log.text += "\n[" + Time.realtimeSinceStartup + "] Closed";
			justExited = false;
		}
	}
	
	void OnGUI ()
	{
		
		if (GUILayout.Button ("Open Local Database")) {

			databaseObject.Open ();
			databaseObject.LoadClassrooms();
			databaseObject.LoadUsers();
			databaseObject.LoadRefocusActivities();
			databaseObject.LoadSessions();
			
			
		}
		
		// Browser Test
		
		if (!rms.IsOpen ()) {
			if (GUILayout.Button ("Open Browser")) {
				log.text += "\n[" + Time.realtimeSinceStartup + "] Opened";
				rms.Open ();
			}
		}
		
		
		
		// Webservice Tests
		
		if (showWebserviceTests) {
			if (GUILayout.Button ("Hide Webservice Tests")) {
				showWebserviceTests = false;
			}
		//SSGCore.RMS.Webservice.Controller.Instance.TestGUI ();
			SSGCore.RMS.Webservice.TestGUI.Instance.GUI();

		} else {
			if (GUILayout.Button ("Show Webservice Tests")) {
				showWebserviceTests = true;
			}
		}
	

		
		SSGCore.RMS.DataSync.Instance.TestGUI (databaseObject);
	
	}
	
	void OnDataSyncComplete (bool status)
	{
		log.text += "\n[" + Time.realtimeSinceStartup + "] Sync complete: [" + status + "] ";
	}

}

