using UnityEngine;
using System.Collections;
using SSGCore;
using HatchFramework;

public class SkillSelectionTest : MonoBehaviour {
	
	
	TestSkillContext context;
	SkillSession session;
	// Use this for initialization
	IEnumerator Start () {
		
		yield return new WaitForEndOfFrame();
		DebugConsole.Instance.ToggleVisible();
		DebugConsole.Instance.Debug_Help();
		
		DebugConsole.LogWarning("RUNNING TEST WITH SKILL PROFICIENCY {0} AND MASTERY {1}", SkillProgress.ProficiencyThreshold, SkillProgress.MasteryThreshold);
		StartCoroutine(TestSkillSelection());	
	
	}
	
	bool continuousRun = false;
	
	[DebugConsoleMethod("auto", " - set automatic contiuation")]
	public void ContinuousRun(bool val){
		continuousRun = val;
		if(val)
			PickRandomResult();
	}
	
	[DebugConsoleMethod("random", " - pick a random result")]
	public void PickRandomResult(){
		result = RandomResult(next.Level);
		cont = true;
	}
	
	[DebugConsoleMethod("result", " - set a random result")]
	public void SetResult(float r){
		result = r;
		cont = true;
	}
	
	float RandomResult(SkillLevel level) {
		if(level == SkillLevel.Tutorial) {
			return (Random.Range(0, 10) < 8) ? 1 : 0;
		}
		else {
			const int trials = 10;
			int successes = 0;
			for (int i = 0; i < trials; i++) {
				if(Random.Range(0, 10) < 7) {
					successes++;
				}
			}
			
			return successes / (float)trials;
		}
	}
	
	IEnumerator TestSkillSelection() {
		
		
		context = new TestSkillContext();
		session = new SkillSession(context);
		next = session.ChooseFirstSkill();		
		
		
		while (next != null) {
			
			
			DebugConsole.Log(DebugConsole.LogLevel.DEBUG, "", string.Format("Playing Skill: {0} {1} ", next.Skill.linkage, next.Level));
			
			if(continuousRun)
				PickRandomResult();
			
			
			while( !cont){
				yield return new WaitForEndOfFrame();
				
			}
			
			DebugConsole.Log(DebugConsole.LogLevel.DEBUG, "", string.Format("result: {0}", result));
			
			session.UpdateProgress(next.Skill, next.Level, result);
			next = session.ChooseSkillForResult(next.Skill, next.Level, result);
			
			
			
			cont = false;
			
			if(continuousRun)
				yield return new WaitForSeconds(0.5f);
			//Debug.Log(string.Format("Got Skill: {0} {1}", next.Skill.linkage, next.Level));
			
		}
	}
	
	SkillLevelPair next;
	float result = 0;
	bool cont = false;
	
}
