using UnityEngine;
using System.Collections;
using pumpkin;
using pumpkin.display;
using pumpkin.geom;
using SSGCore;
using HatchFramework;


public class TestGlow : MonoBehaviour {

	RenderTexture output;
	IEnumerator Start(){
	
		yield return null;
		pumpkin.display.MovieClip mc = new pumpkin.display.MovieClip("uniSWF/common/swf/common_uniswf_overlay.swf:TEXT");
		
		FilterMovieClip filterMovieClip = new FilterMovieClip(mc);
		MovieClipOverlayCameraBehaviour.instance.stage.addChild(filterMovieClip);
		filterMovieClip.x = filterMovieClip.y = 300;
		
		
		DropShadowMovieClipFilter ds = new DropShadowMovieClipFilter(60, 3, 0.02f, Color.black, 4);
		filterMovieClip.AddFilter(ds);
		
		
		
	}
	
}
