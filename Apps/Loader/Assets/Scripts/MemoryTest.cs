using UnityEngine;
using System.Collections;
using HatchFramework;
using SSGCore;
using pumpkin;
using pumpkin.swf;
using pumpkin.display;

public class MemoryTest : MovieClipOverlayCameraBehaviour{
	
	public AudioClip[] clips;
	public AudioClip[] sourceClips;
	public int lastRnd;
	
	public float progress = 0;
	public AssetBundle assetBundle;
	protected WWW www;
	protected bool loaded = false;
	
	protected MovieClip mc;
	// Use this for initialization
	
//	public override void OnStart ()
//	{
//		loaded = false;
//	}
	
	IEnumerator Load() {
	
		www = WWW.LoadFromCacheOrDownload("http://184.106.133.84/IOS_Sorting_0_0_0_6.unity3d", 1);
		
		while(!www.isDone){
			progress = www.progress;
			yield return null;
		}
		
		Debug.Log ("Bundle loaded!");
		assetBundle = www.assetBundle;
		
		www.Dispose();
		www = null;
		progress  = 2;
		
		
		
	}
	

	
	void OnGUI(){
		
		if(loaded == false){
			if(GUILayout.Button("Load!")){
				StartCoroutine(Load());	
				loaded = true;
			}
			return;
		}
	
		if(progress < 2){
			GUILayout.Label(progress.ToString()+"%");	
		}else{
		
			DisplayView();
		}
	}
	
	void Unload(){
		loaded = false;	
		assetBundle.Unload(true);
		assetBundle = null;
		Resources.UnloadUnusedAssets();
	}
	
	void LoadSWF(){
		(MovieClip.rootResourceLoader as BuiltinResourceLoader).addAssetBundle(assetBundle);
		mc = new MovieClip("Sorting.swf:Skill");
		stage.addChild(mc);
	}
	
	void UnloadSWF(){
		(MovieClip.rootResourceLoader as BuiltinResourceLoader).removeAssetBundle(assetBundle);
		stage.removeChild(mc);
		mc = null;
	}
	
	void DisplayView(){
	
		if(loaded){
			if(GUILayout.Button("Unload assets", GUILayout.Height(200))){
				Unload();
			}
			
			if(mc == null){
				if( GUILayout.Button("loadswf", GUILayout.Height(200)))
					LoadSWF();
			}else if(mc != null){
				if( GUILayout.Button("unloadswf", GUILayout.Height(200)))
					UnloadSWF();	
			}
			
		}
	}
	
	
}
