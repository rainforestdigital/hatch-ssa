using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;


/// <summary>
/// Various helper methods
/// </summary>
public class UtilsPatcher : MonoBehaviour
{

    public static bool IsWebplayer()
    {
        return (Application.platform == RuntimePlatform.OSXDashboardPlayer || Application.platform == RuntimePlatform.OSXWebPlayer || Application.platform == RuntimePlatform.WindowsWebPlayer);
    }
    public static bool IsStandalone()
    {
        return (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer);
    }
    public static bool IsEditor()
    {
        return (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor);
    }

    public static bool IsAlfabetical(string input)
    {
        Regex r = new Regex("^[a-z]*$", RegexOptions.IgnoreCase);
        return r.IsMatch(input);
    }

    public static string Md5Sum(string strToEncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        // encrypt bytes
        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        // Convert the encrypted bytes back to a string (base 16)
        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }


    #region Splitting & Merging files

    public const int splitSizeMB = 100;
    public static long splitSizeBytes
    {
        get
        {
            return splitSizeMB * 1024 * 1024;
        }
    }

    public static List<string> SplitFile(string fileName){
        List<string> outputFiles = new List<string>();
        FileInfo fInfo = new FileInfo(fileName);
		if(!fInfo.Exists) {
			return outputFiles;
		}
        float fileMB = ((fInfo.Length/1024.0f)/1024.0f);
        int pieces = (int)Mathf.Ceil( fileMB /splitSizeMB);
        if (pieces < 1) pieces = 1;
		
        FileStream inFile = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Read);
        for (int i = 0; i < pieces; i++)
        {
            
            string outFilename = fileName + "_" + i + ".m2hsplit";
            outputFiles.Add(outFilename);
            FileStream outFile = new FileStream(outFilename, FileMode.OpenOrCreate, FileAccess.Write);
            int data = 0;
            byte[] buffer = new byte[splitSizeBytes];
            if ((data = inFile.Read(buffer, 0, (int)splitSizeBytes)) > 0)
            {
                outFile.Write(buffer, 0, data);
            }
            outFile.Close();
//            Debug.Log("Created splitted file: " + outFilename);
        }
        inFile.Close();
        return outputFiles;
    }

    public static List<string> FileListToSplittedList(List<string> fileList)
    {
        List<string> finalList = new List<string>();
        foreach (string file in fileList)
        {
          //  Debug.Log("Split source files"+file + "  ");
            finalList.AddRange(SplitFile(file));
            
            /*FileInfo fInfo = new FileInfo(file);
            if (fInfo.Length > splitSizeBytes)
            {            
                finalList.AddRange(SplitFile(file));
            }
            else
            {
                finalList.Add(file);
            }*/
        }
        return finalList;
    }

    public static string GetRealFilenameFromSplitted(string splittedFileName)
    {
        int lastIn = splittedFileName.LastIndexOf('_');
        return splittedFileName.Substring(0, lastIn);
    }

    public static void MergeFiles(string rootFolder, bool deleteSplittedFiles)
    {
        string[] files = Directory.GetFiles(rootFolder, "*_*.m2hsplit", SearchOption.AllDirectories);

        //get raw files
        List<string> uniqueFiles = new List<string>();        
        foreach (string file in files)
        {
            string realFileName = GetRealFilenameFromSplitted(file);
            if (!uniqueFiles.Contains(realFileName))
                uniqueFiles.Add(realFileName);
        }

        foreach (string uniqueFile in uniqueFiles)
        {
            int pieces = 0;
            while (File.Exists(uniqueFile + "_" + pieces + ".m2hsplit"))
            {
                pieces++;
            }
            Debug.Log("Merging " + uniqueFile+" into "+pieces+"  pieces");

            FileStream outFile = new FileStream(uniqueFile,FileMode.OpenOrCreate, FileAccess.Write);
            for (int i=0;i<pieces; i++)
            {
                string sourceFile  = uniqueFile+"_"+i+".m2hsplit";
                int data=0;
                byte [] buffer = new byte [1024];
                FileStream inFile = new FileStream(sourceFile,FileMode.OpenOrCreate, FileAccess.Read);
                while ((data=inFile.Read(buffer,0,1024))>0)
                {
                    outFile.Write(buffer,0,data);
                }
                inFile.Close();
                if (deleteSplittedFiles) File.Delete(sourceFile);
            }
            outFile.Close();
        }
    }

    public static void DeleteSplittedFiles(string directory)
    {
        foreach (string file in Directory.GetFiles(directory, "*.m2hsplit", SearchOption.AllDirectories))
        {
            File.Delete(file);
        }
    }

    #endregion


}
