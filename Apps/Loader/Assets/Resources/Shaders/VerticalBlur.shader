Shader "Custom/VerticalBlur" 
{
	Properties 
	{
		blurSize("BlurSize", Float) = 0.01
		strength("Strength", Float) = 0.22
		_MainTex ("Texture", 2D) = "white" { }	 
	}
	SubShader 
	{
		Pass 
		{
            Name "VerticalBlur"
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag			
			#include "UnityCG.cginc"
			
			float blurSize;
			float strength;
			sampler2D _MainTex;
			
			struct v2f 
			{
				float4  pos : SV_POSITION;
				float2  uv : TEXCOORD0;
			};
			
			float4 _MainTex_ST;
			
			v2f vert (appdata_base v)
			{
				v2f o;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
                return o;
			}
			
			half4 frag (v2f i) : COLOR
			{
				half4 sum = half4(0.0, 0.0, 0.0, 0.0);
				
				sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y - 5.0 * blurSize)) * 0.16 * strength;
                sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y - 4.0 * blurSize)) * 0.31 * strength;
                sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y - 3.0 * blurSize)) * 0.56 * strength;
                sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y - 2.0 * blurSize)) * 0.75 * strength;
                sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y - blurSize)) * 0.93 * strength;
                sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y)) * 1.0 * strength;
                sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y + blurSize)) * 0.93 * strength;
                sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y + 2.0 * blurSize)) * 0.75 * strength;
                sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y + 3.0 * blurSize)) * 0.56 * strength;
                sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y + 4.0 * blurSize)) * 0.31 * strength;
                sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y + 5.0 * blurSize)) * 0.16 * strength;
                
				return sum;
			}
			ENDCG
		}
	} 
	FallBack off
}
