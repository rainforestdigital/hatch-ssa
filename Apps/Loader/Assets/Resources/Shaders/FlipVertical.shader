Shader "Custom/FlipVertical" 
{
	Properties 
	{
		_MainTex ("Texture", 2D) = "white" { }	 
	}
	SubShader 
	{
		Pass 
		{
            Name "VerticalBlur"
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag			
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			
			struct v2f 
			{
				float4  pos : SV_POSITION;
				float2  uv : TEXCOORD0;
			};
			
			float4 _MainTex_ST;
			
			v2f vert (appdata_base v)
			{
				v2f o;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
                return o;
			}
			
			half4 frag (v2f i) : COLOR
			{
				half4 sum = half4(0.0, 0.0, 0.0, 0.0);
				sum += tex2D(_MainTex, float2(i.uv.x, 1.0 - i.uv.y));
				return sum;
			}
			ENDCG
		}
	} 
	FallBack off
}
